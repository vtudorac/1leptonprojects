from ROOT import *
from array import *
from math import *
from time import sleep
import AtlasStyle, sys
from optparse import OptionParser
gROOT.SetBatch(True)
def DrawAtlasInfo( x, y, label='', color=kBlack ) :
  latex = TLatex()
  latex.SetNDC();
  latex.SetTextFont( 42 )
  latex.SetTextSize( 0.042 )
  latex.SetTextColor( 1 )
  latex.SetTextAlign( 12 )
  latex.DrawLatex( x, y, '#bf{#it{ATLAS}} Internal' )
  latex.SetTextSize( 0.032 )
  latex.DrawLatex( x, y-0.07, '#intLdt = 35.7 fb^{-1}, #sqrt{s}=13.6 TeV' )
  latex.SetTextSize( 0.04 )
  latex.SetTextSize( 0.036 )
  latex.SetTextColor( color )
  

def PrepareLegend( x1, y1, x2, y2 ) :
  leg = TLegend( x1, y1, x2, y2 )
  leg.SetFillColor( 0 )
  leg.SetBorderSize( 0 )
  leg.SetTextFont( 42 )
  leg.SetTextSize( 0.03 )
  return leg

def DrawLine(xmin, ymin, xmax, ymax, color=kBlack) :
  line = TLine(xmin , ymin , xmax, ymax)
  line.SetLineWidth(2)
  line.SetLineColor(color) # 12 = gray
  return line


def DrawArrow(x1, y1, x2, y2, arrowCut='',sens=' ') :
  
  Line = TLine(x1, y1, x2, y2)
  
  if sens == 'up':
    arrow = TArrow(x1, y1, x1, y2, arrowCut, '|>')
  if sens == 'dw':
    arrow = TArrow(x1, y1, x1, y2, arrowCut, '<|')
  Line.SetLineWidth(2)
  Line.SetLineStyle(1)
  Line.SetLineColor(kRed) 
  arrow.SetLineWidth(2)
  arrow.SetLineStyle(1)
  arrow.SetLineColor(kRed) 
  arrow.SetFillColor(kRed)  
  return [Line, arrow]


def makeError(Hist):
    hError = Hist[0].Clone()
    for i in range(hError.GetNbinsX()):
        e = Hist[0].GetBinError(i+1) / Hist[0].GetBinContent(i+1) 
        print(e)
        hError.SetBinContent(i+1,1)
        hError.SetBinError(i+1,e)
    return hError


def PlotRatio(Hist):
  #print ("data",Hist[10].Integral(), "bkg",Hist[0].Integral())
  hRatio = Hist[10].Clone( )
  for ibin in range(0,Hist[0].GetNbinsX()+1):
  	data=Hist[10].GetBinContent(ibin)
  	bck=Hist[0].GetBinContent(ibin)
  	
  	if data>0:
  	 hRatio.SetBinContent(ibin,data/bck)
  	 #hRatio.Draw()
  	 #hRatio.SetMarkerSize(0)
  	 hRatio.GetYaxis().SetRangeUser( 0., 2.) 
  	 #hRatio.SetBinErrorOption( TH1.kPoisson )
  	 hRatio.SetBinError(ibin,sqrt(data)/bck)
	 
  	 hRatio.Draw("ep2")
  	 #hRatio.Draw("EPSAME") 
  	 #hRatio.GetYaxis().SetTickSize(0)
	 
  hRatio.GetYaxis().SetNdivisions( 505 )
  #hRatio.GetYaxis().SetLabelSize(9)
  #hRatio.GetYaxis().SetTitleSize(15)
  hRatio.GetYaxis().SetTitle( 'data/bkg' )
  hRatio.GetXaxis().SetTitleOffset( 0.95 )
  #hRatio.GetXaxis().SetLabelSize(9)
  #hRatio.GetXaxis().SetTitleSize(15)
  return hRatio 



  
def PlotStackPlot( hVariable, Files, variable, label='', selection=' ' ) :
 
  canRatio = TCanvas( 'global canvas', '', 800, 800 )
  canRatio.Divide( 1, 2 )
  canvas_up   = canRatio.cd(1)
  canvas_dw = canRatio.cd(2)
  canvas_dw.SetPad(0.0, 0.0, 1., 0.20)
  canvas_up.SetPad(0., 0.22, 1., 1.)
  canvas_up.SetBottomMargin(0)
  #canvas_dw.SetTopMargin(0.)
  canvas_dw.SetBottomMargin(0.37)
  
  
  Hist = {}
  hRatio ={}
  Legend = {}
  totBkg ={}
  TotError={}
  
  for i in range( 0, len(Files ) ) :
    File = TFile( Files[i][0] )
    if (i!=10): #data
    	Tree = File.Get( Files[i][2]+'_NoSys' )
    	Hist[i] = hVariable.Clone( 'histo'+str(i) )
    	Legend[i] = Files[i][1]
    	print (File.Get( Files[i][2]+'_NoSys' ))
    	
    	Tree.Draw( variable+' >>' +Hist[i].GetName(), selection )
    	
	#underflow bin
    	Hist[i].AddBinContent(1,Hist[i].GetBinContent(0))
    	Hist[i].SetBinContent(0,0)
	
	#overflow bin
    	n = Hist[i].GetNbinsX();
    	Hist[i].AddBinContent(n,Hist[i].GetBinContent(n+1))
    	Hist[i].SetBinContent(n+1,0);
    	Hist[i].SetBinErrorOption( TH1.kPoisson )
    	Hist[i].SetDirectory( 0 )
      
       
    else:
    	Tree = File.Get( Files[i][2])
    	Hist[i] = hVariable.Clone( 'histo'+str(i) )
    	Legend[i] = Files[i][1]
    	#print (File.Get( Files[i][2]))
    	#print(Hist[i].GetName())
    	
    	Tree.Draw( variable+' >>' +Hist[i].GetName(), selection )
	#underflow
    	Hist[i].AddBinContent(1,Hist[i].GetBinContent(0));
    	Hist[i].SetBinContent(0,0);
	#overflow
    	n = Hist[i].GetNbinsX();
    	Hist[i].AddBinContent(n,Hist[i].GetBinContent(n+1));
    	Hist[i].SetBinContent(n+1,0);
    	#Hist[i].SetBinError(n,sqrt(pow(Hist[i].GetBinError(n),2) + pow(Hist[i].GetBinError(n+1),2)));
    	#Hist[i].SetBinError(n+1,0);
    	Hist[i].SetBinErrorOption( TH1.kPoisson )
    	Hist[i].SetDirectory( 0 )
	
   	
    
    print ('****', "Name=",Hist[i].GetName(), "Entries=",Hist[i].GetEntries(), "Integral=", Hist[i].Integral())#,Hist[i].GetMaximum())
  
  ## building stack
  Hist[8].Add( Hist[9] )
  Hist[7].Add( Hist[8] )
  Hist[6].Add( Hist[7] )
  Hist[5].Add( Hist[6] )
  Hist[4].Add( Hist[5] )
  Hist[3].Add( Hist[4] )
  Hist[2].Add( Hist[3] )
  Hist[1].Add( Hist[2] )
  Hist[0].Add( Hist[1] )
  
  #up canvas
  canvas_up.cd()
  MaxList = []
  Colors = [ kGreen-2, kBlue, kMagenta, kViolet, kYellow, kAzure+9, kViolet+2, kPink, kOrange, kYellow+2, kBlack ]
  leg = PrepareLegend( 0.48, 0.84-0.04*len(Legend), 0.82, 0.84 )
  for i in range( 0, len(Legend)) :
    binWidth = Hist[i].GetXaxis().GetBinWidth(i)
    if (variable=="dPhi_lep_met" or variable=="nLep_signal" or variable =="nBJet" or variable =="nJet30" or variable =="jet1Phi" or variable =="jet1Eta"):
      binUnits =" "
    else:
      binUnits ="GeV" 
     
    ytitle = 'Events / {0} {1}'.format(binWidth, binUnits)
    Hist[i].GetYaxis().SetTitle( ytitle)
    #print("bin=",BinWidth)
    MaxList.append( Hist[i].GetMaximum() )
    #print (Hist[i].GetMaximum(),Legend[i])
    Hist[i].SetLineColor( Colors[i] )
    # filled background
    if (i!=10):
       Hist[i].SetFillColor( Colors[i] )
    if i==0:
    	Hist[i].Draw('HIST')
    	totBkg[i] = Hist[i].Clone()
    	totBkg[i].SetFillColor(0);
    	totBkg[i].SetMarkerStyle(0)
    	totBkg[i].SetLineColor(kBlack)	
    	totBkg[i].DrawCopy("SAMEHIST")
    	totBkg[i].SetFillColor(kBlack)
    	totBkg[i].SetFillStyle(3004)
    	#totBkg[i].SetMarkerSize(10)
    	#totBkg[i].SetLineWidth(2)
    	totBkg[i].Draw("SAME E2")
    	
    elif (i!=10):
    	Hist[i].Draw('SAMEHIST')
   
    if i==10:
      #Hist[i].SetLineStyle( 2 )
      #Hist[i].SetLineWidth( 3 )
      leg.AddEntry( Hist[i], str(Legend[i]), 'LP' )
      Hist[i].Draw('EPSAME')
    elif (i!=10):
       leg.AddEntry( Hist[i], str(Legend[i]), 'lf' )  
  leg.Draw()
  DrawAtlasInfo( 0.17, 0.81, label )
  #print ('aici',MaxList)
  MaxY = sorted( MaxList, reverse=True )[0]
  #print(MaxList)
  
  #down canvas, compute the ratio between data and bck
  canvas_dw.cd()
  canvas_dw.SetGridy()
  #plot ratio
  hRatio=PlotRatio(Hist)
  
  arrows = {} 
  #hData=Hist[10].Clone()
  #hbck=Hist[0].Clone()
  
  for ibin in range( hRatio.GetXaxis().GetNbins()+1 ):
  	RatioContent = hRatio.GetBinContent(ibin)
  	BinCenter = hRatio.GetBinCenter(ibin)
  	
  	if RatioContent > 2:
  		arrows[BinCenter]= DrawArrow( BinCenter, 1.5, BinCenter, 1.9, 0.0020,'up')
  		arrows[BinCenter][1].Draw()
  	if RatioContent < 0:
  		arrows[BinCenter]= DrawArrow( BinCenter, 0., BinCenter, 0.5, 0.0020,'dw')
  		#print ("aici",arrows[BinCenter])
  		arrows[BinCenter][1].Draw()
  ymin=hRatio.GetYaxis().GetXmin() 
  ymax=hRatio.GetYaxis().GetXmax()
  xmin=hRatio.GetXaxis().GetXmin() 
  xmax=hRatio.GetXaxis().GetXmax()
  
  
  
  #print(xmin,ymin,xmax,ymax)
  line = DrawLine(xmin, ymin+1, xmax, ymax, color=kBlack)
  line.SetLineColor(kGray+2)
  line.SetLineWidth(1) 
  line.Draw()
  hRatio.SetLineStyle(1)
  TotError=makeError(Hist)
  TotError.SetMarkerStyle(0) 
  TotError.SetFillColor(kBlack)
  TotError.SetFillStyle(3004)
  TotError.Draw("same L e3")
  TotError.GetYaxis().SetRangeUser( 0, 2 )
  
  #canRatio.SetLogy( 0 )
  #Hist[0].GetYaxis().SetRangeUser( 0, 1.5*MaxY )
  plotname=hVariable.GetName()
  #canRatio.Print( 'plots/'+plotname+'.pdf' )
  
  canvas_up.SetLogy( 1 )
  Hist[0].GetYaxis().SetRangeUser( 0.02, 20000000*MaxY )
  canvas_up.RedrawAxis()
  canRatio.Print( 'plots_mt400/'+plotname+'_Log.pdf' )
 
  

 
  
  


#==#==#==#==#==#==#==#==#==#==#==#

def main():

  lumi = 1
  labels = ''
  totalWeight = '(eventWeight*genWeight*pileupWeight*leptonWeight*jvtWeight*bTagWeight*'+str(lumi)+')'
  labels = ''
  selection = '(mt>50)*'+totalWeight
  
  samplesDir = '/data/analysis/vtudorac/Scripts/merged/'
  Samples = {
    ## 
    0 : ( samplesDir+'wjets_merged_processed.root',       'W+jets',  'wjets'),
    1 : ( samplesDir+'ttbar_merged_processed.root',       't#bar{t}', 'ttbar'),
    2 : ( samplesDir+'dijet_merged_processed.root',       'dijet', 'dijet'),
    3 : ( samplesDir+'diboson_merged_processed.root',     'Diboson','diboson'),
    4 : ( samplesDir+'Vgamma_merged_processed.root',      'Vgamma', 'Vgamma'),
    5 : ( samplesDir+'zjets_merged_processed.root',       'Z+jets', 'zjets'),
    6 : ( samplesDir+'singletop_merged_processed.root',   'Single top','singletop'),       
    7 : ( samplesDir+'higgs_merged_processed.root',       'higgs', 'higgs'),
    8 : ( samplesDir+'gammajet_merged_processed.root',    'gammajet', 'gammajet'),
    9 : ( samplesDir+'triboson_merged_processed.root',     'triboson', 'triboson'),
    10 : ( samplesDir+'data22_merged_processed.root',     'Data22', 'data22'),
    
  
  } 
  

  h_met = TH1F( 'met', ';missing E_{T} [GeV];', 13, 200, 1500 )
  PlotStackPlot( h_met, Samples, 'met', labels,selection)
  
  h_mt = TH1F( 'mt', ';m_{T} [GeV];', 20, 0, 2000 )
  PlotStackPlot( h_mt, Samples, 'mt', labels,selection)

  # ::                                                                                                                                                                              
  #h_jet2Pt = TH1F( 'jet2Pt', ';2^{nd} jet p_{T} [TeV];', 15, 0.025, 1.000 )
  #PlotStackPlot( h_jet2Pt, Samples, 'jet2Pt/1000', labels,selection )
  # ::
  h_LepPt = TH1F( 'lep1pT', ';leading lepton p_{T} [GeV]:', 10, 0., 1000. )
  PlotStackPlot( h_LepPt, Samples, 'lep1pT', labels,selection)
  
  h_dPhi_lep_met = TH1F( 'dPhi_lep_met', ';dPhi(lep,missing E_{T})', 10, 0., 3 )
  PlotStackPlot( h_dPhi_lep_met, Samples, 'dPhi_lep_met', labels,selection)
  
if __name__ == '__main__':
  sys.exit( main() )
