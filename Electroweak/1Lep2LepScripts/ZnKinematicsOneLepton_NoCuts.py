from ROOT import *
from array import *
from math import *
from time import sleep
import AtlasStyle, sys
from optparse import OptionParser
gROOT.SetBatch(True)
def DrawAtlasInfo( x, y, label='', color=kBlack ) :
  latex = TLatex()
  latex.SetNDC();
  latex.SetTextFont( 42 )
  latex.SetTextSize( 0.042 )
  latex.SetTextColor( 1 )
  latex.SetTextAlign( 12 )
  latex.DrawLatex( x, y, '#bf{#it{ATLAS}} Internal' )
  latex.SetTextSize( 0.032 )
  latex.DrawLatex( x, y-0.07, '#intLdt = 35.7 fb^{-1}, #sqrt{s}=13.6 TeV' )
  latex.SetTextSize( 0.04 )
  latex.SetTextSize( 0.036 )
  latex.SetTextColor( color )
  

def PrepareLegend( x1, y1, x2, y2 ) :
  leg = TLegend( x1, y1, x2, y2 )
  leg.SetFillColor( 0 )
  leg.SetBorderSize( 0 )
  leg.SetTextFont( 42 )
  leg.SetTextSize( 0.03 )
  return leg

def DrawLine(xmin, ymin, xmax, ymax, color=kBlack) :
  line = TLine(xmin , ymin , xmax, ymax)
  line.SetLineWidth(2)
  line.SetLineColor(color) # 12 = gray
  return line


def DrawArrow(x1, y1, x2, y2, arrowCut='',sens=' ') :
  
  Line = TLine(x1, y1, x2, y2)
  
  if sens == 'up':
    arrow = TArrow(x1, y1, x1, y2, arrowCut, '|>')
  if sens == 'dw':
    arrow = TArrow(x1, y1, x1, y2, arrowCut, '<|')
  Line.SetLineWidth(2)
  Line.SetLineStyle(1)
  Line.SetLineColor(kRed) 
  arrow.SetLineWidth(2)
  arrow.SetLineStyle(1)
  arrow.SetLineColor(kRed) 
  arrow.SetFillColor(kRed)  
  return [Line, arrow]



def ZnScan( hSignal, hBackground ) :
  ZnScan = hSignal.Clone( hSignal.GetName()+'_ZnScan' )
  ZnScan.GetYaxis().SetRangeUser( 0., 4 )
  ZnScan.GetYaxis().SetNdivisions( 505 )
  ZnScan.GetYaxis().SetTitle( 'Z_{N}' )
  ZnScan.GetXaxis().SetTitleOffset( 1 )
  ZnScan.SetLineStyle(1)
  
  for i in range( 0, ZnScan.GetNbinsX()+1) :
    S = hSignal.Integral( i, ZnScan.GetNbinsX()+1 )
    B = hBackground.Integral( i, ZnScan.GetNbinsX()+1 )
    significance = RooStats.NumberCountingUtils.BinomialExpZ( S, B, 0.3 )
    
    if(S>0 or B>0):
     ZnScan.SetBinContent(i,significance)
     #print(i,S,B,significance)
  ZnScan.SetDirectory(0)
  return ZnScan

def makeError(Hist):
    hError = Hist[0].Clone()
    for i in range(hError.GetNbinsX()):
        e = Hist[0].GetBinError(i+1) / Hist[0].GetBinContent(i+1) 
        #print(e)
        hError.SetBinContent(i+1,1)
        hError.SetBinError(i+1,e)
    return hError


def PlotRatio(Hist):
  #print ("data",Hist[10].Integral(), "bkg",Hist[0].Integral())
  hRatio = Hist[10].Clone( )
  for ibin in range(0,Hist[0].GetNbinsX()+1):
  	data=Hist[10].GetBinContent(ibin)
  	bck=Hist[0].GetBinContent(ibin)
  	
  	if data>0:
  	 hRatio.SetBinContent(ibin,data/bck)
  	 #hRatio.Draw()
  	 #hRatio.SetMarkerSize(0)
  	 hRatio.GetYaxis().SetRangeUser( 0., 2.) 
  	 #hRatio.SetBinErrorOption( TH1.kPoisson )
  	 hRatio.SetBinError(ibin,sqrt(data)/bck)
	 
  	 hRatio.Draw("ep2")
  	 #hRatio.Draw("EPSAME") 
  	 #hRatio.GetYaxis().SetTickSize(0)
	 
  hRatio.GetYaxis().SetNdivisions( 505 )
  #hRatio.GetYaxis().SetLabelSize(9)
  #hRatio.GetYaxis().SetTitleSize(15)
  hRatio.GetYaxis().SetTitle( 'data/bkg' )
  hRatio.GetXaxis().SetTitleOffset( 0.95 )
  #hRatio.GetXaxis().SetLabelSize(9)
  #hRatio.GetXaxis().SetTitleSize(15)
  return hRatio 



  
def PlotStackPlot( hVariable, Files, variable, label='') :
 
  canRatio = TCanvas( 'global canvas', '', 800, 800 )
  canRatio.Divide( 1, 2 )
  canvas_up   = canRatio.cd(1)
  canvas_dw = canRatio.cd(2)
  canvas_dw.SetPad(0.0, 0.0, 1., 0.20)
  canvas_up.SetPad(0., 0.22, 1., 1.)
  canvas_up.SetBottomMargin(0)
  #canvas_dw.SetTopMargin(0.)
  canvas_dw.SetBottomMargin(0.37)
  
  
  Hist = {}
  hRatio ={}
  Legend = {}
  totBkg ={}
  TotError={}
  
  lumi = 1
  lumi1=35700
  labels = ''
  totalWeight = '(eventWeight*genWeight*pileupWeight*leptonWeight*jvtWeight*bTagWeight*'+str(lumi)+')'
  totalWeight1 = '(eventWeight*'+str(lumi1)+')'
  
  labels = ''
  #selection = '(nLep_signal>=1)*(nBJet30_DL1==2)*'+totalWeight
  #selection1 = '(nLep_signal>=1)*(nBJet30_DL1==2)*'+totalWeight1
  
  selection = '1*'+totalWeight
  selection1 = '1*'+totalWeight1
  
  for i in range( 0, len(Files ) ) :
    File = TFile( Files[i][0] )
    if (i<10): #bkg
    	Tree = File.Get( Files[i][2]+'_NoSys' )
    	Hist[i] = hVariable.Clone( 'histo'+str(i) )
    	Legend[i] = Files[i][1]
    	print (File.Get( Files[i][2]+'_NoSys' ))
    	
    	Tree.Draw( variable+' >>' +Hist[i].GetName(), selection )
    	
	#underflow bin
    	Hist[i].AddBinContent(1,Hist[i].GetBinContent(0))
    	Hist[i].SetBinContent(0,0)
	
	#overflow bin
    	n = Hist[i].GetNbinsX();
    	Hist[i].AddBinContent(n,Hist[i].GetBinContent(n+1))
    	Hist[i].SetBinContent(n+1,0);
    	Hist[i].SetBinErrorOption( TH1.kPoisson )
    	Hist[i].SetDirectory( 0 )
      
       
    else: #signal
    	Tree = File.Get( Files[i][2])
    	Hist[i] = hVariable.Clone( 'histo'+str(i) )
    	Legend[i] = Files[i][1]
    	#print (File.Get( Files[i][2]))
    	#print(Hist[i].GetName())
    	
    	Tree.Draw( variable+' >>' +Hist[i].GetName(), selection1 )
	#underflow
    	Hist[i].AddBinContent(1,Hist[i].GetBinContent(0));
    	Hist[i].SetBinContent(0,0);
	#overflow
    	n = Hist[i].GetNbinsX();
    	Hist[i].AddBinContent(n,Hist[i].GetBinContent(n+1));
    	Hist[i].SetBinContent(n+1,0);
    	#Hist[i].SetBinError(n,sqrt(pow(Hist[i].GetBinError(n),2) + pow(Hist[i].GetBinError(n+1),2)));
    	#Hist[i].SetBinError(n+1,0);
    	Hist[i].SetBinErrorOption( TH1.kPoisson )
    	Hist[i].SetDirectory( 0 )
	
   	
    
    print ('****', "Name=",Hist[i].GetName(), "Entries=",Hist[i].GetEntries(), "Integral=", Hist[i].Integral())#,Hist[i].GetMaximum())
  
  ## building stack
  Hist[8].Add( Hist[9] )
  Hist[7].Add( Hist[8] )
  Hist[6].Add( Hist[7] )
  Hist[5].Add( Hist[6] )
  Hist[4].Add( Hist[5] )
  Hist[3].Add( Hist[4] )
  Hist[2].Add( Hist[3] )
  Hist[1].Add( Hist[2] )
  Hist[0].Add( Hist[1] )
  
  #up canvas
  canvas_up.cd()
  MaxList = []
  Colors = [ kGreen-2, kBlue, kMagenta, kViolet, kYellow, kAzure+9, kViolet+2, kPink, kOrange, kYellow+2,kBlack,kAzure+1,kRed+1,kGreen+3,kViolet+1,kGreen ]
  legB = PrepareLegend( 0.47, 0.65-0.01*len(Legend), 0.62, 0.82 )
  legS = PrepareLegend( 0.60, 0.65-0.01*len(Legend), 0.82, 0.84 )
  for i in range( 0, len(Legend)) :
    binWidth = Hist[i].GetXaxis().GetBinWidth(i)
    if (variable=="dPhi_lep_met" or variable=="nLep_signal" or variable =="nBJet30_DL1" or variable =="nJet30" or variable =="jet1Phi" or variable =="jet1Eta"):
      binUnits =" "
    else:
      binUnits ="GeV" 
     
    ytitle = 'Events / {0} {1}'.format(binWidth, binUnits)
    Hist[i].GetYaxis().SetTitle( ytitle)
    #print("bin=",BinWidth)
    MaxList.append( Hist[i].GetMaximum() )
    #print (Hist[i].GetMaximum(),Legend[i])
    
    # filled background
    Hist[i].SetLineColor( Colors[i] )
    if ( i<10 ) :  Hist[i].SetFillColor( Colors[i] )
    if ( i>9 ) :  
      Hist[i].SetLineStyle( 2 )
      Hist[i].SetLineWidth( 3 )
    #Hist[i].Draw( ('HIST' if i==0 else 'SAMEHIST') )
    if i==0:
    	Hist[i].Draw('HIST')
    	totBkg[i] = Hist[i].Clone()
    	totBkg[i].SetFillColor(0);
    	totBkg[i].SetMarkerStyle(0)
    	totBkg[i].SetLineColor(kBlack)	
    	totBkg[i].DrawCopy("SAMEHIST")
    	totBkg[i].SetFillColor(kBlack)
    	totBkg[i].SetFillStyle(3004)
    	#totBkg[i].SetMarkerSize(10)
    	#totBkg[i].SetLineWidth(2)
    	totBkg[i].Draw("SAME E2")
    	
    else:
    	Hist[i].Draw('SAMEHIST')
   
    if i<10:
      #Hist[i].SetLineStyle( 2 )
      #Hist[i].SetLineWidth( 3 )
      legB.AddEntry( Hist[i], str(Legend[i]), 'lf' )
     
    else:
       legS.AddEntry( Hist[i], str(Legend[i]), 'l' )  
  legS.Draw()
  legB.Draw()
  for s in range( 10, len(Legend)) :
    Hist[s].Draw( 'SAMEHIST' )
  DrawAtlasInfo( 0.17, 0.81, label )
  #print ('aici',MaxList)
  MaxY = sorted( MaxList, reverse=True )[0]
    
  SignalZnArray = {
    0 : ZnScan( Hist[10], Hist[0] ),
    1 : ZnScan( Hist[11], Hist[0] ),
    2 : ZnScan( Hist[12], Hist[0] ),
    3 : ZnScan( Hist[13], Hist[0] ),
    4 : ZnScan( Hist[14], Hist[0] ),
    5 : ZnScan( Hist[15], Hist[0] ),
  }
  
  
  #down canvas, compute the Zn
  canvas_dw.cd()
  canvas_dw.SetGridy()
  SignalZnArray[0].GetYaxis().SetTitle( 'Z_{N}' )
  for i in range( 0, len(SignalZnArray) ) :
    SignalZnArray[i].Draw( ('HIST' if i==0 else 'SAMEHIST') )
  
  plotname=hVariable.GetName()
  #canRatio.Print( 'plots/'+plotname+'.pdf' )
  
  canvas_up.SetLogy( 1 )
  Hist[0].GetYaxis().SetRangeUser( 0.02, 20000*MaxY )
  canvas_up.RedrawAxis()
  canRatio.Print( 'N2N3_Zh_plots_noCuts/'+plotname+'noCuts_Log.pdf' )
 
 
  


#==#==#==#==#==#==#==#==#==#==#==#

def main():

  #lumi = 1
  labels = ''
  #totalWeight = '(eventWeight*genWeight*pileupWeight*leptonWeight*jvtWeight*bTagWeight*'+str(lumi)+')'
  #labels = ''
  #selection = '1*'+totalWeight
  
  samplesDir = '/data/analysis/vtudorac/Scripts/merged/'
  samplesDir1 = '/data/analysis/vtudorac/Scripts/Truth/N2N3_Zh/600_1_0/'
  samplesDir2 = '/data/analysis/vtudorac/Scripts/Truth/N2N3_Zh/600_50_0/'
  samplesDir3 = '/data/analysis/vtudorac/Scripts/Truth/N2N3_Zh/600_100_0/'
  samplesDir4 = '/data/analysis/vtudorac/Scripts/Truth/N2N3_Zh/600_200_0/'
  samplesDir5 = '/data/analysis/vtudorac/Scripts/Truth/N2N3_Zh/600_300_0/'
  samplesDir6 = '/data/analysis/vtudorac/Scripts/Truth/N2N3_Zh/600_400_0/'
  Samples = {
    ## 
    0 : ( samplesDir+'wjets_merged_processed.root',       'W+jets',  'wjets'),
    1 : ( samplesDir+'ttbar_merged_processed.root',       't#bar{t}', 'ttbar'),
    2 : ( samplesDir+'dijet_merged_processed.root',       'dijet', 'dijet'),
    3 : ( samplesDir+'diboson_merged_processed.root',     'Diboson','diboson'),
    4 : ( samplesDir+'Vgamma_merged_processed.root',      'Vgamma', 'Vgamma'),
    5 : ( samplesDir+'zjets_merged_processed.root',       'Z+jets', 'zjets'),
    6 : ( samplesDir+'singletop_merged_processed.root',   'Single top','singletop'),       
    7 : ( samplesDir+'higgs_merged_processed.root',       'higgs', 'higgs'),
    8 : ( samplesDir+'gammajet_merged_processed.root',    'gammajet', 'gammajet'),
    9 : ( samplesDir+'triboson_merged_processed.root',     'triboson', 'triboson'),
   
    
    10: ( samplesDir6+'EwkOneLepton2024.root',       'N2N3_Zh_600_400',  'ntuple'),
    11: ( samplesDir5+'EwkOneLepton2024.root',       'N2N3_Zh_600_300',  'ntuple'),
    12: ( samplesDir4+'EwkOneLepton2024.root',       'N2N3_Zh_600_200',  'ntuple'),
    13: ( samplesDir3+'EwkOneLepton2024.root',       'N2N3_Zh_600_100',  'ntuple'),
    14: ( samplesDir2+'EwkOneLepton2024.root',       'N2N3_Zh_600_50',  'ntuple'),
    15: ( samplesDir1+'EwkOneLepton2024.root',       'N2N3_Zh_600_1',  'ntuple'),
    
  
  } 
  

  h_met = TH1F( 'met', ';missing E_{T} [GeV];', 12, 200, 800 )
  PlotStackPlot( h_met, Samples, 'met', labels)

  h_mt = TH1F( 'mt', ';m_{T} [GeV];', 20, 0, 2.000 )
  PlotStackPlot( h_mt, Samples, 'mt/1000', labels)
  
  h_mll = TH1F( 'mll', ';m_{ll} [GeV];', 10, 0, 200 )
  PlotStackPlot( h_mll, Samples, 'mll', labels)
  
  h_mCT = TH1F( 'mCT', ';m_{ct} [GeV];', 8, 0, 400 )
  PlotStackPlot( h_mCT, Samples, 'mct', labels)


  h_Lep1Pt = TH1F( 'lep1pT', ';1^{st} lepton p_{T} [GeV]', 10, 0., 500. )
  PlotStackPlot( h_Lep1Pt, Samples, 'lep1pT', labels)
  
  h_Lep2Pt = TH1F( 'lep2pT', ';2^{nd} lepton p_{T} [GeV]', 10, 0., 500. )
  PlotStackPlot( h_Lep2Pt, Samples, 'lep2pT', labels)
  
  h_jet1Pt = TH1F( 'jet1Pt', ';1^{st} jet p_{T} [TeV];', 15, 0.025, 1.000 )
  PlotStackPlot( h_jet1Pt, Samples, 'jet1pT/1000', labels)
  
  h_jet2Pt = TH1F( 'jet2Pt', ';2^{nd} jet p_{T} [TeV];', 15, 0.025, 1.000 )
  PlotStackPlot( h_jet2Pt, Samples, 'jet2pT/1000', labels)
  
  h_jet1Eta = TH1F( 'jet1Eta', ';1^{st} jet eta;', 10, -3, 3 )
  PlotStackPlot( h_jet1Eta, Samples, 'jet1Eta', labels)
  
  h_jet2Eta = TH1F( 'jet2Eta', ';2^{nd} jet eta;', 10, -3, 3 )
  PlotStackPlot( h_jet2Eta, Samples, 'jet2Eta', labels)
 
  h_jet1Phi = TH1F( 'jet1Phi', ';1^{st} jet phi;', 10, -3, 3 )
  PlotStackPlot( h_jet1Phi, Samples, 'jet1Phi', labels)
  
  h_jet2Phi = TH1F( 'jet2Phi', ';2^{nd} jet phi;', 10, -3, 3 )
  PlotStackPlot( h_jet2Phi, Samples, 'jet2Phi', labels)
  
  h_nLep_signal = TH1F( 'nLep_signal', ';n_{lep};', 5, 0, 5 )
  PlotStackPlot( h_nLep_signal, Samples, 'nLep_signal', labels)
  
  h_nJet30 = TH1F( 'nJet30', ';n_{jet30};', 10, 0, 10 )
  PlotStackPlot( h_nJet30, Samples, 'nJet30', labels)
  
  h_nBJet30 = TH1F( 'nBJet30_DL1', ';n_{Bjet30};', 10, 0, 10 )
  PlotStackPlot( h_nBJet30, Samples, 'nBJet30_DL1', labels)

  
  
  #h_dPhi_lep_met = TH1F( 'dPhi_lep_met', ';dPhi(lep,missing E_{T})', 10, 0., 3 )
  #PlotStackPlot( h_dPhi_lep_met, Samples, 'dPhi_lep_met', labels,selection)
  
if __name__ == '__main__':
  sys.exit( main() )
