import ROOT
import contourPlotter

ROOT.gROOT.LoadMacro("/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/backup/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.LoadMacro("/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/backup/AtlasStyle/AtlasLabels.C")

drawTheorySysts = False

# C1N2_WZ
#name = "C1N2_WZ_prodApril_mjjZ80100"
# C1C1_WW
name = "C1C1_WW_prodApril_mjjW7585"

plot = contourPlotter.contourPlotter(name, 800, 600)

plot.processLabel = "C1C1-WW model"
plot.lumiLabel = "#sqrt{s}=13 TeV, 139 fb^{-1}, All limits at 95% CL"

f = ROOT.TFile("/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/outputs/C1C1_WW/"+ name + ".root")
f.ls()

plot.drawAxes([0,0,1000,400])

plot.drawOneSigmaBand(f.Get("Band_1s_0"))
plot.drawExpected(f.Get("Exp_0"))
plot.drawTextFromTGraph2D(f.Get("CLs_gr"), angle = 45, title = "Grey Numbers Represent Expected CLs Value")
#plot.drawObserved(f.Get("Obs_0"), title = "Observed Limit")

#plot.setXAxisLabel("m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} [GeV]")
plot.setXAxisLabel("m_{#tilde{#chi}^{#pm}_{1}} [GeV]")
plot.setYAxisLabel("m_{#tilde{#chi}^{0}_{1}} [GeV]")

plot.createLegend(shape=(0.24,0.65,0.5,0.78) ).Draw()

if drawTheorySysts:
	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
	# coordinate in NDC
	plot.drawTheoryLegendLines( xyCoord=(0.234,0.6625), length=0.057 )

#ROOT.ATLASLabel(0.24,0.85,"Internal")
plot.decorateCanvas()

#l = ROOT.TLatex()
#l.SetNDC()
#l.SetTextColor( 1 )
#l.SetTextFont(42)
#l.SetTextSize(0.05)

plot.writePlot()
