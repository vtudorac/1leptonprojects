################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import subprocess
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys, path
import fnmatch


from SignalAccUncertainties import SignalTheoryUncertainties

from logger import Logger
log = Logger('EWK2ndWaveFit')

# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels, bkgFiles, systList):
    for chan in channels:
        for sample in chan.sampleList:
            if sample.name != "data" and not "C1N2_WZ" in sample.name and not "C1C1_WW" in sample.name:
                if "diboson" in sample.name: sample.addInputs(bkgFiles["diboson"])
                else: sample.addInputs(bkgFiles[sample.name])
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not - useful to see when to load input trees from eos or from a local directory
onLxplus = 'lx' in subprocess   .getstatusoutput("hostname")[1] or 'vm' in subprocess   .getstatusoutput("hostname")[1] or '.cern.ch' in subprocess   .getstatusoutput("hostname")[1]

debug = False #!!!

if debug: 
    print ("WARNING: Systematics disabled for testing purposes!!!")

# here we have the possibility to activate different groups of systematic uncertainties
SystList = []

SystList.append("JER")                  # Jet Energy Resolution (common)
SystList.append("JES")                  # Jey Energy Scale (common)
SystList.append("MET")                  # MET (common)
SystList.append("LEP")                  # lepton uncertainties (common)
SystList.append("LepEff")               # lepton scale factors (common)
SystList.append("JVT")                  # JVT scale factors (common)
SystList.append("pileup")               # pileup (common)
SystList.append("BTag")                 # b-tagging uncertainties
SystList.append("diboson")              # scale variation for renormalization, factorization, resummation and generator comparison
#SystList.append("multiboson")           # scale variation for renormalization, factorization, resummation and generator comparison
SystList.append("wjets")                # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
SystList.append("ttbar")                # Radiation and QCD scales, Hadronization/fracmentation, Hard scattering generation
SystList.append("singletop")            # Radiation and QCD scales, Hadronization/fracmentation, Hard scattering generation
SystList.append("zjets")                # scale variation for renormalization, factorization, resummation, matching
SystList.append("ttv")                  # part of ttv uncertainties
SystList.append("tth")                  # part of tth uncertainties
SystList.append("vh")                   # part of vh uncertainties
SystList.append("BSTag")                # boson tagging sf uncertainties

if debug: 
    SystList = []

# for creating the backup cache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False

# for creating histograms in the backup cache file, we could run -t option to create histograms of backgrounds and data in all regions for both analyses
doBackupCacheFile = False

# Toggle N-1 plots
doNMinus1Plots = False

# Use reduced JES systematics
useReducedJESNP = False

# Use N Jet normalization factor
useNJetNormFac = False

# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal = True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["resolved", "boosted"]                 # default - modify from command line

# activate inclusive signal regions for model-dependent fit and deactivate exclusive SRs: incl = True
# note that for inclusive SRs you cannot run more than one at a time (they are not orthogonal), still to get similar normalization factors as for the exclusion SRs, the same exclusive SRs are run
# for the VRs, the inclusive SRs are automatically enabled at the same time as the exclusive SRs are run
#incl = False

# Tower selected from command-line
# pickedSRs is set by the "-r" HisFitter option
try:
    pickedSRs
except NameError:
    pickedSRs = None

if pickedSRs != None and len(pickedSRs) >= 1:
    CRregions = pickedSRs
    print ("\n Tower defined from command line: ", pickedSRs, "      (-r resolved,boosted option)")
CRregions = CRregions + ["general"]

# activate associated validation regions 
ValidRegList = {}
# for plotting (turn to True if you want to use them):
ValidRegList["resolved"] = False
ValidRegList["boosted"] = False
ValidRegList["general"] = False

# for tables (turn doTableInputs to True)
doTableInputs = True    # This is used for bkg fit incl VR tables!  # This effectively means no validation plots but only validation tables (100x faster)

# N-1 plots in SRs
VRplots = False

# whether using same normfactor for ttbar and st
OneNFTopST = False


if myFitType == FitType.Exclusion:
    doTableInputs = False ## no need VR tables when exclusion fit


for cr in CRregions:
    if "resolved" in cr and doTableInputs:
        ValidRegList["resolved"] = True
    if "boosted" in cr and doTableInputs:
        ValidRegList["boosted"] = True
    if "general" in cr and doTableInputs:
        ValidRegList["general"] = True


# choose which kind of fit you want to perform: ShapeFit (True) or NoShapeFit (False)
#doShapeFit = True          #

# choose which analysis you want to work with: C1N2_WZ or C1C1_WW using e.g -u ana_C1N2_WZ
myAna = "C1N2_WZ"
if configMgr.userArg != "":
    if "ana" in configMgr.userArg: 
        myAna = configMgr.userArg.replace("ana_", "")

# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    if myAna == "C1N2_WZ":
        if not debug: sigSamples = ["C1N2_WZ_300_0"]
        else: sigSamples = ["C1N2_WZ_200_0", "C1N2_WZ_200_50", "C1N2_WZ_300_0", "C1N2_WZ_300_100", "C1N2_WZ_300_150", "C1N2_WZ_400_0", "C1N2_WZ_400_150", "C1N2_WZ_400_250", "C1N2_WZ_400_50", "C1N2_WZ_500_0", "C1N2_WZ_500_100", "C1N2_WZ_500_200", "C1N2_WZ_500_300", "C1N2_WZ_600_0", "C1N2_WZ_600_100", "C1N2_WZ_600_200", "C1N2_WZ_600_300", "C1N2_WZ_600_400", "C1N2_WZ_700_0", "C1N2_WZ_700_100", "C1N2_WZ_700_200", "C1N2_WZ_700_300", "C1N2_WZ_700_400", "C1N2_WZ_700_500", "C1N2_WZ_800_0", "C1N2_WZ_800_100", "C1N2_WZ_800_200", "C1N2_WZ_800_300", "C1N2_WZ_800_400", "C1N2_WZ_800_500", "C1N2_WZ_900p0_0p0", "C1N2_WZ_900_100", "C1N2_WZ_900p0_200p0", "C1N2_WZ_900_300", "C1N2_WZ_900p0_400p0", "C1N2_WZ_900_500", "C1N2_WZ_1000_0", "C1N2_WZ_1000_100", "C1N2_WZ_1000_200", "C1N2_WZ_1000_300", "C1N2_WZ_1000_400", "C1N2_WZ_1000_500", "C1N2_WZ_1100_0", "C1N2_WZ_1100_100", "C1N2_WZ_1100_200", "C1N2_WZ_1100_300", "C1N2_WZ_1100_400", "C1N2_WZ_1100_500", "C1N2_WZ_1200_0", "C1N2_WZ_1200_100", "C1N2_WZ_1200_200", "C1N2_WZ_1200_300", "C1N2_WZ_1200_400", "C1N2_WZ_500_250", "C1N2_WZ_600_250", "C1N2_WZ_650_250", "C1N2_WZ_700_250","C1N2_WZ_750_250","C1N2_WZ_850_250"]
    if myAna == "C1C1_WW":
        if not debug: sigSamples = ["C1C1_WW_300_0"]
        else: sigSamples = ["C1C1_WW_200_0", "C1C1_WW_200_50", "C1C1_WW_300_0", "C1C1_WW_300_100", "C1C1_WW_300_150", "C1C1_WW_400_0", "C1C1_WW_400_150", "C1C1_WW_400_250", "C1C1_WW_400_50", "C1C1_WW_500_0", "C1C1_WW_500_100", "C1C1_WW_500_200", "C1C1_WW_500_300", "C1C1_WW_600_0", "C1C1_WW_600_100", "C1C1_WW_600_200", "C1C1_WW_600_300", "C1C1_WW_600_400", "C1C1_WW_700_0", "C1C1_WW_700_100", "C1C1_WW_700_200", "C1C1_WW_700_300", "C1C1_WW_700_400", "C1C1_WW_700_500", "C1C1_WW_800_0", "C1C1_WW_800_100", "C1C1_WW_800_200", "C1C1_WW_800_300", "C1C1_WW_800_400", "C1C1_WW_800_500", "C1C1_WW_900p0_0p0", "C1C1_WW_900_100", "C1C1_WW_900p0_200p0", "C1C1_WW_900_300", "C1C1_WW_900p0_400p0", "C1C1_WW_900_500", "C1C1_WW_1000_0", "C1C1_WW_1000_100", "C1C1_WW_1000_200", "C1C1_WW_1000_300", "C1C1_WW_1000_400", "C1C1_WW_1000_500", "C1C1_WW_1100_0", "C1C1_WW_1100_100", "C1C1_WW_1100_200", "C1C1_WW_1100_300", "C1C1_WW_1100_400", "C1C1_WW_1100_500", "C1C1_WW_1200_0", "C1C1_WW_1200_100", "C1C1_WW_1200_200", "C1C1_WW_1200_300", "C1C1_WW_1200_400"]


#### dol MJ
print ('dol check: sigSamples[0] = ', sigSamples[0])
if "C1C1_WW" in sigSamples[0]:
    myAna = "C1C1_WW"
if "C1C1_WZ" in sigSamples[0]:
    myAna = "C1C1_WZ"


# define the analysis name: 
analysissuffix = ""

if not doBackupCacheFile:
    if myFitType == FitType.Exclusion:
        analysissuffix += "_" + sigSamples[0]
    else:
        if myAna == "C1N2_WZ": analysissuffix += "_C1N2_WZ"
        if myAna == "C1C1_WW": analysissuffix += "_C1C1_WW"

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr

analysissuffix_BackupCache = analysissuffix

if myFitType == FitType.Exclusion:
    analysissuffix += "_excl"


mylumi = 138.96516

mysamples = ["diboson", "multiboson", "singletop", "ttbar", "tth", "ttv", "vh", "wjets", "zjets", "data"]

doOnlySignal = False

# check for a user argument given with -u
myoptions = configMgr.userArg
analysisextension = ""
if doBackupCacheFile: analysisextension += "_backup"
if debug: analysisextension += "_NoSys"
if myoptions != "":
    if 'sensitivity' in myoptions:          # userArg should be something like 'sensitivity_3'
        mylumi = float(myoptions.split('_')[-1])
        analysisextension += "_" + myoptions
    
    if myoptions == 'doOnlySignal':
        doOnlySignal = True
        analysisextension += "_onlysignal"

    if 'samples' in myoptions:               # userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split('_'):
            if sam != 'samples':
                mysamples.append(sam)
        analysisextension += "_" + myoptions
else:
    print ("No additional user arguments given - proceed with default analysis!")

print ("Using lumi: ", mylumi)

# First define HistFactory attributes
configMgr.analysisName = "EWK2ndWaveFit_prodOct2021" + analysissuffix + analysisextension + "_v1.0.0"
configMgr.outputFileName = "results/" + configMgr.analysisName + ".root"
configMgr.histCacheFile = "data/" + configMgr.analysisName + ".root"

# activate using of background histogram cache file to speed up processes
#if myoptions != "":
useBackupCache = True
if onLxplus and not myFitType == FitType.Discovery:
    useBackupCache = False #!!!
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache       # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache       # enable the use of an alternate data file
    #MyAnalysisName_BackupCache = "EWK2ndWaveFit" + analysissuffix_BackupCache + analysisextension
    if not onLxplus: histBackupCacheFileName = "data/EWK2ndWaveFit_backup.root"
    print (" using backup cache file: " + histBackupCacheFileName)
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi/inputLumi
configMgr.inputLumi = 0.001             # input lumi 1 pb^-1 = normalization of the HistFitter trees
configMgr.outputLumi = mylumi           # for test
configMgr.setLumiUnits("fb-1")          # Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1

configMgr.fixSigXSec = True

if debug: configMgr.fixSigXSec = False


useToys = False     #!!!
if useToys:
    configMgr.calculatorType = 0            # frequentist calculator (use toys)
    configMgr.nTOYs = 10000                 # number of toys when using frequentist calculator
    print (" using frequentist calculator (toys)")
else:
    configMgr.calculatorType = 2            # asymptotic calculator (creates asimov data set for the background hypothesis)
    print (" using asymptotic calculator")
configMgr.testStatType = 3                  # one-sided profile likelihood test statistics
configMgr.nPoints = 20                      # number of values scanned of signal-strength for upper-limit determination of signal strength

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

# writing xml files for bebugging purposes
configMgr.writeXML = True
if debug: configMgr.writeXML = False 


# blinding of various regions
configMgr.blindSR = False           # Blind the SRs (default is False)
configMgr.blindCR = False           # Blind the CRs (default is False)
configMgr.blindVR = False           # Blind the VRs (default is False)
doBlindSRinBGfit =  False           # Blind SR when performing a bkg fit

if debug:
    configMgr.blindSR = False           # Blind the SRs (default is False)
    configMgr.blindCR = False           # Blind the CRs (default is False)
    configMgr.blindVR = False           # Blind the VRs (default is False)
    doBlindSRinBGfit =  False           # Blind SR when performing a bkg fit

#useSignalInBlindedData = True
configMgr.ReduceCorrMatrix = True

configMgr.prun = True
configMgr.prunThreshold = 0.02

# using of statistical uncertainties
useStat = True

# Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
#FullSimSig = []

# FastSim signals use JET_JER_DataVsMC_AFII and JET_PunchThrough_AFII, while other FullSim samples used JET_JER_DataVsMC_MC16 and JET_PunchThrough_MC16.
# Here, the 2 latter will be added firstly for all samples, then they will be replaced by the 2 former later for FastSim signals, which are listed below
FastSimSig = ["C1N2_WZ_200_0", "C1N2_WZ_200_50", "C1N2_WZ_300_100", "C1N2_WZ_300_150", "C1N2_WZ_400_150", "C1N2_WZ_400_250", "C1N2_WZ_500_300", "C1N2_WZ_600_400", "C1N2_WZ_700_500", "C1C1_WW_200_0", "C1C1_WW_200_50", "C1C1_WW_300_100", "C1C1_WW_300_150", "C1C1_WW_400_150", "C1C1_WW_400_250", "C1C1_WW_500_300", "C1C1_WW_600_400", "C1C1_WW_700_500"]


# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

inputDir = "/data/vtudorac/EWK2ndWave/SystProduction/skimmed_with_JET_LargeR_JER/"
inputDirSig = "/data/vtudorac/EWK2ndWave/SystProduction/skimmed_with_JET_LargeR_JER/"
inputDirData = "/data/vtudorac/EWK2ndWave/NominalProduction/"

if not onLxplus:
    inputDir = "/data/vtudorac/EWK2ndWave/SystProduction/skimmed_with_JET_LargeR_JER/"
    inputDirSig = "/data/vtudorac/EWK2ndWave/SystProduction/skimmed_with_JET_LargeR_JER/"
    inputDirData = "/data/vtudorac/EWK2ndWave/NominalProduction/"

# Bkg files
bkgFiles_e = {}           # for electron channel
bkgFiles_m = {}           # for muon channel
bkgFiles_em = {}          # for both
for sam in mysamples:
    if sam != "data":
        bkgFiles_e[sam] = [inputDir + "allTrees_bkg_skimmed_new.root"]
        bkgFiles_m[sam] = [inputDir + "allTrees_bkg_skimmed_new.root"]
        bkgFiles_em[sam] = [inputDir + "allTrees_bkg_skimmed_new.root"]


# signal files
if myFitType == FitType.Exclusion or doOnlySignal:
    sigFiles_e = {}
    sigFiles_m = {}
    sigFiles_em = {}
    for sigpoint in sigSamples:
        ## dol MJ
        if myAna == "C1N2_WZ":
            sigFiles_e[sigpoint] = [inputDirSig + "C1N2_WZ_skimmed_new.root"]
            sigFiles_m[sigpoint] = [inputDirSig + "C1N2_WZ_skimmed_new.root"]
            sigFiles_em[sigpoint] = [inputDirSig + "C1N2_WZ_skimmed_new.root"]
        if myAna == "C1C1_WW":
            sigFiles_e[sigpoint] = [inputDirSig + "C1C1_WW_skimmed_new.root"]
            sigFiles_m[sigpoint] = [inputDirSig + "C1C1_WW_skimmed_new.root"]
            sigFiles_em[sigpoint] = [inputDirSig + "C1C1_WW_skimmed_new.root"]


# data files
dataFiles = [inputDirData + "allTrees_data_NoSys.root"]

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

# 1l Preselection
preSelection_1l = "nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2"

# 2l selection
preSelection_2l = "nLep_base==2 && nLep_signal==2 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1"

# SRs
# resolved
resolvedSRSelection = "lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.6 && mjj>70. && mjj<105. && nFatjets==0"
configMgr.cutsDict["SRLMresolved"] = preSelection_1l + "&&" + resolvedSRSelection + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMresolved"] = preSelection_1l + "&&" + resolvedSRSelection + "&& mt>300."
# boosted for C1N2_WZ analysis
boostedSRSelection_WZ = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. && ((mjj>80. && mjj<100. && meffInc30<850.) || (meffInc30>850.))"
configMgr.cutsDict["SRLMboostedWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>120. && mt<200."
configMgr.cutsDict["SRMMboostedWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMboostedWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>300."
configMgr.cutsDict["SRHMboostedN1mtWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ 


# boosted for C1C1_WW analysis
boostedSRSelection_WW = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && ((met_Signif>12. && mjj>70. && mjj<90. && meffInc30<850.) || (met_Signif>15. && meffInc30>850.))"
configMgr.cutsDict["SRLMboostedWW"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>120. && mt<200."
configMgr.cutsDict["SRMMboostedWW"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMboostedWW"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>300."


#discovery region
configMgr.cutsDict["SRLMboostedWZdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6&& nFatjets>=1  && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. && (mjj>80. && mjj<100.)&&mt>120&&mt<200" # add meffInc30>600.
configMgr.cutsDict["SRMMboostedWZdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. &&mt>200&& mt<300" #&& meffInc30>850.
configMgr.cutsDict["SRHMboostedWZdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. && mt>300" #&& meffInc30>850.

configMgr.cutsDict["SRLMboostedWWdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && met_Signif>15. &&mt>120&&mt<200"  #meffInc30>600.
configMgr.cutsDict["SRMMboostedWWdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && met_Signif>15. &&mt>200&& mt<300" #meffInc30>600.
configMgr.cutsDict["SRHMboostedWWdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && met_Signif>12. && mjj>70. && mjj<90. && mt>300"# meffInc30>850



# DB2L CR/VR
DB2LSelection = "lep1Pt>25. && lep2Pt>25. && nJet30>=1 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.9 && mll>70. && mll<100. && (mjj<75. || mjj>95.)"
configMgr.cutsDict["DB2LCR"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>12. && mt>50. && mt<200."
configMgr.cutsDict["DB2LVR"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>10. && mt>200. && mt<350."

# WDB1L boosted CR/VR
WDB1LBoostedSelection = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.9 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250."
configMgr.cutsDict["WDB1LCRboosted"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>50. && mt<80."
configMgr.cutsDict["WDB1LVR1boosted"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80."
configMgr.cutsDict["WDB1LVR2boosted"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120."

# WDB1L resolved CR/VR
WDB1LResolvedSelection = "lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.8 && mjj>70. && mjj<105. && nFatjets==0 && met_Signif>12."
configMgr.cutsDict["WDB1LCRresolved"] = preSelection_1l + "&&" + WDB1LResolvedSelection + "&& mt>50. && mt<80."
configMgr.cutsDict["WDB1LVRresolved"] = preSelection_1l + "&&" + WDB1LResolvedSelection + "&& mt>80. && mt<200."

# Top boosted CR/VR
TBoostedSelection = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && nBJet30_DL1>0 && met>200. && dPhi_lep_met<2.9 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250."
configMgr.cutsDict["TCRboosted"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12 && mt>50. && mt<80."
configMgr.cutsDict["TVR1boosted"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80."
configMgr.cutsDict["TVR2boosted"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120."

# Top resolved CR/VR
TResolvedSelection = "lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1>0 && met>200. && dPhi_lep_met<2.8 && mjj>70. && mjj<105. && nFatjets==0"
configMgr.cutsDict["TCRresolved"] = preSelection_1l + "&&" + TResolvedSelection + "&& mt>50. && mt<80."
configMgr.cutsDict["TVRresolved"] = preSelection_1l + "&&" + TResolvedSelection + "&& mt>80. && mt<200."




d = configMgr.cutsDict
defined_regions = []
if "resolved" in CRregions:
    defined_regions += ["WDB1LCRresolved", "TCRresolved", "WDB1LVRresolved", "TVRresolved", "SRLMresolved", "SRHMresolved"]
if "boosted" in CRregions:
    defined_regions += ["WDB1LCRboosted", "TCRboosted", "WDB1LVR1boosted", "WDB1LVR2boosted", "TVR1boosted", "TVR2boosted"]
    if not doBackupCacheFile:
        if myAna == "C1N2_WZ":
            defined_regions += ["SRLMboostedWZ", "SRMMboostedWZ", "SRHMboostedWZ","SRLMboostedWZdisc", "SRMMboostedWZdisc","SRHMboostedWZdisc","SRHMboostedN1mtWZ"]
        if myAna == "C1C1_WW":
            defined_regions += ["SRLMboostedWW", "SRMMboostedWW", "SRHMboostedWW","SRLMboostedWWdisc", "SRMMboostedWWdisc","SRHMboostedWWdisc"]
    else:
        defined_regions += ["SRLMboostedWZ", "SRMMboostedWZ", "SRHMboostedWZ", "SRLMboostedWW", "SRMMboostedWW", "SRHMboostedWW", "SRLMboostedWZdisc", "SRMMboostedWZdisc","SRHMboostedWZdisc","SRLMboostedWWdisc", "SRMMboostedWWdisc","SRHMboostedWWdisc","SRHMboostedN1mtWZ"]
if "general" in CRregions:
    defined_regions += ["DB2LCR", "DB2LVR"]

for pre_region in defined_regions:
    configMgr.cutsDict[pre_region + "El"] = d[pre_region] + "&& AnalysisType==1"
    configMgr.cutsDict[pre_region + "Mu"] = d[pre_region] + "&& AnalysisType==2"
    configMgr.cutsDict[pre_region + "EM"] = d[pre_region] + "&& (AnalysisType==1 || AnalysisType==2)"

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #

# all the weights we need for a default analysis - the boson tagging weights are added later
weights = ["genWeight", "eventWeight", "leptonWeight", "jvtWeight", "pileupWeight", "trigWeight_singleLepTrig", "polWeight", "bTagWeight"]

configMgr.weights = weights

# Systematics

# example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

# muon related uncertainties acting on weights
# Bad muon
#muonEffBadMuonHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_BADMUON_SYS__1up")
#muonEffBadMuonLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_BADMUON_SYS__1down")
# Iso
muonEffIsoHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_STAT__1up")
muonEffIsoLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_STAT__1down")
muonEffIsoHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_SYS__1up")
muonEffIsoLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_SYS__1down")
# Reco
muonEffRecoLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT__1down")
muonEffRecoHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT__1up")
muonEffRecoLowWeights_stat_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down")
muonEffRecoHighWeights_stat_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up")
muonEffRecoLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS__1down")
muonEffRecoHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS__1up")
muonEffRecoLowWeights_sys_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down")
muonEffRecoHighWeights_sys_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up")
# TTVA
muonEffTTVALowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_STAT__1down")
muonEffTTVAHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_STAT__1up")
muonEffTTVALowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_SYS__1down")
muonEffTTVAHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_SYS__1up")
# Trig
muonEffTrigLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigStatUncertainty__1down")
muonEffTrigHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigStatUncertainty__1up")
muonEffTrigLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigSystUncertainty__1down")
muonEffTrigHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigSystUncertainty__1up")

# electron related uncertainties acting on weights
# Charge id
#electronChargeIDLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_CHARGEID_STAT__1down")
#electronChargeIDHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_CHARGEID_STAT__1up")
# Charge id sel
#electronEffChargeIDSelLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down")
#electronEffChargeIDSelHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# ID
electronEffIDLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffIDHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# Iso
electronEffIsoLowWeights = replaceWeight(weights,"leptonWeight", "leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffIsoHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# Reco
electronEffRecoLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffRecoHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# Trigger
electronEffTrigEffLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffTrigEffHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffTrigLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffTrigHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up")

# JVT weight systematics
jvtEffLowWeights = replaceWeight(weights, "jvtWeight", "jvtWeight_JET_JvtEfficiency__1down")
jvtEffHighWeights = replaceWeight(weights, "jvtWeight", "jvtWeight_JET_JvtEfficiency__1up")

# pileup weight systematics
pileupLowWeights = replaceWeight(weights, "pileupWeight", "pileupWeightDown")
pileupHighWeights = replaceWeight(weights, "pileupWeight", "pileupWeightUp")

# b-tag weight systematics
bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")

eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst = {}
eTagFromCSyst = {}
bsTagSFSyst = {}

for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    bsTagSFSyst[region] = []

    # adding dummy 30% syst as an overallSys when debug
   # if debug: basicChanSyst[region].append(Systematic("dummy", configMgr.weights, 1.30, 0.70, "user", "userOverallSys"))

    # Trigger systematic uncertainty
    #basicChanSyst[region].append(Systematic("singleleptrigger", configMgr.weights, 1.02, 0.98, "user", "userOverallSys"))

    #Example systematic uncertainty
    if "JER" in SystList: 
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_1", "_NoSys", "_JET_JER_EffectiveNP_1__1up", "_JET_JER_EffectiveNP_1__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_2", "_NoSys", "_JET_JER_EffectiveNP_2__1up", "_JET_JER_EffectiveNP_2__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_3", "_NoSys", "_JET_JER_EffectiveNP_3__1up", "_JET_JER_EffectiveNP_3__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_4", "_NoSys", "_JET_JER_EffectiveNP_4__1up", "_JET_JER_EffectiveNP_4__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_5", "_NoSys", "_JET_JER_EffectiveNP_5__1up", "_JET_JER_EffectiveNP_5__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_6", "_NoSys", "_JET_JER_EffectiveNP_6__1up", "_JET_JER_EffectiveNP_6__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_7", "_NoSys", "_JET_JER_EffectiveNP_7__1up", "_JET_JER_EffectiveNP_7__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_8", "_NoSys", "_JET_JER_EffectiveNP_8__1up", "_JET_JER_EffectiveNP_8__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_9", "_NoSys", "_JET_JER_EffectiveNP_9__1up", "_JET_JER_EffectiveNP_9__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_10", "_NoSys", "_JET_JER_EffectiveNP_10__1up", "_JET_JER_EffectiveNP_10__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_11", "_NoSys", "_JET_JER_EffectiveNP_11__1up", "_JET_JER_EffectiveNP_11__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_12restTerm", "_NoSys", "_JET_JER_EffectiveNP_12restTerm__1up", "_JET_JER_EffectiveNP_12restTerm__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JET_LargeR_JER", "_NoSys", "_JET_LargeR_JER", "_NoSys", "tree", "overallNormHistoSys"))
	# The syst below will be replaced later with AFII for FastSim signals
        basicChanSyst[region].append(Systematic("JER_DataVsMC_MC16", "_NoSys", "_JET_JER_DataVsMC_MC16__1up", "_JET_JER_DataVsMC_MC16__1down", "tree", "overallNormHistoSys"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1", "_NoSys", "_JET_GroupedNP_1__1up", "_JET_GroupedNP_1__1down", "tree", "overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2", "_NoSys", "_JET_GroupedNP_2__1up", "_JET_GroupedNP_2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3", "_NoSys", "_JET_GroupedNP_3__1up", "_JET_GroupedNP_3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET", "_NoSys", "_JET_EtaIntercalibration_NonClosure__1up", "_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
            basicChanSyst[region].append(Systematic("JES_BJES_Response", "_NoSys", "_JET_BJES_Response__1up", "_JET_BJES_Response__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Baseline", "_NoSys", "_JET_CombMass_Baseline__1up", "_JET_CombMass_Baseline__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Modelling", "_NoSys", "_JET_CombMass_Modelling__1up", "_JET_CombMass_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_TotalStat", "_NoSys", "_JET_CombMass_TotalStat__1up", "_JET_CombMass_TotalStat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Tracking1", "_NoSys", "_JET_CombMass_Tracking1__1up", "_JET_CombMass_Tracking1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Tracking2", "_NoSys", "_JET_CombMass_Tracking2__1up", "_JET_CombMass_Tracking2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Tracking3", "_NoSys", "_JET_CombMass_Tracking3__1up", "_JET_CombMass_Tracking3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Detector1", "_NoSys", "_JET_EffectiveNP_Detector1__1up", "_JET_EffectiveNP_Detector1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Detector2", "_NoSys", "_JET_EffectiveNP_Detector2__1up", "_JET_EffectiveNP_Detector2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Mixed1", "_NoSys", "_JET_EffectiveNP_Mixed1__1up", "_JET_EffectiveNP_Mixed1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Mixed2", "_NoSys", "_JET_EffectiveNP_Mixed2__1up", "_JET_EffectiveNP_Mixed2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Mixed3", "_NoSys", "_JET_EffectiveNP_Mixed3__1up", "_JET_EffectiveNP_Mixed3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling1", "_NoSys", "_JET_EffectiveNP_Modelling1__1up", "_JET_EffectiveNP_Modelling1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling2", "_NoSys", "_JET_EffectiveNP_Modelling2__1up", "_JET_EffectiveNP_Modelling2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling3", "_NoSys", "_JET_EffectiveNP_Modelling3__1up", "_JET_EffectiveNP_Modelling3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling4", "_NoSys", "_JET_EffectiveNP_Modelling4__1up", "_JET_EffectiveNP_Modelling4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Detector1", "_NoSys", "_JET_EffectiveNP_R10_Detector1__1up", "_JET_EffectiveNP_R10_Detector1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Detector2", "_NoSys", "_JET_EffectiveNP_R10_Detector2__1up", "_JET_EffectiveNP_R10_Detector2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed1", "_NoSys", "_JET_EffectiveNP_R10_Mixed1__1up", "_JET_EffectiveNP_R10_Mixed1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed2", "_NoSys", "_JET_EffectiveNP_R10_Mixed2__1up", "_JET_EffectiveNP_R10_Mixed2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed3", "_NoSys", "_JET_EffectiveNP_R10_Mixed3__1up", "_JET_EffectiveNP_R10_Mixed3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed4", "_NoSys", "_JET_EffectiveNP_R10_Mixed4__1up", "_JET_EffectiveNP_R10_Mixed4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling1", "_NoSys", "_JET_EffectiveNP_R10_Modelling1__1up", "_JET_EffectiveNP_R10_Modelling1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling2", "_NoSys", "_JET_EffectiveNP_R10_Modelling2__1up", "_JET_EffectiveNP_R10_Modelling2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling3", "_NoSys", "_JET_EffectiveNP_R10_Modelling3__1up", "_JET_EffectiveNP_R10_Modelling3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling4", "_NoSys", "_JET_EffectiveNP_R10_Modelling4__1up", "_JET_EffectiveNP_R10_Modelling4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical1", "_NoSys", "_JET_EffectiveNP_R10_Statistical1__1up", "_JET_EffectiveNP_R10_Statistical1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical2", "_NoSys", "_JET_EffectiveNP_R10_Statistical2__1up", "_JET_EffectiveNP_R10_Statistical2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical3", "_NoSys", "_JET_EffectiveNP_R10_Statistical3__1up", "_JET_EffectiveNP_R10_Statistical3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical4", "_NoSys", "_JET_EffectiveNP_R10_Statistical4__1up", "_JET_EffectiveNP_R10_Statistical4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical5", "_NoSys", "_JET_EffectiveNP_R10_Statistical5__1up", "_JET_EffectiveNP_R10_Statistical5__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical6", "_NoSys", "_JET_EffectiveNP_R10_Statistical6__1up", "_JET_EffectiveNP_R10_Statistical6__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical1", "_NoSys", "_JET_EffectiveNP_Statistical1__1up", "_JET_EffectiveNP_Statistical1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical2", "_NoSys", "_JET_EffectiveNP_Statistical2__1up", "_JET_EffectiveNP_Statistical2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical3", "_NoSys", "_JET_EffectiveNP_Statistical3__1up", "_JET_EffectiveNP_Statistical3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical4", "_NoSys", "_JET_EffectiveNP_Statistical4__1up", "_JET_EffectiveNP_Statistical4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical5", "_NoSys", "_JET_EffectiveNP_Statistical5__1up", "_JET_EffectiveNP_Statistical5__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical6", "_NoSys", "_JET_EffectiveNP_Statistical6__1up", "_JET_EffectiveNP_Statistical6__1down", "tree", "overallNormHistoSys")) 
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling", "_NoSys", "_JET_EtaIntercalibration_Modelling__1up", "_JET_EtaIntercalibration_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_2018data", "_NoSys", "_JET_EtaIntercalibration_NonClosure_2018data__1up", "_JET_EtaIntercalibration_NonClosure_2018data__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_highE", "_NoSys", "_JET_EtaIntercalibration_NonClosure_highE__1up", "_JET_EtaIntercalibration_NonClosure_highE__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_negEta", "_NoSys", "_JET_EtaIntercalibration_NonClosure_negEta__1up", "_JET_EtaIntercalibration_NonClosure_negEta__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_posEta", "_NoSys", "_JET_EtaIntercalibration_NonClosure_posEta__1up", "_JET_EtaIntercalibration_NonClosure_posEta__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_R10_TotalStat", "_NoSys", "_JET_EtaIntercalibration_R10_TotalStat__1up", "_JET_EtaIntercalibration_R10_TotalStat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat", "_NoSys", "_JET_EtaIntercalibration_TotalStat__1up", "_JET_EtaIntercalibration_TotalStat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Flavor_Composition", "_NoSys", "_JET_Flavor_Composition__1up", "_JET_Flavor_Composition__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Flavor_Response", "_NoSys", "_JET_Flavor_Response__1up", "_JET_Flavor_Response__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_LargeR_TopologyUncertainty_top", "_NoSys", "_JET_LargeR_TopologyUncertainty_top__1up", "_JET_LargeR_TopologyUncertainty_top__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_LargeR_TopologyUncertainty_V", "_NoSys", "_JET_LargeR_TopologyUncertainty_V__1up", "_JET_LargeR_TopologyUncertainty_V__1down", "tree", "overallNormHistoSys"))            
            basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu", "_NoSys", "_JET_Pileup_OffsetMu__1up", "_JET_Pileup_OffsetMu__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV", "_NoSys", "_JET_Pileup_OffsetNPV__1up", "_JET_Pileup_OffsetNPV__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm", "_NoSys", "_JET_Pileup_PtTerm__1up", "_JET_Pileup_PtTerm__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology", "_NoSys", "_JET_Pileup_RhoTopology__1up", "_JET_Pileup_RhoTopology__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_PunchThrough_MC16", "_NoSys", "_JET_PunchThrough_MC16__1up", "_JET_PunchThrough_MC16__1down", "tree", "overallNormHistoSys")) ## will be replaced with AFII for FastSim signals
            basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt", "_NoSys", "_JET_SingleParticle_HighPt__1up", "_JET_SingleParticle_HighPt__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_MassRes_Hbb_comb", "_NoSys", "_JET_MassRes_Hbb_comb__1up", "_JET_MassRes_Hbb_comb__1down", "tree", "overallNormHistoSysOneSideSym"))
            basicChanSyst[region].append(Systematic("JES_MassRes_Top_comb", "_NoSys", "_JET_MassRes_Top_comb__1up", "_JET_MassRes_Top_comb__1down", "tree", "overallNormHistoSysOneSideSym"))
            basicChanSyst[region].append(Systematic("JES_MassRes_WZ_comb", "_NoSys", "_JET_MassRes_WZ_comb__1up", "_JET_MassRes_WZ_comb__1down", "tree", "overallNormHistoSysOneSideSym"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Dijet_Modelling", "_NoSys", "_JET_JetTagSF_Dijet_Modelling__1up", "_JET_JetTagSF_Dijet_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Gammajet_Modelling", "_NoSys", "_JET_JetTagSF_Gammajet_Modelling__1up", "_JET_JetTagSF_Gammajet_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Hadronisation", "_NoSys", "_JET_JetTagSF_Hadronisation__1up", "_JET_JetTagSF_Hadronisation__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_MatrixElement__1up", "_NoSys", "_JET_JetTagSF_MatrixElement__1up", "_JET_JetTagSF_MatrixElement__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Radiation", "_NoSys", "_JET_JetTagSF_Radiation__1up", "_JET_JetTagSF_Radiation__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_BGSF_Dijet_Stat", "_NoSys", "_JET_WTag_SigEff50_BGSF_Dijet_Stat__1up", "_JET_WTag_SigEff50_BGSF_Dijet_Stat__1up", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_BGSF_Gammajet_Stat", "_NoSys", "_JET_WTag_SigEff50_BGSF_Gammajet_Stat__1up", "_JET_WTag_SigEff50_BGSF_Gammajet_Stat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_BGSF_Propagated_AllOthers", "_NoSys", "_JET_WTag_SigEff50_BGSF_Propagated_AllOthers__1up", "_JET_WTag_SigEff50_BGSF_Propagated_AllOthers__1up", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_BinVariation", "_NoSys", "_JET_WTag_SigEff50_SigSF_BinVariation__1up", "_JET_WTag_SigEff50_SigSF_BinVariation__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_ExtrapolationPt", "_NoSys", "_JET_WTag_SigEff50_SigSF_ExtrapolationPt__1up", "_JET_WTag_SigEff50_SigSF_ExtrapolationPt__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_ExtrapolationZ", "_NoSys", "_JET_WTag_SigEff50_SigSF_ExtrapolationZ__1up", "_JET_WTag_SigEff50_SigSF_ExtrapolationZ__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_Propagated_AllOthers", "_NoSys", "_JET_WTag_SigEff50_SigSF_Propagated_AllOthers__1up", "_JET_WTag_SigEff50_SigSF_Propagated_AllOthers__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_Statistics", "_NoSys", "_JET_WTag_SigEff50_SigSF_Statistics__1up", "_JET_WTag_SigEff50_SigSF_Statistics__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_TagEffUnc_GlobalBackground", "_NoSys", "_JET_WTag_SigEff50_TagEffUnc_GlobalBackground__1up", "_JET_WTag_SigEff50_TagEffUnc_GlobalBackground__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_TagEffUnc_GlobalOther__1up", "_NoSys", "_JET_WTag_SigEff50_TagEffUnc_GlobalOther__1up", "_JET_WTag_SigEff50_TagEffUnc_GlobalOther__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_TagEffUnc_GlobalSignal", "_NoSys", "_JET_WTag_SigEff50_TagEffUnc_GlobalSignal__1up", "_JET_WTag_SigEff50_TagEffUnc_GlobalSignal__1down", "tree", "overallNormHistoSys"))
	    


	        
    if "MET" in SystList:
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara", "_NoSys", "_MET_SoftTrk_ResoPara", "_NoSys", "tree", "overallNormHistoSysOneSideSym"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp", "_NoSys", "_MET_SoftTrk_ResoPerp", "_NoSys", "tree", "overallNormHistoSysOneSideSym"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_Scale", "_NoSys", "_MET_SoftTrk_Scale__1up", "_MET_SoftTrk_Scale__1down", "tree", "overallNormHistoSys"))

    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL", "_NoSys", "_EG_RESOLUTION_ALL__1up", "_EG_RESOLUTION_ALL__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_SCALE_AF2", "_NoSys", "_EG_SCALE_AF2__1up", "_EG_SCALE_AF2__1down", "tree", "overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL", "_NoSys", "_EG_SCALE_ALL__1up", "_EG_SCALE_ALL__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID", "_NoSys", "_MUON_ID__1up", "_MUON_ID__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS", "_NoSys", "_MUON_MS__1up", "_MUON_MS__1down", "tree", "overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SAGITTA_RESBIAS","_NoSys", "_MUON_SAGITTA_RESBIAS__1up", "_MUON_SAGITTA_RESBIAS__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_SAGITTA_RHO", "_NoSys", "_MUON_SAGITTA_RHO__1up", "_MUON_SAGITTA_RHO__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_SCALE", "_NoSys", "_MUON_SCALE__1up", "_MUON_SCALE__1down", "tree", "overallNormHistoSys"))

    if "LepEff" in SystList:
        basicChanSyst[region].append(Systematic("MUON_EFF_ISO_STAT",configMgr.weights, muonEffIsoHighWeights_stat, muonEffIsoLowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_ISO_SYS", configMgr.weights, muonEffIsoHighWeights_sys, muonEffIsoLowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_STAT", configMgr.weights, muonEffRecoHighWeights_stat, muonEffRecoLowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_STAT_LOWPT", configMgr.weights, muonEffRecoHighWeights_stat_lowpt, muonEffRecoLowWeights_stat_lowpt, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_SYS", configMgr.weights, muonEffRecoHighWeights_sys, muonEffRecoLowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_SYS_LOWPT", configMgr.weights, muonEffRecoHighWeights_sys_lowpt, muonEffRecoLowWeights_sys_lowpt, "weight", "overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("MUON_TTVA_STAT", configMgr.weights, muonEffTTVAHighWeights_stat, muonEffTTVALowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_SYS", configMgr.weights, muonEffTTVAHighWeights_sys, muonEffTTVALowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_TrigStatUncertainty", configMgr.weights, muonEffTrigHighWeights_stat, muonEffTrigLowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_TrigSystUncertainty", configMgr.weights, muonEffTrigHighWeights_sys, muonEffTrigLowWeights_sys, "weight", "overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_BadMuon_sys", configMgr.weights, muonEffBadMuonHighWeights_sys, muonEffBadMuonLowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_EFF_ID", configMgr.weights, electronEffIDHighWeights, electronEffIDLowWeights, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_EFF_Iso", configMgr.weights, electronEffIsoHighWeights, electronEffIsoLowWeights, "weight", "overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_EFF_Reco", configMgr.weights, electronEffRecoHighWeights, electronEffRecoLowWeights, "weight", "overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_EFF_TriggerEff", configMgr.weights, electronEffTrigEffHighWeights, electronEffTrigEffLowWeights, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_EFF_Trigger",configMgr.weights, electronEffTrigHighWeights, electronEffTrigLowWeights, "weight", "overallNormHistoSys")) 
    if "JVT" in SystList:
        basicChanSyst[region].append(Systematic("JvtEfficiency", configMgr.weights, jvtEffHighWeights, jvtEffLowWeights, "weight", "overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("Pileup", configMgr.weights, pileupHighWeights, pileupLowWeights, "weight", "overallNormHistoSys"))

    if "BTag" in SystList:
        bTagSyst[region] = Systematic("btag_BT", configMgr.weights, bTagHighWeights, bTagLowWeights, "weight", "overallNormHistoSys")
        cTagSyst[region] = Systematic("btag_CT", configMgr.weights, cTagHighWeights, cTagLowWeights, "weight", "overallNormHistoSys")
        mTagSyst[region] = Systematic("btag_LightT", configMgr.weights, mTagHighWeights, mTagLowWeights, "weight", "overallNormHistoSys")
        eTagSyst[region] = Systematic("btag_Extra", configMgr.weights, eTagHighWeights, eTagLowWeights, "weight", "overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm", configMgr.weights, eTagFromCHighWeights, eTagFromCLowWeights, "weight", "overallNormHistoSys")

    if "BSTag" in SystList:
        basicChanSyst[region].append(Systematic("BSTag_B_0", "_NoSys", "_bTag_B_0__1down","_bTag_B_0__1up", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("BSTag_Light_0", "_NoSys", "_bTag_Light_0__1down","_bTag_Light_0__1up", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("BSTag_Light_1", "_NoSys", "_bTag_Light_1__1down","_bTag_Light_1__1up", "tree", "overallNormHistoSys"))

        
# signal uncertainties
xsecSig = Systematic("SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys")
    
# Generator Systematics
generatorSyst = []
    
if "ttbar" in SystList and "ttbar" in mysamples:    
    # ttbar uncertainties
    import theoryUncertainties_EWK2ndWave_ttbar
    theoryUncertainties_EWK2ndWave_ttbar.TheorUnc(generatorSyst)

if "singletop" in SystList and "singletop" in mysamples:
    # single top uncertainties
    import theoryUncertainties_EWK2ndWave_singletop
    theoryUncertainties_EWK2ndWave_singletop.TheorUnc(generatorSyst) 
    
if "diboson" in SystList and "diboson" in mysamples:
    # diboson uncertainties
    import theoryUncertainties_EWK2ndWave_diboson2l
    theoryUncertainties_EWK2ndWave_diboson2l.TheorUnc(generatorSyst)
    import theoryUncertainties_EWK2ndWave_diboson1l
    theoryUncertainties_EWK2ndWave_diboson1l.TheorUnc(generatorSyst)

if "multiboson" in SystList and "multiboson" in mysamples:
    # multiboson uncertainties
    import theoryUncertainties_EWK2ndWave_multiboson
    theoryUncertainties_EWK2ndWave_multiboson.TheorUnc(generatorSyst)        

if "wjets" in SystList and "wjets" in mysamples:
    # wjets uncertainties
    import theoryUncertainties_EWK2ndWave_wjets
    theoryUncertainties_EWK2ndWave_wjets.TheorUnc(generatorSyst) 

if "zjets" in SystList and "zjets" in mysamples:
    # zjets uncertainties
    import theoryUncertainties_EWK2ndWave_zjets
    theoryUncertainties_EWK2ndWave_zjets.TheorUnc(generatorSyst)
    
if "ttv" in SystList and "ttv" in mysamples:
    #ttv uncertainties
    import theoryUncertainties_EWK2ndWave_ttv
    theoryUncertainties_EWK2ndWave_ttv.TheorUnc(generatorSyst)

if "tth" in SystList and "tth" in mysamples:
    #tth uncertainties
    import theoryUncertainties_EWK2ndWave_tth
    theoryUncertainties_EWK2ndWave_tth.TheorUnc(generatorSyst)

if "vh" in SystList and "vh" in mysamples:
    #vh uncertainties
    import theoryUncertainties_EWK2ndWave_vh
    theoryUncertainties_EWK2ndWave_vh.TheorUnc(generatorSyst)

# ********************************************************************* #
#                              Samples
# ********************************************************************* #

configMgr.nomName = "_NoSys"

# Bkg samples
WSampleName = "wjets"
WSample = Sample(WSampleName, kAzure-4)
WSample.setNormFactor("mu_WDB1L", 1., 0., 5.)
WSample.setStatConfig(useStat)

TtbarSampleName = "ttbar"
TtbarSample = Sample(TtbarSampleName, kGreen-9)
TtbarSample.setNormFactor("mu_Top", 1., 0., 5.)
TtbarSample.setStatConfig(useStat)

Diboson1LSampleName = "diboson1l"
Diboson1LSample = Sample(Diboson1LSampleName, kOrange-8)
Diboson1LSample.setNormFactor("mu_WDB1L", 1., 0., 5.)
Diboson1LSample.setStatConfig(useStat)
Diboson1LSample.setPrefixTreeName("diboson")
Diboson1LSample.addSampleSpecificWeight("(DatasetNumber==364255 || DatasetNumber==363359 || DatasetNumber==363360 || DatasetNumber==363489 || DatasetNumber==364304 || DatasetNumber==364305)")

Diboson2LSampleName = "diboson2l"
Diboson2LSample = Sample(Diboson2LSampleName, kOrange-8)
Diboson2LSample.setNormFactor("mu_DB2L", 1., 0., 5.)
Diboson2LSample.setStatConfig(useStat)
Diboson2LSample.setPrefixTreeName("diboson")
Diboson2LSample.addSampleSpecificWeight("(DatasetNumber==364250 || DatasetNumber==364253 || DatasetNumber==364254 || DatasetNumber==364286 || DatasetNumber==364288 || DatasetNumber==364289 || DatasetNumber==364290 || DatasetNumber==363356 || DatasetNumber==363358 || DatasetNumber==364283 || DatasetNumber==364284 || DatasetNumber==364285 || DatasetNumber==364287 || DatasetNumber==364302 || DatasetNumber==345708 || DatasetNumber==345709 || DatasetNumber==345716 || DatasetNumber==345720 || DatasetNumber==345725)")

Diboson0LSampleName = "diboson0l"
Diboson0LSample = Sample(Diboson0LSampleName, kOrange-8)
Diboson0LSample.setStatConfig(useStat)
Diboson0LSample.setNormByTheory()
Diboson0LSample.setPrefixTreeName("diboson")
Diboson0LSample.addSampleSpecificWeight("(DatasetNumber==363494 || DatasetNumber==363355 || DatasetNumber==363357 || DatasetNumber==364303)")
if OneNFTopST:
	SingleTopSampleName = "singletop"
	SingleTopSample = Sample(SingleTopSampleName, kGreen-5)
	SingleTopSample.setNormFactor("mu_Top", 1., 0., 5.)
	SingleTopSample.setStatConfig(useStat)
else:	
	SingleTopSampleName = "singletop"
	SingleTopSample = Sample(SingleTopSampleName, kGreen-5)
	SingleTopSample.setStatConfig(useStat)
	SingleTopSample.setNormByTheory()




ZSampleName = "zjets"
ZSample = Sample(ZSampleName, kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()

TtbarVSampleName = "ttv"
TtbarVSample = Sample(TtbarVSampleName, kGreen-8)
TtbarVSample.setStatConfig(useStat)
TtbarVSample.setNormByTheory()

TtbarHSampleName = "tth"
TtbarHSample = Sample(TtbarHSampleName, kGreen-8)
TtbarHSample.setStatConfig(useStat)
TtbarHSample.setNormByTheory()

TribosonSampleName = "multiboson"
TribosonSample = Sample(TribosonSampleName, kOrange-8)
TribosonSample.setStatConfig(useStat)
TribosonSample.setNormByTheory()

VHSampleName = "vh"
VHSample = Sample(VHSampleName, kGreen-8)
VHSample.setStatConfig(useStat)
VHSample.setNormByTheory()

# data sample for later
DataSampleName = "data"
DataSample = Sample(DataSampleName, kBlack)
DataSample.addInputs(dataFiles)
DataSample.setData()

if doOnlySignal:
    SignalSampleName = sigSamples[0].replace("p5", "").replace("p0", "")
    SignalSample = Sample(SignalSampleName, kPink)
    SignalSample.setPrefixTreeName(sigSamples[0])
    SignalSample.setStatConfig(useStat)
    SignalSample.setNormByTheory()
    SignalSample.setNormFactor("mu_SIG", 1., 0., 5.)
    SignalSample.addInputs(sigFiles_em[sigSamples[0]])

# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")

# creating now list of samples
samplelist = []
if "ttbar" in mysamples: samplelist.append(TtbarSample)
if "singletop" in mysamples: samplelist.append(SingleTopSample)
if "wjets" in mysamples: samplelist.append(WSample)
if "diboson" in mysamples: 
    samplelist.append(Diboson1LSample)
    samplelist.append(Diboson2LSample)
    samplelist.append(Diboson0LSample)
if "multiboson" in mysamples: samplelist.append(TribosonSample)
if "vh" in mysamples: samplelist.append(VHSample)
if "zjets" in mysamples: samplelist.append(ZSample)
if "tth" in mysamples: samplelist.append(TtbarHSample)
if "ttv" in mysamples: samplelist.append(TtbarVSample)

if "data" in mysamples: samplelist.append(DataSample)

if doOnlySignal: samplelist = [SignalSample, DataSample]

bkgOnly.addSamples(samplelist)

if useStat:
    bkgOnly.statErrThreshold = 0.001
else:
    bkgOnly.statErrThreshold = None

# Add measurement
meas = bkgOnly.addMeasurement("BasicMeasurement", lumi = 1.0, lumiErr = 0.017)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi", "const", 1.0)

# Classification of channels
bTagChans = {}
nonBTagChans = {}
ZTagChans = {}
WTagChans = {}
elChans = {}
muChans = {}
elmuChans = {}

for region in CRregions:
    bTagChans[region] = []
    nonBTagChans[region] = []
    ZTagChans[region] = []
    WTagChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################

if "resolved" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts", ["WDB1LCRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
    bkgOnly.addBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts", ["TCRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
    bkgOnly.addBkgConstrainChannels(tmp)

if "boosted" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("meffInc30", ["WDB1LCRboostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
    bkgOnly.addBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("meffInc30", ["TCRboostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
    bkgOnly.addBkgConstrainChannels(tmp)

if "general" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts", ["DB2LCREM"], 1, 0.5, 1.5), [bTagChans["general"], elmuChans["general"]])
    bkgOnly.addBkgConstrainChannels(tmp)

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"] = False
for region in CRregions:
    ValidRegList["OneLep"] += ValidRegList[region]

validation = None
# run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"] or VRplots:
    validation = configMgr.addFitConfigClone(bkgOnly, "Validation")
    for c in validation.channels:
        for region in CRregions:
            appendIfMatchName(c, bTagChans[region])
            appendIfMatchName(c, nonBTagChans[region])
            appendIfMatchName(c, ZTagChans[region])
            appendIfMatchName(c, WTagChans[region])
            appendIfMatchName(c, elChans[region])
            appendIfMatchName(c, muChans[region])
            appendIfMatchName(c, elmuChans[region])

    if doTableInputs:
        if "resolved" in CRregions:
            appendTo(validation.addValidationChannel("cuts", ["WDB1LVRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
            appendTo(validation.addValidationChannel("cuts", ["TVRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
            appendTo(validation.addValidationChannel("cuts", ["SRLMresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
            appendTo(validation.addValidationChannel("cuts", ["SRHMresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])

        if "boosted" in CRregions:
            appendTo(validation.addValidationChannel("meffInc30", ["WDB1LVR1boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            appendTo(validation.addValidationChannel("meffInc30", ["WDB1LVR2boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            appendTo(validation.addValidationChannel("meffInc30", ["TVR1boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            appendTo(validation.addValidationChannel("meffInc30", ["TVR2boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            if not doBackupCacheFile:
                if myAna == "C1N2_WZ":
                    appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWZdiscEM"], 1, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWZdiscEM"], 1, 850., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWZdiscEM"], 1, 850., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                if myAna == "C1C1_WW":
                    appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWWdiscEM"], 1, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWWdiscEM"], 1, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWWdiscEM"], 1, 850., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            else:
                appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
		
                appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWZdiscEM"], 1, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWZdiscEM"], 1, 850., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWZdiscEM"], 1, 850., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWWdiscEM"], 1, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWWdiscEM"], 1, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWWdiscEM"], 1, 850., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])

        if doBlindSRinBGfit:
            if "resolved" in CRregions:
                validation.getChannel("cuts", ["SRLMresolvedEM"]).blind = True
                validation.getChannel("cuts", ["SRHMresolvedEM"]).blind = True
            if "boosted" in CRregions:
                if not doBackupCacheFile:
                    if myAna == "C1N2_WZ":
                        validation.getChannel("meffInc30", ["SRLMboostedWZEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRMMboostedWZEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRHMboostedWZEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRLMboostedWZdiscEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRMMboostedWZdiscEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRHMboostedWZdiscEM"]).blind = True
                    if myAna == "C1C1_WW":
                        validation.getChannel("meffInc30", ["SRLMboostedWWEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRMMboostedWWEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRHMboostedWWEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRLMboostedWWdiscEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRMMboostedWWdiscEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRHMboostedWWdiscEM"]).blind = True
                else:
                    validation.getChannel("meffInc30", ["SRLMboostedWZEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRMMboostedWZEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRHMboostedWZEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRLMboostedWWEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRMMboostedWWEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRHMboostedWWEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRLMboostedWZdiscEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRMMboostedWZdiscEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRHMboostedWZdiscEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRLMboostedWWdiscEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRMMboostedWWdiscEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRHMboostedWWdiscEM"]).blind = True


        if "general" in CRregions:
            appendTo(validation.addValidationChannel("cuts", ["DB2LVREM"], 1, 0.5, 1.5), [bTagChans["general"],  elmuChans["general"]])

        # All validation regions should use over-flow bins
        for v in validation.channels:
            if not "cuts" in v.name: v.useOverflowBin = True
	    
    if VRplots:
        if myAna == "C1N2_WZ":
                appendTo(validation.addValidationChannel("mt",["SRHMboostedN1mtWZEM"],50,50.,300.),[nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                

                if doBlindSRinBGfit:
                    validation.getChannel("mt",["SRHMboostedN1mtWZEM"]).blind = True
                   
   

# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #

if myFitType == FitType.Exclusion:

    if myAna == "C1N2_WZ": signal_uncertainties = SignalTheoryUncertainties ("./systWZ.root")
    if myAna == "C1C1_WW": signal_uncertainties = SignalTheoryUncertainties ("./systWW.root")

    SR_channels = {}
    SRs = []
    if "resolved" in CRregions:
        SRs += ["SRLMresolvedEM", "SRHMresolvedEM"]
    if "boosted" in CRregions:
        if myAna == "C1N2_WZ":
            SRs += ["SRLMboostedWZEM", "SRMMboostedWZEM", "SRHMboostedWZEM"]
        if myAna == "C1C1_WW":
            SRs += ["SRLMboostedWWEM", "SRMMboostedWWEM", "SRHMboostedWWEM"]

    for sig in sigSamples:
        SR_channels[sig] = []
        sig2 = sig.replace("p5", "").replace("p0", "")
        myTopLvl = configMgr.addFitConfigClone(bkgOnly, "Sig_%s"%sig2)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c, bTagChans[region])
                appendIfMatchName(c, nonBTagChans[region])
                appendIfMatchName(c, ZTagChans[region])
                appendIfMatchName(c, WTagChans[region])
                appendIfMatchName(c, elChans[region])
                appendIfMatchName(c, muChans[region])
                appendIfMatchName(c, elmuChans[region])
            if not "cuts" in c.name: c.useOverflowBin = True

        sigSample = Sample(sig2, kPink)
        sigSample.setPrefixTreeName(sig)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG", 1., 0., 5.)

        # signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        if not debug: sigSample.addSystematic(xsecSig)

        if not doHistoBuilding:
            myTopLvl.addSamples(sigSample)
            myTopLvl.setSignalSample(sigSample)

        # Create channels for each SR
        for sr in SRs:
            if "resolved" in sr:
                ch = myTopLvl.addChannel("cuts", [sr], 1, 0.5, 1.5)
                bTagChans["resolved"].append(ch)
                ERRregion = "SR_resol"
            if "boosted" in sr:
                ch = myTopLvl.addChannel("meffInc30", [sr], 2, 600., 1100.)
                ch.useOverflowBin = True
                nonBTagChans["boosted"].append(ch)
                if myAna == "C1N2_WZ":
                    ZTagChans["boosted"].append(ch)
                    ERRregion = "WZ_SR_boost"
                if myAna == "C1C1_WW":
                    WTagChans["boosted"].append(ch)
                    ERRregion = "WW_SR_boost"

            for region in CRregions:
                if region in sr:
                    if "El" in sr: elChans[region].append(ch)
                    elif "Mu" in sr: muChans[region].append(ch)
                    elif "EM" in sr: elmuChans[region].append(ch)
                    else: raise RuntimeError("Unexpected signal region %s"%sr)

            # setup the SR channel
            myTopLvl.setSignalChannels(ch)

            ############## Signal theory uncertainties ###############
            if not doHistoBuilding and ("C1N2_WZ" in sig or "C1C1_WW" in sig) and not debug:
                print (sig)
                errsigList = signal_uncertainties.getUncertainties(ERRregion, sig)
                errsig = []
                errsig += errsigList
                print ("sig=", sig, " region=", ERRregion, " errsig_qcup=", errsig[0], " errsig_qcdw=", errsig[1], " errsig_scup=", errsig[2], " errsig_scdw=", errsig[3], " errsig_raup=", errsig[4], " errsig_radw=", errsig[5])
                if "resolved" in sr:
                    theoSigQc = Systematic("Qc", configMgr.weights, [(1.+errsig[0])], [(1.+errsig[1])], "user", "userHistoSys")
                    theoSigSc = Systematic("Sc", configMgr.weights, [(1.+errsig[2])], [(1.+errsig[3])], "user", "userHistoSys")
                    theoSigRad = Systematic("Rad", configMgr.weights, [(1.+errsig[4])], [(1.+errsig[5])], "user", "userHistoSys")
                if "boosted" in sr:
                    theoSigQc = Systematic("Qc", configMgr.weights, [(1.+errsig[0]), (1.+errsig[0])], [(1.+errsig[1]), (1.+errsig[1])], "user", "userHistoSys")
                    theoSigSc = Systematic("Sc", configMgr.weights, [(1.+errsig[2]), (1.+errsig[2])], [(1.+errsig[3]), (1.+errsig[3])], "user", "userHistoSys")
                    theoSigRad = Systematic("Rad", configMgr.weights, [(1.+errsig[4]), (1.+errsig[4])], [(1.+errsig[5]), (1.+errsig[5])], "user", "userHistoSys")
                ch.getSample(sig2).addSystematic(theoSigQc)
                ch.getSample(sig2).addSystematic(theoSigSc)
                ch.getSample(sig2).addSystematic(theoSigRad)
            ##########################################################

            SR_channels[sig].append(ch)

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #
normRegions = []
if "resolved" in CRregions:
    normRegions += [("WDB1LCRresolvedEM", "cuts"), ("TCRresolvedEM", "cuts")]
if "boosted" in CRregions:
    normRegions += [("WDB1LCRboostedEM", "meffInc30"), ("TCRboostedEM", "meffInc30")]
if "general" in CRregions:
    normRegions += [("DB2LCREM", "cuts")]

AllChannels = {}
AllChannels_all = []
elChans_all = []
muChans_all = []
elmuChans_all = []

for region in CRregions:
    AllChannels[region] = bTagChans[region] + nonBTagChans[region]
    AllChannels_all += AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]
    elmuChans_all += elmuChans[region]

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan

    for chan in AllChannels_all:
        #if tgtchan=="All" or tgtchan==chan.name:
	    #print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal and not debug:
	        #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))

if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region], bkgFiles_e, basicChanSyst[region])
        SetupChannels(muChans[region], bkgFiles_m, basicChanSyst[region])
        SetupChannels(elmuChans[region], bkgFiles_em, basicChanSyst[region])

# Final semi-hacks for signal samples in exclusion fits
if myFitType == FitType.Exclusion and not doHistoBuilding:
    for sig in sigSamples:
        sig2 = sig.replace("p5", "").replace("p0", "")
        myTopLvl = configMgr.getFitConfig("Sig_%s"%sig2)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig2)

            # replacement of JES_PunchThrough and JER_DataVsMC systematic for AF2 signal samples (not for FullSim signal samples)
            if not debug and sig in FastSimSig:
                print ("This is an AFII signal sample -> removing systematics JER_DataVsMC_MC16 and JES_PunchThrough_MC16")
                theSample.removeSystematic("JER_DataVsMC_MC16")
                theSample.removeSystematic("JES_PunchThrough_MC16")
                print ("and adding systematics JER_DataVsMC_AFII and JES_PunchThrough_AFII")
                theSample.addSystematic(Systematic("JER_DataVsMC_AFII", "_NoSys", "_JET_JER_DataVsMC_AFII__1up", "_JET_JER_DataVsMC_AFII__1down", "tree", "overallNormHistoSys"))
                theSample.addSystematic(Systematic("JES_PunchThrough_AFII", "_NoSys", "_JET_PunchThrough_AFII__1up", "_JET_PunchThrough_AFII__1down", "tree", "overallNormHistoSys"))
          #  if debug:
              #  theSample.removeSystematic("dummy")

            theSigFiles = []
            if chan in elChans_all: theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all: theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all: theSigFiles = sigFiles_em[sig]
            else: raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0: theSample.addInputs(theSigFiles)
            else: 
                print ("ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name, chan.name))
                chan.removeSample(theSample)

# Add b-tag, boson-tag systematic
for region in CRregions:
    for chan in bTagChans[region]:
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

    """ for chan in ZTagChans[region]:
        if "BSTag" in SystList and not doOnlySignal:
            # Add boson tagging sf syst

    for chan in WTagChans[region]:
        if "BSTag" in SystList and not doOnlySignal:
            # Add boson tagging sf syst """

    for chan in (bTagChans[region] + nonBTagChans[region]):
        if not doOnlySignal:
            if "ttbar" in mysamples:
                chan.getSample("ttbar").setNormRegions(normRegions)
            if OneNFTopST:
                        if "singletop" in mysamples:
                                 chan.getSample("singletop").setNormRegions(normRegions)
                                 if not debug:chan.getSample("singletop").addSystematic(Systematic("singletop_XS", configMgr.weights, addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*1.04) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*1.05))"), addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*0.96) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*0.95))"), "weight", "overallSys"))
            else:
                         if "singletop" in mysamples and not debug:
                                chan.getSample("singletop").addSystematic(Systematic("singletop_XS", configMgr.weights, addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*1.04) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*1.05))"), addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*0.96) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*0.95))"), "weight", "overallSys"))
            
            if "wjets" in mysamples:
                chan.getSample("wjets").setNormRegions(normRegions)
                chan.getSample("wjets").removeWeight("eventWeight")
                chan.getSample("wjets").addWeight("((fabs(eventWeight)<100.)*eventWeight + (eventWeight>100.) - (eventWeight<-100.))")
            
            if "diboson" in mysamples:
                chan.getSample("diboson1l").setNormRegions(normRegions)
                chan.getSample("diboson2l").setNormRegions(normRegions)
                if not debug: chan.getSample("diboson0l").addSystematic(Systematic("diboson_XS", configMgr.weights, 1.06, 0.94, "user", "userOverallSys"))

            if "multiboson" in mysamples and not debug:
                chan.getSample("multiboson").addSystematic(Systematic("multiboson_XS", configMgr.weights, 1.2, 0.8, "user", "userOverallSys"))

            if "tth" in mysamples and not debug:
                chan.getSample("tth").addSystematic(Systematic("tth_XS", configMgr.weights, 1.1, 0.9, "user", "userOverallSys"))

            if "ttv" in mysamples and not debug:
                chan.getSample("ttv").addSystematic(Systematic("ttv_XS", configMgr.weights, addWeight(weights, "(((DatasetNumber==410155)*1.13) + ((DatasetNumber!=410155)*1.12))"), addWeight(weights, "(((DatasetNumber==410155)*0.87) + ((DatasetNumber!=410155)*0.88))"), "weight", "overallSys"))

            if "zjets" in mysamples and not debug:
                chan.getSample("zjets").addSystematic(Systematic("zjets_XS", configMgr.weights, 1.05, 0.95, "user", "userOverallSys"))

            
    for chan in nonBTagChans[region]:
        chan.removeWeight("bTagWeight")
    
    for chan in ZTagChans[region]:
        chan.addWeight("fatjet1ZSF")

    for chan in WTagChans[region]:
        chan.addWeight("fatjet1WSF")
        

# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55, 0.45, 0.87, 0.89, "") # without signal
#leg = ROOT.TLegend(0.55, 0.35, 0.87, 0.89, "") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("", "Data (#sqrt{s}=13 TeV)", "lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("", "Standard Model", "lf")
entry.SetLineColor(kBlack)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("", "t#bar{t}", "lf")
entry.SetLineColor(kGreen-9)
entry.SetFillColor(kGreen-9)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "W+jets", "lf")
entry.SetLineColor(kAzure-4)
entry.SetFillColor(kAzure-4)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Diboson", "lf")
entry.SetLineColor(kOrange-8)
entry.SetFillColor(kOrange-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Single Top", "lf")
entry.SetLineColor(kGreen-5)
entry.SetFillColor(kGreen-5)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Z+jets", "lf")
entry.SetLineColor(kBlue+3)
entry.SetFillColor(kBlue+3)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "t#bar{t}V", "lf")
entry.SetLineColor(kGreen-8)
entry.SetFillColor(kGreen-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "t#bar{t}H", "lf")
entry.SetLineColor(kGreen-8)
entry.SetFillColor(kGreen-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Multiboson", "lf")
entry.SetLineColor(kOrange-8)
entry.SetFillColor(kOrange-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "VH", "lf")
entry.SetLineColor(kGreen-8)
entry.SetFillColor(kGreen-8)
entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation:
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:
    sig2 = sig.replace("p5", "").replace("p0", "")
    myTopLvl = configMgr.getFitConfig("Sig_%s"%sig2)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"

c.Close()
MeffBins = ['1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: 
        chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    chan.ATLASLabelX = 0.27 # AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan. ATLASLabelText = "Internal"
            chan.showLumi = True

    configMgr.fitConfigs.remove(bkgOnly)

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName)


