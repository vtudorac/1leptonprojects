## Access signal acceptance uncertainties
## as a function of  chargino and LSP mass

import ROOT
import math
import os
import json

class SignalTheoryUncertainties():

    knownRegions = ["SR_resol", "WZ_SR_boost", "WW_SR_boost"]

    def __init__(self, fileName):
        try:
            if fileName.endswith(".root"):
                self.root_mode = True
                self.infile = ROOT.TFile(fileName)
            elif fileName.endswith(".json"):
                self.root_mode = False
                with open(fileName) as f:
                    self.infile = json.load(f)
            else:
                print("Do not know what to do with this file ", fileName)
                return 0
        except:
            print("Something went wrong when loading the signal uncertainties file ", fileName)
            return 0
    def getUncertainties(self, regionName, massString = None):
        """
        Returns signal theory uncertainties from ROOT file.

            Args:
                regionName (str): Region to use.
                massString (str): String containing the masses to be parsed.
            Returns:
                list: List containing uncertainties (up and down) for given region.
        """ 

        if self.root_mode:
            if not massString:
                print ("Need to provide a mass string!")
                return 0
            if regionName not in self.knownRegions:
                print ("{} not known. Please choose among: {}".format(regionName, knownRegions))
                return 0
            
            tokens = massString.split("_")
            if "C1N2_WZ" in massString or "C1C1_WW" in massString:
                mChargino = float(tokens[2].replace("p0", ".0").replace("p5", ".5"))
                mLSP = float(tokens[3].replace("p0", ".0").replace("p5", ".5"))
            else:
                print ("unknown signal sample name!")

            uncertList = []
            variations = ["qcup", "qcdw", "scup", "scdw", "raup", "radw"]
            for var in variations:
                File = self.infile.Get("syst_{0}_{1}".format(regionName, var))
                uncertList.append(File.Interpolate(mChargino, mLSP))
            print ("Uncertainties list: ", uncertList)
            return uncertList
        else:
            return self.infile.get(regionName, {})

