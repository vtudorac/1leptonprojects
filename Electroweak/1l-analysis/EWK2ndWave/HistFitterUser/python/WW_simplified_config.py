########
# IMPORT
########
from configManager import configMgr
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
import collections
from collections import OrderedDict
import itertools
import sys

import os

import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from ROOT import gROOT
from ROOT import TMath


###############################
# Define HistFactory attributes
###############################
configMgr.analysisName = "C1C1_WW"
configMgr.histCacheFile = "data/%s.root"%configMgr.analysisName
configMgr.outputFileName = "results/%s.root"%configMgr.analysisName


################################
# Parameters for hypothesis test
################################
useToys = False
if useToys:
    configMgr.calculatorType = 0      # frequentist calculator (use toys)
    configMgr.nTOYS = 10000           # number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType = 2      # asymptotic calculator (create asimov dataset for the bkg hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType = 3            # one-sided profile likelihood test statistics
configMgr.nPoints = 20                # number of values scanned of signal-strength for upper-limit determination of signal strength

#####################
# Blind CR, VR and SR
#####################
configMgr.blindSR = True
configMgr.blindCR = True
configMgr.blindVR = True

# Only use signal in blinded data for discovery hypothesis test
#configMgr.useSignalInBlindedData = False


#########################
# Input/Output luminosity
#########################
configMgr.inputLumi = 0.001            # Plots scaled with factor of outputLumi/inputLumi
configMgr.outputLumi = 139             # 
configMgr.setLumiUnits("fb-1")

########
# Weight
########
configMgr.weights = ["genWeight", "eventWeight2", "leptonWeight", "bTagWeight", "jvtWeight", "pileupWeight"]

#############
# Input files
#############
signalInputFiles = []
bkgInputFiles = []
if configMgr.readFromTree:
    signalInputFiles.append("/squark1/nkvu/Susy1LInclusive/WZ_study/productions/prodApril/allTrees_signal_NoSys.root")
    bkgInputFiles.append("/squark1/nkvu/Susy1LInclusive/WZ_study/productions/prodApril/allTrees_bkg_NoSys.root")
else:
    inputFiles = [configMgr.histCacheFile]

###################################
# Dictionaty of cuts for Tree->hist    
###################################
# Preselection
preSelection = "nLep_base==1 && nLep_signal==1 && nJet30>=1 && mt>50 && met>100"
resolvedSelection = "lep1Pt>25 && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200 && dPhi_lep_met<2.8 && mjj_W>75 && mjj_W<85 && nFatjets==0"
boostedSelection = "lep1Pt>25 && nJet30<=3 && nBJet30_DL1==0 && met>200 && dPhi_lep_met<2.9 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>300 && met_Signif>14"
# Signal regions
# Resolved SRs
configMgr.cutsDict["SRLMresolved"] = preSelection + "&&" + resolvedSelection + "&& mt>200 && mt<380"
configMgr.cutsDict["SRHMresolved"] = preSelection + "&&" + resolvedSelection + "&& mt>380"
# Boosted SRs
configMgr.cutsDict["SRLMboosted"] = preSelection + "&&" + boostedSelection + "&& mt>120 && mt<240"
configMgr.cutsDict["SRMMboosted"] = preSelection + "&&" + boostedSelection + "&& mt>240 && mt<360"
configMgr.cutsDict["SRHMboosted"] = preSelection + "&&" + boostedSelection + "&& mt>360"

###########################################
# Name of nominal histogram for systematics    
###########################################
configMgr.nomName = "_NoSys"

#########
# Samples
#########
# Backgrounds
bkgSamples = OrderedDict()
backgrounds = ["diboson", "multiboson", "singletop", "ttbar", "tth", "ttv", "vh", "wjets", "zjets"]
bkgColors = [kMagenta+2, kRed+1, kGreen+3, kCyan, kCyan, kCyan-2, kCyan-8, kOrange+2, kBlue-3, kOrange+1]

for (treeName, color) in zip(backgrounds, bkgColors):
    bkgSample = Sample(treeName, color)
    bkgSample.setStatConfig(True)
    bkgSample.addInputs(bkgInputFiles, treeName)
    bkgSamples[treeName] = bkgSample

corb = Systematic("overall_sys", configMgr.weights, [1.3], [0.7], "user", "overallSys")
for bkgSample in bkgSamples.values():
    bkgSample.addSystematic(corb)

# Dataset
dataSample = Sample("Data", kBlack)
dataSample.setData()

# Signals
signals = ["C1C1_WW_200_0", "C1C1_WW_200_50", "C1C1_WW_300_0", "C1C1_WW_300_100", "C1C1_WW_300_150", "C1C1_WW_400_0", "C1C1_WW_400_150", "C1C1_WW_400_250", "C1C1_WW_400_50", "C1C1_WW_500_0", "C1C1_WW_500_100", "C1C1_WW_500_200", "C1C1_WW_500_300", "C1C1_WW_600_0", "C1C1_WW_600_100", "C1C1_WW_600_200", "C1C1_WW_600_300", "C1C1_WW_600_400", "C1C1_WW_700_0", "C1C1_WW_700_100", "C1C1_WW_700_200", "C1C1_WW_700_300", "C1C1_WW_700_400", "C1C1_WW_700_500", "C1C1_WW_800_0", "C1C1_WW_800_100", "C1C1_WW_800_200", "C1C1_WW_800_300", "C1C1_WW_800_400", "C1C1_WW_800_500", "C1C1_WW_900p0_0p0", "C1C1_WW_900_100", "C1C1_WW_900p0_200p0", "C1C1_WW_900_300", "C1C1_WW_900p0_400p0", "C1C1_WW_900_500", "C1C1_WW_1000_0", "C1C1_WW_1000_100", "C1C1_WW_1000_200", "C1C1_WW_1000_300", "C1C1_WW_1000_400", "C1C1_WW_1000_500", "C1C1_WW_1100_0", "C1C1_WW_1100_100", "C1C1_WW_1100_200", "C1C1_WW_1100_300", "C1C1_WW_1100_400", "C1C1_WW_1100_500", "C1C1_WW_1200_0", "C1C1_WW_1200_100", "C1C1_WW_1200_200", "C1C1_WW_1200_300", "C1C1_WW_1200_400"]

for sigPoint in signals:
    sigPoint2 = sigPoint.replace("p0", "")
    ana = configMgr.addFitConfig("Sig_{}".format(sigPoint2))
    
    signalSample = Sample(sigPoint2, kPink)
    signalSample.setNormFactor("mu_Sig", 1., 0., 5.)
    signalSample.setStatConfig(True)
    signalSample.setNormByTheory()
    signalSample.addInputs(signalInputFiles, sigPoint)
    
    ana.addSamples(bkgSamples.values() + [dataSample, signalSample])
    ana.setSignalSample(signalSample)

    # Define measurement
    meas = ana.addMeasurement(name="NormalMeasurement", lumi=1.0, lumiErr=0.032)
    meas.addPOI("mu_Sig")
    meas.addParamSetting("Lumi", "const", 1)

    channels = collections.OrderedDict()
    for sr in ["SRLMresolved", "SRHMresolved", "SRLMboosted", "SRMMboosted", "SRHMboosted"]:
        channels[sr] = ana.addChannel("cuts", [sr], 1, 0.5, 1.5)
        channels[sr].useOverflowBin = False
    ana.addSignalChannels([c for c in channels.values()])



#####
# End
#####         



