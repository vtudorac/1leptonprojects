import ROOT
from ROOT import gSystem
gSystem.Load('libSusyFitter.so')

from systematic import Systematic
from configManager import configMgr

ttbarSystematics = {}

ttbarSystematics["ttbarPartonShowering_TCRboosted"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_TVR1boosted"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.1004),(1.+0.1004)], [(1.-0.1004),(1.-0.1004)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_TVR2boosted"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.1595)], [(1.-0.1595)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_WDB1LCRboosted"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.1192),(1.+0.1192)], [(1.-0.1192),(1.-0.1192)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_WDB1LVR1boosted"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0745),(1.+0.0745)], [(1.-0.0745),(1.-0.0745)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_WDB1LVR2boosted"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0239),(1.+0.0239)], [(1.-0.0239),(1.-0.0239)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRLMboostedWW"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.3854),(1.+0.3854)], [(1.-0.3854),(1.-0.3854)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRMMboostedWW"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.3854),(1.+0.3854)], [(1.-0.3854),(1.-0.3854)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRHMboostedWW"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.3854),(1.+0.3854)], [(1.-0.3854),(1.-0.3854)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRLMboostedWZ"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.2979),(1.+0.2979)], [(1.-0.2979),(1.-0.2979)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRMMboostedWZ"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.2979),(1.+0.2979)], [(1.-0.2979),(1.-0.2979)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRHMboostedWZ"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.2979),(1.+0.2979)], [(1.-0.2979),(1.-0.2979)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_TCRresolved"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0000)], [(1.+0.0000)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_TVRresolved"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.1801)], [(1.-0.1801)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_WDB1LCRresolved"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0617)], [(1.-0.0617)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_WDB1LVRresolved"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0146)], [(1.-0.0146)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRLMresolved"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.3202)], [(1.-0.3202)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_SRHMresolved"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.3202)], [(1.-0.3202)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_DB2LCR"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.2003),(1.+0.2003)], [(1.-0.2003),(1.-0.2003)], "user", "userHistoSys")
ttbarSystematics["ttbarPartonShowering_DB2LVR"] = Systematic("ttbarPartonShowering", configMgr.weights, [(1.+0.0706),(1.+0.0706)], [(1.-0.0706),(1.-0.0706)], "user", "userHistoSys")


ttbarSystematics["ttbarHardScatter_TCRboosted"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_TVR1boosted"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.2229),(1.+0.2229)], [(1.-0.2229),(1.-0.2229)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_TVR2boosted"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0302),(1.+0.0302)], [(1.-0.0302),(1.-0.0302)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_WDB1LCRboosted"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.1897),(1.+0.1897)], [(1.-0.1897),(1.-0.1897)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_WDB1LVR1boosted"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.2706),(1.+0.2706)], [(1.-0.2706),(1.-0.2706)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_WDB1LVR2boosted"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.1433),(1.+0.1433)], [(1.-0.1433),(1.-0.1433)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRLMboostedWW"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0521),(1.+0.0521)], [(1.-0.0521),(1.-0.0521)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRMMboostedWW"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0521),(1.+0.0521)], [(1.-0.0521),(1.-0.0521)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRHMboostedWW"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0521),(1.+0.0521)], [(1.-0.0521),(1.-0.0521)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRLMboostedWZ"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0245),(1.+0.0245)], [(1.-0.0245),(1.-0.0245)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRMMboostedWZ"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0245),(1.+0.0245)], [(1.-0.0245),(1.-0.0245)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRHMboostedWZ"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0245),(1.+0.0245)], [(1.-0.0245),(1.-0.0245)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_TCRresolved"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0000)], [(1.+0.0000)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_TVRresolved"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.1641)], [(1.-0.1641)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_WDB1LCRresolved"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0415)], [(1.-0.0415)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_WDB1LVRresolved"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0625)], [(1.-0.0625)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRLMresolved"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.4325)], [(1.-0.4325)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_SRHMresolved"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.4325)], [(1.-0.4325)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_DB2LCR"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.0789),(1.+0.0789)], [(1.-0.0789),(1.-0.0789)], "user", "userHistoSys")
ttbarSystematics["ttbarHardScatter_DB2LVR"] = Systematic("ttbarHardScatter", configMgr.weights, [(1.+0.1993),(1.+0.1993)], [(1.-0.1993),(1.-0.1993)], "user", "userHistoSys")


ttbarSystematics["ttbarISR_TCRboosted"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_TVR1boosted"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0061),(1.+0.0061)], [(1.-0.0082),(1.-0.0082)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_TVR2boosted"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0007),(1.+0.0007)], [(1.-0.1365),(1.-0.1365)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_WDB1LCRboosted"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0045),(1.-0.0045)], [(1.-0.0511),(1.-0.0511)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_WDB1LVR1boosted"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0159)], [(1.-0.0159)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_WDB1LVR2boosted"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0008)], [(1.-0.1716)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRLMboostedWW"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.1610),(1.+0.1610)], [(1.-0.1389),(1.-0.1389)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRMMboostedWW"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.1610),(1.+0.1610)], [(1.-0.1389),(1.-0.1389)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRHMboostedWW"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.1610),(1.+0.1610)], [(1.-0.1389),(1.-0.1389)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRLMboostedWZ"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0806),(1.+0.0806)], [(1.-0.1210),(1.-0.1210)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRMMboostedWZ"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0806),(1.+0.0806)], [(1.-0.1210),(1.-0.1210)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRHMboostedWZ"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0806),(1.+0.0806)], [(1.-0.1210),(1.-0.1210)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_TCRresolved"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0000)], [(1.+0.0000)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_TVRresolved"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0162)], [(1.-0.1365)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_WDB1LCRresolved"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0342)], [(1.-0.0074)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_WDB1LVRresolved"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0086)], [(1.-0.0903)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRLMresolved"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0175)], [(1.-0.4252)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_SRHMresolved"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0175)], [(1.-0.4252)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_DB2LCR"] = Systematic("ttbarISR", configMgr.weights, [(1.+0.0507),(1.+0.0507)], [(1.-0.1011),(1.-0.1011)], "user", "userHistoSys")
ttbarSystematics["ttbarISR_DB2LVR"] = Systematic("ttbarISR", configMgr.weights, [(1.-0.0319),(1.-0.0319)], [(1.-0.1613),(1.-0.1613)], "user", "userHistoSys")


def TheorUnc(generatorSyst):
    for key in ttbarSystematics:
        name = key.split("_")[-1]

        generatorSyst.append((("ttbar", name+"EM"), ttbarSystematics[key]))
        generatorSyst.append((("ttbar", name+"El"), ttbarSystematics[key]))
        generatorSyst.append((("ttbar", name+"Mu"), ttbarSystematics[key]))
