import ROOT
from ROOT import gSystem
gSystem.Load('libSusyFitter.so')

from systematic import Systematic
from configManager import configMgr

ttvSystematics = {}

ttvSystematics['ttvRenorm_SRLMresolved'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0886)], [(1.+-0.0870)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRHMresolved'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0886)], [(1.+-0.0870)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRLMboostedWZ'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0864),(1.+0.0864)], [(1.+-0.0842),(1.+-0.0842)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRMMboostedWZ'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0864),(1.+0.0864)], [(1.+-0.0842),(1.+-0.0842)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRHMboostedWZ'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0864),(1.+0.0864)], [(1.+-0.0842),(1.+-0.0842)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRLMboostedWW'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0672),(1.+0.0672)], [(1.+-0.0726),(1.+0.0672)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRMMboostedWW'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0672),(1.+0.0672)], [(1.+-0.0726),(1.+-0.0726)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_SRHMboostedWW'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0672),(1.+0.0672)], [(1.+-0.0726),(1.+-0.0726)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_DB2LCR'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.1044),(1.+0.1044)], [(1.+-0.0964),(1.+-0.0964)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_DB2LVR'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0593),(1.+0.0593)], [(1.+-0.0699),(1.+-0.0699)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_WDB1LCRboosted'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0851),(1.+0.0851)], [(1.+-0.0841),(1.+-0.0841)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_WDB1LVR1boosted'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0848),(1.+0.0848)], [(1.+-0.0842),(1.+-0.0842)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_WDB1LVR2boosted'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0862),(1.+0.0862)], [(1.+-0.0841),(1.+-0.0841)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_WDB1LCRresolved'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.1076)], [(1.+-0.0986)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_WDB1LVRresolved'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0779)], [(1.+-0.0814)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_TCRboosted'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0902),(1.+0.0902)], [(1.+-0.0870),(1.+-0.0870)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_TVR1boosted'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0867),(1.+0.0867)], [(1.+-0.0849),(1.+-0.0849)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_TVR2boosted'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0833),(1.+0.0833)], [(1.+-0.0823),(1.+-0.0823)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_TCRresolved'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0732)], [(1.+-0.0781)], 'user', 'userHistoSys')
ttvSystematics['ttvRenorm_TVRresolved'] = Systematic('ttvRenorm', configMgr.weights, [(1.+0.0987)], [(1.+-0.0936)], 'user', 'userHistoSys')


ttvSystematics['ttvFactor_SRLMresolved'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0322)], [(1.+-0.0265)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRHMresolved'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0322)], [(1.+-0.0265)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRLMboostedWZ'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0376),(1.+0.0376)], [(1.+-0.0321),(1.+-0.0321)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRMMboostedWZ'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0376),(1.+0.0376)], [(1.+-0.0321),(1.+-0.0321)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRHMboostedWZ'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0376),(1.+0.0376)], [(1.+-0.0321),(1.+-0.0321)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRLMboostedWW'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0295),(1.+0.0295)], [(1.+-0.0255),(1.+-0.0255)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRMMboostedWW'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0295),(1.+0.0295)], [(1.+-0.0255),(1.+-0.0255)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_SRHMboostedWW'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0295),(1.+0.0295)], [(1.+-0.0255),(1.+-0.0255)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_DB2LCR'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0318),(1.+0.0318)], [(1.+-0.0266),(1.+-0.0266)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_DB2LVR'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0297)], [(1.+-0.0247)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_WDB1LCRboosted'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0428),(1.+0.0428)], [(1.+-0.0349),(1.+-0.0349)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_WDB1LVR1boosted'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0426),(1.+0.0426)], [(1.+-0.0347),(1.+-0.0347)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_WDB1LVR2boosted'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0417),(1.+0.0417)], [(1.+-0.0344),(1.+-0.0344)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_WDB1LCRresolved'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0490),(1.+0.0490)], [(1.+-0.0399),(1.+-0.0399)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_WDB1LVRresolved'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0246),(1.+0.0246)], [(1.+-0.0217),(1.+-0.0217)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_TCRboosted'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.+-0.0281),(1.+-0.0281)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_TVR1boosted'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0367)], [(1.+-0.0303)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_TVR2boosted'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0528)], [(1.+-0.0432)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_TCRresolved'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0042)], [(1.+-0.0037)], 'user', 'userHistoSys')
ttvSystematics['ttvFactor_TVRresolved'] = Systematic('ttvFactor', configMgr.weights, [(1.+0.0238)], [(1.+-0.0194)], 'user', 'userHistoSys')


ttvSystematics['ttvRenormFactor_SRLMresolved'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1129)], [(1.+-0.1171)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRHMresolved'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1129)], [(1.+-0.1171)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRLMboostedWZ'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1137),(1.+0.1137)], [(1.+-0.1206),(1.+-0.1206)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRMMboostedWZ'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1137),(1.+0.1137)], [(1.+-0.1206),(1.+-0.1206)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRHMboostedWZ'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1137),(1.+0.1137)], [(1.+-0.1206),(1.+-0.1206)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRLMboostedWW'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.0806),(1.+0.0806)], [(1.+-0.1054),(1.+-0.1054)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRMMboostedWW'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.0806),(1.+0.0806)], [(1.+-0.1054),(1.+-0.1054)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_SRHMboostedWW'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.0806),(1.+0.0806)], [(1.+-0.1054),(1.+-0.1054)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_DB2LCR'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1324)], [(1.+-0.1244)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_DB2LVR'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.0781)], [(1.+-0.0995)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_WDB1LCRboosted'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1242),(1.+0.1242)], [(1.+-0.1202),(1.+-0.1202)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_WDB1LVR1boosted'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1213)], [(1.+-0.1212)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_WDB1LVR2boosted'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1197)], [(1.+-0.1218)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_WDB1LCRresolved'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1556)], [(1.+-0.1381)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_WDB1LVRresolved'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.0934)], [(1.+-0.1071)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_TCRboosted'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1187),(1.+0.1187)], [(1.+-0.1175),(1.+-0.1175)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_TVR1boosted'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1163),(1.+0.1163)], [(1.+-0.1182),(1.+-0.1182)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_TVR2boosted'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1321),(1.+0.1321)], [(1.+-0.1266),(1.+-0.1266)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_TCRresolved'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.0633),(1.+0.0633)], [(1.+-0.0892),(1.+-0.0892)], 'user', 'userHistoSys')
ttvSystematics['ttvRenormFactor_TVRresolved'] = Systematic('ttvRenormFactor', configMgr.weights, [(1.+0.1148),(1.+0.1148)], [(1.+-0.1167),(1.+-0.1167)], 'user', 'userHistoSys')


ttvSystematics['ttvPDF_SRLMresolved'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0755)], [(1.-0.0755)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRHMresolved'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0755)], [(1.+0.0755)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRLMboostedWZ'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0618),(1.+0.0618)], [(1.-0.0618),(1.-0.0618)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRMMboostedWZ'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0618),(1.+0.0618)], [(1.-0.0618),(1.-0.0618)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRHMboostedWZ'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0618),(1.+0.0618)], [(1.-0.0618),(1.-0.0618)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRLMboostedWW'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0850),(1.+0.0850)], [(1.-0.0850),(1.-0.0850)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRMMboostedWW'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0850),(1.+0.0850)], [(1.-0.0850),(1.-0.0850)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_SRHMboostedWW'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0850),(1.+0.0850)], [(1.-0.0850),(1.-0.0850)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_DB2LCR'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0600),(1.+0.0600)], [(1.-0.0600),(1.-0.0600)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_DB2LVR'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.-0.0423),(1.-0.0423)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_WDB1LCRboosted'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0479),(1.+0.0479)], [(1.-0.0479),(1.-0.0479)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_WDB1LVR1boosted'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0579),(1.+0.0579)], [(1.-0.0579),(1.-0.0579)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_WDB1LVR2boosted'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.1160),(1.+0.1160)], [(1.-0.1160),(1.-0.1160)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_WDB1LCRresolved'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.3135)], [(1.-0.3135)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_WDB1LVRresolved'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.1222)], [(1.-0.1222)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_TCRboosted'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0448),(1.+0.0448)], [(1.-0.0448),(1.-0.0448)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_TVR1boosted'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0470),(1.+0.0470)], [(1.-0.0470),(1.-0.0470)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_TVR2boosted'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0728),(1.+0.0728)], [(1.-0.0728),(1.-0.0728)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_TCRresolved'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0634)], [(1.-0.0634)], 'user', 'userHistoSys')
ttvSystematics['ttvPDF_TVRresolved'] = Systematic('ttvPDF', configMgr.weights, [(1.+0.0472)], [(1.-0.0472)], 'user', 'userHistoSys')


def TheorUnc(generatorSyst):
	for key in ttvSystematics:
		name = key.split('_')[-1]
                
		generatorSyst.append((('ttv', name+'EM'), ttvSystematics[key]))
		generatorSyst.append((('ttv', name+'El'), ttvSystematics[key]))
		generatorSyst.append((('ttv', name+'Mu'), ttvSystematics[key]))
