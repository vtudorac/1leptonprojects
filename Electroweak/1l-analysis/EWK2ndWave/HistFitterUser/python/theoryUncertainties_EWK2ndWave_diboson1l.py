import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

diboson1lSystematics={}



diboson1lSystematics["diboson1lRenorm_SRLMboostedWZ"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.060200),(1.+0.060200)], [(1.-0.055200),(1.-0.055200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRLMboostedWZ"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.081400),(1.+0.081400)], [(1.+0.012200),(1.+0.012200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRLMboostedWZ"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.069100),(1.+0.069100)], [(1.+0.008400),(1.+0.008400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRLMboostedWZ"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.083400),(1.+0.083400)], [(1.-0.083400),(1.-0.083400)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRMMboostedWZ"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.049300),(1.+0.049300)], [(1.-0.059100),(1.-0.059100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRMMboostedWZ"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.072800),(1.+0.072800)], [(1.+0.024400),(1.+0.024400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRMMboostedWZ"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.068600),(1.+0.068600)], [(1.-0.003100),(1.-0.003100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRMMboostedWZ"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.078500),(1.+0.078500)], [(1.-0.078500),(1.-0.078500)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRHMboostedWZ"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.049700),(1.+0.049700)], [(1.-0.039000),(1.-0.039000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRHMboostedWZ"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.073400),(1.+0.073400)], [(1.+0.020900),(1.+0.020900)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRHMboostedWZ"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.049900),(1.+0.049900)], [(1.+0.020400),(1.+0.020400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRHMboostedWZ"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.068400),(1.+0.068400)], [(1.-0.068400),(1.-0.068400)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRLMboostedWW"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.023200),(1.+0.023200)], [(1.-0.025700),(1.-0.025700)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRLMboostedWW"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.088000),(1.+0.088000)], [(1.+0.005300),(1.+0.005300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRLMboostedWW"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.044000),(1.+0.044000)], [(1.+0.015200),(1.+0.015200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRLMboostedWW"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.072200),(1.+0.072200)], [(1.-0.072200),(1.-0.072200)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRMMboostedWW"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.087300),(1.+0.087300)], [(1.-0.075300),(1.-0.075300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRMMboostedWW"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.068700),(1.+0.068700)], [(1.+0.026800),(1.+0.026800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRMMboostedWW"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.110300),(1.+0.110300)], [(1.-0.025800),(1.-0.025800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRMMboostedWW"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.095900),(1.+0.095900)], [(1.-0.095900),(1.-0.095900)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRHMboostedWW"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.060400),(1.+0.060400)], [(1.-0.052400),(1.-0.052400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRHMboostedWW"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.067700),(1.+0.067700)], [(1.+0.027000),(1.+0.027000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRHMboostedWW"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.055600),(1.+0.055600)], [(1.-0.001800),(1.-0.001800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRHMboostedWW"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.118300),(1.+0.118300)], [(1.-0.118300),(1.-0.118300)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_DB2LCR"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.201900)], [(1.-0.184200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_DB2LCR"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.098600)], [(1.-0.009600)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_DB2LCR"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.330600)], [(1.-0.195900)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_DB2LCR"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.101600)], [(1.-0.101600)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_DB2LVR"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.000100)], [(1.-0.016600)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_DB2LVR"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.220800)], [(1.-0.113700)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_DB2LVR"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.246200)], [(1.-0.093100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_DB2LVR"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.195200)], [(1.-0.195200)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_WDB1LCRboosted"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.000000),(1.+0.000000)], [(1.+0.000000),(1.+0.000000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_WDB1LCRboosted"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.000000),(1.+0.000000)], [(1.+0.000000),(1.+0.000000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_WDB1LCRboosted"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.000000),(1.+0.000000)], [(1.+0.000000),(1.+0.000000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_WDB1LCRboosted"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.000000),(1.+0.000000)], [(1.-0.000000),(1.-0.000000)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_WDB1LVR1boosted"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.084600),(1.+0.084600)], [(1.-0.099300),(1.-0.099300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_WDB1LVR1boosted"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.090300),(1.+0.090300)], [(1.-0.003500),(1.-0.003500)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_WDB1LVR1boosted"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.166000),(1.+0.166000)], [(1.-0.112300),(1.-0.112300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_WDB1LVR1boosted"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.245500),(1.+0.245500)], [(1.-0.245500),(1.-0.245500)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_WD1LVR2boosted"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.099400),(1.+0.099400)], [(1.-0.142800),(1.-0.142800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_WD1LVR2boosted"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.105600),(1.+0.105600)], [(1.-0.009600),(1.-0.009600)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_WD1LVR2boosted"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.192000),(1.+0.192000)], [(1.-0.174100),(1.-0.174100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_WD1LVR2boosted"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.083000),(1.+0.083000)], [(1.-0.083000),(1.-0.083000)], "user", "userHistoSys")



diboson1lSystematics["diboson1lRenorm_TCRboosted"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.114500),(1.+0.114500)], [(1.-0.154000),(1.-0.154000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_TCRboosted"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.095900),(1.+0.095900)], [(1.-0.014300),(1.-0.014300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_TCRboosted"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.202600),(1.+0.202600)], [(1.-0.157400),(1.-0.157400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_TCRboosted"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.071800),(1.+0.071800)], [(1.-0.071800),(1.-0.071800)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_TVR1boosted"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.170400),(1.+0.170400)], [(1.-0.218900),(1.-0.218900)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_TVR1boosted"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.132400),(1.+0.132400)], [(1.-0.032500),(1.-0.032500)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_TVR1boosted"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.292500),(1.+0.292500)], [(1.-0.267100),(1.-0.267100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_TVR1boosted"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.090300),(1.+0.090300)], [(1.-0.090300),(1.-0.090300)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_TVR2boosted"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.077000),(1.+0.077000)], [(1.-0.101800),(1.-0.101800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_TVR2boosted"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.106300),(1.+0.106300)], [(1.-0.027100),(1.-0.027100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_TVR2boosted"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.167500),(1.+0.167500)], [(1.-0.147500),(1.-0.147500)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_TVR2boosted"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.073300),(1.+0.073300)], [(1.-0.073300),(1.-0.073300)], "user", "userHistoSys")



diboson1lSystematics["diboson1lRenorm_SRLMboostedWZdisc"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.060200),(1.+0.060200)], [(1.-0.055200),(1.-0.055200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRLMboostedWZdisc"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.081400),(1.+0.081400)], [(1.+0.012200),(1.+0.012200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRLMboostedWZdisc"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.069100),(1.+0.069100)], [(1.+0.008400),(1.+0.008400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRLMboostedWZdisc"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.083400),(1.+0.083400)], [(1.-0.083400),(1.-0.083400)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRMMboostedWZdisc"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.049300),(1.+0.049300)], [(1.-0.059100),(1.-0.059100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRMMboostedWZdisc"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.072800),(1.+0.072800)], [(1.+0.024400),(1.+0.024400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRMMboostedWZdisc"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.068600),(1.+0.068600)], [(1.-0.003100),(1.-0.003100)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRMMboostedWZdisc"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.078500),(1.+0.078500)], [(1.-0.078500),(1.-0.078500)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRHMboostedWZdisc"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.049700),(1.+0.049700)], [(1.-0.039000),(1.-0.039000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRHMboostedWZdisc"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.073400),(1.+0.073400)], [(1.+0.020900),(1.+0.020900)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRHMboostedWZdisc"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.049900),(1.+0.049900)], [(1.+0.020400),(1.+0.020400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRHMboostedWZdisc"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.068400),(1.+0.068400)], [(1.-0.068400),(1.-0.068400)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRLMboostedWWdisc"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.023200),(1.+0.023200)], [(1.-0.025700),(1.-0.025700)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRLMboostedWWdisc"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.088000),(1.+0.088000)], [(1.+0.005300),(1.+0.005300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRLMboostedWWdisc"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.044000),(1.+0.044000)], [(1.+0.015200),(1.+0.015200)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRLMboostedWWdisc"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.072200),(1.+0.072200)], [(1.-0.072200),(1.-0.072200)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRMMboostedWWdisc"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.087300),(1.+0.087300)], [(1.-0.075300),(1.-0.075300)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRMMboostedWWdisc"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.068700),(1.+0.068700)], [(1.+0.026800),(1.+0.026800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRMMboostedWWdisc"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.110300),(1.+0.110300)], [(1.-0.025800),(1.-0.025800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRMMboostedWWdisc"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.095900),(1.+0.095900)], [(1.-0.095900),(1.-0.095900)], "user", "userHistoSys")

diboson1lSystematics["diboson1lRenorm_SRHMboostedWWdisc"] = Systematic("diboson1lRenorm", configMgr.weights, [(1.+0.060400),(1.+0.060400)], [(1.-0.052400),(1.-0.052400)], "user", "userHistoSys")
diboson1lSystematics["diboson1lFactor_SRHMboostedWWdisc"] = Systematic("diboson1lFactor", configMgr.weights, [(1.+0.067700),(1.+0.067700)], [(1.+0.027000),(1.+0.027000)], "user", "userHistoSys")
diboson1lSystematics["diboson1lRenormFactor_SRHMboostedWWdisc"] = Systematic("diboson1lRenormFactor", configMgr.weights, [(1.+0.055600),(1.+0.055600)], [(1.-0.001800),(1.-0.001800)], "user", "userHistoSys")
diboson1lSystematics["diboson1lPDF_SRHMboostedWWdisc"] = Systematic("diboson1lPDF", configMgr.weights, [(1.+0.118300),(1.+0.118300)], [(1.-0.118300),(1.-0.118300)], "user", "userHistoSys")




def TheorUnc(generatorSyst):
    for key in diboson1lSystematics:
        name = key.split('_')[-1]

        generatorSyst.append((('diboson1l', name+'EM'), diboson1lSystematics[key]))
        generatorSyst.append((('diboson1l', name+'El'), diboson1lSystematics[key]))
        generatorSyst.append((('diboson1l', name+'Mu'), diboson1lSystematics[key]))
