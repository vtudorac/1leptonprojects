import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

diboson2lSystematics={}



diboson2lSystematics["diboson2lRenorm_SRLMboostedWZ"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.022600),(1.+0.022600)], [(1.-0.034400),(1.-0.034400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRLMboostedWZ"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.023500),(1.+0.023500)], [(1.-0.020400),(1.-0.020400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRLMboostedWZ"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.002700),(1.+0.002700)], [(1.-0.006300),(1.-0.006300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRLMboostedWZ"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.082400),(1.+0.082400)], [(1.-0.082400),(1.-0.082400)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRMMboostedWZ"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.021700),(1.+0.021700)], [(1.-0.013600),(1.-0.013600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRMMboostedWZ"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.003600),(1.+0.003600)], [(1.+0.000900),(1.+0.000900)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRMMboostedWZ"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.019700),(1.+0.019700)], [(1.-0.014200),(1.-0.014200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRMMboostedWZ"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.081700),(1.+0.081700)], [(1.-0.081700),(1.-0.081700)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRHMboostedWZ"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.011700),(1.+0.011700)], [(1.-0.013700),(1.-0.013700)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRHMboostedWZ"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.002500),(1.+0.002500)], [(1.-0.000400),(1.-0.000400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRHMboostedWZ"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.010400),(1.+0.010400)], [(1.-0.013200),(1.-0.013200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRHMboostedWZ"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.371500),(1.+0.371500)], [(1.-0.371500),(1.-0.371500)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRLMboostedWW"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.015100),(1.+0.015100)], [(1.-0.011400),(1.-0.011400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRLMboostedWW"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.024900),(1.+0.024900)], [(1.-0.019400),(1.-0.019400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRLMboostedWW"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.014600),(1.+0.014600)], [(1.-0.003500),(1.-0.003500)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRLMboostedWW"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.081200),(1.+0.081200)], [(1.-0.081200),(1.-0.081200)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRMMboostedWW"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.016000),(1.+0.016000)], [(1.-0.008200),(1.-0.008200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRMMboostedWW"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.009900),(1.+0.009900)], [(1.-0.005300),(1.-0.005300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRMMboostedWW"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.008600),(1.+0.008600)], [(1.-0.001500),(1.-0.001500)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRMMboostedWW"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.086700),(1.+0.086700)], [(1.-0.086700),(1.-0.086700)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRHMboostedWW"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.017700),(1.+0.017700)], [(1.-0.009400),(1.-0.009400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRHMboostedWW"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.022200),(1.+0.022200)], [(1.-0.016300),(1.-0.016300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRHMboostedWW"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.009000),(1.+0.009000)], [(1.-0.001300),(1.-0.001300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRHMboostedWW"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.098400),(1.+0.098400)], [(1.-0.098400),(1.-0.098400)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_DB2LCR"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.000000)], [(1.+0.000000)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_DB2LCR"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.000000)], [(1.+0.000000)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_DB2LCR"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.000000)], [(1.+0.000000)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_DB2LCR"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.000000)], [(1.-0.000000)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_DB2LVR"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.006500)], [(1.-0.010100)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_DB2LVR"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.009100)], [(1.-0.010300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_DB2LVR"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.018500)], [(1.-0.018100)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_DB2LVR"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.231900)], [(1.-0.231900)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_WDB1LCRboosted"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.028000),(1.+0.028000)], [(1.-0.028700),(1.-0.028700)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_WDB1LCRboosted"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.014200),(1.+0.014200)], [(1.-0.010600),(1.-0.010600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_WDB1LCRboosted"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.041200),(1.+0.041200)], [(1.-0.039600),(1.-0.039600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_WDB1LCRboosted"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.141700),(1.+0.141700)], [(1.-0.141700),(1.-0.141700)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_WDB1LVR1boosted"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.003600),(1.+0.003600)], [(1.-0.004400),(1.-0.004400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_WDB1LVR1boosted"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.001100),(1.+0.001100)], [(1.+0.000400),(1.+0.000400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_WDB1LVR1boosted"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.004400),(1.+0.004400)], [(1.-0.003600),(1.-0.003600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_WDB1LVR1boosted"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.091500),(1.+0.091500)], [(1.-0.091500),(1.-0.091500)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_WD1LVR2boosted"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.022400),(1.+0.022400)], [(1.-0.024900),(1.-0.024900)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_WD1LVR2boosted"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.001100),(1.+0.001100)], [(1.-0.000100),(1.-0.000100)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_WD1LVR2boosted"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.021500),(1.+0.021500)], [(1.-0.022600),(1.-0.022600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_WD1LVR2boosted"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.084300),(1.+0.084300)], [(1.-0.084300),(1.-0.084300)], "user", "userHistoSys")



diboson2lSystematics["diboson2lRenorm_TCRboosted"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.038100),(1.+0.038100)], [(1.-0.063400),(1.-0.063400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_TCRboosted"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.006300),(1.+0.006300)], [(1.-0.002400),(1.-0.002400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_TCRboosted"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.045200),(1.+0.045200)], [(1.-0.065500),(1.-0.065500)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_TCRboosted"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.066200),(1.+0.066200)], [(1.-0.066200),(1.-0.066200)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_TVR1boosted"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.009700),(1.+0.009700)], [(1.-0.000600),(1.-0.000600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_TVR1boosted"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.006300),(1.+0.006300)], [(1.-0.019900),(1.-0.019900)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_TVR1boosted"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.015100),(1.+0.015100)], [(1.-0.022000),(1.-0.022000)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_TVR1boosted"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.065300),(1.+0.065300)], [(1.-0.065300),(1.-0.065300)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_TVR2boosted"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.028100),(1.+0.028100)], [(1.-0.027900),(1.-0.027900)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_TVR2boosted"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.009900),(1.+0.009900)], [(1.+0.002200),(1.+0.002200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_TVR2boosted"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.031100),(1.+0.031100)], [(1.-0.017200),(1.-0.017200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_TVR2boosted"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.069300),(1.+0.069300)], [(1.-0.069300),(1.-0.069300)], "user", "userHistoSys")





diboson2lSystematics["diboson2lRenorm_SRLMboostedWZdisc"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.022600),(1.+0.022600)], [(1.-0.034400),(1.-0.034400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRLMboostedWZdisc"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.023500),(1.+0.023500)], [(1.-0.020400),(1.-0.020400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRLMboostedWZdisc"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.002700),(1.+0.002700)], [(1.-0.006300),(1.-0.006300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRLMboostedWZdisc"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.082400),(1.+0.082400)], [(1.-0.082400),(1.-0.082400)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRMMboostedWZdisc"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.021700),(1.+0.021700)], [(1.-0.013600),(1.-0.013600)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRMMboostedWZdisc"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.003600),(1.+0.003600)], [(1.+0.000900),(1.+0.000900)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRMMboostedWZdisc"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.019700),(1.+0.019700)], [(1.-0.014200),(1.-0.014200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRMMboostedWZdisc"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.081700),(1.+0.081700)], [(1.-0.081700),(1.-0.081700)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRHMboostedWZdisc"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.011700),(1.+0.011700)], [(1.-0.013700),(1.-0.013700)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRHMboostedWZdisc"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.002500),(1.+0.002500)], [(1.-0.000400),(1.-0.000400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRHMboostedWZdisc"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.010400),(1.+0.010400)], [(1.-0.013200),(1.-0.013200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRHMboostedWZdisc"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.371500),(1.+0.371500)], [(1.-0.371500),(1.-0.371500)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRLMboostedWWdisc"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.015100),(1.+0.015100)], [(1.-0.011400),(1.-0.011400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRLMboostedWWdisc"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.024900),(1.+0.024900)], [(1.-0.019400),(1.-0.019400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRLMboostedWWdisc"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.014600),(1.+0.014600)], [(1.-0.003500),(1.-0.003500)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRLMboostedWWdisc"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.081200),(1.+0.081200)], [(1.-0.081200),(1.-0.081200)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRMMboostedWWdisc"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.016000),(1.+0.016000)], [(1.-0.008200),(1.-0.008200)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRMMboostedWWdisc"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.009900),(1.+0.009900)], [(1.-0.005300),(1.-0.005300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRMMboostedWWdisc"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.008600),(1.+0.008600)], [(1.-0.001500),(1.-0.001500)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRMMboostedWWdisc"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.086700),(1.+0.086700)], [(1.-0.086700),(1.-0.086700)], "user", "userHistoSys")

diboson2lSystematics["diboson2lRenorm_SRHMboostedWWdisc"] = Systematic("diboson2lRenorm", configMgr.weights, [(1.+0.017700),(1.+0.017700)], [(1.-0.009400),(1.-0.009400)], "user", "userHistoSys")
diboson2lSystematics["diboson2lFactor_SRHMboostedWWdisc"] = Systematic("diboson2lFactor", configMgr.weights, [(1.+0.022200),(1.+0.022200)], [(1.-0.016300),(1.-0.016300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lRenormFactor_SRHMboostedWWdisc"] = Systematic("diboson2lRenormFactor", configMgr.weights, [(1.+0.009000),(1.+0.009000)], [(1.-0.001300),(1.-0.001300)], "user", "userHistoSys")
diboson2lSystematics["diboson2lPDF_SRHMboostedWWdisc"] = Systematic("diboson2lPDF", configMgr.weights, [(1.+0.098400),(1.+0.098400)], [(1.-0.098400),(1.-0.098400)], "user", "userHistoSys")



def TheorUnc(generatorSyst):
    for key in diboson2lSystematics:
        name = key.split('_')[-1]

        generatorSyst.append((('diboson2l', name+'EM'), diboson2lSystematics[key]))
        generatorSyst.append((('diboson2l', name+'El'), diboson2lSystematics[key]))
        generatorSyst.append((('diboson2l', name+'Mu'), diboson2lSystematics[key]))
