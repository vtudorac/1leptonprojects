import ROOT
from ROOT import gSystem
gSystem.Load('libSusyFitter.so')

from systematic import Systematic
from configManager import configMgr

singletopSystematics = {}

singletopSystematics['singletopWtInterference_TCRboosted'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2261),(1.+0.2261)], [(1.-0.2261),(1.-0.2261)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_TVR1boosted'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.3615),(1.+0.3615)], [(1.-0.3615),(1.-0.3615)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_TVR2boosted'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.5099),(1.+0.5099)], [(1.-0.5099),(1.-0.5099)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_WDB1LCRboosted'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.0446),(1.+0.0446)], [(1.-0.0446),(1.-0.0446)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_WDB1LVR1boosted'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.0693),(1.+0.0693)], [(1.-0.0693),(1.-0.0693)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_WDB1LVR2boosted'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2257),(1.+0.2257)], [(1.-0.2257),(1.-0.2257)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_DB2LCR'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.1262)], [(1.-0.1262)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_DB2LVR'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2414)], [(1.-0.2414)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRLMboostedWW'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2382),(1.+0.2382)], [(1.-0.2382),(1.-0.2382)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRMMboostedWW'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2382),(1.+0.2382)], [(1.-0.2382),(1.-0.2382)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRHMboostedWW'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2382),(1.+0.2382)], [(1.-0.2382),(1.-0.2382)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRLMboostedWZ'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.6228),(1.+0.6228)], [(1.-0.6228),(1.-0.6228)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRMMboostedWZ'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.6228),(1.+0.6228)], [(1.-0.6228),(1.-0.6228)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRHMboostedWZ'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.6228),(1.+0.6228)], [(1.-0.6228),(1.+0.6228)], "user", "userHistoSys")

singletopSystematics['singletopWtInterference_SRLMboostedWWdisc'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2382),(1.+0.2382)], [(1.-0.2382),(1.-0.2382)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRMMboostedWWdisc'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2382),(1.+0.2382)], [(1.-0.2382),(1.-0.2382)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRHMboostedWWdisc'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.2382),(1.+0.2382)], [(1.-0.2382),(1.-0.2382)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRLMboostedWZdisc'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.6228),(1.+0.6228)], [(1.-0.6228),(1.-0.6228)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRMMboostedWZdisc'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.6228),(1.+0.6228)], [(1.-0.6228),(1.-0.6228)], "user", "userHistoSys")
singletopSystematics['singletopWtInterference_SRHMboostedWZdisc'] = Systematic('singletopWtInterference', configMgr.weights, [(1.+0.6228),(1.+0.6228)], [(1.-0.6228),(1.+0.6228)], "user", "userHistoSys")




singletopSystematics['singletopPartonShowering_TCRboosted'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.2498),(1.+0.2498)], [(1.-0.2498),(1.-0.2498)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_TVR1boosted'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.2249),(1.+0.2249)], [(1.-0.2249),(1.-0.2249)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_TVR2boosted'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0575),(1.+0.0575)], [(1.-0.0575),(1.-0.0575)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_WDB1LCRboosted'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0820),(1.+0.0820)], [(1.-0.0820),(1.-0.0820)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_WDB1LVR1boosted'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0317),(1.+0.0317)], [(1.-0.0317),(1.-0.0317)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_WDB1LVR2boosted'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1935),(1.+0.1935)], [(1.-0.1935),(1.-0.1935)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_DB2LCR'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.4000)], [(1.-0.4000)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_DB2LVR'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1066)], [(1.-0.1066)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRLMboostedWW'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1475),(1.+0.1475)], [(1.-0.1475),(1.-0.1475)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRMMboostedWW'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1475),(1.+0.1475)], [(1.-0.1475),(1.-0.1475)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRHMboostedWW'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1475),(1.+0.1475)], [(1.-0.1475),(1.-0.1475)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRLMboostedWZ'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0190),(1.+0.0190)], [(1.-0.0190),(1.-0.0190)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRMMboostedWZ'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0190),(1.+0.0190)], [(1.-0.0190),(1.-0.0190)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRHMboostedWZ'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0190),(1.+0.0190)], [(1.-0.0190),(1.-0.0190)], "user", "userHistoSys")

singletopSystematics['singletopPartonShowering_SRLMboostedWWdisc'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1475),(1.+0.1475)], [(1.-0.1475),(1.-0.1475)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRMMboostedWWdisc'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1475),(1.+0.1475)], [(1.-0.1475),(1.-0.1475)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRHMboostedWWdisc'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.1475),(1.+0.1475)], [(1.-0.1475),(1.-0.1475)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRLMboostedWZdisc'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0190),(1.+0.0190)], [(1.-0.0190),(1.-0.0190)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRMMboostedWZdisc'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0190),(1.+0.0190)], [(1.-0.0190),(1.-0.0190)], "user", "userHistoSys")
singletopSystematics['singletopPartonShowering_SRHMboostedWZdisc'] = Systematic('singletopPartonShowering', configMgr.weights, [(1.+0.0190),(1.+0.0190)], [(1.-0.0190),(1.-0.0190)], "user", "userHistoSys")




singletopSystematics['singletopHardScatter_TCRboosted'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.0284),(1.+0.0284)], [(1.-0.0284),(1.-0.0284)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_TVR1boosted'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.1909),(1.+0.1909)], [(1.-0.1909),(1.-0.1909)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_TVR2boosted'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3161),(1.+0.3161)], [(1.-0.3161),(1.-0.3161)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_WDB1LCRboosted'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.1249),(1.+0.1249)], [(1.-0.1249),(1.-0.1249)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_WDB1LVR1boosted'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2030),(1.+0.2030)], [(1.-0.2030),(1.-0.2030)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_WDB1LVR2boosted'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.1677),(1.+0.1677)], [(1.-0.1677),(1.-0.1677)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_DB2LCR'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2947)], [(1.-0.2947)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_DB2LVR'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2459)], [(1.-0.2459)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRLMboostedWW'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2787),(1.+0.2787)], [(1.-0.2787),(1.-0.2787)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRMMboostedWW'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2787),(1.+0.2787)], [(1.-0.2787),(1.-0.2787)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRHMboostedWW'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2787),(1.+0.2787)], [(1.-0.2787),(1.-0.2787)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRLMboostedWZ'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3223),(1.+0.3223)], [(1.-0.3223),(1.-0.3223)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRMMboostedWZ'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3223),(1.+0.3223)], [(1.-0.3223),(1.-0.3223)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRHMboostedWZ'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3223),(1.+0.3223)], [(1.-0.3223),(1.-0.3223)], "user", "userHistoSys")

singletopSystematics['singletopHardScatter_SRLMboostedWWdisc'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2787),(1.+0.2787)], [(1.-0.2787),(1.-0.2787)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRMMboostedWWdisc'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2787),(1.+0.2787)], [(1.-0.2787),(1.-0.2787)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRHMboostedWWdisc'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.2787),(1.+0.2787)], [(1.-0.2787),(1.-0.2787)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRLMboostedWZdisc'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3223),(1.+0.3223)], [(1.-0.3223),(1.-0.3223)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRMMboostedWZdisc'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3223),(1.+0.3223)], [(1.-0.3223),(1.-0.3223)], "user", "userHistoSys")
singletopSystematics['singletopHardScatter_SRHMboostedWZdisc'] = Systematic('singletopHardScatter', configMgr.weights, [(1.+0.3223),(1.+0.3223)], [(1.-0.3223),(1.-0.3223)], "user", "userHistoSys")



singletopSystematics['singletopISR_TCRboosted'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1052),(1.+0.1052)], [(1.-0.0387),(1.-0.0387)], "user", "userHistoSys")
singletopSystematics['singletopISR_TVR1boosted'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1281),(1.+0.1281)], [(1.-0.0556),(1.-0.0556)], "user", "userHistoSys")
singletopSystematics['singletopISR_TVR2boosted'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2500),(1.+0.2500)], [(1.-0.0736),(1.-0.0736)], "user", "userHistoSys")
singletopSystematics['singletopISR_WDB1LCRboosted'] = Systematic('singletopISR', configMgr.weights, [(1.+0.0717),(1.+0.0717)], [(1.-0.0212),(1.-0.0212)], "user", "userHistoSys")
singletopSystematics['singletopISR_WDB1LVR1boosted'] = Systematic('singletopISR', configMgr.weights, [(1.+0.0813),(1.+0.0813)], [(1.-0.0098),(1.-0.0098)], "user", "userHistoSys")
singletopSystematics['singletopISR_WDB1LVR2boosted'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2299),(1.+0.2299)], [(1.-0.0321),(1.-0.0321)], "user", "userHistoSys")
singletopSystematics['singletopISR_DB2LCR'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1741)], [(1.-0.1150)], "user", "userHistoSys")
singletopSystematics['singletopISR_DB2LVR'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1241)], [(1.-0.0517)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRLMboostedWW'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1796),(1.+0.1796)], [(1.-0.1683),(1.-0.1683)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRMMboostedWW'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1796),(1.+0.1796)], [(1.-0.1683),(1.-0.1683)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRHMboostedWW'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1796),(1.+0.1796)], [(1.-0.1683),(1.-0.1683)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRLMboostedWZ'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2241),(1.+0.2241)], [(1.-0.1828),(1.-0.1828)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRMMboostedWZ'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2241),(1.+0.2241)], [(1.-0.1828),(1.-0.1828)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRHMboostedWZ'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2241),(1.+0.2241)], [(1.-0.1828),(1.-0.1828)], "user", "userHistoSys")

singletopSystematics['singletopISR_SRLMboostedWWdisc'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1796),(1.+0.1796)], [(1.-0.1683),(1.-0.1683)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRMMboostedWWdisc'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1796),(1.+0.1796)], [(1.-0.1683),(1.-0.1683)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRHMboostedWWdisc'] = Systematic('singletopISR', configMgr.weights, [(1.+0.1796),(1.+0.1796)], [(1.-0.1683),(1.-0.1683)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRLMboostedWZdisc'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2241),(1.+0.2241)], [(1.-0.1828),(1.-0.1828)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRMMboostedWZdisc'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2241),(1.+0.2241)], [(1.-0.1828),(1.-0.1828)], "user", "userHistoSys")
singletopSystematics['singletopISR_SRHMboostedWZdisc'] = Systematic('singletopISR', configMgr.weights, [(1.+0.2241),(1.+0.2241)], [(1.-0.1828),(1.-0.1828)], "user", "userHistoSys")



def TheorUnc(generatorSyst):
    for key in singletopSystematics:
        name = key.split("_")[-1]

        generatorSyst.append((("singletop", name+"EM"), singletopSystematics[key]))
        generatorSyst.append((("singletop", name+"El"), singletopSystematics[key]))
        generatorSyst.append((("singletop", name+"Mu"), singletopSystematics[key]))
