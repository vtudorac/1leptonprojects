import ROOT
from ROOT import gSystem
gSystem.Load('libSusyFitter.so')

from systematic import Systematic
from configManager import configMgr

tthSystematics = {}

tthSystematics['tthRenorm_SRLMboostedWZ'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0269),(1.+-0.0269)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRMMboostedWZ'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0269),(1.+-0.0269)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRHMboostedWZ'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0269),(1.+-0.0269)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRLMboostedWW'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0161),(1.+0.0161)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRMMboostedWW'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0161),(1.+0.0161)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRHMboostedWW'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0161),(1.+0.0161)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_DB2LCR'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0661)], [(1.+-0.0750)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_DB2LVR'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0418)], [(1.+-0.0617)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_WDB1LCRboosted'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0158),(1.+0.0158)], [(1.+-0.0446),(1.+-0.0446)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_WDB1LVR1boosted'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0245),(1.+0.0245)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_WDB1LVR2boosted'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0025),(1.+-0.0025)], [(1.+-0.0531),(1.+-0.0531)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_TCRboosted'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0093),(1.+-0.0093)], [(1.+-0.0435),(1.+-0.0435)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_TVR1boosted'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0125),(1.+-0.0125)], [(1.+-0.0270),(1.+-0.0270)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_TVR2boosted'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0154),(1.+0.0154)], [(1.+-0.0820),(1.+-0.0820)], 'user', 'userHistoSys')


tthSystematics['tthRenorm_SRLMboostedWZdisc'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0269),(1.+-0.0269)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRMMboostedWZdisc'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0269),(1.+-0.0269)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRHMboostedWZdisc'] = Systematic('tthRenorm', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0269),(1.+-0.0269)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRLMboostedWWdisc'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0161),(1.+0.0161)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRMMboostedWWdisc'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0161),(1.+0.0161)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')
tthSystematics['tthRenorm_SRHMboostedWWdisc'] = Systematic('tthRenorm', configMgr.weights, [(1.+0.0161),(1.+0.0161)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')


tthSystematics['tthFactor_SRLMboostedWZ'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0163),(1.+0.0163)], [(1.+-0.0130),(1.+-0.0130)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRMMboostedWZ'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0163),(1.+0.0163)], [(1.+-0.0130),(1.+0.0163)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRHMboostedWZ'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0163),(1.+0.0163)], [(1.+-0.0130),(1.+-0.0130)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRLMboostedWW'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0226),(1.+0.0226)], [(1.+-0.0132),(1.+-0.0132)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRMMboostedWW'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0226),(1.+0.0226)], [(1.+-0.0132),(1.+-0.0132)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRHMboostedWW'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0226),(1.+0.0226)], [(1.+-0.0132),(1.+-0.0132)], 'user', 'userHistoSys')
tthSystematics['tthFactor_DB2LCR'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0380)], [(1.+-0.0297)], 'user', 'userHistoSys')
tthSystematics['tthFactor_DB2LVR'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0334)], [(1.+-0.0254)], 'user', 'userHistoSys')
tthSystematics['tthFactor_WDB1LCRboosted'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0154),(1.+0.0154)], [(1.+-0.0125),(1.+-0.0125)], 'user', 'userHistoSys')
tthSystematics['tthFactor_WDB1LVR1boosted'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0222),(1.+0.0222)], [(1.+-0.0187),(1.+-0.0187)], 'user', 'userHistoSys')
tthSystematics['tthFactor_WDB1LVR2boosted'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0044),(1.+0.0044)], [(1.+-0.0089),(1.+-0.0089)], 'user', 'userHistoSys')
tthSystematics['tthFactor_TCRboosted'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0008),(1.+0.0008)], [(1.+-0.0013),(1.+-0.0013)], 'user', 'userHistoSys')
tthSystematics['tthFactor_TVR1boosted'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0080),(1.+0.0080)], [(1.+-0.0072),(1.+-0.0072)], 'user', 'userHistoSys')
tthSystematics['tthFactor_TVR2boosted'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0037),(1.+0.0037)], [(1.+-0.0044),(1.+-0.0044)], 'user', 'userHistoSys')

tthSystematics['tthFactor_SRLMboostedWZdisc'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0163),(1.+0.0163)], [(1.+-0.0130),(1.+-0.0130)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRMMboostedWZdisc'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0163),(1.+0.0163)], [(1.+-0.0130),(1.+0.0163)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRHMboostedWZdisc'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0163),(1.+0.0163)], [(1.+-0.0130),(1.+-0.0130)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRLMboostedWWdisc'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0226),(1.+0.0226)], [(1.+-0.0132),(1.+-0.0132)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRMMboostedWWdisc'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0226),(1.+0.0226)], [(1.+-0.0132),(1.+-0.0132)], 'user', 'userHistoSys')
tthSystematics['tthFactor_SRHMboostedWWdisc'] = Systematic('tthFactor', configMgr.weights, [(1.+0.0226),(1.+0.0226)], [(1.+-0.0132),(1.+-0.0132)], 'user', 'userHistoSys')


tthSystematics['tthRenormFactor_SRLMboostedWZ'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0246),(1.+-0.0246)], [(1.+-0.0546),(1.+-0.0546)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRMMboostedWZ'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0246),(1.+-0.0246)], [(1.+-0.0546),(1.+-0.0546)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRHMboostedWZ'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0246),(1.+-0.0246)], [(1.+-0.0546),(1.+-0.0546)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRLMboostedWW'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0136),(1.+-0.0136)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRMMboostedWW'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0136),(1.+-0.0136)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRHMboostedWW'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0136),(1.+-0.0136)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_DB2LCR'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+0.0916)], [(1.+-0.1105)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_DB2LVR'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+0.0601)], [(1.+-0.0942)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_WDB1LCRboosted'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+0.0019),(1.+0.0019)], [(1.+-0.0712),(1.+-0.0712)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_WDB1LVR1boosted'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+0.0206),(1.+0.0206)], [(1.+-0.0804),(1.+-0.0804)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_WDB1LVR2boosted'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0206),(1.+-0.0206)], [(1.+-0.1084),(1.+-0.1084)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_TCRboosted'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0287),(1.+-0.0287)], [(1.+-0.0856),(1.+-0.0856)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_TVR1boosted'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0394),(1.+-0.0394)], [(1.+-0.0512),(1.+-0.0512)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_TVR2boosted'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0038),(1.+-0.0038)], [(1.+-0.1318),(1.+-0.1318)], 'user', 'userHistoSys')

tthSystematics['tthRenormFactor_SRLMboostedWZdisc'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0246),(1.+-0.0246)], [(1.+-0.0546),(1.+-0.0546)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRMMboostedWZdisc'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0246),(1.+-0.0246)], [(1.+-0.0546),(1.+-0.0546)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRHMboostedWZdisc'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0246),(1.+-0.0246)], [(1.+-0.0546),(1.+-0.0546)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRLMboostedWWdisc'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0136),(1.+-0.0136)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRMMboostedWWdisc'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0136),(1.+-0.0136)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')
tthSystematics['tthRenormFactor_SRHMboostedWWdisc'] = Systematic('tthRenormFactor', configMgr.weights, [(1.+-0.0136),(1.+-0.0136)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')


tthSystematics['tthPDF_SRLMboostedWZ'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0800),(1.+0.0800)], [(1.-0.0800),(1.-0.0800)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRMMboostedWZ'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0800),(1.+0.0800)], [(1.-0.0800),(1.-0.0800)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRHMboostedWZ'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0800),(1.+0.0800)], [(1.-0.0800),(1.-0.0800)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRLMboostedWW'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0934),(1.+0.0934)], [(1.-0.0934),(1.-0.0934)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRMMboostedWW'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0934),(1.+0.0934)], [(1.-0.0934),(1.-0.0934)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRHMboostedWW'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0934),(1.+0.0934)], [(1.-0.0934),(1.-0.0934)], 'user', 'userHistoSys')
tthSystematics['tthPDF_DB2LCR'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0692)], [(1.-0.0692)], 'user', 'userHistoSys')
tthSystematics['tthPDF_DB2LVR'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0875)], [(1.-0.0875)], 'user', 'userHistoSys')
tthSystematics['tthPDF_WDB1LCRboosted'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0988),(1.+0.0988)], [(1.-0.0988),(1.-0.0988)], 'user', 'userHistoSys')
tthSystematics['tthPDF_WDB1LVR1boosted'] = Systematic('tthPDF', configMgr.weights, [(1.+0.1031),(1.+0.1031)], [(1.-0.1031),(1.-0.1031)], 'user', 'userHistoSys')
tthSystematics['tthPDF_WDB1LVR2boosted'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0915),(1.+0.0915)], [(1.-0.0915),(1.-0.0915)], 'user', 'userHistoSys')
tthSystematics['tthPDF_TCRboosted'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0783),(1.+0.0783)], [(1.-0.0783),(1.-0.0783)], 'user', 'userHistoSys')
tthSystematics['tthPDF_TVR1boosted'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0867),(1.+0.0867)], [(1.-0.0867),(1.-0.0867)], 'user', 'userHistoSys')
tthSystematics['tthPDF_TVR2boosted'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0745),(1.+0.0745)], [(1.-0.0745),(1.-0.0745)], 'user', 'userHistoSys')

tthSystematics['tthPDF_SRLMboostedWZdisc'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0800),(1.+0.0800)], [(1.-0.0800),(1.-0.0800)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRMMboostedWZdisc'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0800),(1.+0.0800)], [(1.-0.0800),(1.-0.0800)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRHMboostedWZdisc'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0800),(1.+0.0800)], [(1.-0.0800),(1.-0.0800)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRLMboostedWWdisc'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0934),(1.+0.0934)], [(1.-0.0934),(1.-0.0934)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRMMboostedWWdisc'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0934),(1.+0.0934)], [(1.-0.0934),(1.-0.0934)], 'user', 'userHistoSys')
tthSystematics['tthPDF_SRHMboostedWWdisc'] = Systematic('tthPDF', configMgr.weights, [(1.+0.0934),(1.+0.0934)], [(1.-0.0934),(1.-0.0934)], 'user', 'userHistoSys')


tthSystematics['tthISR_SRLMboostedWZ'] = Systematic('tthISR', configMgr.weights, [(1.+0.0271),(1.+0.0271)], [(1.+-0.0176),(1.+-0.0176)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRMMboostedWZ'] = Systematic('tthISR', configMgr.weights, [(1.+0.0271),(1.+0.0271)], [(1.+-0.0176),(1.+-0.0176)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRHMboostedWZ'] = Systematic('tthISR', configMgr.weights, [(1.+0.0271),(1.+0.0271)], [(1.+-0.0176),(1.+-0.0176)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRLMboostedWW'] = Systematic('tthISR', configMgr.weights, [(1.+0.0390),(1.+0.0390)], [(1.+-0.0128),(1.+-0.0128)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRMMboostedWW'] = Systematic('tthISR', configMgr.weights, [(1.+0.0390),(1.+0.0390)], [(1.+-0.0128),(1.+-0.0128)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRHMboostedWW'] = Systematic('tthISR', configMgr.weights, [(1.+0.0390),(1.+0.0390)], [(1.+-0.0128),(1.+-0.0128)], 'user', 'userHistoSys')
tthSystematics['tthISR_DB2LCR'] = Systematic('tthISR', configMgr.weights, [(1.+0.0085)], [(1.+-0.0087)], 'user', 'userHistoSys')
tthSystematics['tthISR_DB2LVR'] = Systematic('tthISR', configMgr.weights, [(1.+-0.0011)], [(1.+-0.0032)], 'user', 'userHistoSys')
tthSystematics['tthISR_WDB1LCRboosted'] = Systematic('tthISR', configMgr.weights, [(1.+0.0004),(1.+0.0004)], [(1.+-0.0004),(1.+-0.0004)], 'user', 'userHistoSys')
tthSystematics['tthISR_WDB1LVR1boosted'] = Systematic('tthISR', configMgr.weights, [(1.+0.0142),(1.+0.0142)], [(1.+-0.0168),(1.+-0.0168)], 'user', 'userHistoSys')
tthSystematics['tthISR_WDB1LVR2boosted'] = Systematic('tthISR', configMgr.weights, [(1.+0.0780),(1.+0.0780)], [(1.+-0.0790),(1.+-0.0790)], 'user', 'userHistoSys')
tthSystematics['tthISR_TCRboosted'] = Systematic('tthISR', configMgr.weights, [(1.+0.0063),(1.+0.0063)], [(1.+-0.0069),(1.+-0.0069)], 'user', 'userHistoSys')
tthSystematics['tthISR_TVR1boosted'] = Systematic('tthISR', configMgr.weights, [(1.+0.0083),(1.+0.0083)], [(1.+-0.0063),(1.+-0.0063)], 'user', 'userHistoSys')
tthSystematics['tthISR_TVR2boosted'] = Systematic('tthISR', configMgr.weights, [(1.+0.0227),(1.+0.0227)], [(1.+-0.0179),(1.+-0.0179)], 'user', 'userHistoSys')

tthSystematics['tthISR_SRLMboostedWZdisc'] = Systematic('tthISR', configMgr.weights, [(1.+0.0271),(1.+0.0271)], [(1.+-0.0176),(1.+-0.0176)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRMMboostedWZdisc'] = Systematic('tthISR', configMgr.weights, [(1.+0.0271),(1.+0.0271)], [(1.+-0.0176),(1.+-0.0176)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRHMboostedWZdisc'] = Systematic('tthISR', configMgr.weights, [(1.+0.0271),(1.+0.0271)], [(1.+-0.0176),(1.+-0.0176)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRLMboostedWW'] = Systematic('tthISR', configMgr.weights, [(1.+0.0390),(1.+0.0390)], [(1.+-0.0128),(1.+-0.0128)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRMMboostedWW'] = Systematic('tthISR', configMgr.weights, [(1.+0.0390),(1.+0.0390)], [(1.+-0.0128),(1.+-0.0128)], 'user', 'userHistoSys')
tthSystematics['tthISR_SRHMboostedWW'] = Systematic('tthISR', configMgr.weights, [(1.+0.0390),(1.+0.0390)], [(1.+-0.0128),(1.+-0.0128)], 'user', 'userHistoSys')


tthSystematics['tthFSR_SRLMboostedWZ'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1998),(1.+0.1998)], [(1.+-0.1038),(1.+-0.1038)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRMMboostedWZ'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1998),(1.+0.1998)], [(1.+-0.1038),(1.+-0.1038)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRHMboostedWZ'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1998),(1.+0.1998)], [(1.+-0.1038),(1.+-0.1038)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRLMboostedWW'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0145),(1.+-0.0145)], [(1.+-0.2218),(1.+-0.2218)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRMMboostedWW'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0145),(1.+-0.0145)], [(1.+-0.2218),(1.+-0.2218)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRHMboostedWW'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0145),(1.+-0.0145)], [(1.+-0.2218),(1.+-0.2218)], 'user', 'userHistoSys')
tthSystematics['tthFSR_DB2LCR'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1083)], [(1.+-0.0526)], 'user', 'userHistoSys')
tthSystematics['tthFSR_DB2LVR'] = Systematic('tthFSR', configMgr.weights, [(1.+0.4878)], [(1.+-0.1112)], 'user', 'userHistoSys')
tthSystematics['tthFSR_WDB1LCRboosted'] = Systematic('tthFSR', configMgr.weights, [(1.+0.0481),(1.+0.0481)], [(1.+-0.0891),(1.+-0.0891)], 'user', 'userHistoSys')
tthSystematics['tthFSR_WDB1LVR1boosted'] = Systematic('tthFSR', configMgr.weights, [(1.+0.2601),(1.+0.2601)], [(1.+-0.0471),(1.+-0.0471)], 'user', 'userHistoSys')
tthSystematics['tthFSR_WDB1LVR2boosted'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.1632),(1.+-0.1632)], [(1.+-0.1639),(1.+-0.1632)], 'user', 'userHistoSys')
tthSystematics['tthFSR_TCRboosted'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0266),(1.+-0.0266)], [(1.+-0.0648),(1.+-0.0648)], 'user', 'userHistoSys')
tthSystematics['tthFSR_TVR1boosted'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0095),(1.+-0.0095)], [(1.+-0.0647),(1.+-0.0647)], 'user', 'userHistoSys')
tthSystematics['tthFSR_TVR2boosted'] = Systematic('tthFSR', configMgr.weights, [(1.+0.0150),(1.+0.0150)], [(1.+-0.1293),(1.+-0.1293)], 'user', 'userHistoSys')

tthSystematics['tthFSR_SRLMboostedWZdisc'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1998),(1.+0.1998)], [(1.+-0.1038),(1.+-0.1038)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRMMboostedWZdisc'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1998),(1.+0.1998)], [(1.+-0.1038),(1.+-0.1038)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRHMboostedWZdisc'] = Systematic('tthFSR', configMgr.weights, [(1.+0.1998),(1.+0.1998)], [(1.+-0.1038),(1.+-0.1038)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRLMboostedWWdisc'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0145),(1.+-0.0145)], [(1.+-0.2218),(1.+-0.2218)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRMMboostedWWdisc'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0145),(1.+-0.0145)], [(1.+-0.2218),(1.+-0.2218)], 'user', 'userHistoSys')
tthSystematics['tthFSR_SRHMboostedWWdisc'] = Systematic('tthFSR', configMgr.weights, [(1.+-0.0145),(1.+-0.0145)], [(1.+-0.2218),(1.+-0.2218)], 'user', 'userHistoSys')


def TheorUnc(generatorSyst):
	for key in tthSystematics:
		name = key.split('_')[-1]
                
		generatorSyst.append((('tth', name+'EM'), tthSystematics[key]))
		generatorSyst.append((('tth', name+'El'), tthSystematics[key]))
		generatorSyst.append((('tth', name+'Mu'), tthSystematics[key]))
