import ROOT
from ROOT import gSystem
gSystem.Load('libSusyFitter.so')

from systematic import Systematic
from configManager import configMgr

vhSystematics = {}

vhSystematics['vhRenorm_SRLMboostedWZ'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0664),(1.+0.0664)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRMMboostedWZ'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0664),(1.+0.0664)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRHMboostedWZ'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0664),(1.+0.0664)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRLMboostedWW'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0660),(1.+0.0660)], [(1.+-0.0506),(1.+-0.0506)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRMMboostedWW'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0660),(1.+0.0660)], [(1.+-0.0506),(1.+-0.0506)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRHMboostedWW'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0660),(1.+0.0660)], [(1.+-0.0506),(1.+-0.0506)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_DB2LCR'] = Systematic('vhRenorm', configMgr.weights, [(1.+-0.0451)], [(1.+-0.0555)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_DB2LVR'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0449)], [(1.+-0.0477)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_WDB1LCRboosted'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0114),(1.+0.0114)], [(1.+-0.0164),(1.+-0.0164)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_WDB1LVR1boosted'] = Systematic('vhRenorm', configMgr.weights, [(1.+-0.0079),(1.+-0.0079)], [(1.+-0.0205),(1.+-0.0205)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_WDB1LVR2boosted'] = Systematic('vhRenorm', configMgr.weights, [(1.+-0.0133),(1.+-0.0133)], [(1.+-0.0169),(1.+-0.0169)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_TCRboosted'] = Systematic('vhRenorm', configMgr.weights, [(1.+-0.0053),(1.+-0.0053)], [(1.+-0.0247),(1.+-0.0247)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_TVR1boosted'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0336),(1.+0.0336)], [(1.+-0.0243),(1.+-0.0243)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_TVR2boosted'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0015),(1.+0.0015)], [(1.+-0.0283),(1.+-0.0283)], 'user', 'userHistoSys')


vhSystematics['vhRenorm_SRLMboostedWZdisc'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0664),(1.+0.0664)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRMMboostedWZdisc'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0664),(1.+0.0664)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRHMboostedWZdisc'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0664),(1.+0.0664)], [(1.+-0.0496),(1.+-0.0496)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRLMboostedWWdisc'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0660),(1.+0.0660)], [(1.+-0.0506),(1.+-0.0506)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRMMboostedWWdisc'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0660),(1.+0.0660)], [(1.+-0.0506),(1.+-0.0506)], 'user', 'userHistoSys')
vhSystematics['vhRenorm_SRHMboostedWWdisc'] = Systematic('vhRenorm', configMgr.weights, [(1.+0.0660),(1.+0.0660)], [(1.+-0.0506),(1.+-0.0506)], 'user', 'userHistoSys')


vhSystematics['vhFactor_SRLMboostedWZ'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0240),(1.+0.0240)], [(1.+-0.0193),(1.+-0.0193)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRMMboostedWZ'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0240),(1.+0.0240)], [(1.+-0.0193),(1.+-0.0193)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRHMboostedWZ'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0240),(1.+0.0240)], [(1.+-0.0193),(1.+-0.0193)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRLMboostedWW'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0264),(1.+0.0264)], [(1.+-0.0215),(1.+-0.0215)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRMMboostedWW'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0264),(1.+0.0264)], [(1.+-0.0215),(1.+-0.0215)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRHMboostedWW'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0264),(1.+0.0264)], [(1.+-0.0215),(1.+-0.0215)], 'user', 'userHistoSys')
vhSystematics['vhFactor_DB2LCR'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0163)], [(1.+-0.0159)], 'user', 'userHistoSys')
vhSystematics['vhFactor_DB2LVR'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0192)], [(1.+-0.0139)], 'user', 'userHistoSys')
vhSystematics['vhFactor_WDB1LCRboosted'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0111),(1.+0.0111)], [(1.+-0.0039),(1.+-0.0039)], 'user', 'userHistoSys')
vhSystematics['vhFactor_WDB1LVR1boosted'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0136),(1.+0.0136)], [(1.+-0.0073),(1.+-0.0073)], 'user', 'userHistoSys')
vhSystematics['vhFactor_WDB1LVR2boosted'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0228),(1.+0.0228)], [(1.+-0.0146),(1.+-0.0146)], 'user', 'userHistoSys')
vhSystematics['vhFactor_TCRboosted'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0104),(1.+0.0104)], [(1.+-0.0033),(1.+-0.0033)], 'user', 'userHistoSys')
vhSystematics['vhFactor_TVR1boosted'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0123),(1.+0.0123)], [(1.+-0.0060),(1.+-0.0060)], 'user', 'userHistoSys')
vhSystematics['vhFactor_TVR2boosted'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0187),(1.+0.0187)], [(1.+-0.0086),(1.+-0.0086)], 'user', 'userHistoSys')


vhSystematics['vhFactor_SRLMboostedWZdisc'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0240),(1.+0.0240)], [(1.+-0.0193),(1.+-0.0193)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRMMboostedWZdisc'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0240),(1.+0.0240)], [(1.+-0.0193),(1.+-0.0193)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRHMboostedWZdisc'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0240),(1.+0.0240)], [(1.+-0.0193),(1.+-0.0193)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRLMboostedWWdisc'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0264),(1.+0.0264)], [(1.+-0.0215),(1.+-0.0215)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRMMboostedWWdisc'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0264),(1.+0.0264)], [(1.+-0.0215),(1.+-0.0215)], 'user', 'userHistoSys')
vhSystematics['vhFactor_SRHMboostedWWdisc'] = Systematic('vhFactor', configMgr.weights, [(1.+0.0264),(1.+0.0264)], [(1.+-0.0215),(1.+-0.0215)], 'user', 'userHistoSys')


vhSystematics['vhRenormFactor_SRLMboostedWZ'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0824),(1.+0.0824)], [(1.+-0.0724),(1.+-0.0724)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRMMboostedWZ'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0824),(1.+0.0824)], [(1.+-0.0724),(1.+-0.0724)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRHMboostedWZ'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0824),(1.+0.0824)], [(1.+-0.0724),(1.+-0.0724)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRLMboostedWW'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0844),(1.+0.0844)], [(1.+-0.0754),(1.+-0.0754)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRMMboostedWW'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0844),(1.+0.0844)], [(1.+-0.0754),(1.+-0.0754)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRHMboostedWW'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0844),(1.+0.0844)], [(1.+-0.0754),(1.+-0.0754)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_DB2LCR'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+-0.0543)], [(1.+-0.0651)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_DB2LVR'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0641)], [(1.+-0.0627)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_WDB1LCRboosted'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0229),(1.+0.0229)], [(1.+-0.0237),(1.+-0.0237)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_WDB1LVR1boosted'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0055),(1.+0.0055)], [(1.+-0.0313),(1.+-0.0313)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_WDB1LVR2boosted'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+-0.0053),(1.+-0.0053)], [(1.+-0.0379),(1.+-0.0379)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_TCRboosted'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+-0.0127),(1.+-0.0127)], [(1.+-0.0161),(1.+-0.0161)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_TVR1boosted'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0458),(1.+0.0458)], [(1.+-0.0333),(1.+-0.0333)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_TVR2boosted'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+-0.0161),(1.+-0.0161)], [(1.+-0.0260),(1.+-0.0260)], 'user', 'userHistoSys')


vhSystematics['vhRenormFactor_SRLMboostedWZdisc'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0824),(1.+0.0824)], [(1.+-0.0724),(1.+-0.0724)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRMMboostedWZdisc'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0824),(1.+0.0824)], [(1.+-0.0724),(1.+-0.0724)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRHMboostedWZdisc'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0824),(1.+0.0824)], [(1.+-0.0724),(1.+-0.0724)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRLMboostedWWdisc'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0844),(1.+0.0844)], [(1.+-0.0754),(1.+-0.0754)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRMMboostedWWdisc'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0844),(1.+0.0844)], [(1.+-0.0754),(1.+-0.0754)], 'user', 'userHistoSys')
vhSystematics['vhRenormFactor_SRHMboostedWWdisc'] = Systematic('vhRenormFactor', configMgr.weights, [(1.+0.0844),(1.+0.0844)], [(1.+-0.0754),(1.+-0.0754)], 'user', 'userHistoSys')


vhSystematics['vhPDF_SRLMboostedWZ'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.-0.0343),(1.-0.0343)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRMMboostedWZ'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.-0.0343),(1.-0.0343)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRHMboostedWZ'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.-0.0343),(1.-0.0343)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRLMboostedWW'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0352),(1.+0.0352)], [(1.-0.0352),(1.-0.0352)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRMMboostedWW'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0352),(1.+0.0352)], [(1.-0.0352),(1.-0.0352)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRHMboostedWW'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0352),(1.+0.0352)], [(1.-0.0352),(1.-0.0352)], 'user', 'userHistoSys')
vhSystematics['vhPDF_DB2LCR'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0409)], [(1.-0.0409)], 'user', 'userHistoSys')
vhSystematics['vhPDF_DB2LVR'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0373)], [(1.-0.0373)], 'user', 'userHistoSys')
vhSystematics['vhPDF_WDB1LCRboosted'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0439),(1.+0.0439)], [(1.-0.0439),(1.-0.0439)], 'user', 'userHistoSys')
vhSystematics['vhPDF_WDB1LVR1boosted'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0523),(1.+0.0523)], [(1.-0.0523),(1.-0.0523)], 'user', 'userHistoSys')
vhSystematics['vhPDF_WDB1LVR2boosted'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0521),(1.+0.0521)], [(1.-0.0521),(1.-0.0521)], 'user', 'userHistoSys')
vhSystematics['vhPDF_TCRboosted'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0467),(1.+0.0467)], [(1.-0.0467),(1.-0.0467)], 'user', 'userHistoSys')
vhSystematics['vhPDF_TVR1boosted'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0453),(1.+0.0453)], [(1.-0.0453),(1.-0.0453)], 'user', 'userHistoSys')
vhSystematics['vhPDF_TVR2boosted'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0676),(1.+0.0676)], [(1.-0.0676),(1.-0.0676)], 'user', 'userHistoSys')


vhSystematics['vhPDF_SRLMboostedWZdisc'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.-0.0343),(1.-0.0343)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRMMboostedWZdisc'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.-0.0343),(1.-0.0343)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRHMboostedWZdisc'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0343),(1.+0.0343)], [(1.-0.0343),(1.-0.0343)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRLMboostedWWdisc'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0352),(1.+0.0352)], [(1.-0.0352),(1.-0.0352)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRMMboostedWWdisc'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0352),(1.+0.0352)], [(1.-0.0352),(1.-0.0352)], 'user', 'userHistoSys')
vhSystematics['vhPDF_SRHMboostedWWdisc'] = Systematic('vhPDF', configMgr.weights, [(1.+0.0352),(1.+0.0352)], [(1.-0.0352),(1.-0.0352)], 'user', 'userHistoSys')

def TheorUnc(generatorSyst):
	for key in vhSystematics:
		name = key.split('_')[-1]

		generatorSyst.append((('vh', name+'EM'), vhSystematics[key]))
		generatorSyst.append((('vh', name+'El'), vhSystematics[key]))
		generatorSyst.append((('vh', name+'Mu'), vhSystematics[key]))
