import ROOT
from ROOT import gSystem
gSystem.Load('libSusyFitter.so')

from systematic import Systematic
from configManager import configMgr

wjetsSystematics = {}

wjetsSystematics['wjetsRenorm_SRLMboostedWZ'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0282),(1.+0.0282)], [(1.+-0.0229),(1.+-0.0229)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRMMboostedWZ'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0282),(1.+0.0282)], [(1.+-0.0229),(1.+-0.0229)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRHMboostedWZ'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0282),(1.+0.0282)], [(1.+-0.0229),(1.+-0.0229)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRLMboostedWW'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0309),(1.+0.0309)], [(1.+-0.0279),(1.+-0.0279)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRMMboostedWW'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0309),(1.+0.0309)], [(1.+-0.0279),(1.+-0.0279)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRHMboostedWW'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0309),(1.+0.0309)], [(1.+-0.0279),(1.+-0.0279)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_DB2LCR'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.1103)], [(1.+-0.0385)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_DB2LVR'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.2639)], [(1.+-0.1751)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_WDB1LCRboosted'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_WDB1LVR1boosted'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0308),(1.+0.0308)], [(1.+-0.0103),(1.+-0.0103)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_WDB1LVR2boosted'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0221),(1.+0.0221)], [(1.+-0.0165),(1.+-0.0165)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_TCRboosted'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0342),(1.+0.0342)], [(1.+-0.0154),(1.+-0.0154)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_TVR1boosted'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0743),(1.+0.0743)], [(1.+-0.0418),(1.+-0.0418)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_TVR2boosted'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0582),(1.+0.0582)], [(1.+-0.0331),(1.+-0.0331)], 'user', 'userHistoSys')


wjetsSystematics['wjetsRenorm_SRLMboostedWZdisc'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0282),(1.+0.0282)], [(1.+-0.0229),(1.+-0.0229)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRMMboostedWZdisc'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0282),(1.+0.0282)], [(1.+-0.0229),(1.+-0.0229)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRHMboostedWZdisc'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0282),(1.+0.0282)], [(1.+-0.0229),(1.+-0.0229)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRLMboostedWWdisc'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0309),(1.+0.0309)], [(1.+-0.0279),(1.+-0.0279)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRMMboostedWWdisc'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0309),(1.+0.0309)], [(1.+-0.0279),(1.+-0.0279)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenorm_SRHMboostedWWdisc'] = Systematic('wjetsRenorm', configMgr.weights, [(1.+0.0309),(1.+0.0309)], [(1.+-0.0279),(1.+-0.0279)], 'user', 'userHistoSys')



wjetsSystematics['wjetsFactor_SRLMboostedWZ'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0048),(1.+0.0048)], [(1.+-0.0088),(1.+-0.0088)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRMMboostedWZ'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0048),(1.+0.0048)], [(1.+-0.0088),(1.+-0.0088)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRHMboostedWZ'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0048),(1.+0.0048)], [(1.+-0.0088),(1.+-0.0088)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRLMboostedWW'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0013),(1.+0.0013)], [(1.+-0.0019),(1.+-0.0019)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRMMboostedWW'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0013),(1.+0.0013)], [(1.+-0.0019),(1.+-0.0019)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRHMboostedWW'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0013),(1.+0.0013)], [(1.+-0.0019),(1.+-0.0019)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_DB2LCR'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0099)], [(1.+-0.0115)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_DB2LVR'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0140)], [(1.+-0.0218)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_WDB1LCRboosted'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_WDB1LVR1boosted'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0007),(1.+0.0007)], [(1.+-0.0038),(1.+-0.0038)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_WDB1LVR2boosted'] = Systematic('wjetsFactor', configMgr.weights, [(1.+-0.0006),(1.+-0.0006)], [(1.+-0.0036),(1.+-0.0036)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_TCRboosted'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0009),(1.+0.0009)], [(1.+-0.0075),(1.+-0.0075)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_TVR1boosted'] = Systematic('wjetsFactor', configMgr.weights, [(1.+-0.0014),(1.+-0.0014)], [(1.+-0.0023),(1.+-0.0023)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_TVR2boosted'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0047),(1.+0.0047)], [(1.+-0.0107),(1.+-0.0107)], 'user', 'userHistoSys')


wjetsSystematics['wjetsFactor_SRLMboostedWZdisc'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0048),(1.+0.0048)], [(1.+-0.0088),(1.+-0.0088)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRMMboostedWZdisc'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0048),(1.+0.0048)], [(1.+-0.0088),(1.+-0.0088)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRHMboostedWZdisc'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0048),(1.+0.0048)], [(1.+-0.0088),(1.+-0.0088)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRLMboostedWWdisc'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0013),(1.+0.0013)], [(1.+-0.0019),(1.+-0.0019)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRMMboostedWWdisc'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0013),(1.+0.0013)], [(1.+-0.0019),(1.+-0.0019)], 'user', 'userHistoSys')
wjetsSystematics['wjetsFactor_SRHMboostedWWdisc'] = Systematic('wjetsFactor', configMgr.weights, [(1.+0.0013),(1.+0.0013)], [(1.+-0.0019),(1.+-0.0019)], 'user', 'userHistoSys')



wjetsSystematics['wjetsRenormFactor_SRLMboostedWZ'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0297),(1.+0.0297)], [(1.+-0.0308),(1.+-0.0308)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRMMboostedWZ'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0297),(1.+0.0297)], [(1.+-0.0308),(1.+-0.0308)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRHMboostedWZ'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0297),(1.+0.0297)], [(1.+-0.0308),(1.+-0.0308)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRLMboostedWW'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0252),(1.+0.0252)], [(1.+-0.0268),(1.+-0.0268)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRMMboostedWW'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0252),(1.+0.0252)], [(1.+-0.0268),(1.+-0.0268)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRHMboostedWW'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0252),(1.+0.0252)], [(1.+-0.0268),(1.+-0.0268)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_DB2LCR'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0742)], [(1.+-0.0511)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_DB2LVR'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.2908)], [(1.+-0.1917)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_WDB1LCRboosted'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_WDB1LVR1boosted'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0275),(1.+0.0275)], [(1.+-0.0111),(1.+-0.0111)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_WDB1LVR2boosted'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0219),(1.+0.0219)], [(1.+-0.0207),(1.+-0.0207)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_TCRboosted'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0337),(1.+0.0337)], [(1.+-0.0161),(1.+-0.0161)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_TVR1boosted'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0769),(1.+0.0769)], [(1.+-0.0440),(1.+-0.0440)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_TVR2boosted'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0391),(1.+0.0391)], [(1.+-0.0321),(1.+-0.0321)], 'user', 'userHistoSys')


wjetsSystematics['wjetsRenormFactor_SRLMboostedWZdisc'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0297),(1.+0.0297)], [(1.+-0.0308),(1.+-0.0308)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRMMboostedWZdisc'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0297),(1.+0.0297)], [(1.+-0.0308),(1.+-0.0308)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRHMboostedWZdisc'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0297),(1.+0.0297)], [(1.+-0.0308),(1.+-0.0308)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRLMboostedWWdisc'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0252),(1.+0.0252)], [(1.+-0.0268),(1.+-0.0268)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRMMboostedWWdisc'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0252),(1.+0.0252)], [(1.+-0.0268),(1.+-0.0268)], 'user', 'userHistoSys')
wjetsSystematics['wjetsRenormFactor_SRHMboostedWWdisc'] = Systematic('wjetsRenormFactor', configMgr.weights, [(1.+0.0252),(1.+0.0252)], [(1.+-0.0268),(1.+-0.0268)], 'user', 'userHistoSys')


wjetsSystematics['wjetsPDF_SRLMboostedWZ'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0414),(1.+0.0414)], [(1.-0.0414),(1.-0.0414)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRMMboostedWZ'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0414),(1.+0.0414)], [(1.-0.0414),(1.-0.0414)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRHMboostedWZ'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0414),(1.+0.0414)], [(1.-0.0414),(1.-0.0414)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRLMboostedWW'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0273),(1.+0.0273)], [(1.-0.0273),(1.-0.0273)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRMMboostedWW'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0273),(1.+0.0273)], [(1.-0.0273),(1.-0.0273)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRHMboostedWW'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0273),(1.+0.0273)], [(1.-0.0273),(1.-0.0273)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_DB2LCR'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0454)], [(1.-0.0454)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_DB2LVR'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0622)], [(1.-0.0622)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_WDB1LCRboosted'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.-0.0000),(1.-0.0000)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_WDB1LVR1boosted'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0188),(1.+0.0188)], [(1.-0.0188),(1.-0.0188)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_WDB1LVR2boosted'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0212),(1.+0.0212)], [(1.-0.0212),(1.-0.0212)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_TCRboosted'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0184),(1.+0.0184)], [(1.-0.0184),(1.-0.0184)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_TVR1boosted'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0142),(1.+0.0142)], [(1.-0.0142),(1.-0.0142)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_TVR2boosted'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0184),(1.+0.0184)], [(1.-0.0184),(1.-0.0184)], 'user', 'userHistoSys')

wjetsSystematics['wjetsPDF_SRLMboostedWZdisc'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0414),(1.+0.0414)], [(1.-0.0414),(1.-0.0414)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRMMboostedWZdisc'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0414),(1.+0.0414)], [(1.-0.0414),(1.-0.0414)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRHMboostedWZdisc'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0414),(1.+0.0414)], [(1.-0.0414),(1.-0.0414)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRLMboostedWWdisc'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0273),(1.+0.0273)], [(1.-0.0273),(1.-0.0273)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRMMboostedWWdisc'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0273),(1.+0.0273)], [(1.-0.0273),(1.-0.0273)], 'user', 'userHistoSys')
wjetsSystematics['wjetsPDF_SRHMboostedWWdisc'] = Systematic('wjetsPDF', configMgr.weights, [(1.+0.0273),(1.+0.0273)], [(1.-0.0273),(1.-0.0273)], 'user', 'userHistoSys')


wjetsSystematics['wjetsCKKW_SRLMboostedWZ'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0036),(1.+0.0036)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRMMboostedWZ'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0036),(1.+0.0036)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRHMboostedWZ'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0036),(1.+0.0036)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRLMboostedWW'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0402),(1.+0.0402)], [(1.+0.0141),(1.+0.0141)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRMMboostedWW'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0402),(1.+0.0402)], [(1.+0.0141),(1.+0.0141)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRHMboostedWW'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0402),(1.+0.0402)], [(1.+0.0141),(1.+0.0141)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_DB2LCR'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+-0.0299)], [(1.+-0.0762)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_DB2LVR'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0420)], [(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_WDB1LCRboosted'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_WDB1LVR1boosted'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+-0.0035),(1.+-0.0035)], [(1.+-0.0070),(1.+-0.0070)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_WDB1LVR2boosted'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0057),(1.+0.0057)], [(1.+0.0003),(1.+0.0003)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_TCRboosted'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+-0.0110),(1.+-0.0110)], [(1.+-0.0161),(1.+-0.0110)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_TVR1boosted'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+-0.0095),(1.+-0.0095)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_TVR2boosted'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+-0.0001),(1.+-0.0001)], [(1.+-0.0028),(1.+-0.0028)], 'user', 'userHistoSys')


wjetsSystematics['wjetsCKKW_SRLMboostedWZdisc'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0036),(1.+0.0036)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRMMboostedWZdisc'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0036),(1.+0.0036)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRHMboostedWZdisc'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0036),(1.+0.0036)], [(1.+-0.0194),(1.+-0.0194)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRLMboostedWWdisc'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0402),(1.+0.0402)], [(1.+0.0141),(1.+0.0141)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRMMboostedWWdisc'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0402),(1.+0.0402)], [(1.+0.0141),(1.+0.0141)], 'user', 'userHistoSys')
wjetsSystematics['wjetsCKKW_SRHMboostedWWdisc'] = Systematic('wjetsCKKW', configMgr.weights, [(1.+0.0402),(1.+0.0402)], [(1.+0.0141),(1.+0.0141)], 'user', 'userHistoSys')




wjetsSystematics['wjetsQSF_SRLMboostedWZ'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0071),(1.+-0.0071)], [(1.+-0.0149),(1.+-0.0149)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRMMboostedWZ'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0071),(1.+-0.0071)], [(1.+-0.0149),(1.+-0.0149)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRHMboostedWZ'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0071),(1.+-0.0071)], [(1.+-0.0149),(1.+-0.0149)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRLMboostedWW'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.+0.0327),(1.+0.0327)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRMMboostedWW'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.+0.0327),(1.+0.0327)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRHMboostedWW'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.+0.0327),(1.+0.0327)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_DB2LCR'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0493)], [(1.+-0.0751)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_DB2LVR'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.1150)], [(1.+-0.0024)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_WDB1LCRboosted'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0000),(1.+0.0000)], [(1.+0.0000),(1.+0.0000)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_WDB1LVR1boosted'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0050),(1.+-0.0050)], [(1.+-0.0051),(1.+-0.0051)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_WDB1LVR2boosted'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0134),(1.+0.0134)], [(1.+-0.0008),(1.+-0.0008)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_TCRboosted'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0132),(1.+-0.0132)], [(1.+-0.0207),(1.+-0.0207)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_TVR1boosted'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0114),(1.+-0.0114)], [(1.+-0.0232),(1.+-0.0232)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_TVR2boosted'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0097),(1.+0.0097)], [(1.+-0.0051),(1.+-0.0051)], 'user', 'userHistoSys')


wjetsSystematics['wjetsQSF_SRLMboostedWZdisc'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0071),(1.+-0.0071)], [(1.+-0.0149),(1.+-0.0149)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRMMboostedWZdisc'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0071),(1.+-0.0071)], [(1.+-0.0149),(1.+-0.0149)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRHMboostedWZdisc'] = Systematic('wjetsQSF', configMgr.weights, [(1.+-0.0071),(1.+-0.0071)], [(1.+-0.0149),(1.+-0.0149)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRLMboostedWWdisc'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.+0.0327),(1.+0.0327)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRMMboostedWWdisc'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.+0.0327),(1.+0.0327)], 'user', 'userHistoSys')
wjetsSystematics['wjetsQSF_SRHMboostedWWdisc'] = Systematic('wjetsQSF', configMgr.weights, [(1.+0.0423),(1.+0.0423)], [(1.+0.0327),(1.+0.0327)], 'user', 'userHistoSys')


def TheorUnc(generatorSyst):
	for key in wjetsSystematics:
		name = key.split('_')[-1]

		generatorSyst.append((('wjets', name+'EM'), wjetsSystematics[key]))
		generatorSyst.append((('wjets', name+'El'), wjetsSystematics[key]))
		generatorSyst.append((('wjets', name+'Mu'), wjetsSystematics[key]))
