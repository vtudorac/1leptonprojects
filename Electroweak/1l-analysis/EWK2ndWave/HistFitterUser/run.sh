#!/bin/bash

# Absolute path to this script
script=$(readlink -f "$0")
# Absolute path this script is in
my_path=$(dirname "$script")

# Analysis
ana=C1N2_WZ

#make sure that tables are activated!

HistFitter.py -t -w -r resolved,boosted -f -F bkg ${my_path}/python/EWK2ndWaveFit.py 2>&1 | tee logs/EWK2ndWaveFit_${ana}.log

# CRs
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TCRresolvedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_TCRresolvedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WDB1LCRresolvedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_WDB1LCRresolvedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TCRboostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_TCRboostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WDB1LCRboostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_WDB1LCRboostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c DB2LCREM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_DB2LCREM_bkgonly.tex

# SRs
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMresolvedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_SRLMresolvedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRHMresolvedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_SRHMresolvedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMboostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_SRLMboostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRMMboostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_SRMMboostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRHMboostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_SRHMboostedEM_bkgonly.tex

# VRs
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TVRresolvedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_TVRresolvedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WDB1LVRresolvedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_WDB1LVRresolvedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TVR1boostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_TVR1boostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TVR2boostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_TVR2boostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WDB1LVR1boostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_WDB1LVR1boostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WDB1LVR2boostedEM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_WDB1LVR2boostedEM_bkgonly.tex
YieldsTable.py -b -y -w results/EWK2ndWaveFit_${ana}_resolved_boosted_general/Validation_combined_BasicMeasurement_model_afterFit.root -c DB2LVREM -s ttbar,singletop,wjets,zjets,diboson2l,diboson1l,diboson0l,multiboson,tth,ttv,vh -t CR -o tables/MyTable_${ana}_DB2LVREM_bkgonly.tex
