"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtilsVRSR2 module. Adapt to create *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsVRSR2
from pullPlotUtilsVRSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    
    
    myRegionDict["SRLMboostedWZdiscEM"] = "SRLMboostedWZdisc"
    myRegionDict["SRMMboostedWZdiscEM"] = "SRMMboostedWZdisc"
    myRegionDict["SRHMboostedWZdiscEM"] = "SRHMboostedWZdisc"
    myRegionDict["SRLMboostedWWdiscEM"] = "SRLMboostedWWdisc"
    myRegionDict["SRMMboostedWWdiscEM"] = "SRMMboostedWWdisc"
    myRegionDict["SRHMboostedWWdiscEM"] = "SRHMboostedWWdisc"
    
   
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["SRLMboostedWZdisc_bin0","SRLMboostedWZdisc_bin1","SRMMboostedWZdisc_bin0", "SRMMboostedWZdisc_bin1","SRHMboostedWZdisc_bin0","SRHMboostedWZdisc_bin1","SRLMboostedWWdisc_bin0","SRLMboostedWWdisc_bin1","SRMMboostedWWdisc_bin0","SRMMboostedWWdisc_bin1","SRHMboostedWWdisc_bin0","SRHMboostedWWdisc_bin1"]
    
    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SR") != -1: return kBlue    
    elif name.find("VR") != -1: return kGreen    
    else: return kOrange       
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson2l" in sample:     return kViolet - 8
    if "diboson1l" in sample: return kViolet+8
    if "diboson0l" in sample: return kViolet-4
    if "multiboson" in sample:     return kViolet - 7
    if "singletop" in sample:     return kGreen + 2
    if "ttv" in sample:     return kOrange+1
    if "tth" in sample:     return kOrange+2
    if "vh" in sample:     return kOrange+3
    else:
        print ("cannot find color for sample (",sample,")")

    return 1

def main():
    # Override pullPlotUtilsSR2' default colours (which are all black)
    pullPlotUtilsVRSR2.getRegionColor = getRegionColor
    pullPlotUtilsVRSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename= "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/results/EWK2ndWaveFit_prodOct2021_C1N2_WZdisc_boosted_general_unblinding/" 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = ["/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/pickle/SRHMboostedWWdisc.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/pickle/SRMMboostedWWdisc.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/pickle/SRLMboostedWWdisc.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/pickle/SRLMboostedWZdisc.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/pickle/SRMMboostedWZdisc.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/unblinding/pickle/SRHMboostedWZdisc.pickle"]
   
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "EWK2ndWave_SR"
    # C1N2_WZdisc
    #region = "C1N2_WZdisc"
    # C1C1_WWdisc
    #region = "C1C1_WWdisc"

    # Samples to stack on top of eachother in each region
    samples = "multiboson,vh,zjets,ttv,tth,singletop,diboson0l,diboson1l,diboson2l,wjets,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print ("pickle filename %s does not exist" % mypickle)
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
