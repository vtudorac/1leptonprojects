"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtilsVRSR2 module. Adapt to create *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsVRSR2
from pullPlotUtilsVRSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["DB2LCREM"] = "DB2LCR"
    myRegionDict["WDB1LCRboostedEM"] = "WDB1LCRboosted"
    myRegionDict["TCRboostedEM"] = "TCRboosted"

    myRegionDict["DB2LVREM"] = "DB2LVR"
    myRegionDict["WDB1LVR1boostedEM"] = "WDB1LVR1boosted"
    myRegionDict["WDB1LVR2boostedEM"] = "WDB1LVR2boosted"
    myRegionDict["TVR1boostedEM"] = "TVR1boosted"
    myRegionDict["TVR2boostedEM"] = "TVR2boosted"
   
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["DB2LCR","WDB1LCRboosted_bin0","WDB1LCRboosted_bin1","TCRboosted_bin0","TCRboosted_bin1","DB2LVR","WDB1LVR1boosted_bin0","WDB1LVR1boosted_bin1","WDB1LVR2boosted_bin0","WDB1LVR2boosted_bin1","TVR1boosted_bin0","TVR1boosted_bin1","TVR2boosted_bin0","TVR2boosted_bin1"]
   
    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SR") != -1: return kBlue    
    elif name.find("VR") != -1: return kGreen    
    else: return kOrange       
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson2l" in sample:     return kViolet - 8
    if "diboson1l" in sample: return kViolet+8
    if "diboson0l" in sample: return kViolet-4
    if "multiboson" in sample:     return kViolet - 7
    if "singletop" in sample:     return kGreen + 2
    if "ttv" in sample:     return kOrange+1
    if "tth" in sample:     return kOrange+2
    if "vh" in sample:     return kOrange+3
    else:
        print ("cannot find color for sample (",sample,")")

    return 1

def main():
    # Override pullPlotUtilsSR2' default colours (which are all black)
    pullPlotUtilsVRSR2.getRegionColor = getRegionColor
    pullPlotUtilsVRSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename= "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/results/EWK2ndWaveFit_prodOct2021_C1C1_WW_boosted_general_NoSys_unblinding/C1N2WZ_sh221" 
    #wsfilename= "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/results/EWK2ndWaveFit_prodOct2021_C1C1_WW_boosted_general_NoSys_unblinding/C1C1WW_sh2211" 
    
    # Where's the pickle file? - define list of pickle files
    pickleFilename = ["/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/WDB1LCRboosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/TCRboosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/DB2LCR.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/TVR1boosted.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/TVR2boosted.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/WDB1LVR1boosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/WDB1LVR2boosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh221/DB2LVR.pickle"]
    #pickleFilename = ["/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/WDB1LCRboosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/TCRboosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/DB2LCR.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/TVR1boosted.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/TVR2boosted.pickle","/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/WDB1LVR1boosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/WDB1LVR2boosted.pickle", "/data/vtudorac/EWK2ndWave/HFv1.0.0/1l-analysis/EWK2ndWave/HistFitterUser/scripts/sh221_vs_sh2211/C1C1WW_sh2211/DB2LVR.pickle"]
    
    # C1N2_WZ
    #pickleFilename = ["/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/tables/YieldsTable_C1N2_WZ_CR_bkgonly.pickle", "/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/tables/YieldsTable_C1N2_WZ_VR_bkgonly.pickle", "/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/tables/YieldsTable_C1N2_WZ_SR_bkgonly.pickle"]
    # C1C1_WW
    #pickleFilename = ["/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/tables/YieldsTable_C1C1_WW_CR_bkgonly.pickle", "/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/tables/YieldsTable_C1C1_WW_VR_bkgonly.pickle", "/squark1/nkvu/Susy1LInclusive/WZ_study/analysis/HistFitterUser/tables/YieldsTable_C1C1_WW_SR_bkgonly.pickle"]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "EWK2ndWave_CR_VR_sh221"
    # C1N2_WZ
    #region = "C1N2_WZ"
    # C1C1_WW
    #region = "C1C1_WW"

    # Samples to stack on top of eachother in each region
    samples = "multiboson,vh,zjets,ttv,tth,singletop,diboson0l,diboson1l,diboson2l,wjets,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print ("pickle filename %s does not exist" % mypickle)
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
