"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 * Class  : PullPlotUtils                                                         *
 * Created: November 2012                                                         *
 *                                                                                *
 * Description:                                                                   *
 *      Functions to make pull plots                                              *
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

"""
A module to generate pull plots. Import and overwrite getSampleColor and getRegionColor to define your own plot. Call makePullPlot() to make the plot. The other functions are all Preliminary.
"""

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)
gROOT.LoadMacro("AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
gROOT.LoadMacro("AtlasStyle/AtlasLabels.C")

import os, string, pickle, copy
from math import sqrt

def getSampleColor(sample):
    """
    Get colour for a sample
    
    @param sample The sample to return the colour for
    """
    return 1

def getRegionColor(region):
    """
    Get colour for a region
    
    @param region The region to return the colour for
    """
    return 1

def PoissonError(obs):
    """
    Return the Poisson uncertainty on a number

    @param obs The number to calculate the uncertainty for
    """
    posError = TMath.ChisquareQuantile(1. - (1. - 0.68)/2. , 2.* (obs + 1.)) / 2. - obs - 1
    negError = obs - TMath.ChisquareQuantile((1. - 0.68)/2., 2.*obs) / 2
    symError = abs(posError-negError)/2.
    return (posError,negError,symError)

def MakeBox(color=4, offset=0, pull=-1, horizontal=False):
    graph = TGraph(4);
    if horizontal:
        graph.SetPoint(0,0.1+offset,0);
        graph.SetPoint(1,0.1+offset,pull);
        graph.SetPoint(2,0.9+offset,pull);
        graph.SetPoint(3,0.9+offset,0);    
    else:
        graph.SetPoint(0,0,0.3+offset);
        graph.SetPoint(1,pull,0.3+offset);
        graph.SetPoint(2,pull,0.7+offset);
        graph.SetPoint(3,0,0.7+offset);    
    graph.SetFillColor(color); 
    graph.SetLineColor(color);
    graph.SetLineWidth(1);
    graph.SetLineStyle(1);
    return graph

def GetFrame(outFileNamePrefix,Npar,horizontal=False):
    offset = 0.;
    if horizontal:
        frame =TH2F("frame"+outFileNamePrefix, "", 
                    Npar,0.,Npar,80,-4.,4.);
        frame.GetYaxis().SetTitleSize( 0.06 );
        frame.GetYaxis().SetTitleOffset(0.4)
        frame.GetYaxis().SetRangeUser( -3.5, 6.5 );
        frame.GetXaxis().SetLabelOffset( 0.05 );
        frame.GetXaxis().SetLabelSize( 0.08 );
        frame.GetYaxis().SetLabelSize( 0.06 );
        if '2J' in outFileNamePrefix: 
            frame.GetXaxis().SetLabelSize( 0.11 );
            frame.GetYaxis().SetLabelSize( 0.11 );
        elif '4J' in outFileNamePrefix or '6J' in outFileNamePrefix: 
            frame.GetXaxis().SetLabelSize( 0.095 );
            frame.GetYaxis().SetLabelSize( 0.11 );            
        frame.GetYaxis().SetNdivisions(4)
	  
        frame.SetYTitle("(n_{obs}- n_{pred}) / #sigma_{tot}");
    else:
        frame =TH2F( "frame"+outFileNamePrefix, outFileNamePrefix, 
                                1, -3.5, 3.5, 
                                Npar, -offset, Npar+offset );
        
        scale = 1.0;
        frame.SetLabelOffset( 0.012, "Y" );# label offset on x axis
        frame.GetYaxis().SetTitleOffset( 1.25 );
        frame.GetXaxis().SetTitleSize( 0.06 );
        frame.GetYaxis().SetTitleSize( 0.06 );
        frame.GetXaxis().SetLabelSize( 0.06 );
        frame.GetYaxis().SetLabelSize( 0.07 );
        Npar = len(regionList)
    
        frame.SetLineColor(0);
        frame.SetTickLength(0,"Y");
        frame.SetXTitle( "(n_{obs} - n_{pred}) / #sigma_{tot}          " );
        frame.SetLabelOffset( 0.001, "X" );
        frame.SetTitleOffset( 1. , "X");
        frame.SetTitleSize( 0.06, "X" );
        frame.GetYaxis().CenterLabels( 1 );
        frame.GetYaxis().SetNdivisions( frame.GetNbinsY()+10, 1 );
    
        # global style settings
        gPad.SetTicks();
        frame.SetLabelFont(42,"X");
        frame.SetTitleFont(42,"X");
        frame.SetLabelFont(42,"Y");
        frame.SetTitleFont(42,"Y");
    return copy.deepcopy(frame)

def GetBoxes(all, results, renamedRegions, frame, doBlind, horizontal=False):
    counter = 0
    myr = reversed(results)
    
    if horizontal:
        myr = results
    for info in myr:
        name = info[0].replace(" ","")
        name = info[0].replace("_cuts","")

        if name in renamedRegions.keys():
            name = renamedRegions[name]
            #print (name)
        
        frame.LabelsOption("v","x")            
        #if (name.find("SR") >= 0 and doBlind) or name.find("CR") >= 0 :
            #counter += 1
            #continue
            
        if horizontal:
            for bin in range(1,frame.GetNbinsX()+2):
                if frame.GetXaxis().GetBinLabel(bin) != name: continue
                counter = bin - 1
                break
            frame.GetXaxis().SetBinLabel(counter+1,name)
            #print (counter+1,name)
        else:
            frame.GetYaxis().SetBinLabel(counter+1,name)
            
        frame.LabelsOption("v","x")    
        
        color = getRegionColor(name) 
        #print (info[1])
        graph = MakeBox(offset=counter, pull=float(info[1]), color=color, horizontal=horizontal)
        graph.Draw("LF")
        
        counter += 1
        all.append(graph)
         
    return

def MakeHist(regionList, renamedRegions, results, hdata, hbkg, hbkgUp, hbkgDown, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents):
    max = 0
    min = 99999999999.
    for counter in range(len(regionList)):#loop over all the regions
        nObs = 0
        nExp = 0
        nExpEr = 0
        nExpTotEr = 0
        nExpStatEr = 0
        nExpStatErUp = 0
        nExpStatErDo = 0
        nExpTotErUp = 0
        nExpTotErDo = 0
        pull = 0
        
        name = regionList[counter].replace(" ","")
        name = regionList[counter].replace("_cuts", "")
        #print (name)
        if name in renamedRegions.keys():
            name = renamedRegions[name]   
            #print (name)
        for info in results: #extract the information
            #print ("aici",info[4])
            if regionList[counter] == info[0]:
                nObs = info[2]
                nExp = info[3]
                nExpEr = info[4]
                
                if nExp>0:                    
                    nExpStatEr = sqrt(nExp)
                pEr = PoissonError(nExp)                
                nExpStatErUp = pEr[0]
                nExpStatErDo = pEr[1]

                 
                if name.find("CR") < 0:
                    nExpTotEr = nExpEr#sqrt(nExpStatEr*nExpStatEr+nExpEr*nExpEr)
                    nExpTotErUp = nExpEr#sqrt(nExpStatErUp*nExpStatErUp+nExpEr*nExpEr)
                    nExpTotErDo = nExpEr#sqrt(nExpStatErDo*nExpStatErDo+nExpEr*nExpEr)
                else:    
                    nExpTotEr = nExpEr

                if (nObs-nExp) >= 0 and nExpTotErUp != 0:
                    pull = (nObs-nExp)/nExpTotErUp
                if (nObs-nExp) <= 0 and nExpTotErDo != 0:
                    pull = (nObs-nExp)/nExpTotErDo
                if nObs == 0 and nExp == 0:
                    pull = 0
                    nObs = -100
                    nPred = -100
                    nExpEr = 0
                    nExpTotEr = 0
                    nExpStatEr = 0
                    nExpStatErUp = 0
                    nExpStatErDo = 0
                    nExpTotErUp = 0
                    nExpTotErDo = 0

                #bkg components
                compInfo = info[6]
                for i in range(len(compInfo)):
                    hbkgComponents[compInfo[i][0]].SetBinContent(counter+1,compInfo[i][1])
                    
                break

        #print (nObs)
        if nObs>max: max = nObs
        if nExp+nExpTotErUp > max: max = nExp+nExpTotErUp
        if nObs<min and nObs != 0: min = nObs
        if nExp<min and nExp != 0: min = nExp

        graph_bkg.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg.SetPointError(counter, 0.5, 0.5, nExpStatErDo, nExpStatErUp)
        graph_bkg2.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg2.SetPointError(counter, 0.5, 0.5, nExpTotErDo, nExpTotErUp)
        graph_bkg3.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg3.SetPointError(counter, 0.5, 0.5, 0, 0)
        graph_data.SetPoint(counter, hbkg.GetBinCenter(counter+1), nObs)
        graph_data.SetPointError(counter, 0., 0, PoissonError(nObs)[1], PoissonError(nObs)[0])
        
        graph_pull.SetPoint(counter, hbkg.GetBinCenter(counter+1), pull)
        graph_pull.SetPointError(counter, 0., 0, 0, 0)
                          
        hdata.GetXaxis().SetBinLabel(counter+1, name)
        hdata.SetBinContent(counter+1, -1000)
        hdata.SetBinError(counter+1, 0.00001)
        hbkg.SetBinContent(counter+1, nExp)
        hbkg.SetBinError(counter+1, nExpStatEr)

        hbkgUp.SetBinContent(counter+1, nExp+nExpTotErUp)
        hbkgDown.SetBinContent(counter+1, nExp-nExpTotErDo)

    hdata.SetMaximum(1.3*max)
    #if min<=0: min=0.5
    #hdata.SetMinimum(0.95*min)
    hdata.SetMinimum(0.)
    #hdata.SetMinimum(0.1)
    return

def MakeHistPullPlot(samples, regionList, outFileNamePrefix, hresults, renamedRegions, doBlind, extra=""):
    print ("========================================", outFileNamePrefix)
    ROOT.gStyle.SetOptStat(0000);
    Npar=len(regionList)
    #print (regionList)
    
    hdata = TH1F(outFileNamePrefix, outFileNamePrefix, Npar, 0, Npar);
    hdata.GetYaxis().CenterLabels( 1 );
    hdata.GetYaxis().SetTitle("Number of events")
    hdata.GetYaxis().SetTitleSize( 0.04 )
    hdata.GetYaxis().CenterLabels( 1 );
    hdata.GetYaxis().SetTitleOffset(0.6)
    hdata.GetXaxis().SetLabelSize( 0.06 )
    hdata.GetYaxis().SetLabelSize( 0.04 )
    hdata.SetMarkerStyle(20)
    hbkg = TH1F("hbkg", "hbkg", Npar, 0, Npar);

    hbkgUp = TH1F("hbkgUp", "hbkgUp", Npar, 0, Npar);    
    hbkgUp.SetLineStyle(2)
    hbkgUp.SetLineWidth(0)
    
    hbkgDown = TH1F("hbkgDown", "hbkgDown", Npar, 0, Npar); 
    hbkgDown.SetLineStyle(2)
    hbkgDown.SetLineWidth(0)    
    
    hbkgComponents = {}
    samples.replace(" ","") #remove spaces, and split by comma => don't split by ", " 
    for sam in samples.split(","):
        h = TH1F("hbkg"+sam, "hbkg"+sam, Npar, 0, Npar)
        h.SetFillColor(getSampleColor(sam))
        h.SetLineColor(getSampleColor(sam))
        hbkgComponents[sam]=h
    
    graph_bkg = TGraphAsymmErrors(Npar)
    graph_bkg.SetFillColor(kBlack)
    graph_bkg2 = TGraphAsymmErrors(Npar)
    graph_bkg2.SetFillColor(kGray+2)
    graph_bkg2.SetFillStyle(3353)
    
    
    graph_bkg3 = TGraphAsymmErrors(Npar)
    graph_bkg3.SetFillColor(kBlack)
    graph_data = TGraphAsymmErrors(Npar)
    graph_data.SetFillColor(kBlack)
    
    graph_pull = TGraphAsymmErrors(Npar)

    MakeHist(regionList,  renamedRegions,  hresults, hdata, hbkg, hbkgDown, hbkgUp, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents)

    c = TCanvas("c"+outFileNamePrefix, outFileNamePrefix, 1200, 800);
    
    upperPad = ROOT.TPad("upperPad", "upperPad", 0.001, 0.45, 0.995, 0.995)
    lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.001, 0.001, 0.995, 0.45)    

    upperPad.SetFillColor(0);
    upperPad.SetBorderMode(0);
    upperPad.SetBorderSize(2);
    upperPad.SetTicks() 
    upperPad.SetTopMargin   ( 0.1 );
    upperPad.SetRightMargin ( 0.05 );
    upperPad.SetBottomMargin( 0.0025 );
    upperPad.SetLeftMargin( 0.10 );
    upperPad.SetFrameBorderMode(0);
    upperPad.SetFrameBorderMode(0);
    upperPad.SetLogy();
    upperPad.Draw()
    
    lowerPad.SetGridx();
    lowerPad.SetGridy(); 
    lowerPad.SetFillColor(0);
    lowerPad.SetBorderMode(0);
    lowerPad.SetBorderSize(2);
    lowerPad.SetTickx(1);
    lowerPad.SetTicky(1);
    lowerPad.SetTopMargin   ( 0.003 );
    lowerPad.SetRightMargin ( 0.05 );
    lowerPad.SetBottomMargin( 0.65 )
    lowerPad.SetLeftMargin( 0.10 );
    lowerPad.Draw()

    c.SetFrameFillColor(ROOT.kWhite)

    upperPad.cd()  
    
    hdata.GetYaxis().SetRangeUser(0.5,2e5)

    if 'allSR' in outFileNamePrefix: hdata.GetYaxis().SetTickLength(0.02)
    hdata.Draw("E")
    
    if 'all' in outFileNamePrefix:
        nbin=hdata.GetNbinsX()
        getx = hdata.GetBinLowEdge(nbin-2)
        print (getx)
        myline = TLine()
        myline.SetLineStyle(2)
        myline.SetLineWidth(2)
        myline.SetLineColor(kGray+2)
        myline.DrawLine(getx,0.5,getx,85.)
    
    stack = THStack("stack","stack")
    #hbkgComponents['zjets_Sherpa221'].Add(hbkgComponents['ttv'])
    for h in samples.split(","):
        stack.Add(hbkgComponents[h])
    stack.Draw("same")
    hbkg.Draw("hist,same")
    hbkgUp.Draw("hist,same")
    hbkgDown.Draw("hist,same")
    
    graph_bkg2.Draw("2")
    graph_data.SetMarkerStyle(20)
    graph_data.Draw("P")
    hdata.Draw("E,same")
    hdata.Draw("same,axis")
    
    leg = TLegend(0.64,0.62,0.95,0.865)
    leg.SetNColumns(2)
    leg.SetTextFont(42)
    leg.AddEntry(graph_data, "Data","pe")  
    entry = leg.AddEntry("","Total SM","lf")
    entry.SetLineWidth(2)    
    entry.SetFillStyle(3353)
    entry.SetFillColor(kGray+2)       
    entry = leg.AddEntry("","t#bar{t}","lf")
    entry.SetLineColor(kGreen-9)
    entry.SetFillColor(kGreen-9)
    entry.SetFillStyle(1001)   
    entry = leg.AddEntry("","single top","lf")
    entry.SetLineColor(kGreen + 2)
    entry.SetFillColor(kGreen + 2)
    entry.SetFillStyle(1001)       
    entry = leg.AddEntry("","W+jets","lf")
    entry.SetLineColor(kAzure+1)
    entry.SetFillColor(kAzure+1)
    entry.SetFillStyle(1001)
    entry = leg.AddEntry("","Z+jets","lf")
    entry.SetLineColor(kOrange)
    entry.SetFillColor(kOrange)
    entry.SetFillStyle(1001)    
    entry = leg.AddEntry("","diboson (#geq 2L)","lf")
    entry.SetLineColor(kViolet - 8)
    entry.SetFillColor(kViolet - 8)
    entry.SetFillStyle(1001)
    entry = leg.AddEntry("","diboson (1L)","lf")
    entry.SetLineColor(kViolet + 8)
    entry.SetFillColor(kViolet + 8)
    entry.SetFillStyle(1001)
    entry = leg.AddEntry("","diboson (0L)","lf")
    entry.SetLineColor(kViolet - 4)
    entry.SetFillColor(kViolet - 4)
    entry.SetFillStyle(1001)
    entry = leg.AddEntry("","multiboson","lf")
    entry.SetLineColor(kViolet - 7)
    entry.SetFillColor(kViolet - 7)
    entry.SetFillStyle(1001)    
    #entry = leg.AddEntry("","Others","lf")
    #entry.SetLineColor(kOrange)
    #entry.SetFillColor(kOrange)
    #entry.SetFillStyle(1001)   
    entry = leg.AddEntry("","t#bar{t}+V","lf")
    entry.SetLineColor(kOrange+1)
    entry.SetFillColor(kOrange+1)
    entry.SetFillStyle(1001)  
    entry = leg.AddEntry("","t#bar{t}+h","lf")
    entry.SetLineColor(kOrange+2)
    entry.SetFillColor(kOrange+2)
    entry.SetFillStyle(1001)  
    entry = leg.AddEntry("","V+h","lf")
    entry.SetLineColor(kOrange+3)
    entry.SetFillColor(kOrange+3)
    entry.SetFillStyle(1001)      
    leg.SetBorderSize(0)                  
    leg.Draw("same,axis")
    
    x=0.1
    
    ATLASLabel(x+0.25,0.82,"              Internal")
   
    l2 = ROOT.TLatex()
    l2.SetNDC()
    l2.SetTextFont(42)
    
    # C1N2_WZ
    l2.DrawLatex(x+0.25,0.70,"EWK 2nd-wave, 1e/#mu + jets + E_{T}^{miss}")
    #l2.DrawLatex(x+0.25,0.70,"#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{0}_{2} #rightarrow WZ #tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1} #rightarrow 1e/#mu + jets + E_{T}^{miss}")
    # C1C1_WW
    #l2.DrawLatex(x+0.25,0.70,"#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{#mp}_{1} #rightarrow WW #tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1} #rightarrow 1e/#mu + jets + E_{T}^{miss}")
    l2.DrawLatex(x+0.25,0.76,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    #l2.DrawLatex(x+0.25,0.64,"All SRs are blinded")
       
    lowerPad.cd()
    
    # Draw frame with pulls
    frame = GetFrame(outFileNamePrefix,Npar,horizontal=True)
    for bin in range(1,hdata.GetNbinsX()+1):
        frame.GetXaxis().SetBinLabel(bin,hdata.GetXaxis().GetBinLabel(bin))
    frame.Draw();
    
    if 'all' in outFileNamePrefix:
        nbin=hdata.GetNbinsX()
        getx = hdata.GetBinLowEdge(nbin-2)
        print (getx)
        myline = TLine()
        myline.SetLineStyle(2)
        myline.SetLineWidth(2)
        myline.SetLineColor(kGray+2)
        myline.DrawLine(getx,-3.5,getx,3.5)
    
    all = []
    GetBoxes(all, hresults, renamedRegions, frame, doBlind, True)
    
    upperPad.RedrawAxis()

    c.Print("pull_plots/histpull_"+outFileNamePrefix+".eps")
    c.Print("pull_plots/histpull_"+outFileNamePrefix+".png")
    c.Print("pull_plots/histpull_"+outFileNamePrefix+".pdf")
    
    return

def makePullPlot(pickleFilename, regionList, samples, renamedRegions, outputPrefix, doBlind=False):
    """
    Make a pull plot from a pickle file of results

    @param pickleFilename Filename to open
    @param regionList List of regions to draw pulls for
    @param samples List of samples in each region
    @param renamedRegions List of renamed regions; dict of old => new names
    @param outputPrefix Prefix for the output file
    @param doBlind Blind the SR or not?
    """

    results1 = []
    results2 = []
    #print (regionList)
    #print (renamedRegions)
    if type(pickleFilename) is str: pickleFilename = [pickleFilename]
    

    for mypickle in pickleFilename:
     
        #print(mypickle)
        try:
            picklefile = open(mypickle,'rb')
        except IOError:
            print ("Cannot open pickle %s, continuing to next" % mypickle)
            return

        mydict = pickle.load(picklefile)
        #print (mydict)


        for region in mydict["names"]:
         if "bin" in region or "DB2LCR" in region or "DB2LVR" in region :
            #print (region)
            index = mydict["names"].index(region)        
            nbObs = mydict["nobs"][index]        
            nbExp = mydict["TOTAL_FITTED_bkg_events"][index]  
            nbExpEr = mydict["TOTAL_FITTED_bkg_events_err"][index]            
            pEr = PoissonError(nbExp)
            totEr = sqrt(nbExpEr*nbExpEr+pEr[2]*pEr[2])                
            totErDo = sqrt(nbExpEr*nbExpEr+pEr[1]*pEr[1])
            totErUp = sqrt(nbExpEr*nbExpEr+pEr[0]*pEr[0])
            
            if (nbObs-nbExp) > 0 and totErUp != 0:
                pull = (nbObs-nbExp)/totErUp
            if (nbObs-nbExp) <= 0 and totErDo != 0:
                pull = (nbObs-nbExp)/totErDo

            nbExpComponents = []
            for sam in samples.split(","):
                try: 
                    nbExpComponents.append((sam,mydict["Fitted_events_"+sam][index] ))
                    #print (sam, mydict["Fitted_events_"+sam][index], region)
                except: continue
        
            if -0.02 < pull < 0: pull = -0.02 ###ATT: ugly
            if 0 < pull < 0.02:  pull = 0.02 ###ATT: ugly
             
            if region.find("SR")>=0 and doBlind:
                nbObs = -100
                pull = 0
            #print (region)   
            print (region,nbObs,nbExp,totErUp,totErDo,totEr, pull)

            results1.append((region,pull,nbObs,nbExp,nbExpEr,totEr,nbExpComponents))

    #pull
    MakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind)
    
    # return the results array in case you want to use this somewhere else
    return results1
