#!/bin/bash

in_ds=$1

HistFitter.py -tw -r $in_ds python/MyDiscoveryAnalysis_Paper2015_SR.py
UpperLimitTable.py -c $in_ds  -n 10000 -N 30 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_${in_ds}/SPlusB_combined_BasicMeasurement_model.root -l 3.20905
