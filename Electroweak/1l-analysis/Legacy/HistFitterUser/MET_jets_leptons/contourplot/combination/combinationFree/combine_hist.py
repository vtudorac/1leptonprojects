#!/usr/bin/env python
import sys
from ROOT import *

nbin=150

def transform_histo(hist,analysis,dowhich):
    if dowhich:
        #print 'do x12'
        finer_hist=TH2F(hist.GetName()+"_pre"+analysis,hist.GetName()+"_pre"+analysis, nbin,200.,2000.,nbin,0.,1500.) ## for x=1/2
    else:
        #print 'do gridx'
        finer_hist=TH2F(hist.GetName()+"_pre"+analysis,hist.GetName()+"_pre"+analysis, nbin,200.,2000.,nbin,0.,1.)     ## for gridx
        pass
    for i in range(1,nbin+1):
        for j in range(1,nbin+1):
            if finer_hist.GetXaxis().GetBinUpEdge(i)<=hist.GetXaxis().GetBinUpEdge(hist.GetNbinsX()) and finer_hist.GetYaxis().GetBinUpEdge(j)<=hist.GetYaxis().GetBinUpEdge(hist.GetNbinsY()) and finer_hist.GetXaxis().GetBinLowEdge(i)>=hist.GetXaxis().GetBinLowEdge(1) and finer_hist.GetYaxis().GetBinLowEdge(j)>=hist.GetYaxis().GetBinLowEdge(1):
                finer_hist.SetBinContent(i,j,hist.Interpolate(finer_hist.GetXaxis().GetBinCenter(i),finer_hist.GetYaxis().GetBinCenter(j)))
            else:
                finer_hist.SetBinContent(i,j,0.)
    return finer_hist
    
    
    

def find_bestexpected(hist_softlepton,hist_hardlepton,best_expected):

    for i in range(0,hist_softlepton.GetNbinsX()+2):
        for j in range(0,hist_softlepton.GetNbinsY()+2):
	    #print hist_softlepton.GetBinContent(i,j),hist_hardlepton.GetBinContent(i,j)
            if hist_softlepton.GetBinContent(i,j)>hist_hardlepton.GetBinContent(i,j):
                best_expected[str(i)+"_"+str(j)] = 's'
            else:
                best_expected[str(i)+"_"+str(j)] = 'h'
    return
    
    
    
    

def fill_histogram(name,h_soft,h_hard,best_expected):
    if h_soft.GetNbinsX()!=h_hard.GetNbinsX() or h_soft.GetNbinsY()!=h_hard.GetNbinsY() or h_soft.GetNbinsX()!=nbin or h_soft.GetNbinsY()!=nbin:
        print "Histograms have the wrong number of bins!"
        return
	
    new_hist= TH2D(name,name,h_soft.GetNbinsX(),h_soft.GetXaxis().GetBinLowEdge(1),h_soft.GetXaxis().GetBinUpEdge(h_soft.GetNbinsX()),h_soft.GetNbinsY(),h_soft.GetYaxis().GetBinLowEdge(1),h_soft.GetYaxis().GetBinUpEdge(h_soft.GetNbinsY()))    
    
    #print new_hist
    
    for i2 in range(0,h_soft.GetNbinsX()+2):
        for j2 in range(0,h_soft.GetNbinsY()+2):
            if best_expected[str(i2)+"_"+str(j2)] == 's':
                new_hist.SetBinContent(i2,j2,h_soft.GetBinContent(i2,j2))
                #print "Soft Lepton! "+str(new_hist.GetXaxis().GetBinCenter(i2))+" "+str(new_hist.GetYaxis().GetBinCenter(j2))
            elif best_expected[str(i2)+"_"+str(j2)] == 'h':
                new_hist.SetBinContent(i2,j2,h_hard.GetBinContent(i2,j2))
                #print "Hard Lepton! "+str(new_hist.GetXaxis().GetBinCenter(i2))+" "+str(new_hist.GetYaxis().GetBinCenter(j2))
            else:
                print "Error: no match found for bin"
    return new_hist
    
    
def main( in1, in1_up, in1_dn, in2, in2_up, in2_dn, out, out_up, out_dn ):    
    
    inf_softlepton_nominal=in1
    inf_softlepton_up=in1_up
    inf_softlepton_down=in1_dn

    inf_hardlepton_nominal=in2
    inf_hardlepton_up=in2_up
    inf_hardlepton_down=in2_dn

    outf_merged_nominal=out
    outf_merged_up=out_up
    outf_merged_down=out_dn


    print 'inf_softlepton_nominal=',inf_softlepton_nominal
    print 'inf_hardlepton_nominal=',inf_hardlepton_nominal
    print 'outf_merged_nominal=',outf_merged_nominal
    if 'x12' in inf_hardlepton_nominal: dox12=True
    if 'gridx' in inf_hardlepton_nominal: dox12=False
    #sys.argv=[sys.argv[0], '-b'] #?


    #Open files:

    f_softlepton = TFile.Open(inf_softlepton_nominal,"READ")
    f_hardlepton = TFile.Open(inf_hardlepton_nominal,"READ")

    best_expected = {}

    find_bestexpected(transform_histo(f_softlepton.Get("sigp1expclsf"),"soft",dox12),transform_histo(f_hardlepton.Get("sigp1expclsf"),"hard",dox12),best_expected)

    out_nominal = TFile.Open(outf_merged_nominal,"RECREATE")

    list_of_histograms=['hclPmin2','sigp1','sigp1clsf','sigp1expclsf','sigclsu1s','sigclsd1s','upperLimit','xsec','excludedXsec']

    hist_merge={}

    for name in list_of_histograms:
        hist_merge[name]=fill_histogram(name,transform_histo(f_softlepton.Get(name),"soft",dox12),transform_histo(f_hardlepton.Get(name),"hard",dox12),best_expected)
        
    print hist_merge

    for name in list_of_histograms:
        hist_merge[name].Write()

    f_softlepton.Close()
    f_hardlepton.Close()    
    out_nominal.Close()
    ################################################################
    f_softlepton_up = TFile.Open(inf_softlepton_up,"READ")
    f_hardlepton_up = TFile.Open(inf_hardlepton_up,"READ")
    out_up = TFile.Open(outf_merged_up,"RECREATE")

    hist_merge_up={}

    for name in list_of_histograms:
        hist_merge_up[name]=fill_histogram(name,transform_histo(f_softlepton_up.Get(name),"soft",dox12),transform_histo(f_hardlepton_up.Get(name),"hard",dox12),best_expected)    

    for name in list_of_histograms:
        hist_merge_up[name].Write()

    f_softlepton_up.Close()
    f_hardlepton_up.Close()    
    out_up.Close()
    ################################################################
    f_softlepton_down = TFile.Open(inf_softlepton_down,"READ")
    f_hardlepton_down = TFile.Open(inf_hardlepton_down,"READ")
    out_down = TFile.Open(outf_merged_down,"RECREATE")

    hist_merge_down={}

    for name in list_of_histograms:
        hist_merge_down[name]=fill_histogram(name,transform_histo(f_softlepton_down.Get(name),"soft",dox12),transform_histo(f_hardlepton_down.Get(name),"hard",dox12),best_expected)    

    for name in list_of_histograms:
        hist_merge_down[name].Write()

    f_softlepton_down.Close()
    f_hardlepton_down.Close()    
    out_down.Close()
    ################################################################
