#!/usr/bin/env python
import sys
from ROOT import *

nbin=150 ## 150 is better than 100. tested :)

def transform_histo(hist,analysis,dowhich):
    if dowhich:
        finer_hist=TH2F(hist.GetName()+"_pre"+analysis,hist.GetName()+"_pre"+analysis, nbin,200.,2000.,nbin,0.,1500.) ## for x=1/2
    else:
        finer_hist=TH2F(hist.GetName()+"_pre"+analysis,hist.GetName()+"_pre"+analysis, nbin,200.,2000.,nbin,0.,1.)     ## for gridx
        pass
    for i in range(1,nbin+1):
        for j in range(1,nbin+1):
            print i, j
            if finer_hist.GetXaxis().GetBinUpEdge(i)<=hist.GetXaxis().GetBinUpEdge(hist.GetNbinsX()) and finer_hist.GetYaxis().GetBinUpEdge(j)<=hist.GetYaxis().GetBinUpEdge(hist.GetNbinsY()) and finer_hist.GetXaxis().GetBinLowEdge(i)>=hist.GetXaxis().GetBinLowEdge(1) and finer_hist.GetYaxis().GetBinLowEdge(j)>=hist.GetYaxis().GetBinLowEdge(1):
                finer_hist.SetBinContent(i,j,hist.Interpolate(finer_hist.GetXaxis().GetBinCenter(i),finer_hist.GetYaxis().GetBinCenter(j)))
            else:
                finer_hist.SetBinContent(i,j,0.)
    return finer_hist

def find_bestexpected(hist_hardlepton1,hist_hardlepton2,hist_hardlepton3,hist_hardlepton4,best_expected):

    for i in range(0,hist_hardlepton1.GetNbinsX()+2):
        for j in range(0,hist_hardlepton1.GetNbinsY()+2):

        #print hist_hardlepton1.GetBinContent(i,j)
        #print hist_hardlepton2.GetBinContent(i,j)
        #print hist_hardlepton3.GetBinContent(i,j)
        #print hist_hardlepton4.GetBinContent(i,j)

            best_value = float(max(hist_hardlepton1.GetBinContent(i,j),hist_hardlepton2.GetBinContent(i,j),hist_hardlepton3.GetBinContent(i,j),hist_hardlepton4.GetBinContent(i,j)))
            
            if best_value == float(hist_hardlepton1.GetBinContent(i,j)):
                best_expected[str(i)+"_"+str(j)] = 'h1'
            elif best_value == float(hist_hardlepton2.GetBinContent(i,j)):
                best_expected[str(i)+"_"+str(j)] = 'h2'
            elif best_value == float(hist_hardlepton3.GetBinContent(i,j)):
                best_expected[str(i)+"_"+str(j)] = 'h3'
            elif best_value == float(hist_hardlepton4.GetBinContent(i,j)):
                best_expected[str(i)+"_"+str(j)] = 'h4'
                
    return
    

def fill_histogram(name,h_hard1,h_hard2,h_hard3,h_hard4,best_expected):
    if h_hard1.GetNbinsX()!=h_hard1.GetNbinsX() or h_hard1.GetNbinsY()!=h_hard1.GetNbinsY() or h_hard1.GetNbinsX()!=nbin or h_hard1.GetNbinsY()!=nbin:
        print "Histograms have the wrong number of bins!"
        return

    new_hist= TH2D(name,name,h_hard1.GetNbinsX(),h_hard1.GetXaxis().GetBinLowEdge(1),h_hard1.GetXaxis().GetBinUpEdge(h_hard1.GetNbinsX()),h_hard1.GetNbinsY(),h_hard1.GetYaxis().GetBinLowEdge(1),h_hard1.GetYaxis().GetBinUpEdge(h_hard1.GetNbinsY()))    
    
    #print new_hist
    
    for i2 in range(0,h_hard1.GetNbinsX()+2):
        for j2 in range(0,h_hard1.GetNbinsY()+2):
            if best_expected[str(i2)+"_"+str(j2)] == 'h1':
                new_hist.SetBinContent(i2,j2,h_hard1.GetBinContent(i2,j2))
                #print "Hard Lepton1 "+str(new_hist.GetXaxis().GetBinCenter(i2))+" "+str(new_hist.GetYaxis().GetBinCenter(j2))
            elif best_expected[str(i2)+"_"+str(j2)] == 'h2':
                new_hist.SetBinContent(i2,j2,h_hard2.GetBinContent(i2,j2))
                #print "Hard Lepton2 "+str(new_hist.GetXaxis().GetBinCenter(i2))+" "+str(new_hist.GetYaxis().GetBinCenter(j2))
            elif best_expected[str(i2)+"_"+str(j2)] == 'h3':
                new_hist.SetBinContent(i2,j2,h_hard3.GetBinContent(i2,j2))
                #print "Hard Lepton3 "+str(new_hist.GetXaxis().GetBinCenter(i2))+" "+str(new_hist.GetYaxis().GetBinCenter(j2))
            elif best_expected[str(i2)+"_"+str(j2)] == 'h4':
                new_hist.SetBinContent(i2,j2,h_hard4.GetBinContent(i2,j2))
                #print "Hard Lepton4 "+str(new_hist.GetXaxis().GetBinCenter(i2))+" "+str(new_hist.GetYaxis().GetBinCenter(j2))
            else:
                print "Error: no match found for bin"
    return new_hist
    
    
################ inputs ##############################
inf_hardlepton1_nominal=sys.argv[1]
inf_hardlepton1_up=sys.argv[2]
inf_hardlepton1_down=sys.argv[3]

inf_hardlepton2_nominal=sys.argv[4]
inf_hardlepton2_up=sys.argv[5]
inf_hardlepton2_down=sys.argv[6]

inf_hardlepton3_nominal=sys.argv[7]
inf_hardlepton3_up=sys.argv[8]
inf_hardlepton3_down=sys.argv[9]

inf_hardlepton4_nominal=sys.argv[10]
inf_hardlepton4_up=sys.argv[11]
inf_hardlepton4_down=sys.argv[12]

outf_merged_nominal=sys.argv[13]
outf_merged_up=sys.argv[14]
outf_merged_down=sys.argv[15]

if 'x12' in outf_merged_nominal:  dox12=True
if 'gridx' in outf_merged_nominal:  dox12=False

sys.argv=[sys.argv[0], '-b']


#Open files:

f_hardlepton1 = TFile.Open(inf_hardlepton1_nominal,"READ")
f_hardlepton2 = TFile.Open(inf_hardlepton2_nominal,"READ")
f_hardlepton3 = TFile.Open(inf_hardlepton3_nominal,"READ")
f_hardlepton4 = TFile.Open(inf_hardlepton4_nominal,"READ")

best_expected = {}

find_bestexpected(transform_histo(f_hardlepton1.Get("sigp1expclsf"),"hard1",dox12),transform_histo(f_hardlepton2.Get("sigp1expclsf"),"hard2",dox12),transform_histo(f_hardlepton3.Get("sigp1expclsf"),"hard3",dox12),transform_histo(f_hardlepton4.Get("sigp1expclsf"),"hard4",dox12),best_expected)

out_nominal = TFile.Open(outf_merged_nominal,"RECREATE")

list_of_histograms=['hclPmin2','sigp1','sigp1clsf','sigp1expclsf','sigclsu1s','sigclsd1s','upperLimit','xsec','excludedXsec']

hist_merge={}

for name in list_of_histograms:
    hist_merge[name]=fill_histogram(name,transform_histo(f_hardlepton1.Get(name),"hard1",dox12),transform_histo(f_hardlepton2.Get(name),"hard2",dox12),transform_histo(f_hardlepton3.Get(name),"hard3",dox12),transform_histo(f_hardlepton4.Get(name),"hard4",dox12),best_expected)

#print hist_merge

for name in list_of_histograms:
    hist_merge[name].Write()


f_hardlepton1.Close()    
f_hardlepton2.Close()    
f_hardlepton3.Close()    
f_hardlepton4.Close()    
out_nominal.Close()
################################################################

f_hardlepton1_up = TFile.Open(inf_hardlepton1_up,"READ")
f_hardlepton2_up = TFile.Open(inf_hardlepton2_up,"READ")
f_hardlepton3_up = TFile.Open(inf_hardlepton3_up,"READ")
f_hardlepton4_up = TFile.Open(inf_hardlepton4_up,"READ")
out_up = TFile.Open(outf_merged_up,"RECREATE")

hist_merge_up={}

for name in list_of_histograms:
    hist_merge_up[name]=fill_histogram(name,transform_histo(f_hardlepton1_up.Get(name),"hard1",dox12),transform_histo(f_hardlepton2_up.Get(name),"hard2",dox12),transform_histo(f_hardlepton3_up.Get(name),"hard3",dox12),transform_histo(f_hardlepton4_up.Get(name),"hard4",dox12),best_expected)
for name in list_of_histograms:
    hist_merge_up[name].Write()


f_hardlepton1_up.Close()    
f_hardlepton2_up.Close()    
f_hardlepton3_up.Close()    
f_hardlepton4_up.Close()    
out_up.Close()
################################################################

f_hardlepton1_down = TFile.Open(inf_hardlepton1_down,"READ")
f_hardlepton2_down = TFile.Open(inf_hardlepton2_down,"READ")
f_hardlepton3_down = TFile.Open(inf_hardlepton3_down,"READ")
f_hardlepton4_down = TFile.Open(inf_hardlepton4_down,"READ")
out_down = TFile.Open(outf_merged_down,"RECREATE")

hist_merge_down={}

for name in list_of_histograms:
    hist_merge_down[name]=fill_histogram(name,transform_histo(f_hardlepton1_down.Get(name),"hard1",dox12),transform_histo(f_hardlepton2_down.Get(name),"hard2",dox12),transform_histo(f_hardlepton3_down.Get(name),"hard3",dox12),transform_histo(f_hardlepton4_down.Get(name),"hard4",dox12),best_expected)
for name in list_of_histograms:
    hist_merge_down[name].Write()


f_hardlepton1_down.Close()
f_hardlepton2_down.Close()
f_hardlepton3_down.Close()
f_hardlepton4_down.Close()    
out_down.Close()
################################################################
