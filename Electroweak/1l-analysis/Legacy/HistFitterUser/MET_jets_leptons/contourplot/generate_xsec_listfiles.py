#!/usr/bin/env python

import re

import ROOT

treepath = "/project/etp5/SUSYInclusive/trees/T_06_10/allTrees_T_06_10_signal.root"

f = ROOT.TFile.Open(treepath)

xsecs = {}

for key in f.GetListOfKeys():
    if not "NoSys" in key.GetName():
        continue
    prefix = re.search("(.*?)_[0-9]", key.GetName()).groups()[0]
    tree = f.Get(key.GetName())
    tree.Draw("DatasetNumber", "1", "goff", 1)
    DatasetNumber = float(tree.GetV1()[0])
    tree.Draw("xsec", "1", "goff", 1)
    xsec = float(tree.GetV1()[0])
    numbers = [m.replace("_","") for m in re.findall("_[0-9]+", key.GetName())]
    if len(numbers) > 2:
        mg,mc,mn = numbers[0:1]+numbers[-2:]
    elif len(numbers) == 2:
        mg,mn = numbers
        mc = 0
    else:
        raise Exception("Don't know what to do with {}".format(key.GetName()))
    if "oneStep" in prefix:
        if int(mn) == 60:
            prefix += "_gridx"
        else:
            prefix += "_x12"
    if not prefix in xsecs:
        xsecs[prefix] = []
    xsecs[prefix].append((float(mg), float(mc), float(mn), float(xsec)))

f.Close()

for prefix, vallist in xsecs.items():
    with open("{}_xsec_listfile".format(prefix), "w") as of:
        for i, (mg, mc, mn, xsec) in enumerate(vallist):
            of.write("{:>10.0f}{:>10.0f}{:>10.0f}{:>10.0f}{:>15.7f}\n".format(i, mg, mc, mn, xsec))
