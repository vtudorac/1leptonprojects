#!/usr/bin/env python

import sys, commands
import os, os.path
import argparse

#gSystem.Load("libSusyFitter.so")

def extract_information(infile,interpretation):
    temp_dic={}

    for line in infile.readlines():
        #print line
        informationstring = line.lstrip().split(" ")

        if bool(int(float(informationstring[interpretation.index('failedfit/F')]))) and bool(int(float(informationstring[interpretation.index('failedcov/F')]))):
            continue

        G_index=interpretation.index('G/F')
        C_index=interpretation.index('C/F')
        LSP_index=interpretation.index('LSP/F')
        CLsexp_index=interpretation.index('CLsexp/F')
        expup_index=interpretation.index('expectedUpperLimit/F')
        G=informationstring[G_index]
        C=informationstring[C_index]
        LSP=informationstring[LSP_index]
        CLsexp=informationstring[CLsexp_index]
        expup=informationstring[expup_index]
        point = (int(float(G)),int(float(C)),int(float(LSP)))
        if float(CLsexp)==1.00000:
            print "There is something fishy - won't use the result for line "+line
            continue

        #also: we need some information if the result is ok for the point - decide upon fit quality

        #temp_dic[point]=(float(CLsexp),float(expup),line) ## for hypotest
        temp_dic[point]=(float(expup),float(CLsexp),line)  ## for upper limit

    return temp_dic

def get_best_analysis(point,softlepton,hardlepton4Jhighx,hardlepton4Jlowx,hardlepton5J,hardlepton6J):
    #need to convert line to something we can understand
    this_point=(int(point.split("_")[-3]),int(point.split("_")[-2]),int(point.split("_")[-1]))

    if this_point in softlepton.keys() and this_point in hardlepton4Jhighx.keys():
        #check which analysis is giving the lowest expected CLs value
        tmp = float(min(softlepton[this_point][0],hardlepton4Jhighx[this_point][0],hardlepton4Jlowx[this_point][0],hardlepton5J[this_point][0],hardlepton6J[this_point][0]))
        print this_point
        print 'min=',tmp
        print softlepton[this_point][0]
        print hardlepton4Jhighx[this_point][0]
        print hardlepton4Jlowx[this_point][0]
        print hardlepton5J[this_point][0]
        print hardlepton6J[this_point][0]
        print '------'
        if float(softlepton[this_point][0])== tmp:
            return (this_point,softlepton[this_point][0],"S")
        elif float(hardlepton4Jhighx[this_point][0])== tmp:
            return (this_point,hardlepton4Jhighx[this_point][0],"H4Jhighx")
        elif float(hardlepton4Jlowx[this_point][0])== tmp:
            return (this_point,hardlepton4Jlowx[this_point][0],"H4Jlowx")
        elif float(hardlepton5J[this_point][0])== tmp:
            return (this_point,hardlepton5J[this_point][0],"H5J")
        elif float(hardlepton6J[this_point][0])== tmp:
            return (this_point,hardlepton6J[this_point][0],"H6J")
    if this_point not in softlepton.keys() and this_point in hardlepton4Jhighx.keys():
        #check which analysis is giving the lowest expected CLs value
        tmp = float(min(hardlepton4Jhighx[this_point][0],hardlepton4Jlowx[this_point][0],hardlepton5J[this_point][0],hardlepton6J[this_point][0] ))
        if float(hardlepton4Jhighx[this_point][0])== tmp:
            return (this_point,hardlepton4Jhighx[this_point][0],"H4Jhighx")
        elif float(hardlepton4Jlowx[this_point][0])== tmp:
            return (this_point,hardlepton4Jlowx[this_point][0],"H4Jlowx")
        elif float(hardlepton5J[this_point][0])== tmp:
            return (this_point,hardlepton5J[this_point][0],"H5J")
        elif float(hardlepton6J[this_point][0])== tmp:
            return (this_point,hardlepton6J[this_point][0],"H6J")
    if this_point in softlepton.keys() and this_point not in hardlepton4Jhighx.keys():
        #check which analysis is giving the lowest expected CLs value
        tmp = float(softlepton[this_point][0])
        if float(softlepton[this_point][0])== tmp:
            return (this_point,softlepton[this_point][0],"S")

    return "unknown"


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("-s","--input_softlepton",help="give input directory + name for soft lepton files", default="inputlist_upl/GG1stepgridx_5J_upperlimit__1_harvest_list")
    parser.add_argument("-a","--input_hardlepton4Jhighx",default="inputlist_upl/GGgridx_upperlimit_4Jhighx_3ifb_version150116__1_harvest_list")
    parser.add_argument("-b","--input_hardlepton4Jlowx",default="inputlist_upl/GGgridx_upperlimit_4Jlowx_3ifb_version150116__1_harvest_list")
    parser.add_argument("-c","--input_hardlepton5J",default="inputlist_upl/GGgridx_upperlimit_5J_3ifb_version150116__1_harvest_list")
    parser.add_argument("-d","--input_hardlepton6J",default="inputlist_upl/GGgridx_upperlimit_6J_3ifb_version150116__1_harvest_list")
    parser.add_argument("-o","--outputfile",default="outputlist_upl/GGgridx_upperlimit_combSR_3ifb_version150116__1_harvest_list")
    combine = parser.parse_args()

    # open files
    files={}
    files['nominal']={}
    files['nominal']['S'] = open(combine.input_softlepton,'r')
    files['nominal']['H4Jhighx'] = open(combine.input_hardlepton4Jhighx,'r')
    files['nominal']['H4Jlowx'] = open(combine.input_hardlepton4Jlowx,'r')
    files['nominal']['H5J'] = open(combine.input_hardlepton5J,'r')
    files['nominal']['H6J'] = open(combine.input_hardlepton6J,'r')


    #extract information from files into dictionaries - loop for this over all SUSY points for the specific grid
    #and copy information - point, CLsexp and line of the listfiles - into dictionaries
    
    #load description:
    from summary_harvest_tree_description import treedescription
    dummy_file,inter = treedescription()
    interpretation=inter.split(":")
    print "The information is stored in the listfiles in the following order:"
    print interpretation

   
    #initialize dictionaries
    info={}
    for version in ['nominal']:
        info[version]={}
        for analysis in ['S','H4Jhighx','H4Jlowx','H5J','H6J']:
            info[version][analysis]={}
            #copy information from files into dictionaries
            info[version][analysis] = extract_information(files[version][analysis],interpretation)
            #print info[version][analysis]
            files[version][analysis].close()
        

    #now we have all information available to do our comparison
    #store best analysis in a new dictionary
    #for this load the available points externally - we do not want to miss a point
  
    if 'GGx12' in combine.input_hardlepton5J:
        file_points=open("points/susy_points_GG1stepx12.txt","r")
    elif 'GGgridx' in combine.input_hardlepton5J:  
        file_points=open("points/susy_points_GG1stepgridx.txt","r") 
    elif 'SSgridx' in combine.input_hardlepton5J:  
        file_points=open("points/susy_points_SS1stepgridx.txt","r") 
    elif 'SSx12' in combine.input_hardlepton5J:  
        file_points=open("points/susy_points_SS1stepx12.txt","r")
    else:
        print "I am not able to interpret this grid here - I'll stop here!"
        sys.exit(1)

    #write now the combined files + a pickle which analysis was used where
    files['nominal']['outputcombined'] = open(combine.outputfile,'w')

    final_analysis={}
    for version in ['nominal']:
        final_analysis[version]={}


    for line in file_points:
        line=line.replace('\n','')
        #print 'line @',line
        for version in ['nominal']:
            final_analysis[version][line] = get_best_analysis(line,info[version]['S'],info[version]['H4Jhighx'],info[version]['H4Jlowx'],info[version]['H5J'],info[version]['H6J'])
            print final_analysis[version][line]
            if not final_analysis[version][line]=='unknown':
                analysis = (final_analysis[version][line])[2]
                point = (final_analysis[version][line])[0]
                #print analysis, point
                if point in info[version][analysis].keys():
                    result_tuple=info[version][analysis][point]
                    #print result_tuple
                    newline=result_tuple[2]
                    #print "dol newline @",newline
                    files[version]['outputcombined'].write(newline)

    files['nominal']['outputcombined'].close()

    #print final_analysis['nominal']

    import pickle
    outputfile=open(combine.outputfile+".pkl",'wb')
    pickle.dump(final_analysis,outputfile)
    outputfile.close()
        
