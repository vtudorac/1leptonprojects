#!/usr/bin/env python

import sys, commands
import os, os.path
import argparse

#gSystem.Load("libSusyFitter.so")

def extract_information(infile,interpretation):
    temp_dic={}

    for line in infile.readlines():
        #print line
        informationstring = line.lstrip().split(" ")

        if bool(int(float(informationstring[interpretation.index('failedfit/F')]))) and bool(int(float(informationstring[interpretation.index('failedcov/F')]))):
            continue

        G_index=interpretation.index('G/F')
        C_index=interpretation.index('C/F')
        LSP_index=interpretation.index('LSP/F')
        CLsexp_index=interpretation.index('CLsexp/F')
        expup_index=interpretation.index('expectedUpperLimit/F')
        G=informationstring[G_index]
        C=informationstring[C_index]
        LSP=informationstring[LSP_index]
        CLsexp=informationstring[CLsexp_index]
        expup=informationstring[expup_index]
        point = (int(float(G)),int(float(C)),int(float(LSP)))
        if float(CLsexp)==1.00000:
            print "There is something fishy - won't use the result for line "+line
            continue

        #also: we need some information if the result is ok for the point - decide upon fit quality
        temp_dic[point]=(float(CLsexp),float(expup),line) 

        print 'point=',point

    return temp_dic

def get_best_analysis(point,finalanalysis,analysis):
    #need to convert line to something we can understand
    this_point=(int(point.split("_")[-3]),int(point.split("_")[-2]),int(point.split("_")[-1]))

    if this_point in info['nominal'][analysis].keys(): 
        
        if finalanalysis == "unknown": 
            return (this_point,info['nominal'][analysis][this_point][0],analysis)       
        #check which analysis is giving the lowest expected CLs value
        
        else:
            tmp = float(min(finalanalysis[1],info['nominal'][analysis][this_point][0]))
            print this_point
            print 'min=',tmp
            print finalanalysis[1]
            print info['nominal'][analysis][this_point][0]
            print '------'
            if float(info['nominal'][analysis][this_point][0])== tmp:
                print analysis                
                return (this_point,info['nominal'][analysis][this_point][0],analysis)
            else: 
                print finalanalysis[2]
                return finalanalysis
    else: 
        return finalanalysis

    return "unknown"


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--input_softlepton",help="give input directory + name for soft lepton files", default="inputlist/OneSoftLepton_onestepGGx12_2JPaper_fixSigXSecNominal_hypotest__1_harvest_list")
    parser.add_argument("-a","--input_hardlepton4Jhighx",default="inputlist/GGx12_Nominal_hypotest_4Jhighx_3ifb_version150116__1_harvest_list")
    parser.add_argument("-b","--input_hardlepton4Jlowx",default="inputlist/GGx12_Nominal_hypotest_4Jlowx_3ifb_version150116__1_harvest_list")
    parser.add_argument("-c","--input_hardlepton5J",default="inputlist/GGx12_Nominal_hypotest_5J_3ifb_version150116__1_harvest_list")
    parser.add_argument("-d","--input_hardlepton6J",default="inputlist/GGx12_Nominal_hypotest_6J_3ifb_version150116__1_harvest_list")
    parser.add_argument("-o","--outputfile",default="outputlist/GGx12_Nominal_hypotest_combSR_3ifb_version150116__1_harvest_list")
    combine = parser.parse_args()

    # open files
    files={}
    files['nominal']={}
    files['nominal']['S'] = open(combine.input_softlepton,'r')
    files['nominal']['H4Jhighx'] = open(combine.input_hardlepton4Jhighx,'r')
    files['nominal']['H4Jlowx'] = open(combine.input_hardlepton4Jlowx,'r')
    files['nominal']['H5J'] = open(combine.input_hardlepton5J,'r')
    files['nominal']['H6J'] = open(combine.input_hardlepton6J,'r')

    files['upperlimit']={}
    files['upperlimit']['S'] = open(combine.input_softlepton.replace('Nominal_hypotest','upperlimit'),'r')
    files['upperlimit']['H4Jhighx'] = open(combine.input_hardlepton4Jhighx.replace('Nominal_hypotest','upperlimit'),'r')
    files['upperlimit']['H4Jlowx'] = open(combine.input_hardlepton4Jlowx.replace('Nominal_hypotest','upperlimit'),'r')
    files['upperlimit']['H5J'] = open(combine.input_hardlepton5J.replace('Nominal_hypotest','upperlimit'),'r')
    files['upperlimit']['H6J'] = open(combine.input_hardlepton6J.replace('Nominal_hypotest','upperlimit'),'r')


    #extract information from files into dictionaries - loop for this over all SUSY points for the specific grid
    #and copy information - point, CLsexp and line of the listfiles - into dictionaries
    
    #load description:
    from summary_harvest_tree_description import treedescription
    dummy_file,inter = treedescription()
    interpretation=inter.split(":")
    print "The information is stored in the listfiles in the following order:"
    print interpretation

   
    #initialize dictionaries
    info={}
    for version in ['nominal','upperlimit']:
        info[version]={}
        for analysis in ['S','H4Jhighx','H4Jlowx','H5J','H6J']:
            info[version][analysis]={}
            #copy information from files into dictionaries
            info[version][analysis] = extract_information(files[version][analysis],interpretation)
            #print info[version][analysis]
            files[version][analysis].close()
        

    #now we have all information available to do our comparison
    #store best analysis in a new dictionary
    #for this load the available points externally - we do not want to miss a point
  
    if 'GGx12' in combine.input_hardlepton5J:
        file_points=open("points/susy_points_GG1stepx12.txt","r")
    elif 'GGgridx' in combine.input_hardlepton5J:  
        file_points=open("points/susy_points_GG1stepgridx.txt","r") 
    elif 'SSgridx' in combine.input_hardlepton5J:  
        file_points=open("points/susy_points_SS1stepgridx.txt","r") 
    elif 'SSx12' in combine.input_hardlepton5J:  
        file_points=open("points/susy_points_SS1stepx12.txt","r")
    else:
        print "I am not able to interpret this grid here - I'll stop here!"
        sys.exit(1)

    #write now the combined files + a pickle which analysis was used where
    files['nominal']['outputcombined'] = open(combine.outputfile,'w')
    files['upperlimit']['outputcombined'] = open(combine.outputfile.replace('Nominal_hypotest','upperlimit'),'w')

    final_analysis={}


    for line in file_points:
        line=line.replace('\n','')
        #print 'line @',line
        final_analysis[line] = "unknown"
        for analysis in ['S','H4Jhighx','H4Jlowx','H5J','H6J']:
            final_analysis[line] = get_best_analysis(line,final_analysis[line],analysis)
            #print final_analysis[version][line]
        if not final_analysis[line]=='unknown':
            analysis = final_analysis[line][2]
            point = final_analysis[line][0]
            #print analysis, point
            if point in info['nominal'][analysis].keys():
                result_tuple=info['nominal'][analysis][point]
                #print result_tuple
                newline=result_tuple[2]
                #print "dol newline @",newline
                files['nominal']['outputcombined'].write(newline)
            if point in info['upperlimit'][analysis].keys():
                result_tuple2=info['upperlimit'][analysis][point]
                #print result_tuple
                newline2=result_tuple2[2]
                #print "dol newline @",newline
                files['upperlimit']['outputcombined'].write(newline2)                    

    files['nominal']['outputcombined'].close()
    files['upperlimit']['outputcombined'].close()

    print final_analysis

    import pickle
    outputfile=open(combine.outputfile+".pkl",'wb')
    pickle.dump(final_analysis,outputfile)
    outputfile.close()
        
