#!/bin/env python

import os,sys,ROOT

print 'Running makecontourplots'

if len(sys.argv)>1:
    #if sys.argv[1]!='-b': infiles = sys.argv[1:]
    outputname = sys.argv[1]
    if len(sys.argv)>2: infiles = sys.argv[2:]
else:
    print 'Please specify an input file(s)'
    sys.exit(1)
print 'Using files',infiles


print 'Calling over to macro'


execfile( 'summaryplots/plot_CLs.py' )
execfile("summary_harvest_tree_description.py")

plot_CLs( infiles[0] , outputname , '1 e/#mu + jets + E_{T}^{miss}', 3.2)

