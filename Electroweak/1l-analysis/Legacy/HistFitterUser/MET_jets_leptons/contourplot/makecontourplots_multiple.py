#!/bin/env python

import os,sys,ROOT

print 'Running makecontourplots'

if len(sys.argv)>1:
    #if sys.argv[1]!='-b': infiles = sys.argv[1:]
    outputname = sys.argv[1]
    if len(sys.argv)>2: infiles = sys.argv[2:]
else:
    print 'Please specify an input file(s)'
    sys.exit(1)
    
print 'Using files',infiles 

print 'Calling over to macro'


execfile( 'summaryplots/limit_plot_SM_multiple.py' )

#################################################
## OBS bands depend on w/ or w.o up/down files ##
#################################################

limit_plot_SM_multiple( infiles[0], outputname , '1 e/#mu + jets + E_{T}^{miss}', 36.5, True) ### OBS and EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, True, True, False ) ### OBS and EXP w.o band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, False, False, True ) ###  EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, False, False, False ) ### EXP w.o band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, False, False, False, None, -1, True) 

#NOTE:  drawFirstObs=False, allObs=False, firstOneSigma=False, uplimitfile=None , dodis=-1, write_curves=False):
#       dodis = -1 or 3 or 5



