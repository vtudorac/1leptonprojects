#!/bin/env python

import os,sys,ROOT

print 'Running makecontourplots'

if len(sys.argv)>3:
    #if sys.argv[1]!='-b': infiles = sys.argv[1:]
    outputname = sys.argv[1]
    infiles = sys.argv[2]
    infiles2 = sys.argv[3:]
    
else:
    print 'Please specify input files'
    sys.exit(1)
    
print 'Using files',infiles 

print 'Calling over to macro'


execfile( 'summaryplots/limit_plot_SM_multiple_theo.py' )

#################################################
## OBS bands depend on w/ or w.o up/down files ##
#################################################

limit_plot_SM_multiple_theo( infiles, infiles2[0], outputname , '1 e/#mu + jets + E_{T}^{miss}', 36.5, True,True) ### OBS and EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, True, True, False ) ### OBS and EXP w.o band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, False, False, True ) ###  EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, False, False, False ) ### EXP w.o band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, False, False, False, None, -1, True) 


