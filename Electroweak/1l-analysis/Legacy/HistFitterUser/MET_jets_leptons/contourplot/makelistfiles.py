#!/usr/bin/env python

import os,sys,ROOT,math

if len(sys.argv)>1:
    if sys.argv[1]!='-b': infile = sys.argv[1]
    elif len(sys.argv)>2: infile = sys.argv[2]
    else:
        print 'Please specify an input file'
        sys.exit(1)
print 'Using file',infile

ROOT.gSystem.Load('libSusyFitter.so')

print '### For 1-lepton analysis: Please have the namings including : GGx12, GGgridx, SSx12, SSgridx ###'
print '### Or GG2step, pMSSM for the multijet grids'

## choose here if you want discovery or exclusion parameters ##
#dowhich = 'hypo_discovery'  ## for discovery p0, p0exp
dowhich = 'hypo'  ## for exclusion CLs...   

print dowhich
##############################################################################################################
if 'GG2step' in infile:
    format     = dowhich + '_GG2stepWZ_%f_%f_%f_%f'
    interpretation = 'G:mc:C:LSP' # third parameter is N2, for simplification call it "C" anyways
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
    exit()

if 'pMSSM' in infile:
    format     = dowhich + '_pMSSM_M3muM1lsp_%f_%f'
    interpretation = 'G:LSP' # just for simplification lets call the second parameter also "LSP" here (although its the chargino)
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
    exit()

if 'GG' in infile or ('oneStep' in infile and not 'SS_' in infile):
    format     = dowhich + '_GG_oneStep_%f_%f_%f'
    interpretation = 'G:C:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;

if 'SS' in infile or ('oneStep' in infile and not 'GG_' in infile):
    format     = dowhich + '_SS_oneStep_%f_%f_%f'
    interpretation = 'G:C:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;

##############################################################################################################
if 'Gtt_' in infile:
    format     = dowhich + '_Gtt_%f_%f'
    interpretation = 'G:LSP'
    cutStr = '1' 
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
##############################################################################################################
