#!/bin/bash

upperLimitDir="upperLimit_results_05042017_T_06_10"

for grid in GG2stepWZ pMSSM
do
    for var in Nominal Up Down
    do
        ./makelistfiles.py hyporesults_multijet_T_06_10/${grid}_${var}.root
        GenerateTreeDescriptionFromJSON.py -f ${grid}_${var}__1_harvest_list.json
        ./makecontourhists_multijet.py ${grid}_${var}__1_harvest_list
    done

    if [[ -d ${upperLimitDir} ]]
    then
        pushd ${upperLimitDir}
        ln -s ${grid}.root ${grid}_upperlimit.root
        popd
        ./makelistfiles.py ${upperLimitDir}/${grid}_upperlimit.root
        GenerateTreeDescriptionFromJSON.py -f ${grid}_upperlimit__1_harvest_list.json
        ./makecontourhists_multijet.py ${grid}_upperlimit__1_harvest_list
    fi
done
