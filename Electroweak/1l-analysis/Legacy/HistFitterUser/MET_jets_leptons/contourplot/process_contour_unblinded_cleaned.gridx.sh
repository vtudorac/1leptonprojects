#!/bin/bash

#for ds in `cat convertlist.txt `; do sh process.sh $ds; done

i=6ifb_june

###########################################################################################################
# If onew wants to print UPXS/CLS:  the nominal root file will be merged with UPXS/CLS file and renamed
# then it will be read in the plotting macro.
#
# python makecontourplots_contour_cleaned.py  <naming>  <nominal_list.root>  <up_list.root>  <down_list.root>
###########################################################################################################
#                                                                                   

## for one contour ##
#python makecontourplots_contour_cleaned.py contour_GG1stepgridx_${i}_4Jlowx GGgridx_Nominal_hypotest_4Jlowx_${i}__1_harvest_list.root GGgridx_Up_hypotest_4Jlowx_${i}__1_harvest_list.root GGgridx_Down_hypotest_4Jlowx_${i}__1_harvest_list.root

#python makecontourplots_contour_cleaned.py contour_GG1stepgridx_${i}_4Jhighx GGgridx_Nominal_hypotest_4Jhighx_${i}__1_harvest_list.root GGgridx_Up_hypotest_4Jhighx_${i}__1_harvest_list.root GGgridx_Down_hypotest_4Jhighx_${i}__1_harvest_list.root

python makecontourplots_contour_cleaned.py contour_GG1stepgridx_${i}_combSR GGgridx_Nominal_hypotest_combSR_${i}__1_harvest_list.root GGgridx_Up_hypotest_combSR_${i}__1_harvest_list.root GGgridx_Down_hypotest_combSR_${i}__1_harvest_list.root


############################################################################################################################################################
## for multiple contours ##
#python makecontourplots_multiple.py allcontours_GGgridx_${i} GGgridx_Nominal_hypotest_4Jhighx_${i}__1_harvest_list.root,GGgridx_Nominal_hypotest_4Jlowx_${i}__1_harvest_list.root


############################################################################################################################################################
## turn on CLsobs=True @plot_CLs.py
#python makeCLsplots.py CLsobs_GG1stepgridx_${i}_6J GGgridx_Nominal_hypotest_6J_3ifb_version150116__1_harvest_list.root
#python makeCLsplots.py CLsobs_GG1stepgridx_${i}_5J GGgridx_Nominal_hypotest_5J_3ifb_version150116__1_harvest_list.root
#python makeCLsplots.py CLsobs_GG1stepgridx_${i}_4Jlowx GGgridx_Nominal_hypotest_4Jlowx_3ifb_version150116__1_harvest_list.root
#python makeCLsplots.py CLsobs_GG1stepgridx_${i}_4Jhighx GGgridx_Nominal_hypotest_4Jhighx_3ifb_version150116__1_harvest_list.root


## turn on CLsobs=False @plot_CLs.py
#python makeCLsplots.py CLsexp_GG1stepgridx_${i}_6J GGgridx_Nominal_hypotest_6J_3ifb_version150116__1_harvest_list.root
#python makeCLsplots.py CLsexp_GG1stepgridx_${i}_5J GGgridx_Nominal_hypotest_5J_3ifb_version150116__1_harvest_list.root
#python makeCLsplots.py CLsexp_GG1stepgridx_${i}_4Jlowx GGgridx_Nominal_hypotest_4Jlowx_3ifb_version150116__1_harvest_list.root
#python makeCLsplots.py CLsexp_GG1stepgridx_${i}_4Jhighx GGgridx_Nominal_hypotest_4Jhighx_3ifb_version150116__1_harvest_list.root

# not use
#python makecontourplots_multiple_theo.py allcontours_comb_GGgridx_${i} GG1stepgridx_Soft_5J_Nominal_hypotest__1_harvest_list.root,GGgridx_Nominal_hypotest_4Jlowx_3ifb_version150116__1_harvest_list.root,GGgridx_Nominal_hypotest_4Jhighx_3ifb_version150116__1_harvest_list.root,GGgridx_Nominal_hypotest_5J_3ifb_version150116__1_harvest_list.root,GGgridx_Nominal_hypotest_6J_3ifb_version150116__1_harvest_list.root GGgridx_Nominal_hypotest_combSR_3ifb_version150116__1_harvest_list.root,GGgridx_Up_hypotest_combSR_3ifb_version150116__1_harvest_list.root,GGgridx_Down_hypotest_combSR_3ifb_version150116__1_harvest_list.root
