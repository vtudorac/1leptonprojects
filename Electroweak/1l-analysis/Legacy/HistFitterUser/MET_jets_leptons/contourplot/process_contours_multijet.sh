#!/bin/bash

upperLimitDir="upperLimit_results_05042017_T_06_10"

for grid in pMSSM GG2stepWZ
do

    # need to repeat this, since pMSSM and GG2step have different number of columns in the list file
    # (and for upper limits the tree is recreated in the plotting script - so needs the correct description file)
    if [[ -d ${upperLimitDir} ]]
    then
        pushd ${upperLimitDir}
        ln -s ${grid}.root ${grid}_upperlimit.root
        popd
        ./makelistfiles.py ${upperLimitDir}/${grid}_upperlimit.root
        GenerateTreeDescriptionFromJSON.py -f ${grid}_upperlimit__1_harvest_list.json
        ./makecontourhists_multijet.py ${grid}_upperlimit__1_harvest_list
    fi

    ./makecontourplots_contour_cleaned.py ${grid} ${grid}_Nominal__1_harvest_list.root ${grid}_Up__1_harvest_list.root ${grid}_Down__1_harvest_list.root

    if [[ -d ${upperLimitDir} ]]
    then
        ./makecontourplots_contour_cleaned.py -l ${grid}_upperLimit ${grid}_Nominal__1_harvest_list.root ${grid}_Up__1_harvest_list.root ${grid}_Down__1_harvest_list.root
    fi
done
