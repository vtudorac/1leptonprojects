#!/bin/env python

import ROOT
ROOT.gROOT.ProcessLine('#include "contourmacros/CombinationGlob.C"')

ROOT.CombinationGlob.Initialize()

execfile("summary_harvest_tree_description.py")


def limit_plot_SM_multiple( file_nominal, outputfilename='', prefix=None, lumi=21, drawFirstObs=False): 

    #if not 'HiggsSU' in file_nominal:
    #    execfile("summary_harvest_tree_description_SM.py")
    #else: 
    #    execfile("summary_harvest_tree_description_HiggsSU.py")


    if 'GGx12' in file_nominal and not "gridx" in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/limit_contour_GG1stepx12_version150116_combSRcurves.root")
        #file_oldlimit = ROOT.TFile.Open("contourmacros/contour_GG1stepx12_combOneZero_version090215curves.root")
        graph = file_oldlimit.Get("firstObsH_graph0")
        graph3 = file_oldlimit.Get("firstExpH_graph0")
    elif 'GGgridx' in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/limit_contour_GG1stepgridx_version150116_combSRcurves.root")
        #file_oldlimit = ROOT.TFile.Open("contourmacros/contour_GG1stepgridx_combOneZero_version090215curves.root")
        graph = file_oldlimit.Get("firstObsH_graph0")
        graph3 = file_oldlimit.Get("firstExpH_graph0")
    elif 'SSx12' in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/contour_SS1stepx12_combhardsoft_version120115curves.root")
        graph = file_oldlimit.Get("firstObsH_graph")
        graph3 = file_oldlimit.Get("firstExpH_graph")
    elif 'SSgridx' in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/contour_SS1stepgridx_combhardsoft_version141114curves.root")   
        graph = file_oldlimit.Get("firstObsH_graph")
        graph3 = file_oldlimit.Get("firstExpH_graph")
    else:
        file_oldlimit = ROOT.TFile.Open("contourmacros/limit_contour_GG1stepgridx_version150116_combSRcurves.root")
        graph = file_oldlimit.Get("firstObsH_graph0")
        graph3 = file_oldlimit.Get("firstExpH_graph0")
        
    graph.Print()        
    graph.SetFillColor(17)
    graph.SetLineWidth(2)
    graph.SetMarkerStyle(21)
    graph.SetMarkerSize(0.3)
    graph.SetFillStyle(1001)
    graph.SetLineColor(15)
    graph2=graph.Clone()
    graph2.SetLineStyle(3)

    #graph.SetDirectory(0)

    print 'Producing contour plot based on ', file_nominal

    firstObsH = {}
    firstExpH = {}
    histosOrig = []
    firstObs=None
    firstExp=None

    i=0
    for filename in file_nominal.split(","):
        f = ROOT.TFile.Open(filename,'READ')
        firstObs = f.Get( 'sigp1clsf' )
        firstExp = f.Get( 'sigp1expclsf' )
        
        for hizzie in [firstObs,firstExp]: hizzie.SetDirectory(0)
        f.Close()     
        firstObsH[filename] = FixAndSetBorders(filename, firstObs , 'firstObsH'+str(i) , '' , 0 )
        firstExpH[filename] = FixAndSetBorders(filename, firstExp , 'firstExpH'+str(i) , '' , 0 )
        i+=1

    
    # set text style
    ROOT.gStyle.SetPaintTextFormat(".2g")

    # Start drawing
    c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    #c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 ,600, 600)
    
    pwd=ROOT.gDirectory

    # create and draw the frame2 
    if 'GG' in file_nominal and not 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 2000., 150, 25., 1525. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )
    elif 'GG2' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1500., 100, 35., 1025. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'GG' in file_nominal and 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 63,900,2000,75,0.,1.5 )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "x = ( m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}} ) / ( m_{#tilde{g}} - m_{#tilde{#chi}^{0}_{1}} ) " )    
    elif 'SS' in file_nominal and not 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1250., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )
    elif 'SS2CNsl' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1250., 100, 35., 1025. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'SS2WWZZ' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1250., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'SS' in file_nominal and 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 48,240,1200,75,0.,1.5)# 100, 225., 1500., 100, 0, 1. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "x = ( m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}} ) / ( m_{#tilde{q}} - m_{#tilde{#chi}^{0}_{1}} ) " )
    else:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1500., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )

    ROOT.CombinationGlob.SetFrameStyle2D( frame2, 1.0 )

    frame2.GetYaxis().SetTitleOffset(1.35)
 
    frame2.GetXaxis().SetTitleFont( 42 )
    frame2.GetYaxis().SetTitleFont( 42 )
    frame2.GetXaxis().SetLabelFont( 42 )
    frame2.GetYaxis().SetLabelFont( 42 )
 
    frame2.GetXaxis().SetTitleSize( 0.04 )
    frame2.GetYaxis().SetTitleSize( 0.04 )
    frame2.GetXaxis().SetLabelSize( 0.04 )
    frame2.GetYaxis().SetLabelSize( 0.04 )
 
    frame2.Draw()
    
#    if 'GGx12' in file_nominal or 'GGgridx' in file_nominal or 'SSx12' in file_nominal or 'SSgridx' in file_nominal: 
    graph.Draw("Fsame")     
    graph2.Draw("Lsame")
    # Set up the legend
    #leg = ROOT.TLegend(0.14,0.61,0.37,0.73)
    #leg = ROOT.TLegend(0.14,0.44,0.32,0.80)
    #leg = ROOT.TLegend(0.14,0.60,0.3,0.80)
    leg = ROOT.TLegend(0.14,0.53,0.37,0.72)
    
    if 'gridx' in file_nominal:
        leg = ROOT.TLegend(0.61,0.68,0.91,0.85)
        #leg = ROOT.TLegend(0.61,0.65,0.91,0.85)
    leg.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize );
    leg.SetTextSize( 0.03 );
    leg.SetTextFont( 42 );
    leg.SetFillColor( 0 );
    leg.SetFillStyle(1001);

    # Draw the +/- 1 sigma yellow band 
    
    colors = []
    
    for SRregion in file_nominal.split(','):
        if '2J' in SRregion: colors.append(ROOT.CombinationGlob.c_DarkGreen)#
        elif '6JGGx12HM' in SRregion: colors.append(ROOT.CombinationGlob.c_DarkPink)#
        elif '6JGGx12' in SRregion: colors.append(ROOT.CombinationGlob.c_BlueT3)#
        elif 'GG4Jhighx' in SRregion: colors.append(ROOT.CombinationGlob.c_VDarkYellow)#
        elif 'GG4Jlowx' in SRregion: colors.append(ROOT.CombinationGlob.c_DarkOrange)#
        elif '5JSShighx' in SRregion: colors.append(ROOT.CombinationGlob.c_LightBlue)
        elif '4JSSlowx' in SRregion: colors.append(ROOT.CombinationGlob.c_DarkBlue)
        elif '4JSSx12' in SRregion: colors.append(ROOT.CombinationGlob.c_LightRed)
        elif '5JSSx12' in SRregion: colors.append(ROOT.CombinationGlob.c_NovelRed)

        else:
            print "Unknown SR! - Set dummy colour."
            colors.append(ROOT.CombinationGlob.c_DarkGreen)
    
    if len(colors)<len(firstObsH):
        print 'Only have',len(colors),'colors for',len(firstObsH),'histograms.  Will crash...'

    newHists = []

    c2 = ROOT.TCanvas("dummy","dummy", 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    #c2 = ROOT.TCanvas("dummy","dummy", 0 , 0 , 700, 1000)

    j=0
    
    anewhist11=[]#ROOT.TGraph()
    anewhist00=[]#ROOT.TGraph()   
    anewhist000=[]#ROOT.TGraph() 
    for mykey in file_nominal.split(","):   
        try:
            (leg,anewhist1) = DrawContourLine95Graph(mykey, leg, firstExpH[mykey],firstExpH[mykey].GetName()+"_graph", '', colors[j], 6, 2 ) 
            for hist in anewhist1:
                dummy = ROOT.TGraph()
                dummy = hist.Clone()
                anewhist11.append(dummy)
            #anewhist11=anewhist1.Clone()
            #ROOT.gDirectory.Append(anewhist11)
            #(leg,anewhist000) = DummyLegendExpected( leg, 'dummy', 0, 1001, colors[j], 1, 4 )
            #c.cd()
            #anewhist11.Draw("Lsame")
            #newHists.append(anewhist11)
        except:
            print "Expected limit does not exist for "+mykey        

        
        if drawFirstObs:
            try: 
                print mykey
                (leg,anewhist0) = DrawContourLine95Graph(mykey, leg, firstObsH[mykey],firstObsH[mykey].GetName()+"_graph", '', colors[j],1,4)
                for hist in anewhist0:
                    dummy = ROOT.TGraph()
                    dummy = hist.Clone()
                    anewhist00.append(dummy)                
                #anewhist00=anewhist0.Clone()
                #c.cd()                
                leg.AddEntry(anewhist00[-1],InterpretNames(mykey),'l')
                #newHists.append(anewhist00)
            except:
                print "Observed limit does not exist for "+mykey             
        j+=1
        
                        
    newHists.append(anewhist11)         
    newHists.append(anewhist00)

    pwd.cd()
    #pwd.ls()
    c.cd()
    c.Update() 
    
    #print newHists
    
    for h in newHists:
        #print h
        for h2 in h:
            #print h2
            h2.Draw("Lsame")
     
    c.Update()
    
    #if 'GGx12' in file_nominal or 'GGgridx' in file_nominal or 'SSx12' in file_nominal or 'SSgridx' in file_nominal: 
    #    graph.Draw("Fsame") 

    # legend
    textSizeOffset = +0.000;
    xmax = frame2.GetXaxis().GetXmax()
    xmin = frame2.GetXaxis().GetXmin()
    ymax = frame2.GetYaxis().GetXmax()
    ymin = frame2.GetYaxis().GetXmin()
    dx   = xmax - xmin
    dy   = ymax - ymin

    # Label the decay process
    Leg0 = None
    if   'GGx12' in file_nominal and not "gridx" in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrowqqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x = (m(#tilde{#chi}^{#pm}_{1}) - m(#tilde{#chi}^{0}_{1}) ) / ( m(#tilde{g}) - m(#tilde{#chi}^{0}_{1}) ) = 1/2" ) ##tilde{g}-#tilde{g}, #tilde{g}#rightarrow q#bar{q}'W#tilde{#chi}_{1}^{0}" )
    elif 'GGgridx' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrowqqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, m(#tilde{#chi}_{1}^{0}) = 60 GeV" ) ##tilde{g}-#tilde{g}, #tilde{g}#rightarrow q#bar{q}'W#tilde{#chi}_{1}^{0}" )    
    elif 'SSx12' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} #rightarrow qqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x=1/2" )
    elif 'SSgridx' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} #rightarrow qqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, m_{#tilde{#chi}_{1}^{0}} = 60 GeV" )    
    elif 'GG2CNsl' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} decays via sleptons/sneutrinos: #tilde{g}#tilde{g}#rightarrow qqqq(llll/lll#nu/ll#nu#nu/l#nu#nu#nu/#nu#nu#nu#nu)#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    elif 'SS2CNsl' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} decays via sleptons/sneutrinos: #tilde{q}#tilde{q}#rightarrow qq(llll/lll#nu/ll#nu#nu/l#nu#nu#nu/#nu#nu#nu#nu)#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    elif 'GG2WWZZ' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} decays via WWZZ: #tilde{g}#tilde{g}#rightarrow qqqq#tilde{#chi}_{1}^{#pm}#tilde{#chi}_{1}^{#pm} #rightarrow qqqqWZWZ#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    elif 'SS2WWZZ' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} decays via WWZZ: #tilde{q}#tilde{q}#rightarrow qq#tilde{#chi}_{1}^{#pm}#tilde{#chi}_{1}^{#pm} #rightarrow qqWZWZ#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    else: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrow qqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x=1/2" )

    if Leg0 is not None:
        Leg0.SetTextAlign( 11 );
        Leg0.SetTextFont( 42 );
        Leg0.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
        Leg0.SetTextColor( 1 );
        Leg0.AppendPad();

    #Leg3 = ROOT.TLatex( (xmin+xmax)*0.6, ymax + dy*0.025, 'Expected Limit Comparison' )
    #Leg3.SetTextAlign( 11 );
    #Leg3.SetTextFont( 42 );
    #Leg3.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
    #Leg3.SetTextColor( 1 );
    #Leg3.AppendPad();


    #frame2.Draw( 'sameaxis' )
    #leg.Draw( 'same' )
    
    leg.Draw( 'same' )
    Leg1 = ROOT.TLatex()
    Leg1.SetNDC()
    Leg1.SetTextAlign( 11 )
    Leg1.SetTextFont( 42 )
    Leg1.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg1.SetTextColor( 1 )
    Leg1.DrawLatex(0.16,0.75, '#sqrt{s}=13 TeV, %.1f fb^{-1}'%(lumi) )
    #Leg1.DrawLatex(0.65,0.75, '#sqrt{s}=13 TeV, %.1f fb^{-1}'%(lumi) )
    #Leg1.DrawLatex(0.16,0.75, '#sqrt{s}=13 TeV' )
    Leg1.AppendPad()

    Leg2 = ROOT.TLatex()
    Leg2.SetNDC()
    Leg2.SetTextAlign( 11 )
    Leg2.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg2.SetTextColor( 1 )
    Leg2.SetTextFont(42)
    if prefix is not None: 
        Leg2.DrawLatex(0.16,0.82,prefix)
        Leg2.AppendPad()
        
    c.Update()
    
    frame2.Draw( 'sameaxis' )
    #leg.Draw( 'same' )
    
    # Diagonal line
    diagonal = None 
    if ("SS" in file_nominal.split(",")[0] and not 'gridx' in file_nominal.split(",")[0]) or ("GG" in file_nominal.split(",")[0] and not 'gridx' in file_nominal.split(",")[0]) and not 'WWZZ' in file_nominal.split(",")[0]: diagonal = ROOT.TLine(225., 225., 1525., 1525.)
    elif 'gridx' in file_nominal.split(",")[0]:
        diagonal = ROOT.TLine(900., 1., 2000., 1.)
    if diagonal: 
        diagonal.SetLineStyle(2)
        if not '_m12EE60_' in file_nominal.split(",")[0]: diagonal.Draw()
    
    gtt = ROOT.TLatex(550,590,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")
    if "SS" in file_nominal.split(",")[0]:
        gtt.SetText(750,780,"m_{#tilde{q}} < m_{#tilde{#chi}^{0}_{1}}")
    if "GG" in file_nominal.split(",")[0]:
        gtt.SetText(750,790,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")
    #else: gtt.SetText(550,590,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")

    gtt.SetTextSize(0.03)
    gtt.SetTextColor(12)
    gtt.SetTextAngle(43)
    if "SS" in file_nominal:
        gtt.SetTextAngle(40)
    gtt.SetTextFont(42)

    if not "gridx" in file_nominal.split(",")[0]:
        gtt.Draw("same")    
        
              
    leg2 = ROOT.TLegend(0.61,0.85,0.91,0.92)    
    leg2.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize );
    leg2.SetTextSize( 0.03 );
    leg2.SetTextFont( 42 );
    leg2.SetFillColor( 0 );
    leg2.SetFillStyle(1001);
    #leg2.AddEntry(graph,"#splitline{1-2 lepton(s)+ jets + E_{T}^{miss}}{arxiv:1208.4688}","F")
    if 'x12' in file_nominal or 'gridx' in file_nominal:
        leg2.AddEntry(graph,"ATLAS 8 TeV, 20.3 fb^{-1}","F")
    elif "WWZZ" in file_nominal:
        leg2.AddEntry(twostepexclusion_curve,"PRD 86 (2012) 092002","F")
    if 'x12' in file_nominal.split(",")[0] or 'gridx' in file_nominal.split(",")[0] or 'WWZZ' in file_nominal.split(",")[0]: 
        leg2.Draw("same")

    if 'gridx' in file_nominal:
        leg.Draw( 'same' )
    # update the canvas
    c.Update()
    
    clslimits = ROOT.TLatex()
    #clslimits = ROOT.TLatex(360,887-197,"All limits at 95% CL_{S}")
    clslimits.SetNDC()
    clslimits.SetTextSize(0.03)
    clslimits.SetTextFont(42)
    if not 'gridx' in file_nominal.split(",")[0]:
        clslimits.DrawLatex(0.15,0.5,"All limits at 95% CL") 
    else:
        clslimits.DrawLatex(0.17,0.7,"All limits at 95% CL")  
  
    atlasLabel = ROOT.TLatex()
    atlasLabel.SetNDC()
    atlasLabel.SetTextFont(72)
    atlasLabel.SetTextColor(ROOT.kBlack)
    atlasLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    atlasLabel.DrawLatex(0.6, 0.85,"ATLAS")
    #else:
    atlasLabel.DrawLatex(0.16, 0.87,"ATLAS")
    atlasLabel.AppendPad()

    progressLabel = ROOT.TLatex()
    progressLabel.SetNDC()
    progressLabel.SetTextFont(42)
    progressLabel.SetTextColor(ROOT.kBlack)
    progressLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    progressLabel.DrawLatex(0.76, 0.85,"Internal")
    #else:
    #print 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())
    progressLabel.DrawLatex(0.16+0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()), 0.87,"Internal")
    #else: progressLabel.DrawLatex(0.19, 0.81,"Internal")
    progressLabel.AppendPad()
    
    #now expected lines for legend
    expectedline = ROOT.TLine()
    expectedline.SetLineStyle(6)
    expectedline.SetLineWidth(2)
    offset=0.
    if 'x12' in file_nominal.split(',')[0] and not 'gridx' in file_nominal.split(',')[0]:
        for counter in colors:
            print counter
            expectedline.SetLineColor(counter)
            expectedline.DrawLineNDC(0.149,0.685-offset*0.047,0.191,0.685-offset*0.047)
            offset+=1.
    if 'gridx' in file_nominal.split(',')[0]:
        for counter in colors:
            print offset, ' colors=',counter
            expectedline.SetLineColor(counter)
            #expectedline.DrawLineNDC(0.621,0.826-offset*0.021,0.675,0.826-offset*0.021)  # 8
            expectedline.DrawLineNDC(0.621,0.816-offset*0.042,0.675,0.816-offset*0.042) # 4
            offset+=1.


    c.Update()    
    
    print "Here"
    
    if outputfilename=='':
        outFileNom = 'plots/limit_'+file_nominal.split(",")[0].replace('.root','')
    else:
        outFileNom = 'plots/limit_'+outputfilename

    ROOT.CombinationGlob.imgconv( c, outFileNom );

    del leg
    del frame2


def MirrorBorders( hist ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    # corner points
    hist.SetBinContent(0,0,hist.GetBinContent(1,1))
    hist.SetBinContent(numx+1,numy+1,hist.GetBinContent(numx,numy))
    hist.SetBinContent(numx+1,0,hist.GetBinContent(numx,1))
    hist.SetBinContent(0,numy+1,hist.GetBinContent(1,numy))

    # Fix the other points 
    for i in xrange(1,numx+1):
        hist.SetBinContent(i,0,       hist.GetBinContent(i,1));
        hist.SetBinContent(i,numy+1, hist.GetBinContent(i,numy));
    for i in xrange(1,numy+1):
        hist.SetBinContent(0,i,      hist.GetBinContent(1,i));
        hist.SetBinContent(numx+1,i, hist.GetBinContent(numx,i));


def AddBorders( hist, name='StupidName', title='StupidTitle'):
    nbinsx = hist.GetNbinsX()
    nbinsy = hist.GetNbinsY()
  
    xbinwidth = ( hist.GetXaxis().GetBinCenter(nbinsx) - hist.GetXaxis().GetBinCenter(1) ) / float(nbinsx-1)
    ybinwidth = ( hist.GetYaxis().GetBinCenter(nbinsy) - hist.GetYaxis().GetBinCenter(1) ) / float(nbinsy-1)
  
    xmin = hist.GetXaxis().GetBinCenter(0) - xbinwidth/2. 
    xmax = hist.GetXaxis().GetBinCenter(nbinsx+1) + xbinwidth/2. 
    ymin = hist.GetYaxis().GetBinCenter(0) - ybinwidth/2. 
    ymax = hist.GetYaxis().GetBinCenter(nbinsy+1) + ybinwidth/2. 
  
    hist2 = ROOT.TH2F(name, title, nbinsx+2, xmin, xmax, nbinsy+2, ymin, ymax);
  
    for ibin1 in xrange(hist.GetNbinsX()+2):
        for ibin2 in xrange(hist.GetNbinsY()+2):
            hist2.SetBinContent( ibin1+1, ibin2+1, hist.GetBinContent(ibin1,ibin2) );
  
    return hist2


def SetBorders( hist, val=0 ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    for i in xrange(numx+2):
        hist.SetBinContent(i,0,val);
        hist.SetBinContent(i,numy+1,val);
    for i in xrange(numy+2):
        hist.SetBinContent(0,i,val);
        hist.SetBinContent(numx+1,i,val);


def FixAndSetBorders(file_nominal, hist, name='hist3', title='hist3', val=0 ):
    hist0 = hist.Clone() # histogram we can modify
    
    #if not "gridx" in file_nominal:
    if not "gridx" in file_nominal and not "CNsl" in file_nominal:
        MirrorBorders( hist0 ) # mirror values of border bins into overflow bins
        #if 'GG1stepx12' in file_nominal:
        #    MirrorBorders( hist0 )
        #    MirrorBorders( hist0 )
        #    MirrorBorders( hist0 )
        #    print "Here"
    
    hist1 = AddBorders( hist0, "hist1", "hist1" );   
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    SetBorders( hist1, val );                              
    # set overflow bins to value 1
    
    histX = AddBorders( hist1, "histX", "histX" )
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    
    hist3 = histX.Clone()
    hist3.SetName( name )
    hist3.SetTitle( title )
    
    del hist0, hist1, histX
    return hist3 # this can be used for filled contour histograms


def DrawContourLine95( leg, hist, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2 ):
    # contour plot
    h = hist.Clone()
    h.SetContour(1)
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
  
    #print linewidth, linestyle
    h.SetLineColor( linecolor )
    h.SetLineWidth( linewidth )
    h.SetLineStyle( linestyle )
    #h.Print()
    h.Draw( "samecont3" );
    
    if text is not '': leg.AddEntry(h,text,'l')
    return leg,h
    
def DrawContourLine95Graph(file_nominal, leg, hist,name, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2 ):
    # contour plot
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    gr = gr0.Clone(h.GetName())
    h.SetContour(1)
    
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    #print "Number of lists: ",
    #print list0.GetEntries()
    if list0.GetEntries()==0:
        return None
    
    #list0.Print()
    
    all_graphs = []
    
    for k in xrange(list0.GetSize()):
        gr = list0[k]
        #gr.Print()
        #grTmp = ROOT.TGraph()

        #for k in xrange(list0.GetSize()):
        #    if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
        gr.SetName(name+str(k))
        #print gr
        #print "\n"
        #gr.Print()
        #print linewidth, linestyle
        gr.SetLineColor( linecolor )
        gr.SetLineWidth( linewidth )
        gr.SetLineStyle( linestyle )
        #h.Print()
        #gr.Draw( "same" );
    
        points_to_be_removed=[]
        if 'GG2CNsl' in file_nominal or 'GG2WWZZ' in file_nominal or 'SS2CNsl' in file_nominal  or 'HiggsSU' in file_nominal:
            for j in range(0,gr.GetN()):
                #print gr.GetX()[j],gr.GetY()[j],(gr.GetX()[j] - 380.)*(-0.36)+265.5
                #if 'GG2CNsl' in file_nominal:
                #    if (gr.GetX()[j] - 380.)*(-0.32)+265.5 > gr.GetY()[j]:
                #        points_to_be_removed.append(j)
                if 'GG2WWZZ' in file_nominal:
                    if (gr.GetX()[j] - 475.)*(-0.35)+282. > gr.GetY()[j] and gr.GetX()[j] < 1050. and gr.GetX()[j]>435. or (gr.GetX()[j]<491. and gr.GetY()[j]<282.):# or (gr.GetX()[j] < 420. and gr.GetY()[j]<286.):
                        points_to_be_removed.append(j)    
                #if 'SS2CNsl' in file_nominal:
                #    if (gr.GetX()[j] - 325.)*(-0.65)+205.5 > gr.GetY()[j] and gr.GetX()[j]>400. or (gr.GetX()[j] < 400. and gr.GetY()[j]<200.):
                #        points_to_be_removed.append(j)
                if 'HiggsSU' in file_nominal:
                    if gr.GetX()[j] < 1500. and gr.GetY()[j]<450.:
                        points_to_be_removed.append(j)    
    
            #print points_to_be_removed    
            point_counter = 0
            for point in points_to_be_removed:
                gr.RemovePoint(point-point_counter)
                point_counter+=1
            #gr.Sort()     
            gr.Print()    
        
            cut_points = 0.
            x=gr.GetX()
            y=gr.GetY()
            for j2 in range(1,gr.GetN()):
                if x[j2]<x[j2-1] and x[j2]<500.:
                    cut_points=j2
                    break
    
            gr2 = ROOT.TGraph()
            j4=0
            for j3 in range(cut_points,gr.GetN()):
                gr2.SetPoint(j4,x[j3],y[j3])
                j4+=1
            for j5 in range(0,cut_points):
                gr2.SetPoint(j4,x[j5],y[j5])
                j4+=1
            gr2.SetLineColor( linecolor )
            gr2.SetLineWidth( linewidth )
            gr2.SetLineStyle( linestyle )
            gr2.SetName(name+"2")
            all_graphs+=[gr2]
        else:
            all_graphs+=[gr]
            
    if text is not '': leg.AddEntry(all_graphs[0],text,'l')
    return leg,all_graphs    


def ContourGraph(file_nominal, hist ):
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    #h.Print()
    h.GetYaxis().SetRangeUser(0,1500)
    h.GetXaxis().SetRangeUser(100,6100)
    gr = gr0.Clone(h.GetName())
    h.SetContour( 1 )
 
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    if list0.GetEntries()==0:
        return None

    #list.Print()
    gr = list0[0]
    #gr.Print()
    #grTmp = ROOT.TGraph()

    for k in xrange(list0.GetSize()):
        if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
    gr.SetName(hist.GetName())
    #print gr
    #gr.Print() 
    #print "\n"   
    
    points_to_be_removed=[]
    if 'GG2CNsl' in file_nominal or 'GG2WWZZ' in file_nominal or 'SS2CNsl' in file_nominal or 'HiggsSU' in file_nominal:
        for j in range(0,gr.GetN()):
            #print gr.GetX()[j],gr.GetY()[j],(gr.GetX()[j] - 380.)*(-0.36)+265.5
            #if 'GG2CNsl' in file_nominal:
            #    if (gr.GetX()[j] - 380.)*(-0.32)+265.5 > gr.GetY()[j]:
            #        points_to_be_removed.append(j)
            if 'GG2WWZZ' in file_nominal:
                #if (gr.GetX()[j] - 418.)*(-0.35)+282. > gr.GetY()[j] and gr.GetX()[j] < 1000. and gr.GetX()[j]>435. or (gr.GetX()[j]>1000. and gr.GetY()[j]<65.) or (gr.GetX()[j]>1100. and gr.GetY()[j]<65.) or (gr.GetX()[j] < 416. and gr.GetY()[j]<265.):# or  (gr.GetX()[j] > 410. and gr.GetX()[j] < 420. and gr.GetY()[j]<241.) or (gr.GetX()[j] < 416. and gr.GetY()[j]<265.) and gr.GetX()[j]>435.
                if (gr.GetX()[j] - 450.)*(-0.35)+282. > gr.GetY()[j] and gr.GetX()[j] < 1050. and gr.GetX()[j]>504. or (gr.GetX()[j] < 520. and gr.GetY()[j]<265.) or (gr.GetX()[j] < 491. and gr.GetY()[j]<286.) or (gr.GetX()[j] < 530. and gr.GetY()[j]<263.):# or (gr.GetX()[j] < 1063. and gr.GetY()[j]<110.):# or (gr.GetX()[j] > 1063. and gr.GetX()[j] < 1187. and gr.GetY()[j]<168.):# or  (gr.GetX()[j] > 410. and gr.GetX()[j] < 420. and gr.GetY()[j]<241.) or (gr.GetX()[j] < 416. and gr.GetY()[j]<265.) and gr.GetX()[j]>435.
                    points_to_be_removed.append(j)
            #if 'SS2CNsl' in file_nominal:
            #    if (gr.GetX()[j] - 325.)*(-0.65)+205.5 > gr.GetY()[j] and gr.GetX()[j]>420. or (gr.GetX()[j] < 400. and gr.GetY()[j]<200.) or (gr.GetX()[j] > 400. and gr.GetX()[j] < 420. and gr.GetY()[j]<165.):
            #        points_to_be_removed.append(j)          
            if 'HiggsSU' in file_nominal:
                if gr.GetX()[j] < 1500. and gr.GetY()[j]<450.:
                    points_to_be_removed.append(j)   

        #print points_to_be_removed    
        point_counter = 0
        for point in points_to_be_removed:
             gr.RemovePoint(point-point_counter)
             point_counter+=1
        #gr.Sort()     
        #gr.Print()
        #print "\n"    
        
        cut_points = 0
        x=gr.GetX()
        y=gr.GetY()
        for j2 in range(1,gr.GetN()):
            if x[j2]<x[j2-1] and x[j2]<500.:
                cut_points=j2
                break
    
        print cut_points
        gr2 = ROOT.TGraph()
        j4=0
        for j3 in range(cut_points,gr.GetN()):
            gr2.SetPoint(j4,x[j3],y[j3])
            j4+=1
        for j5 in range(0,cut_points):
            gr2.SetPoint(j4,x[j5],y[j5])
            j4+=1

        return gr2
    
    return gr


def DrawExpectedBand( gr1, gr2, fillColor, fillStyle, cut = 0):

    number_of_bins = max(gr1.GetN(),gr2.GetN());
   
    gr1N = gr1.GetN();
    gr2N = gr2.GetN();
    #gr2.Print()

    N = number_of_bins;
   
    xx0 = ROOT.Double(0)
    yy0 = ROOT.Double(0)
    x0 = ROOT.Double(0)
    y0 = ROOT.Double(0)    
    xx1 = ROOT.Double(0)
    yy1 = ROOT.Double(0)
   
    x1=[]
    y1=[]
    x2=[]
    y2=[]    
   
    for j in xrange(gr1N):
        gr1.GetPoint(j,xx0,yy0)
        x1 += [float(xx0)]
        y1 += [float(yy0)]
    if gr1N < N:
        for i in xrange(gr1N,N):
            x1 += [ float(x1[gr1N-1]) ]
            y1 += [ float(y1[gr1N-1]) ]
   
    #gr2.Print()
    for j in xrange(gr2N):
        gr2.GetPoint(j,xx1,yy1)
        x2 += [float(xx1)]
        y2 += [float(yy1)]
    if gr2N < N:
        for i in xrange(gr2N,N):
            x2 += [ float(x2[gr2N-1]) ]
            y2 += [ float(y2[gr2N-1]) ]
            
    #print x1
    #print x2


    grshade = ROOT.TGraphAsymmErrors(2*N)
    for i in xrange(N):
        if x1[i] > cut:
            grshade.SetPoint(i,x1[i],y1[i])
        if x2[N-i-1] > cut:
            grshade.SetPoint(N+i,x2[N-i-1],y2[N-i-1])
   
    #grshade.Print()
   
    # Apply the cut in the shade plot if there is something that doesnt look good...
    Nshade = grshade.GetN()
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0!=0 and y0!=0 :
            x00 = x0
            y00 = y0
            break
            
    #print x00, y00
   
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0 == 0 and y0 == 0:
            grshade.SetPoint(j,x00,y00)
   
    #grshade.Print()
   
    # Now draw the plot...
    grshade.SetFillStyle(fillStyle);
    grshade.SetFillColor(fillColor);
    grshade.SetMarkerStyle(21);
    #grshade.Draw("Fsame");
    return grshade;


def DummyLegendExpected(leg, what,  fillColor, fillStyle, lineColor, lineStyle, lineWidth):
    gr = ROOT.TGraph()
    gr.SetFillColor(fillColor)
    gr.SetFillStyle(fillStyle)
    gr.SetLineColor(lineColor)
    gr.SetLineStyle(lineStyle)
    gr.SetLineWidth(lineWidth)
    leg.AddEntry(gr,what,"LF")
    return leg,gr


def InterpretNames(filename):
     if '2J' in filename: descriptionname = "GG/SS 2J (obs./exp.)"     
     elif '6JGGx12HM' in filename: descriptionname = "GG 6J high-mass (obs./exp.)"     
     elif '6JGGx12' in filename: descriptionname = "GG 6J bulk (obs./exp.)"     
     elif 'GG4Jhighx' in filename: descriptionname = "GG 4J high-x (obs./exp.)" 
     elif 'GG4Jlowx' in filename: descriptionname = "GG 4J low-x (obs./exp.)" 
     elif '5JSShighx' in filename: descriptionname = "SS 5J high-x (obs./exp.)"
     elif '4JSSlowx' in filename: descriptionname = "SS 5J low-x (obs./exp.)"
     elif '4JSSx12' in filename: descriptionname = "SS 4J x=1/2 (obs./exp.)"
     elif '5JSSx12' in filename: descriptionname = "SS 5J x=1/2 (obs./exp.)"


     else: descriptionname = "unknown region"
     
     return descriptionname
     
hh=[]     

def DrawContourMassLine(hist, mass, color=14 ):

  h = ROOT.TH2F( hist )
  hh.append(h)
  h.SetContour( 1 )
  h.SetContourLevel( 0, mass )

  h.SetLineColor( color )
  h.SetLineStyle( 7 )
  h.SetLineWidth( 1 )
  #h.Print("range")
  h.Draw( "samecont3" )
  
  return
  
