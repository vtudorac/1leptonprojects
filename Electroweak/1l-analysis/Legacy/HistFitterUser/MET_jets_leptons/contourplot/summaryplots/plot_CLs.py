#!/bin/env python

import ROOT
ROOT.gROOT.ProcessLine('#include "contourmacros/CombinationGlob.C"')
ROOT.CombinationGlob.Initialize()

#execfile("summary_harvest_tree_description_SM.py")


def plot_CLs( file_nominal, outputfilename='', prefix=None, lumi=21, upperlimit=False, printCLs=True, CLsobs=False): 

    print 'Producing CLs plot based on ', file_nominal


    
    # set text style
    ROOT.gStyle.SetPaintTextFormat(".2g")

    # Start drawing
    c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    #c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 ,600, 600)
    
    pwd=ROOT.gDirectory

    # create and draw the frame2 
    if 'GG' in file_nominal and not 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 2000., 150, 25., 1525. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )
    elif '_GG2' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1500., 100, 35., 1025. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'GG' in file_nominal and 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 63,900,2000,75,0.,1.5 )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "x = ( m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}} ) / ( m_{#tilde{g}} - m_{#tilde{#chi}^{0}_{1}} ) " )    
    elif 'SS' in file_nominal and not 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1250., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )
    elif '_SS2CNsl' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1250., 100, 35., 1025. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif '_SS2WWZZ' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1250., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'SS' in file_nominal and 'gridx' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 48,240,1200,75,0.,1.5)# 100, 225., 1500., 100, 0, 1. )
        frame2.SetXTitle( "m_{#tilde{q}} [GeV]" )
        frame2.SetYTitle( "x = ( m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}} ) / ( m_{#tilde{q}} - m_{#tilde{#chi}^{0}_{1}} ) " )
    else:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1500., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )

    ROOT.CombinationGlob.SetFrameStyle2D( frame2, 1.0 )

    frame2.GetYaxis().SetTitleOffset(1.35)
 
    frame2.GetXaxis().SetTitleFont( 42 )
    frame2.GetYaxis().SetTitleFont( 42 )
    frame2.GetXaxis().SetLabelFont( 42 )
    frame2.GetYaxis().SetLabelFont( 42 )
 
    frame2.GetXaxis().SetTitleSize( 0.04 )
    frame2.GetYaxis().SetTitleSize( 0.04 )
    frame2.GetXaxis().SetLabelSize( 0.04 )
    frame2.GetYaxis().SetLabelSize( 0.04 )
 
    frame2.Draw()
    
    # legend
    textSizeOffset = +0.000;
    xmax = frame2.GetXaxis().GetXmax()
    xmin = frame2.GetXaxis().GetXmin()
    ymax = frame2.GetYaxis().GetXmax()
    ymin = frame2.GetYaxis().GetXmin()
    dx   = xmax - xmin
    dy   = ymax - ymin

    # Label the decay process
    Leg0 = None
    if   'GGx12' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrowqqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x = (m(#tilde{#chi}^{#pm}_{1}) - m(#tilde{#chi}^{0}_{1}) ) / ( m(#tilde{g}) - m(#tilde{#chi}^{0}_{1}) ) = 1/2" ) ##tilde{g}-#tilde{g}, #tilde{g}#rightarrow q#bar{q}'W#tilde{#chi}_{1}^{0}" )
    elif   'GGgridx' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrowqqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, m(#tilde{#chi}_{1}^{0}) = 60 GeV" ) ##tilde{g}-#tilde{g}, #tilde{g}#rightarrow q#bar{q}'W#tilde{#chi}_{1}^{0}" )    
    elif 'SSx12' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} #rightarrow qqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x=1/2" )
    elif 'SSgridx' in file_nominall: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} #rightarrow qqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, m_{#tilde{#chi}_{1}^{0}} = 60 GeV" )    
    elif '_GG2CNsl' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} decays via sleptons/sneutrinos: #tilde{g}#tilde{g}#rightarrow qqqq(llll/lll#nu/ll#nu#nu/l#nu#nu#nu/#nu#nu#nu#nu)#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    elif '_SS2CNsl' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} decays via sleptons/sneutrinos: #tilde{q}#tilde{q}#rightarrow qq(llll/lll#nu/ll#nu#nu/l#nu#nu#nu/#nu#nu#nu#nu)#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    elif '_GG2WWZZ' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} decays via WWZZ: #tilde{g}#tilde{g}#rightarrow qqqq#tilde{#chi}_{1}^{#pm}#tilde{#chi}_{1}^{#pm} #rightarrow qqqqWZWZ#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    elif '_SS2WWZZ' in file_nominal:          Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{q}-#tilde{q} decays via WWZZ: #tilde{q}#tilde{q}#rightarrow qq#tilde{#chi}_{1}^{#pm}#tilde{#chi}_{1}^{#pm} #rightarrow qqWZWZ#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}" )
    else: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrow qqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x=1/2" )
    


    if Leg0 is not None:
        Leg0.SetTextAlign( 11 );
        Leg0.SetTextFont( 42 );
        Leg0.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
        Leg0.SetTextColor( 1 );
        Leg0.AppendPad();
               
    #plotting of legends and labels etc.
    atlasLabel = ROOT.TLatex()
    atlasLabel.SetNDC()
    atlasLabel.SetTextFont(72)
    atlasLabel.SetTextColor(ROOT.kBlack)
    atlasLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    atlasLabel.DrawLatex(0.6, 0.85,"ATLAS")
    #else:
    if not 'HiggsSU' in file_nominal and not 'gridx' in file_nominal: atlasLabel.DrawLatex(0.16, 0.87,"ATLAS")
    elif 'gridx' in file_nominal: atlasLabel.DrawLatex(0.16, 0.87,"ATLAS")
    else: atlasLabel.DrawLatex(0.19, 0.87,"ATLAS")
    atlasLabel.AppendPad()

    progressLabel = ROOT.TLatex()
    progressLabel.SetNDC()
    progressLabel.SetTextFont(42)
    progressLabel.SetTextColor(ROOT.kBlack)
    progressLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    progressLabel.DrawLatex(0.76, 0.85,"Internal")
    #else:
    #print 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())
    if not "HiggsSU" in file_nominal and not 'gridx' in file_nominal: progressLabel.DrawLatex(0.16+0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()), 0.87,"Internal")
    elif 'gridx' in file_nominal: progressLabel.DrawLatex(0.16+0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()), 0.87,"Internal")
    #else: progressLabel.DrawLatex(0.19, 0.81,"Internal")
    else: 
        progressLabel.SetTextSize( 0.04 )
        progressLabel.DrawLatex(0.19, 0.82,"Internal")
    progressLabel.AppendPad()

    c.Update()  

    #leg.Draw( 'same' )
    Leg1 = ROOT.TLatex()
    Leg1.SetNDC()
    Leg1.SetTextAlign( 11 )
    Leg1.SetTextFont( 42 )
    Leg1.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg1.SetTextColor( 1 )
    Leg1.DrawLatex(0.16,0.70, '#sqrt{s}=13 TeV, %.1f fb^{-1}'%(lumi) )
    Leg1.AppendPad()

    Leg2 = ROOT.TLatex()
    Leg2.SetNDC()
    Leg2.SetTextAlign( 11 )
    Leg2.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg2.SetTextColor( 1 )
    Leg2.SetTextFont(42)
    if prefix is not None: 
        Leg2.DrawLatex(0.16,0.82,prefix)
        Leg2.AppendPad()
        
    Leg3 = ROOT.TLatex()
    Leg3.SetNDC()
    Leg3.SetTextAlign( 11 )
    Leg3.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg3.SetTextColor( 1 )
    Leg3.SetTextFont(42)
    SRname = ""
    if not 'Soft' in file_nominal:
        if '6J' in file_nominal: SRname = 'Hard-lepton 6-jet'
        elif '5J' in file_nominal: SRname = 'Hard-lepton 5-jet'
        elif '4Jlowx' in file_nominal: SRname = 'Hard-lepton 4-jet low-x' 
        elif '4Jhighx' in file_nominal: SRname = 'Hard-lepton 4-jet high-x'    
    elif 'Soft' in file_nominal:
        if '2J' in file_nominal: SRname = 'Soft-lepton 2-jet'
        elif '5J' in file_nominal: SRname = 'Soft-lepton 5-jet'
    Leg3.DrawLatex(0.16,0.76,SRname)
    Leg3.AppendPad()
                
        
    c.Update()

    frame2.Draw( 'sameaxis' )

    # Diagonal line
    diagonal = None 
    if ("SS" in file_nominal and not 'gridx' in file_nominal) or ("GG" in file_nominal and not 'gridx' in file_nominal) and not 'WWZZ' in file_nominal: diagonal = ROOT.TLine(225., 225., 1525., 1525.)
    elif 'gridx' in file_nominal:
        diagonal = ROOT.TLine(900., 1., 2000., 1.)
    if diagonal: 
        diagonal.SetLineStyle(2)
        if not '_m12EE60_' in file_nominal: diagonal.Draw()
    
    gtt = ROOT.TLatex(550,590,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")
    if "SS" in file_nominal:
        gtt.SetText(750,780,"m_{#tilde{q}} < m_{#tilde{#chi}^{0}_{1}}")
    if "GG" in file_nominal:
        gtt.SetText(750,790,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")
    #else: gtt.SetText(550,590,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")

    gtt.SetTextSize(0.03)
    gtt.SetTextColor(12)
    gtt.SetTextAngle(43)
    if "SS" in file_nominal:
        gtt.SetTextAngle(40)
    gtt.SetTextFont(42)

    if not "gridx" in file_nominal:
        gtt.Draw("same")    

    # update the canvas
    c.Update()   
    
    if printCLs:
    
        listTree = harvesttree(file_nominal.replace(".root",""))                     
        nentries = listTree.GetEntries()
        print nentries
                           
        pointLabel = ROOT.TLatex()
        pointLabel.SetTextSize(0.02)
        pointLabel.SetTextFont(42)
        pointLabel.SetTextColor(ROOT.kGray+2)
        if CLsobs==True:
            fout_CLs = open(outputfilename+"CLs.txt","w")
        else: 
            fout_CLs = open(outputfilename+"CLsexp.txt","w")

        #need to use now a nasty hack due to changes in ROOT -- required for Root versions after 5.34.11
        from array import array
        m0floatbranch = array('f',[0])
        m12floatbranch = array('f',[0])
        mchfloatbranch = array('f',[0])        
        CLsfloatbranch = array('f',[0])
        listTree.SetBranchStatus("G",1)
        listTree.SetBranchStatus("LSP",1)
        listTree.SetBranchStatus("C",1)        
        listTree.SetBranchAddress("G",m0floatbranch)
        listTree.SetBranchAddress("LSP",m12floatbranch) 
        listTree.SetBranchAddress("C",mchfloatbranch)         
        if CLsobs:
            listTree.SetBranchStatus("CLs",1)
            listTree.SetBranchAddress("CLs",CLsfloatbranch)
        else:
            listTree.SetBranchStatus("CLsexp",1)
            listTree.SetBranchAddress("CLsexp",CLsfloatbranch)            

        for entry in range(nentries):
            status = listTree.GetEntry(entry)
            mgluino = m0floatbranch[0]
            mLSP = m12floatbranch[0]
            mch = mchfloatbranch[0]    
            X=(mch - mLSP)/(mgluino - mLSP)
            CLs = CLsfloatbranch[0]
            if not 'gridx' in file_nominal and (mgluino < frame2.GetXaxis().GetXmin() or mgluino>frame2.GetXaxis().GetXmax()-50. or mLSP>frame2.GetYaxis().GetXmax()-15. or mLSP<frame2.GetYaxis().GetXmin()):
                continue
            if 'gridx' in file_nominal and (mgluino < frame2.GetXaxis().GetXmin() or mgluino>frame2.GetXaxis().GetXmax()-5. or X>frame2.GetYaxis().GetXmax() or X<frame2.GetYaxis().GetXmin()):
                continue    
            if CLs<0.01:
                if 'gridx' in file_nominal:
                    pointLabel.DrawLatex(mgluino,X,"%1.3f" % (CLs))
                    fout_CLs.write("{0:1.0f}; {1:1.3f}; {2:1.3f};\n".format(mgluino,X,CLs))
                else:
                    pointLabel.DrawLatex(mgluino,mLSP,"%1.3f" % (CLs))
                    fout_CLs.write("{0:1.0f}; {1:1.0f}; {2:1.3f};\n".format(mgluino,mLSP,CLs))    
            else:
                if 'gridx' in file_nominal:
                    fout_CLs.write("{0:1.0f}; {1:1.3f}; {2:1.2f};\n".format(mgluino,X,CLs))
                    pointLabel.DrawLatex(mgluino,X,"%1.2f" % (CLs)) 
                else:
                    fout_CLs.write("{0:1.0f}; {1:1.0f}; {2:1.2f};\n".format(mgluino,mLSP,CLs))
                    pointLabel.DrawLatex(mgluino,mLSP,"%1.2f" % (CLs)) 
                    
        axistitle =ROOT.TLatex()
        axistitle.SetTextSize(0.03)
        axistitle.SetTextColor(ROOT.kGray+2)
        axistitle.SetTextAngle(90)
        axistitle.SetTextFont(42)
        axistitle.SetNDC()
        if CLsobs:
            axistitle.DrawLatex(0.98,0.595,"Numbers give observed CL_{S}")
        else: 
            axistitle.DrawLatex(0.98,0.595,"Numbers give expected CL_{S}")
        fout_CLs.close()
        
    if upperlimit and not 'HiggsSU' in file_nominal:
        listTree = harvesttree(file_nominal.replace("Nominal_hypotest","upperlimit").replace(".root","2"))                          
        nentries = listTree.GetEntries()
                           
        pointLabel = ROOT.TLatex()
        pointLabel.SetTextSize(0.02)
        pointLabel.SetTextFont(42)
        pointLabel.SetTextColor(ROOT.kGray+2)
        
        fout_upperlimit = open(outputfilename+"upperlimits.txt","w")

        for entry in range(nentries):
            listTree.GetEntry(entry)
            mstop = listTree.m0
            mchargino = listTree.m12
            if 'HiggsSU' in file_nominal: continue
            if not 'gridx' in file_nominal and (mstop < frame2.GetXaxis().GetXmin() or mstop>frame2.GetXaxis().GetXmax()-50. or mchargino>frame2.GetYaxis().GetXmax()-5. or mchargino<frame2.GetYaxis().GetXmin()):
                continue
            if 'gridx' in file_nominal and (mstop < frame2.GetXaxis().GetXmin() or mstop>frame2.GetXaxis().GetXmax()-5. or mchargino>frame2.GetYaxis().GetXmax() or mchargino<frame2.GetYaxis().GetXmin()):
                continue
            exXsec = listTree.excludedXsec*1000.
            pointLabel.SetTextColor(12)
            if exXsec >= 100.:
                pointLabel.DrawLatex(mstop,mchargino,"%1.0f" % (exXsec))
                fout_upperlimit.write(str(mstop)+"  "+str(mchargino)+"  {0:1.0f} \n".format(exXsec))            
            else:
                pointLabel.DrawLatex(mstop,mchargino,"%1.1f" % (exXsec))
                fout_upperlimit.write(str(mstop)+"  "+str(mchargino)+"  {0:1.1f} \n".format(exXsec))
            #pointLabel.DrawLatex(mstop,mchargino,"%1.0f" % (mstop))
            #fout_upperlimit.write(str(mstop)+"  "+str(mchargino)+"  "+str(exXsec)+"\n")
        axistitle =ROOT.TLatex()
        #axistitle.SetTextSize(0.03)
        axistitle.SetTextAngle(90)
        axistitle.SetTextFont(42)        
        axistitle.SetTextColor(ROOT.kGray+2)
        axistitle.SetNDC()
        axistitle.DrawLatex(0.98,0.18,"Numbers give 95% CL excluded model cross sections [fb]")
        fout_upperlimit.close()

    # create plots
    # store histograms to output file
    if outputfilename=='':
        outFileNom = 'plots/limit_'+file_nominal.replace('.root','')
    else:
        outFileNom = 'plots/limit_'+outputfilename
    ROOT.CombinationGlob.imgconv( c, outFileNom );

    del frame2


def MirrorBorders( hist ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    # corner points
    hist.SetBinContent(0,0,hist.GetBinContent(1,1))
    hist.SetBinContent(numx+1,numy+1,hist.GetBinContent(numx,numy))
    hist.SetBinContent(numx+1,0,hist.GetBinContent(numx,1))
    hist.SetBinContent(0,numy+1,hist.GetBinContent(1,numy))

    # Fix the other points 
    for i in xrange(1,numx+1):
        hist.SetBinContent(i,0,	   hist.GetBinContent(i,1));
        hist.SetBinContent(i,numy+1, hist.GetBinContent(i,numy));
    for i in xrange(1,numy+1):
        hist.SetBinContent(0,i,      hist.GetBinContent(1,i));
        hist.SetBinContent(numx+1,i, hist.GetBinContent(numx,i));


def AddBorders( hist, name='StupidName', title='StupidTitle'):
    nbinsx = hist.GetNbinsX()
    nbinsy = hist.GetNbinsY()
  
    xbinwidth = ( hist.GetXaxis().GetBinCenter(nbinsx) - hist.GetXaxis().GetBinCenter(1) ) / float(nbinsx-1)
    ybinwidth = ( hist.GetYaxis().GetBinCenter(nbinsy) - hist.GetYaxis().GetBinCenter(1) ) / float(nbinsy-1)
  
    xmin = hist.GetXaxis().GetBinCenter(0) - xbinwidth/2. 
    xmax = hist.GetXaxis().GetBinCenter(nbinsx+1) + xbinwidth/2. 
    ymin = hist.GetYaxis().GetBinCenter(0) - ybinwidth/2. 
    ymax = hist.GetYaxis().GetBinCenter(nbinsy+1) + ybinwidth/2. 
  
    hist2 = ROOT.TH2F(name, title, nbinsx+2, xmin, xmax, nbinsy+2, ymin, ymax);
  
    for ibin1 in xrange(hist.GetNbinsX()+2):
        for ibin2 in xrange(hist.GetNbinsY()+2):
            hist2.SetBinContent( ibin1+1, ibin2+1, hist.GetBinContent(ibin1,ibin2) );
  
    return hist2


def SetBorders( hist, val=0 ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    for i in xrange(numx+2):
        hist.SetBinContent(i,0,val);
        hist.SetBinContent(i,numy+1,val);
    for i in xrange(numy+2):
        hist.SetBinContent(0,i,val);
        hist.SetBinContent(numx+1,i,val);


def FixAndSetBorders(file_nominal, hist, name='hist3', title='hist3', val=0 ):
    hist0 = hist.Clone() # histogram we can modify
    
    #if not "gridx" in file_nominal:
    if "HiggsSU" in file_nominal:
        MirrorBorders( hist0 ) # mirror values of border bins into overflow bins
    
    hist1 = AddBorders( hist0, "hist1", "hist1" );   
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    SetBorders( hist1, val );                              
    # set overflow bins to value 1
    
    histX = AddBorders( hist1, "histX", "histX" )
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    
    hist3 = histX.Clone()
    hist3.SetName( name )
    hist3.SetTitle( title )
    
    del hist0, hist1, histX
    return hist3 # this can be used for filled contour histograms


def DrawContourLine95( leg, hist, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2 ):
    # contour plot
    h = hist.Clone()
    h.SetContour(1)
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
  
    #print linewidth, linestyle
    h.SetLineColor( linecolor )
    h.SetLineWidth( linewidth )
    h.SetLineStyle( linestyle )
    #h.Print()
    h.Draw( "samecont3" );
    
    if text is not '': leg.AddEntry(h,text,'l')
    return leg,h

def converttoGraph(hist,name):
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    gr = gr0.Clone(h.GetName())
    h.SetContour(1)
    
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    #print list0.GetEntries()
    if list0.GetEntries()==0:
        return None
        
    #list.Print()
    gr = list0[0]
    #gr.Print()
    #grTmp = ROOT.TGraph()

    for k in xrange(list0.GetSize()):
        if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
    gr.SetName(name)
    return gr
    
    
    
def ContourGraph( hist ):
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    #h.Print()
    h.GetYaxis().SetRangeUser(20,1000)
    h.GetXaxis().SetRangeUser(100,6100)
    gr = gr0.Clone(h.GetName())
    h.SetContour( 1 )
 
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    if list0.GetEntries()==0:
        return None

    #list.Print()
    gr = list0[0]
    #gr.Print()
    #grTmp = ROOT.TGraph()

    for k in xrange(list0.GetSize()):
        if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
    gr.SetName(hist.GetName())
    #print gr
    #gr.Print()
    return gr;


def DrawExpectedBand( gr1, gr2, fillColor, fillStyle, cut = 0):

    number_of_bins = max(gr1.GetN(),gr2.GetN());
   
    gr1N = gr1.GetN();
    gr2N = gr2.GetN();

    N = number_of_bins;
   
    xx0 = ROOT.Double(0)
    yy0 = ROOT.Double(0)
    x0 = ROOT.Double(0)
    y0 = ROOT.Double(0)    
    xx1 = ROOT.Double(0)
    yy1 = ROOT.Double(0)
   
    x1=[]
    y1=[]
    x2=[]
    y2=[]    
   
    for j in xrange(gr1N):
        gr1.GetPoint(j,xx0,yy0)
        x1 += [float(xx0)]
        y1 += [float(yy0)]
    if gr1N < N:
        for i in xrange(gr1N,N):
            x1 += [ float(x1[gr1N-1]) ]
            y1 += [ float(y1[gr1N-1]) ]
   
    for j in xrange(gr2N):
        gr2.GetPoint(j,xx1,yy1)
        x2 += [float(xx1)]
        y2 += [float(yy1)]
    if gr2N < N:
        for i in xrange(gr2N,N):
            x2 += [float(x2[gr2N-1]) ]
            y2 += [ float(y2[gr2N-1]) ]
            
    #print x1,x2,y1,y2

    grshade = ROOT.TGraphAsymmErrors(2*N)
    for i in xrange(N):
        if x1[i] > cut:
            grshade.SetPoint(i,x1[i],y1[i])
        if x2[N-i-1] > cut:
            grshade.SetPoint(N+i,x2[N-i-1],y2[N-i-1])
   
    #grshade.Print()
   
    # Apply the cut in the shade plot if there is something that doesnt look good...
    Nshade = grshade.GetN()
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0!=0 and y0!=0 :
            x00 = x0
            y00 = y0
            break
            
    #print x00, y00
   
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0 == 0 and y0 == 0:
            grshade.SetPoint(j,x00,y00)
   
    #grshade.Print()
   
    # Now draw the plot...
    grshade.SetFillStyle(fillStyle);
    grshade.SetFillColor(fillColor);
    grshade.SetMarkerStyle(21);
    #grshade.Draw("Fsame");
    return grshade;


def DummyLegendExpected(leg, what,  fillColor, fillStyle, lineColor, lineStyle, lineWidth):
    gr = ROOT.TGraph()
    gr.SetFillColor(fillColor)
    gr.SetFillStyle(fillStyle)
    gr.SetLineColor(lineColor)
    gr.SetLineStyle(lineStyle)
    gr.SetLineWidth(lineWidth)
    leg.AddEntry(gr,what,"LF")
    return leg,gr


def InterpretNames(filename):
     name = filename.split('_hypotest')[0].split('OneLepton_')[1]
     if "GG1stepx12" in name: name = name.split('_GG1stepx12')[0]
     if "GG1stepgridx" in name: name = name.split('_GG1stepgridx')[0]
     if "SS1stepx12" in name: name = name.split('_SS1stepx12')[0]
     if "SS1stepgridx" in name: name = name.split('_SS1stepgridx')[0]
     if "GG2WWZZ" in name: name = name.split('_GG2WWZZ')[0]
     if "GG2CNsl" in name: name = name.split('_GG2CNsl')[0]
     if "SS2WWZZ" in name: name = name.split('_SS2WWZZ')[0]
     if "SS2CNsl" in name: name = name.split('_SS2CNsl')[0]
     
     print name
     if name=="combined_5_6_jets": descriptionname="3, 5, 6 jets SRs combined, default"
     elif name=="combined_5_6_jets_onebin": descriptionname="3, 5, 6 jets SRs combined, one bin"
     elif name=="combined_5_6_loose_onebin": descriptionname="relaxed 3, 5, 6 jets SRs combined, one bin"
     elif name=="combined_5_6_jets_met3jet": descriptionname="3 (fit in E_{T}^{miss}), 5, 6 jets SRs combined"
     elif name=="combined_5_6_jets_met3jet_2": descriptionname="3 (fit in E_{T}^{miss}, hard m_{eff} cut), 5, 6 jets SRs combined"     
     elif name=="combined_5_6_jets_met3jet_onebin": descriptionname="3 (fit in E_{T}^{miss}), 5, 6 jets SRs combined, one bin"
     elif name=="combined_5_6_jets_meffonly": descriptionname="3, 5, 6 jets SRs combined, shape fit in m_{eff}" 
     elif name=="combined_5_6_jets_bjetveto": descriptionname="3, 5, 6 jets SRs combined, b-jet veto" 
     elif name=="combined_3_6_7_jets_onebin": descriptionname="3, 6, 7 jets SRs combined, one bin"
     elif name=="combined_3_6_7_jets": descriptionname="3, 6, 7 jets SRs combined" 
     elif name=="combined_3_5_jets": descriptionname="3 and 5 jets SRs combined" 
     elif name=="combined_20sys" or name=="combined_20sys_2": descriptionname="3, 5, 6 jets SRs combined, 20 % syst." 
     elif name=="combined_50sys" or name=="combined_50sys_2": descriptionname="3, 5, 6 jets SRs combined, 50 % syst." 
     elif name=="combined_80sys" or name=="combined_80sys_2": descriptionname="3, 5, 6 jets SRs combined, 80 % syst."
     elif name=="combined_30sys_2": descriptionname="3, 5, 6 jets SRs combined, 30 % syst."   
     elif name=="3jetSR_exclu": descriptionname="3 jet SR, exclusive"
     elif name=="3jetSR_inclu": descriptionname="3 jet SR, inclusive"
     elif name=="5jetSR_exclu": descriptionname="5 jet SR, exclusive"
     elif name=="5jetSR_inclu": descriptionname="5 jet SR, inclusive" 
     elif name=="6jetSR_exclu": descriptionname="6 jet SR, exclusive"
     elif name=="6jetSR_inclu": descriptionname="6 jet SR, inclusive" 
     elif name=="combined_5_6_jets_met3jet_mt6jet": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR)" 
     elif name=="combined_5_6_jets_met3jet_mt6jet_met250": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR), lower E_{T}^{miss} cut in 6 jets SR"   
     elif name=="combined_5_6_jets_met3jet_mt6jet_mt120": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR), lower m_{T} cut in 6 jets SR" 
     elif name=="combined_5_6_jets_met3jet_mt5jet": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{T} (5 jets SR), m_{eff} (6 jets SR)" 
     elif name=="combined_5_6_jets_met3jet_mt6jet_mt130_met300": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR); m_{T} > 130 GeV and E_{T}^{miss} > 300 GeV"  
     elif name=="combined_5_6_jets_met3jet_mt6jet_mt130": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR); m_{T} > 130 GeV"  
     else: descriptionname = "No description found"
     
     
     return descriptionname
     
hh=[]     

def DrawContourMassLine(hist, mass, color=14 ):

  h = ROOT.TH2F( hist )
  hh.append(h)
  h.SetContour( 1 )
  h.SetContourLevel( 0, mass )

  h.SetLineColor( color )
  h.SetLineStyle( 7 )
  h.SetLineWidth( 1 )
  #h.Print("range")
  h.Draw( "samecont3" )
  
  return
  
