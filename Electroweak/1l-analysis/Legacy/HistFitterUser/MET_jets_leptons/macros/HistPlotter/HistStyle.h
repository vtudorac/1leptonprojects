#ifndef HISTSTYLE_H
#define HISTSTYLE_H

#include <iostream>


#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TAxis.h"

#include "RooPlot.h"

#include "atlasstyle-00-03-05/AtlasStyle.h"
#include "atlasstyle-00-03-05/AtlasUtils.h"
#include "atlasstyle-00-03-05/AtlasLabels.h"

class HistStyle
{


 public:
  HistStyle(); 
  ~HistStyle();


  void init();

  TCanvas* getCanvas();
  TLegend* getLegend(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Double_t size=0.055);
  TPad* getMainTPad();
  TPad* getRatioTPad();

  // 
  void setHistAxes(TAxis*& x_axis, TAxis*& y_axis,TString x_axis_label);

  void SetRatioPadStyle(RooPlot* frame,TString x_axis_label);
  void SetMainPadStyle(RooPlot* frame);





};



#endif
