#include "HistPlotter.h"

int main(int argc, char** argv)
{

  TString name;
  TString sigName;
  TString sigName2;
  TString dir;
  TString save;
  TString regionName = "";
  TString regionName2 = "";
  TString region = "";
  TString units = "GeV";
  double ratioYmin = -1.0;
  double ratioYmax = -1.0;
  double mainYmin = -1.0;
  double mainYmax = -1.0;

  float min = 0;
  float max = 0;

  bool printRegionsOnly=false;
  bool integerBins=false;
  bool logY=true;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-name") == 0){
      name = argv[++i];
    }
    else if (strcmp(argv[i], "-sigName") == 0){
      sigName = argv[++i];
    }
    else if (strcmp(argv[i], "-sigName2") == 0){
      sigName2 = argv[++i];
    }
    else if (strcmp(argv[i], "-dir") == 0){
      dir = argv[++i];
    }
    else if (strcmp(argv[i], "-save") == 0){
      save = argv[++i];
    }
    else if (strcmp(argv[i], "-region") == 0){
      region = argv[++i];
    }
    else if (strcmp(argv[i], "-units") == 0){
      units = argv[++i];
    }
   else if (strcmp(argv[i], "-regionName") == 0){
      regionName = argv[++i];
    }
   else if (strcmp(argv[i], "-regionName2") == 0){
      regionName2 = argv[++i];
    }
    else if (strcmp(argv[i], "-ratioYMin") == 0){
      ratioYmin = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-ratioYMax") == 0){
      ratioYmax = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-mainYMin") == 0){
      mainYmin = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-mainYMax") == 0){
      mainYmax = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-minRange") == 0){
      min= atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-maxRange") == 0){
      max= atof(argv[++i]);
    }
    else if (strcmp(argv[i], "--printRegionsOnly") == 0){
      printRegionsOnly=true; 
    }
    else if (strcmp(argv[i], "--integerBins") == 0){
      integerBins=true;
    }
    else if (strcmp(argv[i], "--noLogY") == 0){
      logY=false;
    }



    else{
      cout << "Error: invalid argument!" << endl;
      exit(0);
    }
  }

  cout << "Name:   " << name << endl;
  cout << "Save:   " << save << endl;


  HistPlotter* plotter = new HistPlotter();

  plotter->printRegionsOnly(printRegionsOnly);
  plotter->integerBins(integerBins);
  plotter->setLogY(logY);
  plotter->setRegionName(regionName);
  plotter->setRegionName2(regionName2);
  //plotter->setHistStatus("Preliminary");
  //plotter->setHistStatus("Internal");
  plotter->setHistStatus("");
  //plotter->setRatioYMin(0.5);//ratioYmin);//VR
  //plotter->setRatioYMax(1.6);//ratioYmax);//VR
  plotter->setRatioYMin(ratioYmin);//SR use default 0-2.3 
  plotter->setRatioYMax(ratioYmax);//SR
  plotter->setMainYMin(mainYmin);
  plotter->setMainYMax(mainYmax);
  plotter->setErrorRange(min,max);


  // Overlay the signal
  if(region.Contains("6J")){
    plotter->addProcess("singletop_6J_bin1","Single top",kGreen+2,1);
    plotter->addProcess("singletop_6J_bin2","Single top",kGreen+2,1);
    plotter->addProcess("singletop_6J_bin3","Single top",kGreen+2,1);
    plotter->addProcess("singletop_6J_bin4","Single top",kGreen+2,1);
    plotter->addProcess("ttbar_6J_bin1","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_6J_bin2","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_6J_bin3","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_6J_bin4","t#bar{t}",kGreen-9,1);
    plotter->addProcess("Wjets_6J_bin1","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_6J_bin2","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_6J_bin3","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_6J_bin4","W+jets",kAzure+1,2);
    plotter->addProcess("diboson","Diboson",kViolet-8,2);
    plotter->addProcess("zjets_Sherpa22","Others",kOrange,2);
    plotter->addProcess("ttv","Others",kOrange,2);

    plotter->addSigProcess("GG_oneStep_1545_785_25","m(#tilde{g},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(1545,785,25) GeV",kBlue); // 6J
    plotter->addSigProcess("SS_oneStep_1125_575_25","m(#tilde{q},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(1125,575,25) GeV",kPink); // 6J
  }
  if(region.Contains("4Jhighx")){
    plotter->addProcess("singletop_4Jhighx_bin1","Single top",kGreen+2,1);
    plotter->addProcess("singletop_4Jhighx_bin2","Single top",kGreen+2,1);
    plotter->addProcess("singletop_4Jhighx_bin3","Single top",kGreen+2,1);
    plotter->addProcess("singletop_4Jhighx_bin4","Single top",kGreen+2,1);
    plotter->addProcess("ttbar_4Jhighx_bin1","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_4Jhighx_bin2","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_4Jhighx_bin3","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_4Jhighx_bin4","t#bar{t}",kGreen-9,1);
    plotter->addProcess("Wjets_4Jhighx_bin1","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_4Jhighx_bin2","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_4Jhighx_bin3","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_4Jhighx_bin4","W+jets",kAzure+1,2);
    plotter->addProcess("diboson","Diboson",kViolet-8,2);
    plotter->addProcess("zjets_Sherpa22","Others",kOrange,2);
    plotter->addProcess("ttv","Others",kOrange,2);

    plotter->addSigProcess("GG_oneStep_1700_1500_60","m(#tilde{g},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(1700,1500,60) GeV",kBlue); // 
    plotter->addSigProcess("SS_oneStep_1000_950_60","m(#tilde{q},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(1000,950,60) GeV",kPink); // 
  }
  if(region.Contains("4Jlowx")){  
    plotter->addProcess("singletop_4Jlowx_bin1","Single top",kGreen+2,1);
    plotter->addProcess("singletop_4Jlowx_bin2","Single top",kGreen+2,1);
    plotter->addProcess("singletop_4Jlowx_bin3","Single top",kGreen+2,1);
    plotter->addProcess("singletop_4Jlowx_bin4","Single top",kGreen+2,1);
    plotter->addProcess("ttbar_4Jlowx_bin1","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_4Jlowx_bin2","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_4Jlowx_bin3","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_4Jlowx_bin4","t#bar{t}",kGreen-9,1);
    plotter->addProcess("Wjets_4Jlowx_bin1","W+jets",kAzure+1,2);    
    plotter->addProcess("Wjets_4Jlowx_bin1","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_4Jlowx_bin2","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_4Jlowx_bin3","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_4Jlowx_bin4","W+jets",kAzure+1,2);
    plotter->addProcess("diboson","Diboson",kViolet-8,2);
    plotter->addProcess("zjets_Sherpa22","Others",kOrange,2);
    plotter->addProcess("ttv","Others",kOrange,2);

    plotter->addSigProcess("GG_oneStep_1600_260_60","m(#tilde{g},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(1600,260,60) GeV",kBlue); // 
    plotter->addSigProcess("SS_oneStep_1000_260_60","m(#tilde{q},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(1100,260,60) GeV",kPink); // 
  }
  if(region.Contains("2J")){
    plotter->addProcess("singletop_2J_bin1","Single top",kGreen+2,1);
    plotter->addProcess("singletop_2J_bin2","Single top",kGreen+2,1);
    plotter->addProcess("singletop_2J_bin3","Single top",kGreen+2,1);
    plotter->addProcess("singletop_2J_bin4","Single top",kGreen+2,1);
    plotter->addProcess("ttbar_2J_bin1","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_2J_bin2","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_2J_bin3","t#bar{t}",kGreen-9,1);
    plotter->addProcess("ttbar_2J_bin4","t#bar{t}",kGreen-9,1);
    plotter->addProcess("Wjets_2J_bin1","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_2J_bin2","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_2J_bin3","W+jets",kAzure+1,2);
    plotter->addProcess("Wjets_2J_bin4","W+jets",kAzure+1,2);
    plotter->addProcess("diboson","Diboson",kViolet-8,2);
    plotter->addProcess("zjets_Sherpa22","Others",kOrange,2);
    plotter->addProcess("ttv","Others",kOrange,2);

    plotter->addSigProcess("GG_oneStep_905_705_505","m(#tilde{g},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(905,705,505) GeV",kBlue); // 
    plotter->addSigProcess("SS_oneStep_625_425_225","m(#tilde{q},#tilde{#chi}^{#pm}_{1},#tilde{#chi}^{0}_{1})=(625,425,225) GeV",kPink); // 
  }

  if(regionName.Contains("9J")){
    plotter->addProcess("zjets_Sherpa221","Others",kOrange,2);
    plotter->addProcess("ttv","Others",kOrange,2);
    plotter->addProcess("diboson_Sherpa221","Diboson",kViolet-8,2);
    plotter->addProcess("wjets_Sherpa221","W+jets",kAzure+1,2);
    plotter->addProcess("singletop","Single top",kGreen+2,1);
    plotter->addProcess("ttbar","t#bar{t}",kGreen-9,1);

    plotter->addSigProcess("GG2stepWZ_1200_950_825_700","GG2step m(#tilde{g},#tilde{#chi}^{0}_{1})=(1200,700) GeV",kBlue);
    plotter->addSigProcess("pMSSM_M3muM1lsp_1500_500","pMSSM m(#tilde{g},#tilde{#chi}^{0}_{1})=(1500,500) GeV",kPink);

    plotter->setXaxisNdivisions(4,6,0);
  }

  
  plotter->addXaxisLabelToMap("mct","m_{CT} [GeV]");
  plotter->addXaxisLabelToMap("mt","m_{T} [GeV]");
  plotter->addXaxisLabelToMap("met","E_{T}^{miss} [GeV]");
  plotter->addXaxisLabelToMap("charge","Lepton charge");
  plotter->addXaxisLabelToMap("flavor","Lepton flavor");
  plotter->addXaxisLabelToMap("j0eta","Leading jet #eta");
  plotter->addXaxisLabelToMap("jet1Pt","Leading jet p_{T} [GeV]");
  plotter->addXaxisLabelToMap("jet2Pt","Sub-leading jet p_{T} [GeV]");
  plotter->addXaxisLabelToMap("lep1Pt","Lepton p_{T} [GeV]");
  plotter->addXaxisLabelToMap("nBJet30","Number of #it{b}-jets");
  plotter->addXaxisLabelToMap("meffInc30","m_{eff} [GeV]");
  plotter->addXaxisLabelToMap("nJet30","Number of jets (p_{T}>30 GeV)");
  plotter->addXaxisLabelToMap("metmeffInc30","E_{T}^{miss} / m_{eff}^{inc}");
  plotter->addXaxisLabelToMap("JetAplanarity","Jet Aplanarity");
  plotter->addXaxisLabelToMap("nMinusOne","m_{T} [GeV]");


  // Arrows
  //plotter->addArrowToCanvas("SR6Jnomt_mt","SR6J","lower",10,25,12);
  //plotter->addArrowToCanvas("SR5Jnomt_mt","SR6J","lower",2,275,2);
  //  plotter->addArrowToCanvas("SR4JhighxGGnomtEM_mt","SR6J","lower",9,475,500);
  //  plotter->addArrowToCanvas("SR4JhighxGGnometovermeffEM_metmeffInc30","SR6J","lower",9,0.3,0.38);

  //  plotter->addArrowToCanvas("SR4JlowxGGexclnomtEM_mt","SR6J","lower",9,125,150);
  //  plotter->addArrowToCanvas("SR4JlowxGGexclnoaplanarityEM_JetAplanarity","SR6J","lower",9,0.03,0.04);

  //  plotter->addArrowToCanvas("SR4JlowxGGdiscnomtEM_mt","SR6J","lower",9,125,150);
  //  plotter->addArrowToCanvas("SR4JlowxGGdiscnoaplanarityEM_JetAplanarity","SR6J","lower",9,0.06,0.07);


  plotter->addRegionToPlot(region);
  plotter->setUnits(units);

  plotter->addPlots(dir,name,sigName,sigName2,save,"ALL","Validation_combined_BasicMeasurement_model_afterFit.root","w");
  plotter->Process();

  return 0;
}
