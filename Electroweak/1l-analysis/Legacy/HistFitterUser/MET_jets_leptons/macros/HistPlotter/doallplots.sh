#!/bin/bash

############
## setup HF
## make
############

#echo 'all input files in: /afs/cern.ch/work/d/dxu/public/1l_pub/ForICHEPPlots/'

## for MORIOND 2017  ########################################################################
# GGoneStep
. dooneplot.sh SR2JBTEM_meffInc30  GG_oneStep_905_705_505 "2J b-tag" 10000 SS_oneStep_625_425_225
#. dooneplot.sh SR2JBVEM_meffInc30  GG_oneStep_905_705_505 "2J b-veto" 10000  SS_oneStep_625_425_225

. dooneplot.sh SR4JhighxBTEM_meffInc30  GG_oneStep_1700_1500_60 "4J high-x b-tag" 70 SS_oneStep_1000_950_60
#. dooneplot.sh SR4JhighxBVEM_meffInc30  GG_oneStep_1700_1500_60 "4J high-x b-veto" 70 SS_oneStep_1000_950_60

. dooneplot.sh SR4JlowxBTEM_meffInc30  GG_oneStep_1600_260_60 "4J low-x b-tag" 40 SS_oneStep_1000_260_60
#. dooneplot.sh SR4JlowxBVEM_meffInc30  GG_oneStep_1600_260_60 "4J low-x b-veto" 40 SS_oneStep_1000_260_60

. dooneplot.sh SR6JBTEM_meffInc30  GG_oneStep_1545_785_25 "6J b-tag" 20 SS_oneStep_1125_575_25
#. dooneplot.sh SR6JBVEM_meffInc30  GG_oneStep_1545_785_25 "6J b-veto" 20 SS_oneStep_1125_575_25

# multijet
# change in dooneplot.sh
#. dooneplot.sh SR_meffInc30  sig1 "9J" 10000 sig2

