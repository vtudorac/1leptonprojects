#!/bin/bash

############
## setup HF
## make
############

ds=$1
grid=$2
regionname="$3"
ymax=$4
grid2=$5
echo $regionname
echo $regionname2

## 2017 Moriond ## 

## with signal for N-1 SR

## GGoneStep 
./output.o -dir /afs/cern.ch/work/d/dxu/public/1l_pub/MORIOND_2017/ForFinalPlots -name OneLepton_2J_4Jhighx_4Jlowx_6J -sigName OneLepton_2J_4Jhighx_4Jlowx_6J_onlysignal_${grid} -sigName2 OneLepton_2J_4Jhighx_4Jlowx_6J_onlysignal_${grid2} -save figures -region $ds  -mainYMin 0.05 -mainYMax 10000 -regionName "$regionname" -regionName2 "" # --noLogY

## GGtwoStep pMSSM
#./output.o -dir /afs/cern.ch/work/d/dxu/public/1l_pub/MORIOND_2017/ForFinalPlots/9J -name OneLepton_multijet -sigName2 OneLepton_multijet_pMSSM_M3muM1lsp_1500_500 -sigName OneLepton_multijet_GG2stepWZ_1200_950_825_700 -save figures -region $ds  -mainYMin 0.05 -mainYMax 10000 -regionName "$regionname" -regionName2 "" # --noLogY
