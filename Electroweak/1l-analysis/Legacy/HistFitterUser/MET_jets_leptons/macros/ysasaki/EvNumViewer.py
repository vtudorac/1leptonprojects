import os,sys,ROOT

fileNameList = ["root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneEle.root",
                "root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneMuo.root"]
#Cut = "lep2Pt<0 && met>100 && met<180 && mt>100 && jet4Pt>80 && nB4Jet==0 && meffInc>1200 && meffInc<1400"
#Cut = "lep2Pt<0 && met>100 && met<180 && mt>100 && jet2Pt>80 && jet4Pt>40 && meffInc>800 && met/meff4Jet>0.2"
Cut = "lep2Pt<0 && met>180 && met<250 && mt>100 && jet2Pt>80 && jet4Pt>80 && meffInc>800 && met/meff4Jet>0.2"
treeList = ["Data","QCD",
            "WZAlpgen_NoSys",#"WZAlpgen_JESup","WZAlpgen_JESdown","WZAlpgen_JER",
            "TopMcAtNlo_NoSys",#"TopMcAtNlo_JESup","TopMcAtNlo_JESdown",
            "BG_NoSys"]
Luminosity = 5835 # pb-1
Weight = "(genWeight) * (eventWeight) * (leptonWeight) * (triggerWeight) * (pileupWeight) * (%f)" % Luminosity

ValueList = []

UniqueIndex = 0
for treeName in treeList:
    UniqueIndex += 1
    print "processing",treeName
    tree = ROOT.TChain(treeName)
    for fileName in fileNameList:
        tree.Add(fileName)
    
    hist = ROOT.TH1D("h_"+str(UniqueIndex),"",1,-0.5,0.5)
    hist.Sumw2()
    myWeight = Weight
    if "QCD"  in treeName: myWeight = "qcdWeight"
    if "Data" in treeName: myWeight = "1"
    tree.Project(hist.GetName(),"0.","( %s ) * ( %s )" % (Cut,myWeight))

    val = hist.GetBinContent(hist.GetXaxis().FindBin(0.))
    err = hist.GetBinError(hist.GetXaxis().FindBin(0.))

    ValueList.append((treeName,val,err))


print "-"*50
for item in ValueList:
    name = item[0]
    val  = item[1]
    err  = item[2]

    if val == 0.: strval = "0"
    else: strval = "%+4.3e" % (val)

    if err == 0.: strerr = "0"
    else: strerr = "%+4.3e" % (err)

    print "%20s : %10s +/- %10s" % ( name, strval, strerr )
print "-"*50
