import os,sys,ROOT

fileNameList = ["root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneEle.root"]

####################################################
# Switch
#treeNameBase = "WZAlpgen"
treeNameBase = "TopMcAtNlo"
####################################################
treeList = ["NoSys","JESMediumup","JESMediumdown","JER"]
BaseTree = treeList[0]

ValueList = []
AllEventList = []
AllEventWeightList = []
EventListForTree = {}

UniqueIndex = 0
for treeName in treeList:
    UniqueIndex += 1
    print "processing",treeName
    tree = ROOT.TChain(treeNameBase+"_"+treeName)
    for fileName in fileNameList:
        tree.Add(fileName)
    #print tree.GetEntries()
    EventListForTree[treeName] = []
    for evt in range(tree.GetEntries()):
        tree.GetEntry(evt)
        if not tree.lep2Pt<0.:             continue
        if not tree.jet4Pt>80:             continue
        if not tree.met>250:               continue
        if not tree.mt>100:                continue
        if not tree.met/tree.meff4Jet>0.2: continue
        if not tree.meffInc>800:           continue

        evnum = tree.EventNumber
        weight = tree.genWeight * tree.eventWeight * tree.pileupWeight
        if not evnum in AllEventList: 
            AllEventList.append(evnum)
            AllEventWeightList.append(weight)
        EventListForTree[treeName].append(evnum)
    #print treeName, EventListForTree

#print AllEventList

fractionList = {}
for treeName in EventListForTree:
    fractionList[treeName] = 0


NumberList = {}

print "-"*50
mystr  = "%10s " % "EvtNum"
mystr += "(weight)     : "
for index,treeName in enumerate(treeList):
    mystr += "[%d] "%index
print mystr

AllEventList.sort()

for i in range(len(AllEventList)):
    mystr  = "%10d " % AllEventList[i]
    mystr += "(%+3.2e)  : " % AllEventWeightList[i]

    for treeName in treeList:
        if AllEventList[i] in EventListForTree[treeName]: 
            mystr += "[*] "
            if AllEventList[i] in EventListForTree[BaseTree]:
                fractionList[treeName] += 1
        else : 
            mystr += "[ ] "
    print mystr

print "-"*50
for index,treeName in enumerate(treeList):
    sumNum = len(EventListForTree[BaseTree]) + len(EventListForTree[treeName]) - fractionList[treeName]
    print "[%d] : %50s    Fraction = %.1f%%    Number = %d"%(index,treeNameBase+"_"+treeName, float(fractionList[treeName])/float(sumNum)*100.,fractionList[treeName])
print "-"*50
