## Test script to check bTagWeight
## histBTag  should : Scale up (x2.5) the events with (#TruthBJets in leading 4jets >= 1)
## histBVeto should : Scale down (x0.53) the events with (#TruthBJets in leading 4jets >= 1)
## In both cases, events with (#TruthBJets in leading 4jets == 0) should not be affected.

import os,sys,commands,ROOT

ROOT.gStyle.SetOptStat(False)

treeSherpa = ROOT.TChain("WSherpa_NoSys")
treeAlpgen = ROOT.TChain("WAlpgen_NoSys")
for tree in [treeSherpa,treeAlpgen]:
    tree.Add("root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneEle.root")
    tree.Add("root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneMuo.root")

Weight = "5835 * (genWeight) * (eventWeight) * (leptonWeight) * (triggerWeight) * (pileupWeight)"
#######################
# Choose from TR and WR
#Cut    = "(lep2Pt<10) && (met>100) && (met<180) && (mt>100) && (jet4Pt>80) && (meffInc>800)"
Cut    = "(lep2Pt<10) "
#######################

#Value = "nTruthB4Jet"
Value = "nTruthB40"
#histBase = ROOT.TH1D("histBase",";# of Truth BJets(pT>40GeV, Leading 4jets)", 4,-0.5,3.5)
histBase = ROOT.TH1D("histBase",";# of Truth BJets(pT>40GeV, Leading 4jets)", 7,-0.5,6.5)
histBase.Sumw2()
histBase.SetMarkerStyle(20)
histBase.SetLineWidth(2)

histAlpgen  = histBase.Clone("histAlpgen")
histSherpa  = histBase.Clone("histSherpa")
histAlpgen.SetLineColor(2)
histAlpgen.SetMarkerColor(2)
histSherpa.SetLineColor(4)
histSherpa.SetMarkerColor(4)

treeAlpgen.Project(histAlpgen.GetName()  , Value, "( %s ) * ( %s )" % (Cut,Weight) )
treeSherpa.Project(histSherpa.GetName()  , Value, "( %s ) * ( %s )" % (Cut,Weight) )

canvas = ROOT.TCanvas("c")
canvas.SetGrid()

histAlpgen.Draw("axis")
histAlpgen.Draw("axig same")
histSherpa.Draw("same")
histAlpgen.Draw("same")

print Weight
print Cut
print "Integral (Sherpa) =", histSherpa.Integral()
print "Integral (Alpgen) =", histAlpgen.Integral()

canvas.Update()
raw_input()
