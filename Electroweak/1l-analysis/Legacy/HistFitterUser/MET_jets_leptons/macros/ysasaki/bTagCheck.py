## Test script to check bTagWeight
## histBTag  should : Scale up (x2.5) the events with (#TruthBJets in leading 4jets >= 1)
## histBVeto should : Scale down (x0.53) the events with (#TruthBJets in leading 4jets >= 1)
## In both cases, events with (#TruthBJets in leading 4jets == 0) should not be affected.

import os,sys,commands,ROOT

ROOT.gStyle.SetOptStat(False)

tree = ROOT.TChain("WZSherpa_NoSys")
#tree = ROOT.TChain("WZAlpgen_NoSys")
tree.Add("root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneEle.root")
tree.Add("root://eosatlas//eos/atlas/user/y/ysasaki/trees/v15_2/tree_OneMuo.root")

Weight = "5835 * (genWeight) * (eventWeight) * (leptonWeight) * (triggerWeight) * (pileupWeight)"
#######################
# Choose from TR and WR
#Cut    = "(lep2Pt<10) && (met>100) && (met<180) && (mt>100) && (jet4Pt>80) && (meffInc>800) && (nB4Jet>0)"
Cut    = "(lep2Pt<10) && (met>100) && (met<180) && (mt>100) && (jet4Pt>80) && (meffInc>800) && (nB4Jet==0)"
#######################

WeightNominal = Weight + " * (bTagWeight4Jet)"
BTagEfficiency   = 0.6 # default MV1
SherpaCorrection = 0.4 # efficiency goes down as small as ~40% (conservative)

bTagWeightForSherpa  = " * ( 1 + ( ( ( 1 / %f ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(SherpaCorrection)
WeightBTag = Weight + bTagWeightForSherpa
bVetoWeightForSherpa = " * ( 1 + ( ( ( ( 1 - %f ) / ( 1 - %f * %f ) ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(BTagEfficiency,SherpaCorrection,BTagEfficiency)
WeightBVeto = Weight + bVetoWeightForSherpa

Value = "nTruthB4Jet"
histBase = ROOT.TH1D("histBase",";# of Truth BJets(pT>40GeV, Leading 4jets)", 4,-0.5,3.5)
histBase.Sumw2()
histBase.SetMarkerStyle(20)
histBase.SetLineWidth(2)

histNominal = histBase.Clone("histNominal")
histBTag    = histBase.Clone("histBTag")
histBVeto   = histBase.Clone("histBVeto")
histNominal.SetLineColor(1)
histNominal.SetMarkerColor(1)
histBTag.SetLineColor(2)
histBTag.SetMarkerColor(2)
histBVeto.SetLineColor(4)
histBVeto.SetMarkerColor(4)

tree.Project(histNominal.GetName(), Value, "( %s ) * ( %s )" % (Cut,WeightNominal) )
tree.Project(histBTag.GetName()   , Value, "( %s ) * ( %s )" % (Cut,WeightBTag) )
tree.Project(histBVeto.GetName()  , Value, "( %s ) * ( %s )" % (Cut,WeightBVeto) )

canvas = ROOT.TCanvas("c")
canvas.SetGrid()

histBTag.Draw("axis")
histBTag.Draw("axig same")
histNominal.Draw("same")
histBTag.Draw("same")
histBVeto.Draw("same")

print WeightNominal
print Cut
print "Integral =", histBTag.Integral()

canvas.Update()
raw_input()
