import ROOT,os,sys

class treeProcessor:

    def __init__(this):
        this.SkimCut = []
        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v3_2"
        this.outputFolder = "trees"
        this.fileList = ["bkgtree_%s.root"%(x) for x in ["HardEle","HardMuo"]]
        this.sampleList = []
        this.dividerList = {}

        this.CheckOutFolder()

    def CheckOutFolder(this):
        if not os.path.exists(this.outputFolder):
            os.mkdir(this.outputFolder)
            print "Created Folder :",this.outputFolder
        return

    def SetSample(this,sam,div={}):
        this.sampleList.append(sam)
        mydiv = {}
        for d in div:
            if type(div[d])==type("") : mydiv[d] = [div[d]]
            else                   : mydiv[d] = div[d]
        this.dividerList[sam] = mydiv
        print "Add Sample :",sam," Divider=",str(mydiv)
        return

    def SetSkim(this,skim):
        this.SkimCut = skim
        print "Skim =",str(skim)
        return

    def CutString(this,Cut):
        if Cut:
            myStr = "("
            for c in Cut:
                myStr += " ( %s ) &&"%c
            myStr = myStr[:-2] + ")"
        else:
            myStr = ""
        return myStr


    def ProcessOne(this,inFileName,inTreeName,outFileName,Divider={}):
        inFile = ROOT.TFile.Open(inFileName)
        allkeys = inFile.GetListOfKeys()
        next = ROOT.TIter(allkeys)
        outFile = ROOT.TFile(outFileName,"RECREATE")
        while True:
            obj = next()
            if not obj : break
            inName = obj.GetName()
            if not inName.startswith(inTreeName) : continue
            inTree = inFile.Get(inName)
            print inName,"(",inFileName,")"
            if Divider:
                for item in Divider:
                    newName = inName
                    newName = newName.split("_")
                    newName = newName[:-1]+[item]+[newName[-1]]
                    newName = "_".join(newName)
                    myCut   = this.SkimCut+Divider[item]
                    print " ->",newName, " : ",myCut
                    outtree = inTree.CopyTree(this.CutString(myCut))
                    outtree.Write(newName)
                print " ->",inName," : ",this.SkimCut
                outtree = inTree.CopyTree(this.CutString(this.SkimCut))
                outtree.Write(inName)
        inFile.Close()
        outFile.Close()
        return

    def ProcessAll(this):
        for fil in this.fileList:
            for sam in this.sampleList:
                inFileName  = this.inputFolder .rstrip("/")+"/"+fil
                outFileName = this.outputFolder.rstrip("/")+"/"+fil
                inTreeName  = sam
                Divider     = this.dividerList[sam]
                this.ProcessOne(inFileName,inTreeName,outFileName,Divider=Divider)

if __name__=="__main__":
    tp = treeProcessor()
    tp.SetSkim([])
    Divider = {"Np0":"nTruthJet40==0",
               "Np1":"nTruthJet40==1",
               "Np2":"nTruthJet40==2",
               "Np3":"nTruthJet40==3",
               "Np4":"nTruthJet40==4",
               "Np5":"nTruthJet40>=5"}
    tp.SetSample("SherpaW3jet",div=Divider)
    tp.ProcessAll()
