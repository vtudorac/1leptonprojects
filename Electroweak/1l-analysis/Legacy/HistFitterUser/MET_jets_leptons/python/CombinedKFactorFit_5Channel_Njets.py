################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kYellow,kWhite,kPink,kGray,kMagenta,TFile
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName

#from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
#import ROOT
#ROOT.SetAtlasStyle()

def extractCrossSectionErr( Mass, tol = 0.1, finalState = 2, finalState2 = -1 ):
    #theFile = TFile.Open('/afs/cern.ch/atlas/groups/susy/SignalCrossSectionsUncert/SimplModels/SignalUncertainties-SM_GG_twostep_Moriond2012_v0.root')
    theFile = TFile.Open('/afs/cern.ch/atlas/groups/susy/SignalCrossSectionsUncert/GluinoMediatedAndSbottom/GtXsections.root')
    tt = theFile.Get('SignalUncertainties')
    err = 0.
    for i in xrange(tt.GetEntries()):
        tt.GetEntry(i)
        mgl = 0.
        if theFile.GetName().find("GtXsections")>-1: mgl = tt.G
        else: mgl = tt.mgluino
        if abs(Mass - mgl) < tol and (tt.finalState == finalState or tt.finalState == finalState2):
            #print tt.Mass
            err = tt.Tot_error
            pass
        pass
    return err


def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]
useHardLepMultijetsCR=True
useHardLepCR=False
useSoftLepCR=False
useDiLepCR=False
useStat=True
fullSyst=True

doTableInputs=False #This effectively means no validation plots but only validation tables (but is 100x faster)
doValidationSRLoose=False
doValidationSRTight=doTableInputs
doValidationSlope=doTableInputs
doValidationDilep=doTableInputs
doValidationDilepZ=doTableInputs
doValidationSoftLep=doTableInputs
doValidationMultijet=True

doDiscovery=False
doExclusion_GMSB_combined=False
doExclusion_mSUGRA_dilepton_combined=False
doExclusion_GG_onestepCC_x12=False
doExclusion_GG_onestepCC_gridX=False
doExclusion_GG_twostepCC_slepton=False
blindS=False

doExclusion_mSUGRA_multijets=False
doExclusion_GG_twosteps_multijets=False # take care about fixSigXSec=False
doExclusion_SS_twosteps_multijets=False # take care about fixSigXSec=False
doExclusion_Gtt_multijets=False # take care about fixSigXSec=False

doWptReweighting=False ## deprecated
doSignalOnly=False #Remove all bkgs for signal histo creation step
if configMgr.executeHistFactory:
    doSignalOnly=False
    
if not 'sigSamples' in dir():
    sigSamples=["SU_3060_300_0_10_P"]
    #sigSamples=["SM_GG_onestepCC_445_245_45"]
    #sigSamples=["SM_GG_twostepCC_slepton_415_215_115_15"]
    #sigSamples=["GMSB_3_2d_50_250_3_10_1_1"]

# First define HistFactory attributes
configMgr.analysisName = "Combined_KFactorFit_5Channel" # Name to give the analysis
configMgr.outputFileName = "results/CombinedKFactorFit_5Channel.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 4.713
configMgr.setLumiUnits("fb-1")

#configMgr.doHypoTest=True
#configMgr.nTOYs=100
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 # asimov
configMgr.testStaType=3
configMgr.nPoints=20

#Split bdgFiles per channel
sigFiles = []
sigFiles_l = []
sigFiles_sl = []

configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

inputDir="root://eosatlas//eos/atlas/atlascerngroupdisk/phys-susy/histfitter/stronglepton/Paper_v1/"
inputDir2="root://eosatlas//eos/atlas/atlascerngroupdisk/phys-susy/histfitter/stronglepton/Paper_v2/"
inputDirSLBkg="/afs/cern.ch/work/h/hyamaguc/public/samples/"
inputDirSig="root://eosatlas//eos/atlas/atlascerngroupdisk/phys-susy/histfitter/stronglepton/"

if not onLxplus:
    print "INFO : Mainz bleibt Mainz...\n"
    inputDir="data/"
    inputDir2="data/"
    inputDirSLBkg="data/"
    inputDirSig="data/"
else:
    print "INFO : Running on lxplus... \n"

# Set the files to read from
bgdFiles_ee = [inputDir+"SusyFitterTree_EleEle.root"]
bgdFiles_em = [inputDir+"SusyFitterTree_EleMu.root"]
bgdFiles_mm = [inputDir+"SusyFitterTree_MuMu.root"]
bgdFiles_e = [inputDir2+"SusyFitterTree_OneEle.root"]
bgdFiles_m = [inputDir2+"SusyFitterTree_OneMu.root"]
bgdFiles_se = [inputDirSLBkg+"SusyFitterTree_OneSoftEle_BG_v7.root"]
bgdFiles_sm = [inputDirSLBkg+"SusyFitterTree_OneSoftMuo_BG_v7.root"]
bgdFiles_comb = [inputDir2+"SusyFitterTree_OneEle.root",inputDir2+"SusyFitterTree_OneMu.root"]

sigfile_MH_v2 =  "/afs/cern.ch/work/h/hyamaguc/public/samples/SusyFitterTree_p832_GGonestep_paper_v2.root"
  
if doExclusion_GMSB_combined:
    sigFiles+=[inputDirSig+"/SusyFitterTree_EleEle_GMSB.root",inputDirSig+"/SusyFitterTree_EleMu_GMSB.root",inputDirSig+"/SusyFitterTree_MuMu_GMSB.root"]

if doExclusion_mSUGRA_dilepton_combined:
    sigFiles+=[inputDirSig+"/SusyFitterTree_EleEle_mSUGRA.root",inputDirSig+"/SusyFitterTree_EleMu_mSUGRA.root",inputDirSig+"/SusyFitterTree_MuMu_mSUGRA.root"]
    sigFiles_l+=[inputDirSig+"/SusyFitterTree_p832_mSUGRA_paper_v1.root"]
 
if doExclusion_GG_onestepCC_x12:
    sigFiles+=[inputDirSig+"/SusyFitterTree_EleEle_SM_GG_onestepCC.root",inputDirSig+"/SusyFitterTree_MuMu_SM_GG_onestepCC.root",inputDirSig+"/SusyFitterTree_EleMu_SM_GG_onestepCC.root"]
    sigFiles_l+=[sigfile_MH_v2] #inputDirSig+"/SusyFitterTree_p832_GGonestepLSP60_paper_v1.root"
    sigFiles_sl+=[inputDirSig+"/SusyFitterTree_OneSoftMuo_SM_GG_onestepCC_v3.root",inputDirSig+"/SusyFitterTree_OneSoftEle_SM_GG_onestepCC_v3.root"]

if doExclusion_GG_onestepCC_gridX:
    sigFiles+=[inputDirSig+"/SusyFitterTree_EleEle_SM_GG_onestepCC.root",inputDirSig+"/SusyFitterTree_MuMu_SM_GG_onestepCC.root",inputDirSig+"/SusyFitterTree_EleMu_SM_GG_onestepCC.root"]
    sigFiles_l+=[inputDirSig+"/SusyFitterTree_p832_GGonestepLSP60_paper_v1.root"]
    sigFiles_sl+=[inputDirSig+"/SusyFitterTree_OneSoftMuo_SM_GG_onestepCC_LSP60_v3.root",inputDirSig+"/SusyFitterTree_OneSoftEle_SM_GG_onestepCC_LSP60_v3.root"]

if doExclusion_GG_twostepCC_slepton:
    sigFiles+=[inputDirSig+"/SusyFitterTree_EleEle_SM_GG_twostepCC_slepton.root",inputDirSig+"/SusyFitterTree_EleMu_SM_GG_twostepCC_slepton.root",inputDirSig+"/SusyFitterTree_MuMu_SM_GG_twostepCC_slepton.root"]


if doExclusion_mSUGRA_multijets:
    if not onLxplus:
        sigFiles_l+=["data/SusyFitterTree_p832_mSUGRA_paper_v1.root"]
    else:
        sigFiles_l+=[inputDirSig+"/SusyFitterTree_p832_mSUGRA_paper_v1.root"]

if doExclusion_GG_twosteps_multijets:
    if not onLxplus:
        sigFiles_l+=["data/SusyFitterTree_p832_GGtwostep_paper_v1.root"]
    else:
        sigFiles_l+=[inputDirSig+"/SusyFitterTree_p832_GGtwostep_paper_v1.root"]

if doExclusion_SS_twosteps_multijets:
    if not onLxplus:
        sigFiles_l+=["data/SusyFitterTree_p832_SStwostep_paper_v1.root"]
    else:
        sigFiles_l+=[inputDirSig+"/SusyFitterTree_p832_SStwostep_paper_v1.root"]

if doExclusion_Gtt_multijets:
    if not onLxplus:
        sigFiles_l+=["data/SusyFitterTree_OneEle_SM_Gtt_v1.root","data/SusyFitterTree_OneMu_SM_Gtt_v1.root"]
    else:
        sigFiles_l+=[inputDirSig+"/SusyFitterTree_OneEle_SM_Gtt_v1.root",inputDirSig+"/SusyFitterTree_OneMu_SM_Gtt_v1.root"]

# AnalysisType corresponds to ee,mumu,emu as I want to split these channels up

# Map regions to cut strings
configMgr.cutsDict = {}
configMgr.cutsDict["TRee"]="(mll<80 || mll>100) && met > 30 && met < 80 && jet2Pt > 50 && (jet1Pt > 80 || jet4Pt > 50) && nB3Jet > 0 && AnalysisType==3"
configMgr.cutsDict["TRmm"]="(mll<80 || mll>100) && met > 30 && met < 80 && jet2Pt > 50 && (jet1Pt > 80 || jet4Pt > 50) && nB3Jet > 0 && AnalysisType==4"
configMgr.cutsDict["TRem"]="(mll<80 || mll>100) && met > 30 && met < 80 && jet2Pt > 50 && (jet1Pt > 80 || jet4Pt > 50) && nB3Jet > 0 && AnalysisType==5"
configMgr.cutsDict["ZRee"]="mll>80 && mll<100  && met < 50 && jet2Pt > 50 && (jet1Pt > 80 || jet4Pt > 50) && AnalysisType==3"
configMgr.cutsDict["ZRmm"]="mll>80 && mll<100  && met < 50 && jet2Pt > 50 && (jet1Pt > 80 || jet4Pt > 50) && AnalysisType==4"

configMgr.cutsDict["S2ee"]="met > 300 && nJet>=2 && jet2Pt > 200 && jet4Pt < 50 && AnalysisType==3"
configMgr.cutsDict["S2mm"]="met > 300 && nJet>=2 && jet2Pt > 200 && jet4Pt < 50 && AnalysisType==4"
configMgr.cutsDict["S2em"]="met > 300 && nJet>=2 && jet2Pt > 200 && jet4Pt < 50 && AnalysisType==5"

configMgr.cutsDict["TVJ2ee"]="met > 100 && met < 300 && jet4Pt < 50 && jet2Pt > 120 && AnalysisType==3"
configMgr.cutsDict["TVJ2em"]="met > 100 && met < 300 && jet4Pt < 50 && jet2Pt > 120 && AnalysisType==5"
configMgr.cutsDict["TVJ2mm"]="met > 100 && met < 300 && jet4Pt < 50 && jet2Pt > 120 && AnalysisType==4"

configMgr.cutsDict["ZVJ2ee"]="met > 50 && met < 100 && jet4Pt < 50 && jet2Pt > 120 && nB3Jet == 0 && AnalysisType==3"
configMgr.cutsDict["ZVJ2em"]="met > 50 && met < 100 && jet4Pt < 50 && jet2Pt > 120 && nB3Jet == 0 && AnalysisType==5"                    
configMgr.cutsDict["ZVJ2mm"]="met > 50 && met < 100 && jet4Pt < 50 && jet2Pt > 120 && nB3Jet == 0 && AnalysisType==4"

configMgr.cutsDict["S4ee"]="met > 100 && nJet>=4 && jet4Pt > 50 && met/meff4Jet > 0.2 && meffInc > 650 && AnalysisType==3"
configMgr.cutsDict["S4mm"]="met > 100 && nJet>=4 && jet4Pt > 50 && met/meff4Jet > 0.2 && meffInc > 650 && AnalysisType==4"
configMgr.cutsDict["S4em"]="met > 100 && nJet>=4 && jet4Pt > 50 && met/meff4Jet > 0.2 && meffInc > 650 && AnalysisType==5"

configMgr.cutsDict["TVJ4ee"]="met > 80 && met < 100 && jet4Pt > 50 && nB3Jet > 0 && AnalysisType==3"
configMgr.cutsDict["TVJ4em"]="met > 80 && met < 100 && jet4Pt > 50 && nB3Jet > 0 && AnalysisType==5"
configMgr.cutsDict["TVJ4mm"]="met > 80 && met < 100 && jet4Pt > 50 && nB3Jet > 0 && AnalysisType==4"

configMgr.cutsDict["ZVJ4ee"]="met > 50 && met < 100 & jet4Pt > 50 && nB3Jet == 0 && AnalysisType==3"
configMgr.cutsDict["ZVJ4em"]="met > 50 && met < 100 & jet4Pt > 50 && nB3Jet == 0 && AnalysisType==5"
configMgr.cutsDict["ZVJ4mm"]="met > 50 && met < 100 & jet4Pt > 50 && nB3Jet == 0 && AnalysisType==4"

configMgr.cutsDict["VR3ee"]="met > 100 && met < 300 && jet4Pt < 50 && jet3Pt > 50 && jet1Pt > 80 && AnalysisType==3"
configMgr.cutsDict["VR3em"]="met > 100 && met < 300 && jet4Pt < 50 && jet3Pt > 50 && jet1Pt > 80 && AnalysisType==5"
configMgr.cutsDict["VR3mm"]="met > 100 && met < 300 && jet4Pt < 50 && jet3Pt > 50 && jet1Pt > 80 && AnalysisType==4"

configMgr.cutsDict["VZR3ee"]="met > 50 && met < 100  && jet3Pt > 50 && jet1Pt > 80 && nB3Jet == 0 && AnalysisType==3"
configMgr.cutsDict["VZR3em"]="met > 50 && met < 100 && jet3Pt > 50 && jet1Pt > 80 && nB3Jet == 0 && AnalysisType==5"
configMgr.cutsDict["VZR3mm"]="met > 50 && met < 100 && jet3Pt > 50 && jet1Pt > 80 && nB3Jet == 0 && AnalysisType==4"

configMgr.cutsDict["HMTVL1El"]="AnalysisType==1 && met>40 && met<250 && mt>80 && jet1Pt>80 && jet3Pt>25 && meffInc>500"
configMgr.cutsDict["HMTVL1Mu"]="AnalysisType==2 && met>40 && met<250 && mt>80 && jet1Pt>80 && jet3Pt>25 && meffInc>500"

configMgr.cutsDict["WVL1El"]="lep2Pt<10 && met>150 && met<250 && mt>40 && mt<80 && nB3Jet==0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==1"
configMgr.cutsDict["WVL1Mu"]="lep2Pt<10 && met>150 && met<250 && mt>40 && mt<80 && nB3Jet==0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==2"

configMgr.cutsDict["TVL1El"]="lep2Pt<10 && met>150 && met<250 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==1"
configMgr.cutsDict["TVL1Mu"]="lep2Pt<10 && met>150 && met<250 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==2"

configMgr.cutsDict["WREl"]="lep2Pt<10 && met>40 && met<150 && mt>40 && mt<80 && nB3Jet==0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==1"
configMgr.cutsDict["TREl"]="lep2Pt<10 && met>40 && met<150 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==1"
configMgr.cutsDict["WRMu"]="lep2Pt<10 && met>40 && met<150 && mt>40 && mt<80 && nB3Jet==0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==2"
configMgr.cutsDict["TRMu"]="lep2Pt<10 && met>40 && met<150 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==2"

configMgr.cutsDict["TRElVR"]="lep2Pt<10 && met>40 && met<150 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && AnalysisType==1"
configMgr.cutsDict["TRMuVR"]="lep2Pt<10 && met>40 && met<150 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && AnalysisType==2"

configMgr.cutsDict["TRElVR2"]="lep2Pt<10 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && AnalysisType==1"
configMgr.cutsDict["TRMuVR2"]="lep2Pt<10 && nB3Jet>0 && jet1Pt>80 && jet3Pt>25 && AnalysisType==2" 

configMgr.cutsDict["WRElVR"]="lep2Pt<10 && met>50 && nB3Jet==0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==1"
configMgr.cutsDict["WRMuVR"]="lep2Pt<10 && met>50 && nB3Jet==0 && jet1Pt>80 && jet3Pt>25 && meffInc > 500 && AnalysisType==2"

configMgr.cutsDict["S3El"]="AnalysisType==1 && met>250 && mt>100 && met/meff3Jet>0.3 && jet1Pt>100 && jet3Pt>25 && jet4Pt<80"
configMgr.cutsDict["S4El"]="AnalysisType==1 && met>250 && mt>100 && met/meff4Jet>0.2 && jet4Pt>80"

configMgr.cutsDict["S3Mu"]="AnalysisType==2 && met>250 && mt>100 && met/meff3Jet>0.3 && jet1Pt>100 && jet3Pt>25 && jet4Pt<80"
configMgr.cutsDict["S4Mu"]="AnalysisType==2 && met>250 && mt>100 && met/meff4Jet>0.2 && jet4Pt>80"

configMgr.cutsDict["SR3jTEl"]="AnalysisType==1 && met>250 && mt>100 && met/meff3Jet>0.3 && jet1Pt>100 && jet3Pt>25 && jet4Pt<80 && meffInc>1200"
configMgr.cutsDict["SR4jTEl"]="AnalysisType==1 && met>250 && mt>100 && met/meff4Jet>0.2 && jet4Pt>80 && meffInc>800"

configMgr.cutsDict["SR3jTMu"]="AnalysisType==2 && met>250 && mt>100 && met/meff3Jet>0.3 && jet1Pt>100 && jet3Pt>25 && jet4Pt<80 && meffInc>1200"
configMgr.cutsDict["SR4jTMu"]="AnalysisType==2 && met>250 && mt>100 && met/meff4Jet>0.2 && jet4Pt>80 && meffInc>800"
configMgr.cutsDict["SR7jTEl"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && jet7Pt>25 && meffInc>750" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTMu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && jet7Pt>25 && meffInc>750" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"

configMgr.cutsDict["SR7jTSDDCombBin3El"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==3" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin3El"]="AnalysisType==1 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==3 && nB3Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin4El"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==4" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin4El"]="AnalysisType==1 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==4 && nB4Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin5El"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==5" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin5El"]="AnalysisType==1 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==5 && nB5Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin6El"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==6" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin6El"]="AnalysisType==1 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==6 && nB6Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin7El"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet>=7" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin7El"]="AnalysisType==1 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet>=7 && nB7Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"

configMgr.cutsDict["SR7jTSDDCombBin3Mu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==3" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin3Mu"]="AnalysisType==2 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==3 && nB3Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin4Mu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==4" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin4Mu"]="AnalysisType==2 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==4 && nB4Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin5Mu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==5" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin5Mu"]="AnalysisType==2 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==5 && nB5Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin6Mu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet==6" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin6Mu"]="AnalysisType==2 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet==6 && nB6Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSDDCombBin7Mu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && meffInc>750 && nJet>=7" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jTSCRCombBin7Mu"]="AnalysisType==2 && met>180 && mt>40 && mt<100 && jet1Pt>80 && meffInc>750 && nJet>=7 && nB7Jet==0" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"

configMgr.cutsDict["WREl7J"] = "(AnalysisType==1) && lep2Pt<10 && met>40 && met<120 && mt>40 && mt<80 && nB7Jet==0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"
configMgr.cutsDict["TREl7J"] = "(AnalysisType==1) && lep2Pt<10 && met>40 && met<120 && mt>40 && mt<80 && nB7Jet>0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"
configMgr.cutsDict["WRMu7J"] = "(AnalysisType==2) && lep2Pt<10 && met>40 && met<120 && mt>40 && mt<80 && nB7Jet==0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"
configMgr.cutsDict["TRMu7J"] = "(AnalysisType==2) && lep2Pt<10 && met>40 && met<120 && mt>40 && mt<80 && nB7Jet>0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"

configMgr.cutsDict["WVL1El7J"] = "AnalysisType==1 && lep2Pt<10 && met>120 && met<180 && mt>40 && mt<80 && nB7Jet==0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"
configMgr.cutsDict["WVL1Mu7J"] = "AnalysisType==2 && lep2Pt<10 && met>120 && met<180 && mt>40 && mt<80 && nB7Jet==0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"

configMgr.cutsDict["TVL1El7J"] = "AnalysisType==1 && lep2Pt<10 && met>120 && met<180 && mt>40 && mt<80 && nB7Jet>0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"
configMgr.cutsDict["TVL1Mu7J"] = "AnalysisType==2 && lep2Pt<10 && met>120 && met<180 && mt>40 && mt<80 && nB7Jet>0 && jet1Pt>80 && jet7Pt>25 && meffInc>400"

configMgr.cutsDict["tmpVREl"] = "(AnalysisType==1) && lep2Pt<10 && met>40 && mt>40 && jet1Pt>80 && jet7Pt>25"
configMgr.cutsDict["tmpVRMu"] = "(AnalysisType==2) && lep2Pt<10 && met>40 && mt>40 && jet1Pt>80 && jet7Pt>25"

configMgr.cutsDict["SR7jTSLEl"] = "(AnalysisType==6) && lep1Pt>10 && lep1Pt<20 && lep2Pt<10 && mt>120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"
configMgr.cutsDict["SR7jTSLMu"] = "(AnalysisType==7) && lep1Pt>10 && lep1Pt<20 && lep2Pt<10 && mt>120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"

configMgr.cutsDict["SR7jTmtSLEl"] = "(AnalysisType==6) && lep1Pt>10 && lep1Pt<20 && lep2Pt<10 && mt>40 && mt<120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"
configMgr.cutsDict["SR7jTmtSLMu"] = "(AnalysisType==7) && lep1Pt>10 && lep1Pt<20 && lep2Pt<10 && mt>40 && mt<120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"

configMgr.cutsDict["SR7jEl"]="AnalysisType==1 && met>180 && mt>120 && jet1Pt>80 && jet7Pt>25" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"
configMgr.cutsDict["SR7jMu"]="AnalysisType==2 && met>180 && mt>120 && jet1Pt>80 && jet7Pt>25" # && !(lep1Pt>500 && (1.-((mt*mt)/(2.*lep1Pt*met)))<-0.9)"

configMgr.cutsDict["SR7jTmetEl"]="AnalysisType==1 && met>40 && met<180 && mt>120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"
configMgr.cutsDict["SR7jTmetMu"]="AnalysisType==2 && met>40 && met<180 && mt>120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"

configMgr.cutsDict["SR7jTmtEl"]="AnalysisType==1 && met>180 && mt>40 && mt<120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"
configMgr.cutsDict["SR7jTmtMu"]="AnalysisType==2 && met>180 && mt>40 && mt<120 && jet1Pt>80 && jet7Pt>25 && meffInc>750"

configMgr.cutsDict["SVEl"]="(lep1Pt<25 && lep2Pt<10 && met>180 && met<250 && mt>80 && mt<100 && jet1Pt>130 && jet2Pt>25 && AnalysisType==6)"
configMgr.cutsDict["SVMu"]="(lep1Pt<20 && lep2Pt<10 && met>180 && met<250 && mt>80 && mt<100 && jet1Pt>130 && jet2Pt>25 && AnalysisType==7)"

configMgr.cutsDict["SVWEl"]="lep1Pt<25 && lep2Pt<10 && met>180 && met<250 && mt>40 && mt<80 && nB3Jet==0 && jet1Pt>130 && jet2Pt>25 && AnalysisType==6"
configMgr.cutsDict["SVTEl"]="lep1Pt<25 && lep2Pt<10 && met>180 && met<250 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>130 && jet2Pt>25 && AnalysisType==6"
configMgr.cutsDict["SVWMu"]="lep1Pt<20 && lep2Pt<10 && met>180 && met<250 && mt>40 && mt<80 && nB3Jet==0 && jet1Pt>130 && jet2Pt>25 && AnalysisType==7"
configMgr.cutsDict["SVTMu"]="lep1Pt<20 && lep2Pt<10 && met>180 && met<250 && mt>40 && mt<80 && nB3Jet>0 && jet1Pt>130 && jet2Pt>25 && AnalysisType==7"

configMgr.cutsDict["SSEl"]="lep1Pt < 25 && lep2Pt<10 && met>250 && mt>100 && jet1Pt>130 && jet2Pt>25 && AnalysisType==6"
configMgr.cutsDict["SSMu"]="lep1Pt < 20 && lep2Pt<10 && met>250 && mt>100 && jet1Pt>130 && jet2Pt>25 && AnalysisType==7"

d=configMgr.cutsDict
configMgr.cutsDict["SSElT"] = d["SSEl"]+"&& met/meff2Jet>0.3"
configMgr.cutsDict["SSMuT"] = d["SSMu"]+"&& met/meff2Jet>0.3"
#To allow 1-bin and multi-bins channels based on same cuts
configMgr.cutsDict["S2eeT"] = d["S2ee"]
configMgr.cutsDict["S2emT"] = d["S2em"]
configMgr.cutsDict["S2mmT"] = d["S2mm"]
configMgr.cutsDict["S4eeT"] = d["S4ee"]
configMgr.cutsDict["S4emT"] = d["S4em"]
configMgr.cutsDict["S4mmT"] = d["S4mm"]

## Lists of weights
if doWptReweighting:
    truthWptWeight="truthWptWeight"
else:
    truthWptWeight="1"

weights = ["genWeight","eventWeight","leptonWeight","triggerWeight",truthWptWeight, \
           "truthZpt0GeVWeight", "truthZpt50GeVWeight","truthZpt100GeVWeight","truthZpt150GeVWeight", \
           "truthZpt200GeVWeight","bTagWeight7Jet"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

if not doExclusion_Gtt_multijets:
    xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
    xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")
    print "KHRAMOV 1 set xsecSigHighWeights"

#ktScaleWHighWeights = addWeight(weights,"ktfacUpWeightW")
#ktScaleWHighWeights = addWeight(weights,"ktfacDownWeightW")

#ktScaleWHighWeights = addWeight(weights,"ktfacUpWeightTop")
#ktScaleWHighWeights = addWeight(weights,"ktfacDownWeightTop")
                    
bTagHighWeights = replaceWeight(weights,"bTagWeight7Jet","bTagWeight7JetUp")
bTagLowWeights = replaceWeight(weights,"bTagWeight7Jet","bTagWeight7JetDown")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights,"leptonWeight","leptonWeightDown")

## True Zpt reweighting
pT0GeVHighWeights = replaceWeight(weights,"truthZpt0GeVWeight","truthZpt0GeVWeightUp")
pT0GeVLowWeights = replaceWeight(weights,"truthZpt0GeVWeight","truthZpt0GeVWeightDown")

pT50GeVHighWeights = replaceWeight(weights,"truthZpt50GeVWeight","truthZpt50GeVWeightUp")
pT50GeVLowWeights = replaceWeight(weights,"truthZpt50GeVWeight","truthZpt50GeVWeightDown")

pT100GeVHighWeights = replaceWeight(weights,"truthZpt100GeVWeight","truthZpt100GeVWeightUp")
pT100GeVLowWeights = replaceWeight(weights,"truthZpt100GeVWeight","truthZpt100GeVWeightDown")

pT150GeVHighWeights = replaceWeight(weights,"truthZpt150GeVWeight","truthZpt150GeVWeightUp")
pT150GeVLowWeights = replaceWeight(weights,"truthZpt150GeVWeight","truthZpt150GeVWeightDown")

pT200GeVHighWeights = replaceWeight(weights,"truthZpt200GeVWeight","truthZpt200GeVWeightUp")
pT200GeVLowWeights = replaceWeight(weights,"truthZpt200GeVWeight","truthZpt200GeVWeightDown")

## HF uncertainty on V+Jets
hfHighWeights = addWeight(weights,"hfWeightUp")
hfLowWeights = addWeight(weights,"hfWeightDown")
                                                                                        
#--------------------
# List of systematics
#--------------------
configMgr.nomName = "_NoSys"

# Signal XSec uncertainty as overallSys (pure yeild affect) DEPRECATED
if not doExclusion_Gtt_multijets:
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
    print "KHRAMOV 2 set xsecSig"
# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig","_NoSys","_JESup","_JESdown","tree","histoSys")

basicChanSyst = []
basicChanSyst.append(Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys")) # JES uncertainty - for low pt jets
basicChanSyst.append(Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys")) # JES uncertainty - for medium pt jets
basicChanSyst.append(Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys")) # JES uncertainty - for high pt jets
basicChanSyst.append(Systematic("MC","_NoSys","_METCOup","_METCOdown","tree","histoSys")) # MET cell-out uncertainty - one per channel
basicChanSyst.append(Systematic("MP","_NoSys","_METPUup","_METPUdown","tree","histoSys")) # MET pileup uncertainty - one per channel
             
seChanSyst = []
elChanSyst = []
muChanSyst = []
smChanSyst = []
eeChanSyst = []
mmChanSyst = []
emChanSyst = []
combChanSyst = []
# Lepton weight uncertainty
seChanSyst.append(Systematic("LEse",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")) 
elChanSyst.append(Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")) 
smChanSyst.append(Systematic("LEsm",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
muChanSyst.append(Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
eeChanSyst.append(Systematic("LEee",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
mmChanSyst.append(Systematic("LEmm",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
emChanSyst.append(Systematic("LEem",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
# Leptson energy scale uncertainty
seChanSyst.append(Systematic("LESse","_NoSys","_LESup","_LESdown","tree","overallSys")) 
elChanSyst.append(Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")) 
smChanSyst.append(Systematic("LESsm","_NoSys","_LESup","_LESdown","tree","overallSys")) 
muChanSyst.append(Systematic("LESmu","_NoSys","_LESup","_LESdown","tree","overallSys")) 
eeChanSyst.append(Systematic("LESee","_NoSys","_LESup","_LESdown","tree","overallSys"))
mmChanSyst.append(Systematic("LESmm","_NoSys","_LESup","_LESdown","tree","overallSys"))
emChanSyst.append(Systematic("LESem","_NoSys","_LESup","_LESdown","tree","overallSys"))
# Muon energy resolution (Muon Spectrometer)
smChanSyst.append(Systematic("LRMsm","_NoSys","_LERMSup","_LERMSdown","tree","overallSys"))
muChanSyst.append(Systematic("LRMmu","_NoSys","_LERMSup","_LERMSdown","tree","overallSys"))
mmChanSyst.append(Systematic("LRMmm","_NoSys","_LERMSup","_LERMSdown","tree","overallSys"))
emChanSyst.append(Systematic("LRMem","_NoSys","_LERMSup","_LERMSdown","tree","overallSys"))
# Muon energy resolution (Inner Detector)
smChanSyst.append(Systematic("LRIsm","_NoSys","_LERIDup","_LERIDdown","tree","overallSys"))
muChanSyst.append(Systematic("LRImu","_NoSys","_LERIDup","_LERIDdown","tree","overallSys"))
mmChanSyst.append(Systematic("LRImm","_NoSys","_LERIDup","_LERIDdown","tree","overallSys"))
emChanSyst.append(Systematic("LRIem","_NoSys","_LERIDup","_LERIDdown","tree","overallSys"))
# Trigger efficiency
seChanSyst.append(Systematic("TExe",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
elChanSyst.append(Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
smChanSyst.append(Systematic("TExe",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
muChanSyst.append(Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
eeChanSyst.append(Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
mmChanSyst.append(Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
emChanSyst.append(Systematic("TEem",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))

bTagSyst = Systematic("BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallSys")

##### Ptmin (asymmetric normalized)

# CRs
topPtMin30HLCR = Systematic("PtMinTop",configMgr.weights,[1.08,1.05,1.003,1.001,1.001,1.004,1.001],[0.999,0.999,0.999,0.98,0.95,0.999,0.9],"user","userNormHistoSys")
wzPtMin30HLCR = Systematic("PtMinWZ",configMgr.weights,[1.001,1.001,1.006,1.06,1.2,1.02,1.06],[0.999,0.98,0.999,0.999,0.999,0.999,0.999],"user","userNormHistoSys")
topPtMin30DLCR = Systematic("PtMinTop",configMgr.weights,[1.06,1.04,1.001,1.001,1.001,1.11,1.05,1],[0.999,0.999,0.99,0.92,0.93,0.999,0.999,1],"user","userNormHistoSys")
wzPtMin30DLCR = Systematic("PtMinWZ",configMgr.weights,[1.03,1.001,1.001,1.001,1.05,1.13,1,1],[0.999,0.97,0.93,0.96,0.999,0.999,1,1],"user","userNormHistoSys")
topPtMin30SLCR = Systematic("PtMinTop",configMgr.weights,[1.003,1.04,1.001,1.001,1.001,1.11],[0.999,0.999,0.99,0.98,0.96,0.999],"user","userNormHistoSys")
wzPtMin30SLCR = Systematic("PtMinWZ",configMgr.weights,[1.03,1.001,1.001,1.001,1.001,1.03],[0.999,0.96,0.9,0.86,0.88,0.999],"user","userNormHistoSys")

#SRs
topPtMin30S3 = Systematic("PtMinTopSR",configMgr.weights,1.12,0.88,"user","userOverallSys")
wzPtMin30S3 = Systematic("PtMinWZSR",configMgr.weights,1.19,0.81,"user","userOverallSys")
topPtMin30S4 = Systematic("PtMinTopSR",configMgr.weights,1.16,0.84,"user","userOverallSys")
wzPtMin30S4 = Systematic("PtMinWZSR",configMgr.weights,1.08,0.92,"user","userOverallSys")
topPtMin30SS = Systematic("PtMinTopSR",configMgr.weights,1.02,0.98,"user","userOverallSys")
wzPtMin30SS = Systematic("PtMinWZSR",configMgr.weights,1.30,0.70,"user","userOverallSys")
topPtMin30DLS2 = Systematic("PtMinTopSR",configMgr.weights,1.11,0.89,"user","userOverallSys")
wzPtMin30DLS2 = Systematic("PtMinWZSR",configMgr.weights,1.14,0.86,"user","userOverallSys")
topPtMin30DLS4 = Systematic("PtMinTopSR",configMgr.weights,1.01,0.99,"user","userOverallSys")
wzPtMin30DLS4 = Systematic("PtMinWZSR",configMgr.weights,1.08,0.92,"user","userOverallSys")

##Hadronization in SRs as userOverallSys for VRs

meffCR_SR347=500.0
metCR_SRSL=180.
metovermeffCR_SRSL=0.1
meffCRT_SR24=150.
meffCRWZ_SR24=100.
meffMax=10000.0

from SystematicsUtils import hadroSys,addHadronizationSyst,hadroSysBins
hadTop_SR3jT = Systematic("hadTop",configMgr.weights,1.0+hadroSys(meffCR_SR347,1200.0,"ttbar","meff"),1.0-hadroSys(meffCR_SR347,1200.0,"ttbar","meff"),"user","userOverallSys")
hadWZ_SR3jT  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(meffCR_SR347,1200.0,"WZ","meff"),   1.0-hadroSys(meffCR_SR347,1200.0,"WZ","meff"),"user","userOverallSys")
#SR4jT
hadTop_SR4jT = Systematic("hadTop",configMgr.weights,1.0+hadroSys(meffCR_SR347,800.0,"ttbar","meff"),1.0-hadroSys(meffCR_SR347,800.0,"ttbar","meff"),"user","userOverallSys")
hadWZ_SR4jT  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(meffCR_SR347,800.0,"WZ","meff"),   1.0-hadroSys(meffCR_SR347,800.0,"WZ","meff"),"user","userOverallSys")
#SR7jT
hadTop_SR7jT = Systematic("hadTop",configMgr.weights,1.0+hadroSys(meffCR_SR347,650.0,"ttbar","meff"),1.0-hadroSys(meffCR_SR347,750.0,"ttbar","meff"),"user","userOverallSys")
hadWZ_SR7jT  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(meffCR_SR347,650.0,"WZ","meff"),   1.0-hadroSys(meffCR_SR347,750.0,"WZ","meff"),"user","userOverallSys")
#SL
hadTop_SRSL = Systematic("hadTop",configMgr.weights,1.0+hadroSys(metCR_SRSL,250.0,"ttbar","met"),1.0-hadroSys(metCR_SRSL,250.0,"ttbar","met"),"user","userOverallSys")
hadWZ_SRSL  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(metCR_SRSL,250.0,"WZ","met"),   1.0-hadroSys(metCR_SRSL,250.0,"WZ","met"),"user","userOverallSys")
#hadWZ_SRSL  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(metovermeffCR_SRSL,0.7,"WZ","metovermeff"),   1.0-hadroSys(metovermeffCR_SRSL,0.7,"WZ","metovermeff"),"user","userOverallSys")
#S2
hadTop_SRS2 = Systematic("hadTop",configMgr.weights,1.0+hadroSys(meffCRT_SR24,700.0,"ttbar","meff"),1.0-hadroSys(meffCRT_SR24,700.0,"ttbar","met"),"user","userOverallSys")
hadWZ_SRS2  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(meffCRWZ_SR24,700.0,"WZ","meff"),   1.0-hadroSys(meffCRWZ_SR24,700.0,"WZ","met"),"user","userOverallSys")
#S4
hadTop_SRS4 = Systematic("hadTop",configMgr.weights,1.0+hadroSys(meffCRT_SR24,650.0,"ttbar","meff"),1.0-hadroSys(meffCRT_SR24,650.0,"ttbar","meff"),"user","userOverallSys")
hadWZ_SRS4  = Systematic("hadWZ",configMgr.weights,1.0+hadroSys(meffCRWZ_SR24,650.0,"WZ","meff"),   1.0-hadroSys(meffCRWZ_SR24,650.0,"WZ","meff"),"user","userOverallSys")

##Hadronization in SRs as userHistoSys for exclusion fits
#Hard 1 lepton SR binning
meffNBins1lS3 = 6
meffBinLow1lS3 = 400.
meffBinHigh1lS3 = 1600.

meffNBins1lS4 = 4
meffBinLow1lS4 = 800.
meffBinHigh1lS4 = 1600.

#Dilepton SR binning
meffNBinsS2 = 5
meffBinLowS2 = 700.
meffBinHighS2 = 1700.

meffNBinsS4 = 5
meffBinLowS4 = 600.
meffBinHighS4 = 1600.

meffNBinsHL = 6
meffBinLowHL = 400.
meffBinHighHL = 1600.

metovermeffNBinsSL = 6
metovermeffBinLowSL = 0.1
metovermeffBinHighSL = 0.7

hadTop_SR3jT_hist = Systematic(*(("hadTop",configMgr.weights)+hadroSysBins(meffCR_SR347,meffNBins1lS3,meffBinLow1lS3,meffBinHigh1lS3,"ttbar","meff")+("user","userNormHistoSys")))
hadWZ_SR3jT_hist = Systematic(*(("hadWZ",configMgr.weights)+hadroSysBins(meffCR_SR347,meffNBins1lS3,meffBinLow1lS3,meffBinHigh1lS3,"WZ","meff")+("user","userNormHistoSys")))
hadTop_SR4jT_hist = Systematic(*(("hadTop",configMgr.weights)+hadroSysBins(meffCR_SR347,meffNBins1lS4,meffBinLow1lS4,meffBinHigh1lS4,"ttbar","meff")+("user","userNormHistoSys")))
hadWZ_SR4jT_hist = Systematic(*(("hadWZ",configMgr.weights)+hadroSysBins(meffCR_SR347,meffNBins1lS4,meffBinLow1lS4,meffBinHigh1lS4,"WZ","meff")+("user","userNormHistoSys")))
hadTop_SR7jT_hist = Systematic(*(("hadTop",configMgr.weights)+hadroSysBins(meffCR_SR347,4,750,meffBinHigh1lS3,"ttbar","meff")+("user","userNormHistoSys"))) 
hadWZ_SR7jT_hist = Systematic(*(("hadWZ",configMgr.weights)+hadroSysBins(meffCR_SR347,4,750,meffBinHigh1lS3,"WZ","meff")+("user","userNormHistoSys"))) 

hadTop_SRS2_hist = Systematic(*(("hadTop",configMgr.weights)+hadroSysBins(meffCRT_SR24,meffNBinsS2,meffBinLowS2,meffBinHighS2,"ttbar","meff")+("user","userNormHistoSys")))
hadWZ_SRS2_hist = Systematic(*(("hadWZ",configMgr.weights)+hadroSysBins(meffCRWZ_SR24,meffNBinsS2,meffBinLowS2,meffBinHighS2,"WZ","meff")+("user","userNormHistoSys")))
hadTop_SRS4_hist = Systematic(*(("hadTop",configMgr.weights)+hadroSysBins(meffCRT_SR24,meffNBinsS4,meffBinLowS4,meffBinHighS4,"ttbar","meff")+("user","userNormHistoSys")))
hadWZ_SRS4_hist = Systematic(*(("hadWZ",configMgr.weights)+hadroSysBins(meffCRWZ_SR24,meffNBinsS4,meffBinLowS4,meffBinHighS4,"WZ","meff")+("user","userNormHistoSys")))

#hadWZ_SRSL_hist = Systematic(*(("hadWZ",configMgr.weights)+hadroSysBins(metovermeffCR_SRSL,metovermeffNBinsSL,metovermeffBinLowSL,metovermeffBinHighSL,"WZ","metovermeff")+("user","userNormHistoSys")))

# List of samples and their plotting colours
AlpGenSamples=[]

topSample_Np0 = Sample("Top_Np0",100)
topSample_Np0.setNormFactor("mu_Top",1.,0.,5.)
AlpGenSamples.append(topSample_Np0)

wzSample_Np0 = Sample("WZ_Np0",55)
wzSample_Np0.setNormFactor("mu_WZ",1.,0.,5.)
AlpGenSamples.append(wzSample_Np0)

topSample_Np1 = Sample("Top_Np1",100)
topSample_Np1.setNormFactor("mu_Top",1.,0.,5.)
AlpGenSamples.append(topSample_Np1)

wzSample_Np1 = Sample("WZ_Np1",55)
wzSample_Np1.setNormFactor("mu_WZ",1.,0.,5.)
AlpGenSamples.append(wzSample_Np1)

topSample_Np2 = Sample("Top_Np2",100)
topSample_Np2.setNormFactor("mu_Top",1.,0.,5.)
AlpGenSamples.append(topSample_Np2)

wzSample_Np2 = Sample("WZ_Np2",55)
wzSample_Np2.setNormFactor("mu_WZ",1.,0.,5.)
AlpGenSamples.append(wzSample_Np2)

topSample_Np3 = Sample("Top_Np3",100)
topSample_Np3.setNormFactor("mu_Top",1.,0.,5.)
AlpGenSamples.append(topSample_Np3)

wzSample_Np3 = Sample("WZ_Np3",55)
wzSample_Np3.setNormFactor("mu_WZ",1.,0.,5.)
AlpGenSamples.append(wzSample_Np3)

topSample_Np4 = Sample("Top_Np4",100)
topSample_Np4.setNormFactor("mu_Top",1.,0.,5.)
AlpGenSamples.append(topSample_Np4)

wzSample_Np4 = Sample("WZ_Np4",55)
wzSample_Np4.setNormFactor("mu_WZ",1.,0.,5.)
AlpGenSamples.append(wzSample_Np4)

topSample_Np5 = Sample("Top_Np5",100)
topSample_Np5.setNormFactor("mu_Top",1.,0.,5.)
AlpGenSamples.append(topSample_Np5) 

wzSample_Np5 = Sample("WZ_Np5",55)
wzSample_Np5.setNormFactor("mu_WZ",1.,0.,5.)
AlpGenSamples.append(wzSample_Np5)

#topSample = Sample("Top",91)
#topSample.setNormFactor("mu_Top",1.,0.,5.)
#AlpGenSamples.append(topSample)

#wzSample = Sample("WZ",70)
#wzSample.setNormFactor("mu_WZ",1.,0.,5.)
#AlpGenSamples.append(wzSample)

AlpGenSamples.sort(key=lambda x: x.name)

for sam in AlpGenSamples:
    sam.setStatConfig(useStat)
    sam.addSystematic(Systematic("Zpt50GeV",configMgr.weights,pT50GeVHighWeights,pT50GeVLowWeights,"weight","overallSys"))
    sam.addSystematic(Systematic("Zpt100GeV",configMgr.weights,pT100GeVHighWeights,pT100GeVLowWeights,"weight","overallSys"))
    sam.addSystematic(Systematic("Zpt150GeV",configMgr.weights,pT150GeVHighWeights,pT150GeVLowWeights,"weight","overallSys"))
    sam.addSystematic(Systematic("Zpt200GeV",configMgr.weights,pT200GeVHighWeights,pT200GeVLowWeights,"weight","overallSys"))

### Additional scale uncertainty on WZ Np0 and WZ Np1
wzSample_Np0.addSystematic(Systematic("err_WZ_Np0", configMgr.weights,1.06 ,0.96, "user","userOverallSys"))
wzSample_Np1.addSystematic(Systematic("err_WZ_Np1", configMgr.weights,1.06 ,0.83, "user","userOverallSys"))

### Additional uncertainty on the V+HF samples
if fullSyst:
    hf = Systematic("HF",configMgr.weights,hfHighWeights,hfLowWeights,"weight","histoSys")
    wzSample_Np0.addSystematic(hf)
    wzSample_Np1.addSystematic(hf)
    wzSample_Np2.addSystematic(hf)
    wzSample_Np3.addSystematic(hf)
    wzSample_Np4.addSystematic(hf)
    #wzSample.addSystematic(hf)

### Multiparton systematics
if fullSyst:
    wzMutiparton = Systematic("WZMutiparton",configMgr.weights,2.817,1-1.817,"user","userOverallSys")

bgSample = Sample("BG",kGreen)
bgSample.setStatConfig(useStat)
### Additional uncertainty on BG
bgSample.addSystematic(Systematic("err_BG", configMgr.weights,1.2 ,0.8, "user","userOverallSys"))

#List of bkg samples
bgdsamples=[bgSample]
for sam in AlpGenSamples:
    bgdsamples.append(sam)

#QCD and data samples
qcdSample = Sample("QCD",kGray+1)
qcdSample.setQCD(True,"histoSys")
qcdSample.setStatConfig(useStat)

dataSample = Sample("Data",kBlack)
dataSample.setData()

# nJet Binning for Top Control region
nJetTopeeBinLow = 2
nJetTopeeBinHigh = 10

nJetTopeBinLow = 3
nJetTopeBinHigh = 10

nJetTopseBinLow = 2
nJetTopseBinHigh = 8

nJetTopemBinLow = 2
nJetTopemBinHigh = 10

nJetTopmmBinLow = 2
nJetTopmmBinHigh = 10

nJetTopmBinLow = 3
nJetTopmBinHigh = 10

nJetTopsmBinLow = 2
nJetTopsmBinHigh = 8

# nJet Binning for W Control region
nJetZmmRegions = ["ZRmm"]
nJetZmmBinLow = 2
nJetZmmBinHigh = 10

nJetZmRegions = ["WRMu"]
nJetZm7jRegions = ["WRMu7J"]
nJetZmBinLow = 3
nJetZmBinHigh = 10

nJetZsmRegions = ["SVWMu"]
nJetZsmBinLow = 2
nJetZsmBinHigh = 8

nJetZeeRegions = ["ZRee"]
nJetZeeBinLow = 2
nJetZeeBinHigh = 10

nJetZeRegions = ["WREl"]
nJetZe7jRegions = ["WREl7J"]
nJetZeBinLow = 3
nJetZeBinHigh = 10

nJetZseRegions = ["SVWEl"]
nJetZseBinLow = 2
nJetZseBinHigh = 8

ZptZmmRegions = ["ZRmm"]
ZptZmmNBins = 40
ZptZmmBinLow = 0
ZptZmmBinHigh = 1000

ZptZeeRegions = ["ZRee"]
ZptZeeNBins = 40
ZptZeeBinLow = 0
ZptZeeBinHigh = 1000


#--------------------------------------------------------------
# Background-only fit 
#--------------------------------------------------------------
bkgOnly = configMgr.addTopLevelXML("bkgonly")
if not doSignalOnly:
    bkgOnly.addSamples([qcdSample])
    bkgOnly.addSamples(bgdsamples)
    bkgOnly.addSamples([dataSample])
if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.037)
meas.addPOI("mu_SIG")
# Fix Background 
meas.addParamSetting("mu_WZ_Np0","const",1.0)
meas.addParamSetting("mu_WZ_Np1","const",1.0)
meas.addParamSetting("mu_WZ_Np2","const",1.0)
meas.addParamSetting("mu_WZ_Np3","const",1.0)

#Add common systematics
for syst in basicChanSyst:
    bkgOnly.addSystematic(syst)

#b-tag classification of channels
bReqChans = []
bVetoChans = []
bAgnosticChans = []

#lepton flavor classification of channels
seChans = []
elChans = []
smChans = []
muChans = []
eeChans = []
mmChans = []
emChans = []

##### nJet for Top ####
if useDiLepCR:
    # ele ele
    nJetTopeeChannel=bkgOnly.addChannel("nJet",["TRee"],(nJetTopeeBinHigh-nJetTopeeBinLow),nJetTopeeBinLow,nJetTopeeBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopeeChannel)
    eeChans.append(nJetTopeeChannel)
    if fullSyst and not doSignalOnly:
        nJetTopeeChannel.getSample("Top_Np0").addSystematic(topPtMin30DLCR)
        nJetTopeeChannel.getSample("Top_Np1").addSystematic(topPtMin30DLCR)
        nJetTopeeChannel.getSample("Top_Np2").addSystematic(topPtMin30DLCR)
        nJetTopeeChannel.getSample("Top_Np3").addSystematic(topPtMin30DLCR)
        nJetTopeeChannel.getSample("Top_Np4").addSystematic(topPtMin30DLCR)
        nJetTopeeChannel.getSample("Top_Np5").addSystematic(topPtMin30DLCR)
        nJetTopeeChannel.getSample("WZ_Np0").addSystematic(wzPtMin30DLCR)
        nJetTopeeChannel.getSample("WZ_Np1").addSystematic(wzPtMin30DLCR)
        nJetTopeeChannel.getSample("WZ_Np2").addSystematic(wzPtMin30DLCR)
        nJetTopeeChannel.getSample("WZ_Np3").addSystematic(wzPtMin30DLCR)
        nJetTopeeChannel.getSample("WZ_Np4").addSystematic(wzPtMin30DLCR)
        nJetTopeeChannel.getSample("WZ_Np5").addSystematic(wzPtMin30DLCR)
    #  ele mu
    nJetTopemChannel=bkgOnly.addChannel("nJet",["TRem"],(nJetTopemBinHigh-nJetTopemBinLow),nJetTopemBinLow,nJetTopemBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopemChannel)
    emChans.append(nJetTopemChannel)
    if fullSyst and not doSignalOnly:
        nJetTopemChannel.getSample("Top_Np0").addSystematic(topPtMin30DLCR)
        nJetTopemChannel.getSample("Top_Np1").addSystematic(topPtMin30DLCR)
        nJetTopemChannel.getSample("Top_Np2").addSystematic(topPtMin30DLCR)
        nJetTopemChannel.getSample("Top_Np3").addSystematic(topPtMin30DLCR)
        nJetTopemChannel.getSample("Top_Np4").addSystematic(topPtMin30DLCR)
        nJetTopemChannel.getSample("Top_Np5").addSystematic(topPtMin30DLCR)
        nJetTopemChannel.getSample("WZ_Np0").addSystematic(wzPtMin30DLCR)
        nJetTopemChannel.getSample("WZ_Np1").addSystematic(wzPtMin30DLCR)
        nJetTopemChannel.getSample("WZ_Np2").addSystematic(wzPtMin30DLCR)
        nJetTopemChannel.getSample("WZ_Np3").addSystematic(wzPtMin30DLCR)
        nJetTopemChannel.getSample("WZ_Np4").addSystematic(wzPtMin30DLCR)
        nJetTopemChannel.getSample("WZ_Np5").addSystematic(wzPtMin30DLCR)
    # mu mu
    nJetTopmmChannel=bkgOnly.addChannel("nJet",["TRmm"],(nJetTopmmBinHigh-nJetTopmmBinLow),nJetTopmmBinLow,nJetTopmmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopmmChannel)
    mmChans.append(nJetTopmmChannel)
    if fullSyst and not doSignalOnly:
        nJetTopmmChannel.getSample("Top_Np0").addSystematic(topPtMin30DLCR)
        nJetTopmmChannel.getSample("Top_Np1").addSystematic(topPtMin30DLCR)
        nJetTopmmChannel.getSample("Top_Np2").addSystematic(topPtMin30DLCR)
        nJetTopmmChannel.getSample("Top_Np3").addSystematic(topPtMin30DLCR)
        nJetTopmmChannel.getSample("Top_Np4").addSystematic(topPtMin30DLCR)
        nJetTopmmChannel.getSample("Top_Np5").addSystematic(topPtMin30DLCR)
        nJetTopmmChannel.getSample("WZ_Np0").addSystematic(wzPtMin30DLCR)
        nJetTopmmChannel.getSample("WZ_Np1").addSystematic(wzPtMin30DLCR)
        nJetTopmmChannel.getSample("WZ_Np2").addSystematic(wzPtMin30DLCR)
        nJetTopmmChannel.getSample("WZ_Np3").addSystematic(wzPtMin30DLCR)
        nJetTopmmChannel.getSample("WZ_Np4").addSystematic(wzPtMin30DLCR)
        nJetTopmmChannel.getSample("WZ_Np5").addSystematic(wzPtMin30DLCR)    
        pass
    bReqChans += [nJetTopeeChannel,nJetTopemChannel,nJetTopmmChannel]

if useHardLepCR:
    #  single ele
    nJetTopeChannel=bkgOnly.addChannel("nJet",["TREl"],(nJetTopeBinHigh-nJetTopeBinLow),nJetTopeBinLow,nJetTopeBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopeChannel)
    elChans.append(nJetTopeChannel)
    if fullSyst and not doSignalOnly:
        nJetTopeChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        nJetTopeChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetTopeChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetTopeChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetTopeChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetTopeChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        nJetTopeChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetTopeChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetTopeChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetTopeChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetTopeChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetTopeChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
    # single mu
    nJetTopmChannel=bkgOnly.addChannel("nJet",["TRMu"],(nJetTopmBinHigh-nJetTopmBinLow),nJetTopmBinLow,nJetTopmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopmChannel)
    muChans.append(nJetTopmChannel)
    if fullSyst and not doSignalOnly:
        nJetTopmChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        nJetTopmChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetTopmChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetTopmChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetTopmChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetTopmChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        nJetTopmChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetTopmChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetTopmChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetTopmChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetTopmChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetTopmChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
        pass
    bReqChans += [nJetTopeChannel,nJetTopmChannel]

if useHardLepMultijetsCR:
    #  single ele
    nJetTope7jChannel=bkgOnly.addChannel("nJet",["TREl7J"],(nJetTopeBinHigh-nJetTopeBinLow),nJetTopeBinLow,nJetTopeBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTope7jChannel)
    elChans.append(nJetTope7jChannel)
    if fullSyst and not doSignalOnly:
        #nJetTope7jChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        #nJetTope7jChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetTope7jChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetTope7jChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetTope7jChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetTope7jChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        nJetTope7jChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetTope7jChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetTope7jChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetTope7jChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetTope7jChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetTope7jChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
        #nJetTope7jChannel.getSample("Top").addSystematic(topPtMin30HLCR)
        #nJetTope7jChannel.getSample("WZ").addSystematic(wzPtMin30HLCR)
    # single mu
    nJetTopm7jChannel=bkgOnly.addChannel("nJet",["TRMu7J"],(nJetTopmBinHigh-nJetTopmBinLow),nJetTopmBinLow,nJetTopmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopm7jChannel)
    muChans.append(nJetTopm7jChannel)
    if fullSyst and not doSignalOnly:
        #nJetTopm7jChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        #nJetTopm7jChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetTopm7jChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetTopm7jChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetTopm7jChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetTopm7jChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        nJetTopm7jChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetTopm7jChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetTopm7jChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetTopm7jChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetTopm7jChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetTopm7jChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
        #nJetTopm7jChannel.getSample("WZ").addSystematic(wzPtMin30HLCR)
        #nJetTopm7jChannel.getSample("Top").addSystematic(topPtMin30HLCR)
        pass
    bReqChans += [nJetTope7jChannel,nJetTopm7jChannel]

if useSoftLepCR:
    #  single soft ele
    nJetTopseChannel=bkgOnly.addChannel("nJet",["SVTEl"],(nJetTopseBinHigh-nJetTopseBinLow),nJetTopseBinLow,nJetTopseBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopseChannel)
    seChans.append(nJetTopseChannel)
    if fullSyst and not doSignalOnly:
        nJetTopseChannel.getSample("Top_Np0").addSystematic(topPtMin30SLCR)
        nJetTopseChannel.getSample("Top_Np1").addSystematic(topPtMin30SLCR)
        nJetTopseChannel.getSample("Top_Np2").addSystematic(topPtMin30SLCR)
        nJetTopseChannel.getSample("Top_Np3").addSystematic(topPtMin30SLCR)
        nJetTopseChannel.getSample("Top_Np4").addSystematic(topPtMin30SLCR)
        nJetTopseChannel.getSample("Top_Np5").addSystematic(topPtMin30SLCR)
        nJetTopseChannel.getSample("WZ_Np0").addSystematic(wzPtMin30SLCR)
        nJetTopseChannel.getSample("WZ_Np1").addSystematic(wzPtMin30SLCR)
        nJetTopseChannel.getSample("WZ_Np2").addSystematic(wzPtMin30SLCR)
        nJetTopseChannel.getSample("WZ_Np3").addSystematic(wzPtMin30SLCR)
        nJetTopseChannel.getSample("WZ_Np4").addSystematic(wzPtMin30SLCR)
        nJetTopseChannel.getSample("WZ_Np5").addSystematic(wzPtMin30SLCR)
    # soft single mu
    nJetTopsmChannel=bkgOnly.addChannel("nJet",["SVTMu"],(nJetTopsmBinHigh-nJetTopsmBinLow),nJetTopsmBinLow,nJetTopsmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetTopsmChannel)
    smChans.append(nJetTopsmChannel)
    if fullSyst and not doSignalOnly:
        nJetTopsmChannel.getSample("Top_Np0").addSystematic(topPtMin30SLCR)
        nJetTopsmChannel.getSample("Top_Np1").addSystematic(topPtMin30SLCR)
        nJetTopsmChannel.getSample("Top_Np2").addSystematic(topPtMin30SLCR)
        nJetTopsmChannel.getSample("Top_Np3").addSystematic(topPtMin30SLCR)
        nJetTopsmChannel.getSample("Top_Np4").addSystematic(topPtMin30SLCR)
        nJetTopsmChannel.getSample("Top_Np5").addSystematic(topPtMin30SLCR)
        nJetTopsmChannel.getSample("WZ_Np0").addSystematic(wzPtMin30SLCR)
        nJetTopsmChannel.getSample("WZ_Np1").addSystematic(wzPtMin30SLCR)
        nJetTopsmChannel.getSample("WZ_Np2").addSystematic(wzPtMin30SLCR)
        nJetTopsmChannel.getSample("WZ_Np3").addSystematic(wzPtMin30SLCR)
        nJetTopsmChannel.getSample("WZ_Np4").addSystematic(wzPtMin30SLCR)
        nJetTopsmChannel.getSample("WZ_Np5").addSystematic(wzPtMin30SLCR)
        pass
    bReqChans += [nJetTopseChannel,nJetTopsmChannel]

        
####### nJet for W/Z  #######
if useDiLepCR:
    # ele ele    
    nJetZeeChannel=bkgOnly.addChannel("nJet",nJetZeeRegions,(nJetZeeBinHigh-nJetZeeBinLow),nJetZeeBinLow,nJetZeeBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZeeChannel)
    eeChans.append(nJetZeeChannel)
    if fullSyst and not doSignalOnly:
        nJetZeeChannel.getSample("WZ_Np0").addSystematic(wzPtMin30DLCR)
        nJetZeeChannel.getSample("WZ_Np1").addSystematic(wzPtMin30DLCR)
        nJetZeeChannel.getSample("WZ_Np2").addSystematic(wzPtMin30DLCR)
        nJetZeeChannel.getSample("WZ_Np3").addSystematic(wzPtMin30DLCR)
        nJetZeeChannel.getSample("WZ_Np4").addSystematic(wzPtMin30DLCR)
        nJetZeeChannel.getSample("WZ_Np5").addSystematic(wzPtMin30DLCR)
        nJetZeeChannel.getSample("Top_Np0").addSystematic(topPtMin30DLCR)
        nJetZeeChannel.getSample("Top_Np1").addSystematic(topPtMin30DLCR)
        nJetZeeChannel.getSample("Top_Np2").addSystematic(topPtMin30DLCR)
        nJetZeeChannel.getSample("Top_Np3").addSystematic(topPtMin30DLCR)
        nJetZeeChannel.getSample("Top_Np4").addSystematic(topPtMin30DLCR)
        nJetZeeChannel.getSample("Top_Np5").addSystematic(topPtMin30DLCR)
    # mu mu
    nJetZmmChannel=bkgOnly.addChannel("nJet",nJetZmmRegions,(nJetZmmBinHigh-nJetZmmBinLow),nJetZmmBinLow,nJetZmmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZmmChannel)
    mmChans.append(nJetZmmChannel)
    if fullSyst and not doSignalOnly:
        nJetZmmChannel.getSample("WZ_Np0").addSystematic(wzPtMin30DLCR)
        nJetZmmChannel.getSample("WZ_Np1").addSystematic(wzPtMin30DLCR)
        nJetZmmChannel.getSample("WZ_Np2").addSystematic(wzPtMin30DLCR)
        nJetZmmChannel.getSample("WZ_Np3").addSystematic(wzPtMin30DLCR)
        nJetZmmChannel.getSample("WZ_Np4").addSystematic(wzPtMin30DLCR)
        nJetZmmChannel.getSample("WZ_Np5").addSystematic(wzPtMin30DLCR)
        nJetZmmChannel.getSample("Top_Np0").addSystematic(topPtMin30DLCR)
        nJetZmmChannel.getSample("Top_Np1").addSystematic(topPtMin30DLCR)
        nJetZmmChannel.getSample("Top_Np2").addSystematic(topPtMin30DLCR)
        nJetZmmChannel.getSample("Top_Np3").addSystematic(topPtMin30DLCR)
        nJetZmmChannel.getSample("Top_Np4").addSystematic(topPtMin30DLCR)
        nJetZmmChannel.getSample("Top_Np5").addSystematic(topPtMin30DLCR)
    bAgnosticChans += [nJetZeeChannel,nJetZmmChannel]

if useHardLepCR:
    # single ele
    nJetZeChannel=bkgOnly.addChannel("nJet",nJetZeRegions,(nJetZeBinHigh-nJetZeBinLow),nJetZeBinLow,nJetZeBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZeChannel)
    elChans.append(nJetZeChannel)
    if fullSyst and not doSignalOnly:
        nJetZeChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetZeChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetZeChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetZeChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetZeChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetZeChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
        nJetZeChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        nJetZeChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetZeChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetZeChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetZeChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetZeChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
    # single mu
    nJetZmChannel=bkgOnly.addChannel("nJet",nJetZmRegions,(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZmChannel)
    muChans.append(nJetZmChannel)
    if fullSyst and not doSignalOnly:
        nJetZmChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetZmChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetZmChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetZmChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetZmChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetZmChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
        nJetZmChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        nJetZmChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetZmChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetZmChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetZmChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetZmChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        pass
    bVetoChans += [nJetZmChannel,nJetZeChannel]

if useHardLepMultijetsCR:
    # single ele
    nJetZe7jChannel=bkgOnly.addChannel("nJet",nJetZe7jRegions,(nJetZeBinHigh-nJetZeBinLow),nJetZeBinLow,nJetZeBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZe7jChannel)
    elChans.append(nJetZe7jChannel)
    if fullSyst and not doSignalOnly:
        nJetZe7jChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR) 
        nJetZe7jChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR) 
        nJetZe7jChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR) 
        nJetZe7jChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR) 
        nJetZe7jChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR) 
        nJetZe7jChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR) 
        #nJetZe7jChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR) 
        #nJetZe7jChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR) 
        nJetZe7jChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR) 
        nJetZe7jChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR) 
        nJetZe7jChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR) 
        nJetZe7jChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        #nJetZe7jChannel.getSample("WZ").addSystematic(wzPtMin30HLCR)
        #nJetZe7jChannel.getSample("Top").addSystematic(topPtMin30HLCR)
    # single mu
    nJetZm7jChannel=bkgOnly.addChannel("nJet",nJetZm7jRegions,(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZm7jChannel)
    muChans.append(nJetZm7jChannel)
    if fullSyst and not doSignalOnly:
        nJetZm7jChannel.getSample("WZ_Np0").addSystematic(wzPtMin30HLCR)
        nJetZm7jChannel.getSample("WZ_Np1").addSystematic(wzPtMin30HLCR)
        nJetZm7jChannel.getSample("WZ_Np2").addSystematic(wzPtMin30HLCR)
        nJetZm7jChannel.getSample("WZ_Np3").addSystematic(wzPtMin30HLCR)
        nJetZm7jChannel.getSample("WZ_Np4").addSystematic(wzPtMin30HLCR)
        nJetZm7jChannel.getSample("WZ_Np5").addSystematic(wzPtMin30HLCR)
        #nJetZm7jChannel.getSample("Top_Np0").addSystematic(topPtMin30HLCR)
        #nJetZm7jChannel.getSample("Top_Np1").addSystematic(topPtMin30HLCR)
        nJetZm7jChannel.getSample("Top_Np2").addSystematic(topPtMin30HLCR)
        nJetZm7jChannel.getSample("Top_Np3").addSystematic(topPtMin30HLCR)
        nJetZm7jChannel.getSample("Top_Np4").addSystematic(topPtMin30HLCR)
        nJetZm7jChannel.getSample("Top_Np5").addSystematic(topPtMin30HLCR)
        #nJetZm7jChannel.getSample("WZ").addSystematic(wzPtMin30HLCR)
        #nJetZm7jChannel.getSample("Top").addSystematic(topPtMin30HLCR)
        pass
    bVetoChans += [nJetZm7jChannel,nJetZe7jChannel]

if useSoftLepCR:    
    # single soft mu
    nJetZsmChannel=bkgOnly.addChannel("nJet",nJetZsmRegions,(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZsmChannel)
    smChans.append(nJetZsmChannel)
    if fullSyst and not doSignalOnly:
        nJetZsmChannel.getSample("WZ_Np0").addSystematic(wzPtMin30SLCR)
        nJetZsmChannel.getSample("WZ_Np1").addSystematic(wzPtMin30SLCR)
        nJetZsmChannel.getSample("WZ_Np2").addSystematic(wzPtMin30SLCR)
        nJetZsmChannel.getSample("WZ_Np3").addSystematic(wzPtMin30SLCR)
        nJetZsmChannel.getSample("WZ_Np4").addSystematic(wzPtMin30SLCR)
        nJetZsmChannel.getSample("WZ_Np5").addSystematic(wzPtMin30SLCR)
        nJetZsmChannel.getSample("Top_Np0").addSystematic(topPtMin30SLCR)
        nJetZsmChannel.getSample("Top_Np1").addSystematic(topPtMin30SLCR)
        nJetZsmChannel.getSample("Top_Np2").addSystematic(topPtMin30SLCR)
        nJetZsmChannel.getSample("Top_Np3").addSystematic(topPtMin30SLCR)
        nJetZsmChannel.getSample("Top_Np4").addSystematic(topPtMin30SLCR)
        nJetZsmChannel.getSample("Top_Np5").addSystematic(topPtMin30SLCR)
    # single soft ele
    nJetZseChannel=bkgOnly.addChannel("nJet",nJetZseRegions,(nJetZseBinHigh-nJetZseBinLow),nJetZseBinLow,nJetZseBinHigh)
    bkgOnly.setBkgConstrainChannels(nJetZseChannel)
    seChans.append(nJetZseChannel)
    if fullSyst and not doSignalOnly:
        nJetZseChannel.getSample("WZ_Np0").addSystematic(wzPtMin30SLCR)
        nJetZseChannel.getSample("WZ_Np1").addSystematic(wzPtMin30SLCR)
        nJetZseChannel.getSample("WZ_Np2").addSystematic(wzPtMin30SLCR)
        nJetZseChannel.getSample("WZ_Np3").addSystematic(wzPtMin30SLCR)
        nJetZseChannel.getSample("WZ_Np4").addSystematic(wzPtMin30SLCR)
        nJetZseChannel.getSample("WZ_Np5").addSystematic(wzPtMin30SLCR)
        nJetZseChannel.getSample("Top_Np0").addSystematic(topPtMin30SLCR)
        nJetZseChannel.getSample("Top_Np1").addSystematic(topPtMin30SLCR)
        nJetZseChannel.getSample("Top_Np2").addSystematic(topPtMin30SLCR)
        nJetZseChannel.getSample("Top_Np3").addSystematic(topPtMin30SLCR)
        nJetZseChannel.getSample("Top_Np4").addSystematic(topPtMin30SLCR)
        nJetZseChannel.getSample("Top_Np5").addSystematic(topPtMin30SLCR)
        pass
    bVetoChans += [nJetZsmChannel,nJetZseChannel]


######################################################
# Bkg-only configuration is finished.                #
# Move on with validation config from bkgOnly clone. #
######################################################


#-------------------------------------------------
# Signal regions as validation regions 
#-------------------------------------------------

meffNBinsTR = 20
meffBinLowTR = 0.
meffBinHighTR = 2000.

metNBinsTR = 40
metBinLowTR = 0.
metBinHighTR = 800.

pt1NBinsTR = 40
pt1BinLowTR = 0.
pt1BinHighTR = 800.

pt2NBinsTR = 40
pt2BinLowTR = 0.
pt2BinHighTR = 800.

if doTableInputs or doValidationSRLoose or doValidationSRTight or doValidationSlope or doValidationDilep or doValidationDilepZ or doValidationSoftLep or doValidationMultijet:
    validation = configMgr.addTopLevelXMLClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans)
        appendIfMatchName(c,bVetoChans)
        appendIfMatchName(c,bAgnosticChans)
        appendIfMatchName(c,seChans)
        appendIfMatchName(c,elChans)
        appendIfMatchName(c,smChans)
        appendIfMatchName(c,muChans)
        appendIfMatchName(c,eeChans)
        appendIfMatchName(c,mmChans)
        appendIfMatchName(c,emChans)

if doValidationSlope or doTableInputs:
    #TR
    validationSlopeTRChannels=[]
    if doTableInputs:
        validationSlopeTRChannels.append( validation.addValidationChannel("meffInc",["TVL1El"],1,0,meffMax) )
        elChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("meffInc",["TVL1Mu"],1,0,meffMax) )
        muChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
    else:
        # check impact of kfactor fit on several distributions
        validationSlopeTRChannels.append( validation.addValidationChannel("meffInc",["TRElVR"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        elChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("meffInc",["TRMuVR"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        muChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("jet1Pt",["TRElVR"],pt1NBinsTR,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("jet1Pt",["TRMuVR"],pt1NBinsTR,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("jet2Pt",["TRElVR"],pt2NBinsTR,pt2BinLowTR,pt2BinHighTR) )
        elChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("jet2Pt",["TRMuVR"],pt2NBinsTR,pt2BinLowTR,pt2BinHighTR) )
        muChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])

        validationSlopeTRChannels.append( validation.addValidationChannel("meffInc",["TRElVR2"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        elChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("meffInc",["TRMuVR2"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        muChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("met",["TRElVR2"],metNBinsTR,metBinLowTR,metBinHighTR) )
        elChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        validationSlopeTRChannels.append( validation.addValidationChannel("met",["TRMuVR2"],metNBinsTR,metBinLowTR,metBinHighTR) )
        muChans.append(validationSlopeTRChannels[len(validationSlopeTRChannels)-1])
        pass
    bReqChans += validationSlopeTRChannels
    # WR
    validationSlopeWRChannels=[]
    if doTableInputs:
        validationSlopeWRChannels.append( validation.addValidationChannel("meffInc",["WVL1El"],1,0,meffMax) )
        elChans.append(validationSlopeWRChannels[len(validationSlopeWRChannels)-1])
        validationSlopeWRChannels.append( validation.addValidationChannel("meffInc",["WVL1Mu"],1,0,meffMax) )
        muChans.append(validationSlopeWRChannels[len(validationSlopeWRChannels)-1])
    else:
        validationSlopeWRChannels.append( validation.addValidationChannel("Wpt",["WRElVR"],metNBinsTR,metBinLowTR,metBinHighTR) )
        elChans.append(validationSlopeWRChannels[len(validationSlopeWRChannels)-1])
        validationSlopeWRChannels.append( validation.addValidationChannel("Wpt",["WRMuVR"],metNBinsTR,metBinLowTR,metBinHighTR) )
        muChans.append(validationSlopeWRChannels[len(validationSlopeWRChannels)-1])
        validationSlopeWRChannels.append( validation.addValidationChannel("met",["WRElVR"],metNBinsTR,metBinLowTR,metBinHighTR) )
        elChans.append(validationSlopeWRChannels[len(validationSlopeWRChannels)-1])
        validationSlopeWRChannels.append( validation.addValidationChannel("met",["WRMuVR"],metNBinsTR,metBinLowTR,metBinHighTR) )
        muChans.append(validationSlopeWRChannels[len(validationSlopeWRChannels)-1])
        pass
    bVetoChans+=validationSlopeWRChannels

    #ZR
    validationSlopeZRChannels = []
    if doTableInputs:
        validationSlopeZRChannels.append( validation.addValidationChannel("meffInc",["HMTVL1El"],1,0,meffMax) )
        elChans.append(validationSlopeZRChannels[len(validationSlopeZRChannels)-1])
        validationSlopeZRChannels.append( validation.addValidationChannel("meffInc",["HMTVL1Mu"],1,0,meffMax) )
        muChans.append(validationSlopeZRChannels[len(validationSlopeZRChannels)-1])
    else:
        validationSlopeZRChannels.append( validation.addValidationChannel("Zpt",["ZRee"],metNBinsTR,metBinLowTR,metBinHighTR) )
        eeChans.append(validationSlopeZRChannels[len(validationSlopeZRChannels)-1])
        validationSlopeZRChannels.append( validation.addValidationChannel("Zpt",["ZRmm"],metNBinsTR,metBinLowTR,metBinHighTR) )
        mmChans.append(validationSlopeZRChannels[len(validationSlopeZRChannels)-1])
        pass
    bAgnosticChans += validationSlopeZRChannels

if doValidationMultijet or doTableInputs:
    #TCR7j TREl7J
    validationSlopeTR7jChannels=[]
    if doTableInputs:
        validationSlopeTR7jChannels.append( validation.addValidationChannel("meffInc",["TREl7J"],1,0,meffMax) )
        elChans.append(validationSlopeTR7jChannels[len(validationSlopeTR7jChannels)-1])
        validationSlopeTR7jChannels.append( validation.addValidationChannel("meffInc",["TRMu7J"],1,0,meffMax) )
        muChans.append(validationSlopeTR7jChannels[len(validationSlopeTR7jChannels)-1])
    else:
        # check impact of kfactor fit on several distributions
        validationSlopeTR7jChannels.append( validation.addValidationChannel("meffInc",["TREl7J"],4,400,meffBinHighTR) )
        elChans.append(validationSlopeTR7jChannels[len(validationSlopeTR7jChannels)-1])
        validationSlopeTR7jChannels.append( validation.addValidationChannel("meffInc",["TRMu7J"],4,400,meffBinHighTR) )
        muChans.append(validationSlopeTR7jChannels[len(validationSlopeTR7jChannels)-1])
        validationSlopeTR7jChannels.append( validation.addValidationChannel("jet1Pt",["TREl7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeTR7jChannels[len(validationSlopeTR7jChannels)-1])
        validationSlopeTR7jChannels.append( validation.addValidationChannel("jet1Pt",["TRMu7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeTR7jChannels[len(validationSlopeTR7jChannels)-1])
        pass
    bReqChans += validationSlopeTR7jChannels

    #WCR7j WRMu7J
    validationSlopeWR7jChannels=[]
    if doTableInputs:
        validationSlopeWR7jChannels.append( validation.addValidationChannel("meffInc",["WREl7J"],1,0,meffMax) )
        elChans.append(validationSlopeWR7jChannels[len(validationSlopeWR7jChannels)-1])
        validationSlopeWR7jChannels.append( validation.addValidationChannel("meffInc",["WRMu7J"],1,0,meffMax) )
        muChans.append(validationSlopeWR7jChannels[len(validationSlopeWR7jChannels)-1])
    else:
        # check impact of kfactor fit on several distributions
        validationSlopeWR7jChannels.append( validation.addValidationChannel("meffInc",["WREl7J"],4,400,meffBinHighTR) )
        elChans.append(validationSlopeWR7jChannels[len(validationSlopeWR7jChannels)-1])
        validationSlopeWR7jChannels.append( validation.addValidationChannel("meffInc",["WRMu7J"],4,400,meffBinHighTR) )
        muChans.append(validationSlopeWR7jChannels[len(validationSlopeWR7jChannels)-1])
        validationSlopeWR7jChannels.append( validation.addValidationChannel("jet1Pt",["WREl7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeWR7jChannels[len(validationSlopeWR7jChannels)-1])
        validationSlopeWR7jChannels.append( validation.addValidationChannel("jet1Pt",["WRMu7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeWR7jChannels[len(validationSlopeWR7jChannels)-1])
        pass
    bReqChans += validationSlopeWR7jChannels

    #TCR TREl
    validationSlopeTRMJChannels=[]
    if doTableInputs:
        validationSlopeTRMJChannels.append( validation.addValidationChannel("meffInc",["TREl"],1,0,meffMax) )
        elChans.append(validationSlopeTRMJChannels[len(validationSlopeTRMJChannels)-1])
        validationSlopeTRMJChannels.append( validation.addValidationChannel("meffInc",["TRMu"],1,0,meffMax) )
        muChans.append(validationSlopeTRMJChannels[len(validationSlopeTRMJChannels)-1])
    else:
        # check impact of kfactor fit on several distributions
        validationSlopeTRMJChannels.append( validation.addValidationChannel("meffInc",["TREl"],4,400,meffBinHighTR) )
        elChans.append(validationSlopeTRMJChannels[len(validationSlopeTRMJChannels)-1])
        validationSlopeTRMJChannels.append( validation.addValidationChannel("meffInc",["TRMu"],4,400,meffBinHighTR) )
        muChans.append(validationSlopeTRMJChannels[len(validationSlopeTRMJChannels)-1])
        validationSlopeTRMJChannels.append( validation.addValidationChannel("jet1Pt",["TREl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeTRMJChannels[len(validationSlopeTRMJChannels)-1])
        validationSlopeTRMJChannels.append( validation.addValidationChannel("jet1Pt",["TRMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeTRMJChannels[len(validationSlopeTRMJChannels)-1])
        pass
    bReqChans += validationSlopeTRMJChannels

    #WCR WRMu
    validationSlopeWRMJChannels=[]
    if doTableInputs:
        validationSlopeWRMJChannels.append( validation.addValidationChannel("meffInc",["WREl"],1,0,meffMax) )
        elChans.append(validationSlopeWRMJChannels[len(validationSlopeWRMJChannels)-1])
        validationSlopeWRMJChannels.append( validation.addValidationChannel("meffInc",["WRMu"],1,0,meffMax) )
        muChans.append(validationSlopeWRMJChannels[len(validationSlopeWRMJChannels)-1])
    else:
        # check impact of kfactor fit on several distributions
        validationSlopeWRMJChannels.append( validation.addValidationChannel("meffInc",["WREl"],4,400,meffBinHighTR) )
        elChans.append(validationSlopeWRMJChannels[len(validationSlopeWRMJChannels)-1])
        validationSlopeWRMJChannels.append( validation.addValidationChannel("meffInc",["WRMu"],4,400,meffBinHighTR) )
        muChans.append(validationSlopeWRMJChannels[len(validationSlopeWRMJChannels)-1])
        validationSlopeWRMJChannels.append( validation.addValidationChannel("jet1Pt",["WREl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeWRMJChannels[len(validationSlopeWRMJChannels)-1])
        validationSlopeWRMJChannels.append( validation.addValidationChannel("jet1Pt",["WRMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeWRMJChannels[len(validationSlopeWRMJChannels)-1])
        pass
    bReqChans += validationSlopeWRMJChannels

	#TVL1
    validationSlopeTVLChannels=[]
    if doTableInputs:
        validationSlopeTVLChannels.append( validation.addValidationChannel("meffInc",["TVL1El7J"],1,0,meffMax) )
        elChans.append(validationSlopeTVLChannels[len(validationSlopeTVLChannels)-1])
        validationSlopeTVLChannels.append( validation.addValidationChannel("meffInc",["TVL1Mu7J"],1,0,meffMax) )
        muChans.append(validationSlopeTVLChannels[len(validationSlopeTVLChannels)-1])
    else:
        # check impact of kfactor fit on several distributions
        validationSlopeTVLChannels.append( validation.addValidationChannel("meffInc",["TVL1El7J"],4,400,meffBinHighTR) )
        elChans.append(validationSlopeTVLChannels[len(validationSlopeTVLChannels)-1])
        validationSlopeTVLChannels.append( validation.addValidationChannel("meffInc",["TVL1Mu7J"],4,400,meffBinHighTR) )
        muChans.append(validationSlopeTVLChannels[len(validationSlopeTVLChannels)-1])
        validationSlopeTVLChannels.append( validation.addValidationChannel("jet1Pt",["TVL1El7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeTVLChannels[len(validationSlopeTVLChannels)-1])
        validationSlopeTVLChannels.append( validation.addValidationChannel("jet1Pt",["TVL1Mu7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeTVLChannels[len(validationSlopeTVLChannels)-1])
        pass
    bReqChans += validationSlopeTVLChannels

    # WVL1El7J
    validationSlopeWVLChannels=[]
    if doTableInputs:
        validationSlopeWVLChannels.append( validation.addValidationChannel("meffInc",["WVL1El7J"],1,0,meffMax) )
        elChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("meffInc",["WVL1Mu7J"],1,0,meffMax) )
        muChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
    else:
        validationSlopeWVLChannels.append( validation.addValidationChannel("meffInc",["WVL1El7J"],4,400,meffBinHighTR) )
        elChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("meffInc",["WVL1Mu7J"],4,400,meffBinHighTR) )
        muChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("Wpt",["WVL1El7J"],metNBinsTR/2,100,400) )
        elChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("Wpt",["WVL1Mu7J"],metNBinsTR/2,100,400) )
        muChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("met",["WVL1El7J"],2,120,180) )
        elChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("met",["WVL1Mu7J"],2,120,180) )
        muChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("jet1Pt",["WVL1El7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        validationSlopeWVLChannels.append( validation.addValidationChannel("jet1Pt",["WVL1Mu7J"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeWVLChannels[len(validationSlopeWVLChannels)-1])
        pass
    bVetoChans+=validationSlopeWVLChannels

    # tmpVR
    validationSlopetmpVRChannels=[]
    if doTableInputs:
        validationSlopetmpVRChannels.append( validation.addValidationChannel("meffInc",["tmpVREl"],1,0,meffMax) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("meffInc",["tmpVRMu"],1,0,meffMax) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
    else:
        validationSlopetmpVRChannels.append( validation.addValidationChannel("meffInc",["tmpVREl"],4,meffBinLowTR,meffBinHighTR) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("meffInc",["tmpVRMu"],4,meffBinLowTR,meffBinHighTR) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("jet1Pt",["tmpVREl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("jet1Pt",["tmpVRMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("jet7Pt",["tmpVREl"],5,pt2BinLowTR,200) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("jet7Pt",["tmpVRMu"],5,pt2BinLowTR,200) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("met",["tmpVREl"],metNBinsTR/2,metBinLowTR,metBinHighTR) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("met",["tmpVRMu"],metNBinsTR/2,metBinLowTR,metBinHighTR) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("mt",["tmpVREl"],metNBinsTR/2,metBinLowTR,metBinHighTR) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("mt",["tmpVRMu"],metNBinsTR/2,metBinLowTR,metBinHighTR) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("lep1Pt",["tmpVREl"],10,0,500) )
        elChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        validationSlopetmpVRChannels.append( validation.addValidationChannel("lep1Pt",["tmpVRMu"],10,0,500) )
        muChans.append(validationSlopetmpVRChannels[len(validationSlopetmpVRChannels)-1])
        pass
    bAgnosticChans+=validationSlopetmpVRChannels

    #SR7jTSL
    validationSlopeSRSLChannels=[]
    if doTableInputs:
        validationSlopeSRSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTSLEl"],1,0,meffMax) )
        seChans.append(validationSlopeSRSLChannels[len(validationSlopeSRSLChannels)-1])
        validationSlopeSRSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTSLMu"],1,0,meffMax) )
        smChans.append(validationSlopeSRSLChannels[len(validationSlopeSRSLChannels)-1])
    else:
        validationSlopeSRSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTSLEl"],4,750,meffBinHighTR) )
        seChans.append(validationSlopeSRSLChannels[len(validationSlopeSRSLChannels)-1])
        validationSlopeSRSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTSLMu"],4,750,meffBinHighTR) )
        smChans.append(validationSlopeSRSLChannels[len(validationSlopeSRSLChannels)-1])
        validationSlopeSRSLChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTSLEl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeSRSLChannels[len(validationSlopeSRSLChannels)-1])
        validationSlopeSRSLChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTSLMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeSRSLChannels[len(validationSlopeSRSLChannels)-1])
        pass
    bAgnosticChans+=validationSlopeSRSLChannels

     #SR7jTmtSL
    validationSlopeSRmtSLChannels=[]
    if doTableInputs:
        validationSlopeSRmtSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtSLEl"],1,0,meffMax) )
        seChans.append(validationSlopeSRmtSLChannels[len(validationSlopeSRmtSLChannels)-1])
        validationSlopeSRmtSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtSLMu"],1,0,meffMax) )
        smChans.append(validationSlopeSRmtSLChannels[len(validationSlopeSRmtSLChannels)-1])
    else:
        validationSlopeSRmtSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtSLEl"],4,750,meffBinHighTR) )
        seChans.append(validationSlopeSRmtSLChannels[len(validationSlopeSRmtSLChannels)-1])
        validationSlopeSRmtSLChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtSLMu"],4,750,meffBinHighTR) )
        smChans.append(validationSlopeSRmtSLChannels[len(validationSlopeSRmtSLChannels)-1])
        validationSlopeSRmtSLChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTmtSLEl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeSRmtSLChannels[len(validationSlopeSRmtSLChannels)-1])
        validationSlopeSRmtSLChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTmtSLMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeSRmtSLChannels[len(validationSlopeSRmtSLChannels)-1])
        pass
    bAgnosticChans+=validationSlopeSRmtSLChannels

    # SR7j
    validationSlopeSRChannels=[]
    if doTableInputs:
        validationSlopeSRChannels.append( validation.addValidationChannel("meffInc",["SR7jEl"],1,0,meffMax) )
        elChans.append(validationSlopeSRChannels[len(validationSlopeSRChannels)-1])
        validationSlopeSRChannels.append( validation.addValidationChannel("meffInc",["SR7jMu"],1,0,meffMax) )
        muChans.append(validationSlopeSRChannels[len(validationSlopeSRChannels)-1])
    else:
        validationSlopeSRChannels.append( validation.addValidationChannel("meffInc",["SR7jEl"],4,0,meffBinHighTR) )
        elChans.append(validationSlopeSRChannels[len(validationSlopeSRChannels)-1])
        validationSlopeSRChannels.append( validation.addValidationChannel("meffInc",["SR7jMu"],4,0,meffBinHighTR) )
        muChans.append(validationSlopeSRChannels[len(validationSlopeSRChannels)-1])
        validationSlopeSRChannels.append( validation.addValidationChannel("jet1Pt",["SR7jEl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeSRChannels[len(validationSlopeSRChannels)-1])
        validationSlopeSRChannels.append( validation.addValidationChannel("jet1Pt",["SR7jMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeSRChannels[len(validationSlopeSRChannels)-1])
        pass
    bAgnosticChans+=validationSlopeSRChannels

    # SR7jTmet
    validationSlopeSRmetChannels=[]
    if doTableInputs:
        validationSlopeSRmetChannels.append( validation.addValidationChannel("meffInc",["SR7jTmetEl"],1,0,meffMax) )
        elChans.append(validationSlopeSRmetChannels[len(validationSlopeSRmetChannels)-1])
        validationSlopeSRmetChannels.append( validation.addValidationChannel("meffInc",["SR7jTmetMu"],1,0,meffMax) )
        muChans.append(validationSlopeSRmetChannels[len(validationSlopeSRmetChannels)-1])
    else:
        validationSlopeSRmetChannels.append( validation.addValidationChannel("meffInc",["SR7jTmetEl"],4,750,meffBinHighTR) )
        elChans.append(validationSlopeSRmetChannels[len(validationSlopeSRmetChannels)-1])
        validationSlopeSRmetChannels.append( validation.addValidationChannel("meffInc",["SR7jTmetMu"],4,750,meffBinHighTR) )
        muChans.append(validationSlopeSRmetChannels[len(validationSlopeSRmetChannels)-1])
        validationSlopeSRmetChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTmetEl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeSRmetChannels[len(validationSlopeSRmetChannels)-1])
        validationSlopeSRmetChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTmetMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeSRmetChannels[len(validationSlopeSRmetChannels)-1])
        pass
    bAgnosticChans+=validationSlopeSRmetChannels

    # SR7jTmt
    validationSlopeSRmtChannels=[]
    if doTableInputs:
        validationSlopeSRmtChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtEl"],1,0,meffMax) )
        elChans.append(validationSlopeSRmtChannels[len(validationSlopeSRmtChannels)-1])
        validationSlopeSRmtChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtMu"],1,0,meffMax) )
        muChans.append(validationSlopeSRmtChannels[len(validationSlopeSRmtChannels)-1])
    else:
        validationSlopeSRmtChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtEl"],4,750,meffBinHighTR) )
        elChans.append(validationSlopeSRmtChannels[len(validationSlopeSRmtChannels)-1])
        validationSlopeSRmtChannels.append( validation.addValidationChannel("meffInc",["SR7jTmtMu"],4,750,meffBinHighTR) )
        muChans.append(validationSlopeSRmtChannels[len(validationSlopeSRmtChannels)-1])
        validationSlopeSRmtChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTmtEl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        elChans.append(validationSlopeSRmtChannels[len(validationSlopeSRmtChannels)-1])
        validationSlopeSRmtChannels.append( validation.addValidationChannel("jet1Pt",["SR7jTmtMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR) )
        muChans.append(validationSlopeSRmtChannels[len(validationSlopeSRmtChannels)-1])
        pass
    bAgnosticChans+=validationSlopeSRmtChannels

    # binned SCRs and SDDs
    validationSlopeSCRChannels=[]
    validationSlopeSDDChannels=[]
    if doTableInputs:
        for ir in range(3,8):
            validationSlopeSCRChannels.append( validation.addValidationChannel("meffInc",["SR7jTSCRCombBin%dEl" % ir],1,0,meffMax) )
            elChans.append(validationSlopeSCRChannels[len(validationSlopeSCRChannels)-1])
            validationSlopeSCRChannels.append( validation.addValidationChannel("meffInc",["SR7jTSCRCombBin%dMu" % ir],1,0,meffMax) )
            muChans.append(validationSlopeSCRChannels[len(validationSlopeSCRChannels)-1])
            validationSlopeSDDChannels.append( validation.addValidationChannel("meffInc",["SR7jTSDDCombBin%dEl" % ir],1,0,meffMax) )
            elChans.append(validationSlopeSDDChannels[len(validationSlopeSDDChannels)-1])
            validationSlopeSDDChannels.append( validation.addValidationChannel("meffInc",["SR7jTSDDCombBin%dMu" % ir],1,0,meffMax) )
            muChans.append(validationSlopeSDDChannels[len(validationSlopeSDDChannels)-1])
    else:
        for ir in range(3,8):
            validationSlopeSCRChannels.append( validation.addValidationChannel("meffInc",["SR7jTSCRCombBin%dEl" % ir],1,0,meffMax) )
            elChans.append(validationSlopeSCRChannels[len(validationSlopeSCRChannels)-1])
            validationSlopeSCRChannels.append( validation.addValidationChannel("meffInc",["SR7jTSCRCombBin%dMu" % ir],1,0,meffMax) )
            muChans.append(validationSlopeSCRChannels[len(validationSlopeSCRChannels)-1])
            validationSlopeSDDChannels.append( validation.addValidationChannel("meffInc",["SR7jTSDDCombBin%dEl" % ir],1,0,meffMax) )
            elChans.append(validationSlopeSDDChannels[len(validationSlopeSDDChannels)-1])
            validationSlopeSDDChannels.append( validation.addValidationChannel("meffInc",["SR7jTSDDCombBin%dMu" % ir],1,0,meffMax) )
            muChans.append(validationSlopeSDDChannels[len(validationSlopeSDDChannels)-1])
    bVetoChans+=validationSlopeSCRChannels
    bAgnosticChans+=validationSlopeSDDChannels

    # SR7jT
    validationSlopeSR7TChannels=[]
    if doTableInputs:
        meff_SR7TEl = validation.addValidationChannel("meffInc",["SR7jTEl"],1,0,meffMax)
        if fullSyst:
            for sam in range(6):
                meff_SR7TEl.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
            #meff_SR7TEl.getSample("WZ").addSystematic(wzMutiparton)
        addHadronizationSyst(meff_SR7TEl,hadTop_SR7jT,hadWZ_SR7jT)
        validationSlopeSR7TChannels.append(meff_SR7TEl)
        elChans.append(meff_SR7TEl)

        meff_SR7TMu = validation.addValidationChannel("meffInc",["SR7jTMu"],1,0,meffMax)
        if fullSyst:
            for sam in range(6):
                meff_SR7TMu.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
            #meff_SR7TMu.getSample("WZ").addSystematic(wzMutiparton)
        addHadronizationSyst(meff_SR7TMu,hadTop_SR7jT,hadWZ_SR7jT)
        validationSlopeSR7TChannels.append(meff_SR7TMu)
        muChans.append(meff_SR7TMu)
    else:
        meff_SR7TEl = validation.addValidationChannel("meffInc",["SR7jTEl"],4,750,meffBinHigh1lS3)
        jet1Pt_SR7TEl = validation.addValidationChannel("jet1Pt",["SR7jTEl"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR)
        nJet_SR7TEl = validation.addValidationChannel("nJet",["SR7jTEl"],(nJetZeBinHigh-nJetZeBinLow),nJetZeBinLow,nJetZeBinHigh)
        if fullSyst:
            for sam in range(6):
                meff_SR7TEl.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
                jet1Pt_SR7TEl.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
                nJet_SR7TEl.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
            #meff_SR7TEl.getSample("WZ").addSystematic(wzMutiparton)
            #jet1Pt_SR7TEl.getSample("WZ").addSystematic(wzMutiparton)
            #nJet_SR7TEl.getSample("WZ").addSystematic(wzMutiparton)
        addHadronizationSyst(meff_SR7TEl,hadTop_SR7jT,hadWZ_SR7jT)
        addHadronizationSyst(jet1Pt_SR7TEl,hadTop_SR7jT,hadWZ_SR7jT)
        addHadronizationSyst(nJet_SR7TEl,hadTop_SR7jT,hadWZ_SR7jT)
        validationSlopeSR7TChannels.append(meff_SR7TEl)
        validationSlopeSR7TChannels.append(jet1Pt_SR7TEl)
        validationSlopeSR7TChannels.append(nJet_SR7TEl)
        elChans.append(meff_SR7TEl)
        elChans.append(jet1Pt_SR7TEl)
        elChans.append(nJet_SR7TEl)

        meff_SR7TMu = validation.addValidationChannel("meffInc",["SR7jTMu"],4,750,meffBinHigh1lS3)
        jet1Pt_SR7TMu = validation.addValidationChannel("jet1Pt",["SR7jTMu"],pt1NBinsTR/2,pt1BinLowTR,pt1BinHighTR)
        nJet_SR7TMu = validation.addValidationChannel("nJet",["SR7jTMu"],(nJetZeBinHigh-nJetZeBinLow),nJetZeBinLow,nJetZeBinHigh)
        if fullSyst:
            for sam in range(6):
                meff_SR7TMu.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
                jet1Pt_SR7TMu.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
                nJet_SR7TMu.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
            #meff_SR7TMu.getSample("WZ").addSystematic(wzMutiparton)
            #jet1Pt_SR7TMu.getSample("WZ").addSystematic(wzMutiparton)
            #nJet_SR7TMu.getSample("WZ").addSystematic(wzMutiparton)
        addHadronizationSyst(meff_SR7TMu,hadTop_SR7jT,hadWZ_SR7jT)
        addHadronizationSyst(jet1Pt_SR7TMu,hadTop_SR7jT,hadWZ_SR7jT)
        addHadronizationSyst(nJet_SR7TMu,hadTop_SR7jT,hadWZ_SR7jT)
        validationSlopeSR7TChannels.append(meff_SR7TMu)
        validationSlopeSR7TChannels.append(jet1Pt_SR7TMu)
        validationSlopeSR7TChannels.append(nJet_SR7TMu)
        muChans.append(meff_SR7TMu)
        muChans.append(jet1Pt_SR7TMu)
        muChans.append(nJet_SR7TMu)
        pass
    bAgnosticChans+=validationSlopeSR7TChannels

##############################################

if doValidationSRLoose:
    looseSRs = []
    #DILEPTONS
    meff2ee = validation.addValidationChannel("meffInc",["S2ee"],meffNBinsS2,meffBinLowS2,meffBinHighS2)
    meff2ee.setFileList(bgdFiles_ee)
    looseSRs.append(meff2ee)
    meff4ee = validation.addValidationChannel("meffInc",["S4ee"],meffNBinsS4,meffBinLowS4,meffBinHighS4)
    meff4ee.setFileList(bgdFiles_ee)
    looseSRs.append(meff4ee)
    meff2em = validation.addValidationChannel("meffInc",["S2em"],meffNBinsS2,meffBinLowS2,meffBinHighS2)
    meff2em.setFileList(bgdFiles_em)
    looseSRs.append(meff2em)
    meff4em = validation.addValidationChannel("meffInc",["S4em"],meffNBinsS4,meffBinLowS4,meffBinHighS4)
    meff4em.setFileList(bgdFiles_em)
    looseSRs.append(meff4em)
    meff2mm = validation.addValidationChannel("meffInc",["S2mm"],meffNBinsS2,meffBinLowS2,meffBinHighS2)
    meff2mm.setFileList(bgdFiles_mm)
    looseSRs.append(meff2mm)
    meff4mm = validation.addValidationChannel("meffInc",["S4mm"],meffNBinsS4,meffBinLowS4,meffBinHighS4)
    meff4mm.setFileList(bgdFiles_mm)
    looseSRs.append(meff4mm)
    # HARD LEPTON SRS
    meffS3_El=validation.addValidationChannel("meffInc",["S3El"],meffNBinsHL,meffBinLowHL,meffBinHighHL)
    meffS3_El.setFileList(bgdFiles_e)
    looseSRs.append(meffS3_El)
    meffS3_Mu=validation.addValidationChannel("meffInc",["S3Mu"],meffNBinsHL,meffBinLowHL,meffBinHighHL)
    meffS3_Mu.setFileList(bgdFiles_m)
    looseSRs.append(meffS3_Mu)
    meffS4_El=validation.addValidationChannel("meffInc",["S4El"],meffNBinsHL,meffBinLowHL,meffBinHighHL)
    meffS4_El.setFileList(bgdFiles_e)
    looseSRs.append(meffS4_El)
    meffS4_Mu=validation.addValidationChannel("meffInc",["S4Mu"],meffNBinsHL,meffBinLowHL,meffBinHighHL)
    meffS4_Mu.setFileList(bgdFiles_m)
    looseSRs.append(meffS4_Mu)
    # SOFT LEPTON SRS
    mmSSEl = validation.addValidationChannel("met/meff2Jet",["SSEl"],6,0.1,0.7)
    mmSSEl.setFileList(bgdFiles_se)
    looseSRs.append(mmSSEl)
    mmSSMu = validation.addValidationChannel("met/meff2Jet",["SSMu"],6,0.1,0.7)
    mmSSMu.setFileList(bgdFiles_sm)
    looseSRs.append(mmSSMu)
    
    for chan in looseSRs:
        chan.useOverflowBin=True
        pass
    bAgnosticChans += looseSRs


if doValidationSRTight:
    tightSRs = []
    #DILEPTONS
    meff2ee = validation.addValidationChannel("meffInc",["S2eeT"],1,meffBinLowS2,meffBinHighS2)
    meff2ee.setFileList(bgdFiles_ee)
    addHadronizationSyst(meff2ee,hadTop_SRS2,hadWZ_SRS2)
    tightSRs.append(meff2ee)
    eeChans.append(meff2ee)

    meff4ee = validation.addValidationChannel("meffInc",["S4eeT"],1,meffBinLowS4,meffBinHighS4)
    meff4ee.setFileList(bgdFiles_ee)
    addHadronizationSyst(meff4ee,hadTop_SRS4,hadWZ_SRS4)
    tightSRs.append(meff4ee)
    eeChans.append(meff4ee)

    meff2em = validation.addValidationChannel("meffInc",["S2emT"],1,meffBinLowS2,meffBinHighS2)
    meff2em.setFileList(bgdFiles_em)
    addHadronizationSyst(meff2em,hadTop_SRS2,hadWZ_SRS2)
    tightSRs.append(meff2em)
    emChans.append(meff2em)

    meff4em = validation.addValidationChannel("meffInc",["S4emT"],1,meffBinLowS4,meffBinHighS4)
    meff4em.setFileList(bgdFiles_em)
    addHadronizationSyst(meff4em,hadTop_SRS4,hadWZ_SRS4)
    tightSRs.append(meff4em)
    emChans.append(meff4em)

    meff2mm = validation.addValidationChannel("meffInc",["S2mmT"],1,meffBinLowS2,meffBinHighS2)
    meff2mm.setFileList(bgdFiles_mm)
    addHadronizationSyst(meff2mm,hadTop_SRS2,hadWZ_SRS2)
    tightSRs.append(meff2mm)
    mmChans.append(meff2mm)

    meff4mm = validation.addValidationChannel("meffInc",["S4mmT"],1,meffBinLowS4,meffBinHighS4)
    meff4mm.setFileList(bgdFiles_mm)
    addHadronizationSyst(meff4mm,hadTop_SRS4,hadWZ_SRS4)
    tightSRs.append(meff4mm)
    mmChans.append(meff4mm)

    # HARD LEPTON SRS
    meffS3T_El=validation.addValidationChannel("meffInc",["SR3jTEl"],1,1200,meffBinHighHL)
    meffS3T_El.setFileList(bgdFiles_e)
    addHadronizationSyst(meffS3T_El,hadTop_SR3jT,hadWZ_SR3jT)
    tightSRs.append(meffS3T_El)
    elChans.append(meffS3T_El)

    meffS3T_Mu=validation.addValidationChannel("meffInc",["SR3jTMu"],1,1200,meffBinHighHL)
    meffS3T_Mu.setFileList(bgdFiles_m)
    addHadronizationSyst(meffS3T_Mu,hadTop_SR3jT,hadWZ_SR3jT)
    tightSRs.append(meffS3T_Mu)
    muChans.append(meffS3T_Mu)

    meffS4T_El=validation.addValidationChannel("meffInc",["SR4jTEl"],1,800,meffBinHighHL)
    meffS4T_El.setFileList(bgdFiles_e)
    addHadronizationSyst(meffS4T_El,hadTop_SR4jT,hadWZ_SR4jT)
    tightSRs.append(meffS4T_El)
    elChans.append(meffS4T_El)

    meffS4T_Mu=validation.addValidationChannel("meffInc",["SR4jTMu"],1,800,meffBinHighHL)
    meffS4T_Mu.setFileList(bgdFiles_m)
    addHadronizationSyst(meffS4T_Mu,hadTop_SR4jT,hadWZ_SR4jT)
    tightSRs.append(meffS4T_Mu)
    muChans.append(meffS4T_Mu)

    # MULTIJETS SRS
    #meffS7T_El=validation.addValidationChannel("meffInc",["SR7jTEl"],1,750,meffBinHighHL)
    #meffS7T_El.setFileList(bgdFiles_e)
    #addHadronizationSyst(meffS7T_El,hadTop_SR7jT,hadWZ_SR7jT)
    #tightSRs.append(meffS7T_El)
    #elChans.append(meffS7T_El)

    #meffS7T_Mu=validation.addValidationChannel("meffInc",["SR7jTMu"],1,750,meffBinHighHL)
    #meffS7T_Mu.setFileList(bgdFiles_m)
    #addHadronizationSyst(meffS7T_Mu,hadTop_SR7jT,hadWZ_SR7jT)
    #tightSRs.append(meffS7T_Mu)
    #muChans.append(meffS7T_Mu)

    # SOFT LEPTON SRS
    mmSSElT = validation.addValidationChannel("met/meff2Jet",["SSElT"],1,0.3,0.7)
    mmSSElT.setFileList(bgdFiles_se)
    addHadronizationSyst(mmSSElT,hadTop_SRSL,hadWZ_SRSL)
    tightSRs.append(mmSSElT)
    seChans.append(mmSSElT)

    mmSSMuT = validation.addValidationChannel("met/meff2Jet",["SSMuT"],1,0.3,0.7)
    mmSSMuT.setFileList(bgdFiles_sm)
    addHadronizationSyst(mmSSMuT,hadTop_SRSL,hadWZ_SRSL)
    tightSRs.append(mmSSMuT)
    smChans.append(mmSSMuT)

    for chan in tightSRs:
        chan.useOverflowBin=True
        pass
    bAgnosticChans += tightSRs


if doValidationDilep:
    validation2LepChannels = []
    if doTableInputs:
        validation2LepChannels.append( validation.addValidationChannel("meffInc",["TVJ2ee"],1,0,meffMax) )
        eeChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("meffInc",["TVJ2em"],1,0,meffMax) )
        emChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("meffInc",["TVJ2mm"],1,0,meffMax) )
        mmChans.append(validation2LepChannels[len(validation2LepChannels)-1])
    else:
        validation2LepChannels.append( validation.addValidationChannel("meffInc",["TVJ2ee"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        eeChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("meffInc",["TVJ2em"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        emChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("meffInc",["TVJ2mm"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        mmChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("nJet",["TVJ2ee"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        eeChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("nJet",["TVJ2em"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        emChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        validation2LepChannels.append( validation.addValidationChannel("nJet",["TVJ2mm"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        mmChans.append(validation2LepChannels[len(validation2LepChannels)-1])
        pass
    bAgnosticChans += validation2LepChannels

    validation2LepTopChannels = []
    if doTableInputs:
        validation2LepTopChannels.append( validation.addValidationChannel("meffInc",["TVJ4ee"],1,0,meffMax) )
        eeChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("meffInc",["TVJ4em"],1,0,meffMax) )
        emChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("meffInc",["TVJ4mm"],1,0,meffMax) )
        mmChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
    else:
        validation2LepTopChannels.append( validation.addValidationChannel("meffInc",["TVJ4ee"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        eeChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("meffInc",["TVJ4em"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        emChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("meffInc",["TVJ4mm"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        mmChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("nJet",["TVJ4ee"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        eeChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("nJet",["TVJ4em"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        emChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        validation2LepTopChannels.append( validation.addValidationChannel("nJet",["TVJ4mm"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        mmChans.append(validation2LepTopChannels[len(validation2LepTopChannels)-1])
        pass
    bReqChans += validation2LepTopChannels

    
if doValidationDilepZ:
    validation2LepZChannels=[]
    if doTableInputs:
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ2ee"],1,0,meffMax) )
        eeChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ2em"],1,0,meffMax) )
        emChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ2mm"],1,0,meffMax) )
        mmChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ4ee"],1,0,meffMax) )
        eeChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ4em"],1,0,meffMax) )
        emChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ4mm"],1,0,meffMax) )
        mmChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
    else:
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ2ee"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        eeChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ2em"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        emChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ2mm"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        mmChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("nJet",["ZVJ2ee"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        eeChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("nJet",["ZVJ2em"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        emChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("nJet",["ZVJ2mm"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        mmChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ4ee"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        eeChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ4em"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        emChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("meffInc",["ZVJ4mm"],meffNBinsTR,meffBinLowTR,meffBinHighTR) )
        mmChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("nJet",["ZVJ4ee"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        eeChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("nJet",["ZVJ4em"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        emChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        validation2LepZChannels.append( validation.addValidationChannel("nJet",["ZVJ4mm"],(nJetZmBinHigh-nJetZmBinLow),nJetZmBinLow,nJetZmBinHigh) )
        mmChans.append(validation2LepZChannels[len(validation2LepZChannels)-1])
        pass
    bVetoChans += validation2LepZChannels

if doValidationSoftLep:
    validationSoftLepChannels = []
    validationSoftLepBtagChannels = []
    validationSoftLepBvetoChannels = []

    if doTableInputs:
        validationSoftLepChannels.append( validation.addValidationChannel("nJet",["SVEl"],1,nJetZsmBinLow,nJetZsmBinHigh) )
        seChans.append(validationSoftLepChannels[len(validationSoftLepChannels)-1])
        validationSoftLepChannels.append( validation.addValidationChannel("nJet",["SVMu"],1,nJetZsmBinLow,nJetZsmBinHigh) )
        smChans.append(validationSoftLepChannels[len(validationSoftLepChannels)-1])
    else:
        validationSoftLepChannels.append( validation.addValidationChannel("nJet",["SVEl"],(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh) )
        seChans.append(validationSoftLepChannels[len(validationSoftLepChannels)-1])
        validationSoftLepChannels.append( validation.addValidationChannel("nJet",["SVMu"],(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh) )
        smChans.append(validationSoftLepChannels[len(validationSoftLepChannels)-1])
        pass

    if not useSoftLepCR:
        if doTableInputs:
            validationSoftLepBvetoChannels.append( validation.addValidationChannel("nJet",["SVWEl"],1,nJetZsmBinLow,nJetZsmBinHigh) )
            seChans.append(validationSoftLepBvetoChannels[len(validationSoftLepBvetoChannels)-1])
            validationSoftLepBvetoChannels.append( validation.addValidationChannel("nJet",["SVWMu"],1,nJetZsmBinLow,nJetZsmBinHigh) )
            smChans.append(validationSoftLepBvetoChannels[len(validationSoftLepBvetoChannels)-1])
            validationSoftLepBtagChannels.append( validation.addValidationChannel("nJet",["SVTEl"],1,nJetZsmBinLow,nJetZsmBinHigh) )
            seChans.append(validationSoftLepBtagChannels[len(validationSoftLepBtagChannels)-1])
            validationSoftLepBtagChannels.append( validation.addValidationChannel("nJet",["SVTMu"],1,nJetZsmBinLow,nJetZsmBinHigh) )
            smChans.append(validationSoftLepBtagChannels[len(validationSoftLepBtagChannels)-1])
        else:
            validationSoftLepBvetoChannels.append( validation.addValidationChannel("nJet",["SVWEl"],(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh) )
            seChans.append(validationSoftLepBvetoChannels[len(validationSoftLepBvetoChannels)-1])
            validationSoftLepBvetoChannels.append( validation.addValidationChannel("nJet",["SVWMu"],(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh) )
            smChans.append(validationSoftLepBvetoChannels[len(validationSoftLepBvetoChannels)-1])
            validationSoftLepBtagChannels.append( validation.addValidationChannel("nJet",["SVTEl"],(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh) )
            seChans.append(validationSoftLepBtagChannels[len(validationSoftLepBtagChannels)-1])
            validationSoftLepBtagChannels.append( validation.addValidationChannel("nJet",["SVTMu"],(nJetZsmBinHigh-nJetZsmBinLow),nJetZsmBinLow,nJetZsmBinHigh) )
            smChans.append(validationSoftLepBtagChannels[len(validationSoftLepBtagChannels)-1])
            pass

    bReqChans+=validationSoftLepBtagChannels
    bVetoChans+=validationSoftLepBvetoChannels
    bAgnosticChans+=validationSoftLepChannels

#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
doExclusion = (doExclusion_GMSB_combined or doExclusion_mSUGRA_dilepton_combined or doExclusion_GG_twostepCC_slepton or doExclusion_GG_onestepCC_x12 or doExclusion_GG_onestepCC_gridX or doExclusion_mSUGRA_multijets or doExclusion_GG_twosteps_multijets or doExclusion_SS_twosteps_multijets or doExclusion_Gtt_multijets)
if doExclusion:

    SRs=["S3El","S3Mu","S4El","S4Mu","S2ee","S2em","S2mm","S4ee","S4em","S4mm"]
    if doExclusion_GMSB_combined:
        SRs=["S2ee","S2em","S2mm","S4ee","S4em","S4mm"]
    elif doExclusion_mSUGRA_dilepton_combined:
        SRs=["S3El","S3Mu","S4El","S4Mu","S2ee","S2em","S2mm","S4ee","S4em","S4mm"]
        #SRs=["S2ee","S2em","S2mm","S4ee","S4em","S4mm"]
    elif doExclusion_GG_twostepCC_slepton:
        SRs=["S4ee","S4em","S4mm"]
    elif doExclusion_GG_onestepCC_x12:
        SRs=["S3El","S3Mu","S4El","S4Mu","SSEl","SSMu"] # only hard lepton so far
    elif doExclusion_GG_onestepCC_gridX:
        SRs=["S3El","S3Mu","S4El","S4Mu"] # only hard lepton so far
    elif doExclusion_mSUGRA_multijets or doExclusion_GG_twosteps_multijets or doExclusion_SS_twosteps_multijets or doExclusion_Gtt_multijets:
        SRs=["SR7jTEl","SR7jTMu"]
        pass

    for sig in sigSamples:
        myTopLvl = configMgr.addTopLevelXMLClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans)
            appendIfMatchName(c,bVetoChans)
            appendIfMatchName(c,bAgnosticChans)
            appendIfMatchName(c,seChans)
            appendIfMatchName(c,elChans)
            appendIfMatchName(c,smChans)
            appendIfMatchName(c,muChans)
            appendIfMatchName(c,eeChans)
            appendIfMatchName(c,mmChans)
            appendIfMatchName(c,emChans)

        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(jesSignal)
        if doExclusion_Gtt_multijets:
            sig_tmp = sig.split("_")
            mgl = sig_tmp[2]
            unc = extractCrossSectionErr(float(mgl))
            xsecSig = Systematic("SigXSec",configMgr.weights,1+unc,1/(1+unc),"user","userOverallSys")
        sigSample.addSystematic(xsecSig)
        if sig.startswith("SM"):
            from SystematicsUtils import getISRSyst
            isrSyst = getISRSyst(sig)
            sigSample.addSystematic(isrSyst)
            pass
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if doValidationSRLoose:
                #don't re-create already existing channel, but unset as Validation and set as Signal channel
                ch = myTopLvl.getChannel("meffInc",[sr])
                iPop=myTopLvl.validationChannels.index(sr+"_meffInc")
                myTopLvl.validationChannels.pop(iPop)
            else:            
                if sr=="S3El" or sr=="S3Mu":
                    ch = myTopLvl.addChannel("meffInc",[sr],meffNBins1lS3,meffBinLow1lS3,meffBinHigh1lS3)
                    addHadronizationSyst(ch,hadTop_SR3jT_hist,hadWZ_SR3jT_hist)
                elif sr=="S4El" or sr=="S4Mu":
                    ch = myTopLvl.addChannel("meffInc",[sr],meffNBins1lS4,meffBinLow1lS4,meffBinHigh1lS4)
                    addHadronizationSyst(ch,hadTop_SR4jT_hist,hadWZ_SR4jT_hist)
                elif sr=="S2ee" or sr=="S2em" or sr=="S2mm":
                    ch = myTopLvl.addChannel("meffInc",[sr],meffNBinsS2,meffBinLowS2,meffBinHighS2)
                    addHadronizationSyst(ch,hadTop_SRS2_hist,hadWZ_SRS2_hist)
                elif sr=="S4ee" or sr=="S4em" or sr=="S4mm":
                    ch = myTopLvl.addChannel("meffInc",[sr],meffNBinsS4,meffBinLowS4,meffBinHighS4)
                    addHadronizationSyst(ch,hadTop_SRS4_hist,hadWZ_SRS4_hist)
                elif sr=="SSEl" or sr=="SSMu":
                    ch = myTopLvl.addChannel("met/meff2Jet",[sr],6,0.1,0.7)
                    addHadronizationSyst(ch,hadTop_SRSL,hadWZ_SRSL)
                elif sr=="SR7jTEl" or sr=="SR7jTMu":
                    ch = myTopLvl.addChannel("meffInc",[sr],4,750,meffBinHigh1lS3)
                    addHadronizationSyst(ch,hadTop_SR7jT_hist,hadWZ_SR7jT_hist)
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)


                if sr=="S3El" or sr=="S4El":
                    elChans.append(ch)
                elif sr=="SSEl":
                    seChans.append(ch)
                elif sr=="S3Mu" or sr=="S4Mu":
                    muChans.append(ch)
                elif sr=="SSMu":
                    smChans.append(ch)
                elif sr=="S2ee" or sr=="S4ee":
                    eeChans.append(ch)
                elif sr=="S2em" or sr=="S4em":
                    emChans.append(ch)
                elif sr=="S2mm" or sr=="S4mm":
                    mmChans.append(ch)
                elif sr=="SR7jTEl":
                    elChans.append(ch)
                elif sr=="SR7jTMu":
                    muChans.append(ch)
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                pass
            
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.useOverflowBin=True
            bAgnosticChans.append(ch)
                
            ## Ptmin
            if fullSyst and not doSignalOnly:
                if (ch.name.find("S3El")>-1 or ch.name.find("S3Mu")>-1):
                    ch.getSample("Top_Np0").addSystematic(topPtMin30S3)
                    ch.getSample("Top_Np1").addSystematic(topPtMin30S3)
                    ch.getSample("Top_Np2").addSystematic(topPtMin30S3)
                    ch.getSample("Top_Np3").addSystematic(topPtMin30S3)
                    ch.getSample("Top_Np4").addSystematic(topPtMin30S3)
                    ch.getSample("Top_Np5").addSystematic(topPtMin30S3)
                    ch.getSample("WZ_Np0").addSystematic(wzPtMin30S3)
                    ch.getSample("WZ_Np1").addSystematic(wzPtMin30S3)
                    ch.getSample("WZ_Np2").addSystematic(wzPtMin30S3)
                    ch.getSample("WZ_Np3").addSystematic(wzPtMin30S3)
                    ch.getSample("WZ_Np4").addSystematic(wzPtMin30S3)
                    ch.getSample("WZ_Np5").addSystematic(wzPtMin30S3)
                elif (ch.name.find("SR7jTEl")>-1 or ch.name.find("SR7TMu")>-1):
                    for sam in range(6):
                        ch.getSample("WZ_Np%d" % sam).addSystematic(wzMutiparton)
                elif (ch.name.find("S4El")>-1 or ch.name.find("S4Mu")>-1):
                    ch.getSample("Top_Np0").addSystematic(topPtMin30S4)
                    ch.getSample("Top_Np1").addSystematic(topPtMin30S4)
                    ch.getSample("Top_Np2").addSystematic(topPtMin30S4)
                    ch.getSample("Top_Np3").addSystematic(topPtMin30S4)
                    ch.getSample("Top_Np4").addSystematic(topPtMin30S4)
                    ch.getSample("Top_Np5").addSystematic(topPtMin30S4)
                    ch.getSample("WZ_Np0").addSystematic(wzPtMin30S4)
                    ch.getSample("WZ_Np1").addSystematic(wzPtMin30S4)
                    ch.getSample("WZ_Np2").addSystematic(wzPtMin30S4)
                    ch.getSample("WZ_Np3").addSystematic(wzPtMin30S4)
                    ch.getSample("WZ_Np4").addSystematic(wzPtMin30S4)
                    ch.getSample("WZ_Np5").addSystematic(wzPtMin30S4)
                elif (ch.name.find("S2ee")>-1 or ch.name.find("S2mm")>-1 or ch.name.find("S2em")>-1):
                    ch.getSample("Top_Np0").addSystematic(topPtMin30DLS2)
                    ch.getSample("Top_Np1").addSystematic(topPtMin30DLS2)
                    ch.getSample("Top_Np2").addSystematic(topPtMin30DLS2)
                    ch.getSample("Top_Np3").addSystematic(topPtMin30DLS2)
                    ch.getSample("Top_Np4").addSystematic(topPtMin30DLS2)
                    ch.getSample("Top_Np5").addSystematic(topPtMin30DLS2)
                    ch.getSample("WZ_Np0").addSystematic(wzPtMin30DLS2)
                    ch.getSample("WZ_Np1").addSystematic(wzPtMin30DLS2)
                    ch.getSample("WZ_Np2").addSystematic(wzPtMin30DLS2)
                    ch.getSample("WZ_Np3").addSystematic(wzPtMin30DLS2)
                    ch.getSample("WZ_Np4").addSystematic(wzPtMin30DLS2)
                    ch.getSample("WZ_Np5").addSystematic(wzPtMin30DLS2)
                elif (ch.name.find("S4ee")>-1 or ch.name.find("S4mm")>-1 or ch.name.find("S4em")>-1):
                    ch.getSample("Top_Np0").addSystematic(topPtMin30DLS4)
                    ch.getSample("Top_Np1").addSystematic(topPtMin30DLS4)
                    ch.getSample("Top_Np2").addSystematic(topPtMin30DLS4)
                    ch.getSample("Top_Np3").addSystematic(topPtMin30DLS4)
                    ch.getSample("Top_Np4").addSystematic(topPtMin30DLS4)
                    ch.getSample("Top_Np5").addSystematic(topPtMin30DLS4)
                    ch.getSample("WZ_Np0").addSystematic(wzPtMin30DLS4)
                    ch.getSample("WZ_Np1").addSystematic(wzPtMin30DLS4)
                    ch.getSample("WZ_Np2").addSystematic(wzPtMin30DLS4)
                    ch.getSample("WZ_Np3").addSystematic(wzPtMin30DLS4)
                    ch.getSample("WZ_Np4").addSystematic(wzPtMin30DLS4)
                    ch.getSample("WZ_Np5").addSystematic(wzPtMin30DLS4)
                elif (ch.name.find("SSEl")>-1 or ch.name.find("SSMu")>-1):
                    ch.getSample("Top_Np0").addSystematic(topPtMin30SS)
                    ch.getSample("Top_Np1").addSystematic(topPtMin30SS)
                    ch.getSample("Top_Np2").addSystematic(topPtMin30SS)
                    ch.getSample("Top_Np3").addSystematic(topPtMin30SS)
                    ch.getSample("Top_Np4").addSystematic(topPtMin30SS)
                    ch.getSample("Top_Np5").addSystematic(topPtMin30SS)
                    ch.getSample("WZ_Np0").addSystematic(wzPtMin30SS)
                    ch.getSample("WZ_Np1").addSystematic(wzPtMin30SS)
                    ch.getSample("WZ_Np2").addSystematic(wzPtMin30SS)
                    ch.getSample("WZ_Np3").addSystematic(wzPtMin30SS)
                    ch.getSample("WZ_Np4").addSystematic(wzPtMin30SS)
                    ch.getSample("WZ_Np5").addSystematic(wzPtMin30SS)          
                    pass
                pass
            pass
        

##############################
# Finalize fit configs setup #
##############################

# b-tag reg/veto/agnostic channels
for chan in bReqChans:
    chan.hasBQCD = True
    chan.addSystematic(bTagSyst)

for chan in bVetoChans:
    chan.hasBQCD = False
    chan.addSystematic(bTagSyst)

for chan in bAgnosticChans:
    chan.hasBQCD = False
    chan.removeWeight("bTagWeight3Jet")

#Lepton flavor channels
def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

SetupChannels(seChans,bgdFiles_se,seChanSyst)
SetupChannels(elChans,bgdFiles_e, elChanSyst)
SetupChannels(smChans,bgdFiles_sm,smChanSyst)
SetupChannels(muChans,bgdFiles_m, muChanSyst)
SetupChannels(eeChans,bgdFiles_ee,eeChanSyst)
SetupChannels(mmChans,bgdFiles_mm,mmChanSyst)
SetupChannels(emChans,bgdFiles_em,emChanSyst)

##Final semi-hacks for signal samples in exclusion fits
if doExclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getTopLevelXML("Sig_%s"%sig)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
            #theSample.removeSystematic("JHigh")
            #theSample.removeSystematic("JMedium")
            #theSample.removeSystematic("JLow")                
            theSigFiles=[]
            if chan in elChans:
                theSigFiles = sigFiles_l
                theSample.removeSystematic("LESel")
            elif chan in seChans:
                theSigFiles = sigFiles_sl
                theSample.removeSystematic("LESse")
            elif chan in muChans:
                theSigFiles = sigFiles_l
                theSample.removeSystematic("LESmu")
                theSample.removeSystematic("LRMmu")
                theSample.removeSystematic("LRImu")
            elif chan in smChans:
                theSigFiles = sigFiles_sl
                theSample.removeSystematic("LESsm")
                theSample.removeSystematic("LRMsm")
                theSample.removeSystematic("LRIsm")
            elif (chan in eeChans) or (chan in mmChans) or (chan in emChans):
                theSigFiles = sigFiles
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)


#-------------------------------------------------
# Discovery fit (probably broken)
#-------------------------------------------------

#signalRegions = ["SR3jTEl", "SR3jTMu", "SR4jTEl", "SR4jTMu", "SSElT", "SSMuT", "S2eeT", "S2emT", "S2mmT", "S4eeT", "S4emT", "S4mmT","SR7jTEl","SR7jTMu"]
signalRegions = ["SR7jTEl","SR7jTMu"]

if doDiscovery:
    for iSR,sr in enumerate(signalRegions):
        discovery = configMgr.addTopLevelXMLClone(bkgOnly,"Discovery_%s"%sr)
        meas_dis=discovery.addMeasurement("DiscoveryMeasurement",lumi=1.0,lumiErr=0.037)
        meas_dis.addPOI("mu_SIG")
        srChannel = discovery.addChannel("meffInc",[sr],1,0,meffMax)
        discovery.setSignalChannels([srChannel])

        srChannel.addDiscoverySamples([sr],[1.],[0.],[100.],[kMagenta])     

        if (srChannel.name.find("SR3jTEl")>-1 or srChannel.name.find("SR4jTEl")>-1 or srChannel.name.find("SR7jTEl")>-1):
            srChannel.setFileList(bgdFiles_e)
        elif srChannel.name.find("SSEl")>-1:
            srChannel.setFileList(bgdFiles_se)
        elif (srChannel.name.find("SR3jTMu")>-1 or srChannel.name.find("SR4jTMu")>-1 or srChannel.name.find("SR7jTMu")>-1):
            srChannel.setFileList(bgdFiles_m)
        elif srChannel.name.find("SSMu")>-1:
            srChannel.setFileList(bgdFiles_sm)
        elif srChannel.name.find("ee")>-1:
            srChannel.setFileList(bgdFiles_ee)
        elif srChannel.name.find("em")>-1:
            srChannel.setFileList(bgdFiles_em)
        elif srChannel.name.find("mm")>-1:
            srChannel.setFileList(bgdFiles_mm)
