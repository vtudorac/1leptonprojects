################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

from logger import Logger
log = Logger('HardLepton')

# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return


# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

#check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]
SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")      # lepton scale factors (common)

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["5J"] #this is the default - modify from command line

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 5J,6J option)"
    
#remove later: warning not to use two towers at the same time at the moment.
if len(CRregions) > 1:
    print "FATAL: CR and SR are not yet orthogonal - don't run with more than one tower at the moment!"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["SR4J"] = False
ValidRegList["VR4J"] = False
ValidRegList["SR5J"] = False
ValidRegList["VR5J"] = False
ValidRegList["SR6J"] = False
ValidRegList["VR6J"] = False
#for tables (turn doTableInputs to True)
doTableInputs = True #This effectively means no validation plots but only validation tables (but is 100x faster)
for cr in CRregions:
    if "4J" in cr and doTableInputs:
        ValidRegList["VR4J"] = True
    if "5J" in cr and doTableInputs:
        ValidRegList["VR5J"] = True
    if "6J" in cr and doTableInputs:
        ValidRegList["VR6J"] = True        

#take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    sigSamples=["onestepGG_1225_625_25"]
    
#define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0]:
        analysissuffix += '_GG1step'
        
# First define HistFactory attributes
configMgr.analysisName = "OneHardLepton"+analysissuffix # Name to give the analysis
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = True # enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/shape_fit/HistFitter_earlyRun2/HistFitterUser/MET_jets_leptons/data/histograms.root" # the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1.
configMgr.outputLumi = 2. 
configMgr.setLumiUnits("fb-1")

configMgr.fixSigXSec=True
#configMgr.calculatorType=0 #toys
configMgr.calculatorType=2 #asimov
configMgr.testStatType=3
configMgr.nPoints=20

#configMgr.scanRange = (0., 20.) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
#configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = True # Blind the CRs (default is False)
configMgr.blindVR = True # Blind the VRs (default is False)

#using of statistical uncertainties?
useStat=True

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

#directory with background files:
inputDir="root://eosatlas//eos/atlas/user/n/nihartma/trees/01-10/"

#data and QCD:
# (don't have any at the moment)

#signal files
inputDirSig="root://eosatlas//eos/atlas/user/n/nihartma/trees/01-10/"


if not onLxplus:
    print "INFO : Running locally...\n"
    # put here paths to your local files if not running on lxplus
    # the following paths are for running locally at LMU
    inputDir="/project/etp3/nhartmann/trees/01-10/"
    inputDirSig="/project/etp3/nhartmann/trees/01-10/"   
else:
    print "INFO : Running on lxplus... \n"
    
#background files, separately for electron and muon channel:
bgdFiles_e = [inputDir+"allTrees.root"] 
bgdFiles_m = [inputDir+"allTrees.root"]
bgdFiles_em = [inputDir+"allTrees.root"]

#signal files
if myFitType==FitType.Exclusion:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allTrees.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allTrees.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allTrees.root"]


# ********************************************************************* #
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&& lep1Pt>35."
OneEleSelection = "&& AnalysisType==1"
OneMuoSelection = "&& AnalysisType==2"

# ------- 4J regions
configMgr.cutsDict["SR4J"]="nJet30>=4 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && met>350. && mt>350. && (met/meffInc30) > 0.2 && meffInc30>1600." + CommonSelection
configMgr.cutsDict["WR4J"]="nJet30>=4 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && met>200. && mt>40. && mt<80. && (met/meffInc30) > 0.2 && meffInc30<1600. && nBJet30_MV1==0" + CommonSelection
configMgr.cutsDict["TR4J"]="nJet30>=4 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && met>200. && mt>40. && mt<120. && (met/meffInc30) > 0.2 && meffInc30<1600. && nBJet30_MV1>0" + CommonSelection
configMgr.cutsDict["VR4J_mt"]="nJet30>=4 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && met>350. && mt>120.&& (met/meffInc30) > 0.2 && meffInc30<1600." + CommonSelection
configMgr.cutsDict["VR4J_meff"]="nJet30>=4 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && met>350. && mt>120. && mt<350. && (met/meffInc30) > 0.2 && meffInc30>1600." + CommonSelection

# ------- 5J region
configMgr.cutsDict["SR5J"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && jet3Pt>100. && jet4Pt>100. && jet5Pt>100. && met>200. && mt>250. && meffInc30>1400." + CommonSelection
configMgr.cutsDict["WR5J"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && jet3Pt>40. && jet4Pt>40. && jet5Pt>40. && met>200. && mt>40. && mt<120. && meffInc30>800. && meffInc30<1400. && nBJet30_MV1==0" + CommonSelection
configMgr.cutsDict["TR5J"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && jet3Pt>40. && jet4Pt>40. && jet5Pt>40. && met>200. && mt>40. && mt<120. && meffInc30>800. && meffInc30<1400. && nBJet30_MV1>0" + CommonSelection
configMgr.cutsDict["VR5J_mt"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && jet3Pt>100. && jet4Pt>100. && jet5Pt>100. && met>200. && mt>120. && meffInc30>800. && meffInc30<1200." + CommonSelection
configMgr.cutsDict["VR5J_meff"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && jet3Pt>100. && jet4Pt>100. && jet5Pt>100. && met>200. && mt>120. && mt<200. && meffInc30>1400." + CommonSelection

# ------- 6J region
configMgr.cutsDict["SR6J"]="nJet30>=6 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && jet5Pt>50. && jet6Pt>50. && met>350. && mt>200. && meffInc30>1200." + CommonSelection
configMgr.cutsDict["WR6J"]="nJet30>=6 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>30. && jet5Pt>30. && jet6Pt>30. && met>200. && met<350. && mt>40. && mt<120. && meffInc30>600 && nBJet30_MV1==0" + CommonSelection
configMgr.cutsDict["TR6J"]="nJet30>=6 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>30. && jet5Pt>30. && jet6Pt>30. && met>200. && met<350. && mt>40. && mt<120. && meffInc30>600 && nBJet30_MV1>0" + CommonSelection
configMgr.cutsDict["VR6J_mt"]="nJet30>=6 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && jet5Pt>50. && jet6Pt>50. && met>200. && met<350. && mt>200. && meffInc30<1400" + CommonSelection
configMgr.cutsDict["VR6J_met"]="nJet30>=6 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && jet5Pt>50. && jet6Pt>50. && met>350. && mt<200. && meffInc30>1200." + CommonSelection

#second part: splitting into electron and muon channel for validation purposes
d=configMgr.cutsDict
configMgr.cutsDict["SR4JEl"] = d["SR4J"]+OneEleSelection
configMgr.cutsDict["SR4JMu"] = d["SR4J"]+OneMuoSelection
configMgr.cutsDict["WR4JEl"] = d["WR4J"]+OneEleSelection
configMgr.cutsDict["WR4JMu"] = d["WR4J"]+OneMuoSelection
configMgr.cutsDict["TR4JEl"] = d["TR4J"]+OneEleSelection
configMgr.cutsDict["TR4JMu"] = d["TR4J"]+OneMuoSelection
configMgr.cutsDict["VR4J_meffEl"] = d["VR4J_meff"]+OneEleSelection
configMgr.cutsDict["VR4J_meffMu"] = d["VR4J_meff"]+OneMuoSelection
configMgr.cutsDict["VR4J_mtEl"] = d["VR4J_mt"]+OneEleSelection
configMgr.cutsDict["VR4J_mtMu"] = d["VR4J_mt"]+OneMuoSelection

configMgr.cutsDict["SR5JEl"] = d["SR5J"]+OneEleSelection
configMgr.cutsDict["SR5JMu"] = d["SR5J"]+OneMuoSelection
configMgr.cutsDict["WR5JEl"] = d["WR5J"]+OneEleSelection
configMgr.cutsDict["WR5JMu"] = d["WR5J"]+OneMuoSelection
configMgr.cutsDict["TR5JEl"] = d["TR5J"]+OneEleSelection
configMgr.cutsDict["TR5JMu"] = d["TR5J"]+OneMuoSelection
configMgr.cutsDict["VR5J_meffEl"] = d["VR5J_meff"]+OneEleSelection
configMgr.cutsDict["VR5J_meffMu"] = d["VR5J_meff"]+OneMuoSelection
configMgr.cutsDict["VR5J_mtEl"] = d["VR5J_mt"]+OneEleSelection
configMgr.cutsDict["VR5J_mtMu"] = d["VR5J_mt"]+OneMuoSelection

configMgr.cutsDict["SR6JEl"] = d["SR6J"]+OneEleSelection
configMgr.cutsDict["SR6JMu"] = d["SR6J"]+OneMuoSelection
configMgr.cutsDict["WR6JEl"] = d["WR6J"]+OneEleSelection
configMgr.cutsDict["WR6JMu"] = d["WR6J"]+OneMuoSelection
configMgr.cutsDict["TR6JEl"] = d["TR6J"]+OneEleSelection
configMgr.cutsDict["TR6JMu"] = d["TR6J"]+OneMuoSelection
configMgr.cutsDict["VR6J_metEl"] = d["VR6J_met"]+OneEleSelection
configMgr.cutsDict["VR6J_metMu"] = d["VR6J_met"]+OneMuoSelection
configMgr.cutsDict["VR6J_mtEl"] = d["VR6J_mt"]+OneEleSelection
configMgr.cutsDict["VR6J_mtMu"] = d["VR6J_mt"]+OneMuoSelection

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #

#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]

configMgr.weights = weights
#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

muonSFHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUONSFSTAT__1up")
muonSFLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUONSFSTAT__1down")
muonSFHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUONSFSYS__1up")
muonSFLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUONSFSYS__1down")

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}

for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    #Example systematic uncertainty
    if "JER" in SystList: 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JER__1up","_JER__1up","tree","overallNormHistoSysOneSide"))
    if "JES" in SystList: 
        basicChanSyst[region].append(Systematic("JES_BJES_Response","_NoSys","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))    
        basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_NoSys","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_NoSys","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_NoSys","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_NoSys","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))  
        basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_NoSys","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("JES_EffectiveNP_6restTerm","_NoSys","_JET_EffectiveNP_6restTerm__1up","_JET_EffectiveNP_6restTerm__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_NoSys","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_NoSys","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_NoSys","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))         
        basicChanSyst[region].append(Systematic("JES_Flavor_Response","_NoSys","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_NoSys","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_NoSys","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_NoSys","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_NoSys","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("JES_PunchThrough_MC12","_NoSys","_JET_PunchThrough_MC12__1up","_JET_PunchThrough_MC12__1down","tree","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_NoSys","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys")) 
    if "MET" in SystList:
        basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_MET_SoftTrk_ResoPara","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_MET_SoftTrk_ResoPerp","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSysOneSide"))     
        basicChanSyst[region].append(Systematic("EL_SCALE_MOMENTUM","_NoSys","_EL_SCALE_MOMENTUM__1up","_EL_SCALE_MOMENTUM__1down","tree","overallNormHistoSysOneSide"))
        basicChanSyst[region].append(Systematic("MUONS_ID","_NoSys","_MUONS_ID__1up","_MUONS_ID__1down","tree","overallNormHistoSysOneSide"))
        basicChanSyst[region].append(Systematic("MUONS_MS","_NoSys","_MUONS_MS__1up","_MUONS_MS__1down","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MUONS_SCALE","_NoSys","_MUONS_SCALE__1up","_MUONS_SCALE__1down","tree","overallNormHistoSysOneSide"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_SF_stat",configMgr.weights,muonSFHighWeights_stat,muonSFLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_SF_sys",configMgr.weights,muonSFHighWeights_sys,muonSFLowWeights_sys,"weight","overallNormHistoSys"))        


# ********************************************************************* #
#                              Background samples
# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("Data",kBlack)
#DataSample.setFileList([inputDir_data+"",inputDir_data+""])
DataSample.setData()

# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")
bkgOnly.addSamples([ttbarVSample,DibosonsSample,SingleTopSample,ZSample,TTbarSample,WSample,DataSample])

if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.028)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
bReqChans["4J"] = []
bVetoChans["4J"] = []
bAgnosticChans["4J"] = []
bReqChans["5J"] = []
bVetoChans["5J"] = []
bAgnosticChans["5J"] = []
bReqChans["6J"] = []
bVetoChans["6J"] = []
bAgnosticChans["6J"] = []

#lepton flavor classification of channels
elChans = {}
muChans = {}
elChans["4J"] = []
muChans["4J"] = []
elChans["5J"] = []
muChans["5J"] = []
elChans["6J"] = []
muChans["6J"] = []

elmuChans = {}
elmuChans["4J"] = []
elmuChans["5J"] = []
elmuChans["6J"] = []


######################################################
# Add channels to Bkg-only configuration             #
######################################################

#-----3JET--------#
if "4J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4J"],1,0.5,1.5),[elmuChans["4J"],bVetoChans["4J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4J"],1,0.5,1.5),[elmuChans["4J"],bReqChans["4J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
if "5J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR5J"],1,0.5,1.5),[elmuChans["5J"],bVetoChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR5J"],1,0.5,1.5),[elmuChans["5J"],bReqChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
if "6J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6J"],1,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6J"],1,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)    

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #
#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]  = ValidRegList["VR4J"] + ValidRegList["VR5J"] + ValidRegList["VR6J"]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans["4J"])
        appendIfMatchName(c,bVetoChans["4J"])
        appendIfMatchName(c,bAgnosticChans["4J"])
        appendIfMatchName(c,elChans["4J"])
        appendIfMatchName(c,muChans["4J"])      
        appendIfMatchName(c,elmuChans["4J"])

        appendIfMatchName(c,bReqChans["5J"])
        appendIfMatchName(c,bVetoChans["5J"])
        appendIfMatchName(c,bAgnosticChans["5J"])
        appendIfMatchName(c,elChans["5J"])
        appendIfMatchName(c,muChans["5J"])      
        appendIfMatchName(c,elmuChans["5J"])

        appendIfMatchName(c,bReqChans["6J"])
        appendIfMatchName(c,bVetoChans["6J"])
        appendIfMatchName(c,bAgnosticChans["6J"])
        appendIfMatchName(c,elChans["6J"])
        appendIfMatchName(c,muChans["6J"])      
        appendIfMatchName(c,elmuChans["6J"])       

    if doTableInputs:
        if "4J" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR4JEl"],1,0.5,1.5),[bAgnosticChans["4J"],elChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JMu"],1,0.5,1.5),[bAgnosticChans["4J"],muChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["SR4J"],1,0.5,1.5),[bAgnosticChans["4J"],elmuChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["VR4J_meffEl"],1,0.5,1.5),[bAgnosticChans["4J"],elChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["VR4J_meffMu"],1,0.5,1.5),[bAgnosticChans["4J"],muChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["VR4J_meff"],1,0.5,1.5),[bAgnosticChans["4J"],elmuChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["VR4J_mtEl"],1,0.5,1.5),[bAgnosticChans["4J"],elChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["VR4J_mtMu"],1,0.5,1.5),[bAgnosticChans["4J"],muChans["4J"]])
            appendTo(validation.addValidationChannel("cuts",["VR4J_mt"],1,0.5,1.5),[bAgnosticChans["4J"],elmuChans["4J"]])
        if "5J" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR5JEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["SR5JMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["SR5J"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_meffEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_meffMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_meff"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_mtEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_mtMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_mt"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
        if "6J" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR6JEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6J"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_metEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_metMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_met"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_mtEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_mtMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_mt"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])            
           
    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs       


# ********************************************************************* #
#                              Later: exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    if '4J' in CRregions:
        SRs=["SR4J"]
    if '5J' in CRregions:
        SRs=["SR5J"]
    elif '6J' in CRregions:
        SRs=["SR6J"]

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans["4J"])
            appendIfMatchName(c,bVetoChans["4J"])
            appendIfMatchName(c,bAgnosticChans["4J"])
            appendIfMatchName(c,elChans["4J"])
            appendIfMatchName(c,muChans["4J"])
            appendIfMatchName(c,elmuChans["4J"])
            appendIfMatchName(c,bReqChans["5J"])
            appendIfMatchName(c,bVetoChans["5J"])
            appendIfMatchName(c,bAgnosticChans["5J"])
            appendIfMatchName(c,elChans["5J"])
            appendIfMatchName(c,muChans["5J"])
            appendIfMatchName(c,elmuChans["5J"])
            appendIfMatchName(c,bReqChans["6J"])
            appendIfMatchName(c,bVetoChans["6J"])
            appendIfMatchName(c,bAgnosticChans["6J"])
            appendIfMatchName(c,elChans["6J"])
            appendIfMatchName(c,muChans["6J"])
            appendIfMatchName(c,elmuChans["6J"])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        #sigSample.addSystematic(xsecSig)
            
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:           
            if sr=="SR4JEl" or sr=="SR4JMu" or sr=="SR4J":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
            elif sr=="SR5JEl" or sr=="SR5JMu" or sr=="SR5J":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)                    
            elif sr=="SR6JEl" or sr=="SR6JMu" or sr=="SR6J":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)                    
            else:
                raise RuntimeError("Unexpected signal region %s"%sr)
                
            if sr=="SR4JEl": 
                elChans["4J"].append(ch) 
                bAgnosticChans["4J"].append(ch)
            elif sr=="SR4JMu": 
                muChans["4J"].append(ch) 
                bAgnosticChans["4J"].append(ch)
            elif sr=="SR4J":
                elmuChans["4J"].append(ch)
                bAgnosticChans["4J"].append(ch)                
            elif sr=="SR5JEl": 
                elChans["5J"].append(ch) 
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR5JMu": 
                muChans["5J"].append(ch) 
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR5J":
                elmuChans["5J"].append(ch)
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR6JEl": 
                elChans["6J"].append(ch) 
                bAgnosticChans["6J"].append(ch)
            elif sr=="SR6JMu": 
                muChans["6J"].append(ch) 
                bAgnosticChans["6J"].append(ch)
            elif sr=="SR6J":
                elmuChans["6J"].append(ch)
                bAgnosticChans["6J"].append(ch)               
            else:
                raise RuntimeError("Unexpected signal region %s"%sr)
            pass
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]

for region in CRregions:
    SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
    SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
    SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])

##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig) 
            sys_region = ""
            if "4J" in chan.name: sys_region = "4J"
            elif "5J" in chan.name: sys_region = "5J"
            elif "6J" in chan.name: sys_region = "6J"            
            else: 
                print "Unknown region! - Take systematics from 5J regions."
                sys_region = "5J"
            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans["4J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["4J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["4J"]:
                theSigFiles = sigFiles_em[sig]
            elif chan in elChans["5J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["5J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["5J"]:
                theSigFiles = sigFiles_em[sig]
            elif chan in elChans["6J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["6J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["6J"]:
                theSigFiles = sigFiles_em[sig]                
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)
        chan.addWeight("bTagWeight")


    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        chan.addWeight("bTagWeight")  
 

    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        chan.getSample("ttbar").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
        chan.getSample("wjets").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
        chan.getSample("ttv").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
        chan.getSample("singletop").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])               
        chan.getSample("diboson").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
        chan.getSample("zjets").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation : validation.tLegend = leg
if myFitType==FitType.Exclusion: 
    entry = leg.AddEntry("","Signal","lf")
    entry.SetLineColor(sigSample.color)
    entry.SetFillColor(sigSample.color)
    entry.SetFillStyle(compFillStyle)
    myTopLvl.tLegend = leg
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion: chan.logY = True
    if chan.logY:
        chan.minY = 0.02
        chan.maxY = 1000000
    else:
        chan.minY = 0.05 
        chan.maxY = 3000
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Entries"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.15
            chan.ATLASLabelY = 0.83
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
