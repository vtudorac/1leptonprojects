################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

#gROOT.LoadMacro("~/atlasstyle/AtlasStyle.C")
#ROOT.SetAtlasStyle()

import SignalAccUncertainties

from logger import Logger
log = Logger('HardLepton')

# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return


# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

#check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]
SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
SystList.append("pileup")  # pileup (common)
SystList.append("BTag")     #b-tagging uncertainties

#SystList.append("GenPowhegJimmyTTbar") # PowhegPythia/PowhegJimmy generators difference TTbar
#SystList.append("FactScaleTTbar") # factorization scale variation in TTbar
#SystList.append("ISRFSRTTbar") # ISR/FSR variation in ttbar
#SystList.append("RenScaleTTbar") # re-normalization scale variation in TTbar

SystList.append("FactRenScaleISRFSRTTbar") # QCD re-normalization and factorization scale and ISR/FSR variation in ttbar
SystList.append("HadFragTTbar") # Hadronization/fragmentation, PowhegPythia/PowhegHerwigpp generator differences in ttbar
SystList.append("HardScatteringGenTTbar") #Hard scattering generation,PowhegHerwigpp/MC@NLOHerwigpp. 
##SystList.append("GenSingleTop")#cross section uncertainties
SystList.append("GenDB") # Generator Systematics DB    (common)
SystList.append("GenZjets") # Generator Systematics Zjets    (common)
SystList.append("WjetsTheo") # comparing Sherpa (baseline) to Madgraph+Pythia8 (alternative)
SystList.append("WjetsScaleVar") # scale variations in Sherpa
SystList.append("ZjetsTheo") # comparing Sherpa (baseline) to Madgraph+Pythia8 (alternative)
SystList.append("ZjetsScaleVar") # scale variations in Sherpa
SystList.append("DibosonsTheo") # comparing Sherpa (baseline) to Powheg-box+Pythia8 (alternative
SystList.append("DibosonsScaleVar") # scale variations 
  

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["5J"] #this is the default - modify from command line

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 5J,6J,4J,4Jlowx,4Jhighx option)"
    
#remove later: warning not to use two towers at the same time at the moment.
if len(CRregions) > 1:
    print "FATAL: CR and SR are not yet orthogonal - don't run with more than one tower at the moment!"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["CR4Jlowx"] = False
ValidRegList["SR4Jlowx"] = False
ValidRegList["VR4Jlowx"] = False
ValidRegList["CR4Jhighx"] = False
ValidRegList["SR4Jhighx"] = False
ValidRegList["VR4Jhighx"] = False
ValidRegList["CR5J"] = False
ValidRegList["SR5J"] = False
ValidRegList["VR5J"] = False
ValidRegList["CR6J"] = False
ValidRegList["SR6J"] = False
ValidRegList["VR6J"] = False
#for tables (turn doTableInputs to True)
doTableInputs = False #This effectively means no validation plots but only validation tables (but is 100x faster)
for cr in CRregions:
    if "4Jlowx" in cr and doTableInputs:
        ValidRegList["VR4Jlowx"] = True       
    if "4Jhighx" in cr and doTableInputs:
        ValidRegList["VR4Jhighx"] = True     
    if "5J" in cr and doTableInputs:
        ValidRegList["VR5J"] = True
    if "6J" in cr and doTableInputs:
        ValidRegList["VR6J"] = True        

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]  = ValidRegList["VR4Jlowx"] + ValidRegList["VR4Jhighx"] + ValidRegList["VR5J"] + ValidRegList["VR6J"] + ValidRegList["SR4Jlowx"] + ValidRegList["SR4Jhighx"] + ValidRegList["SR5J"] + ValidRegList["SR6J"] + ValidRegList["CR4Jlowx"] + ValidRegList["CR4Jhighx"] + ValidRegList["CR5J"] + ValidRegList["CR6J"]

#take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    sigSamples=["onestepGG_1385_705_25"]
    
#define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0]:
        analysissuffix += '_GG1step'

mylumi= 3.20905 #3.31668 #3.34258 #2.67399 #1.98923  #1.71207   #    1.41104  for  trees in root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/strong1L/T_03_02/trees/tmp/

doOnlySignal=False
#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension = "_"+myoptions
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension = "_onlysignal"
else:
    print "No additional user arguments given - proceed with default analysis!"
        
# First define HistFactory attributes
configMgr.analysisName = "OneHardLepton"+analysissuffix+analysisextension # Name to give the analysis
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
if myoptions!="" or myFitType==FitType.Exclusion or ValidRegList["OneLep"]:
   configMgr.useCacheToTreeFallback = True # enable the fallback to trees
   configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
# the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
if myoptions=="sensitivity_4":
    configMgr.histBackupCacheFile =  "root://eosatlas//eos/atlas/user/j/jlorenz/AfterFAR/OneHardLepton_5J_sensitivity_4.root"  
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/OneHardLepton_5J_sensitivity_4.root"          
elif myoptions=="sensitivity_3":
    configMgr.histBackupCacheFile =  "root://eosatlas//eos/atlas/user/j/jlorenz/AfterFAR/OneHardLepton_5J_sensitivity_3.root" 
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/OneHardLepton_5J_sensitivity_3.root"       
elif myoptions=="sensitivity_2":
    configMgr.histBackupCacheFile =  "root://eosatlas//eos/atlas/user/j/jlorenz/AfterFAR/OneHardLepton_5J_sensitivity_2.root" 
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/OneHardLepton_5J_sensitivity_2.root" 
elif (myFitType==FitType.Exclusion and '5J' in CRregions) or ValidRegList["CR5J"] or ValidRegList["VR5J"] or ValidRegList["SR5J"]:
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/HistFitterOutput/OneHardLepton_5J_4.root"  
elif (myFitType==FitType.Exclusion and '6J' in CRregions) or ValidRegList["CR6J"] or ValidRegList["VR6J"] or ValidRegList["SR6J"]:
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/HistFitterOutput/OneHardLepton_6J_4.root"    
elif (myFitType==FitType.Exclusion and '4Jlowx' in CRregions) or ValidRegList["CR4Jlowx"] or ValidRegList["VR4Jlowx"] or ValidRegList["SR4Jlowx"]:
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/HistFitterOutput/OneHardLepton_4Jlowx_4.root"   
elif (myFitType==FitType.Exclusion and '4Jhighx' in CRregions) or ValidRegList["CR4Jhighx"] or ValidRegList["VR4Jhighx"] or ValidRegList["SR4Jhighx"]:
    if not onLxplus: configMgr.histBackupCacheFile =  "/project/etp3/jlorenz/HistFitterTrees/15Earlydata/HistFitterOutput/OneHardLepton_4Jhighx_4.root"       
    
# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
#configMgr.outputLumi =  0.0814  #output lumi 84. pb-1
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

configMgr.fixSigXSec=True
#configMgr.calculatorType=0 #toys
configMgr.calculatorType=2 #asimov
configMgr.testStatType=3
configMgr.nPoints=20

#configMgr.scanRange = (200.,400.) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#in the case we only want signal histograms we now do a simple trick to get a dummy after fit workspace without the need to run over all backgrounds - we just blind all the regions. Obviously, the results after fits are then wrong, but the results before the fit are just fine....
if doOnlySignal:
    configMgr.blindCR = True # Blind the CRs (default is False)
    configMgr.blindVR = True # Blind the VRs (default is False)

#using of statistical uncertainties?
useStat=True

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

#directory with background files:
inputDir="root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/strong1L/T_04_02/"

#data and QCD:
# (don't have any at the moment)

#signal files
inputDirSig="root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/strong1L/T_04_02/"

if not onLxplus:
    print "INFO : Running locally...\n"
    # put here paths to your local files if not running on lxplus
    # the following paths are for running locally at LMU
    inputDir="/project/etp3/jlorenz/HistFitterTrees/15Earlydata/"
    inputDirSig="/project/etp3/jlorenz/HistFitterTrees/15Earlydata/"   
else:
    print "INFO : Running on lxplus... \n"
    
#background files, separately for electron and muon channel:
bgdFiles_e = [inputDir+"allTrees_T_04_02.root"] 
bgdFiles_m = [inputDir+"allTrees_T_04_02.root"]
bgdFiles_em = [inputDir+"allTrees_T_04_02.root"]

#signal files
if myFitType==FitType.Exclusion or doOnlySignal:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allTrees_T_04_02_GGsigOnly.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allTrees_T_04_02_GGsigOnly.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allTrees_T_04_02_GGsigOnly.root"]


# ********************************************************************* #
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&& lep1Pt>35. && HLT_xe70_tc_lcw"# && isDuplicateEvent==0"
OneEleSelection = "&& (AnalysisType==1)"
OneMuoSelection = "&& (AnalysisType==2)"
OneLepSelection = ""#"&& ( (AnalysisType==1 && (HLT_e24_lhmedium_iloose_L1EM18VH || HLT_e60_lhmedium)) || (AnalysisType==2 && (HLT_mu20_iloose_L1MU15 || HLT_mu50)) )"


# ------- 4J regions for gluino gridx low x
configMgr.cutsDict["SR4Jlowx"]="nJet30>=4 && jet1Pt>325. && jet4Pt>150. && mt>125. && met>200. && meffInc30>2000. && JetAplanarity>0.04 " + CommonSelection# Oct2015
configMgr.cutsDict["WR4Jlowx"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && mt>60. && mt<125. && met>200. && meffInc30>1900. && nBJet30_MV1==0 && JetAplanarity<0.04" + CommonSelection
#configMgr.cutsDict["TR4Jlowx"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && mt>60. && mt<125. && met>200. && meffInc30>1900. && nBJet30_MV1>0 && JetAplanarity<0.04" + CommonSelection
configMgr.cutsDict["TR4Jlowx"]="nJet30>3  && jet1Pt>325. && jet4Pt>30 && mt>95 && mt<125 && met>200  && meffInc30>1500 && nBJet30_MV1>0 && JetAplanarity<0.04" + CommonSelection
configMgr.cutsDict["VR4Jlowx_mt"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && mt>125. && mt<350.  && met>200. && meffInc30>1900. && JetAplanarity<0.04 " + CommonSelection
configMgr.cutsDict["VR4Jlowx_aplanarity"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && mt>60. && mt<125.  && met>200. && meffInc30>1900. && JetAplanarity>0.04 " + CommonSelection
#different additional validation regions
configMgr.cutsDict["SR4Jlowxnomt"]="nJet30>=4 && jet1Pt>325. && jet4Pt>150. && met>200. && meffInc30>2000. && JetAplanarity>0.04 " + CommonSelection
configMgr.cutsDict["SR4JlowxnoJetAplanarity"]="nJet30>=4 && jet1Pt>325. && jet4Pt>150. && mt>125. && met>200. && meffInc30>2000. " + CommonSelection
configMgr.cutsDict["SR4Jlowxnomeff"]="nJet30>=4 && jet1Pt>325. && jet4Pt>150. && mt>125. && met>200. && JetAplanarity>0.04 " + CommonSelection

# ------- 4J regions for gluino gridx high x
configMgr.cutsDict["SR4Jhighx"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && mt>425. && meffInc30>1800. && (met/meffInc30)>0.3" + CommonSelection # FAR OCT15 - sr_gg_1400_1300_60
configMgr.cutsDict["WR4Jhighx"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && mt>60. && mt<125. && meffInc30>1800. && (met/meffInc30)<0.3 && nBJet30_MV1==0" + CommonSelection # FAR OCT15
configMgr.cutsDict["TR4Jhighx"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && mt>60. && mt<125. && meffInc30>1800. && (met/meffInc30)<0.3 && nBJet30_MV1>0" + CommonSelection  # FAR OCT15
configMgr.cutsDict["VR4Jhighx_mt"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && mt>150. && mt<475. && meffInc30>1800. && (met/meffInc30)<0.3 && JetAplanarity<0.04" + CommonSelection
configMgr.cutsDict["VR4Jhighx_meff"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && mt>60. && mt<125. && meffInc30>1800. && (met/meffInc30)>0.3" + CommonSelection
#different additional validation regions
configMgr.cutsDict["SR4Jhighxnomt"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && meffInc30>1800. && (met/meffInc30)>0.3" + CommonSelection
configMgr.cutsDict["SR4Jhighxnometovermeff"]="nJet30>=4 && jet1Pt>325. && jet4Pt>30. && met>200. && mt>425. && meffInc30>1800." + CommonSelection

# ------- 5J regions
configMgr.cutsDict["SR5J"]="nJet30>=5 && jet1Pt>225. && jet5Pt>50.  && met>250. && mt>275. && meffInc30>1800. && JetAplanarity>0.04 && (met/meffInc30) > 0.1" + CommonSelection# Oct2015
configMgr.cutsDict["TR5J"]="nJet30>=5 && jet1Pt>225. && jet5Pt>30. && mt<125. && mt>60. && met>250. && meffInc30>1500. && JetAplanarity < 0.04 && (met/meffInc30)>0.1 && nBJet30_MV1>0" + CommonSelection
configMgr.cutsDict["WR5J"]="nJet30>=5 && jet1Pt>225. && jet5Pt>30. && mt<125. && mt>60. && met>250. && meffInc30>1500. && JetAplanarity < 0.04 && (met/meffInc30)>0.1 && nBJet30_MV1==0" + CommonSelection
configMgr.cutsDict["VR5J_mt"]="nJet30>=5 && jet1Pt>225. && jet5Pt>30. && mt>125. && mt<350. && met>250. && meffInc30>1500. && JetAplanarity<0.04 && (met/meffInc30)>0.1" + CommonSelection
configMgr.cutsDict["VR5J_aplanarity"]="nJet30>=5 && jet1Pt>225. && jet5Pt>30. && mt>60. && mt<125. && met>250. && meffInc30>1500. && JetAplanarity>0.04 && (met/meffInc30)>0.1" + CommonSelection
#different additional validation regions
configMgr.cutsDict["SR5Jnomeff"]="nJet30>=5 && jet1Pt>225. && jet5Pt>50.  && met>250. && mt>275. && JetAplanarity>0.04 && (met/meffInc30) > 0.1" + CommonSelection#
configMgr.cutsDict["SR5Jnomt"]="nJet30>=5 && jet1Pt>225. && jet5Pt>50.  && met>250. && meffInc30>1800. && JetAplanarity>0.04 && (met/meffInc30) > 0.1" + CommonSelection
configMgr.cutsDict["SR5JnoJetAplanarity"]="nJet30>=5 && jet1Pt>225. && jet5Pt>50.  && met>250. && mt>275. && meffInc30>1800. && (met/meffInc30) > 0.1" + CommonSelection

# ------- 6J region
configMgr.cutsDict["SR6J"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection# Oct2015
configMgr.cutsDict["TR6J"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt<125. && mt>60. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2  && nBJet30_MV1>0" + CommonSelection
configMgr.cutsDict["WR6J"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt<125. && mt>60. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2  && nBJet30_MV1==0" + CommonSelection
configMgr.cutsDict["VR6J_mt"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>125. && mt<350. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["VR6J_aplanarity"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250.  && mt<125. && mt>60. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection
#different additional validation regions
configMgr.cutsDict["SR6Jnomt"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["SR6JnoJetAplanarity"]="nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>1000. && (met/meffInc30) > 0.2" + CommonSelection



#second part: splitting into electron and muon channel for validation purposes
d=configMgr.cutsDict
defined_regions = []
if '5J' in CRregions: defined_regions+=['SR5J','TR5J','WR5J','VR5J_mt','VR5J_aplanarity','SR5Jnomeff','SR5Jnomt','SR5JnoJetAplanarity']
if '6J' in CRregions: defined_regions+=['SR6J','TR6J','WR6J','VR6J_mt','VR6J_aplanarity','SR6Jnomt','SR6JnoJetAplanarity']
if '4Jlowx' in CRregions: defined_regions+=['SR4Jlowx','TR4Jlowx','WR4Jlowx','VR4Jlowx_mt','VR4Jlowx_aplanarity','SR4Jlowxnomt','SR4JlowxnoJetAplanarity','SR4Jlowxnomeff']
if '4Jhighx' in CRregions: defined_regions+=['SR4Jhighx','TR4Jhighx','WR4Jhighx','VR4Jhighx_mt','VR4Jhighx_meff','SR4Jhighxnomt','SR4Jhighxnometovermeff']
for pre_region in defined_regions:
    configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection
    configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection
    configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection    


# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #

#hacks for W+jets and Z+jets samples:
wjets_hackedweight = "(1. + ( DatasetNumber==361365 || DatasetNumber==361364 || DatasetNumber==361363 || DatasetNumber==361362 || DatasetNumber==361361 || DatasetNumber==361360 || DatasetNumber==361359 || DatasetNumber==361358 || DatasetNumber==361357 || DatasetNumber==361341 || DatasetNumber==361340 || DatasetNumber==361339 || DatasetNumber==361338 || DatasetNumber==361337 || DatasetNumber==361336 || DatasetNumber==361335 || DatasetNumber==361334 || DatasetNumber==361333 || DatasetNumber==361317 || DatasetNumber==361316 || DatasetNumber==361315 || DatasetNumber==361314 || DatasetNumber==361312 || DatasetNumber==361311 || DatasetNumber==361310 || DatasetNumber==361309  )*0.1)"
zjets_hackedweight = "(1. + (DatasetNumber==361461 || DatasetNumber==361460 || DatasetNumber==361459 || DatasetNumber==361458 || DatasetNumber==361457 || DatasetNumber==361456 || DatasetNumber==361455 || DatasetNumber==361454 || DatasetNumber==361453 || DatasetNumber==361437 || DatasetNumber==361436 || DatasetNumber==361435 || DatasetNumber==361434 || DatasetNumber==361433 || DatasetNumber==361432 || DatasetNumber==361431 || DatasetNumber==361430 || DatasetNumber==361429 || DatasetNumber==361413 || DatasetNumber==361412 || DatasetNumber==361411 || DatasetNumber==361410 || DatasetNumber==361409 || DatasetNumber==361408 || DatasetNumber==361407 || DatasetNumber==361406 || DatasetNumber==361405 || DatasetNumber==361389 || DatasetNumber==361388 || DatasetNumber==361387 || DatasetNumber==361386 || DatasetNumber==361385 || DatasetNumber==361384 || DatasetNumber==361383 || DatasetNumber==361382 || DatasetNumber==361381 )*0.1)"

#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","((DatasetNumber==361313)+pileupWeight*(DatasetNumber!=361313))","bTagWeight"] #,"triggerWeight"
#weights=["genWeightWithDuplicates","eventWeight","leptonWeight","pileupWeight","bTagWeight"]

configMgr.weights = weights
#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")
muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")

#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#electron related uncertainties acting on weights
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TotalCorrUncertainty__1up") 
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TotalCorrUncertainty__1down")
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TotalCorrUncertainty__1up") 
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TotalCorrUncertainty__1down")
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TotalCorrUncertainty__1up") 
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TotalCorrUncertainty__1down")

#pileup systematics
pileupup = replaceWeight(weights,"((DatasetNumber==361313)+pileupWeight*(DatasetNumber!=361313))","((DatasetNumber==361313)+pileupWeightUp*(DatasetNumber!=361313))")
pileupdown = replaceWeight(weights,"((DatasetNumber==361313)+pileupWeight*(DatasetNumber!=361313))","((DatasetNumber==361313)+pileupWeightDown*(DatasetNumber!=361313))")

#b-tagging:
if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")   
    

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst = {}

SingleTopComm = {}
WjetsComm = {}
DibosonsComm = {}
ZjetsComm = {}
ttvComm = {}
SingleTopWtXsec = {}
SingleTopSXsec = {}
SingleTopTXsec ={}
SingleAntiTopSXsec = {}
SingleAntiTopTXsec ={}
DBXsec={}
ZjetsXsec={}


for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))
    if "JES" in SystList: 
        basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
        basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_ID","_NoSys","_MUONS_ID__1up","_MUONS_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_MS","_NoSys","_MUONS_MS__1up","_MUONS_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUONS_SCALE","_NoSys","_MUONS_SCALE__1up","_MUONS_SCALE__1down","tree","overallNormHistoSys"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys"))         
        #trigger
        #basicChanSyst[region].append(Systematic("MUON_Eff_trigger_stat",configMgr.weights,muonHighWeights_trigger_stat,muonLowWeights_trigger_stat,"weight","overallNormHistoSys")) 
        #basicChanSyst[region].append(Systematic("MUON_Eff_trigger_syst",configMgr.weights,muonHighWeights_trigger_syst,muonLowWeights_trigger_syst,"weight","overallNormHistoSys"))         
        basicChanSyst[region].append(Systematic("EL_Eff_ID",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EL_Eff_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys"))   
        basicChanSyst[region].append(Systematic("EL_Eff_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys"))         
    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))   
        
    if "BTag" in SystList:
        bTagSyst[region] = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region] = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region] = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys") 
        eTagSyst[region] = Systematic("btag_Extrapolation",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")
	
	
    #Temporary common syst for minor backgrounds and Wjets
    SingleTopComm[region] = Systematic("h1L_SingleTopComm", configMgr.weights,configMgr.weights+["( 1+0.8 )"],configMgr.weights+["( 1-0.8 )"], "weight","overallSys")
    WjetsComm[region] = Systematic("h1L_WjetsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallNormSys")
    DibosonsComm[region] = Systematic("h1L_DibosonsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
    ZjetsComm[region] = Systematic("h1L_ZjetsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
    ttvComm[region] = Systematic("h1L_ttvComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
         
         
    #signal uncertainties
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
	
      
# Generator Systematics
    generatorSyst = []	


# cross section uncertainties for SingleTop, diboson and Z(? to add)
    if "GenSingleTop" in SystList:
        #Wt channels
        SingleTopWt = "(DatasetNumber==410013 ||DatasetNumber==410014 || DatasetNumber==407021 || DatasetNumber==407019 )"
        SingleTopWtXsec[region] = Systematic("h1L_SingleTopWtXsec", configMgr.weights,configMgr.weights+["( (0.053) * %s + 1 )" % SingleTopWt],configMgr.weights+["( (-0.053) * %s + 1 )" % SingleTopWt], "weight","overallSys")
        #t channels
        SingleTopT = "(DatasetNumber==410011)"
        SingleTopTXsec[region] = Systematic("h1L_SingleTopTXsec", configMgr.weights,configMgr.weights+["( (0.040) * %s + 1 )" % SingleTopT],configMgr.weights+["( (-0.040) * %s + 1 )" % SingleTopT], "weight","overallSys")
        SingleAntiTopT = "(DatasetNumber==410012)"
        SingleAntiTopTXsec[region] = Systematic("h1L_SingleAntiTopTXsec", configMgr.weights,configMgr.weights+["( (0.050) * %s + 1 )" % SingleAntiTopT],configMgr.weights+["( (-0.050) * %s + 1 )" % SingleAntiTopT], "weight","overallSys")
        #s channels
        SingleTopS = "(DatasetNumber==410025)"
        SingleTopSXsec[region] = Systematic("h1L_SingleTopSXsec", configMgr.weights,configMgr.weights+["( (0.037) * %s + 1 )" % SingleTopS],configMgr.weights+["( (-0.037) * %s + 1 )" % SingleTopS], "weight","overallSys")
        SingleAntiTopS  ="(DatasetNumber==410026)"
        SingleAntiTopSXsec[region] = Systematic("h1L_SingleAntiTopSXsec", configMgr.weights,configMgr.weights+["( (0.047) * %s + 1 )" % SingleAntiTopS],configMgr.weights+["( (-0.047) * %s + 1 )" % SingleAntiTopS], "weight","overallSys")
    if "GenDB" in SystList:       
        diboson = "(DatasetNumber==361063 || DatasetNumber==361064 || DatasetNumber==361065 || DatasetNumber==361066 || DatasetNumber==361067 || DatasetNumber==361068 || DatasetNumber==361069 || DatasetNumber==361070 || DatasetNumber==361071 || DatasetNumber==361072 || DatasetNumber==361073 || DatasetNumber==361077 || DatasetNumber==361081 || DatasetNumber==361082 || DatasetNumber==361083 || DatasetNumber==361084 || DatasetNumber==361085 || DatasetNumber==361086  || DatasetNumber==361087 )"
        DBXsec[region] = Systematic("h1L_DBXsec", configMgr.weights,configMgr.weights+["( (0.06) * %s + 1 )" % diboson],configMgr.weights+["( (-0.06) * %s + 1 )" % diboson], "weight","overallSys")
  
    if "GenZjets" in SystList:        
        ZjetsXsec[region] = Systematic("h1L_ZjetsXsec", configMgr.weights,configMgr.weights+["(0.05 + 1 )" ],configMgr.weights+["( -0.05 + 1 )"], "weight","overallSys")	
		


    
'''
old theo uncert for ttbar	
# Theory uncertainties in ttbar
if "GenPowhegJimmyTTbar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_PowhegPythia_PowhegJimmy
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_PowhegPythia_PowhegJimmy.TheorUnc(generatorSyst)
	
# Factorization Scale variation	in ttbar    	
if "FactScaleTTbar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_FactScale_ttbar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_FactScale_ttbar.TheorUnc(generatorSyst)
	
	
# ISR/FSR uncertainty calculated by comparing PowhegPythia with morePS  and lessPS 
if "ISRFSRTTbar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_ISRFSR_ttbar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_ISRFSR_ttbar.TheorUnc(generatorSyst)	
	
# Renormalization Scale variation in ttbar		    
if "RenScaleTTbar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_RenScale_ttbar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_RenScale_ttbar.TheorUnc(generatorSyst)
'''
#new theo uncert for ttbar
# Theory uncertainties in ttbar
# QCD re-normalization and factorization scale and ISR/FSR variation in ttbar
if "FactRenScaleISRFSRTTbar" in SystList:
        import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_FactRenScaleISRFSR_ttbar
        EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_FactRenScaleISRFSR_ttbar.TheorUnc(generatorSyst)
	
# Hadronization/fragmentation, PowhegPythia/PowhegHerwigpp generator differences in ttbar 	
if "HadFragTTbar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_HadFrag_ttbar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_HadFrag_ttbar.TheorUnc(generatorSyst)
	
#Hard scattering generation,PowhegHerwigpp/MC@NLOHerwigpp.
if "HardScatteringGenTTbar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_HardScatteringGen_ttbar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_HardScatteringGen_ttbar.TheorUnc(generatorSyst)	

	
if "WjetsTheo" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_Wjets
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_Wjets.TheorUnc(generatorSyst)	
	
if "WjetsScaleVar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_WjetsScalesVar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_WjetsScalesVar.TheorUnc(generatorSyst)
	
	
if "ZjetsTheo" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_Zjets
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_Zjets.TheorUnc(generatorSyst)	
	
if "ZjetsScaleVar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_ZjetsScalesVar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_ZjetsScalesVar.TheorUnc(generatorSyst)
			
if "DibosonsTheo" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_Dibosons
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_Dibosons.TheorUnc(generatorSyst)	
	
if "DibosonsScaleVar" in SystList:
	import EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_DibosonsScalesVar
	EarlyRun2.Hard1Lepton.theoryUncertainties_hardLepton_DibosonsScalesVar.TheorUnc(generatorSyst)
	


	

# ********************************************************************* #
#                              Background samples
# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.addSampleSpecificWeight(wjets_hackedweight)
#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
ZSample.addSampleSpecificWeight(zjets_hackedweight)
#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList([inputDir+"allTrees_T_04_02.root"])#setFileList([inputDir+"allTrees_T_03_04_data_2_7_fb.root"])
#DataSample.setFileList(["/afs/cern.ch/work/m/mgignac/public/StrongProduction/SUSYAPR/T_03_04/merged/allTrees_T_03_04_data_2_7_fb.root"])
DataSample.setData()

if doOnlySignal: 
    sigSample = Sample(sigSamples[0],kPink)    
    sigSample.setStatConfig(useStat)
    sigSample.setNormByTheory()
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)
    sigSample.setFileList(sigFiles_em[sigSamples[0]])
        
# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")
if not doOnlySignal: bkgOnly.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
else: 
    bkgOnly.addSamples([sigSample,DataSample])
    


if useStat:
    bkgOnly.statErrThreshold=0.0 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.05)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
bReqChans["4Jlowx"] = []
bVetoChans["4Jlowx"] = []
bAgnosticChans["4Jlowx"] = []
bReqChans["4Jhighx"] = []
bVetoChans["4Jhighx"] = []
bAgnosticChans["4Jhighx"] = []
bReqChans["5J"] = []
bVetoChans["5J"] = []
bAgnosticChans["5J"] = []
bReqChans["6J"] = []
bVetoChans["6J"] = []
bAgnosticChans["6J"] = []

#lepton flavor classification of channels
elChans = {}
muChans = {}
elChans["4Jlowx"] = []
muChans["4Jlowx"] = []
elChans["4Jhighx"] = []
muChans["4Jhighx"] = []
elChans["5J"] = []
muChans["5J"] = []
elChans["6J"] = []
muChans["6J"] = []

elmuChans = {}
elmuChans["4Jlowx"] = []
elmuChans["4Jhighx"] = []
elmuChans["5J"] = []
elmuChans["6J"] = []


######################################################
# Add channels to Bkg-only configuration             #
######################################################

#-----3JET--------#
if "4J" in CRregions and not 'lowx' in CRregions and not 'highx' in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JEM"],1,0.5,1.5),[elmuChans["4J"],bVetoChans["4J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JEM"],1,0.5,1.5),[elmuChans["4J"],bReqChans["4J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
if "4Jlowx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JlowxEM"],1,0.5,1.5),[elmuChans["4Jlowx"],bVetoChans["4Jlowx"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JlowxEM"],1,0.5,1.5),[elmuChans["4Jlowx"],bReqChans["4Jlowx"]])
    bkgOnly.setBkgConstrainChannels(tmp)    
if "4Jhighx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JhighxEM"],1,0.5,1.5),[elmuChans["4Jhighx"],bVetoChans["4Jhighx"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JhighxEM"],1,0.5,1.5),[elmuChans["4Jhighx"],bReqChans["4Jhighx"]])
    bkgOnly.setBkgConstrainChannels(tmp)
if "5J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR5JEM"],1,0.5,1.5),[elmuChans["5J"],bVetoChans["5J"]])
    #tmp = appendTo(bkgOnly.addChannel("cuts",["WR5JEM"],1,0.5,1.5), [elmuChans["5J"]] ) # for test
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR5JEM"],1,0.5,1.5),[elmuChans["5J"],bReqChans["5J"]]) 
    #tmp = appendTo(bkgOnly.addChannel("cuts",["TR5JEM"],1,0.5,1.5),[elmuChans["5J"]]) # for test
    bkgOnly.setBkgConstrainChannels(tmp)
if "6J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6JEM"],1,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6JEM"],1,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)    

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        for region in ['4Jlowx','4Jhighx','5J','6J']:
            appendIfMatchName(c,bReqChans[region])
            appendIfMatchName(c,bVetoChans[region])
            appendIfMatchName(c,bAgnosticChans[region])
            appendIfMatchName(c,elChans[region])
            appendIfMatchName(c,muChans[region])      
            appendIfMatchName(c,elmuChans[region])    

    if doTableInputs:
        if "4Jlowx" in CRregions:
            if configMgr.blindVR==True or configMgr.blindSR==False:
                appendTo(validation.addValidationChannel("cuts",["SR4JlowxEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
                appendTo(validation.addValidationChannel("cuts",["SR4JlowxMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
                appendTo(validation.addValidationChannel("cuts",["SR4JlowxEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowx_mtEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowx_mtMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowx_mtEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]]) 
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowx_aplanarityEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowx_aplanarityMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowx_aplanarityEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])                         
        if "4Jhighx" in CRregions:
            if configMgr.blindVR==True or configMgr.blindSR==False:
                appendTo(validation.addValidationChannel("cuts",["SR4JhighxEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
                appendTo(validation.addValidationChannel("cuts",["SR4JhighxMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
                appendTo(validation.addValidationChannel("cuts",["SR4JhighxEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighx_meffEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighx_meffMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighx_meffEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighx_mtEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighx_mtMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighx_mtEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
        if "5J" in CRregions:
            if configMgr.blindVR==True or configMgr.blindSR==False:
                appendTo(validation.addValidationChannel("cuts",["SR5JEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
                appendTo(validation.addValidationChannel("cuts",["SR5JMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
                appendTo(validation.addValidationChannel("cuts",["SR5JEM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_mtEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_mtMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_mtEM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_aplanarityEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_aplanarityMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
            appendTo(validation.addValidationChannel("cuts",["VR5J_aplanarityEM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])            
        if "6J" in CRregions:
            if configMgr.blindVR==True or configMgr.blindSR==False:
                appendTo(validation.addValidationChannel("cuts",["SR6JEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
                appendTo(validation.addValidationChannel("cuts",["SR6JMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
                appendTo(validation.addValidationChannel("cuts",["SR6JEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_mtEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_mtMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_mtEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])            
            appendTo(validation.addValidationChannel("cuts",["VR6J_aplanarityEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_aplanarityMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6J_aplanarityEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
           
    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs 
    if ValidRegList["OneLep"] and not doTableInputs:
        
        if ValidRegList["SR6J"]:
            SR6Jnomt            = validation.addValidationChannel("mt",["SR6JnomtEM"],6,65.0,545.0)
            SR6JnoJetAplanarity = validation.addValidationChannel("JetAplanarity",["SR6JnoJetAplanarityEM"],6,0.0,0.12)
            SR6Jnomt.useOverflowBin=True
            SR6JnoJetAplanarity.useOverflowBin=True
            appendTo(SR6Jnomt,[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(SR6JnoJetAplanarity,[bAgnosticChans["6J"],elmuChans["6J"]])
            
        if ValidRegList["SR5J"]:   
            SR5JnoJetAplanarity = validation.addValidationChannel("JetAplanarity",["SR5JnoJetAplanarityEM"],6,0.0,0.12)
            SR5Jnomt            = validation.addValidationChannel("mt",["SR5JnomtEM"],9,25.0,475.0)
            SR5Jnomeff          = validation.addValidationChannel("meffInc30",["SR5JnomeffEM"],3,1000.0,2200.0)
            SR5JnoJetAplanarity.useOverflowBin=True
            SR5Jnomt.useOverflowBin=True
            SR5Jnomeff.useOverflowBin=True
            appendTo(SR5Jnomt,[bAgnosticChans["5J"],elmuChans["5J"]])  #275 SR cut
            appendTo(SR5JnoJetAplanarity,[bAgnosticChans["5J"],elmuChans["5J"]])
            appendTo(SR5Jnomeff ,[bAgnosticChans["5J"],elmuChans["5J"]]) #1800
            
        if ValidRegList["SR4Jlowx"]:   
            SR4Jlowxnomt            = validation.addValidationChannel("mt",["SR4JlowxnomtEM"],7,65.0,485.0) # Edge at 125 GeV
            SR4JlowxnoJetAplanarity = validation.addValidationChannel("JetAplanarity",["SR4JlowxnoJetAplanarityEM"],6,0.0,0.12) # Edge at 0.04
            SR4Jlowxnomeff          = validation.addValidationChannel("meffInc30",["SR4JlowxnomeffEM"],6,1800.0,3000.0) # Edge at 2000
            SR4Jlowxnomt.useOverflowBin=True
            SR4JlowxnoJetAplanarity.useOverflowBin=True
            SR4Jlowxnomeff.useOverflowBin=True
            appendTo(SR4Jlowxnomt,[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(SR4JlowxnoJetAplanarity,[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(SR4Jlowxnomeff,[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

        if ValidRegList["SR4Jhighx"]:  
            SR4Jhighxnomt = validation.addValidationChannel("mt",["SR4JhighxnomtEM"],6,25.0,625.0) # Edge at 425
            SR4Jhighxnometovermeff = validation.addValidationChannel("met/meffInc30",["SR4JhighxnometovermeffEM"],4,0.1,0.5) # Edge at 0.3
            SR4Jhighxnomt.useOverflowBin=True
            SR4Jhighxnometovermeff.useOverflowBin=True
            appendTo(SR4Jhighxnomt,[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(SR4Jhighxnometovermeff,[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])

        # Validation plots
        # Binning
        # nBins, min, max
        metBinsVR     = (20,   200.,600.)
        meffBinsVR    = (10,   500.,2500.)
        lep1PtBinsVR  = (25,   0.,500.)
        WptBinsVR     = (25,   0.,500.)
        njets         = (10,   0.,10.)
        nbjets         = (10,   0.,10.)
        jet1ptBinsVR  = (24,   50.,500.)
        mtBinsVR      = (10,   25.,525.)
        metmeff =  (20, 0.,1.)
        aplanarity = (10,0.,0.1)
 
        
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        if ValidRegList["VR4Jlowx"] : 
            ProcessRegions.append(["VR4Jlowx_mt" ,bAgnosticChans["4Jlowx"]])
            ProcessRegions.append(["VR4Jlowx_aplanarity" ,bAgnosticChans["4Jlowx"]])    
        if ValidRegList["VR4Jhighx"] : 
            ProcessRegions.append(["VR4Jhighx_mt" ,bAgnosticChans["4Jhighx"]])
            ProcessRegions.append(["VR4Jhighx_meff" ,bAgnosticChans["4Jhighx"]])  
        if ValidRegList["VR5J"] : 
            ProcessRegions.append(["VR5J_mt" ,bAgnosticChans["5J"]])
            ProcessRegions.append(["VR5J_aplanarity" ,bAgnosticChans["5J"]])     
        if ValidRegList["VR6J"] : 
            ProcessRegions.append(["VR6J_mt" ,bAgnosticChans["6J"]])
            ProcessRegions.append(["VR6J_aplanarity" ,bAgnosticChans["6J"]])         
        if ValidRegList["CR4Jlowx"] : 
            ProcessRegions.append(["TR4Jlowx" ,bReqChans["4Jlowx"]])   
            ProcessRegions.append(["WR4Jlowx" ,bVetoChans["4Jlowx"]])   
        if ValidRegList["CR4Jhighx"] : 
            ProcessRegions.append(["TR4Jhighx" ,bReqChans["4Jhighx"]])   
            ProcessRegions.append(["WR4Jhighx" ,bVetoChans["4Jhighx"]])   
        if ValidRegList["CR5J"] : 
            ProcessRegions.append(["TR5J" ,bReqChans["5J"]])   
            ProcessRegions.append(["WR5J" ,bVetoChans["5J"]])     
        if ValidRegList["CR6J"] : 
            ProcessRegions.append(["TR6J" ,bReqChans["6J"]])   
            ProcessRegions.append(["WR6J" ,bVetoChans["6J"]])                 
            
        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            sys_region = ""
            if "4Jlowx" in CutPrefix: sys_region = "4Jlowx"
            elif "4Jhighx" in CutPrefix: sys_region = "4Jhighx"
            elif "5J" in CutPrefix: sys_region = "5J"
            elif "6J" in CutPrefix: sys_region = "6J"
            else: 
                print "Unknown region! - Take systematics from 5J regions."
                sys_region = "5J"
            #for chan in [["El",elChans],["Mu",muChans],["EM",elmuChans]]:
            for chan in [["EM",elmuChans]]:
                ChanSuffix = chan[0]
                FlavorList_pre = chan[1]
                FlavorList = FlavorList_pre[sys_region]
                
                if not 'SR' in CutPrefix:
                    appendTo(validation.addValidationChannel("met/meffInc30",[CutPrefix+ChanSuffix],  metmeff[0],  metmeff[1],  metmeff[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("meffInc30",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("mt"    ,[CutPrefix+ChanSuffix],   mtBinsVR[0],   mtBinsVR[1],   mtBinsVR[2]),[bChanKind,FlavorList])    
                    appendTo(validation.addValidationChannel("jet1Pt"    ,[CutPrefix+ChanSuffix],   jet1ptBinsVR[0],   jet1ptBinsVR[1],   jet1ptBinsVR[2]),[bChanKind,FlavorList])    
                    appendTo(validation.addValidationChannel("nJet30"    ,[CutPrefix+ChanSuffix],   njets[0],   njets[1],   njets[2]),[bChanKind,FlavorList])   
                    appendTo(validation.addValidationChannel("JetAplanarity"    ,[CutPrefix+ChanSuffix],   aplanarity[0],   aplanarity[1],   aplanarity[2]),[bChanKind,FlavorList])
                else:
                    if 'meff' in CutPrefix and not 'metovermeff' in CutPrefix:
                        appendTo(validation.addValidationChannel("meffInc30",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
                    if 'mt' in CutPrefix:
                        appendTo(validation.addValidationChannel("mt"    ,[CutPrefix+ChanSuffix],   mtBinsVR[0],   mtBinsVR[1],   mtBinsVR[2]),[bChanKind,FlavorList])
                    if 'JetAplanarity' in CutPrefix:
                        appendTo(validation.addValidationChannel("JetAplanarity"    ,[CutPrefix+ChanSuffix],   aplanarity[0],   aplanarity[1],   aplanarity[2]),[bChanKind,FlavorList])
                    if 'metovermeff' in CutPrefix:
                        appendTo(validation.addValidationChannel("met/meffInc30",[CutPrefix+ChanSuffix],  metmeff[0],  metmeff[1],  metmeff[2]),[bChanKind,FlavorList])

# ********************************************************************* #
#                              Later: exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    if '4Jlowx' in CRregions:
        SRs=["SR4JlowxEM"]        
    if '4Jhighx' in CRregions:
        SRs=["SR4JhighxEM"]   
    if '5J' in CRregions:
        SRs=["SR5JEM"]
    elif '6J' in CRregions:
        SRs=["SR6JEM"]

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)

        ######### ISR uncertainties #####################
        if '4Jhighx' in CRregions: ERRregion='4Jhx'
        if '4Jlowx'  in CRregions: ERRregion='4Jlx'
        if '5J'  in CRregions: ERRregion='5J'
        if '6J'  in CRregions: ERRregion='6J'
        errsig = SignalAccUncertainties.GetUncertaintyFromName(ERRregion,sig)
        print "sig=",sig,"  region=",ERRregion,"errsig=",errsig
        isrSig = Systematic("SigISR",configMgr.weights,1.00+errsig,1.00-errsig,"user","userOverallSys")
        #################################################
        sigSample.addSystematic(xsecSig)
        #sigSample.mergeOverallSysSet = ["SigXSec","SigISR"]
            
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:           
            if sr=="SR4JlowxEl" or sr=="SR4JlowxMu" or sr=="SR4JlowxEM":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)                    
            elif sr=="SR4JhighxEl" or sr=="SR4JhighxMu" or sr=="SR4JhighxEM":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)               
            elif sr=="SR5JEl" or sr=="SR5JMu" or sr=="SR5JEM":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)                    
            elif sr=="SR6JEl" or sr=="SR6JMu" or sr=="SR6JEM":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)                    
            else:
                raise RuntimeError("Unexpected signal region %s"%sr)
                
            if sr=="SR4JlowxEl": 
                elChans["4Jlowx"].append(ch) 
                bAgnosticChans["4Jlowx"].append(ch)
            elif sr=="SR4JlowxMu": 
                muChans["4Jlowx"].append(ch) 
                bAgnosticChans["4Jlowx"].append(ch)
            elif sr=="SR4JlowxEM":
                elmuChans["4Jlowx"].append(ch)
                bAgnosticChans["4Jlowx"].append(ch)                
            elif sr=="SR4JhighxEl": 
                elChans["4Jhighx"].append(ch) 
                bAgnosticChans["4Jhighx"].append(ch)
            elif sr=="SR4JhighxMu": 
                muChans["4Jhighx"].append(ch) 
                bAgnosticChans["4Jhighx"].append(ch)
            elif sr=="SR4JhighxEM":
                elmuChans["4Jhighx"].append(ch)
                bAgnosticChans["4Jhighx"].append(ch)              
            elif sr=="SR5JEl": 
                elChans["5J"].append(ch) 
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR5JMu": 
                muChans["5J"].append(ch) 
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR5JEM":
                elmuChans["5J"].append(ch)
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR6JEl": 
                elChans["6J"].append(ch) 
                bAgnosticChans["6J"].append(ch)
            elif sr=="SR6JMu": 
                muChans["6J"].append(ch) 
                bAgnosticChans["6J"].append(ch)
            elif sr=="SR6JEM":
                elmuChans["6J"].append(ch)
                bAgnosticChans["6J"].append(ch)               
            else:
                raise RuntimeError("Unexpected signal region %s"%sr)
            pass
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.getSample(sig).addSystematic(isrSig)
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan = tgt[1]
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
    
    
    
if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
        SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
        SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])

##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig) 
            sys_region = ""
            if "4Jlowx" in chan.name: sys_region = "4Jlowx"           
            elif "4Jhighx" in chan.name: sys_region = "4Jhighx"
            elif "5J" in chan.name: sys_region = "5J"
            elif "6J" in chan.name: sys_region = "6J"            
            else: 
                print "Unknown region! - Take systematics from 5J regions."
                sys_region = "5J"
            theSample.removeSystematic("EL_Eff_Iso")
            #theSample.removeSystematic("EL_Eff_Reco")
            #theSample.removeSystematic("EL_Eff_ID")           
            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans["4Jlowx"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["4Jlowx"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["4Jlowx"]:
                theSigFiles = sigFiles_em[sig]               
            elif chan in elChans["4Jhighx"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["4Jhighx"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["4Jhighx"]:
                theSigFiles = sigFiles_em[sig]            
            elif chan in elChans["5J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["5J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["5J"]:
                theSigFiles = sigFiles_em[sig]
            elif chan in elChans["6J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["6J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["6J"]:
                theSigFiles = sigFiles_em[sig]                
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])           

    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])            
            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")

    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            if "GenDB" in SystList:
                chan.getSample("diboson").addSystematic(DBXsec[region])    
            if "GenZjets" in SystList:
                chan.getSample("zjets").addSystematic(ZjetsXsec[region])    
    
            if "GenSingleTop" in SystList:
                chan.getSample("singletop").addSystematic(SingleTopSXsec[region])  
                chan.getSample("singletop").addSystematic(SingleTopWtXsec[region])  
                chan.getSample("singletop").addSystematic(SingleTopTXsec[region])  
                chan.getSample("singletop").addSystematic(SingleAntiTopSXsec[region])  
                chan.getSample("singletop").addSystematic(SingleAntiTopTXsec[region]) 


            chan.getSample("singletop").addSystematic(SingleTopComm[region])
            #chan.getSample("wjets").addSystematic(WjetsComm[region])
            #chan.getSample("diboson").addSystematic(DibosonsComm[region])
            #chan.getSample("zjets").addSystematic(ZjetsComm[region])
            chan.getSample("ttv").addSystematic(ttvComm[region])

            chan.getSample("ttbar").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
            chan.getSample("wjets").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
            #chan.getSample("ttv").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
            #chan.getSample("singletop").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])               
            #chan.getSample("diboson").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
            #chan.getSample("zjets").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])




# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
#leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
entry = leg.AddEntry("","#tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") #only for SR5J
entry.SetLineColor(kMagenta)
entry.SetLineStyle(kDashed)
entry.SetLineWidth(4)
#
entry = leg.AddEntry("","(1385, 705, 25) GeV","l") # not for SR5J
entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    
    if "SR4J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 10.
        #chan.titleY = "Events / 200 GeV"
        #chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR5J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        if 'mt' in chan.name:
            chan.maxY = 25.
        elif 'JetAplanarity' in chan.name:
            chan.maxY = 5.
        else: 
            chan.maxY = 7.
        #chan.titleY = "Events / 200 GeV"
        #chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR6J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 10.
        #chan.titleY = "Events / 100 GeV"
        #chan.titleX = "E^{miss}_{T} [GeV]"      
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
