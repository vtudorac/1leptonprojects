################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

from logger import Logger
log = Logger('HardLepton')

# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return


# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

#check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]
#SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
#SystList.append("pileup")  # pileup (common)
SystList.append("BTag")     #b-tagging uncertainties

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["4J"] #this is the default - modify from command line

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 5J,6J option)"
    
#remove later: warning not to use two towers at the same time at the moment.
if len(CRregions) > 1:
    print "FATAL: CR and SR are not yet orthogonal - don't run with more than one tower at the moment!"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["SR4J"] = False
ValidRegList["VR4J"] = True
ValidRegList["VR4J_extra"] = False
ValidRegList["SR5J"] = False
ValidRegList["VR5J"] = False
ValidRegList["SR6J"] = False
ValidRegList["VR6J"] = False
#for tables (turn doTableInputs to True)
doTableInputs = False #This effectively means no validation plots but only validation tables (but is 100x faster)
for cr in CRregions:
    if "4J" in cr and doTableInputs:
        ValidRegList["VR4J"] = True     

#take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    #sigSamples=["onestepGG_1225_625_25"]
    sigSamples=["tree"]
    
#define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0]:
        analysissuffix += '_GG1step'
        
#in case we want to overlay signal on the plots, we will need to produce signal histograms first. Do this via the following option and running only -t
doOnlySignal=False
if myFitType==FitType.Exclusion:
    doOnlySignal=False
        
# First define HistFactory attributes
configMgr.analysisName = "OneHardLepton_mtfirstdata"+analysissuffix # Name to give the analysis
if doOnlySignal: configMgr.analysisName = "OneHardLepton_mtfirstdata_signal"+analysissuffix
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False # enable the fallback to trees
configMgr.useHistBackupCacheFile = False # enable the use of an alternate data file
configMgr.histBackupCacheFile =  "data/"+configMgr.analysisName+".root" #"/project/etp3/jlorenz/shape_fit/HistFitter_earlyRun2/HistFitterUser/MET_jets_leptons/data/histograms.root" # the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True

# Scaling calculated by outputLumi / inputLumi
#configMgr.inputLumi = 6.630771
#configMgr.outputLumi = 6.630771 
#configMgr.setLumiUnits("pb-1")
configMgr.inputLumi = 1.0
configMgr.outputLumi = 78.38 
configMgr.setLumiUnits("fb-1")

configMgr.fixSigXSec=True
#configMgr.calculatorType=0 #toys
configMgr.calculatorType=2 #asimov
configMgr.testStatType=3
configMgr.nPoints=20

#configMgr.scanRange = (0., 20.) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
#configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#using of statistical uncertainties?
useStat=True

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

#directory with background files:
inputDir="/afs/cern.ch/work/m/mgignac/public/StrongProduction/02_06/EPS/T_02_06/merged/"

#data and QCD:
# (don't have any at the moment)

#signal files
inputDirSig="/afs/cern.ch/work/m/mgignac/public/StrongProduction/02_06/EPS/T_02_06/merged/"


if not onLxplus:
    print "INFO : Running locally...\n"
    # put here paths to your local files if not running on lxplus
    # the following paths are for running locally at LMU
    inputDir="/project/etp3/jlorenz/HistFitterTrees/15Earlydata/"
    inputDirSig="/project/etp3/jlorenz/HistFitterTrees/15Earlydata/"   
else:
    print "INFO : Running on lxplus... \n"
    
#background files, separately for electron and muon channel:
bgdFiles_e = [inputDir+"allTrees_T_02_06.root"] 
bgdFiles_m = [inputDir+"allTrees_T_02_06.root"]
bgdFiles_em = [inputDir+"allTrees_T_02_06.root"]

#signal files
if myFitType==FitType.Exclusion:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"GG_onestepCC_1225_625_25.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"GG_onestepCC_1225_625_25.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"GG_onestepCC_1225_625_25.root"]


# ********************************************************************* #
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&& lep1Pt>35."
OneEleSelection = "&& AnalysisType==1"
OneMuoSelection = "&& AnalysisType==2"
OneLepSelection = "&& ( (AnalysisType==1 && (HLT_e24_lhmedium_iloose_L1EM18VH || HLT_e60_medium)) || (AnalysisType==2 && (HLT_mu24_iloose_L1MU15 || HLT_mu50)) )"

# ------- 4J regions
#configMgr.cutsDict["SR4J"]="nJet30>=4 && jet1Pt>100. && jet2Pt>50. && jet3Pt>50. && jet4Pt>50. && met>350. && mt>350. && (met/meffInc30) > 0.2 && meffInc30>1600." + CommonSelection
configMgr.cutsDict["WR4J_pre"]="nJet30>=4 && jet1Pt>30. && jet2Pt>30. && jet3Pt>30. && jet4Pt>30. && met>100. && mt>40. && mt<100. && nBJet30_MV1==0" + CommonSelection
configMgr.cutsDict["TR4J_pre"]="nJet30>=4 && jet1Pt>30. && jet2Pt>30. && jet3Pt>30. && jet4Pt>30. && met>100. && mt>40. && mt<100. && nBJet30_MV1>0" + CommonSelection
configMgr.cutsDict["VR4J_mt_pre"]="nJet30>=4 && jet1Pt>30. && jet2Pt>30. && jet3Pt>30. && jet4Pt>30. && met>100." + CommonSelection

#second part: splitting into electron and muon channel for validation purposes
d=configMgr.cutsDict
configMgr.cutsDict["WR4JEl"] = d["WR4J_pre"]+OneEleSelection
configMgr.cutsDict["WR4JMu"] = d["WR4J_pre"]+OneMuoSelection
configMgr.cutsDict["TR4JEl"] = d["TR4J_pre"]+OneEleSelection
configMgr.cutsDict["TR4JMu"] = d["TR4J_pre"]+OneMuoSelection
configMgr.cutsDict["VR4J_mtEl"] = d["VR4J_mt_pre"]+OneEleSelection
configMgr.cutsDict["VR4J_mtMu"] = d["VR4J_mt_pre"]+OneMuoSelection
configMgr.cutsDict["WR4J"] = d["WR4J_pre"]+OneLepSelection
configMgr.cutsDict["TR4J"] = d["TR4J_pre"]+OneLepSelection
configMgr.cutsDict["VR4J_mt"] = d["VR4J_mt_pre"]+OneLepSelection


# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #

#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","triggerWeight","bTagWeight"] #+,"pileupWeight"

configMgr.weights = weights
#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonHighWeights_trigger = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_TrigUncertainty__1up")
muonLowWeights_trigger = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_TrigUncertainty__1down")

#electron related uncertainties acting on weights
electronEffHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_TotalCorrUncertainty__1up")
electronEffLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_TotalCorrUncertainty__1down")

#pileup systematics
#pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
#pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

#b-tagging:
if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_Light_systematics__1down")

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}

for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #Example systematic uncertainty
    if "JER" in SystList: 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide"))
    if "JES" in SystList: 
        basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
        basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_MET_SoftTrk_ResoPara","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_MET_SoftTrk_ResoPerp","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSysOneSide"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSysOneSide"))
        basicChanSyst[region].append(Systematic("MUONS_ID","_NoSys","_MUONS_ID__1up","_MUONS_ID__1down","tree","overallNormHistoSysOneSide"))
        basicChanSyst[region].append(Systematic("MUONS_MS","_NoSys","_MUONS_MS__1up","_MUONS_MS__1down","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MUONS_SCALE","_NoSys","_MUONS_SCALE__1up","_MUONS_SCALE__1down","tree","overallNormHistoSysOneSide"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys")) 
        # no trigger uncertainties calculated yet
        #basicChanSyst[region].append(Systematic("MUON_Eff_trigger",configMgr.weights,muonHighWeights_trigger,muonLowWeights_trigger,"weight","overallNormHistoSys"))  
        #following excluded since buggy in tree production
        basicChanSyst[region].append(Systematic("EL_Eff",configMgr.weights,electronEffHighWeights,electronEffLowWeights,"weight","overallNormHistoSys"))         
#    if "pileup" in SystList:
#        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))   
        
    if "BTag" in SystList:
        bTagSyst[region] = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region] = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region] = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")

# ********************************************************************* #
#                              Background samples
# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data_NoSys",kBlack)
DataSample.setFileList([inputDir+"allTrees_T_02_07.root"])
DataSample.setData()
#
if doOnlySignal:
    sigSample = Sample(sigSamples[0],kPink)    
    sigSample.setNormByTheory()
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)
    sigSample.setStatConfig(useStat)
    sigSample.setFileList([inputDirSig+"GG_onestepCC_1225_625_25_v5c.root"])

# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")
#bkgOnly.addSamples([ttbarVSample,DibosonsSample,SingleTopSample,ZSample,TTbarSample,WSample,DataSample])
if not doOnlySignal: bkgOnly.addSamples([ZSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
else: bkgOnly.addSamples([sigSample])

if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.09)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
bReqChans["4J"] = []
bVetoChans["4J"] = []
bAgnosticChans["4J"] = []

#lepton flavor classification of channels
elChans = {}
muChans = {}
elChans["4J"] = []
muChans["4J"] = []

elmuChans = {}
elmuChans["4J"] = []


######################################################
# Add channels to Bkg-only configuration             #
######################################################

#-----3JET--------#
if "4J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4J"],1,0.5,1.5),[elmuChans["4J"],bVetoChans["4J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4J"],1,0.5,1.5),[elmuChans["4J"],bReqChans["4J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #
#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]  = ValidRegList["VR4J"] + ValidRegList["VR4J_extra"]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans["4J"])
        appendIfMatchName(c,bVetoChans["4J"])
        appendIfMatchName(c,bAgnosticChans["4J"])
        appendIfMatchName(c,elChans["4J"])
        appendIfMatchName(c,muChans["4J"])      
        appendIfMatchName(c,elmuChans["4J"])
     

    if "4J" in CRregions:
        appendTo(validation.addValidationChannel("mt",["VR4J_mtEl"],12,0.,600.),[bAgnosticChans["4J"],elChans["4J"]])
        appendTo(validation.addValidationChannel("mt",["VR4J_mtMu"],12,0.,600.),[bAgnosticChans["4J"],muChans["4J"]])
        appendTo(validation.addValidationChannel("mt",["VR4J_mt"],12,0.,600.),[bAgnosticChans["4J"],elmuChans["4J"]])   
     
    if ValidRegList["VR4J_extra"]:

        metBinsVR     = (20,   200.,600.)
        meffBinsVR    = (15,   500.,2000.)
        metphiBins  = (14,   -3.5,3.5)
        njets         = (10,   0.,10.)
        nbjets         = (10,   0.,10.)
        mtBinsVR      = (23,   40.,500.)
        metmeff =  (20, 0.,1.)
        
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        ProcessRegions.append(["VR4J_mt",bAgnosticChans["4J"]]) 
        ProcessRegions.append(["TR4J",bReqChans["4J"]])   
        ProcessRegions.append(["WR4J",bVetoChans["4J"]])      

        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            sys_region = ""
            if "4J" in CutPrefix: sys_region = "4J"
            else: 
                print "Unknown region! - Take systematics from 4J regions."
                sys_region = "4J"
            for chan in [["El",elChans],["Mu",muChans],["",elmuChans]]:
                ChanSuffix = chan[0]
                FlavorList_pre = chan[1]
                FlavorList = FlavorList_pre[sys_region]
                appendTo(validation.addValidationChannel("met/meffInc30",[CutPrefix+ChanSuffix],  metmeff[0],  metmeff[1],  metmeff[2]),    [bChanKind,FlavorList])
                appendTo(validation.addValidationChannel("meffInc30",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
                appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
                appendTo(validation.addValidationChannel("nBJet30_MV1"    ,[CutPrefix+ChanSuffix],   nbjets[0],   nbjets[1],   nbjets[2]),[bChanKind,FlavorList])
                appendTo(validation.addValidationChannel("nJet30"    ,[CutPrefix+ChanSuffix],   njets[0],   njets[1],   njets[2]),[bChanKind,FlavorList])
                appendTo(validation.addValidationChannel("metPhi"    ,[CutPrefix+ChanSuffix],   metphiBins[0],   metphiBins[1],   metphiBins[2]),[bChanKind,FlavorList])
                #appendTo(validation.addValidationChannel("mt"    ,[CutPrefix+ChanSuffix],   mtBinsVR[0],   mtBinsVR[1],   mtBinsVR[2]),[bChanKind,FlavorList])

           
    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs       


# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]

for region in CRregions:
    SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
    SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
    SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])

    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")


    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            chan.getSample("ttbar").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
            chan.getSample("wjets").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
            #chan.getSample("ttv").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
            chan.getSample("singletop").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])               
            #chan.getSample("diboson").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
            chan.getSample("zjets").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])
        else:
            sigSample.setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
#leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","ttbarV","lf")
#entry.SetLineColor(ttbarVSample.color)
#entry.SetFillColor(ttbarVSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") #only for SR5J
entry.SetLineColor(kMagenta)
entry.SetLineStyle(kDashed)
entry.SetLineWidth(4)
#
entry = leg.AddEntry("","(1225, 625, 25) GeV","l") # not for SR5J
entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    
    if "SR3J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 70.
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR5J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 18.5
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR6J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 25.5
        chan.titleY = "Events / 100 GeV"
        chan.titleX = "E^{miss}_{T} [GeV]"    
    elif "VR4J_mt" in chan.name:
        if chan.logY:
            chan.maxY = 3000
        else:
            chan.maxY = 1000
        chan.titleY = "Events / 50 GeV"
        chan.titleX = "m_{T} [GeV]"    
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
