## Access signal acceptance uncertainties
## as a function of gluino, chargino and LSP mass
## The numbers are the result of a sim in quadrature of various uncertainty components summarised at
## https://svnweb.cern.ch/cern/wsvn/atlasphys-susy/Physics/SUSY/Analyses/StopSbottom/MC15JobOptionsExample/tags/MC15JobOptionsExample-00-00-07/README

import ROOT
import math

infile = ROOT.TFile("/afs/cern.ch/work/d/dxu/public/HistFitter-2015/HistFitterUser/MET_jets_leptons/python/EarlyRun2/Hard1Lepton/SignalAcc_unc.root")
infile_mlsp60 = ROOT.TFile("/afs/cern.ch/work/d/dxu/public/HistFitter-2015/HistFitterUser/MET_jets_leptons/python/EarlyRun2/Hard1Lepton/SignalAcc_unc_mlsp60.root")

knownregions=['5J','6J','4Jhx','4Jlx','soft_SR2J','soft_SR5J']



def GetSusyParsFromName(name):
    '''
    in case of gtt the second parameter corresponds to mstop
    '''
    tokens = name.split('_')
    if 'onestepGG' in name:
        mgluino=float(tokens[1])
        mchargino=float(tokens[2])
        mlsp=float(tokens[3])
    else:
        print "unknown signalsample name"
    x = (mchargino-mlsp)/(mgluino-mlsp)
    
    return (mgluino,mchargino,mlsp,x)


def distance( p0, p1 ):
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

def GetUncertaintyFromName(signalregion, massstring):
    mgl,mch,mlsp,x = GetSusyParsFromName(massstring)
    return GetUncertainty(signalregion, mgl, mch, mlsp  )

def GetUncertainty(signalregion, mgluino, mchargino, mlsp):
    grid = 'x12'
    x = float((mchargino-mlsp))/(mgluino-mlsp)
    if mlsp==60 and ((mchargino-mlsp)/(mgluino-mlsp) != 0.5):
        grid = 'mlsp60'
    
    if signalregion not in knownregions:
        print "{0} not known. Please choose among:".format(signalregion)
        print knownregions
        return 0.

    if grid == 'mlsp60':
        interpolation = infile_mlsp60.Get("interpolation_{0}".format(signalregion))
        binnr = interpolation.FindBin(mgluino, x)
        var = interpolation.GetBinContent(binnr)
        if var!= 0:
            print "Returning unc. for mlsp60 grid: {0}".format(var)
            return var

        ## Requested variation outside the interpolation area.
        ## Find the closest non 0 bin
        print "WARNING: Requested point {0}:{1} lies outside the evaluated phase-space area. Returning closest point inside".format(mgluino, x)
        minds=999999.
        for bx in range(1, interpolation.GetNbinsX()+1):
            for by in range(1, interpolation.GetNbinsY()+1):

                x = interpolation.GetXaxis().GetBinCenter(bx)
                y = interpolation.GetYaxis().GetBinCenter(by)
                binnr = interpolation.FindBin(x, y)
                tmpvar = interpolation.GetBinContent(binnr)
                if tmpvar==0: continue
                
                tmpd = distance( (x,y), (mgluino, x) )
                if tmpd< minds:
                    minds = tmpd
                    var = tmpvar
        print "Closest point is {0} away in mgl-x space and found variation is {1}".format(minds, var)
        return var
        
    if grid == 'x12':
        interpolation = infile.Get("interpolation_{0}".format(signalregion))

        binnr = interpolation.FindBin(mgluino, mlsp)
        var = interpolation.GetBinContent(binnr)
        if var!= 0:
            print "Returning unc. for x12 grid: {0}".format(var)
            return var

        ## Requested variation outside the interpolation area.
        ## Find the closest non 0 bin
        print "WARNING: Requested point {0}:{1} lies outside the evaluated phase-space area. Returning closest point inside".format(mgluino, mlsp)
        minds=999999.
        for bx in range(1, interpolation.GetNbinsX()+1):
            for by in range(1, interpolation.GetNbinsY()+1):

                x = interpolation.GetXaxis().GetBinCenter(bx)
                y = interpolation.GetYaxis().GetBinCenter(by)
                binnr = interpolation.FindBin(x, y)
                tmpvar = interpolation.GetBinContent(binnr)
                if tmpvar==0: continue
                
                tmpd = distance( (x,y), (mgluino,mlsp) )
                if tmpd< minds:
                    minds = tmpd
                    var = tmpvar
        print "Closest point is {0} away in mgl-mlsp space and found variation is {1}".format(minds, var)
        return var
