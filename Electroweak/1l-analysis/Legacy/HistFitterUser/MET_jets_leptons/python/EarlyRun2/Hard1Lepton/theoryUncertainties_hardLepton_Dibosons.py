import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

DibosonsTheoSR4Jlowx = Systematic("Dibosons_SR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.34)"],configMgr.weights+["( 1-0.34 )"], "weight","overallSys")
DibosonsTheoSR4Jhighx = Systematic("Dibosons_SR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.30)"],configMgr.weights+["( 1-0.30 )"], "weight","overallSys")
DibosonsTheoSR5J = Systematic("Dibosons_SR5J",configMgr.weights,configMgr.weights+["( 1+0.23)"],configMgr.weights+["( 1-0.23 )"], "weight","overallSys")
DibosonsTheoSR6J = Systematic("Dibosons_SR6J",configMgr.weights,configMgr.weights+["( 1+0.30)"],configMgr.weights+["( 1-0.30 )"], "weight","overallSys")

DibosonsTheoWR4Jlowx = Systematic("Dibosons_WR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")
DibosonsTheoWR4Jhighx = Systematic("Dibosons_WR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.28)"],configMgr.weights+["( 1-0.28 )"], "weight","overallSys")
DibosonsTheoWR5J = Systematic("Dibosons_WR5J",configMgr.weights,configMgr.weights+["( 1+0.20)"],configMgr.weights+["( 1-0.20 )"], "weight","overallSys")
DibosonsTheoWR6J = Systematic("Dibosons_WR6J",configMgr.weights,configMgr.weights+["( 1+0.30)"],configMgr.weights+["( 1-0.30 )"], "weight","overallSys")

DibosonsTheoTR4Jlowx = Systematic("Dibosons_TR4Jlowx",configMgr.weights,1.22 ,0.78 ,"user","userOverallSys")
DibosonsTheoTR4Jhighx = Systematic("Dibosons_TR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.28)"],configMgr.weights+["( 1-0.28 )"], "weight","overallSys")
DibosonsTheoTR5J = Systematic("Dibosons_TR5J",configMgr.weights,configMgr.weights+["( 1+0.20)"],configMgr.weights+["( 1-0.20 )"], "weight","overallSys")
DibosonsTheoTR6J = Systematic("Dibosons_TR6J",configMgr.weights,configMgr.weights+["( 1+0.30)"],configMgr.weights+["( 1-0.30 )"], "weight","overallSys")


DibosonsTheoVR4Jlowx_mt = Systematic("Dibosons_VR4Jlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")
DibosonsTheoVR4Jlowx_aplanarity = Systematic("Dibosons_VR4Jlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")

DibosonsTheoVR4Jhighx_mt = Systematic("Dibosons_VR4Jhighx_mt",configMgr.weights,configMgr.weights+["( 1+0.28)"],configMgr.weights+["( 1-0.28 )"], "weight","overallSys")
DibosonsTheoVR4Jhighx_meff = Systematic("Dibosons_VR4Jhighx_meff",configMgr.weights,configMgr.weights+["( 1+0.28)"],configMgr.weights+["( 1-0.28 )"], "weight","overallSys")


DibosonsTheoVR5J_mt = Systematic("Dibosons_VR5J_mt",configMgr.weights,configMgr.weights+["( 1+0.20)"],configMgr.weights+["( 1-0.20 )"], "weight","overallSys")
DibosonsTheoVR5J_aplanarity = Systematic("Dibosons_VR5J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.20)"],configMgr.weights+["( 1-0.20 )"], "weight","overallSys")

DibosonsTheoVR6J_mt = Systematic("Dibosons_VR6J_mt",configMgr.weights,configMgr.weights+["( 1+0.30)"],configMgr.weights+["( 1-0.30 )"], "weight","overallSys")
DibosonsTheoVR6J_aplanarity= Systematic("Dibosons_VR6J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.30)"],configMgr.weights+["( 1-0.30 )"], "weight","overallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("diboson","SR4JhighxnomtEM"), DibosonsTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JhighxnometovermeffEM"), DibosonsTheoSR4Jhighx))

    generatorSyst.append((("diboson","SR4JlowxnomtEM"), DibosonsTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JlowxnomeffEM"), DibosonsTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JlowxnoJetAplanarityEM"), DibosonsTheoSR4Jlowx))

    generatorSyst.append((("diboson","SR5JnomtEM"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR5JnomeffEM"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR5JnoJetAplanarityEM"), DibosonsTheoSR5J))

    generatorSyst.append((("diboson","SR6JnomtEM"), DibosonsTheoSR6J))
    generatorSyst.append((("diboson","SR6JnoJetAplanarityEM"), DibosonsTheoSR6J))

    ###############################################################################
    generatorSyst.append((("diboson","SR4JhighxEl"), DibosonsTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JhighxMu"), DibosonsTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JlowxEl"), DibosonsTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JlowxMu"), DibosonsTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JhighxEM"),DibosonsTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JlowxEM"), DibosonsTheoSR4Jlowx)) 
    generatorSyst.append((("diboson","SR5JEl"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR5JMu"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR5JEM"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR6JEl"), DibosonsTheoSR6J))
    generatorSyst.append((("diboson","SR6JMu"), DibosonsTheoSR6J))
    generatorSyst.append((("diboson","SR6JEM"), DibosonsTheoSR6J))
    ###############################################################################
    generatorSyst.append((("diboson","WR4JhighxEl"), DibosonsTheoWR4Jhighx))
    generatorSyst.append((("diboson","WR4JhighxMu"), DibosonsTheoWR4Jhighx))
    generatorSyst.append((("diboson","WR4JlowxEl"), DibosonsTheoWR4Jlowx))
    generatorSyst.append((("diboson","WR4JlowxMu"), DibosonsTheoWR4Jlowx))
    generatorSyst.append((("diboson","WR4JhighxEM"),DibosonsTheoWR4Jhighx))
    generatorSyst.append((("diboson","WR4JlowxEM"), DibosonsTheoWR4Jlowx)) 
    generatorSyst.append((("diboson","WR5JEl"), DibosonsTheoWR5J))
    generatorSyst.append((("diboson","WR5JMu"), DibosonsTheoWR5J))
    generatorSyst.append((("diboson","WR5JEM"), DibosonsTheoWR5J))
    generatorSyst.append((("diboson","WR6JEl"), DibosonsTheoWR6J))
    generatorSyst.append((("diboson","WR6JMu"), DibosonsTheoWR6J))
    generatorSyst.append((("diboson","WR6JEM"), DibosonsTheoWR6J))
    ###############################################################################
    generatorSyst.append((("diboson","TR4JhighxEl"), DibosonsTheoTR4Jhighx))
    generatorSyst.append((("diboson","TR4JhighxMu"), DibosonsTheoTR4Jhighx))
    generatorSyst.append((("diboson","TR4JlowxEl"), DibosonsTheoTR4Jlowx))
    generatorSyst.append((("diboson","TR4JlowxMu"), DibosonsTheoTR4Jlowx))
    generatorSyst.append((("diboson","TR4JhighxEM"),DibosonsTheoTR4Jhighx))
    generatorSyst.append((("diboson","TR4JlowxEM"), DibosonsTheoTR4Jlowx)) 
    generatorSyst.append((("diboson","TR5JEl"), DibosonsTheoTR5J))
    generatorSyst.append((("diboson","TR5JMu"), DibosonsTheoTR5J))
    generatorSyst.append((("diboson","TR5JEM"), DibosonsTheoTR5J))
    generatorSyst.append((("diboson","TR6JEl"), DibosonsTheoTR6J))
    generatorSyst.append((("diboson","TR6JMu"), DibosonsTheoTR6J))
    generatorSyst.append((("diboson","TR6JEM"), DibosonsTheoTR6J))
    ###############################################################################
    
    generatorSyst.append((("diboson","VR4Jlowx_mtEl"), DibosonsTheoVR4Jlowx_mt))
    generatorSyst.append((("diboson","VR4Jlowx_mtMu"), DibosonsTheoVR4Jlowx_mt))
    generatorSyst.append((("diboson","VR4Jlowx_mtEM"), DibosonsTheoVR4Jlowx_mt))
    generatorSyst.append((("diboson","VR4Jlowx_aplanarityEl"), DibosonsTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("diboson","VR4Jlowx_aplanarityMu"), DibosonsTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("diboson","VR4Jlowx_aplanarityEM"), DibosonsTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("diboson","VR4Jhighx_meffEl"), DibosonsTheoVR4Jhighx_meff))
    generatorSyst.append((("diboson","VR4Jhighx_meffMu"), DibosonsTheoVR4Jhighx_meff))
    generatorSyst.append((("diboson","VR4Jhighx_meffEM"), DibosonsTheoVR4Jhighx_meff))
    generatorSyst.append((("diboson","VR4Jhighx_mtEl"), DibosonsTheoVR4Jhighx_mt))
    generatorSyst.append((("diboson","VR4Jhighx_mtMu"), DibosonsTheoVR4Jhighx_mt))
    generatorSyst.append((("diboson","VR4Jhighx_mtEM"), DibosonsTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("diboson","VR5J_mtEl"), DibosonsTheoVR5J_mt))
    generatorSyst.append((("diboson","VR5J_mtMu"), DibosonsTheoVR5J_mt))
    generatorSyst.append((("diboson","VR5J_mtEM"), DibosonsTheoVR5J_mt))
    generatorSyst.append((("diboson","VR5J_aplanarityEl"), DibosonsTheoVR5J_aplanarity))
    generatorSyst.append((("diboson","VR5J_aplanarityMu"), DibosonsTheoVR5J_aplanarity))
    generatorSyst.append((("diboson","VR5J_aplanarityEM"), DibosonsTheoVR5J_aplanarity))
    
    generatorSyst.append((("diboson","VR6J_mtEl"), DibosonsTheoVR6J_mt))
    generatorSyst.append((("diboson","VR6J_mtMu"), DibosonsTheoVR6J_mt))
    generatorSyst.append((("diboson","VR6J_mtEM"), DibosonsTheoVR6J_mt))
    generatorSyst.append((("diboson","VR6J_aplanarityEl"), DibosonsTheoVR6J_aplanarity))
    generatorSyst.append((("diboson","VR6J_aplanarityMu"), DibosonsTheoVR6J_aplanarity))
    generatorSyst.append((("diboson","VR6J_aplanarityEM"), DibosonsTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
