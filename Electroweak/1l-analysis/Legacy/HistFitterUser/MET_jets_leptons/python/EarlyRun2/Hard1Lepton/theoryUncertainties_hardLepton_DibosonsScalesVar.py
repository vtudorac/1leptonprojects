import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

DibosonsScalesVarTheoSR4Jlowx = Systematic("DibosonsScalesVar_SR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
DibosonsScalesVarTheoSR4Jhighx = Systematic("DibosonsScalesVar_SR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsScalesVarTheoSR5J = Systematic("DibosonsScalesVar_SR5J",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsScalesVarTheoSR6J = Systematic("DibosonsScalesVar_SR6J",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")

DibosonsScalesVarTheoWR4Jlowx = Systematic("DibosonsScalesVar_WR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsScalesVarTheoWR4Jhighx = Systematic("DibosonsScalesVar_WR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")
DibosonsScalesVarTheoWR5J = Systematic("DibosonsScalesVar_WR5J",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")
DibosonsScalesVarTheoWR6J = Systematic("DibosonsScalesVar_WR6J",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")

DibosonsScalesVarTheoTR4Jlowx = Systematic("DibosonsScalesVar_TR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsScalesVarTheoTR4Jhighx = Systematic("DibosonsScalesVar_TR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03)"], "weight","overallSys")
DibosonsScalesVarTheoTR5J = Systematic("DibosonsScalesVar_TR5J",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")
DibosonsScalesVarTheoTR6J = Systematic("DibosonsScalesVar_TR6J",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")


DibosonsScalesVarTheoVR4Jlowx_mt = Systematic("DibosonsScalesVar_VR4Jlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsScalesVarTheoVR4Jlowx_aplanarity = Systematic("DibosonsScalesVar_VR4Jlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")

DibosonsScalesVarTheoVR4Jhighx_mt = Systematic("DibosonsScalesVar_VR4Jhighx_mt",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")
DibosonsScalesVarTheoVR4Jhighx_meff = Systematic("DibosonsScalesVar_VR4Jhighx_meff",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")


DibosonsScalesVarTheoVR5J_mt = Systematic("DibosonsScalesVar_VR5J_mt",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")
DibosonsScalesVarTheoVR5J_aplanarity = Systematic("DibosonsScalesVar_VR5J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")

DibosonsScalesVarTheoVR6J_mt = Systematic("DibosonsScalesVar_VR6J_mt",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsScalesVarTheoVR6J_aplanarity= Systematic("DibosonsScalesVar_VR6J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("diboson","SR4JhighxnomtEM"), DibosonsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JhighxnometovermeffEM"), DibosonsScalesVarTheoSR4Jhighx))

    generatorSyst.append((("diboson","SR4JlowxnomtEM"), DibosonsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JlowxnomeffEM"), DibosonsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JlowxnoJetAplanarityEM"), DibosonsScalesVarTheoSR4Jlowx))

    generatorSyst.append((("diboson","SR5JnomtEM"), DibosonsScalesVarTheoSR5J))
    generatorSyst.append((("diboson","SR5JnomeffEM"), DibosonsScalesVarTheoSR5J))
    generatorSyst.append((("diboson","SR5JnoJetAplanarityEM"), DibosonsScalesVarTheoSR5J))

    generatorSyst.append((("diboson","SR6JnomtEM"), DibosonsScalesVarTheoSR6J))
    generatorSyst.append((("diboson","SR6JnoJetAplanarityEM"), DibosonsScalesVarTheoSR6J))

    ###############################################################################
    generatorSyst.append((("diboson","SR4JhighxEl"), DibosonsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JhighxMu"), DibosonsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JlowxEl"), DibosonsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JlowxMu"), DibosonsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("diboson","SR4JhighxEM"),DibosonsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("diboson","SR4JlowxEM"), DibosonsScalesVarTheoSR4Jlowx)) 
    generatorSyst.append((("diboson","SR5JEl"), DibosonsScalesVarTheoSR5J))
    generatorSyst.append((("diboson","SR5JMu"), DibosonsScalesVarTheoSR5J))
    generatorSyst.append((("diboson","SR5JEM"), DibosonsScalesVarTheoSR5J))
    generatorSyst.append((("diboson","SR6JEl"), DibosonsScalesVarTheoSR6J))
    generatorSyst.append((("diboson","SR6JMu"), DibosonsScalesVarTheoSR6J))
    generatorSyst.append((("diboson","SR6JEM"), DibosonsScalesVarTheoSR6J))
    ###############################################################################
    generatorSyst.append((("diboson","WR4JhighxEl"), DibosonsScalesVarTheoWR4Jhighx))
    generatorSyst.append((("diboson","WR4JhighxMu"), DibosonsScalesVarTheoWR4Jhighx))
    generatorSyst.append((("diboson","WR4JlowxEl"), DibosonsScalesVarTheoWR4Jlowx))
    generatorSyst.append((("diboson","WR4JlowxMu"), DibosonsScalesVarTheoWR4Jlowx))
    generatorSyst.append((("diboson","WR4JhighxEM"),DibosonsScalesVarTheoWR4Jhighx))
    generatorSyst.append((("diboson","WR4JlowxEM"), DibosonsScalesVarTheoWR4Jlowx)) 
    generatorSyst.append((("diboson","WR5JEl"), DibosonsScalesVarTheoWR5J))
    generatorSyst.append((("diboson","WR5JMu"), DibosonsScalesVarTheoWR5J))
    generatorSyst.append((("diboson","WR5JEM"), DibosonsScalesVarTheoWR5J))
    generatorSyst.append((("diboson","WR6JEl"), DibosonsScalesVarTheoWR6J))
    generatorSyst.append((("diboson","WR6JMu"), DibosonsScalesVarTheoWR6J))
    generatorSyst.append((("diboson","WR6JEM"), DibosonsScalesVarTheoWR6J))
    ###############################################################################
    generatorSyst.append((("diboson","TR4JhighxEl"), DibosonsScalesVarTheoTR4Jhighx))
    generatorSyst.append((("diboson","TR4JhighxMu"), DibosonsScalesVarTheoTR4Jhighx))
    generatorSyst.append((("diboson","TR4JlowxEl"), DibosonsScalesVarTheoTR4Jlowx))
    generatorSyst.append((("diboson","TR4JlowxMu"), DibosonsScalesVarTheoTR4Jlowx))
    generatorSyst.append((("diboson","TR4JhighxEM"),DibosonsScalesVarTheoTR4Jhighx))
    generatorSyst.append((("diboson","TR4JlowxEM"), DibosonsScalesVarTheoTR4Jlowx)) 
    generatorSyst.append((("diboson","TR5JEl"), DibosonsScalesVarTheoTR5J))
    generatorSyst.append((("diboson","TR5JMu"), DibosonsScalesVarTheoTR5J))
    generatorSyst.append((("diboson","TR5JEM"), DibosonsScalesVarTheoTR5J))
    generatorSyst.append((("diboson","TR6JEl"), DibosonsScalesVarTheoTR6J))
    generatorSyst.append((("diboson","TR6JMu"), DibosonsScalesVarTheoTR6J))
    generatorSyst.append((("diboson","TR6JEM"), DibosonsScalesVarTheoTR6J))
    ###############################################################################
    
    
    generatorSyst.append((("diboson","VR4Jlowx_mtEl"), DibosonsScalesVarTheoVR4Jlowx_mt))
    generatorSyst.append((("diboson","VR4Jlowx_mtMu"), DibosonsScalesVarTheoVR4Jlowx_mt))
    generatorSyst.append((("diboson","VR4Jlowx_mtEM"), DibosonsScalesVarTheoVR4Jlowx_mt))
    generatorSyst.append((("diboson","VR4Jlowx_aplanarityEl"), DibosonsScalesVarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("diboson","VR4Jlowx_aplanarityMu"), DibosonsScalesVarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("diboson","VR4Jlowx_aplanarityEM"), DibosonsScalesVarTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("diboson","VR4Jhighx_meffEl"), DibosonsScalesVarTheoVR4Jhighx_meff))
    generatorSyst.append((("diboson","VR4Jhighx_meffMu"), DibosonsScalesVarTheoVR4Jhighx_meff))
    generatorSyst.append((("diboson","VR4Jhighx_meffEM"), DibosonsScalesVarTheoVR4Jhighx_meff))
    generatorSyst.append((("diboson","VR4Jhighx_mtEl"), DibosonsScalesVarTheoVR4Jhighx_mt))
    generatorSyst.append((("diboson","VR4Jhighx_mtMu"), DibosonsScalesVarTheoVR4Jhighx_mt))
    generatorSyst.append((("diboson","VR4Jhighx_mtEM"), DibosonsScalesVarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("diboson","VR5J_mtEl"), DibosonsScalesVarTheoVR5J_mt))
    generatorSyst.append((("diboson","VR5J_mtMu"), DibosonsScalesVarTheoVR5J_mt))
    generatorSyst.append((("diboson","VR5J_mtEM"), DibosonsScalesVarTheoVR5J_mt))
    generatorSyst.append((("diboson","VR5J_aplanarityEl"), DibosonsScalesVarTheoVR5J_aplanarity))
    generatorSyst.append((("diboson","VR5J_aplanarityMu"), DibosonsScalesVarTheoVR5J_aplanarity))
    generatorSyst.append((("diboson","VR5J_aplanarityEM"), DibosonsScalesVarTheoVR5J_aplanarity))
    
    generatorSyst.append((("diboson","VR6J_mtEl"), DibosonsScalesVarTheoVR6J_mt))
    generatorSyst.append((("diboson","VR6J_mtMu"), DibosonsScalesVarTheoVR6J_mt))
    generatorSyst.append((("diboson","VR6J_mtEM"), DibosonsScalesVarTheoVR6J_mt))
    generatorSyst.append((("diboson","VR6J_aplanarityEl"), DibosonsScalesVarTheoVR6J_aplanarity))
    generatorSyst.append((("diboson","VR6J_aplanarityMu"), DibosonsScalesVarTheoVR6J_aplanarity))
    generatorSyst.append((("diboson","VR6J_aplanarityEM"), DibosonsScalesVarTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
