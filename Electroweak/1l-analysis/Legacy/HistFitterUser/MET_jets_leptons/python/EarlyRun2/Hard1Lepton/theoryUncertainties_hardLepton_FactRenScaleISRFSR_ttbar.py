import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

FactRenScaleISRFSRTheoSR4Jlowx = Systematic("FactRenScaleISRFSR_SR4Jlowx",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")
FactRenScaleISRFSRTheoSR4Jhighx = Systematic("FactRenScaleISRFSR_SR4Jhighx",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
FactRenScaleISRFSRTheoSR5J = Systematic("FactRenScaleISRFSR_SR5J",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
FactRenScaleISRFSRTheoSR6J = Systematic("FactRenScaleISRFSR_SR6J",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")


FactRenScaleISRFSRTheoVR4Jlowx_mt = Systematic("FactRenScaleISRFSR_VR4Jlowx_mt",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
FactRenScaleISRFSRTheoVR4Jlowx_aplanarity = Systematic("FactRenScaleISRFSR_VR4Jlowx_aplanarity",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")

FactRenScaleISRFSRTheoVR4Jhighx_mt = Systematic("FactRenScaleISRFSR_VR4Jhighx_mt",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
FactRenScaleISRFSRTheoVR4Jhighx_meff = Systematic("FactRenScaleISRFSR_VR4Jhighx_meff",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")


FactRenScaleISRFSRTheoVR5J_mt = Systematic("FactRenScaleISRFSR_VR5J_mt",configMgr.weights,1.08,0.92 ,"user","userOverallSys")
FactRenScaleISRFSRTheoVR5J_aplanarity = Systematic("FactRenScaleISRFSR_VR5J_aplanarity",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")

FactRenScaleISRFSRTheoVR6J_mt = Systematic("FactRenScaleISRFSR_VR6J_mt",configMgr.weights,1.08 ,0.92 ,"user","userOverallSys")
FactRenScaleISRFSRTheoVR6J_aplanarity= Systematic("FactRenScaleISRFSR_VR6J_aplanarity",configMgr.weights,1.01 ,0.98 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("ttbar","SR4JhighxnomtEM"), FactRenScaleISRFSRTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxnometovermeffEM"), FactRenScaleISRFSRTheoSR4Jhighx))

    generatorSyst.append((("ttbar","SR4JlowxnomtEM"), FactRenScaleISRFSRTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxnomeffEM"), FactRenScaleISRFSRTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxnoJetAplanarityEM"), FactRenScaleISRFSRTheoSR4Jlowx))

    generatorSyst.append((("ttbar","SR5JnomtEM"), FactRenScaleISRFSRTheoSR5J))
    generatorSyst.append((("ttbar","SR5JnomeffEM"), FactRenScaleISRFSRTheoSR5J))
    generatorSyst.append((("ttbar","SR5JnoJetAplanarityEM"), FactRenScaleISRFSRTheoSR5J))

    generatorSyst.append((("ttbar","SR6JnomtEM"), FactRenScaleISRFSRTheoSR6J))
    generatorSyst.append((("ttbar","SR6JnoJetAplanarityEM"), FactRenScaleISRFSRTheoSR6J))

    ###############################################################################
    generatorSyst.append((("ttbar","SR4JhighxEl"), FactRenScaleISRFSRTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), FactRenScaleISRFSRTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), FactRenScaleISRFSRTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), FactRenScaleISRFSRTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),FactRenScaleISRFSRTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), FactRenScaleISRFSRTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), FactRenScaleISRFSRTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), FactRenScaleISRFSRTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), FactRenScaleISRFSRTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), FactRenScaleISRFSRTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), FactRenScaleISRFSRTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), FactRenScaleISRFSRTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), FactRenScaleISRFSRTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), FactRenScaleISRFSRTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), FactRenScaleISRFSRTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), FactRenScaleISRFSRTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), FactRenScaleISRFSRTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), FactRenScaleISRFSRTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), FactRenScaleISRFSRTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), FactRenScaleISRFSRTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), FactRenScaleISRFSRTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), FactRenScaleISRFSRTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), FactRenScaleISRFSRTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), FactRenScaleISRFSRTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), FactRenScaleISRFSRTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), FactRenScaleISRFSRTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), FactRenScaleISRFSRTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), FactRenScaleISRFSRTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), FactRenScaleISRFSRTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), FactRenScaleISRFSRTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), FactRenScaleISRFSRTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), FactRenScaleISRFSRTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), FactRenScaleISRFSRTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), FactRenScaleISRFSRTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), FactRenScaleISRFSRTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), FactRenScaleISRFSRTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
