import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#FactScaleTTbarTheoSR4J = Systematic("FactScaleTTbar_4J",configMgr.weights,1.0168 ,0.9833 ,"user","userOverallSys")

#FactScaleTTbarTheoSR5J = Systematic("FactScaleTTbar_5J",configMgr.weights,1.0195 ,0.983 ,"user","userOverallSys")

#FactScaleTTbarTheoSR6J = Systematic("FactScaleTTbar_6J",configMgr.weights,1.0543 ,0.9527 ,"user","userOverallSys")

FactScaleTTbarTheoSR4Jlowx = Systematic("FactScaleTTbar_SR4Jlowx",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
FactScaleTTbarTheoSR4Jhighx = Systematic("FactScaleTTbar_SR4Jhighx",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
FactScaleTTbarTheoSR5J = Systematic("FactScaleTTbar_SR5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
FactScaleTTbarTheoSR6J = Systematic("FactScaleTTbar_SR6J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")


FactScaleTTbarTheoVR4Jlowx_mt = Systematic("FactScaleTTbar_VR4Jlowx_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
FactScaleTTbarTheoVR4Jlowx_aplanarity = Systematic("FactScaleTTbar_VR4Jlowx_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")

FactScaleTTbarTheoVR4Jhighx_mt = Systematic("FactScaleTTbar_VR4Jhighx_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
FactScaleTTbarTheoVR4Jhighx_meff = Systematic("FactScaleTTbar_VR4Jhighx_meff",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")


FactScaleTTbarTheoVR5J_mt = Systematic("FactScaleTTbar_VR5J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
FactScaleTTbarTheoVR5J_aplanarity = Systematic("FactScaleTTbar_VR5J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")

FactScaleTTbarTheoVR6J_mt = Systematic("FactScaleTTbar_VR6J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
FactScaleTTbarTheoVR6J_aplanarity= Systematic("FactScaleTTbar_VR6J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR4JhighxEl"), FactScaleTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), FactScaleTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), FactScaleTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), FactScaleTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),FactScaleTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), FactScaleTTbarTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), FactScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), FactScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), FactScaleTTbarTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), FactScaleTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), FactScaleTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), FactScaleTTbarTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), FactScaleTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), FactScaleTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), FactScaleTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), FactScaleTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), FactScaleTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), FactScaleTTbarTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), FactScaleTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), FactScaleTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), FactScaleTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), FactScaleTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), FactScaleTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), FactScaleTTbarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), FactScaleTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), FactScaleTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), FactScaleTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), FactScaleTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), FactScaleTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), FactScaleTTbarTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), FactScaleTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), FactScaleTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), FactScaleTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), FactScaleTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), FactScaleTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), FactScaleTTbarTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
