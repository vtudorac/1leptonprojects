import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

HadFragTheoSR4Jlowx = Systematic("HadFrag_SR4Jlowx",configMgr.weights,1.12 ,0.88 ,"user","userOverallSys")
HadFragTheoSR4Jhighx = Systematic("HadFrag_SR4Jhighx",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
HadFragTheoSR5J = Systematic("HadFrag_SR5J",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
HadFragTheoSR6J = Systematic("HadFrag_SR6J",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")


HadFragTheoVR4Jlowx_mt = Systematic("HadFrag_VR4Jlowx_mt",configMgr.weights,1.02,0.98 ,"user","userOverallSys")
HadFragTheoVR4Jlowx_aplanarity = Systematic("HadFrag_VR4Jlowx_aplanarity",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")

HadFragTheoVR4Jhighx_mt = Systematic("HadFrag_VR4Jhighx_mt",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
HadFragTheoVR4Jhighx_meff = Systematic("HadFrag_VR4Jhighx_meff",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")


HadFragTheoVR5J_mt = Systematic("HadFrag_VR5J_mt",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")
HadFragTheoVR5J_aplanarity = Systematic("HadFrag_VR5J_aplanarity",configMgr.weights,1.08 ,0.92 ,"user","userOverallSys")

HadFragTheoVR6J_mt = Systematic("HadFrag_VR6J_mt",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
HadFragTheoVR6J_aplanarity= Systematic("HadFrag_VR6J_aplanarity",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##
    generatorSyst.append((("ttbar","SR4JhighxnomtEM"), HadFragTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxnometovermeffEM"), HadFragTheoSR4Jhighx))
    
    generatorSyst.append((("ttbar","SR4JlowxnomtEM"), HadFragTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxnomeffEM"), HadFragTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxnoJetAplanarityEM"), HadFragTheoSR4Jlowx))

    generatorSyst.append((("ttbar","SR5JnomtEM"), HadFragTheoSR5J))
    generatorSyst.append((("ttbar","SR5JnomeffEM"), HadFragTheoSR5J))
    generatorSyst.append((("ttbar","SR5JnoJetAplanarityEM"), HadFragTheoSR5J))

    generatorSyst.append((("ttbar","SR6JnomtEM"), HadFragTheoSR6J))
    generatorSyst.append((("ttbar","SR6JnoJetAplanarityEM"), HadFragTheoSR6J))

    #####################################################################
    generatorSyst.append((("ttbar","SR4JhighxEl"), HadFragTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), HadFragTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), HadFragTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), HadFragTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),HadFragTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), HadFragTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), HadFragTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), HadFragTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), HadFragTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), HadFragTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), HadFragTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), HadFragTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), HadFragTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), HadFragTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), HadFragTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), HadFragTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), HadFragTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), HadFragTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), HadFragTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), HadFragTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), HadFragTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), HadFragTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), HadFragTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), HadFragTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), HadFragTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), HadFragTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), HadFragTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), HadFragTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), HadFragTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), HadFragTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), HadFragTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), HadFragTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), HadFragTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), HadFragTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), HadFragTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), HadFragTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
