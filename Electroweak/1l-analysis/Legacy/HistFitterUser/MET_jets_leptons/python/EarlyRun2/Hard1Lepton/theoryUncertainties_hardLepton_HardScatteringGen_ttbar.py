import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

HardScatteringGenTheoSR4Jlowx = Systematic("HardScatteringGen_SR4Jlowx",configMgr.weights,1.23 ,0.77 ,"user","userOverallSys")
HardScatteringGenTheoSR4Jhighx = Systematic("HardScatteringGen_SR4Jhighx",configMgr.weights,1.08 ,0.92 ,"user","userOverallSys")
HardScatteringGenTheoSR5J = Systematic("HardScatteringGen_SR5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
HardScatteringGenTheoSR6J = Systematic("HardScatteringGen_SR6J",configMgr.weights,1.17 ,0.83 ,"user","userOverallSys")


HardScatteringGenTheoVR4Jlowx_mt = Systematic("HardScatteringGen_VR4Jlowx_mt",configMgr.weights,1.23 ,0.77 ,"user","userOverallSys")
HardScatteringGenTheoVR4Jlowx_aplanarity = Systematic("HardScatteringGen_VR4Jlowx_aplanarity",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")

HardScatteringGenTheoVR4Jhighx_mt = Systematic("HardScatteringGen_VR4Jhighx_mt",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
HardScatteringGenTheoVR4Jhighx_meff = Systematic("HardScatteringGen_VR4Jhighx_meff",configMgr.weights,1.03,0.97 ,"user","userOverallSys")


HardScatteringGenTheoVR5J_mt = Systematic("HardScatteringGen_VR5J_mt",configMgr.weights,1.02 ,0.98 ,"user","userOverallSys")
HardScatteringGenTheoVR5J_aplanarity = Systematic("HardScatteringGen_VR5J_aplanarity",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")

HardScatteringGenTheoVR6J_mt = Systematic("HardScatteringGen_VR6J_mt",configMgr.weights,1.11 ,0.89 ,"user","userOverallSys")
HardScatteringGenTheoVR6J_aplanarity= Systematic("HardScatteringGen_VR6J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("ttbar","SR4JhighxnomtEM"), HardScatteringGenTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxnometovermeffEM"), HardScatteringGenTheoSR4Jhighx))

    generatorSyst.append((("ttbar","SR4JlowxnomtEM"), HardScatteringGenTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxnomeffEM"), HardScatteringGenTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxnoJetAplanarityEM"), HardScatteringGenTheoSR4Jlowx))

    generatorSyst.append((("ttbar","SR5JnomtEM"), HardScatteringGenTheoSR5J))
    generatorSyst.append((("ttbar","SR5JnomeffEM"), HardScatteringGenTheoSR5J))
    generatorSyst.append((("ttbar","SR5JnoJetAplanarityEM"), HardScatteringGenTheoSR5J))

    generatorSyst.append((("ttbar","SR6JnomtEM"), HardScatteringGenTheoSR6J))
    generatorSyst.append((("ttbar","SR6JnoJetAplanarityEM"), HardScatteringGenTheoSR6J))
    ##################################################################################
   
    generatorSyst.append((("ttbar","SR4JhighxEl"), HardScatteringGenTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), HardScatteringGenTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), HardScatteringGenTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), HardScatteringGenTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),HardScatteringGenTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), HardScatteringGenTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), HardScatteringGenTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), HardScatteringGenTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), HardScatteringGenTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), HardScatteringGenTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), HardScatteringGenTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), HardScatteringGenTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), HardScatteringGenTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), HardScatteringGenTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), HardScatteringGenTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), HardScatteringGenTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), HardScatteringGenTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), HardScatteringGenTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), HardScatteringGenTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), HardScatteringGenTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), HardScatteringGenTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), HardScatteringGenTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), HardScatteringGenTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), HardScatteringGenTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), HardScatteringGenTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), HardScatteringGenTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), HardScatteringGenTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), HardScatteringGenTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), HardScatteringGenTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), HardScatteringGenTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), HardScatteringGenTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), HardScatteringGenTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), HardScatteringGenTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), HardScatteringGenTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), HardScatteringGenTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), HardScatteringGenTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
