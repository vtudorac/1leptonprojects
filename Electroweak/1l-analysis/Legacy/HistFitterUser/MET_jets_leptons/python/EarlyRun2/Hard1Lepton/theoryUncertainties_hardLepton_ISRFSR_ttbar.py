import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#ISRFSRTTbarTheoSR4J = Systematic("ISRFSRTTbar_4J",configMgr.weights,1.0189 ,0.9827 ,"user","userOverallSys")

#ISRFSRTTbarTheoSR5J = Systematic("ISRFSRTTbar_5J",configMgr.weights,1.0276 ,0.9751 ,"user","userOverallSys")

#ISRFSRTTbarTheoSR6J = Systematic("ISRFSRTTbar_6J",configMgr.weights,1.0605 ,0.9429 ,"user","userOverallSys")


ISRFSRTTbarTheoSR4Jlowx = Systematic("ISRFSRTTbar_SR4Jlowx",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")
ISRFSRTTbarTheoSR4Jhighx = Systematic("ISRFSRTTbar_SR4Jhighx",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
ISRFSRTTbarTheoSR5J = Systematic("ISRFSRTTbar_SR5J",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
ISRFSRTTbarTheoSR6J = Systematic("ISRFSRTTbar_SR6J",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")


ISRFSRTTbarTheoVR4Jlowx_mt = Systematic("ISRFSRTTbar_VR4Jlowx_mt",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
ISRFSRTTbarTheoVR4Jlowx_aplanarity = Systematic("ISRFSRTTbar_VR4Jlowx_aplanarity",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")

ISRFSRTTbarTheoVR4Jhighx_mt = Systematic("ISRFSRTTbar_VR4Jhighx_mt",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
ISRFSRTTbarTheoVR4Jhighx_meff = Systematic("ISRFSRTTbar_VR4Jhighx_meff",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")


ISRFSRTTbarTheoVR5J_mt = Systematic("ISRFSRTTbar_VR5J_mt",configMgr.weights,1.08 ,0.92 ,"user","userOverallSys")
ISRFSRTTbarTheoVR5J_aplanarity = Systematic("ISRFSRTTbar_VR5J_aplanarity",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")

ISRFSRTTbarTheoVR6J_mt = Systematic("ISRFSRTTbar_VR6J_mt",configMgr.weights,1.08 ,0.92 ,"user","userOverallSys")
ISRFSRTTbarTheoVR6J_aplanarity= Systematic("ISRFSRTTbar_VR6J_aplanarity",configMgr.weights,1.02 ,0.98 ,"user","userOverallSys")





def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR4JhighxEl"), ISRFSRTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), ISRFSRTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), ISRFSRTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), ISRFSRTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),ISRFSRTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), ISRFSRTTbarTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), ISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), ISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), ISRFSRTTbarTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), ISRFSRTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), ISRFSRTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), ISRFSRTTbarTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), ISRFSRTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), ISRFSRTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), ISRFSRTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), ISRFSRTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), ISRFSRTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), ISRFSRTTbarTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), ISRFSRTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), ISRFSRTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), ISRFSRTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), ISRFSRTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), ISRFSRTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), ISRFSRTTbarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), ISRFSRTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), ISRFSRTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), ISRFSRTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), ISRFSRTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), ISRFSRTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), ISRFSRTTbarTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), ISRFSRTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), ISRFSRTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), ISRFSRTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), ISRFSRTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), ISRFSRTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), ISRFSRTTbarTheoVR6J_aplanarity))
    
    
    
    
    
    
   

  
    return generatorSyst
