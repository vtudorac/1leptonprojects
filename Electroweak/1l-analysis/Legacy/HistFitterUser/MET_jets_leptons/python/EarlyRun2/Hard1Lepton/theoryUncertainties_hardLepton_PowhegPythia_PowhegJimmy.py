import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#PowhegJimmyTTbarTheoSR4J = Systematic("PowhegJimmyTTbar_4J",configMgr.weights,1.0153 ,0.9851 ,"user","userOverallSys")

#PowhegJimmyTTbarTheoSR5J = Systematic("PowhegJimmyTTbar_5J",configMgr.weights,1.0182 ,0.9818 ,"user","userOverallSys")

#PowhegJimmyTTbarTheoSR6J = Systematic("PowhegJimmyTTbar_6J",configMgr.weights,1.0507 ,0.9536 ,"user","userOverallSys")


PowhegJimmyTTbarTheoSR4Jlowx = Systematic("PowhegJimmyTTbar_SR4Jlowx",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
PowhegJimmyTTbarTheoSR4Jhighx = Systematic("PowhegJimmyTTbar_SR4Jhighx",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
PowhegJimmyTTbarTheoSR5J = Systematic("PowhegJimmyTTbar_SR5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
PowhegJimmyTTbarTheoSR6J = Systematic("PowhegJimmyTTbar_SR6J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")


PowhegJimmyTTbarTheoVR4Jlowx_mt = Systematic("PowhegJimmyTTbar_VR4Jlowx_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR4Jlowx_aplanarity = Systematic("PowhegJimmyTTbar_VR4Jlowx_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")

PowhegJimmyTTbarTheoVR4Jhighx_mt = Systematic("PowhegJimmyTTbar_VR4Jhighx_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR4Jhighx_meff = Systematic("PowhegJimmyTTbar_VR4Jhighx_meff",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")


PowhegJimmyTTbarTheoVR5J_mt = Systematic("PowhegJimmyTTbar_VR5J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR5J_aplanarity = Systematic("PowhegJimmyTTbar_VR5J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")

PowhegJimmyTTbarTheoVR6J_mt = Systematic("PowhegJimmyTTbar_VR6J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR6J_aplanarity= Systematic("PowhegJimmyTTbar_VR6J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR4JhighxEl"), PowhegJimmyTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), PowhegJimmyTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), PowhegJimmyTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), PowhegJimmyTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),PowhegJimmyTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), PowhegJimmyTTbarTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), PowhegJimmyTTbarTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), PowhegJimmyTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), PowhegJimmyTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), PowhegJimmyTTbarTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), PowhegJimmyTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), PowhegJimmyTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), PowhegJimmyTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), PowhegJimmyTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), PowhegJimmyTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), PowhegJimmyTTbarTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), PowhegJimmyTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), PowhegJimmyTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), PowhegJimmyTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), PowhegJimmyTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), PowhegJimmyTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), PowhegJimmyTTbarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), PowhegJimmyTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), PowhegJimmyTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), PowhegJimmyTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), PowhegJimmyTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), PowhegJimmyTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), PowhegJimmyTTbarTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), PowhegJimmyTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), PowhegJimmyTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), PowhegJimmyTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), PowhegJimmyTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), PowhegJimmyTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), PowhegJimmyTTbarTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
