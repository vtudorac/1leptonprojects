import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#RenScaleTTbarTheoSR4J = Systematic("RenScaleTTbar_4J",configMgr.weights,1.0170 ,0.9832 ,"user","userOverallSys")

#RenScaleTTbarTheoSR5J = Systematic("RenScaleTTbar_5J",configMgr.weights,1.0189 ,0.9829 ,"user","userOverallSys")

#RenScaleTTbarTheoSR6J = Systematic("RenScaleTTbar_6J",configMgr.weights,1.0536 ,0.9522 ,"user","userOverallSys")

RenScaleTTbarTheoSR4Jlowx = Systematic("RenScaleTTbar_SR4Jlowx",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
RenScaleTTbarTheoSR4Jhighx = Systematic("RenScaleTTbar_SR4Jhighx",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
RenScaleTTbarTheoSR5J = Systematic("RenScaleTTbar_SR5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
RenScaleTTbarTheoSR6J = Systematic("RenScaleTTbar_SR6J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")


RenScaleTTbarTheoVR4Jlowx_mt = Systematic("RenScaleTTbar_VR4Jlowx_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
RenScaleTTbarTheoVR4Jlowx_aplanarity = Systematic("RenScaleTTbar_VR4Jlowx_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")

RenScaleTTbarTheoVR4Jhighx_mt = Systematic("RenScaleTTbar_VR4Jhighx_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
RenScaleTTbarTheoVR4Jhighx_meff = Systematic("RenScaleTTbar_VR4Jhighx_meff",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")


RenScaleTTbarTheoVR5J_mt = Systematic("RenScaleTTbar_VR5J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
RenScaleTTbarTheoVR5J_aplanarity = Systematic("RenScaleTTbar_VR5J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")

RenScaleTTbarTheoVR6J_mt = Systematic("RenScaleTTbar_VR6J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
RenScaleTTbarTheoVR6J_aplanarity= Systematic("RenScaleTTbar_VR6J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR4JhighxEl"), RenScaleTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JhighxMu"), RenScaleTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEl"), RenScaleTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JlowxMu"), RenScaleTTbarTheoSR4Jlowx))
    generatorSyst.append((("ttbar","SR4JhighxEM"),RenScaleTTbarTheoSR4Jhighx))
    generatorSyst.append((("ttbar","SR4JlowxEM"), RenScaleTTbarTheoSR4Jhighx)) 
        
    generatorSyst.append((("ttbar","SR5JEl"), RenScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), RenScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JEM"), RenScaleTTbarTheoSR5J))
   
    generatorSyst.append((("ttbar","SR6JEl"), RenScaleTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JMu"), RenScaleTTbarTheoSR6J))
    generatorSyst.append((("ttbar","SR6JEM"), RenScaleTTbarTheoSR6J))
    
    
    generatorSyst.append((("ttbar","VR4Jlowx_mtEl"), RenScaleTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtMu"), RenScaleTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_mtEM"), RenScaleTTbarTheoVR4Jlowx_mt))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEl"), RenScaleTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityMu"), RenScaleTTbarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4Jlowx_aplanarityEM"), RenScaleTTbarTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("ttbar","VR4Jhighx_meffEl"), RenScaleTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffMu"), RenScaleTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_meffEM"), RenScaleTTbarTheoVR4Jhighx_meff))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEl"), RenScaleTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtMu"), RenScaleTTbarTheoVR4Jhighx_mt))
    generatorSyst.append((("ttbar","VR4Jhighx_mtEM"), RenScaleTTbarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("ttbar","VR5J_mtEl"), RenScaleTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtMu"), RenScaleTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_mtEM"), RenScaleTTbarTheoVR5J_mt))
    generatorSyst.append((("ttbar","VR5J_aplanarityEl"), RenScaleTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityMu"), RenScaleTTbarTheoVR5J_aplanarity))
    generatorSyst.append((("ttbar","VR5J_aplanarityEM"), RenScaleTTbarTheoVR5J_aplanarity))
    
    generatorSyst.append((("ttbar","VR6J_mtEl"), RenScaleTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtMu"), RenScaleTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_mtEM"), RenScaleTTbarTheoVR6J_mt))
    generatorSyst.append((("ttbar","VR6J_aplanarityEl"), RenScaleTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityMu"), RenScaleTTbarTheoVR6J_aplanarity))
    generatorSyst.append((("ttbar","VR6J_aplanarityEM"), RenScaleTTbarTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
