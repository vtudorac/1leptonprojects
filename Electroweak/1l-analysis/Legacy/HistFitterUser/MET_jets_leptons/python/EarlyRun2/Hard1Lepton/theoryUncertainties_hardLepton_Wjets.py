import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

WjetsTheoSR4Jlowx = Systematic("Wjets_SR4Jlowx",configMgr.weights,1.39 ,0.61 ,"user","userOverallSys")
WjetsTheoSR4Jhighx = Systematic("Wjets_SR4Jhighx",configMgr.weights,1.30 ,0.70 ,"user","userOverallSys")
WjetsTheoSR5J = Systematic("Wjets_SR5J",configMgr.weights,1.29 ,0.71 ,"user","userOverallSys")
WjetsTheoSR6J = Systematic("Wjets_SR6J",configMgr.weights,1.30 ,0.70 ,"user","userOverallSys")


WjetsTheoVR4Jlowx_mt = Systematic("Wjets_VR4Jlowx_mt",configMgr.weights,1.09 ,0.91 ,"user","userOverallSys")
WjetsTheoVR4Jlowx_aplanarity = Systematic("Wjets_VR4Jlowx_aplanarity",configMgr.weights,1.24 ,0.76 ,"user","userOverallSys")

WjetsTheoVR4Jhighx_mt = Systematic("Wjets_VR4Jhighx_mt",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")
WjetsTheoVR4Jhighx_meff = Systematic("Wjets_VR4Jhighx_meff",configMgr.weights,1.29 ,0.71 ,"user","userOverallSys")


WjetsTheoVR5J_mt = Systematic("Wjets_VR5J_mt",configMgr.weights,1.21,0.79 ,"user","userOverallSys")
WjetsTheoVR5J_aplanarity = Systematic("Wjets_VR5J_aplanarity",configMgr.weights,1.10 ,0.90 ,"user","userOverallSys")

WjetsTheoVR6J_mt = Systematic("Wjets_VR6J_mt",configMgr.weights,1.30,0.70 ,"user","userOverallSys")
WjetsTheoVR6J_aplanarity= Systematic("Wjets_VR6J_aplanarity",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("wjets","SR4JhighxnomtEM"), WjetsTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JhighxnometovermeffEM"), WjetsTheoSR4Jhighx))

    generatorSyst.append((("wjets","SR4JlowxnomtEM"), WjetsTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JlowxnomeffEM"), WjetsTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JlowxnoJetAplanarityEM"), WjetsTheoSR4Jlowx))

    generatorSyst.append((("wjets","SR5JnomtEM"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR5JnomeffEM"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR5JnoJetAplanarityEM"), WjetsTheoSR5J))

    generatorSyst.append((("wjets","SR6JnomtEM"), WjetsTheoSR6J))
    generatorSyst.append((("wjets","SR6JnoJetAplanarityEM"), WjetsTheoSR6J))

    ###############################################################################
    generatorSyst.append((("wjets","SR4JhighxEl"), WjetsTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JhighxMu"), WjetsTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JlowxEl"), WjetsTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JlowxMu"), WjetsTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JhighxEM"),WjetsTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JlowxEM"), WjetsTheoSR4Jhighx)) 
        
    generatorSyst.append((("wjets","SR5JEl"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR5JMu"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR5JEM"), WjetsTheoSR5J))
   
    generatorSyst.append((("wjets","SR6JEl"), WjetsTheoSR6J))
    generatorSyst.append((("wjets","SR6JMu"), WjetsTheoSR6J))
    generatorSyst.append((("wjets","SR6JEM"), WjetsTheoSR6J))
    
    
    generatorSyst.append((("wjets","VR4Jlowx_mtEl"), WjetsTheoVR4Jlowx_mt))
    generatorSyst.append((("wjets","VR4Jlowx_mtMu"), WjetsTheoVR4Jlowx_mt))
    generatorSyst.append((("wjets","VR4Jlowx_mtEM"), WjetsTheoVR4Jlowx_mt))
    generatorSyst.append((("wjets","VR4Jlowx_aplanarityEl"), WjetsTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("wjets","VR4Jlowx_aplanarityMu"), WjetsTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("wjets","VR4Jlowx_aplanarityEM"), WjetsTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("wjets","VR4Jhighx_meffEl"), WjetsTheoVR4Jhighx_meff))
    generatorSyst.append((("wjets","VR4Jhighx_meffMu"), WjetsTheoVR4Jhighx_meff))
    generatorSyst.append((("wjets","VR4Jhighx_meffEM"), WjetsTheoVR4Jhighx_meff))
    generatorSyst.append((("wjets","VR4Jhighx_mtEl"), WjetsTheoVR4Jhighx_mt))
    generatorSyst.append((("wjets","VR4Jhighx_mtMu"), WjetsTheoVR4Jhighx_mt))
    generatorSyst.append((("wjets","VR4Jhighx_mtEM"), WjetsTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("wjets","VR5J_mtEl"), WjetsTheoVR5J_mt))
    generatorSyst.append((("wjets","VR5J_mtMu"), WjetsTheoVR5J_mt))
    generatorSyst.append((("wjets","VR5J_mtEM"), WjetsTheoVR5J_mt))
    generatorSyst.append((("wjets","VR5J_aplanarityEl"), WjetsTheoVR5J_aplanarity))
    generatorSyst.append((("wjets","VR5J_aplanarityMu"), WjetsTheoVR5J_aplanarity))
    generatorSyst.append((("wjets","VR5J_aplanarityEM"), WjetsTheoVR5J_aplanarity))
    
    generatorSyst.append((("wjets","VR6J_mtEl"), WjetsTheoVR6J_mt))
    generatorSyst.append((("wjets","VR6J_mtMu"), WjetsTheoVR6J_mt))
    generatorSyst.append((("wjets","VR6J_mtEM"), WjetsTheoVR6J_mt))
    generatorSyst.append((("wjets","VR6J_aplanarityEl"), WjetsTheoVR6J_aplanarity))
    generatorSyst.append((("wjets","VR6J_aplanarityMu"), WjetsTheoVR6J_aplanarity))
    generatorSyst.append((("wjets","VR6J_aplanarityEM"), WjetsTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
