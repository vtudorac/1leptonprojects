import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

WJetsScaleVarTheoSR4Jlowx = Systematic("WJetsScaleVar_SR4Jlowx",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")
WJetsScaleVarTheoSR4Jhighx = Systematic("WJetsScaleVar_SR4Jhighx",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
WJetsScaleVarTheoSR5J = Systematic("WJetsScaleVar_SR5J",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
WJetsScaleVarTheoSR6J = Systematic("WJetsScaleVar_SR6J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")


WJetsScaleVarTheoVR4Jlowx_mt = Systematic("WJetsScaleVar_VR4Jlowx_mt",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
WJetsScaleVarTheoVR4Jlowx_aplanarity = Systematic("WJetsScaleVar_VR4Jlowx_aplanarity",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")

WJetsScaleVarTheoVR4Jhighx_mt = Systematic("WJetsScaleVar_VR4Jhighx_mt",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
WJetsScaleVarTheoVR4Jhighx_meff = Systematic("WJetsScaleVar_VR4Jhighx_meff",configMgr.weights,1.05,0.95 ,"user","userOverallSys")


WJetsScaleVarTheoVR5J_mt = Systematic("WJetsScaleVar_VR5J_mt",configMgr.weights,1.05,0.95 ,"user","userOverallSys")
WJetsScaleVarTheoVR5J_aplanarity = Systematic("WJetsScaleVar_VR5J_aplanarity",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")

WJetsScaleVarTheoVR6J_mt = Systematic("WJetsScaleVar_VR6J_mt",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
WJetsScaleVarTheoVR6J_aplanarity= Systematic("WJetsScaleVar_VR6J_aplanarity",configMgr.weights,1.11,0.89 ,"user","userOverallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("wjets","SR4JhighxnomtEM"), WJetsScaleVarTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JhighxnometovermeffEM"), WJetsScaleVarTheoSR4Jhighx))

    generatorSyst.append((("wjets","SR4JlowxnomtEM"), WJetsScaleVarTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JlowxnomeffEM"), WJetsScaleVarTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JlowxnoJetAplanarityEM"), WJetsScaleVarTheoSR4Jlowx))

    generatorSyst.append((("wjets","SR5JnomtEM"), WJetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR5JnomeffEM"), WJetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR5JnoJetAplanarityEM"), WJetsScaleVarTheoSR5J))

    generatorSyst.append((("wjets","SR6JnomtEM"), WJetsScaleVarTheoSR6J))
    generatorSyst.append((("wjets","SR6JnoJetAplanarityEM"), WJetsScaleVarTheoSR6J))

    ###############################################################################
    generatorSyst.append((("wjets","SR4JhighxEl"), WJetsScaleVarTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JhighxMu"), WJetsScaleVarTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JlowxEl"), WJetsScaleVarTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JlowxMu"), WJetsScaleVarTheoSR4Jlowx))
    generatorSyst.append((("wjets","SR4JhighxEM"),WJetsScaleVarTheoSR4Jhighx))
    generatorSyst.append((("wjets","SR4JlowxEM"), WJetsScaleVarTheoSR4Jhighx)) 
        
    generatorSyst.append((("wjets","SR5JEl"), WJetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR5JMu"), WJetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR5JEM"), WJetsScaleVarTheoSR5J))
   
    generatorSyst.append((("wjets","SR6JEl"), WJetsScaleVarTheoSR6J))
    generatorSyst.append((("wjets","SR6JMu"), WJetsScaleVarTheoSR6J))
    generatorSyst.append((("wjets","SR6JEM"), WJetsScaleVarTheoSR6J))
    
    
    generatorSyst.append((("wjets","VR4Jlowx_mtEl"), WJetsScaleVarTheoVR4Jlowx_mt))
    generatorSyst.append((("wjets","VR4Jlowx_mtMu"), WJetsScaleVarTheoVR4Jlowx_mt))
    generatorSyst.append((("wjets","VR4Jlowx_mtEM"), WJetsScaleVarTheoVR4Jlowx_mt))
    generatorSyst.append((("wjets","VR4Jlowx_aplanarityEl"), WJetsScaleVarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("wjets","VR4Jlowx_aplanarityMu"), WJetsScaleVarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("wjets","VR4Jlowx_aplanarityEM"), WJetsScaleVarTheoVR4Jlowx_aplanarity))
    
       
   
    
    generatorSyst.append((("wjets","VR4Jhighx_meffEl"), WJetsScaleVarTheoVR4Jhighx_meff))
    generatorSyst.append((("wjets","VR4Jhighx_meffMu"), WJetsScaleVarTheoVR4Jhighx_meff))
    generatorSyst.append((("wjets","VR4Jhighx_meffEM"), WJetsScaleVarTheoVR4Jhighx_meff))
    generatorSyst.append((("wjets","VR4Jhighx_mtEl"), WJetsScaleVarTheoVR4Jhighx_mt))
    generatorSyst.append((("wjets","VR4Jhighx_mtMu"), WJetsScaleVarTheoVR4Jhighx_mt))
    generatorSyst.append((("wjets","VR4Jhighx_mtEM"), WJetsScaleVarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("wjets","VR5J_mtEl"), WJetsScaleVarTheoVR5J_mt))
    generatorSyst.append((("wjets","VR5J_mtMu"), WJetsScaleVarTheoVR5J_mt))
    generatorSyst.append((("wjets","VR5J_mtEM"), WJetsScaleVarTheoVR5J_mt))
    generatorSyst.append((("wjets","VR5J_aplanarityEl"), WJetsScaleVarTheoVR5J_aplanarity))
    generatorSyst.append((("wjets","VR5J_aplanarityMu"), WJetsScaleVarTheoVR5J_aplanarity))
    generatorSyst.append((("wjets","VR5J_aplanarityEM"), WJetsScaleVarTheoVR5J_aplanarity))
    
    generatorSyst.append((("wjets","VR6J_mtEl"), WJetsScaleVarTheoVR6J_mt))
    generatorSyst.append((("wjets","VR6J_mtMu"), WJetsScaleVarTheoVR6J_mt))
    generatorSyst.append((("wjets","VR6J_mtEM"), WJetsScaleVarTheoVR6J_mt))
    generatorSyst.append((("wjets","VR6J_aplanarityEl"), WJetsScaleVarTheoVR6J_aplanarity))
    generatorSyst.append((("wjets","VR6J_aplanarityMu"), WJetsScaleVarTheoVR6J_aplanarity))
    generatorSyst.append((("wjets","VR6J_aplanarityEM"), WJetsScaleVarTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
