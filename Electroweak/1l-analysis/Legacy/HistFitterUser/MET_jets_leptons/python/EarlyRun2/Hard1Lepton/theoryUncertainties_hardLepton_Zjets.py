import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

ZjetsTheoSR4Jlowx = Systematic("ZjetsTheo_SR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.26)"],configMgr.weights+["( 1-0.26 )"], "weight","overallSys")
ZjetsTheoSR4Jhighx = Systematic("ZjetsTheo_SR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22)"], "weight","overallSys")
ZjetsTheoSR5J = Systematic("ZjetsTheo_SR5J",configMgr.weights,configMgr.weights+["( 1+0.19)"],configMgr.weights+["( 1-0.19 )"], "weight","overallSys")
ZjetsTheoSR6J = Systematic("ZjetsTheo_SR6J",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")

ZjetsTheoWR4Jlowx = Systematic("ZjetsTheo_WR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.26)"],configMgr.weights+["( 1-0.26 )"], "weight","overallSys")
ZjetsTheoWR4Jhighx = Systematic("ZjetsTheo_WR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")
ZjetsTheoWR5J = Systematic("ZjetsTheo_WR5J",configMgr.weights,configMgr.weights+["( 1+0.19)"],configMgr.weights+["( 1-0.19 )"], "weight","overallSys")
ZjetsTheoWR6J = Systematic("ZjetsTheo_WR6J",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")

ZjetsTheoTR4Jlowx = Systematic("ZjetsTheo_TR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.26)"],configMgr.weights+["( 1-0.26 )"], "weight","overallSys")
ZjetsTheoTR4Jhighx = Systematic("ZjetsTheo_TR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")
ZjetsTheoTR5J = Systematic("ZjetsTheo_TR5J",configMgr.weights,configMgr.weights+["( 1+0.19)"],configMgr.weights+["( 1-0.19 )"], "weight","overallSys")
ZjetsTheoTR6J = Systematic("ZjetsTheo_TR6J",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")


ZjetsTheoVR4Jlowx_mt = Systematic("ZjetsTheo_VR4Jlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.26)"],configMgr.weights+["( 1-0.26 )"], "weight","overallSys")
ZjetsTheoVR4Jlowx_aplanarity = Systematic("ZjetsTheo_VR4Jlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.26)"],configMgr.weights+["( 1-0.26 )"], "weight","overallSys")

ZjetsTheoVR4Jhighx_mt = Systematic("ZjetsTheo_VR4Jhighx_mt",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")
ZjetsTheoVR4Jhighx_meff = Systematic("ZjetsTheo_VR4Jhighx_meff",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")

ZjetsTheoVR5J_mt = Systematic("ZjetsTheo_VR5J_mt",configMgr.weights,configMgr.weights+["( 1+0.19)"],configMgr.weights+["( 1-0.19 )"], "weight","overallSys")
ZjetsTheoVR5J_aplanarity = Systematic("ZjetsTheo_VR5J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.19)"],configMgr.weights+["( 1-0.19 )"], "weight","overallSys")

ZjetsTheoVR6J_mt = Systematic("ZjetsTheo_VR6J_mt",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")
ZjetsTheoVR6J_aplanarity= Systematic("ZjetsTheo_VR6J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.22)"],configMgr.weights+["( 1-0.22 )"], "weight","overallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("zjets","SR4JhighxnomtEM"), ZjetsTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JhighxnometovermeffEM"), ZjetsTheoSR4Jhighx))

    generatorSyst.append((("zjets","SR4JlowxnomtEM"), ZjetsTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JlowxnomeffEM"), ZjetsTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JlowxnoJetAplanarityEM"), ZjetsTheoSR4Jlowx))

    generatorSyst.append((("zjets","SR5JnomtEM"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR5JnomeffEM"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR5JnoJetAplanarityEM"), ZjetsTheoSR5J))

    generatorSyst.append((("zjets","SR6JnomtEM"), ZjetsTheoSR6J))
    generatorSyst.append((("zjets","SR6JnoJetAplanarityEM"), ZjetsTheoSR6J))

    ###############################################################################
    generatorSyst.append((("zjets","SR4JhighxEl"), ZjetsTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JhighxMu"), ZjetsTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JlowxEl"), ZjetsTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JlowxMu"), ZjetsTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JhighxEM"),ZjetsTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JlowxEM"), ZjetsTheoSR4Jlowx)) 
    generatorSyst.append((("zjets","SR5JEl"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR5JMu"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR5JEM"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR6JEl"), ZjetsTheoSR6J))
    generatorSyst.append((("zjets","SR6JMu"), ZjetsTheoSR6J))
    generatorSyst.append((("zjets","SR6JEM"), ZjetsTheoSR6J))
    ###############################################################################
    generatorSyst.append((("zjets","WR4JhighxEl"), ZjetsTheoWR4Jhighx))
    generatorSyst.append((("zjets","WR4JhighxMu"), ZjetsTheoWR4Jhighx))
    generatorSyst.append((("zjets","WR4JlowxEl"), ZjetsTheoWR4Jlowx))
    generatorSyst.append((("zjets","WR4JlowxMu"), ZjetsTheoWR4Jlowx))
    generatorSyst.append((("zjets","WR4JhighxEM"),ZjetsTheoWR4Jhighx))
    generatorSyst.append((("zjets","WR4JlowxEM"), ZjetsTheoWR4Jlowx)) 
    generatorSyst.append((("zjets","WR5JEl"), ZjetsTheoWR5J))
    generatorSyst.append((("zjets","WR5JMu"), ZjetsTheoWR5J))
    generatorSyst.append((("zjets","WR5JEM"), ZjetsTheoWR5J))
    generatorSyst.append((("zjets","WR6JEl"), ZjetsTheoWR6J))
    generatorSyst.append((("zjets","WR6JMu"), ZjetsTheoWR6J))
    generatorSyst.append((("zjets","WR6JEM"), ZjetsTheoWR6J))
    ###############################################################################
    generatorSyst.append((("zjets","TR4JhighxEl"), ZjetsTheoTR4Jhighx))
    generatorSyst.append((("zjets","TR4JhighxMu"), ZjetsTheoTR4Jhighx))
    generatorSyst.append((("zjets","TR4JlowxEl"), ZjetsTheoTR4Jlowx))
    generatorSyst.append((("zjets","TR4JlowxMu"), ZjetsTheoTR4Jlowx))
    generatorSyst.append((("zjets","TR4JhighxEM"),ZjetsTheoTR4Jhighx))
    generatorSyst.append((("zjets","TR4JlowxEM"), ZjetsTheoTR4Jlowx)) 
    generatorSyst.append((("zjets","TR5JEl"), ZjetsTheoTR5J))
    generatorSyst.append((("zjets","TR5JMu"), ZjetsTheoTR5J))
    generatorSyst.append((("zjets","TR5JEM"), ZjetsTheoTR5J))
    generatorSyst.append((("zjets","TR6JEl"), ZjetsTheoTR6J))
    generatorSyst.append((("zjets","TR6JMu"), ZjetsTheoTR6J))
    generatorSyst.append((("zjets","TR6JEM"), ZjetsTheoTR6J))
    ################################################################################
    
    generatorSyst.append((("zjets","VR4Jlowx_mtEl"), ZjetsTheoVR4Jlowx_mt))
    generatorSyst.append((("zjets","VR4Jlowx_mtMu"), ZjetsTheoVR4Jlowx_mt))
    generatorSyst.append((("zjets","VR4Jlowx_mtEM"), ZjetsTheoVR4Jlowx_mt))
    generatorSyst.append((("zjets","VR4Jlowx_aplanarityEl"), ZjetsTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("zjets","VR4Jlowx_aplanarityMu"), ZjetsTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("zjets","VR4Jlowx_aplanarityEM"), ZjetsTheoVR4Jlowx_aplanarity))
   
    
    generatorSyst.append((("zjets","VR4Jhighx_meffEl"), ZjetsTheoVR4Jhighx_meff))
    generatorSyst.append((("zjets","VR4Jhighx_meffMu"), ZjetsTheoVR4Jhighx_meff))
    generatorSyst.append((("zjets","VR4Jhighx_meffEM"), ZjetsTheoVR4Jhighx_meff))
    generatorSyst.append((("zjets","VR4Jhighx_mtEl"), ZjetsTheoVR4Jhighx_mt))
    generatorSyst.append((("zjets","VR4Jhighx_mtMu"), ZjetsTheoVR4Jhighx_mt))
    generatorSyst.append((("zjets","VR4Jhighx_mtEM"), ZjetsTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("zjets","VR5J_mtEl"), ZjetsTheoVR5J_mt))
    generatorSyst.append((("zjets","VR5J_mtMu"), ZjetsTheoVR5J_mt))
    generatorSyst.append((("zjets","VR5J_mtEM"), ZjetsTheoVR5J_mt))
    generatorSyst.append((("zjets","VR5J_aplanarityEl"), ZjetsTheoVR5J_aplanarity))
    generatorSyst.append((("zjets","VR5J_aplanarityMu"), ZjetsTheoVR5J_aplanarity))
    generatorSyst.append((("zjets","VR5J_aplanarityEM"), ZjetsTheoVR5J_aplanarity))
    
    generatorSyst.append((("zjets","VR6J_mtEl"), ZjetsTheoVR6J_mt))
    generatorSyst.append((("zjets","VR6J_mtMu"), ZjetsTheoVR6J_mt))
    generatorSyst.append((("zjets","VR6J_mtEM"), ZjetsTheoVR6J_mt))
    generatorSyst.append((("zjets","VR6J_aplanarityEl"), ZjetsTheoVR6J_aplanarity))
    generatorSyst.append((("zjets","VR6J_aplanarityMu"), ZjetsTheoVR6J_aplanarity))
    generatorSyst.append((("zjets","VR6J_aplanarityEM"), ZjetsTheoVR6J_aplanarity))
    
    

  
    return generatorSyst
