import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

ZjetsScalesVarTheoSR4Jlowx = Systematic("ZjetsScalesVar_SR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.07)"],configMgr.weights+["( 1-0.07 )"], "weight","overallSys")
ZjetsScalesVarTheoSR4Jhighx = Systematic("ZjetsScalesVar_SR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoSR5J = Systematic("ZjetsScalesVar_SR5J",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoSR6J = Systematic("ZjetsScalesVar_SR6J",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")

ZjetsScalesVarTheoWR4Jlowx = Systematic("ZjetsScalesVar_WR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.07)"],configMgr.weights+["( 1-0.07 )"], "weight","overallSys")
ZjetsScalesVarTheoWR4Jhighx = Systematic("ZjetsScalesVar_WR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoWR5J = Systematic("ZjetsScalesVar_WR5J",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoWR6J = Systematic("ZjetsScalesVar_WR6J",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")

ZjetsScalesVarTheoTR4Jlowx = Systematic("ZjetsScalesVar_TR4Jlowx",configMgr.weights,configMgr.weights+["( 1+0.07)"],configMgr.weights+["( 1-0.07 )"], "weight","overallSys")
ZjetsScalesVarTheoTR4Jhighx = Systematic("ZjetsScalesVar_TR4Jhighx",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoTR5J = Systematic("ZjetsScalesVar_TR5J",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoTR6J = Systematic("ZjetsScalesVar_TR6J",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")


ZjetsScalesVarTheoVR4Jlowx_mt = Systematic("ZjetsScalesVar_VR4Jlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.07)"],configMgr.weights+["( 1-0.07 )"], "weight","overallSys")
ZjetsScalesVarTheoVR4Jlowx_aplanarity = Systematic("ZjetsScalesVar_VR4Jlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.07)"],configMgr.weights+["( 1-0.07 )"], "weight","overallSys")

ZjetsScalesVarTheoVR4Jhighx_mt = Systematic("ZjetsScalesVar_VR4Jhighx_mt",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoVR4Jhighx_meff = Systematic("ZjetsScalesVar_VR4Jhighx_meff",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")


ZjetsScalesVarTheoVR5J_mt = Systematic("ZjetsScalesVar_VR5J_mt",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
ZjetsScalesVarTheoVR5J_aplanarity = Systematic("ZjetsScalesVar_VR5J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")

ZjetsScalesVarTheoVR6J_mt = Systematic("ZjetsScalesVar_VR6J_mt",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")
ZjetsScalesVarTheoVR6J_aplanarity= Systematic("ZjetsScalesVar_VR6J_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")




def TheorUnc(generatorSyst):
    ## for loose SR ##                                                                                                                                       
    generatorSyst.append((("zjets","SR4JhighxnomtEM"), ZjetsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JhighxnometovermeffEM"), ZjetsScalesVarTheoSR4Jhighx))

    generatorSyst.append((("zjets","SR4JlowxnomtEM"), ZjetsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JlowxnomeffEM"), ZjetsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JlowxnoJetAplanarityEM"), ZjetsScalesVarTheoSR4Jlowx))

    generatorSyst.append((("zjets","SR5JnomtEM"), ZjetsScalesVarTheoSR5J))
    generatorSyst.append((("zjets","SR5JnomeffEM"), ZjetsScalesVarTheoSR5J))
    generatorSyst.append((("zjets","SR5JnoJetAplanarityEM"), ZjetsScalesVarTheoSR5J))

    generatorSyst.append((("zjets","SR6JnomtEM"), ZjetsScalesVarTheoSR6J))
    generatorSyst.append((("zjets","SR6JnoJetAplanarityEM"), ZjetsScalesVarTheoSR6J))

    ###############################################################################
    generatorSyst.append((("zjets","SR4JhighxEl"), ZjetsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JhighxMu"), ZjetsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JlowxEl"), ZjetsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JlowxMu"), ZjetsScalesVarTheoSR4Jlowx))
    generatorSyst.append((("zjets","SR4JhighxEM"),ZjetsScalesVarTheoSR4Jhighx))
    generatorSyst.append((("zjets","SR4JlowxEM"), ZjetsScalesVarTheoSR4Jlowx)) 
    generatorSyst.append((("zjets","SR5JEl"), ZjetsScalesVarTheoSR5J))
    generatorSyst.append((("zjets","SR5JMu"), ZjetsScalesVarTheoSR5J))
    generatorSyst.append((("zjets","SR5JEM"), ZjetsScalesVarTheoSR5J))
    generatorSyst.append((("zjets","SR6JEl"), ZjetsScalesVarTheoSR6J))
    generatorSyst.append((("zjets","SR6JMu"), ZjetsScalesVarTheoSR6J))
    generatorSyst.append((("zjets","SR6JEM"), ZjetsScalesVarTheoSR6J))
    ###############################################################################
    generatorSyst.append((("zjets","WR4JhighxEl"), ZjetsScalesVarTheoWR4Jhighx))
    generatorSyst.append((("zjets","WR4JhighxMu"), ZjetsScalesVarTheoWR4Jhighx))
    generatorSyst.append((("zjets","WR4JlowxEl"), ZjetsScalesVarTheoWR4Jlowx))
    generatorSyst.append((("zjets","WR4JlowxMu"), ZjetsScalesVarTheoWR4Jlowx))
    generatorSyst.append((("zjets","WR4JhighxEM"),ZjetsScalesVarTheoWR4Jhighx))
    generatorSyst.append((("zjets","WR4JlowxEM"), ZjetsScalesVarTheoWR4Jlowx)) 
    generatorSyst.append((("zjets","WR5JEl"), ZjetsScalesVarTheoWR5J))
    generatorSyst.append((("zjets","WR5JMu"), ZjetsScalesVarTheoWR5J))
    generatorSyst.append((("zjets","WR5JEM"), ZjetsScalesVarTheoWR5J))
    generatorSyst.append((("zjets","WR6JEl"), ZjetsScalesVarTheoWR6J))
    generatorSyst.append((("zjets","WR6JMu"), ZjetsScalesVarTheoWR6J))
    generatorSyst.append((("zjets","WR6JEM"), ZjetsScalesVarTheoWR6J))
    ###############################################################################
    generatorSyst.append((("zjets","TR4JhighxEl"), ZjetsScalesVarTheoTR4Jhighx))
    generatorSyst.append((("zjets","TR4JhighxMu"), ZjetsScalesVarTheoTR4Jhighx))
    generatorSyst.append((("zjets","TR4JlowxEl"), ZjetsScalesVarTheoTR4Jlowx))
    generatorSyst.append((("zjets","TR4JlowxMu"), ZjetsScalesVarTheoTR4Jlowx))
    generatorSyst.append((("zjets","TR4JhighxEM"),ZjetsScalesVarTheoTR4Jhighx))
    generatorSyst.append((("zjets","TR4JlowxEM"), ZjetsScalesVarTheoTR4Jlowx)) 
    generatorSyst.append((("zjets","TR5JEl"), ZjetsScalesVarTheoTR5J))
    generatorSyst.append((("zjets","TR5JMu"), ZjetsScalesVarTheoTR5J))
    generatorSyst.append((("zjets","TR5JEM"), ZjetsScalesVarTheoTR5J))
    generatorSyst.append((("zjets","TR6JEl"), ZjetsScalesVarTheoTR6J))
    generatorSyst.append((("zjets","TR6JMu"), ZjetsScalesVarTheoTR6J))
    generatorSyst.append((("zjets","TR6JEM"), ZjetsScalesVarTheoTR6J))
    ###############################################################################
        

    
    
    generatorSyst.append((("zjets","VR4Jlowx_mtEl"), ZjetsScalesVarTheoVR4Jlowx_mt))
    generatorSyst.append((("zjets","VR4Jlowx_mtMu"), ZjetsScalesVarTheoVR4Jlowx_mt))
    generatorSyst.append((("zjets","VR4Jlowx_mtEM"), ZjetsScalesVarTheoVR4Jlowx_mt))
    generatorSyst.append((("zjets","VR4Jlowx_aplanarityEl"), ZjetsScalesVarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("zjets","VR4Jlowx_aplanarityMu"), ZjetsScalesVarTheoVR4Jlowx_aplanarity))
    generatorSyst.append((("zjets","VR4Jlowx_aplanarityEM"), ZjetsScalesVarTheoVR4Jlowx_aplanarity))
    
       
    generatorSyst.append((("zjets","VR4Jhighx_meffEl"), ZjetsScalesVarTheoVR4Jhighx_meff))
    generatorSyst.append((("zjets","VR4Jhighx_meffMu"), ZjetsScalesVarTheoVR4Jhighx_meff))
    generatorSyst.append((("zjets","VR4Jhighx_meffEM"), ZjetsScalesVarTheoVR4Jhighx_meff))
    generatorSyst.append((("zjets","VR4Jhighx_mtEl"), ZjetsScalesVarTheoVR4Jhighx_mt))
    generatorSyst.append((("zjets","VR4Jhighx_mtMu"), ZjetsScalesVarTheoVR4Jhighx_mt))
    generatorSyst.append((("zjets","VR4Jhighx_mtEM"), ZjetsScalesVarTheoVR4Jhighx_mt))
    
    
    generatorSyst.append((("zjets","VR5J_mtEl"), ZjetsScalesVarTheoVR5J_mt))
    generatorSyst.append((("zjets","VR5J_mtMu"), ZjetsScalesVarTheoVR5J_mt))
    generatorSyst.append((("zjets","VR5J_mtEM"), ZjetsScalesVarTheoVR5J_mt))
    generatorSyst.append((("zjets","VR5J_aplanarityEl"), ZjetsScalesVarTheoVR5J_aplanarity))
    generatorSyst.append((("zjets","VR5J_aplanarityMu"), ZjetsScalesVarTheoVR5J_aplanarity))
    generatorSyst.append((("zjets","VR5J_aplanarityEM"), ZjetsScalesVarTheoVR5J_aplanarity))
    
    generatorSyst.append((("zjets","VR6J_mtEl"), ZjetsScalesVarTheoVR6J_mt))
    generatorSyst.append((("zjets","VR6J_mtMu"), ZjetsScalesVarTheoVR6J_mt))
    generatorSyst.append((("zjets","VR6J_mtEM"), ZjetsScalesVarTheoVR6J_mt))
    generatorSyst.append((("zjets","VR6J_aplanarityEl"), ZjetsScalesVarTheoVR6J_aplanarity))
    generatorSyst.append((("zjets","VR6J_aplanarityMu"), ZjetsScalesVarTheoVR6J_aplanarity))
    generatorSyst.append((("zjets","VR6J_aplanarityEM"), ZjetsScalesVarTheoVR6J_aplanarity))
  
    return generatorSyst
