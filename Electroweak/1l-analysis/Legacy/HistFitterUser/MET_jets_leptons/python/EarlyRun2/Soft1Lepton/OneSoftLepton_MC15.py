################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

import EarlyRun2.Soft1Lepton.SignalAccUncertainties

from logger import Logger
log = Logger('SoftLepton')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return


# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

#check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]

SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
SystList.append("pileup")  # pileup (common)
SystList.append("BTag")     #b-tagging uncertainties
#SystList.append("GenPowhegJimmyTTbar") # PowhegPythia/PowhegJimmy generators difference TTbar
#SystList.append("FactScaleTTbar") # factorization scale variation in TTbar
#SystList.append("ISRFSRTTbar") # ISR/FSR variation in ttbar
#SystList.append("RenScaleTTbar") # re-normalization scale variation in TTbar
SystList.append("FactRenScaleISRFSRTTbar") # factorization/re-normalization scale variation in TTbar
SystList.append("HadFragTTbar") # Hadronization/Fragmentation TTbar
SystList.append("HardScatteringGenTTbar") # HardScattering  TTbar
#SystList.append("GenSingleTop")# Generator Systematics SingleTop  (common)
SystList.append("GenDB") # Generator Systematics DB    (common)
SystList.append("GenZjets") # Generator Systematics Zjets    (common)
SystList.append("WjetsTheo") # comparing Sherpa (baseline) to Madgraph+Pythia8 (alternative)
SystList.append("WjetsScaleVar") # scale variations in Sherpa
SystList.append("ZjetsTheo") # comparing Sherpa (baseline) to Madgraph+Pythia8 (alternative)
SystList.append("ZjetsScaleVar") # scale variations in Sherpa
SystList.append("DibosonsTheo") # comparing Sherpa (baseline) to Powheg-box+Pythia8 (alternative
SystList.append("DibosonsScaleVar") # scale variations 
  
  



# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["5J"] #this is the default - modify from command line

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 2J, 5J option)"
    
#remove later: warning not to use two towers at the same time at the moment.
if len(CRregions) > 1:
    print "FATAL: CR and SR are not yet orthogonal - don't run with more than one tower at the moment!"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["SR2J"] = False
ValidRegList["VR2J"] = False
ValidRegList["SR5J"] = False
ValidRegList["VR5J"] = False

#for tables (turn doTableInputs to True)
doTableInputs = True #This effectively means no validation plots but only validation tables (but is 100x faster)
for cr in CRregions:
    if "2J" in cr and doTableInputs:
        ValidRegList["VR2J"] = True
    if "5J" in cr and doTableInputs:
        ValidRegList["VR5J"] = True        

#take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    sigSamples=["onestepGG_825_785_745"] 
    
#define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0]:
        analysissuffix += '_GG1step'

mylumi=3.31668  #3.34258       
        
#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension = "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
        
# First define HistFactory attributes
configMgr.analysisName = "OneSoftLepton"+analysissuffix+analysisextension# Name to give the analysis
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
configMgr.useCacheToTreeFallback = False# enable the fallback to trees
configMgr.useHistBackupCacheFile = False# enable the use of an alternate data file
#if myoptions=="sensitivity_2":
#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/EarlyRun2/Soft1Lepton/CacheFiles/OneSoftLepton_5JWithoutPileupinSR5J.root" # the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
#configMgr.inputLumi =  1 
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

configMgr.fixSigXSec=True
#configMgr.calculatorType=0 #toys
configMgr.calculatorType=2 #asimov
configMgr.testStatType=3
configMgr.nPoints=20

#configMgr.scanRange = (0., 20.) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False# Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

#directory with background files:
#inputDir="root://eosatlas//eos/atlas/user/v/vtudorac/SoftTreesp2419FAR/"
inputDir="root://eosatlas//eos/atlas/user/v/vtudorac/SoftTrees0402/"
#inputDir="root://eosatlas//eos/atlas/user/v/vtudorac/SoftTrees50ns/"


#data and QCD:
# (don't have any at the moment)

#signal files
#inputDirSig="root://eosatlas//eos/atlas/user/v/vtudorac/SoftTreesp2419FAR/"
inputDirSig="root://eosatlas//eos/atlas/user/v/vtudorac/SoftTrees0402/"
#inputDirSig="root://eosatlas//eos/atlas/user/v/vtudorac/SoftTrees50ns/"

'''
if not onLxplus:
    print "INFO : Running locally...\n"
    # put here paths to your local files if not running on lxplus
    # the following paths are for running locally at LMU
    inputDir="/data/vtudorac/SoftTrees0305/"
    inputDirSig="/data/vtudorac/SoftTrees0305/"   
else:
    print "INFO : Running on lxplus... \n" 
'''    
       
#background files, separately for electron and muon channel:
#bgdFiles_e = [inputDir+"alltrees_T_03_02.root"] 
#bgdFiles_m = [inputDir+"alltrees_T_03_02.root"]
#bgdFiles_em = [inputDir+"alltrees_T_03_02.root"]

bgdFiles_e = [inputDir+"allTrees_T_04_02.root"] 
bgdFiles_m = [inputDir+"allTrees_T_04_02.root"]
bgdFiles_em = [inputDir+"allTrees_T_04_02.root"]


#signal files
if myFitType==FitType.Exclusion:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allTrees_T_04_02_SigOnly.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allTrees_T_04_02_SigOnly.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allTrees_T_04_02_SigOnly.root"]
	


# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&& lep1Pt<35 && nLep_baseline==1" #&& isDuplicateEvent==0"
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>7 &&  HLT_xe70_tc_lcw)"
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>6 &&  HLT_xe70_tc_lcw)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>7 &&  HLT_xe70_tc_lcw) || (AnalysisType==2 && lep1Pt>6 &&  HLT_xe70_tc_lcw) )"
#EtaCommonSelection="&&lep1Eta>-2.2&&lep1Eta<2.2"


# ------- 2J region

configMgr.cutsDict["SR2J"]="nJet30>=2 && jet1Pt>180.  && met>530. && mt>100. && (met/meffInc30) > 0.38" + CommonSelection# FAR
configMgr.cutsDict["WR2J"]="nJet30>=2 && jet1Pt>180.  && met>250. && met<500. && mt>40. && mt<80 && (met/meffInc30) > 0.38  && nBJet30_MV2==0" + CommonSelection
configMgr.cutsDict["TR2J"]="nJet30>=2 && jet1Pt>180.  && met>250. && met<500. && mt>40. && mt<80 && (met/meffInc30) > 0.38  && nBJet30_MV2>=1" + CommonSelection
#configMgr.cutsDict["WR2J"]="nJet30>=2 && jet1Pt>180.  && met>250. && met<500. && mt<100 && (met/meffInc30) > 0.38  && nBJet30_MV2==0" + CommonSelection
#configMgr.cutsDict["TR2J"]="nJet30>=2 && jet1Pt>180.  && met>250. && met<500. && mt<100 && (met/meffInc30) > 0.38  && nBJet30_MV2>=1" + CommonSelection
configMgr.cutsDict["VR2J_1"]="nJet30>=2 && jet1Pt>180. && met>500. && mt>40. && mt<100. && (met/meffInc30) > 0.38" + CommonSelection 
configMgr.cutsDict["VR2J_2"]="nJet30>=2 && jet1Pt>180. && met>250. && met<500. && mt>80. && (met/meffInc30) > 0.38" + CommonSelection

# ------- 5J region

configMgr.cutsDict["SR5J"]="nJet30>=5 && jet1Pt>200. && jet2Pt>200. && jet3Pt>200. && met>375. && Ht30>1100. && JetAplanarity>0.02" + CommonSelection# FAR
configMgr.cutsDict["WR5J"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && met>250. && met<375. && Ht30>500. && Ht30<1000. && JetAplanarity>0.02 && nBJet30_MV2==0" + CommonSelection
configMgr.cutsDict["TR5J"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && met>250. && met<375. && Ht30>500. && Ht30<1000. && JetAplanarity>0.02 && nBJet30_MV2>=1" + CommonSelection
configMgr.cutsDict["VR5J_1"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && met>375. && Ht30>500. && Ht30<1100. && JetAplanarity>0.02" + CommonSelection
configMgr.cutsDict["VR5J_2"]="nJet30>=5 && jet1Pt>150. && jet2Pt>100. && met>250. && met<375. && Ht30>1000. && JetAplanarity>0.02" + CommonSelection




#cond part: splitting into electron and muon channel for validation purposes
d=configMgr.cutsDict
configMgr.cutsDict["SR2JEl"] = d["SR2J"]+OneEleSelection 
configMgr.cutsDict["SR2JMu"] = d["SR2J"]+OneMuoSelection
configMgr.cutsDict["SR2JEM"] = d["SR2J"]+OneLepSelection
configMgr.cutsDict["WR2JEl"] = d["WR2J"]+OneEleSelection
configMgr.cutsDict["WR2JMu"] = d["WR2J"]+OneMuoSelection
configMgr.cutsDict["WR2JEM"] = d["WR2J"]+OneLepSelection
configMgr.cutsDict["TR2JEl"] = d["TR2J"]+OneEleSelection
configMgr.cutsDict["TR2JMu"] = d["TR2J"]+OneMuoSelection
configMgr.cutsDict["TR2JEM"] = d["TR2J"]+OneLepSelection
configMgr.cutsDict["VR2J_1El"] = d["VR2J_1"]+OneEleSelection
configMgr.cutsDict["VR2J_1Mu"] = d["VR2J_1"]+OneMuoSelection
configMgr.cutsDict["VR2J_1EM"] = d["VR2J_1"]+OneLepSelection
configMgr.cutsDict["VR2J_2El"] = d["VR2J_2"]+OneEleSelection
configMgr.cutsDict["VR2J_2Mu"] = d["VR2J_2"]+OneMuoSelection
configMgr.cutsDict["VR2J_2EM"] = d["VR2J_2"]+OneLepSelection

configMgr.cutsDict["SR5JEl"] = d["SR5J"]+OneEleSelection
configMgr.cutsDict["SR5JMu"] = d["SR5J"]+OneMuoSelection
configMgr.cutsDict["SR5JEM"] = d["SR5J"]+OneLepSelection
configMgr.cutsDict["WR5JEl"] = d["WR5J"]+OneEleSelection
configMgr.cutsDict["WR5JMu"] = d["WR5J"]+OneMuoSelection
configMgr.cutsDict["WR5JEM"] = d["WR5J"]+OneLepSelection
configMgr.cutsDict["TR5JEl"] = d["TR5J"]+OneEleSelection
configMgr.cutsDict["TR5JMu"] = d["TR5J"]+OneMuoSelection
configMgr.cutsDict["TR5JEM"] = d["TR5J"]+OneLepSelection
configMgr.cutsDict["VR5J_1El"] = d["VR5J_1"]+OneEleSelection
configMgr.cutsDict["VR5J_1Mu"] = d["VR5J_1"]+OneMuoSelection
configMgr.cutsDict["VR5J_1EM"] = d["VR5J_1"]+OneLepSelection
configMgr.cutsDict["VR5J_2El"] = d["VR5J_2"]+OneEleSelection
configMgr.cutsDict["VR5J_2Mu"] = d["VR5J_2"]+OneMuoSelection
configMgr.cutsDict["VR5J_2EM"] = d["VR5J_2"]+OneLepSelection
# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #

#hacks for W+jets and Z+jets samples:
wjets_hackedweight = "(1. + ( DatasetNumber==361365 || DatasetNumber==361364 || DatasetNumber==361363 || DatasetNumber==361362 || DatasetNumber==361361 || DatasetNumber==361360 || DatasetNumber==361359 || DatasetNumber==361358 || DatasetNumber==361357 || DatasetNumber==361341 || DatasetNumber==361340 || DatasetNumber==361339 || DatasetNumber==361338 || DatasetNumber==361337 || DatasetNumber==361336 || DatasetNumber==361335 || DatasetNumber==361334 || DatasetNumber==361333 || DatasetNumber==361317 || DatasetNumber==361316 || DatasetNumber==361315 || DatasetNumber==361314 || DatasetNumber==361312 || DatasetNumber==361311 || DatasetNumber==361310 || DatasetNumber==361309  )*0.1)"
zjets_hackedweight = "(1. + (DatasetNumber==361461 || DatasetNumber==361460 || DatasetNumber==361459 || DatasetNumber==361458 || DatasetNumber==361457 || DatasetNumber==361456 || DatasetNumber==361455 || DatasetNumber==361454 || DatasetNumber==361453 || DatasetNumber==361437 || DatasetNumber==361436 || DatasetNumber==361435 || DatasetNumber==361434 || DatasetNumber==361433 || DatasetNumber==361432 || DatasetNumber==361431 || DatasetNumber==361430 || DatasetNumber==361429 || DatasetNumber==361413 || DatasetNumber==361412 || DatasetNumber==361411 || DatasetNumber==361410 || DatasetNumber==361409 || DatasetNumber==361408 || DatasetNumber==361407 || DatasetNumber==361406 || DatasetNumber==361405 || DatasetNumber==361389 || DatasetNumber==361388 || DatasetNumber==361387 || DatasetNumber==361386 || DatasetNumber==361385 || DatasetNumber==361384 || DatasetNumber==361383 || DatasetNumber==361382 || DatasetNumber==361381 )*0.1)"

#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights
#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")

muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")



#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#electron related uncertainties acting on weights
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TotalCorrUncertainty__1up") 
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TotalCorrUncertainty__1down")
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TotalCorrUncertainty__1up") 
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TotalCorrUncertainty__1down")
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TotalCorrUncertainty__1up") 
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TotalCorrUncertainty__1down")



#pileup systematics
pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")
    

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}

SingleTopComm = {}
WjetsComm = {}
DibosonsComm = {}
ZjetsComm = {}
ttbarVComm={}
SingleTopWtXsec = {}
SingleTopSXsec = {}
SingleTopTXsec ={}
SingleAntiTopSXsec = {}
SingleAntiTopTXsec ={}
DBXsec={}
ZjetsXsec={}

for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))
    if "JES" in SystList: 
        basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
        basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_ID","_NoSys","_MUONS_ID__1up","_MUONS_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_MS","_NoSys","_MUONS_MS__1up","_MUONS_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUONS_SCALE","_NoSys","_MUONS_SCALE__1up","_MUONS_SCALE__1down","tree","overallNormHistoSys"))
    if "LepEff" in SystList :
    	basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys")) 
	 
        #trigger
        #basicChanSyst[region].append(Systematic("MUON_Eff_trigger_stat",configMgr.weights,muonHighWeights_trigger_stat,muonLowWeights_trigger_stat,"weight","overallNormHistoSys")) 
        #basicChanSyst[region].append(Systematic("MUON_Eff_trigger_syst",configMgr.weights,muonHighWeights_trigger_syst,muonLowWeights_trigger_syst,"weight","overallNormHistoSys"))         
    	basicChanSyst[region].append(Systematic("EL_Eff_ID",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
    	basicChanSyst[region].append(Systematic("EL_Eff_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys"))        
    	basicChanSyst[region].append(Systematic("EL_Eff_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys"))  
    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))
    if "BTag" in SystList:
    	bTagSyst[region] = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
    	cTagSyst[region] = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
    	mTagSyst[region] = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")
    	eTagSyst[region] = Systematic("btag_Extra",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")	
		
# Temporary common syst for minor backgrounds
    SingleTopComm[region] = Systematic("h1L_SingleTopComm", configMgr.weights,configMgr.weights+["( 1+0.8 )"],configMgr.weights+["( 1-0.8 )"], "weight","overallSys")
    #WjetsComm[region] = Systematic("h1L_WjetsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallNormSys")
    #DibosonsComm[region] = Systematic("h1L_DibosonsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
    #ZjetsComm[region] = Systematic("h1L_ZjetsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
    ttbarVComm[region] = Systematic("h1L_ttbarVComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
#signal uncertainties
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")     

# Generator Systematics
    generatorSyst = []	
    
# theory uncertainties in SingleTop
    if "GenSingleTop" in SystList:
	    #Wt channels
	        SingleTopWt = "(DatasetNumber==410013 ||DatasetNumber==410014 || DatasetNumber==407021 || DatasetNumber==407019 )"
	        SingleTopWtXsec[region] = Systematic("h1L_SingleTopWtXsec", configMgr.weights,configMgr.weights+["( (0.053) * %s + 1 )" % SingleTopWt],configMgr.weights+["( (-0.053) * %s + 1 )" % SingleTopWt], "weight","overallSys")
	    #t channels
	        SingleTopT = "(DatasetNumber==410011)"
	        SingleTopTXsec[region] = Systematic("h1L_SingleTopTXsec", configMgr.weights,configMgr.weights+["( (0.040) * %s + 1 )" % SingleTopT],configMgr.weights+["( (-0.040) * %s + 1 )" % SingleTopT], "weight","overallSys")
	        SingleAntiTopT = "(DatasetNumber==410012)"
	        SingleAntiTopTXsec[region] = Systematic("h1L_SingleAntiTopTXsec", configMgr.weights,configMgr.weights+["( (0.050) * %s + 1 )" % SingleAntiTopT],configMgr.weights+["( (-0.050) * %s + 1 )" % SingleAntiTopT], "weight","overallSys")
	    #s channels
	        SingleTopS = "(DatasetNumber==410025)"
	        SingleTopSXsec[region] = Systematic("h1L_SingleTopSXsec", configMgr.weights,configMgr.weights+["( (0.037) * %s + 1 )" % SingleTopS],configMgr.weights+["( (-0.037) * %s + 1 )" % SingleTopS], "weight","overallSys")
	        SingleAntiTopS  ="(DatasetNumber==410026)"
	        SingleAntiTopSXsec[region] = Systematic("h1L_SingleAntiTopSXsec", configMgr.weights,configMgr.weights+["( (0.047) * %s + 1 )" % SingleAntiTopS],configMgr.weights+["( (-0.047) * %s + 1 )" % SingleAntiTopS], "weight","overallSys")
    if "GenDB" in SystList:	       
	        diboson = "(DatasetNumber==361063 || DatasetNumber==361064 || DatasetNumber==361065 || DatasetNumber==361066 || DatasetNumber==361067 || DatasetNumber==361068 || DatasetNumber==361069 || DatasetNumber==361070 || DatasetNumber==361071 || DatasetNumber==361072 || DatasetNumber==361073 || DatasetNumber==361077 || DatasetNumber==361081 || DatasetNumber==361082 || DatasetNumber==361083 || DatasetNumber==361084 || DatasetNumber==361085 || DatasetNumber==361086  || DatasetNumber==361087 )"
	        DBXsec[region] = Systematic("h1L_DBXsec", configMgr.weights,configMgr.weights+["( (0.06) * %s + 1 )" % diboson],configMgr.weights+["( (-0.06) * %s + 1 )" % diboson], "weight","overallSys")	
    if "GenZjets" in SystList:        
	        ZjetsXsec[region] = Systematic("h1L_ZjetsXsec", configMgr.weights,configMgr.weights+["(0.05 + 1 )" ],configMgr.weights+["( -0.05 + 1 )"], "weight","overallSys")	
				
	
# Theory uncertainties in ttbar
'''
if "GenPowhegJimmyTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_PowhegPythia_PowhegJimmy
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_PowhegPythia_PowhegJimmy.TheorUnc(generatorSyst)
	
# Factorization Scale variation	in ttbar    	
if "FactScaleTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_FactScale_ttbar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_FactScale_ttbar.TheorUnc(generatorSyst)
	
	
# ISR/FSR uncertainty calculated by comparing PowhegPythia with morePS  and lessPS 
if "ISRFSRTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_ISRFSR_ttbar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_ISRFSR_ttbar.TheorUnc(generatorSyst)	
	
# Renormalization Scale variation in ttbar		    
if "RenScaleTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_RenScale_ttbar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_RenScale_ttbar.TheorUnc(generatorSyst)
'''

if "FactRenScaleISRFSRTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_FactRenScaleISRFSR_ttbar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_FactRenScaleISRFSR_ttbar.TheorUnc(generatorSyst)
	
    	
if "HadFragTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_HadFrag_ttbar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_HadFrag_ttbar.TheorUnc(generatorSyst)
	

if "HardScatteringGenTTbar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_HardScatteringGen_ttbar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_HardScatteringGen_ttbar.TheorUnc(generatorSyst)
	
	
if "WjetsTheo" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_Wjets
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_Wjets.TheorUnc(generatorSyst)	
	
if "WjetsScaleVar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_WjetsScalesVar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_WjetsScalesVar.TheorUnc(generatorSyst)
	
	
if "ZjetsTheo" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_Zjets
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_Zjets.TheorUnc(generatorSyst)	
	
if "ZjetsScaleVar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_ZjetsScalesVar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_ZjetsScalesVar.TheorUnc(generatorSyst)
			
if "DibosonsTheo" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_Dibosons
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_Dibosons.TheorUnc(generatorSyst)	
	
if "DibosonsScaleVar" in SystList:
	import EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_DibosonsScalesVar
	EarlyRun2.Soft1Lepton.theoryUncertainties_softLepton_DibosonsScalesVar.TheorUnc(generatorSyst)
	

# ********************************************************************* #
#                              Background samples
# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.addSampleSpecificWeight(wjets_hackedweight)
#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
ZSample.addSampleSpecificWeight(zjets_hackedweight)
#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
#DataSample.setFileList([inputDir+"alltrees_T_03_02.root"])
DataSample.setFileList([inputDir+"allTrees_T_04_02.root"])
DataSample.setData()

# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")
bkgOnly.addSamples([ZSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,ttbarVSample,DataSample])


if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.09)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
bReqChans["5J"] = []
bVetoChans["5J"] = []
bAgnosticChans["5J"] = []
bReqChans["2J"] = []
bVetoChans["2J"] = []
bAgnosticChans["2J"] = []

#lepton flavor classification of channels
elChans = {}
muChans = {}

elChans["5J"] = []
muChans["5J"] = []
elChans["2J"] = []
muChans["2J"] = []

elmuChans = {}
elmuChans["5J"] = []
elmuChans["2J"] = []


######################################################
# Add channels to Bkg-only configuration             #
######################################################



if "5J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR5JEM"],1,0.5,1.5),[elmuChans["5J"],bVetoChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR5JEM"],1,0.5,1.5),[elmuChans["5J"],bReqChans["5J"]]) 
    bkgOnly.setBkgConstrainChannels(tmp)
if "2J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR2JEM"],1,0.5,1.5),[elmuChans["2J"],bVetoChans["2J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR2JEM"],1,0.5,1.5),[elmuChans["2J"],bReqChans["2J"]])
    bkgOnly.setBkgConstrainChannels(tmp)    

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #
#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]  = ValidRegList["VR2J"] + ValidRegList["VR5J"]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans["2J"])
        appendIfMatchName(c,bVetoChans["2J"])
        appendIfMatchName(c,bAgnosticChans["2J"])
        appendIfMatchName(c,elChans["2J"])
        appendIfMatchName(c,muChans["2J"])      
        appendIfMatchName(c,elmuChans["2J"])

        appendIfMatchName(c,bReqChans["5J"])
        appendIfMatchName(c,bVetoChans["5J"])
        appendIfMatchName(c,bAgnosticChans["5J"])
        appendIfMatchName(c,elChans["5J"])
        appendIfMatchName(c,muChans["5J"])      
        appendIfMatchName(c,elmuChans["5J"])
   

    if doTableInputs:
        if "2J" in CRregions:
         
          appendTo(validation.addValidationChannel("cuts",["SR2JEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["SR2JMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["SR2JEM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
          appendTo(validation.addValidationChannel("cuts",["VR2J_1El"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["VR2J_1Mu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["VR2J_1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["VR2J_2El"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["VR2J_2Mu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
          appendTo(validation.addValidationChannel("cuts",["VR2J_2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
	  
        if "5J" in CRregions:
         
          appendTo(validation.addValidationChannel("cuts",["SR5JEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["SR5JMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["SR5JEM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
	  
          appendTo(validation.addValidationChannel("cuts",["VR5J_1El"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["VR5J_1Mu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["VR5J_1EM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["VR5J_2El"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["VR5J_2Mu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
          appendTo(validation.addValidationChannel("cuts",["VR5J_2EM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
                   
           
    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs       


# ********************************************************************* #
#                              Later: exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    if '2J' in CRregions:
        SRs=["SR2JEM"]
    if '5J' in CRregions:
        SRs=["SR5JEM"]

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        ######### ISR uncertainties #####################
        if '2J'  in CRregions: ERRregion='soft_SR2J'
        if '5J'  in CRregions: ERRregion='soft_SR5J'
        errsig =  EarlyRun2.Soft1Lepton.SignalAccUncertainties.GetUncertaintyFromName(ERRregion,sig)
        print "sig=",sig,"  region=",ERRregion,"errsig=",errsig
        isrSig = Systematic("SigISR",configMgr.weights,1.00+errsig,1.00-errsig,"user","userOverallSys")
        #################################################
        sigSample.addSystematic(xsecSig)
        #sigSample.mergeOverallSysSet = ["SigXSec","SigISR"]
            
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:           
            if sr=="SR2JEl" or sr=="SR2JMu" or sr=="SR2JEM":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)          
            elif sr=="SR5JEl" or sr=="SR5JMu" or sr=="SR5JEM":
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)                    
                                
            else:
                raise RuntimeError("Unexpected signal region %s"%sr)
                
            if sr=="SR2JEl": 
                elChans["2J"].append(ch) 
                bAgnosticChans["2J"].append(ch)
            elif sr=="SR2JMu": 
                muChans["2J"].append(ch) 
                bAgnosticChans["2J"].append(ch)
            elif sr=="SR2JEM":
                elmuChans["2J"].append(ch)
                bAgnosticChans["2J"].append(ch)               
            elif sr=="SR5JEl": 
                elChans["5J"].append(ch) 
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR5JMu": 
                muChans["5J"].append(ch) 
                bAgnosticChans["5J"].append(ch)
            elif sr=="SR5JEM":
                elmuChans["5J"].append(ch)
                bAgnosticChans["5J"].append(ch)
                         
            else:
                raise RuntimeError("Unexpected signal region %s"%sr)
            pass
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.getSample(sig).addSystematic(isrSig)
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan = tgt[1]
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
        if tgtchan=="All" or tgtchan in chan.name:
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
    
    
    

for region in CRregions:
    SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
    SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
    SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])

##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
	    
            if "SR2J" in chan.name:	    
                theSample.removeWeight("pileupWeight")
                theSample.removeSystematic("pileup")
             
            sys_region = ""
            if "2J" in chan.name: sys_region = "2J"
            elif "5J" in chan.name: sys_region = "5J"
                       
            else: 
                print "Unknown region! - Take systematics from 5J regions."
                sys_region = "5J"
		
            theSample.removeSystematic("EL_Eff_Iso")
	    #theSample.removeSystematic("EL_Eff_Reco")
	    #theSample.removeSystematic("EL_Eff_ID")
		
            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans["2J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["2J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["2J"]:
                theSigFiles = sigFiles_em[sig]
            elif chan in elChans["5J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["5J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["5J"]:
                theSigFiles = sigFiles_em[sig]
                                 
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")

    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
    
        if "GenDB" in SystList:
           	chan.getSample("diboson").addSystematic(DBXsec[region])
        if "GenZjets" in SystList:
           	chan.getSample("zjets").addSystematic(ZjetsXsec[region]) 	    
    
        if "GenSingleTop" in SystList:
           	chan.getSample("singletop").addSystematic(SingleTopSXsec[region])  
           	chan.getSample("singletop").addSystematic(SingleTopWtXsec[region])  
           	chan.getSample("singletop").addSystematic(SingleTopTXsec[region])  
           	chan.getSample("singletop").addSystematic(SingleAntiTopSXsec[region])  
           	chan.getSample("singletop").addSystematic(SingleAntiTopTXsec[region])  
	   
        chan.getSample("singletop").addSystematic(SingleTopComm[region])
        #chan.getSample("wjets").addSystematic(WjetsComm[region])
        #chan.getSample("diboson").addSystematic(DibosonsComm[region])
        #chan.getSample("zjets").addSystematic(ZjetsComm[region])
        chan.getSample("ttv").addSystematic(ttbarVComm[region])
        chan.getSample("ttbar").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
        chan.getSample("wjets").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
        #chan.getSample("ttv").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
        #chan.getSample("singletop").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])               
        #chan.getSample("diboson").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
        #chan.getSample("zjets").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])




# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") #only for SR5J
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") # not for SR5J
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    
    if "SR3J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 70.
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR5J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 18.5
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 25.5
        chan.titleY = "Events / 100 GeV"
        chan.titleX = "E^{miss}_{T} [GeV]"      
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
