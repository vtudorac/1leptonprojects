import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


DibosonsTheoSR2J = Systematic("DibosonsTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.40)"],configMgr.weights+["( 1-0.40 )"], "weight","overallSys")
DibosonsTheoSR5J = Systematic("DibosonsTheo_SR5J", configMgr.weights,configMgr.weights+["( 1+0.24)"],configMgr.weights+["( 1-0.24 )"], "weight","overallSys")
DibosonsTheoVR2J_1 = Systematic("DibosonsTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.40)"],configMgr.weights+["( 1-0.40 )"], "weight","overallSys")
DibosonsTheoVR2J_2 = Systematic("DibosonsTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.40)"],configMgr.weights+["( 1-0.40 )"], "weight","overallSys")
DibosonsTheoVR5J_1 = Systematic("DibosonsTheo_VR5J_1", configMgr.weights,configMgr.weights+["( 1+0.24)"],configMgr.weights+["( 1-0.24 )"], "weight","overallSys")
DibosonsTheoVR5J_2 = Systematic("DibosonsTheo_VR5J_2", configMgr.weights,configMgr.weights+["( 1+0.24)"],configMgr.weights+["( 1-0.24 )"], "weight","overallSys")

DibosonsTheoTR2J = Systematic("DibosonsTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.40)"],configMgr.weights+["( 1-0.40 )"], "weight","overallSys")
DibosonsTheoTR5J = Systematic("DibosonsTheo_TR5J",configMgr.weights,configMgr.weights+["( 1+0.24)"],configMgr.weights+["( 1-0.24 )"], "weight","overallSys")
DibosonsTheoWR2J = Systematic("DibosonsTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.40)"],configMgr.weights+["( 1-0.40 )"], "weight","overallSys")
DibosonsTheoWR5J = Systematic("DibosonsTheo_WR5J",configMgr.weights,configMgr.weights+["( 1+0.24)"],configMgr.weights+["( 1-0.24 )"], "weight","overallSys")










def TheorUnc(generatorSyst):
   
    generatorSyst.append((("diboson","SR5JEl"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR5JMu"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR2JEl"), DibosonsTheoSR2J))
    generatorSyst.append((("diboson","SR2JMu"), DibosonsTheoSR2J))
    generatorSyst.append((("diboson","SR5JEM"), DibosonsTheoSR5J))
    generatorSyst.append((("diboson","SR2JEM"), DibosonsTheoSR2J))
    
    generatorSyst.append((("diboson","VR2J_1El"), DibosonsTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_1Mu"), DibosonsTheoVR2J_1))
    generatorSyst.append((("diboson","VR5J_1El"), DibosonsTheoVR5J_1))
    generatorSyst.append((("diboson","VR5J_1Mu"), DibosonsTheoVR5J_1))
    
    generatorSyst.append((("diboson","VR2J_2El"), DibosonsTheoVR2J_2))
    generatorSyst.append((("diboson","VR2J_2Mu"), DibosonsTheoVR2J_2))
    generatorSyst.append((("diboson","VR5J_2El"), DibosonsTheoVR5J_2))
    generatorSyst.append((("diboson","VR5J_2Mu"), DibosonsTheoVR5J_2))
        
    generatorSyst.append((("diboson","VR2J_1EM"), DibosonsTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_2EM"), DibosonsTheoVR2J_2))
    
    generatorSyst.append((("diboson","VR5J_1EM"), DibosonsTheoVR5J_1))
    generatorSyst.append((("diboson","VR5J_2EM"), DibosonsTheoVR5J_2))
    
    generatorSyst.append((("diboson","TR2JEl"), DibosonsTheoTR2J))
    generatorSyst.append((("diboson","TR2JMu"), DibosonsTheoTR2J))
    generatorSyst.append((("diboson","TR2JEM"), DibosonsTheoTR2J))
    generatorSyst.append((("diboson","WR2JEl"), DibosonsTheoWR2J))
    generatorSyst.append((("diboson","WR2JMu"), DibosonsTheoWR2J))
    generatorSyst.append((("diboson","WR2JEM"), DibosonsTheoWR2J))
    generatorSyst.append((("diboson","TR5JEl"), DibosonsTheoTR5J))
    generatorSyst.append((("diboson","TR5JMu"), DibosonsTheoTR5J))
    generatorSyst.append((("diboson","TR5JEM"), DibosonsTheoTR5J))
    generatorSyst.append((("diboson","WR5JEl"), DibosonsTheoWR5J))
    generatorSyst.append((("diboson","WR5JMu"), DibosonsTheoWR5J))
    generatorSyst.append((("diboson","WR5JEM"), DibosonsTheoWR5J))
    
    
    
    
    
   

  
    return generatorSyst
