import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

DibosonsScaleVarTheoSR2J= Systematic("DibosonsScaleVarTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
DibosonsScaleVarTheoSR5J = Systematic("DibosonsScaleVarTheo_SR5J",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")
DibosonsScaleVarTheoVR2J_1 = Systematic("DibosonsScaleVarTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
DibosonsScaleVarTheoVR2J_2 = Systematic("DibosonsScaleVarTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
DibosonsScaleVarTheoVR5J_1 = Systematic("DibosonsScaleVarTheo_VR5J_1",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")
DibosonsScaleVarTheoVR5J_2 = Systematic("DibosonsScaleVarTheo_VR5J_2",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")

DibosonsScaleVarTheoTR2J = Systematic("DibosonsScaleVarTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
DibosonsScaleVarTheoTR5J = Systematic("DibosonsScaleVarTheo_TR5J",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")
DibosonsScaleVarTheoWR2J = Systematic("DibosonsScaleVarTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")
DibosonsScaleVarTheoWR5J = Systematic("DibosonsScaleVarTheo_WR5J",configMgr.weights,configMgr.weights+["( 1+0.11)"],configMgr.weights+["( 1-0.11 )"], "weight","overallSys")









def TheorUnc(generatorSyst):
   
    generatorSyst.append((("diboson","SR5JEl"), DibosonsScaleVarTheoSR5J))
    generatorSyst.append((("diboson","SR5JMu"), DibosonsScaleVarTheoSR5J))
    generatorSyst.append((("diboson","SR2JEl"), DibosonsScaleVarTheoSR2J))
    generatorSyst.append((("diboson","SR2JMu"), DibosonsScaleVarTheoSR2J))
    generatorSyst.append((("diboson","SR5JEM"), DibosonsScaleVarTheoSR5J))
    generatorSyst.append((("diboson","SR2JEM"), DibosonsScaleVarTheoSR2J))
    
    generatorSyst.append((("diboson","VR2J_1El"), DibosonsScaleVarTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_1Mu"), DibosonsScaleVarTheoVR2J_1))
    generatorSyst.append((("diboson","VR5J_1El"), DibosonsScaleVarTheoVR5J_1))
    generatorSyst.append((("diboson","VR5J_1Mu"), DibosonsScaleVarTheoVR5J_1))
    
    generatorSyst.append((("diboson","VR2J_2El"), DibosonsScaleVarTheoVR2J_2))
    generatorSyst.append((("diboson","VR2J_2Mu"), DibosonsScaleVarTheoVR2J_2))
    generatorSyst.append((("diboson","VR5J_2El"), DibosonsScaleVarTheoVR5J_2))
    generatorSyst.append((("diboson","VR5J_2Mu"), DibosonsScaleVarTheoVR5J_2))
        
    generatorSyst.append((("diboson","VR2J_1EM"), DibosonsScaleVarTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_2EM"), DibosonsScaleVarTheoVR2J_2))
    
    generatorSyst.append((("diboson","VR5J_1EM"), DibosonsScaleVarTheoVR5J_1))
    generatorSyst.append((("diboson","VR5J_2EM"), DibosonsScaleVarTheoVR5J_2))
    
    generatorSyst.append((("diboson","TR2JEl"), DibosonsScaleVarTheoTR2J))
    generatorSyst.append((("diboson","TR2JMu"), DibosonsScaleVarTheoTR2J))
    generatorSyst.append((("diboson","TR2JEM"), DibosonsScaleVarTheoTR2J))
    generatorSyst.append((("diboson","WR2JEl"), DibosonsScaleVarTheoWR2J))
    generatorSyst.append((("diboson","WR2JMu"), DibosonsScaleVarTheoWR2J))
    generatorSyst.append((("diboson","WR2JEM"), DibosonsScaleVarTheoWR2J))
    generatorSyst.append((("diboson","TR5JEl"), DibosonsScaleVarTheoTR5J))
    generatorSyst.append((("diboson","TR5JMu"), DibosonsScaleVarTheoTR5J))
    generatorSyst.append((("diboson","TR5JEM"), DibosonsScaleVarTheoTR5J))
    generatorSyst.append((("diboson","WR5JEl"), DibosonsScaleVarTheoWR5J))
    generatorSyst.append((("diboson","WR5JMu"), DibosonsScaleVarTheoWR5J))
    generatorSyst.append((("diboson","WR5JEM"), DibosonsScaleVarTheoWR5J))
   

  
    return generatorSyst
