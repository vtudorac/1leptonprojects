import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#FactRenScaleISRFSRTTbarTheoSR2J = Systematic("FactRenScaleISRFSRTTbar_2J",configMgr.weights,1.0236 ,0.9765 ,"user","userOverallSys")
#FactRenScaleISRFSRTTbarTheoSR5J = Systematic("FactRenScaleISRFSRTTbar_5J",configMgr.weights,1.0459 ,0.9574 ,"user","userOverallSys")
FactRenScaleISRFSRTTbarTheoSR2J = Systematic("FactRenScaleISRFSRTTbar_SR2J",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
FactRenScaleISRFSRTTbarTheoSR5J = Systematic("FactRenScaleISRFSRTTbar_SR5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
FactRenScaleISRFSRTTbarTheoVR2J_1 = Systematic("FactRenScaleISRFSRTTbar_VR2J_1",configMgr.weights, 1.05 ,0.95,"user","userOverallSys")
FactRenScaleISRFSRTTbarTheoVR2J_2 = Systematic("FactRenScaleISRFSRTTbar_VR2J_2",configMgr.weights, 1.02 ,0.98 ,"user","userOverallSys")
FactRenScaleISRFSRTTbarTheoVR5J_1 = Systematic("FactRenScaleISRFSRTTbar_VR5J_1",configMgr.weights, 1.05 ,0.95,"user","userOverallSys")
FactRenScaleISRFSRTTbarTheoVR5J_2 = Systematic("FactRenScaleISRFSRTTbar_VR5J_2",configMgr.weights, 1.17 ,0.83,"user","userOverallSys")










def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR5JEl"), FactRenScaleISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), FactRenScaleISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEl"), FactRenScaleISRFSRTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), FactRenScaleISRFSRTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEM"), FactRenScaleISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEM"), FactRenScaleISRFSRTTbarTheoSR2J))
    
    generatorSyst.append((("ttbar","VR2J_1El"), FactRenScaleISRFSRTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), FactRenScaleISRFSRTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), FactRenScaleISRFSRTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), FactRenScaleISRFSRTTbarTheoVR5J_1))
    
    generatorSyst.append((("ttbar","VR2J_2El"), FactRenScaleISRFSRTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), FactRenScaleISRFSRTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), FactRenScaleISRFSRTTbarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), FactRenScaleISRFSRTTbarTheoVR5J_2))
        
    generatorSyst.append((("ttbar","VR2J_1EM"), FactRenScaleISRFSRTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), FactRenScaleISRFSRTTbarTheoVR2J_2))
    
    generatorSyst.append((("ttbar","VR5J_1EM"), FactRenScaleISRFSRTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), FactRenScaleISRFSRTTbarTheoVR5J_2))
    
    
   

  
    return generatorSyst
