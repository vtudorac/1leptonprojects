import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#FactScaleTTbarTheoSR2J = Systematic("FactScaleTTbar_2J",configMgr.weights,1.0236 ,0.9765 ,"user","userOverallSys")
#FactScaleTTbarTheoSR5J = Systematic("FactScaleTTbar_5J",configMgr.weights,1.0459 ,0.9574 ,"user","userOverallSys")
FactScaleTTbarTheoSR2J = Systematic("FactScaleTTbar_2J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
FactScaleTTbarTheoSR5J = Systematic("FactScaleTTbar_5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
FactScaleTTbarTheoVR2J_1 = Systematic("FactScaleTTbar_2J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
FactScaleTTbarTheoVR2J_2 = Systematic("FactScaleTTbar_2J",configMgr.weights, 1.15 ,0.85 ,"user","userOverallSys")
FactScaleTTbarTheoVR5J_1 = Systematic("FactScaleTTbar_5J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
FactScaleTTbarTheoVR5J_2 = Systematic("FactScaleTTbar_5J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")










def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR5JEl"), FactScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), FactScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEl"), FactScaleTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), FactScaleTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEM"), FactScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEM"), FactScaleTTbarTheoSR2J))
    
    generatorSyst.append((("ttbar","VR2J_1El"), FactScaleTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), FactScaleTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), FactScaleTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), FactScaleTTbarTheoVR5J_1))
    
    generatorSyst.append((("ttbar","VR2J_2El"), FactScaleTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), FactScaleTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), FactScaleTTbarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), FactScaleTTbarTheoVR5J_2))
        
    generatorSyst.append((("ttbar","VR2J_1EM"), FactScaleTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), FactScaleTTbarTheoVR2J_2))
    
    generatorSyst.append((("ttbar","VR5J_1EM"), FactScaleTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), FactScaleTTbarTheoVR5J_2))
    
    
   

  
    return generatorSyst
