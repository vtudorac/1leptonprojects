import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#HadFragTTbarTheoSR2J = Systematic("HadFragTTbar_2J",configMgr.weights,1.0236 ,0.9765 ,"user","userOverallSys")
#HadFragTTbarTheoSR5J = Systematic("HadFragTTbar_5J",configMgr.weights,1.0459 ,0.9574 ,"user","userOverallSys")
HadFragTTbarTheoSR2J = Systematic("HadFragTTbar_SR2J",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
HadFragTTbarTheoSR5J = Systematic("HadFragTTbar_SR5J",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
HadFragTTbarTheoVR2J_1 = Systematic("HadFragTTbar_VR2J_1",configMgr.weights, 1.04 ,0.96,"user","userOverallSys")
HadFragTTbarTheoVR2J_2 = Systematic("HadFragTTbar_VR2J_2",configMgr.weights, 1.01 ,0.99 ,"user","userOverallSys")
HadFragTTbarTheoVR5J_1 = Systematic("HadFragTTbar_VR5J_1",configMgr.weights, 1.04 ,0.96,"user","userOverallSys")
HadFragTTbarTheoVR5J_2 = Systematic("HadFragTTbar_VR5J_2",configMgr.weights, 1.01 ,0.99,"user","userOverallSys")










def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR5JEl"), HadFragTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), HadFragTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEl"), HadFragTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), HadFragTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEM"), HadFragTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEM"), HadFragTTbarTheoSR2J))
    
    generatorSyst.append((("ttbar","VR2J_1El"), HadFragTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), HadFragTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), HadFragTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), HadFragTTbarTheoVR5J_1))
    
    generatorSyst.append((("ttbar","VR2J_2El"), HadFragTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), HadFragTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), HadFragTTbarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), HadFragTTbarTheoVR5J_2))
        
    generatorSyst.append((("ttbar","VR2J_1EM"), HadFragTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), HadFragTTbarTheoVR2J_2))
    
    generatorSyst.append((("ttbar","VR5J_1EM"), HadFragTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), HadFragTTbarTheoVR5J_2))
    
    
   

  
    return generatorSyst
