import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#HardScatteringGenTTbarTheoSR2J = Systematic("HardScatteringGenTTbar_2J",configMgr.weights,1.0236 ,0.9765 ,"user","userOverallSys")
#HardScatteringGenTTbarTheoSR5J = Systematic("HardScatteringGenTTbar_5J",configMgr.weights,1.0459 ,0.9574 ,"user","userOverallSys")
HardScatteringGenTTbarTheoSR2J = Systematic("HardScatteringGenTTbar_SR2J",configMgr.weights,1.25 ,0.75 ,"user","userOverallSys")
HardScatteringGenTTbarTheoSR5J = Systematic("HardScatteringGenTTbar_SR5J",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
HardScatteringGenTTbarTheoVR2J_1 = Systematic("HardScatteringGenTTbar_VR2J_1",configMgr.weights, 1.06 ,0.94,"user","userOverallSys")
HardScatteringGenTTbarTheoVR2J_2 = Systematic("HardScatteringGenTTbar_VR2J_2",configMgr.weights, 1.12,0.88 ,"user","userOverallSys")
HardScatteringGenTTbarTheoVR5J_1 = Systematic("HardScatteringGenTTbar_VR5J_1",configMgr.weights, 1.04 ,0.96,"user","userOverallSys")
HardScatteringGenTTbarTheoVR5J_2 = Systematic("HardScatteringGenTTbar_VR5J_2",configMgr.weights, 1.14 ,0.86,"user","userOverallSys")











def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR5JEl"), HardScatteringGenTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), HardScatteringGenTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEl"), HardScatteringGenTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), HardScatteringGenTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEM"), HardScatteringGenTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEM"), HardScatteringGenTTbarTheoSR2J))
    
    generatorSyst.append((("ttbar","VR2J_1El"), HardScatteringGenTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), HardScatteringGenTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), HardScatteringGenTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), HardScatteringGenTTbarTheoVR5J_1))
    
    generatorSyst.append((("ttbar","VR2J_2El"), HardScatteringGenTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), HardScatteringGenTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), HardScatteringGenTTbarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), HardScatteringGenTTbarTheoVR5J_2))
        
    generatorSyst.append((("ttbar","VR2J_1EM"), HardScatteringGenTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), HardScatteringGenTTbarTheoVR2J_2))
    
    generatorSyst.append((("ttbar","VR5J_1EM"), HardScatteringGenTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), HardScatteringGenTTbarTheoVR5J_2))
    
    
   

  
    return generatorSyst
