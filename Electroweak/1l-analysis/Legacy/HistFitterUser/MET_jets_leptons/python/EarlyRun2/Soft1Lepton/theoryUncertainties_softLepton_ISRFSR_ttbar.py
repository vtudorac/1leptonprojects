import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

ISRFSRTTbarTheoSR2J = Systematic("ISRFSRTTbar_2J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
ISRFSRTTbarTheoSR5J = Systematic("ISRFSRTTbar_5J",configMgr.weights, 1.20 ,0.80 ,"user","userOverallSys")

ISRFSRTTbarTheoVR2J_1 = Systematic("ISRFSRTTbar_2J",configMgr.weights, 1.05 ,0.95,"user","userOverallSys")
ISRFSRTTbarTheoVR2J_2 = Systematic("ISRFSRTTbar_2J",configMgr.weights, 1.02 ,0.98 ,"user","userOverallSys")

ISRFSRTTbarTheoVR5J_1 = Systematic("ISRFSRTTbar_5J",configMgr.weights, 1.05 ,0.95,"user","userOverallSys")
ISRFSRTTbarTheoVR5J_2 = Systematic("ISRFSRTTbar_5J",configMgr.weights, 1.17 ,0.83,"user","userOverallSys")




def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR2JEl"), ISRFSRTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), ISRFSRTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEl"), ISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), ISRFSRTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEM"), ISRFSRTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEM"), ISRFSRTTbarTheoSR5J))
    
    
    generatorSyst.append((("ttbar","VR2J_1El"), ISRFSRTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), ISRFSRTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), ISRFSRTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), ISRFSRTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR2J_2El"), ISRFSRTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), ISRFSRTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), ISRFSRTTbarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), ISRFSRTTbarTheoVR5J_2))    
    generatorSyst.append((("ttbar","VR2J_1EM"), ISRFSRTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), ISRFSRTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_1EM"), ISRFSRTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), ISRFSRTTbarTheoVR5J_2))
   
   

  
    return generatorSyst
