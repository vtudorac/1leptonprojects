import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#PowhegJimmyTTbarTheoSR2J = Systematic("PowhegJimmyTTbar_2J",configMgr.weights, 1.0260 ,0.9781 ,"user","userOverallSys")
#PowhegJimmyTTbarTheoSR5J = Systematic("PowhegJimmyTTbar_5J",configMgr.weights, 1.0501 ,0.9526 ,"user","userOverallSys")

PowhegJimmyTTbarTheoSR2J = Systematic("PowhegJimmyTTbar_2J",configMgr.weights, 1.20 ,0.80 ,"user","userOverallSys")
PowhegJimmyTTbarTheoSR5J = Systematic("PowhegJimmyTTbar_5J",configMgr.weights, 1.20 ,0.80 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR2J_1 = Systematic("PowhegJimmyTTbar_2J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
PowhegJimmyTTbarTheoVR2J_2 = Systematic("PowhegJimmyTTbar_2J",configMgr.weights, 1.15 ,0.85 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR5J_1 = Systematic("PowhegJimmyTTbar_5J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
PowhegJimmyTTbarTheoVR5J_2 = Systematic("PowhegJimmyTTbar_5J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")






def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR2JEl"), PowhegJimmyTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), PowhegJimmyTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEl"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), PowhegJimmyTTbarTheoSR5J))   
    generatorSyst.append((("ttbar","SR2JEM"), PowhegJimmyTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR5JEM"), PowhegJimmyTTbarTheoSR5J))
    
    generatorSyst.append((("ttbar","VR2J_1El"), PowhegJimmyTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), PowhegJimmyTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), PowhegJimmyTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), PowhegJimmyTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR2J_2El"), PowhegJimmyTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), PowhegJimmyTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), PowhegJimmyTTbarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), PowhegJimmyTTbarTheoVR5J_2))    
    generatorSyst.append((("ttbar","VR2J_1EM"), PowhegJimmyTTbarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), PowhegJimmyTTbarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_1EM"), PowhegJimmyTTbarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), PowhegJimmyTTbarTheoVR5J_2))
    
    

   

  
    return generatorSyst
