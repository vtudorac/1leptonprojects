import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

#RenScaleTTbarTheoSR2J = Systematic("RenScaleTTbar_2J",configMgr.weights,1.0227 ,0.9792 ,"user","userOverallSys")
#RenScaleTTbarTheoSR5J = Systematic("RenScaleTTbar_5J",configMgr.weights,1.0528 ,0.9504 ,"user","userOverallSys")
RenScaleTTbarTheoSR2J = Systematic("RenScaleTTbar_2J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
RenScaleTTbarTheoSR5J = Systematic("RenScaleTTbar_5J",configMgr.weights,1.20 ,0.80 ,"user","userOverallSys")
RenScaleTTBarTheoVR2J_1 = Systematic("RenScaleTTBar_2J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
RenScaleTTBarTheoVR2J_2 = Systematic("RenScaleTTBar_2J",configMgr.weights, 1.15 ,0.85 ,"user","userOverallSys")
RenScaleTTBarTheoVR5J_1 = Systematic("RenScaleTTBar_5J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")
RenScaleTTBarTheoVR5J_2 = Systematic("RenScaleTTBar_5J",configMgr.weights, 1.15 ,0.85,"user","userOverallSys")









def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR5JEl"), RenScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR5JMu"), RenScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEl"), RenScaleTTbarTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), RenScaleTTbarTheoSR2J))   
    generatorSyst.append((("ttbar","SR5JEM"), RenScaleTTbarTheoSR5J))
    generatorSyst.append((("ttbar","SR2JEM"), RenScaleTTbarTheoSR2J))
    generatorSyst.append((("ttbar","VR2J_1El"), RenScaleTTBarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), RenScaleTTBarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR5J_1El"), RenScaleTTBarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_1Mu"), RenScaleTTBarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR2J_2El"), RenScaleTTBarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), RenScaleTTBarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_2El"), RenScaleTTBarTheoVR5J_2))
    generatorSyst.append((("ttbar","VR5J_2Mu"), RenScaleTTBarTheoVR5J_2))    
    generatorSyst.append((("ttbar","VR2J_1EM"), RenScaleTTBarTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), RenScaleTTBarTheoVR2J_2))
    generatorSyst.append((("ttbar","VR5J_1EM"), RenScaleTTBarTheoVR5J_1))
    generatorSyst.append((("ttbar","VR5J_2EM"), RenScaleTTBarTheoVR5J_2))
    
   

  
    return generatorSyst
