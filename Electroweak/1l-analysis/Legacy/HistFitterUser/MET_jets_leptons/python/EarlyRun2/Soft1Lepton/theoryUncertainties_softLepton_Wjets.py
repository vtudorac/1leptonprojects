import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


WjetsTheoSR2J = Systematic("WjetsTheo_SR2J",configMgr.weights,1.18 ,0.82 ,"user","userOverallSys")
WjetsTheoSR5J = Systematic("WjetsTheo_SR5J",configMgr.weights,1.34 ,0.66 ,"user","userOverallSys")
WjetsTheoVR2J_1 = Systematic("WjetsTheo_VR2J_1",configMgr.weights, 1.05 ,0.95,"user","userOverallSys")
WjetsTheoVR2J_2 = Systematic("WjetsTheo_VR2J_2",configMgr.weights, 1.14 ,0.86 ,"user","userOverallSys")
WjetsTheoVR5J_1 = Systematic("WjetsTheo_VR5J_1",configMgr.weights, 1.07 ,0.93,"user","userOverallSys")
WjetsTheoVR5J_2 = Systematic("WjetsTheo_VR5J_2",configMgr.weights, 1.30 ,0.70,"user","userOverallSys")










def TheorUnc(generatorSyst):
   
    generatorSyst.append((("wjets","SR5JEl"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR5JMu"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR2JEl"), WjetsTheoSR2J))
    generatorSyst.append((("wjets","SR2JMu"), WjetsTheoSR2J))
    generatorSyst.append((("wjets","SR5JEM"), WjetsTheoSR5J))
    generatorSyst.append((("wjets","SR2JEM"), WjetsTheoSR2J))
    
    generatorSyst.append((("wjets","VR2J_1El"), WjetsTheoVR2J_1))
    generatorSyst.append((("wjets","VR2J_1Mu"), WjetsTheoVR2J_1))
    generatorSyst.append((("wjets","VR5J_1El"), WjetsTheoVR5J_1))
    generatorSyst.append((("wjets","VR5J_1Mu"), WjetsTheoVR5J_1))
    
    generatorSyst.append((("wjets","VR2J_2El"), WjetsTheoVR2J_2))
    generatorSyst.append((("wjets","VR2J_2Mu"), WjetsTheoVR2J_2))
    generatorSyst.append((("wjets","VR5J_2El"), WjetsTheoVR5J_2))
    generatorSyst.append((("wjets","VR5J_2Mu"), WjetsTheoVR5J_2))
        
    generatorSyst.append((("wjets","VR2J_1EM"), WjetsTheoVR2J_1))
    generatorSyst.append((("wjets","VR2J_2EM"), WjetsTheoVR2J_2))
    
    generatorSyst.append((("wjets","VR5J_1EM"), WjetsTheoVR5J_1))
    generatorSyst.append((("wjets","VR5J_2EM"), WjetsTheoVR5J_2))
    
    
   

  
    return generatorSyst
