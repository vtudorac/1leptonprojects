import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


WjetsScaleVarTheoSR2J = Systematic("WjetsScaleVarTheo_SR2J",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")
WjetsScaleVarTheoSR5J = Systematic("WjetsScaleVarTheo_SR5J",configMgr.weights,1.11 ,0.89 ,"user","userOverallSys")
WjetsScaleVarTheoVR2J_1 = Systematic("WjetsScaleVarTheo_VR2J_1",configMgr.weights, 1.10 ,0.90,"user","userOverallSys")
WjetsScaleVarTheoVR2J_2 = Systematic("WjetsScaleVarTheo_VR2J_2",configMgr.weights, 1.13 ,0.87 ,"user","userOverallSys")
WjetsScaleVarTheoVR5J_1 = Systematic("WjetsScaleVarTheo_VR5J_1",configMgr.weights, 1.08 ,0.92,"user","userOverallSys")
WjetsScaleVarTheoVR5J_2 = Systematic("WjetsScaleVarTheo_VR5J_2",configMgr.weights, 1.07 ,0.93,"user","userOverallSys")










def TheorUnc(generatorSyst):
   
    generatorSyst.append((("wjets","SR5JEl"), WjetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR5JMu"), WjetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR2JEl"), WjetsScaleVarTheoSR2J))
    generatorSyst.append((("wjets","SR2JMu"), WjetsScaleVarTheoSR2J))
    generatorSyst.append((("wjets","SR5JEM"), WjetsScaleVarTheoSR5J))
    generatorSyst.append((("wjets","SR2JEM"), WjetsScaleVarTheoSR2J))
    
    generatorSyst.append((("wjets","VR2J_1El"), WjetsScaleVarTheoVR2J_1))
    generatorSyst.append((("wjets","VR2J_1Mu"), WjetsScaleVarTheoVR2J_1))
    generatorSyst.append((("wjets","VR5J_1El"), WjetsScaleVarTheoVR5J_1))
    generatorSyst.append((("wjets","VR5J_1Mu"), WjetsScaleVarTheoVR5J_1))
    
    generatorSyst.append((("wjets","VR2J_2El"), WjetsScaleVarTheoVR2J_2))
    generatorSyst.append((("wjets","VR2J_2Mu"), WjetsScaleVarTheoVR2J_2))
    generatorSyst.append((("wjets","VR5J_2El"), WjetsScaleVarTheoVR5J_2))
    generatorSyst.append((("wjets","VR5J_2Mu"), WjetsScaleVarTheoVR5J_2))
        
    generatorSyst.append((("wjets","VR2J_1EM"), WjetsScaleVarTheoVR2J_1))
    generatorSyst.append((("wjets","VR2J_2EM"), WjetsScaleVarTheoVR2J_2))
    
    generatorSyst.append((("wjets","VR5J_1EM"), WjetsScaleVarTheoVR5J_1))
    generatorSyst.append((("wjets","VR5J_2EM"), WjetsScaleVarTheoVR5J_2))
    
    
   

  
    return generatorSyst
