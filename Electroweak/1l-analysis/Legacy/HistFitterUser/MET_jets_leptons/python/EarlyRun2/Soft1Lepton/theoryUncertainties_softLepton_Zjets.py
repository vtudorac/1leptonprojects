import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


ZjetsTheoSR2J = Systematic("ZjetsTheo_SR2J",configMgr.weights,configMgr.weights+["( 1+0.15)"],configMgr.weights+["( 1-0.15 )"], "weight","overallSys")
ZjetsTheoSR5J = Systematic("ZjetsTheo_SR5J",configMgr.weights,configMgr.weights+["( 1+0.27)"],configMgr.weights+["( 1-0.27 )"], "weight","overallSys")
ZjetsTheoVR2J_1 = Systematic("ZjetsTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.15)"],configMgr.weights+["( 1-0.15 )"], "weight","overallSys")
ZjetsTheoVR2J_2 = Systematic("ZjetsTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.15)"],configMgr.weights+["( 1-0.15 )"], "weight","overallSys")
ZjetsTheoVR5J_1 = Systematic("ZjetsTheo_VR5J_1",configMgr.weights,configMgr.weights+["( 1+0.27)"],configMgr.weights+["( 1-0.27 )"], "weight","overallSys")
ZjetsTheoVR5J_2 = Systematic("ZjetsTheo_VR5J_2",configMgr.weights,configMgr.weights+["( 1+0.27)"],configMgr.weights+["( 1-0.27 )"], "weight","overallSys")

ZjetsTheoWR2J = Systematic("ZjetsTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.15)"],configMgr.weights+["( 1-0.15 )"], "weight","overallSys")
ZjetsTheoWR5J = Systematic("ZjetsTheo_WR5J",configMgr.weights,configMgr.weights+["( 1+0.27)"],configMgr.weights+["( 1-0.27 )"], "weight","overallSys")
ZjetsTheoTR2J = Systematic("ZjetsTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.15)"],configMgr.weights+["( 1-0.15 )"], "weight","overallSys")
ZjetsTheoTR5J = Systematic("ZjetsTheo_TR5J",configMgr.weights,configMgr.weights+["( 1+0.27)"],configMgr.weights+["( 1-0.27 )"], "weight","overallSys")








def TheorUnc(generatorSyst):
   
    generatorSyst.append((("zjets","SR5JEl"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR5JMu"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR2JEl"), ZjetsTheoSR2J))
    generatorSyst.append((("zjets","SR2JMu"), ZjetsTheoSR2J))
    generatorSyst.append((("zjets","SR5JEM"), ZjetsTheoSR5J))
    generatorSyst.append((("zjets","SR2JEM"), ZjetsTheoSR2J))
    
    generatorSyst.append((("zjets","VR2J_1El"), ZjetsTheoVR2J_1))
    generatorSyst.append((("zjets","VR2J_1Mu"), ZjetsTheoVR2J_1))
    generatorSyst.append((("zjets","VR5J_1El"), ZjetsTheoVR5J_1))
    generatorSyst.append((("zjets","VR5J_1Mu"), ZjetsTheoVR5J_1))
    
    generatorSyst.append((("zjets","VR2J_2El"), ZjetsTheoVR2J_2))
    generatorSyst.append((("zjets","VR2J_2Mu"), ZjetsTheoVR2J_2))
    generatorSyst.append((("zjets","VR5J_2El"), ZjetsTheoVR5J_2))
    generatorSyst.append((("zjets","VR5J_2Mu"), ZjetsTheoVR5J_2))
        
    generatorSyst.append((("zjets","VR2J_1EM"), ZjetsTheoVR2J_1))
    generatorSyst.append((("zjets","VR2J_2EM"), ZjetsTheoVR2J_2))
    
    generatorSyst.append((("zjets","VR5J_1EM"), ZjetsTheoVR5J_1))
    generatorSyst.append((("zjets","VR5J_2EM"), ZjetsTheoVR5J_2))
    
    generatorSyst.append((("zjets","TR2JEl"), ZjetsTheoTR2J))
    generatorSyst.append((("zjets","TR2JMu"), ZjetsTheoTR2J))
    generatorSyst.append((("zjets","TR2JEM"), ZjetsTheoTR2J))
    generatorSyst.append((("zjets","WR2JEl"), ZjetsTheoWR2J))
    generatorSyst.append((("zjets","WR2JMu"), ZjetsTheoWR2J))
    generatorSyst.append((("zjets","WR2JEM"), ZjetsTheoWR2J))
    generatorSyst.append((("zjets","TR5JEl"), ZjetsTheoTR5J))
    generatorSyst.append((("zjets","TR5JMu"), ZjetsTheoTR5J))
    generatorSyst.append((("zjets","TR5JEM"), ZjetsTheoTR5J))
    generatorSyst.append((("zjets","WR5JEl"), ZjetsTheoWR5J))
    generatorSyst.append((("zjets","WR5JMu"), ZjetsTheoWR5J))
    generatorSyst.append((("zjets","WR5JEM"), ZjetsTheoWR5J))
    
    
    
   

  
    return generatorSyst
