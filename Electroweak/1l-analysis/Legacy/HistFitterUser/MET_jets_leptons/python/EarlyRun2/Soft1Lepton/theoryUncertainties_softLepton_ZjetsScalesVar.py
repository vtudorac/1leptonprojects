import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


ZjetsScaleVarTheoSR2J = Systematic("ZjetsScaleVarTheo_SR2J",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
ZjetsScaleVarTheoSR5J = Systematic("ZjetsScaleVarTheo_SR5J",configMgr.weights,configMgr.weights+["( 1+0.09)"],configMgr.weights+["( 1-0.09 )"], "weight","overallSys")
ZjetsScaleVarTheoVR2J_1 = Systematic("ZjetsScaleVarTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
ZjetsScaleVarTheoVR2J_2 = Systematic("ZjetsScaleVarTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
ZjetsScaleVarTheoVR5J_1 = Systematic("ZjetsScaleVarTheo_VR5J_1",configMgr.weights,configMgr.weights+["( 1+0.09)"],configMgr.weights+["( 1-0.09 )"], "weight","overallSys")
ZjetsScaleVarTheoVR5J_2 = Systematic("ZjetsScaleVarTheo_VR5J_2",configMgr.weights,configMgr.weights+["( 1+0.09)"],configMgr.weights+["( 1-0.09 )"], "weight","overallSys")

ZjetsScaleVarTheoWR2J = Systematic("ZjetsScaleVarTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
ZjetsScaleVarTheoWR5J = Systematic("ZjetsScaleVarTheo_WR5J",configMgr.weights,configMgr.weights+["( 1+0.09)"],configMgr.weights+["( 1-0.09 )"], "weight","overallSys")
ZjetsScaleVarTheoTR2J = Systematic("ZjetsScaleVarTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
ZjetsScaleVarTheoTR5J = Systematic("ZjetsScaleVarTheo_TR5J",configMgr.weights,configMgr.weights+["( 1+0.09)"],configMgr.weights+["( 1-0.09 )"], "weight","overallSys")








def TheorUnc(generatorSyst):
   
    generatorSyst.append((("zjets","SR5JEl"), ZjetsScaleVarTheoSR5J))
    generatorSyst.append((("zjets","SR5JMu"), ZjetsScaleVarTheoSR5J))
    generatorSyst.append((("zjets","SR2JEl"), ZjetsScaleVarTheoSR2J))
    generatorSyst.append((("zjets","SR2JMu"), ZjetsScaleVarTheoSR2J))
    generatorSyst.append((("zjets","SR5JEM"), ZjetsScaleVarTheoSR5J))
    generatorSyst.append((("zjets","SR2JEM"), ZjetsScaleVarTheoSR2J))
    
    generatorSyst.append((("zjets","VR2J_1El"), ZjetsScaleVarTheoVR2J_1))
    generatorSyst.append((("zjets","VR2J_1Mu"), ZjetsScaleVarTheoVR2J_1))
    generatorSyst.append((("zjets","VR5J_1El"), ZjetsScaleVarTheoVR5J_1))
    generatorSyst.append((("zjets","VR5J_1Mu"), ZjetsScaleVarTheoVR5J_1))
    
    generatorSyst.append((("zjets","VR2J_2El"), ZjetsScaleVarTheoVR2J_2))
    generatorSyst.append((("zjets","VR2J_2Mu"), ZjetsScaleVarTheoVR2J_2))
    generatorSyst.append((("zjets","VR5J_2El"), ZjetsScaleVarTheoVR5J_2))
    generatorSyst.append((("zjets","VR5J_2Mu"), ZjetsScaleVarTheoVR5J_2))
        
    generatorSyst.append((("zjets","VR2J_1EM"), ZjetsScaleVarTheoVR2J_1))
    generatorSyst.append((("zjets","VR2J_2EM"), ZjetsScaleVarTheoVR2J_2))
    
    generatorSyst.append((("zjets","VR5J_1EM"), ZjetsScaleVarTheoVR5J_1))
    generatorSyst.append((("zjets","VR5J_2EM"), ZjetsScaleVarTheoVR5J_2))
    
    generatorSyst.append((("zjets","TR2JEl"), ZjetsScaleVarTheoTR2J))
    generatorSyst.append((("zjets","TR2JMu"), ZjetsScaleVarTheoTR2J))
    generatorSyst.append((("zjets","TR2JEM"), ZjetsScaleVarTheoTR2J))
    generatorSyst.append((("zjets","WR2JEl"), ZjetsScaleVarTheoWR2J))
    generatorSyst.append((("zjets","WR2JMu"), ZjetsScaleVarTheoWR2J))
    generatorSyst.append((("zjets","WR2JEM"), ZjetsScaleVarTheoWR2J))
    generatorSyst.append((("zjets","TR5JEl"), ZjetsScaleVarTheoTR5J))
    generatorSyst.append((("zjets","TR5JMu"), ZjetsScaleVarTheoTR5J))
    generatorSyst.append((("zjets","TR5JEM"), ZjetsScaleVarTheoTR5J))
    generatorSyst.append((("zjets","WR5JEl"), ZjetsScaleVarTheoWR5J))
    generatorSyst.append((("zjets","WR5JMu"), ZjetsScaleVarTheoWR5J))
    generatorSyst.append((("zjets","WR5JEM"), ZjetsScaleVarTheoWR5J))
   
   

  
    return generatorSyst
