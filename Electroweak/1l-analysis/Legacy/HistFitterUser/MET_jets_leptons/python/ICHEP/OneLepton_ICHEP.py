################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch


from logger import Logger
log = Logger('OneLepton')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onWestGrid='westgrid' in commands.getstatusoutput("hostname")[1] or fnmatch.fnmatch(commands.getstatusoutput("hostname")[1], 'u*')
onUBCAtlasServ='atlasserv' in commands.getstatusoutput("hostname")[1]
onMOGON="mogon" in commands.getstatusoutput("hostname")[1]
onDOL=False
if onDOL: onLxplus=True ## previous status does not detect the batch system in lxplus

debug = False #!!!
if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]
SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
#SystList.append("JVT")      # JVT scale factors
SystList.append("pileup")   # pileup (common)
SystList.append("BTag")     # b-tagging uncertainties
SystList.append("Dibosons") # scale variation for renormalization, factorization, resummation and generator comparison
SystList.append("Wjets")    # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
SystList.append("Ttbar")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
SystList.append("ttbarPDF") # 
SystList.append("wjetsPDF") # 
SystList.append("Zjets")    # scale variation for renormalization, factorization, resummation,matching

if debug:
    SystList=[]

# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=False

# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["2J"] #this is the default - modify from command line


# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 2J,4JhighxGG,4JlowxGG,4JlowxGGexcl,4JlowxGGdisc,5JremapGGx12,6JGGx12,4JSSlowx,5JSShighx,4JSSx12,5JSSx12 option)"
    
#remove later: warning not to use two towers at the same time at the moment.
if len(CRregions) > 1:
    print "FATAL: CR and SR are not yet orthogonal - don't run with more than one tower at the moment!"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["2J"] = False
ValidRegList["4JhighxGG"] = False
ValidRegList["4JlowxGG"] = False
ValidRegList["6JGGx12HM"] = False
ValidRegList["6JGGx12"] = False
ValidRegList["4JSSlowx"]  = False
ValidRegList["5JSShighx"] = False
ValidRegList["4JSSx12"]   = False
ValidRegList["5JSSx12"]   = False


# for tables (turn doTableInputs to True)
doTableInputs = False  #This effectively means no validation plots but only validation tables (but is 100x faster)
for cr in CRregions:
    if "2J" in cr and doTableInputs:
        ValidRegList["2J"] = True
    if "4JhighxGG" in cr and doTableInputs:
        ValidRegList["4JhighxGG"] = True	
    if "4JlowxGG" in cr and doTableInputs:
        ValidRegList["4JlowxGG"] = True
    if "6JGGx12HM" in cr and doTableInputs:
        ValidRegList["6JGGx12HM"] = True    	
    if "6JGGx12" in cr and doTableInputs:
        ValidRegList["6JGGx12"] = True    
    if "4JSSlowx" in cr and doTableInputs:
        ValidRegList["4JSSlowx"] = True    
    if "5JSShighx" in cr and doTableInputs:
        ValidRegList["5JSShighx"] = True   
    if "4JSSx12" in cr and doTableInputs:
        ValidRegList["4JSSx12"] = True   
    if "5JSSx12" in cr and doTableInputs:
        ValidRegList["5JSSx12"] = True             

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=False
# Default is to do shape fits here:
if 'SSlowx' in CRregions[0] :
    doShapeFit=False #no shape-fit in mt here -> no advantage over single bin observed
if 'lowxGG' in CRregions[0] :
    doShapeFit=False #no shape-fit in mt here, + use exclusion region
if '2J' in CRregions[0] :
    doShapeFit=False #no shape-fit in ht here -> would fix SR2J fit instability
    
if doShapeFit:
    print "Shape-fit enabled."

# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    sigSamples=["SS_oneStep_825_425_25"]
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
    
if myFitType==FitType.Exclusion and not onDOL:
    if 'GG_oneStep' in sigSamples[0]:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]

mylumi= 36.46161 #28.01286 #14.78446 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  

doOnlySignal=False
mysamples = ["Zjets","ttv","singletop","diboson","Wjets","ttbar","data"]

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "OneLepton"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
useBackupCache = True
if (onMOGON or onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "OneLepton"+analysissuffix_BackupCache+analysisextension
    if not onLxplus: histBackupCacheFileName = "/project/etp3/jlorenz/shape_fit/HistFitter_git/HistFitterUser/MET_jets_leptons/data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    else: histBackupCacheFileName = "data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

configMgr.fixSigXSec=True
useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = False 

#blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)
doBlindSRinBGfit  = False # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = ["SS_oneStep_725_375_25",
              "SS_oneStep_925_475_25",
              "SS_oneStep_800_790_60",
              "SS_oneStep_900_700_60",
              "SS_oneStep_900_800_60",
              "SS_oneStep_1200_1190_60",
              "GG_oneStep_1385_705_25",
              "GG_oneStep_1545_785_25",
              "GG_oneStep_1000_990_60",
              "GG_oneStep_1400_1060_60",
              "GG_oneStep_1400_1260_60",
              "GG_oneStep_1400_1390_60",
              "GG_oneStep_1600_1590_60",
              "GG_oneStep_1800_1790_60"]

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #
'''
#directory with background files:
inputDir="/data/vtudorac/Tag0602/"
inputDirData="/data/vtudorac/Tag0602/"
#signal files
inputDirSig="/data/vtudorac/Tag0602/"
'''

inputDir="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
inputDirSig="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
inputDirData="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"


if not onLxplus:
    inputDir="/project/etp5/SUSYInclusive/trees/T_06_05/"
    inputDirSig="/project/etp5/SUSYInclusive/trees/T_06_05/"
    inputDirData="/project/etp5/SUSYInclusive/trees/T_06_05/"

if onWestGrid :
    inputDir="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirSig="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirData="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"

if onUBCAtlasServ :
    inputDir="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirSig="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirData="/mnt/xrootdd/mgignac/MC15C/T_06_03/"

if onMOGON:
    inputDir="../InputTrees/"
    inputDirSig="../InputTrees/"
    inputDirData="../InputTrees/"

#background files, separately for electron and muon channel:
bgdFiles_e = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]
bgdFiles_m = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]
bgdFiles_em = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]

#CAUTION: Signal files not yet updated!
#signal files
if myFitType==FitType.Exclusion or doOnlySignal:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allTrees_T_06_05_skimslim_met200_oneStep.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allTrees_T_06_05_skimslim_met200_oneStep.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allTrees_T_06_05_skimslim_met200_oneStep.root"]
        
#data files
#dataFiles = [inputDirData+"allTrees_T_06_03_dataOnly.root"] # 949.592 /pb
#dataFiles = [inputDirData+"allDS2_T_06_03.root"]    # DS2: 13277.66 /pb (remember to check lumiErr: should be 2.9%)
#dataFiles = [inputDirData+"allDS2v01_T_06_03.root"] # DS2.1: 14784.46 /pb (remember to check lumiErr: should be 3.0%)
dataFiles = [inputDirData+"allTrees_T_06_05_data.root"] # DS3: 36461.61 /pb (remember to check lumiErr: should be 4.1%)

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","jvtWeight","SherpaVjetsNjetsWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights

#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")

muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")

#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#### electron related uncertainties acting on weights ####
#ID 
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Rec
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Iso
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")


# JVT weight systematics
JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyUp")
JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyDown")


# pileup weight systematics
pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

    eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
    eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}


SingleTopComm = {}
ZjetsComm = {}


for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 20% syst and an overallSys
    #basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.2,0.8,"user","userOverallSys"))
    
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_NoSys","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_NoSys","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_NoSys","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_NoSys","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_NoSys","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_NoSys","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6restTerm","_NoSys","_JET_EffectiveNP_6restTerm__1up","_JET_EffectiveNP_6restTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_NoSys","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_NoSys","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_NoSys","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_NoSys","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_NoSys","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_NoSys","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_NoSys","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_NoSys","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_NoSys","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_NoSys","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys"))

    	        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_ID","_NoSys","_MUONS_ID__1up","_MUONS_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_MS","_NoSys","_MUONS_MS__1up","_MUONS_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUONS_SCALE","_NoSys","_MUONS_SCALE__1up","_MUONS_SCALE__1down","tree","overallNormHistoSys"))
    if "LepEff" in SystList :
    	basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
    	basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys")) 
    	basicChanSyst[region].append(Systematic("EG_Eff",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
    	basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys")) 
    	basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys")) 

    if "JVT" in SystList :
    	basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))
    if "BTag" in SystList:
    	bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
    	cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
    	mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")
    	eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")
    	eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,eTagFromCHighWeights,eTagFromCLowWeights,"weight","overallNormHistoSys")

	
	
    SingleTopComm[region] = Systematic("h1L_SingleTopComm", configMgr.weights,configMgr.weights+["( 1+0.8 )"],configMgr.weights+["( 1-0.8 )"], "weight","overallSys")
    ZjetsComm[region] = Systematic("h1L_ZjetsComm", configMgr.weights,configMgr.weights+["( 1+0.5 )"],configMgr.weights+["( 1-0.5 )"], "weight","overallSys")
	
    #signal uncertainties                                                                                       
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

    #Generator Systematics
    generatorSyst = []  	
	
    if "Dibosons" in SystList and 'diboson' in mysamples:
        import ICHEP.theoryUncertainties_OneLepton_Dibosons
        ICHEP.theoryUncertainties_OneLepton_Dibosons.TheorUnc(generatorSyst)
    if "Wjets" in SystList and 'Wjets' in mysamples:
        import ICHEP.theoryUncertainties_OneLepton_Wjets
        ICHEP.theoryUncertainties_OneLepton_Wjets.TheorUnc(generatorSyst)
    if "Ttbar" in SystList and 'ttbar' in mysamples:
        import ICHEP.theoryUncertainties_OneLepton_Ttbar
        ICHEP.theoryUncertainties_OneLepton_Ttbar.TheorUnc(generatorSyst)
    if "ttbarPDF" in SystList and 'ttbar' in mysamples:
        import ICHEP.theoryUncertainties_OneLepton_ttbarPDF
        ICHEP.theoryUncertainties_OneLepton_ttbarPDF.TheorUnc(generatorSyst)
    if "wjetsPDF" in SystList and 'Wjets' in mysamples:
        import ICHEP.theoryUncertainties_OneLepton_wjetsPDF
        ICHEP.theoryUncertainties_OneLepton_wjetsPDF.TheorUnc(generatorSyst)		
    if "Zjets" in SystList and 'Zjets' in mysamples:
        import ICHEP.theoryUncertainties_OneLepton_Zjets
        ICHEP.theoryUncertainties_OneLepton_Zjets.TheorUnc(generatorSyst)
       
	
    

# ********************************************************************* #
#                              Background samples

# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets_Sherpa221"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)

#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)

#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
#DibosonsSample.setNormByTheory()

#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()

#
ZSampleName = "zjets_Sherpa22"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()

#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()

#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()

if doOnlySignal: 
    sigSample = Sample(sigSamples[0],kPink)    
    sigSample.setStatConfig(useStat)
    sigSample.setNormByTheory()
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)
    sigSample.setFileList(sigFiles_em[sigSamples[0]])

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&  nLep_base==1&&nLep_signal==1 && combinedTrigResultMET"## TSTcleaning here
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>7) "
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>6)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
TwoLepSelection = "&&  nLep_base>=2 && nLep_signal>=2 && combinedTrigResultMET"

# ------- 2J region
configMgr.cutsDict["SR2J"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200.  && met>460. && mt>100. && (met/meffInc30) > 0.35" + CommonSelection
configMgr.cutsDict["WR2J"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200.  && met>250. && met<460. && mt>40. && mt<80 && (met/meffInc30) > 0.35  && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["TR2J"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200.  && met>250. && met<460. && mt>40. && mt<80 && (met/meffInc30) > 0.35  && nBJet30_MV2c10>=1" + CommonSelection
configMgr.cutsDict["VR2J_1"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>460. && mt>40. && mt<100. && (met/meffInc30) > 0.35" + CommonSelection 
configMgr.cutsDict["VR2J_2"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>250. && met<460. && mt>80. && (met/meffInc30) > 0.35" + CommonSelection


configMgr.cutsDict["SR2Jnomet"]="lep1Pt<35&&nJet30>=2 && jet1Pt>200.&& mt>100.&& (met/meffInc30) > 0.35" + CommonSelection
configMgr.cutsDict["SR2Jnomt"]="lep1Pt<35&&nJet30>=2 && jet1Pt>200. && met>460. && (met/meffInc30) > 0.35" + CommonSelection

configMgr.cutsDict["VR2J1nomet"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>250. && mt>40.&& mt<80 && (met/meffInc30) > 0.35" + CommonSelection
configMgr.cutsDict["VR2J2nomt"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>250. && met<460. && mt>40. && (met/meffInc30) > 0.35" + CommonSelection

# ------- 4J regions for gluino gridx high x
configMgr.cutsDict["SR4JhighxGG"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt>475 && met/meffInc30>0.3"+ CommonSelection
configMgr.cutsDict["WR4JhighxGG"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt<100 && met/meffInc30<0.3 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["TR4JhighxGG"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt<100 && met/meffInc30<0.3 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["VR4JhighxGG_mt"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt>100 && met/meffInc30<0.3 && jet6Pt<30" + CommonSelection
configMgr.cutsDict["VR4JhighxGG_metovermeff"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt<100 && met/meffInc30>0.3" + CommonSelection
# data/mc plots
configMgr.cutsDict["SR4JhighxGGnomt"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && met/meffInc30>0.3"+ CommonSelection#dol
configMgr.cutsDict["SR4JhighxGGnometovermeff"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt>475"+ CommonSelection#dol
configMgr.cutsDict["CR4JhighxGGnomt"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && met/meffInc30<0.3 " + CommonSelection
configMgr.cutsDict["VR4JhighxGGnomt"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && met/meffInc30<0.3 && jet6Pt<30" + CommonSelection
configMgr.cutsDict["VR4JhighxGGnometovermeff"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt<100 " + CommonSelection
configMgr.cutsDict["TVR4JhighxGGnomt"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && met/meffInc30<0.3 && jet6Pt<30 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TVR4JhighxGGnometovermeff"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt<100 && nBJet30_MV2c10>0" + CommonSelection

# ------- 4J regions for gluino gridx low x
# SR for exclusion
configMgr.cutsDict["SR4JlowxGGexcl"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt>125 && JetAplanarity>0.03 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JlowxGGdisc"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt>125 && JetAplanarity>0.06 " + CommonSelection

# N-1 SR
configMgr.cutsDict["SR4JlowxGGexclnomt"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && JetAplanarity>0.03 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR4JlowxGGexclnoaplanarity"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt>125 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JlowxGGdiscnomt"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && JetAplanarity>0.06 " + CommonSelection
configMgr.cutsDict["SR4JlowxGGdiscnoaplanarity"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt>125 " + CommonSelection

# CR/VR for both exclusion and discovery SRs
# excl CR/VR are the same as disc CR/VR
configMgr.cutsDict["WR4JlowxGG"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt<100 && JetAplanarity<0.03 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["TR4JlowxGG"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt<100 && JetAplanarity<0.03 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["VR4JlowxGG_mt"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt>100 && JetAplanarity<0.03 && met/meffInc30<0.35" + CommonSelection
configMgr.cutsDict["VR4JlowxGG_aplanarity"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt<100 && JetAplanarity>0.03" + CommonSelection
# data/mc plots
configMgr.cutsDict["CR4JlowxGGnomt"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && JetAplanarity<0.03" + CommonSelection
configMgr.cutsDict["VR4JlowxGGnomt"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && JetAplanarity<0.03 && met/meffInc30<0.35" + CommonSelection
configMgr.cutsDict["VR4JlowxGGnoaplanarity"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt<100 " + CommonSelection
configMgr.cutsDict["TVR4JlowxGGnomt"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && JetAplanarity<0.03 && met/meffInc30<0.35 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TVR4JlowxGGnoaplanarity"]="nJet30>=4 && jet4Pt>100 && meffInc30>2000. && met>250 && mt<100 && nBJet30_MV2c10>0 " + CommonSelection


# ------- 6JGGx12 region
configMgr.cutsDict["SR6JGGx12"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["TR6JGGx12"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt<125. && mt>60. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2  && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["WR6JGGx12"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt<125. && mt>60. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2  && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["VR6JGGx12_mt"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && met>250&&met<400 && mt>125. && mt<300. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["VR6JGGx12_aplanarity"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250.  && mt<125. && mt>60. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection

configMgr.cutsDict["SR6JGGx12nomt"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["SR6JGGx12noJetAplanarity"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>1000. && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["VR6JGGx12nomt"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && met>250&&met<400 && mt>60. && meffInc30>1000. && JetAplanarity<0.04 && (met/meffInc30) > 0.2" + CommonSelection
configMgr.cutsDict["VR6JGGx12noaplanarity"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250.  && mt<125. && mt>60. && meffInc30>1000. && (met/meffInc30) > 0.2" + CommonSelection


# ------- 6JGGx12HM regions -- remapped from old 5J hard-lepton
configMgr.cutsDict["SR6JGGx12HM"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>2000. && JetAplanarity>0.04 && (met/meffInc30) > 0.1" + CommonSelection
configMgr.cutsDict["TR6JGGx12HM"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && mt<125. && mt>60. && met>200. && meffInc30>1500. && JetAplanarity < 0.04 && (met/meffInc30)>0.1 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["WR6JGGx12HM"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && mt<125. && mt>60. && met>200. && meffInc30>1500. && JetAplanarity < 0.04 && (met/meffInc30)>0.1 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["VR6JGGx12HM_mt"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && mt>125. && mt<350. && met>250. && meffInc30>1500. && JetAplanarity<0.04 && (met/meffInc30)>0.1" + CommonSelection
configMgr.cutsDict["VR6JGGx12HM_aplanarity"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && mt>60. && mt<125. && met>250. && meffInc30>1500. && JetAplanarity>0.04 && (met/meffInc30)>0.1" + CommonSelection

configMgr.cutsDict["SR6JGGx12HMnomt"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && met>250. && meffInc30>2000. && JetAplanarity>0.04 && (met/meffInc30) > 0.1" + CommonSelection
configMgr.cutsDict["SR6JGGx12HMnoJetAplanarity"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>2000. && (met/meffInc30) > 0.1" + CommonSelection
configMgr.cutsDict["VR6JGGx12HMnomt"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && mt>60 && met>250. && meffInc30>1500. && JetAplanarity<0.04 && (met/meffInc30)>0.1" + CommonSelection
configMgr.cutsDict["VR6JGGx12HMnoaplanarity"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30. && mt>60. && mt<125. && met>250. && meffInc30>1500. && (met/meffInc30)>0.1" + CommonSelection
	
	
# ------- 4JSSx12
configMgr.cutsDict["SR4JSSx12"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300 && nBJet30_MV2c10==0 && mt>175.0 && LepAplanarity>0.08" + CommonSelection
configMgr.cutsDict["WR4JSSx12"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300.0 && nBJet30_MV2c10==0 && mt>50.0 && mt<100.0 && LepAplanarity<0.08" + CommonSelection
configMgr.cutsDict["TR4JSSx12"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300.0 && nBJet30_MV2c10>0  && mt>50.0 && mt<100.0 && LepAplanarity<0.08" + CommonSelection
configMgr.cutsDict["DR4JSSx12"] = "lep1Pt>6.0 && nJet30>=2 && met>250.0 && mt>50.0 && mt<100.0 && nBJet30_MV2c10==0" + TwoLepSelection
configMgr.cutsDict["VR4JSSx12_DR"] = "lep1Pt>6.0 && nJet30>=2 && met>250.0 && mt>100.0 && nBJet30_MV2c10==0 && (lep1Charge==lep2Charge || (nLep_signal>=3 && nLep_signal==nLep_base))" + TwoLepSelection
configMgr.cutsDict["VR4JSSx12_mt"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>200.0 && nBJet30_MV2c10==0 && mt>100.0 && LepAplanarity<0.05" + CommonSelection
configMgr.cutsDict["VR4JSSx12_aplanarity"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300.0 && nBJet30_MV2c10==0 && mt>50.0 && mt<150.0 && LepAplanarity>0.08" + CommonSelection
configMgr.cutsDict["VR4JSSx12nomt"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>200.0 && nBJet30_MV2c10==0 && mt>50.0 && LepAplanarity<0.05" + CommonSelection 
configMgr.cutsDict["VR4JSSx12noaplanarity"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300.0 && nBJet30_MV2c10==0 && mt>50.0 && mt<150.0 " + CommonSelection 
configMgr.cutsDict["SR4JSSx12nomt"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300 && nBJet30_MV2c10==0 && mt>50.0 && LepAplanarity>0.08" + CommonSelection
configMgr.cutsDict["SR4JSSx12noaplanarity"] = "lep1Pt>35 && nJet30>=4 && jet4Pt>50.0 && meffInc30>1200.0 && met>300 && nBJet30_MV2c10==0 && mt>175.0" + CommonSelection

# ------- 5JSSx12
configMgr.cutsDict["SR5JSSx12"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>300.0 && (met/meffInc30)>0.2 && mt>175.0 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR5JSSx12"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>200.0 && met<300.0 && (met/meffInc30)>0.2 && mt>50.0 && mt<100.0 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["TR5JSSx12"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>200.0 && met<300.0 && (met/meffInc30)>0.2 && mt>50.0 && mt<100.0 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["DR5JSSx12"] = "lep1Pt>6.0 && nJet30>=3 && met>200.0 && mt>50.0 && mt<150.0 && nBJet30_MV2c10==0" + TwoLepSelection
configMgr.cutsDict["VR5JSSx12_DR"] = "lep1Pt>6.0 && nJet30>=3 && met>200.0 && mt>150.0 && nBJet30_MV2c10==0 && (lep1Charge==lep2Charge || (nLep_signal>=3 && nLep_signal==nLep_base))" + TwoLepSelection
configMgr.cutsDict["VR5JSSx12_mt"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>30.0 && met>200.0 && met<300.0 && (met/meffInc30)>0.0 && mt>100.0 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["VR5JSSx12_met"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>300.0 && (met/meffInc30)>0.2 && mt>50.0 && mt<150.0 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["VR5JSSx12nomt"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>30.0 && met>200.0 && met<300.0 && (met/meffInc30)>0.0 && mt>50.0 && nBJet30_MV2c10==0" + CommonSelection 
configMgr.cutsDict["VR5JSSx12nomet"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>200.0 && (met/meffInc30)>0.2 && mt>50.0 && mt<150.0 && nBJet30_MV2c10==0" + CommonSelection 
configMgr.cutsDict["SR5JSSx12nomt"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>300.0 && (met/meffInc30)>0.2 && mt>50.0 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR5JSSx12nomet"] = "lep1Pt>35 && nJet30>=5 && jet5Pt>50.0 && met>200.0 && (met/meffInc30)>0.2 && mt>175.0 && nBJet30_MV2c10==0" + CommonSelection


# ------- 4JSSlowx
configMgr.cutsDict["SR4JSSlowx"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10==0 && jet1Pt>250.0 && jet2Pt>250.0 && LepAplanarity>0.03 && mt>150.0 && mt<400.0" + CommonSelection
configMgr.cutsDict["WR4JSSlowx"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10==0 && jet1Pt>250.0 && jet2Pt>250.0 && LepAplanarity<0.03 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["TR4JSSlowx"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10>0 && jet1Pt>250.0 && jet2Pt>250.0 && LepAplanarity<0.03 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["DR4JSSlowx"] = "lep1Pt>6.0 && nJet30>=2 && met>250.0 && mt>50.0 && mt<100.0 && nBJet30_MV2c10==0" + TwoLepSelection
configMgr.cutsDict["VR4JSSlowx_DR"] = "lep1Pt>6.0 && nJet30>=2 && met>250.0 && mt>100.0 && nBJet30_MV2c10==0 && (lep1Charge==lep2Charge || (nLep_signal>=3 && nLep_signal==nLep_base))" + TwoLepSelection
configMgr.cutsDict["VR4JSSlowx_mt"] = "lep1Pt>35 && nJet30>=4 && met>200.0 && nBJet30_MV2c10==0 && jet1Pt>30.0 && jet2Pt>30.0 && LepAplanarity<0.03 && mt>150.0 && mt<400.0" + CommonSelection
configMgr.cutsDict["VR4JSSlowx_aplanarity"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10==0 && jet1Pt>250.0 && jet2Pt>200.0 && LepAplanarity>0.03 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["VR4JSSlowxnomt"] = "lep1Pt>35 && nJet30>=4 && met>200.0 && nBJet30_MV2c10==0 && jet1Pt>30.0 && jet2Pt>30.0 && LepAplanarity<0.03 && mt>50.0 && mt<400.0" + CommonSelection
configMgr.cutsDict["VR4JSSlowxnoaplanarity"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10==0 && jet1Pt>250.0 && jet2Pt>200.0 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["SR4JSSlowxnomt"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10==0 && jet1Pt>250.0 && jet2Pt>250.0 && LepAplanarity>0.03 && mt>50.0 && mt<400.0" + CommonSelection
configMgr.cutsDict["SR4JSSlowxnoaplanarity"] = "lep1Pt>35 && nJet30>=4 && met>250.0 && nBJet30_MV2c10==0 && jet1Pt>250.0 && jet2Pt>250.0 && mt>150.0 && mt<400.0" + CommonSelection


# ------- 5JSShighx
configMgr.cutsDict["SR5JSShighx"] = "lep1Pt>35 && nJet30>=5  && met>400.0 && LepAplanarity>0.03 && nBJet30_MV2c10==0 && mt>400.0" + CommonSelection
configMgr.cutsDict["WR5JSShighx"] = "lep1Pt>35 && nJet30>=5  && met>400.0 && LepAplanarity<0.03 && nBJet30_MV2c10==0  && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["TR5JSShighx"] = "lep1Pt>35 && nJet30>=5  && met>300.0 && LepAplanarity<0.03 && nBJet30_MV2c10>0 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["DR5JSShighx"] = "lep1Pt>6.0 && nJet30>=3 && met>200.0 && mt>50.0 && mt<150.0 && nBJet30_MV2c10==0" + TwoLepSelection
configMgr.cutsDict["VR5JSShighx_DR"] = "lep1Pt>6.0 && nJet30>=3 && met>200.0 && mt>150.0 && nBJet30_MV2c10==0 && (lep1Charge==lep2Charge || (nLep_signal>=3 && nLep_signal==nLep_base))" + TwoLepSelection
configMgr.cutsDict["VR5JSShighx_mt"] = "lep1Pt>35 && nJet30>=5 && met>200.0 && LepAplanarity<0.03 && nBJet30_MV2c10==0 && mt>100.0" + CommonSelection
configMgr.cutsDict["VR5JSShighx_aplanarity"] = "lep1Pt>35 && nJet30>=5  && met>400.0 && LepAplanarity>0.03 && nBJet30_MV2c10==0 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["VR5JSShighxnomt"] = "lep1Pt>35 && nJet30>=5 && met>200.0 && LepAplanarity<0.03 && nBJet30_MV2c10==0 && mt>50.0" + CommonSelection
configMgr.cutsDict["VR5JSShighxnoaplanarity"] = "lep1Pt>35 && nJet30>=5  && met>400.0 && nBJet30_MV2c10==0 && mt>50.0 && mt<100.0" + CommonSelection
configMgr.cutsDict["SR5JSShighxnomt"] = "lep1Pt>35 && nJet30>=5  && met>400.0 && LepAplanarity>0.03 && nBJet30_MV2c10==0 && mt>50.0" + CommonSelection
configMgr.cutsDict["SR5JSShighxnoaplanarity"] = "lep1Pt>35 && nJet30>=5  && met>400.0 && nBJet30_MV2c10==0 && mt>400.0" + CommonSelection



d=configMgr.cutsDict
#second part: splitting into electron and muon channel for validation purposes	d=configMgr.cutsDict
defined_regions = []
if '2J' in CRregions: 
    defined_regions+=['SR2J','TR2J','WR2J','VR2J_1','VR2J_2']
    if doNMinus1Plots :
        defined_regions+=['VR2J2nomt','VR2J1nomet','SR2Jnomet','SR2Jnomt']
if '4JhighxGG' in CRregions: 
    defined_regions+=['SR4JhighxGG','TR4JhighxGG','WR4JhighxGG','VR4JhighxGG_mt','VR4JhighxGG_metovermeff']
    if doNMinus1Plots :
        defined_regions+=['SR4JhighxGGnomt','SR4JhighxGGnometovermeff','CR4JhighxGGnomt','VR4JhighxGGnomt','VR4JhighxGGnometovermeff','TVR4JhighxGGnomt','TVR4JhighxGGnometovermeff']
if '4JlowxGG' in CRregions: 
    defined_regions+=['SR4JlowxGGexcl','SR4JlowxGGdisc','TR4JlowxGG','WR4JlowxGG','VR4JlowxGG_mt','VR4JlowxGG_aplanarity']
    if doNMinus1Plots :
        defined_regions+=['CR4JlowxGGnomt','VR4JlowxGGnomt','VR4JlowxGGnoaplanarity','TVR4JlowxGGnomt','TVR4JlowxGGnoaplanarity','SR4JlowxGGexclnomt','SR4JlowxGGexclnoaplanarity','SR4JlowxGGdiscnomt','SR4JlowxGGdiscnoaplanarity']
if '6JGGx12HM' in CRregions: 
    defined_regions+=['SR6JGGx12HM','TR6JGGx12HM','WR6JGGx12HM','VR6JGGx12HM_mt','VR6JGGx12HM_aplanarity']
    if doNMinus1Plots :
        defined_regions+=['VR6JGGx12HMnomt','VR6JGGx12HMnoaplanarity','SR6JGGx12HMnomt','SR6JGGx12HMnoJetAplanarity']
if '6JGGx12' in CRregions: 
    defined_regions+=['SR6JGGx12','TR6JGGx12','WR6JGGx12','VR6JGGx12_mt','VR6JGGx12_aplanarity']
    if doNMinus1Plots :
        defined_regions+=['VR6JGGx12nomt','VR6JGGx12noaplanarity','SR6JGGx12nomt','SR6JGGx12noJetAplanarity']


#### Squark regions ####
if '4JSSlowx' in CRregions:
    defined_regions+=['SR4JSSlowx','WR4JSSlowx','TR4JSSlowx','DR4JSSlowx','VR4JSSlowx_mt','VR4JSSlowx_aplanarity','VR4JSSlowx_DR'] 
    if doNMinus1Plots :
        defined_regions+=['VR4JSSlowxnomt','VR4JSSlowxnoaplanarity','SR4JSSlowxnomt','SR4JSSlowxnoaplanarity']

if '5JSShighx' in CRregions: 
    defined_regions+=['SR5JSShighx','WR5JSShighx','TR5JSShighx','DR5JSShighx','VR5JSShighx_mt','VR5JSShighx_aplanarity','VR5JSShighx_DR']
    if doNMinus1Plots :
        defined_regions+=['VR5JSShighxnomt','VR5JSShighxnoaplanarity','SR5JSShighxnomt','SR5JSShighxnoaplanarity']

if '4JSSx12' in CRregions: 
    defined_regions+=['SR4JSSx12','WR4JSSx12','TR4JSSx12','DR4JSSx12','VR4JSSx12_mt','VR4JSSx12_aplanarity','VR4JSSx12_DR']
    if doNMinus1Plots :
        defined_regions+=['VR4JSSx12nomt','VR4JSSx12noaplanarity','SR4JSSx12nomt','SR4JSSx12noaplanarity']

if '5JSSx12' in CRregions: 
    defined_regions+=['SR5JSSx12','WR5JSSx12','TR5JSSx12','DR5JSSx12','VR5JSSx12_mt','VR5JSSx12_met','VR5JSSx12_DR']
    if doNMinus1Plots :
        defined_regions+=['VR5JSSx12nomt','VR5JSSx12nomet','SR5JSSx12nomt','SR5JSSx12nomet']

for pre_region in defined_regions:
            configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection
            configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection
            configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection  


# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")

#creating now list of samples
samplelist=[]
if 'Zjets' in mysamples: samplelist.append(ZSample)
if 'ttv' in mysamples: samplelist.append(ttbarVSample)
if 'singletop' in mysamples: samplelist.append(SingleTopSample)
if 'diboson' in mysamples: samplelist.append(DibosonsSample)
if 'Wjets' in mysamples: samplelist.append(WSample)
if 'ttbar' in mysamples: samplelist.append(TTbarSample)
if 'data' in mysamples: samplelist.append(DataSample)

if doOnlySignal: samplelist = [sigSample,DataSample]

#bkgOnly.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
bkgOnly.addSamples(samplelist)
    
if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.041) ## DS3
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in ['2J','4JhighxGG','4JlowxGG','6JGGx12HM','6JGGx12','4JSSlowx','5JSShighx','4JSSx12','5JSSx12']:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []


######################################################
# Add channels to Bkg-only configuration             #
######################################################


if "2J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR2JEM"],1,0.5,1.5),[elmuChans["2J"],bVetoChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR2JEM"],1,0.5,1.5),[elmuChans["2J"],bReqChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
if "4JhighxGG" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JhighxGGEM"],1,0.5,1.5),[elmuChans["4JhighxGG"],bVetoChans["4JhighxGG"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JhighxGGEM"],1,0.5,1.5),[elmuChans["4JhighxGG"],bReqChans["4JhighxGG"]])
    bkgOnly.addBkgConstrainChannels(tmp)    
if "4JlowxGG" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JlowxGGEM"],1,0.5,1.5),[elmuChans["4JlowxGG"],bVetoChans["4JlowxGG"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JlowxGGEM"],1,0.5,1.5),[elmuChans["4JlowxGG"],bReqChans["4JlowxGG"]])
    bkgOnly.addBkgConstrainChannels(tmp)
if "6JGGx12" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6JGGx12EM"],1,0.5,1.5),[elmuChans["6JGGx12"],bVetoChans["6JGGx12"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6JGGx12EM"],1,0.5,1.5),[elmuChans["6JGGx12"],bReqChans["6JGGx12"]])
    bkgOnly.addBkgConstrainChannels(tmp)        
if "6JGGx12HM" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6JGGx12HMEM"],1,0.5,1.5),[elmuChans["6JGGx12HM"],bVetoChans["6JGGx12HM"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6JGGx12HMEM"],1,0.5,1.5),[elmuChans["6JGGx12HM"],bReqChans["6JGGx12HM"]])     
    bkgOnly.addBkgConstrainChannels(tmp)        
if "4JSSlowx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JSSlowxEM"],1,0.5,1.5),[elmuChans["4JSSlowx"],bVetoChans["4JSSlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JSSlowxEM"],1,0.5,1.5),[elmuChans["4JSSlowx"],bReqChans["4JSSlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    #tmp = appendTo(bkgOnly.addChannel("cuts",["DR4JSSlowxEM"],1,0.5,1.5),[elmuChans["4JSSlowx"],bVetoChans["4JSSlowx"]])
    #bkgOnly.addBkgConstrainChannels(tmp)
if "5JSShighx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR5JSShighxEM"],1,0.5,1.5),[elmuChans["5JSShighx"],bVetoChans["5JSShighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR5JSShighxEM"],1,0.5,1.5),[elmuChans["5JSShighx"],bReqChans["5JSShighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    #tmp = appendTo(bkgOnly.addChannel("cuts",["DR5JSShighxEM"],1,0.5,1.5),[elmuChans["5JSShighx"],bVetoChans["5JSShighx"]])
    #bkgOnly.addBkgConstrainChannels(tmp)
if "5JSSx12" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR5JSSx12EM"],1,0.5,1.5),[elmuChans["5JSSx12"],bVetoChans["5JSSx12"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR5JSSx12EM"],1,0.5,1.5),[elmuChans["5JSSx12"],bReqChans["5JSSx12"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    #tmp = appendTo(bkgOnly.addChannel("cuts",["DR5JSSx12EM"],1,0.5,1.5),[elmuChans["5JSSx12"],bVetoChans["5JSSx12"]])
    #bkgOnly.addBkgConstrainChannels(tmp)
if "4JSSx12" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4JSSx12EM"],1,0.5,1.5),[elmuChans["4JSSx12"],bVetoChans["4JSSx12"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4JSSx12EM"],1,0.5,1.5),[elmuChans["4JSSx12"],bReqChans["4JSSx12"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    #tmp = appendTo(bkgOnly.addChannel("cuts",["DR4JSSx12EM"],1,0.5,1.5),[elmuChans["4JSSx12"],bVetoChans["4JSSx12"]])
    #bkgOnly.addBkgConstrainChannels(tmp)
            

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]=False
for region in CRregions:
    ValidRegList["OneLep"]  += ValidRegList[region]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
       for region in CRregions:
           appendIfMatchName(c,bReqChans[region])
           appendIfMatchName(c,bVetoChans[region])
           appendIfMatchName(c,bAgnosticChans[region])
           appendIfMatchName(c,elChans[region])
           appendIfMatchName(c,muChans[region])      
           appendIfMatchName(c,elmuChans[region])
	   
	
	
   
    if doTableInputs:
        if "2J" in CRregions:
         
            appendTo(validation.addValidationChannel("cuts",["SR2JEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JEM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
          
            #appendTo(validation.addValidationChannel("cuts",["VR2J_1El"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["VR2J_1Mu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2J_1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["VR2J_2El"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["VR2J_2Mu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2J_2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("mt",["SR2JnomtEM"],5,50,300),[bAgnosticChans["2J"],elChans["2J"]])
                appendTo(validation.addValidationChannel("met",["SR2JnometEM"],6,250,670),[bAgnosticChans["2J"],elChans["2J"]])
                appendTo(validation.addValidationChannel("mt",["VR2J2nomtEM"],5,25,225),[bAgnosticChans["2J"],elmuChans["2J"]])
                appendTo(validation.addValidationChannel("met",["VR2J1nometEM"],10,250,600),[bAgnosticChans["2J"],elmuChans["2J"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR2JEM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JEl"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JMu"]).doBlindingOverwrite = True

        if "4JhighxGG" in CRregions:
            
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxGGEl"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],elChans["4JhighxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxGGMu"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],muChans["4JhighxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxGGEM"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])

            #appendTo(validation.addValidationChannel("cuts",["VR4JhighxGG_metovermeffEl"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],elChans["4JhighxGG"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JhighxGG_metovermeffMu"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],muChans["4JhighxGG"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JhighxGG_metovermeffEM"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JhighxGG_mtEl"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],elChans["4JhighxGG"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JhighxGG_mtMu"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],muChans["4JhighxGG"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JhighxGG_mtEM"],1,0.5,1.5),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]]) 
            
            if doNMinus1Plots :
                #appendTo(validation.addValidationChannel("mt",["CR4JhighxGGnomtEM"],5,0.0,500.0),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])
                appendTo(validation.addValidationChannel("mt",["VR4JhighxGGnomtEM"],5,0.0,500.0),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])
                appendTo(validation.addValidationChannel("met/meffInc30",["VR4JhighxGGnometovermeffEM"],5,0,0.5),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])
                appendTo(validation.addValidationChannel("mt",["SR4JhighxGGnomtEM"],4,175.0,575.0),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])#dol
                appendTo(validation.addValidationChannel("met/meffInc30",["SR4JhighxGGnometovermeffEM"],5,0,0.5),[bAgnosticChans["4JhighxGG"],elmuChans["4JhighxGG"]])#dol
                #appendTo(validation.addValidationChannel("mt",["TVR4JhighxGGnomtEM"],5,0.0,500.0),[bReqChans["4JhighxGG"],elmuChans["4JhighxGG"]])
                #appendTo(validation.addValidationChannel("met/meffInc30",["TVR4JhighxGGnometovermeffEM"],5,0,0.5),[bReqChans["4JhighxGG"],elmuChans["4JhighxGG"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR4JhighxGGEM"]).doBlindingOverwrite = True        
                validation.getChannel("cuts",["SR4JhighxGGEl"]).doBlindingOverwrite = True        
                validation.getChannel("cuts",["SR4JhighxGGMu"]).doBlindingOverwrite = True        

        if "4JlowxGG" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxGGexclEl"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxGGexclMu"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],muChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxGGexclEM"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxGGdiscEl"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxGGdiscMu"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],muChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxGGdiscEM"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])

            #appendTo(validation.addValidationChannel("cuts",["VR4JlowxGG_mtEl"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elChans["4JlowxGG"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JlowxGG_mtMu"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],muChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JlowxGG_mtEM"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]]) 
            #appendTo(validation.addValidationChannel("cuts",["VR4JlowxGG_aplanarityEl"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elChans["4JlowxGG"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JlowxGG_aplanarityMu"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],muChans["4JlowxGG"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JlowxGG_aplanarityEM"],1,0.5,1.5),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
            
            if doNMinus1Plots :
                #appendTo(validation.addValidationChannel("mt",["CR4JlowxGGnomtEM"],5,0.,500.0),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                appendTo(validation.addValidationChannel("mt",["VR4JlowxGGnomtEM"],5,0.,500.0),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["VR4JlowxGGnoaplanarityEM"],10,0,0.1),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                #appendTo(validation.addValidationChannel("mt",["TVR4JlowxGGnomtEM"],5,0.,500.0),[bReqChans["4JlowxGG"],elmuChans["4JlowxGG"]]) 
                #appendTo(validation.addValidationChannel("JetAplanarity",["TVR4JlowxGGnoaplanarityEM"],10,0,0.1),[bReqChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                appendTo(validation.addValidationChannel("mt",["SR4JlowxGGexclnomtEM"],4,25,425),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["SR4JlowxGGexclnoaplanarityEM"],6,0,0.09),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                appendTo(validation.addValidationChannel("mt",["SR4JlowxGGdiscnomtEM"],4,25,425),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["SR4JlowxGGdiscnoaplanarityEM"],6,0,0.09),[bAgnosticChans["4JlowxGG"],elmuChans["4JlowxGG"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR4JlowxGGexclEM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxGGdiscEM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxGGexclEl"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxGGdiscEl"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxGGexclMu"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxGGdiscMu"]).doBlindingOverwrite = True

        if "6JGGx12" in CRregions:
            
            appendTo(validation.addValidationChannel("cuts",["SR6JGGx12El"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],elChans["6JGGx12"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JGGx12Mu"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],muChans["6JGGx12"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JGGx12EM"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12_mtEl"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],elChans["6JGGx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12_mtMu"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],muChans["6JGGx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR6JGGx12_mtEM"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])           
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12_aplanarityEl"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],elChans["6JGGx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12_aplanarityMu"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],muChans["6JGGx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR6JGGx12_aplanarityEM"],1,0.5,1.5),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])
            
            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("mt",["SR6JGGx12nomtEM"],6,65.0,545.0),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["SR6JGGx12noJetAplanarityEM"],6,0.0,0.12),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])
                appendTo(validation.addValidationChannel("mt",["VR6JGGx12nomtEM"],5,25,350),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["VR6JGGx12noaplanarityEM"],5,0,0.1),[bAgnosticChans["6JGGx12"],elmuChans["6JGGx12"]])
	    
            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR6JGGx12EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JGGx12El"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JGGx12Mu"]).doBlindingOverwrite = True
  
        if "6JGGx12HM" in CRregions:

            appendTo(validation.addValidationChannel("cuts",["SR6JGGx12HMEl"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],elChans["6JGGx12HM"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JGGx12HMMu"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],muChans["6JGGx12HM"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JGGx12HMEM"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12HM_mtEl"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],elChans["6JGGx12HM"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12HM_mtMu"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],muChans["6JGGx12HM"]])
            appendTo(validation.addValidationChannel("cuts",["VR6JGGx12HM_mtEM"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12HM_aplanarityEl"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],elChans["6JGGx12HM"]])
            #appendTo(validation.addValidationChannel("cuts",["VR6JGGx12HM_aplanarityMu"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],muChans["6JGGx12HM"]])
            appendTo(validation.addValidationChannel("cuts",["VR6JGGx12HM_aplanarityEM"],1,0.5,1.5),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]])
	    
            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("mt",["SR6JGGx12HMnomtEM"],6,65.0,545.0),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["SR6JGGx12HMnoJetAplanarityEM"],6,0.0,0.12),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]]) 
                appendTo(validation.addValidationChannel("mt",["VR6JGGx12HMnomtEM"],5,25,350),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]])
                appendTo(validation.addValidationChannel("JetAplanarity",["VR6JGGx12HMnoaplanarityEM"],5,0,0.1),[bAgnosticChans["6JGGx12HM"],elmuChans["6JGGx12HM"]])      
      

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR6JGGx12HMEM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JGGx12HMEl"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JGGx12HMMu"]).doBlindingOverwrite = True

        if "4JSSlowx" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR4JSSlowxEl"],1,0.5,1.5),[bVetoChans["4JSSlowx"],elChans["4JSSlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JSSlowxMu"],1,0.5,1.5),[bVetoChans["4JSSlowx"],muChans["4JSSlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JSSlowxEM"],1,0.5,1.5),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
            #appendTo(validation.addValidationChannel("mt",["VR4JSSlowx_mtEl"],1,0.5,1.5),[bVetoChans["4JSSlowx"],elChans["4JSSlowx"]])
            #appendTo(validation.addValidationChannel("mt",["VR4JSSlowx_mtMu"],1,0.5,1.5),[bVetoChans["4JSSlowx"],muChans["4JSSlowx"]])
            appendTo(validation.addValidationChannel("mt",["VR4JSSlowx_mtEM"],5,150.0,400.0),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JSSlowx_DREM"],1,0.5,1.5),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JSSlowx_aplanarityEl"],1,0.5,1.5),[bVetoChans["4JSSlowx"],elChans["4JSSlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JSSlowx_aplanarityMu"],1,0.5,1.5),[bVetoChans["4JSSlowx"],muChans["4JSSlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JSSlowx_aplanarityEM"],1,0.5,1.5),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
            
            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("LepAplanarity",["VR4JSSlowxnoaplanarityEM"],8,0,0.12),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
                appendTo(validation.addValidationChannel("mt",["VR4JSSlowxnomtEM"],7,50.0,400.0),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
                appendTo(validation.addValidationChannel("mt",["SR4JSSlowxnomtEM"],7,50.0,400.0),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])
                appendTo(validation.addValidationChannel("LepAplanarity",["SR4JSSlowxnoaplanarityEM"],4,0.0,0.12),[bVetoChans["4JSSlowx"],elmuChans["4JSSlowx"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR4JSSlowxEl"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JSSlowxMu"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JSSlowxEM"]).doBlindingOverwrite = True


        if "5JSShighx" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR5JSShighxEl"],1,0.5,1.5),[bVetoChans["5JSShighx"],elChans["5JSShighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR5JSShighxMu"],1,0.5,1.5),[bVetoChans["5JSShighx"],muChans["5JSShighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR5JSShighxEM"],1,0.5,1.5),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_mtEl"],1,0.5,1.5),[bVetoChans["5JSShighx"],elChans["5JSShighx"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_mtMu"],1,0.5,1.5),[bVetoChans["5JSShighx"],muChans["5JSShighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_mtEM"],1,0.5,1.5),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_DREM"],1,0.5,1.5),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_aplanarityEl"],1,0.5,1.5),[bVetoChans["5JSShighx"],elChans["5JSShighx"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_aplanarityMu"],1,0.5,1.5),[bVetoChans["5JSShighx"],muChans["5JSShighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR5JSShighx_aplanarityEM"],1,0.5,1.5),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])

            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("LepAplanarity",["VR5JSShighxnoaplanarityEM"],8,0,0.12),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])
                appendTo(validation.addValidationChannel("mt",["VR5JSShighxnomtEM"],10,50.0,550.0),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])
                appendTo(validation.addValidationChannel("mt",["SR5JSShighxnomtEM"],10,50.0,550.0),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])
                appendTo(validation.addValidationChannel("LepAplanarity",["SR5JSShighxnoaplanarityEM"],3,0.0,0.09),[bVetoChans["5JSShighx"],elmuChans["5JSShighx"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR5JSShighxEl"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR5JSShighxMu"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR5JSShighxEM"]).doBlindingOverwrite = True

        if "5JSSx12" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR5JSSx12El"],1,0.5,1.5),[bVetoChans["5JSSx12"],elChans["5JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["SR5JSSx12Mu"],1,0.5,1.5),[bVetoChans["5JSSx12"],muChans["5JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["SR5JSSx12EM"],1,0.5,1.5),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_mtEl"],1,0.5,1.5),[bVetoChans["5JSSx12"],elChans["5JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_mtMu"],1,0.5,1.5),[bVetoChans["5JSSx12"],elChans["5JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_mtEM"],1,0.5,1.5),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_DREM"],1,0.5,1.5),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_metEl"],1,0.5,1.5),[bVetoChans["5JSSx12"],elChans["5JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_metMu"],1,0.5,1.5),[bVetoChans["5JSSx12"],elChans["5JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR5JSSx12_metEM"],1,0.5,1.5),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
            
            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("mt",["VR5JSSx12nomtEM"],5,50,300),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
                appendTo(validation.addValidationChannel("met",["VR5JSSx12nometEM"],6,200,500),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
                appendTo(validation.addValidationChannel("mt",["SR5JSSx12nomtEM"],6,75.0,375.0),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])
                appendTo(validation.addValidationChannel("met",["SR5JSSx12nometEM"],6,200.0,500.0),[bVetoChans["5JSSx12"],elmuChans["5JSSx12"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR5JSSx12El"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR5JSSx12Mu"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR5JSSx12EM"]).doBlindingOverwrite = True


        if "4JSSx12" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SR4JSSx12El"],1,0.5,1.5),[bVetoChans["4JSSx12"],elChans["4JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JSSx12Mu"],1,0.5,1.5),[bVetoChans["4JSSx12"],muChans["4JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JSSx12EM"],1,0.5,1.5),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_mtEl"],1,0.5,1.5),[bVetoChans["4JSSx12"],elChans["4JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_mtMu"],1,0.5,1.5),[bVetoChans["4JSSx12"],elChans["4JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_mtEM"],1,0.5,1.5),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_DREM"],1,0.5,1.5),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_aplanarityEl"],1,0.5,1.5),[bVetoChans["4JSSx12"],elChans["4JSSx12"]])
            #appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_aplanarityMu"],1,0.5,1.5),[bVetoChans["4JSSx12"],elChans["4JSSx12"]])
            appendTo(validation.addValidationChannel("cuts",["VR4JSSx12_aplanarityEM"],1,0.5,1.5),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
            
            if doNMinus1Plots :
                appendTo(validation.addValidationChannel("mt",["VR4JSSx12nomtEM"],5,50,300),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
                appendTo(validation.addValidationChannel("LepAplanarity",["VR4JSSx12noaplanarityEM"],10,0,0.2),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
                appendTo(validation.addValidationChannel("mt",["SR4JSSx12nomtEM"],4,75.0,275.0),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])
                appendTo(validation.addValidationChannel("LepAplanarity",["SR4JSSx12noaplanarityEM"],4,0.0,0.16),[bVetoChans["4JSSx12"],elmuChans["4JSSx12"]])


            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR4JSSx12El"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JSSx12Mu"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JSSx12EM"]).doBlindingOverwrite = True

        # All validation regions should use over-flow bins!
        for v in validation.channels : 
            if not 'cuts' in v.name: v.useOverflowBin=True

    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs       
    
   
	
	
	
	
	        
      




# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    if '2J' in CRregions:
        SRs=["SR2JEM"]
    if '4JhighxGG' in CRregions:
        SRs=["SR4JhighxGGEM"]
    if '4JlowxGG' in CRregions:
        SRs=["SR4JlowxGGexclEM"]
    if '6JGGx12HM' in CRregions:
        SRs=["SR6JGGx12HMEM"]	
    if '4JSSlowx' in CRregions:
        SRs=["SR4JSSlowxEM"]
    if '5JSShighx' in CRregions:
        SRs=["SR5JSShighxEM"]
    if '5JSSx12' in CRregions:
        SRs=["SR5JSSx12EM"]
    if '4JSSx12' in CRregions:
        SRs=["SR4JSSx12EM"]

    elif '6JGGx12' in CRregions:
        SRs=["SR6JGGx12EM"] 
    

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
                
        #signal-specific uncertainties  
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(xsecSig)
        ######### Signal uncertainties #####################                                               
        errsig = 0.001 ## still need inputs for other regions
        if '2J' in CRregions: 
            if 'GG' in sigSamples[0]: errsig = 0.12
            if 'SS' in sigSamples[0]: errsig = 0.15

        if '6JGGx12HM' in CRregions: errsig = 0.10
        if '6JGGx12'   in CRregions: errsig = 0.035
        if '4JlowxGG'  in CRregions:  errsig = 0.10
        if '4JhighxGG' in CRregions: errsig = 0.15

        if '4JSSx12' in CRregions: errsig = 0.14
        if '5JSSx12'   in CRregions: errsig = 0.07
        if '4JSSlowx'  in CRregions:  errsig = 0.06
        if '5JSShighx' in CRregions: errsig = 0.14
        #print "sig=",sig,"  region=",ERRregion,"errsig=",errsig
        systSig = Systematic("SigSyst",configMgr.weights,1.00+errsig,1.00-errsig,"user","userOverallSys")
        #################################################                                               

        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if not doShapeFit:
                ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
            else:
                if sr=="SR4JlowxGGexclEl" or sr=="SR4JlowxGGexclMu" or sr=="SR4JlowxGGexclEM":
                    ch = myTopLvl.addChannel("mt",[sr],6,125,425)
                    ch.useOverflowBin=True      
                elif sr=="SR6JGGx12El" or sr=="SR6JGGx12Mu" or sr=="SR6JGGx12EM":
                    ch = myTopLvl.addChannel("cuts",[sr],4,1000,2000)
                    ch.useOverflowBin=True
                elif 'SR4JSSlowx' in sr:
                    ch = myTopLvl.addChannel("mt",[sr],6,150,400)
                    ch.useOverflowBin=True      
                elif 'SR2J' in sr: #!!!
                    ch = myTopLvl.addChannel("Ht30",[sr],2,250,1250)
                    ch.useOverflowBin=True  
                else: 
                    raise RuntimeError("This region is not yet implemented in a shape fit mode: %s"%sr)

            for region in CRregions:
                if region in sr:
                    if 'El' in sr:                
                        elChans[region].append(ch) 
                    elif 'Mu' in sr:
                        muChans[region].append(ch)
                    elif 'EM' in sr:
                        elmuChans[region].append(ch)
                    else: raise RuntimeError("Unexpected signal region %s"%sr)
                    bAgnosticChans[region].append(ch)
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.getSample(sig).addSystematic(systSig) 
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)
        

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]
    

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan = tgt[1]
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in CRregions:
    	SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
    	SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
    	SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
	    
            #if "SR2J" in chan.name:	    
                #theSample.removeWeight("pileupWeight")
                #theSample.removeSystematic("pileup")
             
            sys_region = ""
            if "2J" in chan.name: sys_region = "2J"
            elif "4JhighxGG" in chan.name: sys_region = "4JhighxGG"
            elif "4JlowxGG" in chan.name: sys_region = "4JlowxGG"
            elif "6JGGx12HM" in chan.name: sys_region = "6JGGx12HM"
            elif "6JGGx12" in chan.name: sys_region = "6JGGx12"
            elif "4JSSlowx" in chan.name: sys_region = "4JSSlowx"
            elif "5JSShighx" in chan.name: sys_region = "5JSShighx"  
            elif "4JSSx12" in chan.name: sys_region = "4JSSx12"
            elif "5JSSx12" in chan.name: sys_region = "5JSSx12"            
            else: 
                print "Unknown region! - Take systematics from 6JGGx12 regions."
                sys_region = "6JGGx12"

            # replacement of JES PunchThrough systematic for AF2 signal samples (not for FullSim signal samples)
            if not debug and not (sig in FullSimSig):
                print "This is an AFII signal sample -> removing JES_PunchThrough_MC15, adding JES_PunchThrough_AFII and JES_RelativeNonClosure_AFII systematics"
                theSample.removeSystematic("JES_PunchThrough_MC15")
                theSample.addSystematic(Systematic("JES_PunchThrough_AFII","_NoSys","_JET_PunchThrough_AFII__1up","_JET_PunchThrough_AFII__1down","tree","overallNormHistoSys"))
                theSample.addSystematic(Systematic("JES_RelativeNonClosure_AFII","_NoSys","_JET_RelativeNonClosure_AFII__1up","_JET_RelativeNonClosure_AFII__1down","tree","overallNormHistoSys"))            

            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans_all:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all:
                theSigFiles = sigFiles_em[sig]	                  
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")
        
    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            if 'Zjets' in mysamples: chan.getSample("zjets_Sherpa22").addSystematic(ZjetsComm[region])
            if 'singletop' in mysamples: chan.getSample("singletop").addSystematic(SingleTopComm[region])
            
            #if not 'SS' in chan.name:
            if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
            if 'Wjets' in mysamples: chan.getSample(WSampleName).setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts")])
            #else:
            #    chan.getSample("ttbar").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts"),("DR"+region+"EM","cuts")])
            #    chan.getSample(WSampleName).setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts"),("DR"+region+"EM","cuts")])           
            #    chan.getSample("diboson").setNormRegions([("WR"+region+"EM","cuts"),("TR"+region+"EM","cuts"),("DR"+region+"EM","cuts")])
            pass
   
# different diboson approaches in the background estimation
# Float diboson when running over a squark fit setup
if not doOnlySignal and 'diboson' in mysamples:
    for chan in AllChannels_all:
        #if 'SS' in chan.name:
        #    chan.getSample(DibosonsSampleName).setNormFactor("mu_Diboson",1.,0.,5.)
        #else:
        chan.getSample(DibosonsSampleName).setNormByTheory()

# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    
    if "SR3J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 70.
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR6JGGx12HM" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 18.5
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 25.5
        chan.titleY = "Events / 100 GeV"
        chan.titleX = "p_{T} [GeV]"      
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
