
#
# Matthew Gignac (UBC)
# 2016/06/27
#

namemap = {}
namemap['1,Theoretical $t\\bar{t}$ Uncertainties']=[]
namemap['2,$t\\bar{t}$ Normalization Uncertainity']=[]
namemap['3,Theoretical W+jet Uncertainties']=[]
namemap['4,W+jets Normalization Uncertainity']=[]
namemap['5,Diboson Normalization Uncertainty']=[]
namemap['6,Theoretical Diboson Uncertainties']=[]
namemap['7,Theoretical Single top Uncertainties']=[]
namemap['8,Jet Energy Scale']=[]
namemap['9,Jet Energy Resolution']=[]
namemap['10,B-tagging Scale Factors']=[]
namemap['11,MET Uncertainties']=[]
namemap['12,Muon Instrumental Uncertainties']=[]
namemap['13,Electron Instrumental Uncertainties']=[]
namemap['14,Statistical Uncertainties'] = []
namemap['15,Others'] = []


w = Util.GetWorkspaceFromFile(wsFileName,'w')
result = w.obj("RooExpandedFitResult_afterFit")
fpf = result.floatParsFinal()

for idx in range(fpf.getSize()):
    parname = fpf[idx].GetName()
    
    if parname.startswith("alpha_Ttbar"):
        namemap['1,Theoretical $t\\bar{t}$ Uncertainties'].append(parname)        
    elif parname.startswith("mu") and parname.endswith("Top"):
        namemap['2,$t\\bar{t}$ Normalization Uncertainity'].append(parname)       
    elif parname.startswith("alpha_Wjets"):
        namemap['3,Theoretical W+jet Uncertainties'].append(parname)  
    elif parname.startswith("mu") and parname.endswith("W"):
        namemap['4,W+jets Normalization Uncertainity'].append(parname)
    elif parname.startswith("mu_Diboson"):
        namemap['5,Diboson Normalization Uncertainty'].append(parname)
    elif parname.startswith("alpha_Dibosons"):
        namemap['6,Theoretical Diboson Uncertainties'].append(parname)
    elif parname.endswith("alpha_h1L_SingleTopComm"):
        namemap['7,Theoretical Single top Uncertainties'].append(parname)
    elif parname.startswith("alpha_JES") or parname.startswith("alpha_JET"):
        namemap['8,Jet Energy Scale'].append(parname)
    elif parname.startswith("alpha_JER"):
        namemap['9,Jet Energy Resolution'].append(parname)
    elif parname.startswith("alpha_btag"):
        namemap['10,B-tagging Scale Factors'].append(parname)
    elif parname.startswith("alpha_MET"):
        namemap['11,MET Uncertainties'].append(parname)
    elif parname.startswith("alpha_MUON"):
        namemap['12,Muon Instrumental Uncertainties'].append(parname)
    elif parname.startswith("alpha_EL_Eff") or parname.startswith("alpha_EG"):
        namemap['13,Electron Instrumental Uncertainties'].append(parname)
    elif parname.startswith("gamma_stat"):
        namemap['14,Statistical Uncertainties'].append(parname)
    else:
        print "WARNING: don't know what to do with nuisance parameter: %s. Adding to others."%parname
        namemap['15,Others'].append(parname)

