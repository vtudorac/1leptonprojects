import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
'''
uncert in TF
'''
#DibosonsGenTheoSR2J = Systematic("DibosonsGenTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.309)"],configMgr.weights+["( 1-0.309 )"], "weight","overallSys")
DibosonsGenTheoSR2J = Systematic("DibosonsGenTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.53)"],configMgr.weights+["( 1-0.53 )"], "weight","overallSys")
DibosonsGenTheoWR2J = Systematic("DibosonsGenTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.09)"],configMgr.weights+["( 1-0.09)"], "weight","overallSys")
DibosonsGenTheoTR2J = Systematic("DibosonsGenTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.025)"],configMgr.weights+["( 1-0.025)"], "weight","overallSys")
DibosonsGenTheoVR2J_1 = Systematic("DibosonsGenTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.173)"],configMgr.weights+["( 1-0.173 )"], "weight","overallSys")
DibosonsGenTheoVR2J_2 = Systematic("DibosonsGenTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.506)"],configMgr.weights+["( 1-0.506 )"], "weight","overallSys")

DibosonsGenTheoSR6JGGx12 = Systematic("DibosonsGenTheo_SR6JGGx12", configMgr.weights,configMgr.weights+["( 1+0.717)"],configMgr.weights+["( 1-0.717 )"], "weight","overallSys")
DibosonsGenTheoWR6JGGx12 = Systematic("DibosonsGenTheo_WR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.675)"],configMgr.weights+["( 1-0.675)"], "weight","overallSys")
DibosonsGenTheoTR6JGGx12 = Systematic("DibosonsGenTheo_TR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.693)"],configMgr.weights+["( 1-0.693)"], "weight","overallSys")
DibosonsGenTheoVR6JGGx12_mt = Systematic("DibosonsGenTheo_VR6JGGx12_mt",configMgr.weights,configMgr.weights+["( 1+0.515)"],configMgr.weights+["( 1-0.515 )"], "weight","overallSys")
DibosonsGenTheoVR6JGGx12_aplanarity = Systematic("DibosonsGenTheo_VR6JGGx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.514)"],configMgr.weights+["( 1-0.514 )"], "weight","overallSys")


DibosonsGenTheoSR6JGGx12HM = Systematic("DibosonsGenTheo_SR6JGGx12HM", configMgr.weights,configMgr.weights+["( 1+0.93)"],configMgr.weights+["( 1-0.93 )"], "weight","overallSys")
DibosonsGenTheoWR6JGGx12HM = Systematic("DibosonsGenTheo_WR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.707)"],configMgr.weights+["( 1-0.707)"], "weight","overallSys")
DibosonsGenTheoTR6JGGx12HM = Systematic("DibosonsGenTheo_TR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.699)"],configMgr.weights+["( 1-0.699)"], "weight","overallSys")
DibosonsGenTheoVR6JGGx12HM_mt = Systematic("DibosonsGenTheo_VR6JGGx12HM_mt",configMgr.weights,configMgr.weights+["( 1+0.131)"],configMgr.weights+["( 1-0.131 )"], "weight","overallSys")
DibosonsGenTheoVR6JGGx12HM_aplanarity = Systematic("DibosonsGenTheo_VR6JGGx12HM_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.61)"],configMgr.weights+["( 1-0.61 )"], "weight","overallSys")


DibosonsGenTheoSR4JhighxGG = Systematic("DibosonsGenTheo_SR4JhighxGG", configMgr.weights,configMgr.weights+["( 1+0.661)"],configMgr.weights+["( 1-0.661 )"], "weight","overallSys")
DibosonsGenTheoWR4JhighxGG = Systematic("DibosonsGenTheo_WR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.308)"],configMgr.weights+["( 1-0.308)"], "weight","overallSys")
DibosonsGenTheoTR4JhighxGG = Systematic("DibosonsGenTheo_TR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.571)"],configMgr.weights+["( 1-0.571)"], "weight","overallSys")
DibosonsGenTheoVR4JhighxGG_mt = Systematic("DibosonsGenTheo_VR4JhighxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.44)"],configMgr.weights+["( 1-0.44)"], "weight","overallSys")
DibosonsGenTheoVR4JhighxGG_metovermeff = Systematic("DibosonsGenTheo_VR4JhighxGG_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.083)"],configMgr.weights+["( 1-0.803)"], "weight","overallSys")


DibosonsGenTheoSR4JlowxGGexcl = Systematic("DibosonsGenTheo_SR4JlowxGGexcl", configMgr.weights,configMgr.weights+["( 1+0.74)"],configMgr.weights+["( 1-0.74 )"], "weight","overallSys")
DibosonsGenTheoSR4JlowxGGdisc = Systematic("DibosonsGenTheo_SR4JlowxGGdisc", configMgr.weights,configMgr.weights+["( 1+0.83)"],configMgr.weights+["( 1-0.83 )"], "weight","overallSys")
DibosonsGenTheoWR4JlowxGG = Systematic("DibosonsGenTheo_WR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.753)"],configMgr.weights+["( 1-0.753)"], "weight","overallSys")
DibosonsGenTheoTR4JlowxGG = Systematic("DibosonsGenTheo_TR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.975)"],configMgr.weights+["( 1-0.975)"], "weight","overallSys")
DibosonsGenTheoVR4JlowxGG_mt = Systematic("DibosonsGenTheo_VR4JlowxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.603)"],configMgr.weights+["( 1-0.603)"], "weight","overallSys")
DibosonsGenTheoVR4JlowxGG_aplanarity = Systematic("DibosonsGenTheo_VR4JlowxGG_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.625)"],configMgr.weights+["( 1-0.625)"], "weight","overallSys")








DibosonsResummationTheoSR2J = Systematic("DibosonsResummationTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.126)"],configMgr.weights+["( 1-0.126 )"], "weight","overallSys")
DibosonsResummationTheoWR2J = Systematic("DibosonsResummationTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.019)"],configMgr.weights+["( 1-0.019)"], "weight","overallSys")
DibosonsResummationTheoTR2J = Systematic("DibosonsResummationTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.125)"],configMgr.weights+["( 1-0.125)"], "weight","overallSys")
DibosonsResummationTheoVR2J_1 = Systematic("DibosonsResummationTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.043)"],configMgr.weights+["( 1-0.043 )"], "weight","overallSys")
DibosonsResummationTheoVR2J_2 = Systematic("DibosonsResummationTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.009)"],configMgr.weights+["( 1-0.009 )"], "weight","overallSys")

DibosonsResummationTheoSR6JGGx12 = Systematic("DibosonsResummationTheo_SR6JGGx12", configMgr.weights,configMgr.weights+["( 1+0.099)"],configMgr.weights+["( 1-0.099 )"], "weight","overallSys")
DibosonsResummationTheoWR6JGGx12 = Systematic("DibosonsResummationTheo_WR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.216)"],configMgr.weights+["( 1-0.216)"], "weight","overallSys")
DibosonsResummationTheoTR6JGGx12 = Systematic("DibosonsResummationTheo_TR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.693)"],configMgr.weights+["( 1-0.693)"], "weight","overallSys")
DibosonsResummationTheoVR6JGGx12_mt = Systematic("DibosonsResummationTheo_VR6JGGx12_mt",configMgr.weights,configMgr.weights+["( 1+0.167)"],configMgr.weights+["( 1-0.167 )"], "weight","overallSys")
DibosonsResummationTheoVR6JGGx12_aplanarity = Systematic("DibosonsResummationTheo_VR6JGGx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.079)"],configMgr.weights+["( 1-0.079 )"], "weight","overallSys")


DibosonsResummationTheoSR6JGGx12HM = Systematic("DibosonsResummationTheo_SR6JGGx12HM", configMgr.weights,configMgr.weights+["( 1+0.302)"],configMgr.weights+["( 1-0.302 )"], "weight","overallSys")
DibosonsResummationTheoWR6JGGx12HM = Systematic("DibosonsResummationTheo_WR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.033)"],configMgr.weights+["( 1-0.033)"], "weight","overallSys")
DibosonsResummationTheoTR6JGGx12HM = Systematic("DibosonsResummationTheo_TR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.365)"],configMgr.weights+["( 1-0.365)"], "weight","overallSys")
DibosonsResummationTheoVR6JGGx12HM_mt = Systematic("DibosonsResummationTheo_VR6JGGx12HM_mt",configMgr.weights,configMgr.weights+["( 1+0.048)"],configMgr.weights+["( 1-0.048 )"], "weight","overallSys")
DibosonsResummationTheoVR6JGGx12HM_aplanarity = Systematic("DibosonsResummationTheo_VR6JGGx12HM_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")


DibosonsResummationTheoSR4JhighxGG = Systematic("DibosonsResummationTheo_SR4JhighxGG", configMgr.weights,configMgr.weights+["( 1+0.561)"],configMgr.weights+["( 1-0.561 )"], "weight","overallSys")
DibosonsResummationTheoWR4JhighxGG = Systematic("DibosonsResummationTheo_WR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.002)"],configMgr.weights+["( 1-0.002)"], "weight","overallSys")
DibosonsResummationTheoTR4JhighxGG = Systematic("DibosonsResummationTheo_TR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.073)"],configMgr.weights+["( 1-0.073)"], "weight","overallSys")
DibosonsResummationTheoVR4JhighxGG_mt = Systematic("DibosonsResummationTheo_VR4JhighxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.105)"],configMgr.weights+["( 1-0.105)"], "weight","overallSys")
DibosonsResummationTheoVR4JhighxGG_metovermeff = Systematic("DibosonsResummationTheo_VR4JhighxGG_metovermeff",configMgr.weights,configMgr.weights+["( 1+0.02)"],configMgr.weights+["( 1-0.02)"], "weight","overallSys")


DibosonsResummationTheoSR4JlowxGGexcl = Systematic("DibosonsResummationTheo_SR4JlowxGGexcl", configMgr.weights,configMgr.weights+["( 1+0.248)"],configMgr.weights+["( 1-0.248 )"], "weight","overallSys")
DibosonsResummationTheoSR4JlowxGGdisc = Systematic("DibosonsResummationTheo_SR4JlowxGGdisc", configMgr.weights,configMgr.weights+["( 1+0.256)"],configMgr.weights+["( 1-0.256 )"], "weight","overallSys")
DibosonsResummationTheoWR4JlowxGG = Systematic("DibosonsResummationTheo_WR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.051)"],configMgr.weights+["( 1-0.051)"], "weight","overallSys")
DibosonsResummationTheoTR4JlowxGG = Systematic("DibosonsResummationTheo_TR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.361)"],configMgr.weights+["( 1-0.361)"], "weight","overallSys")
DibosonsResummationTheoVR4JlowxGG_mt = Systematic("DibosonsResummationTheo_VR4JlowxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.035)"],configMgr.weights+["( 1-0.035)"], "weight","overallSys")
DibosonsResummationTheoVR4JlowxGG_aplanarity = Systematic("DibosonsResummationTheo_VR4JlowxGG_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.017)"],configMgr.weights+["( 1-0.017)"], "weight","overallSys")





#DibosonsRenormTheoSR2J = Systematic("DibosonsRenormTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.434)"],configMgr.weights+["( 1-0.434 )"], "weight","overallSys")
DibosonsRenormTheoWR2J = Systematic("DibosonsRenormTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.223)"],configMgr.weights+["( 1-0.223)"], "weight","overallSys")
DibosonsRenormTheoTR2J = Systematic("DibosonsRenormTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.197)"],configMgr.weights+["( 1-0.197)"], "weight","overallSys")
DibosonsRenormTheoVR2J_1 = Systematic("DibosonsRenormTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.307)"],configMgr.weights+["( 1-0.307 )"], "weight","overallSys")
DibosonsRenormTheoVR2J_2 = Systematic("DibosonsRenormTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.385)"],configMgr.weights+["( 1-0.385 )"], "weight","overallSys")

DibosonsRenormTheoSR6JGGx12 = Systematic("DibosonsRenormTheo_SR6JGGx12", configMgr.weights,configMgr.weights+["( 1+0.46)"],configMgr.weights+["( 1-0.46 )"], "weight","overallSys")
DibosonsRenormTheoWR6JGGx12 = Systematic("DibosonsRenormTheo_WR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.247)"],configMgr.weights+["( 1-0.247)"], "weight","overallSys")
DibosonsRenormTheoTR6JGGx12 = Systematic("DibosonsRenormTheo_TR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.521)"],configMgr.weights+["( 1-0.521)"], "weight","overallSys")
DibosonsRenormTheoVR6JGGx12_mt = Systematic("DibosonsRenormTheo_VR6JGGx12_mt",configMgr.weights,configMgr.weights+["( 1+0.549)"],configMgr.weights+["( 1-0.549 )"], "weight","overallSys")
DibosonsRenormTheoVR6JGGx12_aplanarity = Systematic("DibosonsRenormTheo_VR6JGGx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.357)"],configMgr.weights+["( 1-0.357 )"], "weight","overallSys")


DibosonsRenormTheoSR6JGGx12HM = Systematic("DibosonsRenormTheo_SR6JGGx12HM", configMgr.weights,configMgr.weights+["( 1+0.206)"],configMgr.weights+["( 1-0.206 )"], "weight","overallSys")
DibosonsRenormTheoWR6JGGx12HM = Systematic("DibosonsRenormTheo_WR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.195)"],configMgr.weights+["( 1-0.195)"], "weight","overallSys")
DibosonsRenormTheoTR6JGGx12HM = Systematic("DibosonsRenormTheo_TR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.477)"],configMgr.weights+["( 1-0.477)"], "weight","overallSys")
DibosonsRenormTheoVR6JGGx12HM_mt = Systematic("DibosonsRenormTheo_VR6JGGx12HM_mt",configMgr.weights,configMgr.weights+["( 1+0.15)"],configMgr.weights+["( 1-0.15 )"], "weight","overallSys")
DibosonsRenormTheoVR6JGGx12HM_aplanarity = Systematic("DibosonsRenormTheo_VR6JGGx12HM_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.445)"],configMgr.weights+["( 1-0.445)"], "weight","overallSys")


DibosonsRenormTheoSR4JhighxGG = Systematic("DibosonsRenormTheo_SR4JhighxGG", configMgr.weights,configMgr.weights+["( 1+0.429)"],configMgr.weights+["( 1-0.429 )"], "weight","overallSys")
DibosonsRenormTheoWR4JhighxGG = Systematic("DibosonsRenormTheo_WR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.347)"],configMgr.weights+["( 1-0.347)"], "weight","overallSys")
DibosonsRenormTheoTR4JhighxGG = Systematic("DibosonsRenormTheo_TR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.323)"],configMgr.weights+["( 1-0.323)"], "weight","overallSys")
DibosonsRenormTheoVR4JhighxGG_mt = Systematic("DibosonsRenormTheo_VR4JhighxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.401)"],configMgr.weights+["( 1-0.401)"], "weight","overallSys")
DibosonsRenormTheoVR4JhighxGG_metovermeff = Systematic("DibosonsRenormTheo_VR4JhighxGG_metovermeff",configMgr.weights,configMgr.weights+["( 1+0.356)"],configMgr.weights+["( 1-0.356)"], "weight","overallSys")


DibosonsRenormTheoSR4JlowxGGexcl = Systematic("DibosonsRenormTheo_SR4JlowxGGexcl", configMgr.weights,configMgr.weights+["( 1+0.164)"],configMgr.weights+["( 1-0.164 )"], "weight","overallSys")
DibosonsRenormTheoSR4JlowxGGdisc = Systematic("DibosonsRenormTheo_SR4JlowxGGdisc", configMgr.weights,configMgr.weights+["( 1+0.393)"],configMgr.weights+["( 1-0.393 )"], "weight","overallSys")
DibosonsRenormTheoWR4JlowxGG = Systematic("DibosonsRenormTheo_WR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.225)"],configMgr.weights+["( 1-0.225)"], "weight","overallSys")
DibosonsRenormTheoTR4JlowxGG = Systematic("DibosonsRenormTheo_TR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.253)"],configMgr.weights+["( 1-0.253)"], "weight","overallSys")
DibosonsRenormTheoVR4JlowxGG_mt = Systematic("DibosonsRenormTheo_VR4JlowxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.085)"],configMgr.weights+["( 1-0.085)"], "weight","overallSys")
DibosonsRenormTheoVR4JlowxGG_aplanarity = Systematic("DibosonsRenormTheo_VR4JlowxGG_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.438)"],configMgr.weights+["( 1-0.438)"], "weight","overallSys")





DibosonsFacTheoSR2J = Systematic("DibosonsFacTheo_SR2J", configMgr.weights,configMgr.weights+["( 1+0.066)"],configMgr.weights+["( 1-0.066 )"], "weight","overallSys")
DibosonsFacTheoWR2J = Systematic("DibosonsFacTheo_WR2J",configMgr.weights,configMgr.weights+["( 1+0.02)"],configMgr.weights+["( 1-0.02)"], "weight","overallSys")
DibosonsFacTheoTR2J = Systematic("DibosonsFacTheo_TR2J",configMgr.weights,configMgr.weights+["( 1+0.114)"],configMgr.weights+["( 1-0.114)"], "weight","overallSys")
DibosonsFacTheoVR2J_1 = Systematic("DibosonsFacTheo_VR2J_1",configMgr.weights,configMgr.weights+["( 1+0.143)"],configMgr.weights+["( 1-0.143 )"], "weight","overallSys")
DibosonsFacTheoVR2J_2 = Systematic("DibosonsFacTheo_VR2J_2",configMgr.weights,configMgr.weights+["( 1+0.02)"],configMgr.weights+["( 1-0.02 )"], "weight","overallSys")

DibosonsFacTheoSR6JGGx12 = Systematic("DibosonsFacTheo_SR6JGGx12", configMgr.weights,configMgr.weights+["( 1+0.01)"],configMgr.weights+["( 1-0.01 )"], "weight","overallSys")
DibosonsFacTheoWR6JGGx12 = Systematic("DibosonsFacTheo_WR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.023)"],configMgr.weights+["( 1-0.023)"], "weight","overallSys")
DibosonsFacTheoTR6JGGx12 = Systematic("DibosonsFacTheo_TR6JGGx12",configMgr.weights,configMgr.weights+["( 1+0.284)"],configMgr.weights+["( 1-0.284)"], "weight","overallSys")
DibosonsFacTheoVR6JGGx12_mt = Systematic("DibosonsFacTheo_VR6JGGx12_mt",configMgr.weights,configMgr.weights+["( 1+0.077)"],configMgr.weights+["( 1-0.077 )"], "weight","overallSys")
DibosonsFacTheoVR6JGGx12_aplanarity = Systematic("DibosonsFacTheo_VR6JGGx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.101)"],configMgr.weights+["( 1-0.101 )"], "weight","overallSys")


DibosonsFacTheoSR6JGGx12HM = Systematic("DibosonsFacTheo_SR6JGGx12HM", configMgr.weights,configMgr.weights+["( 1+0.239)"],configMgr.weights+["( 1-0.239 )"], "weight","overallSys")
DibosonsFacTheoWR6JGGx12HM = Systematic("DibosonsFacTheo_WR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.106)"],configMgr.weights+["( 1-0.106)"], "weight","overallSys")
DibosonsFacTheoTR6JGGx12HM = Systematic("DibosonsFacTheo_TR6JGGx12HM",configMgr.weights,configMgr.weights+["( 1+0.314)"],configMgr.weights+["( 1-0.314)"], "weight","overallSys")
DibosonsFacTheoVR6JGGx12HM_mt = Systematic("DibosonsFacTheo_VR6JGGx12HM_mt",configMgr.weights,configMgr.weights+["( 1+0.106)"],configMgr.weights+["( 1-0.106 )"], "weight","overallSys")
DibosonsFacTheoVR6JGGx12HM_aplanarity = Systematic("DibosonsFacTheo_VR6JGGx12HM_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.198)"],configMgr.weights+["( 1-0.198)"], "weight","overallSys")


DibosonsFacTheoSR4JhighxGG = Systematic("DibosonsFacTheo_SR4JhighxGG", configMgr.weights,configMgr.weights+["( 1+0.095)"],configMgr.weights+["( 1-0.095 )"], "weight","overallSys")
DibosonsFacTheoWR4JhighxGG = Systematic("DibosonsFacTheo_WR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.025)"],configMgr.weights+["( 1-0.025)"], "weight","overallSys")
DibosonsFacTheoTR4JhighxGG = Systematic("DibosonsFacTheo_TR4JhighxGG",configMgr.weights,configMgr.weights+["( 1+0.074)"],configMgr.weights+["( 1-0.074)"], "weight","overallSys")
DibosonsFacTheoVR4JhighxGG_mt = Systematic("DibosonsFacTheo_VR4JhighxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.148)"],configMgr.weights+["( 1-0.148)"], "weight","overallSys")
DibosonsFacTheoVR4JhighxGG_metovermeff = Systematic("DibosonsFacTheo_VR4JhighxGG_metovermeff",configMgr.weights,configMgr.weights+["( 1+0.023)"],configMgr.weights+["( 1-0.023)"], "weight","overallSys")


DibosonsFacTheoSR4JlowxGGexcl = Systematic("DibosonsFacTheo_SR4JlowxGGexcl", configMgr.weights,configMgr.weights+["( 1+0.143)"],configMgr.weights+["( 1-0.143 )"], "weight","overallSys")
DibosonsFacTheoSR4JlowxGGdisc = Systematic("DibosonsFacTheo_SR4JlowxGGdisc", configMgr.weights,configMgr.weights+["( 1+0.526)"],configMgr.weights+["( 1-0.526 )"], "weight","overallSys")
DibosonsFacTheoWR4JlowxGG = Systematic("DibosonsFacTheo_WR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.136)"],configMgr.weights+["( 1-0.136)"], "weight","overallSys")
DibosonsFacTheoTR4JlowxGG = Systematic("DibosonsFacTheo_TR4JlowxGG",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05)"], "weight","overallSys")
DibosonsFacTheoVR4JlowxGG_mt = Systematic("DibosonsFacTheo_VR4JlowxGG_mt",configMgr.weights,configMgr.weights+["( 1+0.185)"],configMgr.weights+["( 1-0.185)"], "weight","overallSys")
DibosonsFacTheoVR4JlowxGG_aplanarity = Systematic("DibosonsFacTheo_VR4JlowxGG_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.01)"],configMgr.weights+["( 1-0.01)"], "weight","overallSys")


'''
uncert on TF for squarks
'''
DibosonsGenTheoSR4JSSx12 = Systematic("DibosonsGenTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.529)"],configMgr.weights+["( 1-0.529 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSx12_mt = Systematic("DibosonsGenTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.118)"],configMgr.weights+["( 1-0.118 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSx12_aplanarity = Systematic("DibosonsGenTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.653)"],configMgr.weights+["( 1-0.653 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSx12_DR = Systematic("DibosonsGenTheo_VR4JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.261)"],configMgr.weights+["( 1-0.261 )"], "weight","overallSys")


DibosonsGenTheoSR5JSSx12 = Systematic("DibosonsGenTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.191)"],configMgr.weights+["( 1-0.191 )"], "weight","overallSys")
DibosonsGenTheoVR5JSSx12_mt = Systematic("DibosonsGenTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.231)"],configMgr.weights+["( 1-0.231 )"], "weight","overallSys")
DibosonsGenTheoVR5JSSx12_met = Systematic("DibosonsGenTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.084)"],configMgr.weights+["( 1-0.084 )"], "weight","overallSys")
DibosonsGenTheoVR5JSSx12_DR = Systematic("DibosonsGenTheo_VR5JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.382)"],configMgr.weights+["( 1-0.382)"], "weight","overallSys")


DibosonsGenTheoSR4JSSlowx = Systematic("DibosonsGenTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.405)"],configMgr.weights+["( 1-0.405 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSlowx_mt = Systematic("DibosonsGenTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.206)"],configMgr.weights+["( 1-0.206 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSlowx_aplanarity = Systematic("DibosonsGenTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.022)"],configMgr.weights+["( 1-0.022)"], "weight","overallSys")
DibosonsGenTheoVR4JSSlowx_DR = Systematic("DibosonsGenTheo_VR4JSSlowx_DR",configMgr.weights,configMgr.weights+["( 1+0.254)"],configMgr.weights+["( 1-0.254)"], "weight","overallSys")


DibosonsGenTheoSR5JSShighx = Systematic("DibosonsGenTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.551)"],configMgr.weights+["( 1-0.551 )"], "weight","overallSys")
DibosonsGenTheoVR5JSShighx_mt = Systematic("DibosonsGenTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.057)"],configMgr.weights+["( 1-0.057 )"], "weight","overallSys")
DibosonsGenTheoVR5JSShighx_aplanarity = Systematic("DibosonsGenTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.251)"],configMgr.weights+["( 1-0.251 )"], "weight","overallSys")
DibosonsGenTheoVR5JSShighx_DR = Systematic("DibosonsGenTheo_VR5JSShighx_DR",configMgr.weights,configMgr.weights+["( 1+0.303)"],configMgr.weights+["( 1-0.303)"], "weight","overallSys")



DibosonsResummationTheoSR4JSSx12 = Systematic("DibosonsResummationTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.248)"],configMgr.weights+["( 1-0.248 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSx12_mt = Systematic("DibosonsResummationTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.039)"],configMgr.weights+["( 1-0.039 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSx12_aplanarity = Systematic("DibosonsResummationTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.334)"],configMgr.weights+["( 1-0.334 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSx12_DR = Systematic("DibosonsResummationTheo_VR4JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.023)"],configMgr.weights+["( 1-0.023 )"], "weight","overallSys")


DibosonsResummationTheoSR5JSSx12 = Systematic("DibosonsResummationTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.012)"],configMgr.weights+["( 1-0.012)"], "weight","overallSys")
DibosonsResummationTheoVR5JSSx12_mt = Systematic("DibosonsResummationTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.023)"],configMgr.weights+["( 1-0.023 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSSx12_met = Systematic("DibosonsResummationTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.023)"],configMgr.weights+["( 1-0.023 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSSx12_DR = Systematic("DibosonsResummationTheo_VR5JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.022)"],configMgr.weights+["( 1-0.022)"], "weight","overallSys")


DibosonsResummationTheoSR4JSSlowx = Systematic("DibosonsResummationTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.028)"],configMgr.weights+["( 1-0.028 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSlowx_mt = Systematic("DibosonsResummationTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.017)"],configMgr.weights+["( 1-0.017 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSlowx_aplanarity = Systematic("DibosonsResummationTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.001)"],configMgr.weights+["( 1-0.001)"], "weight","overallSys")
DibosonsResummationTheoVR4JSSlowx_DR = Systematic("DibosonsResummationTheo_VR4JSSlowx_DR",configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03)"], "weight","overallSys")


DibosonsResummationTheoSR5JSShighx = Systematic("DibosonsResummationTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.058)"],configMgr.weights+["( 1-0.058 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSShighx_mt = Systematic("DibosonsResummationTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.042)"],configMgr.weights+["( 1-0.042 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSShighx_aplanarity = Systematic("DibosonsResummationTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.034)"],configMgr.weights+["( 1-0.034 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSShighx_DR = Systematic("DibosonsResummationTheo_VR5JSShighx_DR",configMgr.weights,configMgr.weights+["( 1+0.014)"],configMgr.weights+["( 1-0.014 )"], "weight","overallSys")


DibosonsRenormTheoSR4JSSx12 = Systematic("DibosonsRenormTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.375)"],configMgr.weights+["( 1-0.375 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSx12_mt = Systematic("DibosonsRenormTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.304)"],configMgr.weights+["( 1-0.304 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSx12_aplanarity = Systematic("DibosonsRenormTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.123)"],configMgr.weights+["( 1-0.123 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSx12_DR= Systematic("DibosonsRenormTheo_VR4JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.35)"],configMgr.weights+["( 1-0.35 )"], "weight","overallSys")


DibosonsRenormTheoSR5JSSx12 = Systematic("DibosonsRenormTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.189)"],configMgr.weights+["( 1-0.189 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSSx12_mt = Systematic("DibosonsRenormTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.251)"],configMgr.weights+["( 1-0.251 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSSx12_met = Systematic("DibosonsRenormTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.130)"],configMgr.weights+["( 1-0.130 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSSx12_DR = Systematic("DibosonsRenormTheo_VR5JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")


DibosonsRenormTheoSR4JSSlowx = Systematic("DibosonsRenormTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.055 )"],configMgr.weights+["( 1-0.055  )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSlowx_mt = Systematic("DibosonsRenormTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.056)"],configMgr.weights+["( 1-0.056 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSlowx_aplanarity = Systematic("DibosonsRenormTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.313)"],configMgr.weights+["( 1-0.313)"], "weight","overallSys")
DibosonsRenormTheoVR4JSSlowx_DR = Systematic("DibosonsRenormTheo_VR4JSSlowx_DR",configMgr.weights,configMgr.weights+["( 1+0.397)"],configMgr.weights+["( 1-0.397)"], "weight","overallSys")


DibosonsRenormTheoSR5JSShighx = Systematic("DibosonsRenormTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.233)"],configMgr.weights+["( 1-0.233 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSShighx_mt = Systematic("DibosonsRenormTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.063)"],configMgr.weights+["( 1-0.063 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSShighx_aplanarity = Systematic("DibosonsRenormTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.087)"],configMgr.weights+["( 1-0.087 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSShighx_DR= Systematic("DibosonsRenormTheo_VR5JSShighx_DR",configMgr.weights,configMgr.weights+["( 1+0.02)"],configMgr.weights+["( 1-0.02 )"], "weight","overallSys")




DibosonsFacTheoSR4JSSx12 = Systematic("DibosonsFacTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.146)"],configMgr.weights+["( 1-0.146 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSx12_mt = Systematic("DibosonsFacTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.036)"],configMgr.weights+["( 1-0.036 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSx12_aplanarity = Systematic("DibosonsFacTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.059)"],configMgr.weights+["( 1-0.059 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSx12_DR = Systematic("DibosonsFacTheo_VR4JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.029)"],configMgr.weights+["( 1-0.029 )"], "weight","overallSys")


DibosonsFacTheoSR5JSSx12 = Systematic("DibosonsFacTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.074)"],configMgr.weights+["( 1-0.074 )"], "weight","overallSys")
DibosonsFacTheoVR5JSSx12_mt = Systematic("DibosonsFacTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.017)"],configMgr.weights+["( 1-0.017 )"], "weight","overallSys")
DibosonsFacTheoVR5JSSx12_met = Systematic("DibosonsFacTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.039)"],configMgr.weights+["( 1-0.039 )"], "weight","overallSys")
DibosonsFacTheoVR5JSSx12_DR = Systematic("DibosonsFacTheo_VR5JSSx12_DR",configMgr.weights,configMgr.weights+["( 1+0.074)"],configMgr.weights+["( 1-0.074 )"], "weight","overallSys")



DibosonsFacTheoSR4JSSlowx = Systematic("DibosonsFacTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.056)"],configMgr.weights+["( 1-0.056 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSlowx_mt = Systematic("DibosonsFacTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.071)"],configMgr.weights+["( 1-0.071 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSlowx_aplanarity = Systematic("DibosonsFacTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.039)"],configMgr.weights+["( 1-0.039)"], "weight","overallSys")
DibosonsFacTheoVR4JSSlowx_DR = Systematic("DibosonsFacTheo_VR4JSSlowx_DR",configMgr.weights,configMgr.weights+["( 1+0.028)"],configMgr.weights+["( 1-0.028)"], "weight","overallSys")



DibosonsFacTheoSR5JSShighx = Systematic("DibosonsFacTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.102)"],configMgr.weights+["( 1-0.102 )"], "weight","overallSys")
DibosonsFacTheoVR5JSShighx_mt = Systematic("DibosonsFacTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.038)"],configMgr.weights+["( 1-0.038 )"], "weight","overallSys")
DibosonsFacTheoVR5JSShighx_aplanarity = Systematic("DibosonsFacTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.031)"],configMgr.weights+["( 1-0.031 )"], "weight","overallSys")
DibosonsFacTheoVR5JSShighx_DR = Systematic("DibosonsFacTheo_VR5JSShighx_DR",configMgr.weights,configMgr.weights+["( 1+0.048)"],configMgr.weights+["( 1-0.048 )"], "weight","overallSys")

'''
uncert on yields for squarks

DibosonsGenTheoSR4JSSx12 = Systematic("DibosonsGenTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.058)"],configMgr.weights+["( 1-0.058 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSx12_mt = Systematic("DibosonsGenTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.456)"],configMgr.weights+["( 1-0.456 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSx12_aplanarity = Systematic("DibosonsGenTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.786)"],configMgr.weights+["( 1-0.786)"], "weight","overallSys")

DibosonsGenTheoSR5JSSx12 = Systematic("DibosonsGenTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.408)"],configMgr.weights+["( 1-0.408)"], "weight","overallSys")
DibosonsGenTheoVR5JSSx12_mt = Systematic("DibosonsGenTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.618)"],configMgr.weights+["( 1-0.618 )"], "weight","overallSys")
DibosonsGenTheoVR5JSSx12_met = Systematic("DibosonsGenTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.545)"],configMgr.weights+["( 1-0.545 )"], "weight","overallSys")

DibosonsGenTheoSR4JSSlowx = Systematic("DibosonsGenTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.637)"],configMgr.weights+["( 1-0.637 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSlowx_mt = Systematic("DibosonsGenTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.263)"],configMgr.weights+["( 1-0.263 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSlowx_aplanarity = Systematic("DibosonsGenTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.376)"],configMgr.weights+["( 1-0.022)"], "weight","overallSys")

DibosonsGenTheoSR5JSShighx = Systematic("DibosonsGenTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.317)"],configMgr.weights+["( 1-0.317)"], "weight","overallSys")
DibosonsGenTheoVR5JSShighx_mt = Systematic("DibosonsGenTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.585)"],configMgr.weights+["( 1-0.585)"], "weight","overallSys")
DibosonsGenTheoVR5JSShighx_aplanarity = Systematic("DibosonsGenTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.45)"],configMgr.weights+["( 1-0.45 )"], "weight","overallSys")

DibosonsGenTheoWR4JSSx12 = Systematic("DibosonsGenTheo_WR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.424)"],configMgr.weights+["( 1-0.424 )"], "weight","overallSys")
DibosonsGenTheoTR4JSSx12 = Systematic("DibosonsGenTheo_TR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.361)"],configMgr.weights+["( 1-0.361 )"], "weight","overallSys")
DibosonsGenTheoDR4JSSx12 = Systematic("DibosonsGenTheo_DR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.384)"],configMgr.weights+["( 1-0.384 )"], "weight","overallSys")
DibosonsGenTheoVR4JSSx12_DR = Systematic("DibosonsGenTheo_VR4JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.545)"],configMgr.weights+["( 1-0.545 )"], "weight","overallSys")

DibosonsGenTheoWR5JSSx12 = Systematic("DibosonsGenTheo_WR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.696)"],configMgr.weights+["( 1-0.696 )"], "weight","overallSys")
DibosonsGenTheoTR5JSSx12 = Systematic("DibosonsGenTheo_TR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.844)"],configMgr.weights+["( 1-0.844 )"], "weight","overallSys")
DibosonsGenTheoDR5JSSx12 = Systematic("DibosonsGenTheo_DR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.503)"],configMgr.weights+["( 1-0.503 )"], "weight","overallSys")
DibosonsGenTheoVR5JSSx12_DR = Systematic("DibosonsGenTheo_VR5JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.693)"],configMgr.weights+["( 1-0.693 )"], "weight","overallSys")

DibosonsGenTheoWR4JSSlowx = Systematic("DibosonsGenTheo_WR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.448)"],configMgr.weights+["( 1-0.448 )"], "weight","overallSys")
DibosonsGenTheoTR4JSSlowx = Systematic("DibosonsGenTheo_TR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.268)"],configMgr.weights+["( 1-0.268 )"], "weight","overallSys")
DibosonsGenTheoDR4JSSlowx = Systematic("DibosonsGenTheo_DR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.389)"],configMgr.weights+["( 1-0.389)"], "weight","overallSys")
DibosonsGenTheoVR4JSSlowx_DR = Systematic("DibosonsGenTheo_VR4JSSlowx_DR", configMgr.weights,configMgr.weights+["( 1+0.545)"],configMgr.weights+["( 1-0.545)"], "weight","overallSys")

DibosonsGenTheoWR5JSShighx = Systematic("DibosonsGenTheo_WR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.413)"],configMgr.weights+["( 1-0.413)"], "weight","overallSys")
DibosonsGenTheoTR5JSShighx = Systematic("DibosonsGenTheo_TR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.595)"],configMgr.weights+["( 1-0.595 )"], "weight","overallSys")
DibosonsGenTheoDR5JSShighx = Systematic("DibosonsGenTheo_DR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.56)"],configMgr.weights+["( 1-0.56 )"], "weight","overallSys")
DibosonsGenTheoVR5JSShighx_DR = Systematic("DibosonsGenTheo_VR5JSShighx_DR", configMgr.weights,configMgr.weights+["( 1+0.693)"],configMgr.weights+["( 1-0.693 )"], "weight","overallSys")




DibosonsResummationTheoSR4JSSx12 = Systematic("DibosonsResummationTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.0241)"],configMgr.weights+["( 1-0.241 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSx12_mt = Systematic("DibosonsResummationTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.032)"],configMgr.weights+["( 1-0.032 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSx12_aplanarity = Systematic("DibosonsResummationTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.34)"],configMgr.weights+["( 1-0.34 )"], "weight","overallSys")

DibosonsResummationTheoSR5JSSx12 = Systematic("DibosonsResummationTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.002)"],configMgr.weights+["( 1-0.002)"], "weight","overallSys")
DibosonsResummationTheoVR5JSSx12_mt = Systematic("DibosonsResummationTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.014)"],configMgr.weights+["( 1-0.014 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSSx12_met = Systematic("DibosonsResummationTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.014)"],configMgr.weights+["( 1-0.014 )"], "weight","overallSys")

DibosonsResummationTheoSR4JSSlowx = Systematic("DibosonsResummationTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.029)"],configMgr.weights+["( 1-0.029 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSlowx_mt = Systematic("DibosonsResummationTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.017)"],configMgr.weights+["( 1-0.017 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSlowx_aplanarity = Systematic("DibosonsResummationTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.001)"],configMgr.weights+["( 1-0.001)"], "weight","overallSys")


DibosonsResummationTheoSR5JSShighx = Systematic("DibosonsResummationTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.06)"],configMgr.weights+["( 1-0.06 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSShighx_mt = Systematic("DibosonsResummationTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04)"], "weight","overallSys")
DibosonsResummationTheoVR5JSShighx_aplanarity = Systematic("DibosonsResummationTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.032)"],configMgr.weights+["( 1-0.032 )"], "weight","overallSys")









DibosonsResummationTheoWR4JSSx12 = Systematic("DibosonsResummationTheo_WR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")
DibosonsResummationTheoTR4JSSx12 = Systematic("DibosonsResummationTheo_TR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.063)"],configMgr.weights+["( 1-0.063 )"], "weight","overallSys")
DibosonsResummationTheoDR4JSSx12 = Systematic("DibosonsResummationTheo_DR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.007)"],configMgr.weights+["( 1-0.007 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSx12_DR = Systematic("DibosonsResummationTheo_VR4JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03 )"], "weight","overallSys")


DibosonsResummationTheoWR5JSSx12 = Systematic("DibosonsResummationTheo_WR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.037)"],configMgr.weights+["( 1-0.037 )"], "weight","overallSys")
DibosonsResummationTheoTR5JSSx12 = Systematic("DibosonsResummationTheo_TR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.067)"],configMgr.weights+["( 1-0.067 )"], "weight","overallSys")
DibosonsResummationTheoDR5JSSx12 = Systematic("DibosonsResummationTheo_DR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.01)"],configMgr.weights+["( 1-0.013)"], "weight","overallSys")
DibosonsResummationTheoVR5JSSx12_DR = Systematic("DibosonsResummationTheo_VR5JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.13)"],configMgr.weights+["( 1-0.13 )"], "weight","overallSys")

DibosonsResummationTheoWR4JSSlowx = Systematic("DibosonsResummationTheo_WR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsResummationTheoTR4JSSlowx = Systematic("DibosonsResummationTheo_TR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.253)"],configMgr.weights+["( 1-0.253 )"], "weight","overallSys")
DibosonsResummationTheoDR4JSSlowx = Systematic("DibosonsResummationTheo_DR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.001)"],configMgr.weights+["( 1-0.001 )"], "weight","overallSys")
DibosonsResummationTheoVR4JSSlowx_DR = Systematic("DibosonsResummationTheo_VR4JSSlowx_DR", configMgr.weights,configMgr.weights+["( 1+0.03)"],configMgr.weights+["( 1-0.03)"], "weight","overallSys")

DibosonsResummationTheoWR5JSShighx = Systematic("DibosonsResummationTheo_WR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.009)"],configMgr.weights+["( 1-0.009 )"], "weight","overallSys")
DibosonsResummationTheoTR5JSShighx = Systematic("DibosonsResummationTheo_TR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.048)"],configMgr.weights+["( 1-0.048 )"], "weight","overallSys")
DibosonsResummationTheoDR5JSShighx = Systematic("DibosonsResummationTheo_DR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.002)"],configMgr.weights+["( 1-0.002 )"], "weight","overallSys")
DibosonsResummationTheoVR5JSShighx_DR = Systematic("DibosonsResummationTheo_VR5JSShighx_DR", configMgr.weights,configMgr.weights+["( 1+0.13)"],configMgr.weights+["( 1-0.13 )"], "weight","overallSys")




DibosonsRenormTheoSR4JSSx12 = Systematic("DibosonsRenormTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.439)"],configMgr.weights+["( 1-0.439 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSx12_mt = Systematic("DibosonsRenormTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.372)"],configMgr.weights+["( 1-0.372 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSx12_aplanarity = Systematic("DibosonsRenormTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.046)"],configMgr.weights+["( 1-0.046 )"], "weight","overallSys")


DibosonsRenormTheoSR5JSSx12 = Systematic("DibosonsRenormTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.376)"],configMgr.weights+["( 1-0.376)"], "weight","overallSys")
DibosonsRenormTheoVR5JSSx12_mt = Systematic("DibosonsRenormTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.43)"],configMgr.weights+["( 1-0.43 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSSx12_met = Systematic("DibosonsRenormTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.323)"],configMgr.weights+["( 1-0.323)"], "weight","overallSys")


DibosonsRenormTheoSR4JSSlowx = Systematic("DibosonsRenormTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.184 )"],configMgr.weights+["( 1-0.184  )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSlowx_mt = Systematic("DibosonsRenormTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.075)"],configMgr.weights+["( 1-0.075 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSlowx_aplanarity = Systematic("DibosonsRenormTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.427)"],configMgr.weights+["( 1-0.427)"], "weight","overallSys")

DibosonsRenormTheoSR5JSShighx = Systematic("DibosonsRenormTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.328)"],configMgr.weights+["( 1-0.328 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSShighx_mt = Systematic("DibosonsRenormTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.041)"],configMgr.weights+["( 1-0.041 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSShighx_aplanarity = Systematic("DibosonsRenormTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.016)"],configMgr.weights+["( 1-0.016 )"], "weight","overallSys")

DibosonsRenormTheoWR4JSSx12 = Systematic("DibosonsRenormTheo_WR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.298)"],configMgr.weights+["( 1-0.298 )"], "weight","overallSys")
DibosonsRenormTheoTR4JSSx12 = Systematic("DibosonsRenormTheo_TR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.397)"],configMgr.weights+["( 1-0.397 )"], "weight","overallSys")
DibosonsRenormTheoDR4JSSx12 = Systematic("DibosonsRenormTheo_DR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.077)"],configMgr.weights+["( 1-0.077 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSx12_DR = Systematic("DibosonsRenormTheo_VR4JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.281)"],configMgr.weights+["( 1-0.281 )"], "weight","overallSys")

DibosonsRenormTheoWR5JSSx12 = Systematic("DibosonsRenormTheo_WR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.242)"],configMgr.weights+["( 1-0.242 )"], "weight","overallSys")
DibosonsRenormTheoTR5JSSx12 = Systematic("DibosonsRenormTheo_TR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.298)"],configMgr.weights+["( 1-0.298 )"], "weight","overallSys")
DibosonsRenormTheoDR5JSSx12 = Systematic("DibosonsRenormTheo_DR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.201)"],configMgr.weights+["( 1-0.201 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSSx12_DR = Systematic("DibosonsRenormTheo_VR5JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.123)"],configMgr.weights+["( 1-0.123 )"], "weight","overallSys")

DibosonsRenormTheoWR4JSSlowx = Systematic("DibosonsRenormTheo_WR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.281)"],configMgr.weights+["( 1-0.281 )"], "weight","overallSys")
DibosonsRenormTheoTR4JSSlowx = Systematic("DibosonsRenormTheo_TR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.35)"],configMgr.weights+["( 1-0.35)"], "weight","overallSys")
DibosonsRenormTheoDR4JSSlowx = Systematic("DibosonsRenormTheo_DR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.131)"],configMgr.weights+["( 1-0.131 )"], "weight","overallSys")
DibosonsRenormTheoVR4JSSlowx_DR = Systematic("DibosonsRenormTheo_VR4JSSlowx_DR", configMgr.weights,configMgr.weights+["( 1+0.281)"],configMgr.weights+["( 1-0.281)"], "weight","overallSys")

DibosonsRenormTheoWR5JSShighx = Systematic("DibosonsRenormTheo_WR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.246)"],configMgr.weights+["( 1-0.246 )"], "weight","overallSys")
DibosonsRenormTheoTR5JSShighx = Systematic("DibosonsRenormTheo_TR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.379)"],configMgr.weights+["( 1-0.379 )"], "weight","overallSys")
DibosonsRenormTheoDR5JSShighx = Systematic("DibosonsRenormTheo_DR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.103)"],configMgr.weights+["( 1-0.103 )"], "weight","overallSys")
DibosonsRenormTheoVR5JSShighx_DR = Systematic("DibosonsRenormTheo_VR5JSShighx_DR", configMgr.weights,configMgr.weights+["( 1+0.123)"],configMgr.weights+["( 1-0.123)"], "weight","overallSys")




DibosonsFacTheoSR4JSSx12 = Systematic("DibosonsFacTheo_SR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.104)"],configMgr.weights+["( 1-0.104 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSx12_mt = Systematic("DibosonsFacTheo_VR4JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.077)"],configMgr.weights+["( 1-0.077 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSx12_aplanarity = Systematic("DibosonsFacTheo_VR4JSSx12_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.017)"],configMgr.weights+["( 1-0.017 )"], "weight","overallSys")

DibosonsFacTheoSR5JSSx12 = Systematic("DibosonsFacTheo_SR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.008)"],configMgr.weights+["( 1-0.008 )"], "weight","overallSys")
DibosonsFacTheoVR5JSSx12_mt = Systematic("DibosonsFacTheo_VR5JSSx12_mt",configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
DibosonsFacTheoVR5JSSx12_met = Systematic("DibosonsFacTheo_VR5JSSx12_met",configMgr.weights,configMgr.weights+["( 1+0.105)"],configMgr.weights+["( 1-0.105 )"], "weight","overallSys")

DibosonsFacTheoSR4JSSlowx = Systematic("DibosonsFacTheo_SR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.015)"],configMgr.weights+["( 1-0.015 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSlowx_mt = Systematic("DibosonsFacTheo_VR4JSSlowx_mt",configMgr.weights,configMgr.weights+["( 1+0.031)"],configMgr.weights+["( 1-0.031 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSlowx_aplanarity = Systematic("DibosonsFacTheo_VR4JSSlowx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.079)"],configMgr.weights+["( 1-0.079)"], "weight","overallSys")

DibosonsFacTheoSR5JSShighx = Systematic("DibosonsFacTheo_SR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.142)"],configMgr.weights+["( 1-0.142 )"], "weight","overallSys")
DibosonsFacTheoVR5JSShighx_mt = Systematic("DibosonsFacTheo_VR5JSShighx_mt",configMgr.weights,configMgr.weights+["( 1+0.002)"],configMgr.weights+["( 1-0.002 )"], "weight","overallSys")
DibosonsFacTheoVR5JSShighx_aplanarity = Systematic("DibosonsFacTheo_VR5JSShighx_aplanarity",configMgr.weights,configMgr.weights+["( 1+0.071)"],configMgr.weights+["( 1-0.071 )"], "weight","overallSys")


DibosonsFacTheoWR4JSSx12 = Systematic("DibosonsFacTheo_WR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.021)"],configMgr.weights+["( 1-0.021 )"], "weight","overallSys")
DibosonsFacTheoTR4JSSx12 = Systematic("DibosonsFacTheo_TR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.135)"],configMgr.weights+["( 1-0.135 )"], "weight","overallSys")
DibosonsFacTheoDR4JSSx12 = Systematic("DibosonsFacTheo_DR4JSSx12", configMgr.weights,configMgr.weights+["( 1+0.042)"],configMgr.weights+["( 1-0.042 )"], "weight","overallSys")
DibosonsFacTheoVR4JSSx12_DR = Systematic("DibosonsFacTheo_VR4JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.13)"],configMgr.weights+["( 1-0.13 )"], "weight","overallSys")

DibosonsFacTheoWR5JSSx12 = Systematic("DibosonsFacTheo_WR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.021)"],configMgr.weights+["( 1-0.021 )"], "weight","overallSys")
DibosonsFacTheoTR5JSSx12 = Systematic("DibosonsFacTheo_TR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.05)"],configMgr.weights+["( 1-0.05 )"], "weight","overallSys")
DibosonsFacTheoDR5JSSx12 = Systematic("DibosonsFacTheo_DR5JSSx12", configMgr.weights,configMgr.weights+["( 1+0.066)"],configMgr.weights+["( 1-0.066)"], "weight","overallSys")
DibosonsFacTheoVR5JSSx12_DR = Systematic("DibosonsFacTheo_VR5JSSx12_DR", configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08 )"], "weight","overallSys")

DibosonsFacTheoWR4JSSlowx = Systematic("DibosonsFacTheo_WR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.026)"],configMgr.weights+["( 1-0.026 )"], "weight","overallSys")
DibosonsFacTheoTR4JSSlowx = Systematic("DibosonsFacTheo_TR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.206)"],configMgr.weights+["( 1-0.206 )"], "weight","overallSys")
DibosonsFacTheoDR4JSSlowx = Systematic("DibosonsFacTheo_DR4JSSlowx", configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04)"], "weight","overallSys")
DibosonsFacTheoVR4JSSlowx_DR = Systematic("DibosonsFacTheo_VR4JSSlowx_DR", configMgr.weights,configMgr.weights+["( 1+0.13)"],configMgr.weights+["( 1-0.13)"], "weight","overallSys")

DibosonsFacTheoWR5JSShighx = Systematic("DibosonsFacTheo_WR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.064)"],configMgr.weights+["( 1-0.064 )"], "weight","overallSys")
DibosonsFacTheoTR5JSShighx = Systematic("DibosonsFacTheo_TR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.143)"],configMgr.weights+["( 1-0.143 )"], "weight","overallSys")
DibosonsFacTheoDR5JSShighx = Systematic("DibosonsFacTheo_DR5JSShighx", configMgr.weights,configMgr.weights+["( 1+0.04)"],configMgr.weights+["( 1-0.04 )"], "weight","overallSys")
DibosonsFacTheoVR5JSShighx_DR = Systematic("DibosonsFacTheo_VR5JSShighx_DR", configMgr.weights,configMgr.weights+["( 1+0.08)"],configMgr.weights+["( 1-0.08)"], "weight","overallSys")
'''



def TheorUnc(generatorSyst):
   
    generatorSyst.append((("diboson","SR2JEl"), DibosonsGenTheoSR2J))
    generatorSyst.append((("diboson","SR2JMu"), DibosonsGenTheoSR2J))   
    generatorSyst.append((("diboson","SR2JEM"), DibosonsGenTheoSR2J))
    generatorSyst.append((("diboson","TR2JEl"), DibosonsGenTheoTR2J))
    generatorSyst.append((("diboson","TR2JMu"), DibosonsGenTheoTR2J))
    generatorSyst.append((("diboson","TR2JEM"), DibosonsGenTheoTR2J))
    generatorSyst.append((("diboson","WR2JEl"), DibosonsGenTheoWR2J))
    generatorSyst.append((("diboson","WR2JMu"), DibosonsGenTheoWR2J))
    generatorSyst.append((("diboson","WR2JEM"), DibosonsGenTheoWR2J))    
    generatorSyst.append((("diboson","VR2J_1El"), DibosonsGenTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_1Mu"), DibosonsGenTheoVR2J_1))    
    generatorSyst.append((("diboson","VR2J_2El"), DibosonsGenTheoVR2J_2))
    generatorSyst.append((("diboson","VR2J_2Mu"), DibosonsGenTheoVR2J_2))    
    generatorSyst.append((("diboson","VR2J_1EM"), DibosonsGenTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_2EM"), DibosonsGenTheoVR2J_2))
    
    generatorSyst.append((("diboson","SR6JGGx12El"), DibosonsGenTheoSR6JGGx12))
    generatorSyst.append((("diboson","SR6JGGx12Mu"), DibosonsGenTheoSR6JGGx12))   
    generatorSyst.append((("diboson","SR6JGGx12EM"), DibosonsGenTheoSR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12El"), DibosonsGenTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12Mu"), DibosonsGenTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12EM"), DibosonsGenTheoTR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12El"), DibosonsGenTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12Mu"), DibosonsGenTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12EM"), DibosonsGenTheoWR6JGGx12))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEl"), DibosonsGenTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_mtMu"), DibosonsGenTheoVR6JGGx12_mt))    
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEl"), DibosonsGenTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityMu"), DibosonsGenTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEM"), DibosonsGenTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEM"), DibosonsGenTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("diboson","SR6JGGx12HMEl"), DibosonsGenTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","SR6JGGx12HMMu"), DibosonsGenTheoSR6JGGx12HM))   
    generatorSyst.append((("diboson","SR6JGGx12HMEM"), DibosonsGenTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEl"), DibosonsGenTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMMu"), DibosonsGenTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEM"), DibosonsGenTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEl"), DibosonsGenTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMMu"), DibosonsGenTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEM"), DibosonsGenTheoWR6JGGx12HM))    
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEl"), DibosonsGenTheoVR6JGGx12HM_mt))
    generatorSyst.append((("diboson","VR6JGGx12HM_mtMu"), DibosonsGenTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEM"), DibosonsGenTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEl"), DibosonsGenTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityMu"), DibosonsGenTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEM"), DibosonsGenTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("diboson","SR4JhighxGGEl"), DibosonsGenTheoSR4JhighxGG))
    generatorSyst.append((("diboson","SR4JhighxGGMu"), DibosonsGenTheoSR4JhighxGG))   
    generatorSyst.append((("diboson","SR4JhighxGGEM"), DibosonsGenTheoSR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEl"), DibosonsGenTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGMu"), DibosonsGenTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEM"), DibosonsGenTheoTR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEl"), DibosonsGenTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGMu"), DibosonsGenTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEM"), DibosonsGenTheoWR4JhighxGG))    
    generatorSyst.append((("diboson","VR4JhighxGG_mtEl"), DibosonsGenTheoVR4JhighxGG_mt))
    generatorSyst.append((("diboson","VR4JhighxGG_mtMu"), DibosonsGenTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("diboson","VR4JhighxGG_mtEM"), DibosonsGenTheoVR4JhighxGG_mt))   
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEl"), DibosonsGenTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffMu"), DibosonsGenTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEM"), DibosonsGenTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("diboson","SR4JlowxGGexclEl"), DibosonsGenTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGexclMu"), DibosonsGenTheoSR4JlowxGGexcl))   
    generatorSyst.append((("diboson","SR4JlowxGGexclEM"), DibosonsGenTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGdiscEl"), DibosonsGenTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","SR4JlowxGGdiscMu"), DibosonsGenTheoSR4JlowxGGdisc))   
    generatorSyst.append((("diboson","SR4JlowxGGdiscEM"), DibosonsGenTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","TR4JlowxGGEl"), DibosonsGenTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGMu"), DibosonsGenTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGEM"), DibosonsGenTheoTR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEl"), DibosonsGenTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGMu"), DibosonsGenTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEM"), DibosonsGenTheoWR4JlowxGG))    
    generatorSyst.append((("diboson","VR4JlowxGG_mtEl"), DibosonsGenTheoVR4JlowxGG_mt))
    generatorSyst.append((("diboson","VR4JlowxGG_mtMu"), DibosonsGenTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("diboson","VR4JlowxGG_mtEM"), DibosonsGenTheoVR4JlowxGG_mt))   
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEl"), DibosonsGenTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffMu"), DibosonsGenTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEM"), DibosonsGenTheoVR4JlowxGG_aplanarity))
    
    generatorSyst.append((("diboson","SR2JEl"), DibosonsResummationTheoSR2J))
    generatorSyst.append((("diboson","SR2JMu"), DibosonsResummationTheoSR2J))   
    generatorSyst.append((("diboson","SR2JEM"), DibosonsResummationTheoSR2J))
    generatorSyst.append((("diboson","TR2JEl"), DibosonsResummationTheoTR2J))
    generatorSyst.append((("diboson","TR2JMu"), DibosonsResummationTheoTR2J))
    generatorSyst.append((("diboson","TR2JEM"), DibosonsResummationTheoTR2J))
    generatorSyst.append((("diboson","WR2JEl"), DibosonsResummationTheoWR2J))
    generatorSyst.append((("diboson","WR2JMu"), DibosonsResummationTheoWR2J))
    generatorSyst.append((("diboson","WR2JEM"), DibosonsResummationTheoWR2J))    
    generatorSyst.append((("diboson","VR2J_1El"), DibosonsResummationTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_1Mu"), DibosonsResummationTheoVR2J_1))    
    generatorSyst.append((("diboson","VR2J_2El"), DibosonsResummationTheoVR2J_2))
    generatorSyst.append((("diboson","VR2J_2Mu"), DibosonsResummationTheoVR2J_2))    
    generatorSyst.append((("diboson","VR2J_1EM"), DibosonsResummationTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_2EM"), DibosonsResummationTheoVR2J_2))
    
    generatorSyst.append((("diboson","SR6JGGx12El"), DibosonsResummationTheoSR6JGGx12))
    generatorSyst.append((("diboson","SR6JGGx12Mu"), DibosonsResummationTheoSR6JGGx12))   
    generatorSyst.append((("diboson","SR6JGGx12EM"), DibosonsResummationTheoSR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12El"), DibosonsResummationTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12Mu"), DibosonsResummationTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12EM"), DibosonsResummationTheoTR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12El"), DibosonsResummationTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12Mu"), DibosonsResummationTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12EM"), DibosonsResummationTheoWR6JGGx12))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEl"), DibosonsResummationTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_mtMu"), DibosonsResummationTheoVR6JGGx12_mt))    
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEl"), DibosonsResummationTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityMu"), DibosonsResummationTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEM"), DibosonsResummationTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEM"), DibosonsResummationTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("diboson","SR6JGGx12HMEl"), DibosonsResummationTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","SR6JGGx12HMMu"), DibosonsResummationTheoSR6JGGx12HM))   
    generatorSyst.append((("diboson","SR6JGGx12HMEM"), DibosonsResummationTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEl"), DibosonsResummationTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMMu"), DibosonsResummationTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEM"), DibosonsResummationTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEl"), DibosonsResummationTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMMu"), DibosonsResummationTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEM"), DibosonsResummationTheoWR6JGGx12HM))    
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEl"), DibosonsResummationTheoVR6JGGx12HM_mt))
    generatorSyst.append((("diboson","VR6JGGx12HM_mtMu"), DibosonsResummationTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEM"), DibosonsResummationTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEl"), DibosonsResummationTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityMu"), DibosonsResummationTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEM"), DibosonsResummationTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("diboson","SR4JhighxGGEl"), DibosonsResummationTheoSR4JhighxGG))
    generatorSyst.append((("diboson","SR4JhighxGGMu"), DibosonsResummationTheoSR4JhighxGG))   
    generatorSyst.append((("diboson","SR4JhighxGGEM"), DibosonsResummationTheoSR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEl"), DibosonsResummationTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGMu"), DibosonsResummationTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEM"), DibosonsResummationTheoTR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEl"), DibosonsResummationTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGMu"), DibosonsResummationTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEM"), DibosonsResummationTheoWR4JhighxGG))    
    generatorSyst.append((("diboson","VR4JhighxGG_mtEl"), DibosonsResummationTheoVR4JhighxGG_mt))
    generatorSyst.append((("diboson","VR4JhighxGG_mtMu"), DibosonsResummationTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("diboson","VR4JhighxGG_mtEM"), DibosonsResummationTheoVR4JhighxGG_mt))   
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEl"), DibosonsResummationTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffMu"), DibosonsResummationTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEM"), DibosonsResummationTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("diboson","SR4JlowxGGexclEl"), DibosonsResummationTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGexclMu"), DibosonsResummationTheoSR4JlowxGGexcl))   
    generatorSyst.append((("diboson","SR4JlowxGGexclEM"), DibosonsResummationTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGdiscEl"), DibosonsResummationTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","SR4JlowxGGdiscMu"), DibosonsResummationTheoSR4JlowxGGdisc))   
    generatorSyst.append((("diboson","SR4JlowxGGdiscEM"), DibosonsResummationTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","TR4JlowxGGEl"), DibosonsResummationTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGMu"), DibosonsResummationTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGEM"), DibosonsResummationTheoTR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEl"), DibosonsResummationTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGMu"), DibosonsResummationTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEM"), DibosonsResummationTheoWR4JlowxGG))    
    generatorSyst.append((("diboson","VR4JlowxGG_mtEl"), DibosonsResummationTheoVR4JlowxGG_mt))
    generatorSyst.append((("diboson","VR4JlowxGG_mtMu"), DibosonsResummationTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("diboson","VR4JlowxGG_mtEM"), DibosonsResummationTheoVR4JlowxGG_mt))   
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEl"), DibosonsResummationTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffMu"), DibosonsResummationTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEM"), DibosonsResummationTheoVR4JlowxGG_aplanarity))
    
   
    #generatorSyst.append((("diboson","SR2JEl"), DibosonsRenormTheoSR2J))
    #generatorSyst.append((("diboson","SR2JMu"), DibosonsRenormTheoSR2J))   
    #generatorSyst.append((("diboson","SR2JEM"), DibosonsRenormTheoSR2J))
    generatorSyst.append((("diboson","TR2JEl"), DibosonsRenormTheoTR2J))
    generatorSyst.append((("diboson","TR2JMu"), DibosonsRenormTheoTR2J))
    generatorSyst.append((("diboson","TR2JEM"), DibosonsRenormTheoTR2J))
    generatorSyst.append((("diboson","WR2JEl"), DibosonsRenormTheoWR2J))
    generatorSyst.append((("diboson","WR2JMu"), DibosonsRenormTheoWR2J))
    generatorSyst.append((("diboson","WR2JEM"), DibosonsRenormTheoWR2J))    
    generatorSyst.append((("diboson","VR2J_1El"), DibosonsRenormTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_1Mu"), DibosonsRenormTheoVR2J_1))    
    generatorSyst.append((("diboson","VR2J_2El"), DibosonsRenormTheoVR2J_2))
    generatorSyst.append((("diboson","VR2J_2Mu"), DibosonsRenormTheoVR2J_2))    
    generatorSyst.append((("diboson","VR2J_1EM"), DibosonsRenormTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_2EM"), DibosonsRenormTheoVR2J_2))
    
    generatorSyst.append((("diboson","SR6JGGx12El"), DibosonsRenormTheoSR6JGGx12))
    generatorSyst.append((("diboson","SR6JGGx12Mu"), DibosonsRenormTheoSR6JGGx12))   
    generatorSyst.append((("diboson","SR6JGGx12EM"), DibosonsRenormTheoSR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12El"), DibosonsRenormTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12Mu"), DibosonsRenormTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12EM"), DibosonsRenormTheoTR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12El"), DibosonsRenormTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12Mu"), DibosonsRenormTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12EM"), DibosonsRenormTheoWR6JGGx12))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEl"), DibosonsRenormTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_mtMu"), DibosonsRenormTheoVR6JGGx12_mt))    
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEl"), DibosonsRenormTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityMu"), DibosonsRenormTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEM"), DibosonsRenormTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEM"), DibosonsRenormTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("diboson","SR6JGGx12HMEl"), DibosonsRenormTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","SR6JGGx12HMMu"), DibosonsRenormTheoSR6JGGx12HM))   
    generatorSyst.append((("diboson","SR6JGGx12HMEM"), DibosonsRenormTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEl"), DibosonsRenormTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMMu"), DibosonsRenormTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEM"), DibosonsRenormTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEl"), DibosonsRenormTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMMu"), DibosonsRenormTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEM"), DibosonsRenormTheoWR6JGGx12HM))    
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEl"), DibosonsRenormTheoVR6JGGx12HM_mt))
    generatorSyst.append((("diboson","VR6JGGx12HM_mtMu"), DibosonsRenormTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEM"), DibosonsRenormTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEl"), DibosonsRenormTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityMu"), DibosonsRenormTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEM"), DibosonsRenormTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("diboson","SR4JhighxGGEl"), DibosonsRenormTheoSR4JhighxGG))
    generatorSyst.append((("diboson","SR4JhighxGGMu"), DibosonsRenormTheoSR4JhighxGG))   
    generatorSyst.append((("diboson","SR4JhighxGGEM"), DibosonsRenormTheoSR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEl"), DibosonsRenormTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGMu"), DibosonsRenormTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEM"), DibosonsRenormTheoTR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEl"), DibosonsRenormTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGMu"), DibosonsRenormTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEM"), DibosonsRenormTheoWR4JhighxGG))    
    generatorSyst.append((("diboson","VR4JhighxGG_mtEl"), DibosonsRenormTheoVR4JhighxGG_mt))
    generatorSyst.append((("diboson","VR4JhighxGG_mtMu"), DibosonsRenormTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("diboson","VR4JhighxGG_mtEM"), DibosonsRenormTheoVR4JhighxGG_mt))   
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEl"), DibosonsRenormTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffMu"), DibosonsRenormTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEM"), DibosonsRenormTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("diboson","SR4JlowxGGexclEl"), DibosonsRenormTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGexclMu"), DibosonsRenormTheoSR4JlowxGGexcl))   
    generatorSyst.append((("diboson","SR4JlowxGGexclEM"), DibosonsRenormTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGdiscEl"), DibosonsRenormTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","SR4JlowxGGdiscMu"), DibosonsRenormTheoSR4JlowxGGdisc))   
    generatorSyst.append((("diboson","SR4JlowxGGdiscEM"), DibosonsRenormTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","TR4JlowxGGEl"), DibosonsRenormTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGMu"), DibosonsRenormTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGEM"), DibosonsRenormTheoTR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEl"), DibosonsRenormTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGMu"), DibosonsRenormTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEM"), DibosonsRenormTheoWR4JlowxGG))    
    generatorSyst.append((("diboson","VR4JlowxGG_mtEl"), DibosonsRenormTheoVR4JlowxGG_mt))
    generatorSyst.append((("diboson","VR4JlowxGG_mtMu"), DibosonsRenormTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("diboson","VR4JlowxGG_mtEM"), DibosonsRenormTheoVR4JlowxGG_mt))   
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEl"), DibosonsRenormTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffMu"), DibosonsRenormTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEM"), DibosonsRenormTheoVR4JlowxGG_aplanarity))
   
    
    generatorSyst.append((("diboson","SR2JEl"), DibosonsFacTheoSR2J))
    generatorSyst.append((("diboson","SR2JMu"), DibosonsFacTheoSR2J))   
    generatorSyst.append((("diboson","SR2JEM"), DibosonsFacTheoSR2J))
    generatorSyst.append((("diboson","TR2JEl"), DibosonsFacTheoTR2J))
    generatorSyst.append((("diboson","TR2JMu"), DibosonsFacTheoTR2J))
    generatorSyst.append((("diboson","TR2JEM"), DibosonsFacTheoTR2J))
    generatorSyst.append((("diboson","WR2JEl"), DibosonsFacTheoWR2J))
    generatorSyst.append((("diboson","WR2JMu"), DibosonsFacTheoWR2J))
    generatorSyst.append((("diboson","WR2JEM"), DibosonsFacTheoWR2J))    
    generatorSyst.append((("diboson","VR2J_1El"), DibosonsFacTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_1Mu"), DibosonsFacTheoVR2J_1))    
    generatorSyst.append((("diboson","VR2J_2El"), DibosonsFacTheoVR2J_2))
    generatorSyst.append((("diboson","VR2J_2Mu"), DibosonsFacTheoVR2J_2))    
    generatorSyst.append((("diboson","VR2J_1EM"), DibosonsFacTheoVR2J_1))
    generatorSyst.append((("diboson","VR2J_2EM"), DibosonsFacTheoVR2J_2))
    
    generatorSyst.append((("diboson","SR6JGGx12El"), DibosonsFacTheoSR6JGGx12))
    generatorSyst.append((("diboson","SR6JGGx12Mu"), DibosonsFacTheoSR6JGGx12))   
    generatorSyst.append((("diboson","SR6JGGx12EM"), DibosonsFacTheoSR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12El"), DibosonsFacTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12Mu"), DibosonsFacTheoTR6JGGx12))
    generatorSyst.append((("diboson","TR6JGGx12EM"), DibosonsFacTheoTR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12El"), DibosonsFacTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12Mu"), DibosonsFacTheoWR6JGGx12))
    generatorSyst.append((("diboson","WR6JGGx12EM"), DibosonsFacTheoWR6JGGx12))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEl"), DibosonsFacTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_mtMu"), DibosonsFacTheoVR6JGGx12_mt))    
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEl"), DibosonsFacTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityMu"), DibosonsFacTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12_mtEM"), DibosonsFacTheoVR6JGGx12_mt))
    generatorSyst.append((("diboson","VR6JGGx12_aplanarityEM"), DibosonsFacTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("diboson","SR6JGGx12HMEl"), DibosonsFacTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","SR6JGGx12HMMu"), DibosonsFacTheoSR6JGGx12HM))   
    generatorSyst.append((("diboson","SR6JGGx12HMEM"), DibosonsFacTheoSR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEl"), DibosonsFacTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMMu"), DibosonsFacTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","TR6JGGx12HMEM"), DibosonsFacTheoTR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEl"), DibosonsFacTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMMu"), DibosonsFacTheoWR6JGGx12HM))
    generatorSyst.append((("diboson","WR6JGGx12HMEM"), DibosonsFacTheoWR6JGGx12HM))    
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEl"), DibosonsFacTheoVR6JGGx12HM_mt))
    generatorSyst.append((("diboson","VR6JGGx12HM_mtMu"), DibosonsFacTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("diboson","VR6JGGx12HM_mtEM"), DibosonsFacTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEl"), DibosonsFacTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityMu"), DibosonsFacTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("diboson","VR6JGGx12HM_aplanarityEM"), DibosonsFacTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("diboson","SR4JhighxGGEl"), DibosonsFacTheoSR4JhighxGG))
    generatorSyst.append((("diboson","SR4JhighxGGMu"), DibosonsFacTheoSR4JhighxGG))   
    generatorSyst.append((("diboson","SR4JhighxGGEM"), DibosonsFacTheoSR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEl"), DibosonsFacTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGMu"), DibosonsFacTheoTR4JhighxGG))
    generatorSyst.append((("diboson","TR4JhighxGGEM"), DibosonsFacTheoTR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEl"), DibosonsFacTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGMu"), DibosonsFacTheoWR4JhighxGG))
    generatorSyst.append((("diboson","WR4JhighxGGEM"), DibosonsFacTheoWR4JhighxGG))    
    generatorSyst.append((("diboson","VR4JhighxGG_mtEl"), DibosonsFacTheoVR4JhighxGG_mt))
    generatorSyst.append((("diboson","VR4JhighxGG_mtMu"), DibosonsFacTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("diboson","VR4JhighxGG_mtEM"), DibosonsFacTheoVR4JhighxGG_mt))   
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEl"), DibosonsFacTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffMu"), DibosonsFacTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("diboson","VR4JhighxGG_metovermeffEM"), DibosonsFacTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("diboson","SR4JlowxGGexclEl"), DibosonsFacTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGexclMu"), DibosonsFacTheoSR4JlowxGGexcl))   
    generatorSyst.append((("diboson","SR4JlowxGGexclEM"), DibosonsFacTheoSR4JlowxGGexcl))
    generatorSyst.append((("diboson","SR4JlowxGGdiscEl"), DibosonsFacTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","SR4JlowxGGdiscMu"), DibosonsFacTheoSR4JlowxGGdisc))   
    generatorSyst.append((("diboson","SR4JlowxGGdiscEM"), DibosonsFacTheoSR4JlowxGGdisc))
    generatorSyst.append((("diboson","TR4JlowxGGEl"), DibosonsFacTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGMu"), DibosonsFacTheoTR4JlowxGG))
    generatorSyst.append((("diboson","TR4JlowxGGEM"), DibosonsFacTheoTR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEl"), DibosonsFacTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGMu"), DibosonsFacTheoWR4JlowxGG))
    generatorSyst.append((("diboson","WR4JlowxGGEM"), DibosonsFacTheoWR4JlowxGG))    
    generatorSyst.append((("diboson","VR4JlowxGG_mtEl"), DibosonsFacTheoVR4JlowxGG_mt))
    generatorSyst.append((("diboson","VR4JlowxGG_mtMu"), DibosonsFacTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("diboson","VR4JlowxGG_mtEM"), DibosonsFacTheoVR4JlowxGG_mt))   
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEl"), DibosonsFacTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffMu"), DibosonsFacTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("diboson","VR4JlowxGG_metovermeffEM"), DibosonsFacTheoVR4JlowxGG_aplanarity))


    generatorSyst.append((("diboson","SR4JSSx12El"), DibosonsGenTheoSR4JSSx12))
    generatorSyst.append((("diboson","SR4JSSx12Mu"), DibosonsGenTheoSR4JSSx12))   
    generatorSyst.append((("diboson","SR4JSSx12EM"), DibosonsGenTheoSR4JSSx12))

    #generatorSyst.append((("diboson","WR4JSSx12El"), DibosonsGenTheoWR4JSSx12))
    #generatorSyst.append((("diboson","WR4JSSx12Mu"), DibosonsGenTheoWR4JSSx12))   
    #generatorSyst.append((("diboson","WR4JSSx12EM"), DibosonsGenTheoWR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12El"), DibosonsGenTheoTR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12Mu"), DibosonsGenTheoTR4JSSx12))   
    #generatorSyst.append((("diboson","TR4JSSx12EM"), DibosonsGenTheoTR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12El"), DibosonsGenTheoDR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12Mu"), DibosonsGenTheoDR4JSSx12))   
    #generatorSyst.append((("diboson","DR4JSSx12EM"), DibosonsGenTheoDR4JSSx12))
    generatorSyst.append((("diboson","VR4JSSx12_mtEl"), DibosonsGenTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_mtMu"), DibosonsGenTheoVR4JSSx12_mt))    
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEl"), DibosonsGenTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityMu"), DibosonsGenTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSx12_mtEM"), DibosonsGenTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEM"), DibosonsGenTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_DREl"), DibosonsGenTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DRMu"), DibosonsGenTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DREM"), DibosonsGenTheoVR4JSSx12_DR))





   
    generatorSyst.append((("diboson","SR5JSSx12El"), DibosonsGenTheoSR5JSSx12))
    generatorSyst.append((("diboson","SR5JSSx12Mu"), DibosonsGenTheoSR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12El"), DibosonsGenTheoWR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12Mu"), DibosonsGenTheoWR5JSSx12))   
    #generatorSyst.append((("diboson","WR5JSSx12EM"), DibosonsGenTheoWR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12El"), DibosonsGenTheoTR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12Mu"), DibosonsGenTheoTR5JSSx12))   
    #generatorSyst.append((("diboson","TR5JSSx12EM"), DibosonsGenTheoTR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12El"), DibosonsGenTheoDR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12Mu"), DibosonsGenTheoDR5JSSx12))   
    #generatorSyst.append((("diboson","DR5JSSx12EM"), DibosonsGenTheoDR5JSSx12))   
    generatorSyst.append((("diboson","SR5JSSx12EM"), DibosonsGenTheoSR5JSSx12))
    generatorSyst.append((("diboson","VR5JSSx12_mtEl"), DibosonsGenTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_mtMu"), DibosonsGenTheoVR5JSSx12_mt))    
    generatorSyst.append((("diboson","VR5JSSx12_metEl"), DibosonsGenTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_metMu"), DibosonsGenTheoVR5JSSx12_met))    
    generatorSyst.append((("diboson","VR5JSSx12_mtEM"), DibosonsGenTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_metEM"), DibosonsGenTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_DREM"), DibosonsGenTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DREl"), DibosonsGenTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DRMu"), DibosonsGenTheoVR5JSSx12_DR))



    generatorSyst.append((("diboson","SR4JSSlowxEl"), DibosonsGenTheoSR4JSSlowx))
    generatorSyst.append((("diboson","SR4JSSlowxMu"), DibosonsGenTheoSR4JSSlowx))   
    generatorSyst.append((("diboson","SR4JSSlowxEM"), DibosonsGenTheoSR4JSSlowx))

    generatorSyst.append((("diboson","SR4JSSlowxnoBJetEM"), DibosonsGenTheoSR4JSSlowx))

    generatorSyst.append((("diboson","VR4JSSlowx_mtEl"), DibosonsGenTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_mtMu"), DibosonsGenTheoVR4JSSlowx_mt))
    #generatorSyst.append((("diboson","WR4JSSlowxEl"), DibosonsGenTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","WR4JSSlowxMu"), DibosonsGenTheoWR4JSSlowx))   
    #generatorSyst.append((("diboson","WR4JSSlowxEM"), DibosonsGenTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxEl"), DibosonsGenTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxMu"), DibosonsGenTheoTR4JSSlowx))   
    #generatorSyst.append((("diboson","TR4JSSlowxEM"), DibosonsGenTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxEl"), DibosonsGenTheoDR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxMu"), DibosonsGenTheoDR4JSSlowx))   
    #generatorSyst.append((("diboson","DR4JSSlowxEM"), DibosonsGenTheoDR4JSSlowx))    
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEl"), DibosonsGenTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityMu"), DibosonsGenTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSlowx_mtEM"), DibosonsGenTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEM"), DibosonsGenTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_DREM"), DibosonsGenTheoVR4JSSlowx_DR))
    generatorSyst.append((("diboson","VR4JSSlowx_DREl"), DibosonsGenTheoVR4JSSlowx_DR))
    generatorSyst.append((("diboson","VR4JSSlowx_DRMu"), DibosonsGenTheoVR4JSSlowx_DR))


    generatorSyst.append((("diboson","SR5JSShighxEl"), DibosonsGenTheoSR5JSShighx))
    generatorSyst.append((("diboson","SR5JSShighxMu"), DibosonsGenTheoSR5JSShighx))   
    generatorSyst.append((("diboson","SR5JSShighxEM"), DibosonsGenTheoSR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxEl"), DibosonsGenTheoWR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxMu"), DibosonsGenTheoWR5JSShighx))   
    #generatorSyst.append((("diboson","WR5JSShighxEM"), DibosonsGenTheoWR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxEl"), DibosonsGenTheoTR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxMu"), DibosonsGenTheoTR5JSShighx))   
    #generatorSyst.append((("diboson","TR5JSShighxEM"), DibosonsGenTheoTR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxEl"), DibosonsGenTheoDR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxMu"), DibosonsGenTheoDR5JSShighx))   
    #generatorSyst.append((("diboson","DR5JSShighxEM"), DibosonsGenTheoDR5JSShighx))
    generatorSyst.append((("diboson","VR5JSShighx_mtEl"), DibosonsGenTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_mtMu"), DibosonsGenTheoVR5JSShighx_mt))    
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEl"), DibosonsGenTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityMu"), DibosonsGenTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("diboson","VR5JSShighx_mtEM"), DibosonsGenTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEM"), DibosonsGenTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_DREM"), DibosonsGenTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DREl"), DibosonsGenTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DRMu"), DibosonsGenTheoVR5JSShighx_DR))

    


    generatorSyst.append((("diboson","SR4JSSx12El"), DibosonsResummationTheoSR4JSSx12))
    generatorSyst.append((("diboson","SR4JSSx12Mu"), DibosonsResummationTheoSR4JSSx12))   
    generatorSyst.append((("diboson","SR4JSSx12EM"), DibosonsResummationTheoSR4JSSx12)) 
    #generatorSyst.append((("diboson","WR4JSSx12El"), DibosonsResummationTheoWR4JSSx12))
    #generatorSyst.append((("diboson","WR4JSSx12Mu"), DibosonsResummationTheoWR4JSSx12))   
    #generatorSyst.append((("diboson","WR4JSSx12EM"), DibosonsResummationTheoWR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12El"), DibosonsResummationTheoTR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12Mu"), DibosonsResummationTheoTR4JSSx12))   
    #generatorSyst.append((("diboson","TR4JSSx12EM"), DibosonsResummationTheoTR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12El"), DibosonsResummationTheoDR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12Mu"), DibosonsResummationTheoDR4JSSx12))   
    #generatorSyst.append((("diboson","DR4JSSx12EM"), DibosonsResummationTheoDR4JSSx12))     
    generatorSyst.append((("diboson","VR4JSSx12_mtEl"), DibosonsResummationTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_mtMu"), DibosonsResummationTheoVR4JSSx12_mt))    
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEl"), DibosonsResummationTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityMu"), DibosonsResummationTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSx12_mtEM"), DibosonsResummationTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEM"), DibosonsResummationTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_DREM"), DibosonsResummationTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DREl"), DibosonsResummationTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DRMu"), DibosonsResummationTheoVR4JSSx12_DR))


 


  
    generatorSyst.append((("diboson","SR5JSSx12El"), DibosonsResummationTheoSR5JSSx12))
    generatorSyst.append((("diboson","SR5JSSx12Mu"), DibosonsResummationTheoSR5JSSx12))   
    generatorSyst.append((("diboson","SR5JSSx12EM"), DibosonsResummationTheoSR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12El"), DibosonsResummationTheoWR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12Mu"), DibosonsResummationTheoWR5JSSx12))   
    #generatorSyst.append((("diboson","WR5JSSx12EM"), DibosonsResummationTheoWR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12El"), DibosonsResummationTheoTR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12Mu"), DibosonsResummationTheoTR5JSSx12))   
    #generatorSyst.append((("diboson","TR5JSSx12EM"), DibosonsResummationTheoTR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12El"), DibosonsResummationTheoDR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12Mu"), DibosonsResummationTheoDR5JSSx12))   
    #generatorSyst.append((("diboson","DR5JSSx12EM"), DibosonsResummationTheoDR5JSSx12))
    
    generatorSyst.append((("diboson","VR5JSSx12_mtEl"), DibosonsResummationTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_mtMu"), DibosonsResummationTheoVR5JSSx12_mt))    
    generatorSyst.append((("diboson","VR5JSSx12_metEl"), DibosonsResummationTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_metMu"), DibosonsResummationTheoVR5JSSx12_met))    
    generatorSyst.append((("diboson","VR5JSSx12_mtEM"), DibosonsResummationTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_metEM"), DibosonsResummationTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_DREM"), DibosonsResummationTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DREl"), DibosonsResummationTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DRMu"), DibosonsResummationTheoVR5JSSx12_DR))


    generatorSyst.append((("diboson","SR4JSSlowxEl"), DibosonsResummationTheoSR4JSSlowx))
    generatorSyst.append((("diboson","SR4JSSlowxMu"), DibosonsResummationTheoSR4JSSlowx))   
    generatorSyst.append((("diboson","SR4JSSlowxEM"), DibosonsResummationTheoSR4JSSlowx))   

    generatorSyst.append((("diboson","SR4JSSlowxnoBJetEM"), DibosonsResummationTheoSR4JSSlowx))
    #generatorSyst.append((("diboson","WR4JSSlowxEl"), DibosonsResummationTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","WR4JSSlowxMu"), DibosonsResummationTheoWR4JSSlowx))   
    #generatorSyst.append((("diboson","WR4JSSlowxEM"), DibosonsResummationTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxEl"), DibosonsResummationTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxMu"), DibosonsResummationTheoTR4JSSlowx))   
    #generatorSyst.append((("diboson","TR4JSSlowxEM"), DibosonsResummationTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxEl"), DibosonsResummationTheoDR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxMu"), DibosonsResummationTheoDR4JSSlowx))   
    #generatorSyst.append((("diboson","DR4JSSlowxEM"), DibosonsResummationTheoDR4JSSlowx))


    generatorSyst.append((("diboson","VR4JSSlowx_mtEl"), DibosonsResummationTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_mtMu"), DibosonsResummationTheoVR4JSSlowx_mt))    
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEl"), DibosonsResummationTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityMu"), DibosonsResummationTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSlowx_mtEM"), DibosonsResummationTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEM"), DibosonsResummationTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_DREM"), DibosonsResummationTheoVR4JSSlowx_DR))
    generatorSyst.append((("diboson","VR4JSSlowx_DREl"), DibosonsResummationTheoVR4JSSlowx_DR))
    generatorSyst.append((("diboson","VR4JSSlowx_DRMu"), DibosonsResummationTheoVR4JSSlowx_DR))


    generatorSyst.append((("diboson","SR5JSShighxEl"), DibosonsResummationTheoSR5JSShighx))
    generatorSyst.append((("diboson","SR5JSShighxMu"), DibosonsResummationTheoSR5JSShighx))   
    generatorSyst.append((("diboson","SR5JSShighxEM"), DibosonsResummationTheoSR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxEl"), DibosonsResummationTheoWR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxMu"), DibosonsResummationTheoWR5JSShighx))   
    #generatorSyst.append((("diboson","WR5JSShighxEM"), DibosonsResummationTheoWR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxEl"), DibosonsResummationTheoTR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxMu"), DibosonsResummationTheoTR5JSShighx))   
    #generatorSyst.append((("diboson","TR5JSShighxEM"), DibosonsResummationTheoTR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxEl"), DibosonsResummationTheoDR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxMu"), DibosonsResummationTheoDR5JSShighx))   
    #generatorSyst.append((("diboson","DR5JSShighxEM"), DibosonsResummationTheoDR5JSShighx))
    generatorSyst.append((("diboson","VR5JSShighx_mtEl"), DibosonsResummationTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_mtMu"), DibosonsResummationTheoVR5JSShighx_mt))    
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEl"), DibosonsResummationTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityMu"), DibosonsResummationTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("diboson","VR5JSShighx_mtEM"), DibosonsResummationTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEM"), DibosonsResummationTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_DREM"), DibosonsResummationTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DREl"), DibosonsResummationTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DRMu"), DibosonsResummationTheoVR5JSShighx_DR))



    generatorSyst.append((("diboson","SR4JSSx12El"), DibosonsRenormTheoSR4JSSx12))
    generatorSyst.append((("diboson","SR4JSSx12Mu"), DibosonsRenormTheoSR4JSSx12))   
    generatorSyst.append((("diboson","SR4JSSx12EM"), DibosonsRenormTheoSR4JSSx12))
    #generatorSyst.append((("diboson","WR4JSSx12El"), DibosonsRenormTheoWR4JSSx12))
    #generatorSyst.append((("diboson","WR4JSSx12Mu"), DibosonsRenormTheoWR4JSSx12))   
    #generatorSyst.append((("diboson","WR4JSSx12EM"), DibosonsRenormTheoWR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12El"), DibosonsRenormTheoTR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12Mu"), DibosonsRenormTheoTR4JSSx12))   
    #generatorSyst.append((("diboson","TR4JSSx12EM"), DibosonsRenormTheoTR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12El"), DibosonsRenormTheoDR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12Mu"), DibosonsRenormTheoDR4JSSx12))   
    #generatorSyst.append((("diboson","DR4JSSx12EM"), DibosonsRenormTheoDR4JSSx12))     
    #generatorSyst.append((("diboson","VR4JSSx12_mtEl"), DibosonsRenormTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_mtMu"), DibosonsRenormTheoVR4JSSx12_mt))    
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEl"), DibosonsRenormTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityMu"), DibosonsRenormTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSx12_mtEM"), DibosonsRenormTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEM"), DibosonsRenormTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_DREM"), DibosonsRenormTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DREl"), DibosonsRenormTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DRMu"), DibosonsRenormTheoVR4JSSx12_DR))

   
    generatorSyst.append((("diboson","SR5JSSx12El"), DibosonsRenormTheoSR5JSSx12))
    generatorSyst.append((("diboson","SR5JSSx12Mu"), DibosonsRenormTheoSR5JSSx12))   
    generatorSyst.append((("diboson","SR5JSSx12EM"), DibosonsRenormTheoSR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12El"), DibosonsRenormTheoWR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12Mu"), DibosonsRenormTheoWR5JSSx12))   
    #generatorSyst.append((("diboson","WR5JSSx12EM"), DibosonsRenormTheoWR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12El"), DibosonsRenormTheoTR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12Mu"), DibosonsRenormTheoTR5JSSx12))   
    #generatorSyst.append((("diboson","TR5JSSx12EM"), DibosonsRenormTheoTR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12El"), DibosonsRenormTheoDR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12Mu"), DibosonsRenormTheoDR5JSSx12))   
    #generatorSyst.append((("diboson","DR5JSSx12EM"), DibosonsRenormTheoDR5JSSx12))
    generatorSyst.append((("diboson","VR5JSSx12_mtEl"), DibosonsRenormTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_mtMu"), DibosonsRenormTheoVR5JSSx12_mt))    
    generatorSyst.append((("diboson","VR5JSSx12_metEl"), DibosonsRenormTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_metMu"), DibosonsRenormTheoVR5JSSx12_met))    
    generatorSyst.append((("diboson","VR5JSSx12_mtEM"), DibosonsRenormTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_metEM"), DibosonsRenormTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_DREM"), DibosonsRenormTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DREl"), DibosonsRenormTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DRMu"), DibosonsRenormTheoVR5JSSx12_DR))


    #generatorSyst.append((("diboson","WR4JSSlowxEl"), DibosonsRenormTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","WR4JSSlowxMu"), DibosonsRenormTheoWR4JSSlowx))   
    #generatorSyst.append((("diboson","WR4JSSlowxEM"), DibosonsRenormTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxEl"), DibosonsRenormTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxMu"), DibosonsRenormTheoTR4JSSlowx))   
    #generatorSyst.append((("diboson","TR4JSSlowxEM"), DibosonsRenormTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxEl"), DibosonsRenormTheoDR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxMu"), DibosonsRenormTheoDR4JSSlowx))   
    #generatorSyst.append((("diboson","DR4JSSlowxEM"), DibosonsRenormTheoDR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxEl"), DibosonsRenormTheoDR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxMu"), DibosonsRenormTheoDR4JSSlowx))


    generatorSyst.append((("diboson","VR4JSSlowx_mtEl"), DibosonsRenormTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_mtMu"), DibosonsRenormTheoVR4JSSlowx_mt))
      
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEl"), DibosonsRenormTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityMu"), DibosonsRenormTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSlowx_mtEM"), DibosonsRenormTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEM"), DibosonsRenormTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_DREM"), DibosonsRenormTheoVR4JSSlowx_DR))


    generatorSyst.append((("diboson","SR5JSShighxEl"), DibosonsRenormTheoSR5JSShighx))
    generatorSyst.append((("diboson","SR5JSShighxMu"), DibosonsRenormTheoSR5JSShighx))   
    generatorSyst.append((("diboson","SR5JSShighxEM"), DibosonsRenormTheoSR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxEl"), DibosonsRenormTheoWR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxMu"), DibosonsRenormTheoWR5JSShighx))   
    #generatorSyst.append((("diboson","WR5JSShighxEM"), DibosonsRenormTheoWR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxEl"), DibosonsRenormTheoTR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxMu"), DibosonsRenormTheoTR5JSShighx))   
    #generatorSyst.append((("diboson","TR5JSShighxEM"), DibosonsRenormTheoTR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxEl"), DibosonsRenormTheoDR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxMu"), DibosonsRenormTheoDR5JSShighx))   
    #generatorSyst.append((("diboson","DR5JSShighxEM"), DibosonsRenormTheoDR5JSShighx))
    generatorSyst.append((("diboson","VR5JSShighx_mtEl"), DibosonsRenormTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_mtMu"), DibosonsRenormTheoVR5JSShighx_mt))    
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEl"), DibosonsRenormTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityMu"), DibosonsRenormTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("diboson","VR5JSShighx_mtEM"), DibosonsRenormTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEM"), DibosonsRenormTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_DREM"), DibosonsRenormTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DREl"), DibosonsRenormTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DRMu"), DibosonsRenormTheoVR5JSShighx_DR))    

    generatorSyst.append((("diboson","SR4JSSx12El"), DibosonsFacTheoSR4JSSx12))
    generatorSyst.append((("diboson","SR4JSSx12Mu"), DibosonsFacTheoSR4JSSx12))   
    generatorSyst.append((("diboson","SR4JSSx12EM"), DibosonsFacTheoSR4JSSx12))
    #generatorSyst.append((("diboson","WR4JSSx12El"), DibosonsFacTheoWR4JSSx12))
    #generatorSyst.append((("diboson","WR4JSSx12Mu"), DibosonsFacTheoWR4JSSx12))   
    #generatorSyst.append((("diboson","WR4JSSx12EM"), DibosonsFacTheoWR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12El"), DibosonsFacTheoTR4JSSx12))
    #generatorSyst.append((("diboson","TR4JSSx12Mu"), DibosonsFacTheoTR4JSSx12))   
    #generatorSyst.append((("diboson","TR4JSSx12EM"), DibosonsFacTheoTR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12El"), DibosonsFacTheoDR4JSSx12))
    #generatorSyst.append((("diboson","DR4JSSx12Mu"), DibosonsFacTheoDR4JSSx12))   
    #generatorSyst.append((("diboson","DR4JSSx12EM"), DibosonsFacTheoDR4JSSx12))
    generatorSyst.append((("diboson","VR4JSSx12_mtEl"), DibosonsFacTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_mtMu"), DibosonsFacTheoVR4JSSx12_mt))    
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEl"), DibosonsFacTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityMu"), DibosonsFacTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSx12_mtEM"), DibosonsFacTheoVR4JSSx12_mt))
    generatorSyst.append((("diboson","VR4JSSx12_aplanarityEM"), DibosonsFacTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("diboson","VR4JSSx12_DREM"), DibosonsFacTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DREl"), DibosonsFacTheoVR4JSSx12_DR))
    generatorSyst.append((("diboson","VR4JSSx12_DRMu"), DibosonsFacTheoVR4JSSx12_DR))

   
    generatorSyst.append((("diboson","SR5JSSx12El"), DibosonsFacTheoSR5JSSx12))
    generatorSyst.append((("diboson","SR5JSSx12Mu"), DibosonsFacTheoSR5JSSx12))   
    generatorSyst.append((("diboson","SR5JSSx12EM"), DibosonsFacTheoSR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12El"), DibosonsFacTheoWR5JSSx12))
    #generatorSyst.append((("diboson","WR5JSSx12Mu"), DibosonsFacTheoWR5JSSx12))   
    #generatorSyst.append((("diboson","WR5JSSx12EM"), DibosonsFacTheoWR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12El"), DibosonsFacTheoTR5JSSx12))
    #generatorSyst.append((("diboson","TR5JSSx12Mu"), DibosonsFacTheoTR5JSSx12))   
    #generatorSyst.append((("diboson","TR5JSSx12EM"), DibosonsFacTheoTR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12El"), DibosonsFacTheoDR5JSSx12))
    #generatorSyst.append((("diboson","DR5JSSx12Mu"), DibosonsFacTheoDR5JSSx12))   
    #generatorSyst.append((("diboson","DR5JSSx12EM"), DibosonsFacTheoDR5JSSx12))
    generatorSyst.append((("diboson","VR5JSSx12_mtEl"), DibosonsFacTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_mtMu"), DibosonsFacTheoVR5JSSx12_mt))    
    generatorSyst.append((("diboson","VR5JSSx12_metEl"), DibosonsFacTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_metMu"), DibosonsFacTheoVR5JSSx12_met))    
    generatorSyst.append((("diboson","VR5JSSx12_mtEM"), DibosonsFacTheoVR5JSSx12_mt))
    generatorSyst.append((("diboson","VR5JSSx12_metEM"), DibosonsFacTheoVR5JSSx12_met))
    generatorSyst.append((("diboson","VR5JSSx12_DREM"), DibosonsFacTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DREl"), DibosonsFacTheoVR5JSSx12_DR))
    generatorSyst.append((("diboson","VR5JSSx12_DRu"), DibosonsFacTheoVR5JSSx12_DR))


    generatorSyst.append((("diboson","SR4JSSlowxEl"), DibosonsFacTheoSR4JSSlowx))
    generatorSyst.append((("diboson","SR4JSSlowxMu"), DibosonsFacTheoSR4JSSlowx))   
    generatorSyst.append((("diboson","SR4JSSlowxEM"), DibosonsFacTheoSR4JSSlowx))
    generatorSyst.append((("diboson","SR4JSSlowxnoBJetEM"), DibosonsFacTheoSR4JSSlowx))
    #generatorSyst.append((("diboson","WR4JSSlowxEl"), DibosonsFacTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","WR4JSSlowxMu"), DibosonsFacTheoWR4JSSlowx))   
    #generatorSyst.append((("diboson","WR4JSSlowxEM"), DibosonsFacTheoWR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxEl"), DibosonsFacTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","TR4JSSlowxMu"), DibosonsFacTheoTR4JSSlowx))   
    #generatorSyst.append((("diboson","TR4JSSlowxEM"), DibosonsFacTheoTR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxEl"), DibosonsFacTheoDR4JSSlowx))
    #generatorSyst.append((("diboson","DR4JSSlowxMu"), DibosonsFacTheoDR4JSSlowx))   
    #generatorSyst.append((("diboson","DR4JSSlowxEM"), DibosonsFacTheoDR4JSSlowx))    


    generatorSyst.append((("diboson","VR4JSSlowx_mtEl"), DibosonsFacTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_mtMu"), DibosonsFacTheoVR4JSSlowx_mt))    
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEl"), DibosonsFacTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityMu"), DibosonsFacTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("diboson","VR4JSSlowx_mtEM"), DibosonsFacTheoVR4JSSlowx_mt))
    generatorSyst.append((("diboson","VR4JSSlowx_aplanarityEM"), DibosonsFacTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("diboson","VR4JSSlowx_DREM"), DibosonsFacTheoVR4JSSlowx_DR))
    generatorSyst.append((("diboson","VR4JSSlowx_DREl"), DibosonsFacTheoVR4JSSlowx_DR))
    generatorSyst.append((("diboson","VR4JSSlowx_DRMu"), DibosonsFacTheoVR4JSSlowx_DR))


    generatorSyst.append((("diboson","SR5JSShighxEl"), DibosonsFacTheoSR5JSShighx))
    generatorSyst.append((("diboson","SR5JSShighxMu"), DibosonsFacTheoSR5JSShighx))   
    generatorSyst.append((("diboson","SR5JSShighxEM"), DibosonsFacTheoSR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxEl"), DibosonsFacTheoWR5JSShighx))
    #generatorSyst.append((("diboson","WR5JSShighxMu"), DibosonsFacTheoWR5JSShighx))   
    #generatorSyst.append((("diboson","WR5JSShighxEM"), DibosonsFacTheoWR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxEl"), DibosonsFacTheoTR5JSShighx))
    #generatorSyst.append((("diboson","TR5JSShighxMu"), DibosonsFacTheoTR5JSShighx))   
    #generatorSyst.append((("diboson","TR5JSShighxEM"), DibosonsFacTheoTR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxEl"), DibosonsFacTheoDR5JSShighx))
    #generatorSyst.append((("diboson","DR5JSShighxMu"), DibosonsFacTheoDR5JSShighx))   
    #generatorSyst.append((("diboson","DR5JSShighxEM"), DibosonsFacTheoDR5JSShighx))
    generatorSyst.append((("diboson","VR5JSShighx_mtEl"), DibosonsFacTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_mtMu"), DibosonsFacTheoVR5JSShighx_mt))    
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEl"), DibosonsFacTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityMu"), DibosonsFacTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("diboson","VR5JSShighx_mtEM"), DibosonsFacTheoVR5JSShighx_mt))
    generatorSyst.append((("diboson","VR5JSShighx_aplanarityEM"), DibosonsFacTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("diboson","VR5JSShighx_DREM"), DibosonsFacTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DREl"), DibosonsFacTheoVR5JSShighx_DR))
    generatorSyst.append((("diboson","VR5JSShighx_DRMu"), DibosonsFacTheoVR5JSShighx_DR))
    

   

  
    return generatorSyst
