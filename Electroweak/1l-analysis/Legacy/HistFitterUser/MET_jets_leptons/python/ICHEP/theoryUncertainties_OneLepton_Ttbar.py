import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


TtbarRadQCDScalesTheoSR2J = Systematic("TtbarRadQCDScalesTheo_SR2J", configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR2J_1 = Systematic("TtbarRadQCDScalesTheo_VR2J_1",configMgr.weights, 1.+0.05, 1.-0.05, "user","userOverallSys")
TtbarRadQCDScalesTheoVR2J_2 = Systematic("TtbarRadQCDScalesTheo_VR2J_2",configMgr.weights, 1.+0.03, 1.-0.03, "user","userOverallSys")

TtbarRadQCDScalesTheoSR6JGGx12 = Systematic("TtbarRadQCDScalesTheo_SR6JGGx12", configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR6JGGx12_mt = Systematic("TtbarRadQCDScalesTheo_VR6JGGx12_mt",configMgr.weights, 1.+0.09, 1.-0.09, "user","userOverallSys")
TtbarRadQCDScalesTheoVR6JGGx12_aplanarity = Systematic("TtbarRadQCDScalesTheo_VR6JGGx12_aplanarity",configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")


TtbarRadQCDScalesTheoSR6JGGx12HM = Systematic("TtbarRadQCDScalesTheo_SR6JGGx12HM", configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR6JGGx12HM_mt = Systematic("TtbarRadQCDScalesTheo_VR6JGGx12HM_mt",configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR6JGGx12HM_aplanarity = Systematic("TtbarRadQCDScalesTheo_VR6JGGx12HM_aplanarity",configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")


TtbarRadQCDScalesTheoSR4JhighxGG = Systematic("TtbarRadQCDScalesTheo_SR4JhighxGG", configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JhighxGG_mt = Systematic("TtbarRadQCDScalesTheo_VR4JhighxGG_mt",configMgr.weights, 1.+0.06, 1.-0.06, "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JhighxGG_metovermeff = Systematic("TtbarRadQCDScalesTheo_VR4JhighxGG_aplanarity",configMgr.weights, 1.+0.11, 1.-0.11, "user","userOverallSys")


TtbarRadQCDScalesTheoSR4JlowxGGexcl = Systematic("TtbarRadQCDScalesTheo_SR4JlowxGGexcl", configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarRadQCDScalesTheoSR4JlowxGGdisc = Systematic("TtbarRadQCDScalesTheo_SR4JlowxGGdisc", configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JlowxGG_mt = Systematic("TtbarRadQCDScalesTheo_VR4JlowxGG_mt",configMgr.weights, 1.+0.02, 1.-0.02, "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JlowxGG_aplanarity = Systematic("TtbarRadQCDScalesTheo_VR4JlowxGG_aplanarity",configMgr.weights, 1.+0.02, 1.-0.02, "user","userOverallSys")








TtbarHadFragTheoSR2J = Systematic("TtbarHadFragTheo_SR2J", configMgr.weights, 1.+0.07, 1.-0.07, "user","userOverallSys")
TtbarHadFragTheoVR2J_1 = Systematic("TtbarHadFragTheo_VR2J_1",configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
TtbarHadFragTheoVR2J_2 = Systematic("TtbarHadFragTheo_VR2J_2",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")

TtbarHadFragTheoSR6JGGx12 = Systematic("TtbarHadFragTheo_SR6JGGx12", configMgr.weights, 1.+0.14, 1.-0.14 , "user","userOverallSys")
TtbarHadFragTheoVR6JGGx12_mt = Systematic("TtbarHadFragTheo_VR6JGGx12_mt",configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
TtbarHadFragTheoVR6JGGx12_aplanarity = Systematic("TtbarHadFragTheo_VR6JGGx12_aplanarity",configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")


TtbarHadFragTheoSR6JGGx12HM = Systematic("TtbarHadFragTheo_SR6JGGx12HM", configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
TtbarHadFragTheoVR6JGGx12HM_mt = Systematic("TtbarHadFragTheo_VR6JGGx12HM_mt",configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
TtbarHadFragTheoVR6JGGx12HM_aplanarity = Systematic("TtbarHadFragTheo_VR6JGGx12HM_aplanarity",configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")


TtbarHadFragTheoSR4JhighxGG = Systematic("TtbarHadFragTheo_SR4JhighxGG", configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
TtbarHadFragTheoVR4JhighxGG_mt = Systematic("TtbarHadFragTheo_VR4JhighxGG_mt",configMgr.weights, 1.+0.07, 1.-0.07, "user","userOverallSys")
TtbarHadFragTheoVR4JhighxGG_metovermeff = Systematic("TtbarHadFragTheo_VR4JhighxGG_metovermeff",configMgr.weights, 1.+0.06, 1.-0.06, "user","userOverallSys")


TtbarHadFragTheoSR4JlowxGGexcl = Systematic("TtbarHadFragTheo_SR4JlowxGGdisc", configMgr.weights, 1.+0.12, 1.-0.12, "user","userOverallSys")
TtbarHadFragTheoSR4JlowxGGdisc = Systematic("TtbarHadFragTheo_SR4JlowxGGexcl", configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
TtbarHadFragTheoVR4JlowxGG_mt = Systematic("TtbarHadFragTheo_VR4JlowxGG_mt",configMgr.weights, 1.+0.12, 1.-0.12, "user","userOverallSys")
TtbarHadFragTheoVR4JlowxGG_aplanarity = Systematic("TtbarHadFragTheo_VR4JlowxGG_aplanarity",configMgr.weights, 1.+0.04, 1.-0.04, "user","userOverallSys")





TtbarHardScatGenTheoSR2J = Systematic("TtbarHardScatGenTheo_SR2J", configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
TtbarHardScatGenTheoVR2J_1 = Systematic("TtbarHardScatGenTheo_VR2J_1",configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
TtbarHardScatGenTheoVR2J_2 = Systematic("TtbarHardScatGenTheo_VR2J_2",configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")

TtbarHardScatGenTheoSR6JGGx12 = Systematic("TtbarHardScatGenTheo_SR6JGGx12", configMgr.weights, 1.+0.14, 1.-0.14, "user","userOverallSys")
TtbarHardScatGenTheoVR6JGGx12_mt = Systematic("TtbarHardScatGenTheo_VR6JGGx12_mt",configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
TtbarHardScatGenTheoVR6JGGx12_aplanarity = Systematic("TtbarHardScatGenTheo_VR6JGGx12_aplanarity",configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")


TtbarHardScatGenTheoSR6JGGx12HM = Systematic("TtbarHardScatGenTheo_SR6JGGx12HM", configMgr.weights, 1.+0.35, 1.-0.35 , "user","userOverallSys")
TtbarHardScatGenTheoVR6JGGx12HM_mt = Systematic("TtbarHardScatGenTheo_VR6JGGx12HM_mt",configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
TtbarHardScatGenTheoVR6JGGx12HM_aplanarity = Systematic("TtbarHardScatGenTheo_VR6JGGx12HM_aplanarity",configMgr.weights, 1.+0.19, 1.-0.19, "user","userOverallSys")


TtbarHardScatGenTheoSR4JhighxGG = Systematic("TtbarHardScatGenTheo_SR4JhighxGG", configMgr.weights, 1.+0.39, 1.-0.39 , "user","userOverallSys")
TtbarHardScatGenTheoVR4JhighxGG_mt = Systematic("TtbarHardScatGenTheo_VR4JhighxGG_mt",configMgr.weights, 1.+0.08, 1.-0.08, "user","userOverallSys")
TtbarHardScatGenTheoVR4JhighxGG_metovermeff = Systematic("TtbarHardScatGenTheo_VR4JhighxGG_metovermeff",configMgr.weights, 1.+0.10, 1.-0.10, "user","userOverallSys")


TtbarHardScatGenTheoSR4JlowxGGexcl = Systematic("TtbarHardScatGenTheo_SR4JlowxGGdisc", configMgr.weights, 1.+0.21, 1.-0.21 , "user","userOverallSys")
TtbarHardScatGenTheoSR4JlowxGGdisc = Systematic("TtbarHardScatGenTheo_SR4JlowxGGexcl", configMgr.weights, 1.+0.21, 1.-0.21 , "user","userOverallSys")
TtbarHardScatGenTheoVR4JlowxGG_mt = Systematic("TtbarHardScatGenTheo_VR4JlowxGG_mt",configMgr.weights, 1.+0.13, 1.-0.13, "user","userOverallSys")
TtbarHardScatGenTheoVR4JlowxGG_aplanarity = Systematic("TtbarHardScatGenTheo_VR4JlowxGG_aplanarity",configMgr.weights, 1.+0.10, 1.-0.10, "user","userOverallSys")










TtbarRadQCDScalesTheoSR4JSSx12 = Systematic("TtbarRadQCDScalesTheo_SR4JSSx12", configMgr.weights, 1.+0.10, 1.-0.10, "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JSSx12_mt = Systematic("TtbarRadQCDScalesTheo_VR4JSSx12_mt",configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JSSx12_aplanarity = Systematic("TtbarRadQCDScalesTheo_VR4JSSx12_aplanarity",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")

TtbarRadQCDScalesTheoSR5JSSx12 = Systematic("TtbarRadQCDScalesTheo_SR5JSSx12", configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR5JSSx12_mt = Systematic("TtbarRadQCDScalesTheo_VR5JSSx12_mt",configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR5JSSx12_met = Systematic("TtbarRadQCDScalesTheo_VR5JSSx12_met",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")

TtbarRadQCDScalesTheoSR4JSSlowx = Systematic("TtbarRadQCDScalesTheo_SR4JSSlowx", configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JSSlowx_mt = Systematic("TtbarRadQCDScalesTheo_VR4JSSlowx_mt",configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JSSlowx_aplanarity = Systematic("TtbarRadQCDScalesTheo_VR4JSSlowx_aplanarity",configMgr.weights, 1.+0.04, 1.-0.04, "user","userOverallSys")

TtbarRadQCDScalesTheoSR5JSShighx = Systematic("TtbarRadQCDScalesTheo_SR5JSShighx", configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR5JSShighx_mt = Systematic("TtbarRadQCDScalesTheo_VR5JSShighx_mt",configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR5JSShighx_aplanarity = Systematic("TtbarRadQCDScalesTheo_VR5JSShighx_aplanarity",configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")



TtbarRadQCDScalesTheoVR4JSSx12_DB = Systematic("TtbarRadQCDScalesTheo_VR4JSSx12_DB",configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR5JSSx12_DB = Systematic("TtbarRadQCDScalesTheo_VR5JSSx12_DB",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR4JSSlowx_DB = Systematic("TtbarRadQCDScalesTheo_VR4JSSlowx_DB",configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarRadQCDScalesTheoVR5JSShighx_DB = Systematic("TtbarRadQCDScalesTheo_VR5JSShighx_DB",configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")



TtbarHadFragTheoSR4JSSx12 = Systematic("TtbarHadFragTheo_SR4JSSx12", configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
TtbarHadFragTheoVR4JSSx12_mt = Systematic("TtbarHadFragTheo_VR4JSSx12_mt",configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
TtbarHadFragTheoVR4JSSx12_aplanarity = Systematic("TtbarHadFragTheo_VR4JSSx12_aplanarity",configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")

TtbarHadFragTheoSR5JSSx12 = Systematic("TtbarHadFragTheo_SR5JSSx12", configMgr.weights, 1.+0.08, 1.-0.08, "user","userOverallSys")
TtbarHadFragTheoVR5JSSx12_mt = Systematic("TtbarHadFragTheo_VR5JSSx12_mt",configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
TtbarHadFragTheoVR5JSSx12_met = Systematic("TtbarHadFragTheo_VR5JSSx12_met",configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")

TtbarHadFragTheoSR4JSSlowx = Systematic("TtbarHadFragTheo_SR4JSSlowx", configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
TtbarHadFragTheoVR4JSSlowx_mt = Systematic("TtbarHadFragTheo_VR4JSSlowx_mt",configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
TtbarHadFragTheoVR4JSSlowx_aplanarity = Systematic("TtbarHadFragTheo_VR4JSSlowx_aplanarity",configMgr.weights, 1.+0.02, 1.-0.02, "user","userOverallSys")

TtbarHadFragTheoSR5JSShighx = Systematic("TtbarHadFragTheo_SR5JSShighx", configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
TtbarHadFragTheoVR5JSShighx_mt = Systematic("TtbarHadFragTheo_VR5JSShighx_mt",configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
TtbarHadFragTheoVR5JSShighx_aplanarity = Systematic("TtbarHadFragTheo_VR5JSShighx_aplanarity",configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")



TtbarHadFragTheoVR4JSSx12_DB = Systematic("TtbarHadFragTheo_VR4JSSx12_DB",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
TtbarHadFragTheoVR5JSSx12_DB = Systematic("TtbarHadFragTheo_VR5JSSx12_DB",configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarHadFragTheoVR4JSSlowx_DB = Systematic("TtbarHadFragTheo_VR4JSSlowx_DB",configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
TtbarHadFragTheoVR5JSShighx_DB = Systematic("TtbarHadFragTheo_VR5JSShighx_DB",configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")




TtbarHardScatGenTheoSR4JSSx12 = Systematic("TtbarHardScatGenTheo_SR4JSSx12", configMgr.weights, 1.+0.14, 1.-0.14 , "user","userOverallSys")
TtbarHardScatGenTheoVR4JSSx12_mt = Systematic("TtbarHardScatGenTheo_VR4JSSx12_mt",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
TtbarHardScatGenTheoVR4JSSx12_aplanarity = Systematic("TtbarHardScatGenTheo_VR4JSSx12_aplanarity",configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")


TtbarHardScatGenTheoSR5JSSx12 = Systematic("TtbarHardScatGenTheo_SR5JSSx12", configMgr.weights, 1.+0.17, 1.-0.17, "user","userOverallSys")
TtbarHardScatGenTheoVR5JSSx12_mt = Systematic("TtbarHardScatGenTheo_VR5JSSx12_mt",configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
TtbarHardScatGenTheoVR5JSSx12_met = Systematic("TtbarHardScatGenTheo_VR5JSSx12_met",configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")


TtbarHardScatGenTheoSR4JSSlowx = Systematic("TtbarHardScatGenTheo_SR4JSSlowx", configMgr.weights, 1.+0.35 , 1.-0.35  , "user","userOverallSys")
TtbarHardScatGenTheoVR4JSSlowx_mt = Systematic("TtbarHardScatGenTheo_VR4JSSlowx_mt",configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
TtbarHardScatGenTheoVR4JSSlowx_aplanarity = Systematic("TtbarHardScatGenTheo_VR4JSSlowx_aplanarity",configMgr.weights, 1.+0.12, 1.-0.12, "user","userOverallSys")

TtbarHardScatGenTheoSR5JSShighx = Systematic("TtbarHardScatGenTheo_SR5JSShighx", configMgr.weights, 1.+0.18, 1.-0.18 , "user","userOverallSys")
TtbarHardScatGenTheoVR5JSShighx_mt = Systematic("TtbarHardScatGenTheo_VR5JSShighx_mt",configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
TtbarHardScatGenTheoVR5JSShighx_aplanarity = Systematic("TtbarHardScatGenTheo_VR5JSShighx_aplanarity",configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")

TtbarHardScatGenTheoVR4JSSx12_DB = Systematic("TtbarHardScatGenTheo_VR4JSSx12_DB",configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
TtbarHardScatGenTheoVR5JSSx12_DB = Systematic("TtbarHardScatGenTheo_VR5JSSx12_DB",configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
TtbarHardScatGenTheoVR4JSSlowx_DB = Systematic("TtbarHardScatGenTheo_VR4JSSlowx_DB",configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
TtbarHardScatGenTheoVR5JSShighx_DB = Systematic("TtbarHardScatGenTheo_VR5JSShighx_DB",configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")




def TheorUnc(generatorSyst):
   
    generatorSyst.append((("ttbar","SR2JEl"), TtbarRadQCDScalesTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), TtbarRadQCDScalesTheoSR2J))   
    generatorSyst.append((("ttbar","SR2JEM"), TtbarRadQCDScalesTheoSR2J))   
    generatorSyst.append((("ttbar","VR2J_1El"), TtbarRadQCDScalesTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), TtbarRadQCDScalesTheoVR2J_1))    
    generatorSyst.append((("ttbar","VR2J_2El"), TtbarRadQCDScalesTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), TtbarRadQCDScalesTheoVR2J_2))    
    generatorSyst.append((("ttbar","VR2J_1EM"), TtbarRadQCDScalesTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), TtbarRadQCDScalesTheoVR2J_2))
    
    generatorSyst.append((("ttbar","SR6JGGx12El"), TtbarRadQCDScalesTheoSR6JGGx12))
    generatorSyst.append((("ttbar","SR6JGGx12Mu"), TtbarRadQCDScalesTheoSR6JGGx12))   
    generatorSyst.append((("ttbar","SR6JGGx12EM"), TtbarRadQCDScalesTheoSR6JGGx12))
    generatorSyst.append((("ttbar","VR6JGGx12_mtEl"), TtbarRadQCDScalesTheoVR6JGGx12_mt))
    generatorSyst.append((("ttbar","VR6JGGx12_mtMu"), TtbarRadQCDScalesTheoVR6JGGx12_mt))    
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityEl"), TtbarRadQCDScalesTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityMu"), TtbarRadQCDScalesTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("ttbar","VR6JGGx12_mtEM"), TtbarRadQCDScalesTheoVR6JGGx12_mt))
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityEM"), TtbarRadQCDScalesTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("ttbar","SR6JGGx12HMEl"), TtbarRadQCDScalesTheoSR6JGGx12HM))
    generatorSyst.append((("ttbar","SR6JGGx12HMMu"), TtbarRadQCDScalesTheoSR6JGGx12HM))   
    generatorSyst.append((("ttbar","SR6JGGx12HMEM"), TtbarRadQCDScalesTheoSR6JGGx12HM))
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtEl"), TtbarRadQCDScalesTheoVR6JGGx12HM_mt))
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtMu"), TtbarRadQCDScalesTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtEM"), TtbarRadQCDScalesTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityEl"), TtbarRadQCDScalesTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityMu"), TtbarRadQCDScalesTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityEM"), TtbarRadQCDScalesTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("ttbar","SR4JhighxGGEl"), TtbarRadQCDScalesTheoSR4JhighxGG))
    generatorSyst.append((("ttbar","SR4JhighxGGMu"), TtbarRadQCDScalesTheoSR4JhighxGG))   
    generatorSyst.append((("ttbar","SR4JhighxGGEM"), TtbarRadQCDScalesTheoSR4JhighxGG))
    generatorSyst.append((("ttbar","VR4JhighxGG_mtEl"), TtbarRadQCDScalesTheoVR4JhighxGG_mt))
    generatorSyst.append((("ttbar","VR4JhighxGG_mtMu"), TtbarRadQCDScalesTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("ttbar","VR4JhighxGG_mtEM"), TtbarRadQCDScalesTheoVR4JhighxGG_mt))   
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffEl"), TtbarRadQCDScalesTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffMu"), TtbarRadQCDScalesTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffEM"), TtbarRadQCDScalesTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("ttbar","SR4JlowxGGexclEl"), TtbarRadQCDScalesTheoSR4JlowxGGexcl))
    generatorSyst.append((("ttbar","SR4JlowxGGexclMu"), TtbarRadQCDScalesTheoSR4JlowxGGexcl))   
    generatorSyst.append((("ttbar","SR4JlowxGGexclEM"), TtbarRadQCDScalesTheoSR4JlowxGGexcl))
    generatorSyst.append((("ttbar","SR4JlowxGGdiscEl"), TtbarRadQCDScalesTheoSR4JlowxGGdisc))
    generatorSyst.append((("ttbar","SR4JlowxGGdiscMu"), TtbarRadQCDScalesTheoSR4JlowxGGdisc))   
    generatorSyst.append((("ttbar","SR4JlowxGGdiscEM"), TtbarRadQCDScalesTheoSR4JlowxGGdisc))
    generatorSyst.append((("ttbar","VR4JlowxGG_mtEl"), TtbarRadQCDScalesTheoVR4JlowxGG_mt))
    generatorSyst.append((("ttbar","VR4JlowxGG_mtMu"), TtbarRadQCDScalesTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("ttbar","VR4JlowxGG_mtEM"), TtbarRadQCDScalesTheoVR4JlowxGG_mt))   
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffEl"), TtbarRadQCDScalesTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffMu"), TtbarRadQCDScalesTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffEM"), TtbarRadQCDScalesTheoVR4JlowxGG_aplanarity))
    
    generatorSyst.append((("ttbar","SR2JEl"), TtbarHadFragTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), TtbarHadFragTheoSR2J))   
    generatorSyst.append((("ttbar","SR2JEM"), TtbarHadFragTheoSR2J))
    generatorSyst.append((("ttbar","VR2J_1El"), TtbarHadFragTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), TtbarHadFragTheoVR2J_1))    
    generatorSyst.append((("ttbar","VR2J_2El"), TtbarHadFragTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), TtbarHadFragTheoVR2J_2))    
    generatorSyst.append((("ttbar","VR2J_1EM"), TtbarHadFragTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), TtbarHadFragTheoVR2J_2))
    
    generatorSyst.append((("ttbar","SR6JGGx12El"), TtbarHadFragTheoSR6JGGx12))
    generatorSyst.append((("ttbar","SR6JGGx12Mu"), TtbarHadFragTheoSR6JGGx12))   
    generatorSyst.append((("ttbar","SR6JGGx12EM"), TtbarHadFragTheoSR6JGGx12))
    generatorSyst.append((("ttbar","VR6JGGx12_mtEl"), TtbarHadFragTheoVR6JGGx12_mt))
    generatorSyst.append((("ttbar","VR6JGGx12_mtMu"), TtbarHadFragTheoVR6JGGx12_mt))    
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityEl"), TtbarHadFragTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityMu"), TtbarHadFragTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("ttbar","VR6JGGx12_mtEM"), TtbarHadFragTheoVR6JGGx12_mt))
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityEM"), TtbarHadFragTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("ttbar","SR6JGGx12HMEl"), TtbarHadFragTheoSR6JGGx12HM))
    generatorSyst.append((("ttbar","SR6JGGx12HMMu"), TtbarHadFragTheoSR6JGGx12HM))   
    generatorSyst.append((("ttbar","SR6JGGx12HMEM"), TtbarHadFragTheoSR6JGGx12HM))
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtEl"), TtbarHadFragTheoVR6JGGx12HM_mt))
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtMu"), TtbarHadFragTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtEM"), TtbarHadFragTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityEl"), TtbarHadFragTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityMu"), TtbarHadFragTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityEM"), TtbarHadFragTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("ttbar","SR4JhighxGGEl"), TtbarHadFragTheoSR4JhighxGG))
    generatorSyst.append((("ttbar","SR4JhighxGGMu"), TtbarHadFragTheoSR4JhighxGG))   
    generatorSyst.append((("ttbar","SR4JhighxGGEM"), TtbarHadFragTheoSR4JhighxGG))
    generatorSyst.append((("ttbar","VR4JhighxGG_mtEl"), TtbarHadFragTheoVR4JhighxGG_mt))
    generatorSyst.append((("ttbar","VR4JhighxGG_mtMu"), TtbarHadFragTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("ttbar","VR4JhighxGG_mtEM"), TtbarHadFragTheoVR4JhighxGG_mt))   
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffEl"), TtbarHadFragTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffMu"), TtbarHadFragTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffEM"), TtbarHadFragTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("ttbar","SR4JlowxGGexclEl"), TtbarHadFragTheoSR4JlowxGGexcl))
    generatorSyst.append((("ttbar","SR4JlowxGGexclMu"), TtbarHadFragTheoSR4JlowxGGexcl))   
    generatorSyst.append((("ttbar","SR4JlowxGGexclEM"), TtbarHadFragTheoSR4JlowxGGexcl))
    generatorSyst.append((("ttbar","SR4JlowxGGdiscEl"), TtbarHadFragTheoSR4JlowxGGdisc))
    generatorSyst.append((("ttbar","SR4JlowxGGdiscMu"), TtbarHadFragTheoSR4JlowxGGdisc))   
    generatorSyst.append((("ttbar","SR4JlowxGGdiscEM"), TtbarHadFragTheoSR4JlowxGGdisc))
    generatorSyst.append((("ttbar","VR4JlowxGG_mtEl"), TtbarHadFragTheoVR4JlowxGG_mt))
    generatorSyst.append((("ttbar","VR4JlowxGG_mtMu"), TtbarHadFragTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("ttbar","VR4JlowxGG_mtEM"), TtbarHadFragTheoVR4JlowxGG_mt))   
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffEl"), TtbarHadFragTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffMu"), TtbarHadFragTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffEM"), TtbarHadFragTheoVR4JlowxGG_aplanarity))
    
   
    generatorSyst.append((("ttbar","SR2JEl"), TtbarHardScatGenTheoSR2J))
    generatorSyst.append((("ttbar","SR2JMu"), TtbarHardScatGenTheoSR2J))   
    generatorSyst.append((("ttbar","SR2JEM"), TtbarHardScatGenTheoSR2J))
    generatorSyst.append((("ttbar","VR2J_1El"), TtbarHardScatGenTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_1Mu"), TtbarHardScatGenTheoVR2J_1))    
    generatorSyst.append((("ttbar","VR2J_2El"), TtbarHardScatGenTheoVR2J_2))
    generatorSyst.append((("ttbar","VR2J_2Mu"), TtbarHardScatGenTheoVR2J_2))    
    generatorSyst.append((("ttbar","VR2J_1EM"), TtbarHardScatGenTheoVR2J_1))
    generatorSyst.append((("ttbar","VR2J_2EM"), TtbarHardScatGenTheoVR2J_2))
    
    generatorSyst.append((("ttbar","SR6JGGx12El"), TtbarHardScatGenTheoSR6JGGx12))
    generatorSyst.append((("ttbar","SR6JGGx12Mu"), TtbarHardScatGenTheoSR6JGGx12))   
    generatorSyst.append((("ttbar","SR6JGGx12EM"), TtbarHardScatGenTheoSR6JGGx12))
    generatorSyst.append((("ttbar","VR6JGGx12_mtEl"), TtbarHardScatGenTheoVR6JGGx12_mt))
    generatorSyst.append((("ttbar","VR6JGGx12_mtMu"), TtbarHardScatGenTheoVR6JGGx12_mt))    
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityEl"), TtbarHardScatGenTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityMu"), TtbarHardScatGenTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("ttbar","VR6JGGx12_mtEM"), TtbarHardScatGenTheoVR6JGGx12_mt))
    generatorSyst.append((("ttbar","VR6JGGx12_aplanarityEM"), TtbarHardScatGenTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("ttbar","SR6JGGx12HMEl"), TtbarHardScatGenTheoSR6JGGx12HM))
    generatorSyst.append((("ttbar","SR6JGGx12HMMu"), TtbarHardScatGenTheoSR6JGGx12HM))   
    generatorSyst.append((("ttbar","SR6JGGx12HMEM"), TtbarHardScatGenTheoSR6JGGx12HM))
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtEl"), TtbarHardScatGenTheoVR6JGGx12HM_mt))
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtMu"), TtbarHardScatGenTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("ttbar","VR6JGGx12HM_mtEM"), TtbarHardScatGenTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityEl"), TtbarHardScatGenTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityMu"), TtbarHardScatGenTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("ttbar","VR6JGGx12HM_aplanarityEM"), TtbarHardScatGenTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("ttbar","SR4JhighxGGEl"), TtbarHardScatGenTheoSR4JhighxGG))
    generatorSyst.append((("ttbar","SR4JhighxGGMu"), TtbarHardScatGenTheoSR4JhighxGG))   
    generatorSyst.append((("ttbar","SR4JhighxGGEM"), TtbarHardScatGenTheoSR4JhighxGG))
    generatorSyst.append((("ttbar","VR4JhighxGG_mtEl"), TtbarHardScatGenTheoVR4JhighxGG_mt))
    generatorSyst.append((("ttbar","VR4JhighxGG_mtMu"), TtbarHardScatGenTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("ttbar","VR4JhighxGG_mtEM"), TtbarHardScatGenTheoVR4JhighxGG_mt))   
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffEl"), TtbarHardScatGenTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffMu"), TtbarHardScatGenTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("ttbar","VR4JhighxGG_metovermeffEM"), TtbarHardScatGenTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("ttbar","SR4JlowxGGexclEl"), TtbarHardScatGenTheoSR4JlowxGGexcl))
    generatorSyst.append((("ttbar","SR4JlowxGGexclMu"), TtbarHardScatGenTheoSR4JlowxGGexcl))   
    generatorSyst.append((("ttbar","SR4JlowxGGexclEM"), TtbarHardScatGenTheoSR4JlowxGGexcl))
    generatorSyst.append((("ttbar","SR4JlowxGGdiscEl"), TtbarHardScatGenTheoSR4JlowxGGdisc))
    generatorSyst.append((("ttbar","SR4JlowxGGdiscMu"), TtbarHardScatGenTheoSR4JlowxGGdisc))   
    generatorSyst.append((("ttbar","SR4JlowxGGdiscEM"), TtbarHardScatGenTheoSR4JlowxGGdisc))
    generatorSyst.append((("ttbar","VR4JlowxGG_mtEl"), TtbarHardScatGenTheoVR4JlowxGG_mt))
    generatorSyst.append((("ttbar","VR4JlowxGG_mtMu"), TtbarHardScatGenTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("ttbar","VR4JlowxGG_mtEM"), TtbarHardScatGenTheoVR4JlowxGG_mt))   
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffEl"), TtbarHardScatGenTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffMu"), TtbarHardScatGenTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("ttbar","VR4JlowxGG_metovermeffEM"), TtbarHardScatGenTheoVR4JlowxGG_aplanarity))
   
    
   
    


    generatorSyst.append((("ttbar","SR4JSSx12El"), TtbarRadQCDScalesTheoSR4JSSx12))
    generatorSyst.append((("ttbar","SR4JSSx12Mu"), TtbarRadQCDScalesTheoSR4JSSx12))   
    generatorSyst.append((("ttbar","SR4JSSx12EM"), TtbarRadQCDScalesTheoSR4JSSx12))
    generatorSyst.append((("ttbar","VR4JSSx12_mtEl"), TtbarRadQCDScalesTheoVR4JSSx12_mt))
    generatorSyst.append((("ttbar","VR4JSSx12_mtMu"), TtbarRadQCDScalesTheoVR4JSSx12_mt))    
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityEl"), TtbarRadQCDScalesTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityMu"), TtbarRadQCDScalesTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("ttbar","VR4JSSx12_mtEM"), TtbarRadQCDScalesTheoVR4JSSx12_mt))
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityEM"), TtbarRadQCDScalesTheoVR4JSSx12_aplanarity))
   
    generatorSyst.append((("ttbar","SR5JSSx12El"), TtbarRadQCDScalesTheoSR5JSSx12))
    generatorSyst.append((("ttbar","SR5JSSx12Mu"), TtbarRadQCDScalesTheoSR5JSSx12))   
    generatorSyst.append((("ttbar","SR5JSSx12EM"), TtbarRadQCDScalesTheoSR5JSSx12))
    generatorSyst.append((("ttbar","VR5JSSx12_mtEl"), TtbarRadQCDScalesTheoVR5JSSx12_mt))
    generatorSyst.append((("ttbar","VR5JSSx12_mtMu"), TtbarRadQCDScalesTheoVR5JSSx12_mt))    
    generatorSyst.append((("ttbar","VR5JSSx12_metEl"), TtbarRadQCDScalesTheoVR5JSSx12_met))
    generatorSyst.append((("ttbar","VR5JSSx12_metMu"), TtbarRadQCDScalesTheoVR5JSSx12_met))    
    generatorSyst.append((("ttbar","VR5JSSx12_mtEM"), TtbarRadQCDScalesTheoVR5JSSx12_mt))
    generatorSyst.append((("ttbar","VR5JSSx12_metEM"), TtbarRadQCDScalesTheoVR5JSSx12_met))

    generatorSyst.append((("ttbar","SR4JSSlowxEl"), TtbarRadQCDScalesTheoSR4JSSlowx))
    generatorSyst.append((("ttbar","SR4JSSlowxMu"), TtbarRadQCDScalesTheoSR4JSSlowx))   
    generatorSyst.append((("ttbar","SR4JSSlowxEM"), TtbarRadQCDScalesTheoSR4JSSlowx))
    generatorSyst.append((("ttbar","SR4JSSlowxnoBJetEM"), TtbarRadQCDScalesTheoSR4JSSlowx))   
    generatorSyst.append((("ttbar","VR4JSSlowx_mtEl"), TtbarRadQCDScalesTheoVR4JSSlowx_mt))
    generatorSyst.append((("ttbar","VR4JSSlowx_mtMu"), TtbarRadQCDScalesTheoVR4JSSlowx_mt))    
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityEl"), TtbarRadQCDScalesTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityMu"), TtbarRadQCDScalesTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("ttbar","VR4JSSlowx_mtEM"), TtbarRadQCDScalesTheoVR4JSSlowx_mt))
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityEM"), TtbarRadQCDScalesTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("ttbar","SR5JSShighxEl"), TtbarRadQCDScalesTheoSR5JSShighx))
    generatorSyst.append((("ttbar","SR5JSShighxMu"), TtbarRadQCDScalesTheoSR5JSShighx))   
    generatorSyst.append((("ttbar","SR5JSShighxEM"), TtbarRadQCDScalesTheoSR5JSShighx))
    generatorSyst.append((("ttbar","VR5JSShighx_mtEl"), TtbarRadQCDScalesTheoVR5JSShighx_mt))
    generatorSyst.append((("ttbar","VR5JSShighx_mtMu"), TtbarRadQCDScalesTheoVR5JSShighx_mt))    
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityEl"), TtbarRadQCDScalesTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityMu"), TtbarRadQCDScalesTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("ttbar","VR5JSShighx_mtEM"), TtbarRadQCDScalesTheoVR5JSShighx_mt))
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityEM"), TtbarRadQCDScalesTheoVR5JSShighx_aplanarity))
    


    generatorSyst.append((("ttbar","SR4JSSx12El"), TtbarHadFragTheoSR4JSSx12))
    generatorSyst.append((("ttbar","SR4JSSx12Mu"), TtbarHadFragTheoSR4JSSx12))   
    generatorSyst.append((("ttbar","SR4JSSx12EM"), TtbarHadFragTheoSR4JSSx12))      
    generatorSyst.append((("ttbar","VR4JSSx12_mtEl"), TtbarHadFragTheoVR4JSSx12_mt))
    generatorSyst.append((("ttbar","VR4JSSx12_mtMu"), TtbarHadFragTheoVR4JSSx12_mt))    
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityEl"), TtbarHadFragTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityMu"), TtbarHadFragTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("ttbar","VR4JSSx12_mtEM"), TtbarHadFragTheoVR4JSSx12_mt))
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityEM"), TtbarHadFragTheoVR4JSSx12_aplanarity))   
    generatorSyst.append((("ttbar","SR5JSSx12El"), TtbarHadFragTheoSR5JSSx12))
    generatorSyst.append((("ttbar","SR5JSSx12Mu"), TtbarHadFragTheoSR5JSSx12))   
    generatorSyst.append((("ttbar","SR5JSSx12EM"), TtbarHadFragTheoSR5JSSx12))
    
    generatorSyst.append((("ttbar","VR5JSSx12_mtEl"), TtbarHadFragTheoVR5JSSx12_mt))
    generatorSyst.append((("ttbar","VR5JSSx12_mtMu"), TtbarHadFragTheoVR5JSSx12_mt))    
    generatorSyst.append((("ttbar","VR5JSSx12_metEl"), TtbarHadFragTheoVR5JSSx12_met))
    generatorSyst.append((("ttbar","VR5JSSx12_metMu"), TtbarHadFragTheoVR5JSSx12_met))    
    generatorSyst.append((("ttbar","VR5JSSx12_mtEM"), TtbarHadFragTheoVR5JSSx12_mt))
    generatorSyst.append((("ttbar","VR5JSSx12_metEM"), TtbarHadFragTheoVR5JSSx12_met))

    generatorSyst.append((("ttbar","SR4JSSlowxEl"), TtbarHadFragTheoSR4JSSlowx))
    generatorSyst.append((("ttbar","SR4JSSlowxMu"), TtbarHadFragTheoSR4JSSlowx))   
    generatorSyst.append((("ttbar","SR4JSSlowxEM"), TtbarHadFragTheoSR4JSSlowx))   
    generatorSyst.append((("ttbar","SR4JSSlowxnoBJetEM"), TtbarHadFragTheoSR4JSSlowx))    
    generatorSyst.append((("ttbar","VR4JSSlowx_mtEl"), TtbarHadFragTheoVR4JSSlowx_mt))
    generatorSyst.append((("ttbar","VR4JSSlowx_mtMu"), TtbarHadFragTheoVR4JSSlowx_mt))    
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityEl"), TtbarHadFragTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityMu"), TtbarHadFragTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("ttbar","VR4JSSlowx_mtEM"), TtbarHadFragTheoVR4JSSlowx_mt))
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityEM"), TtbarHadFragTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("ttbar","SR5JSShighxEl"), TtbarHadFragTheoSR5JSShighx))
    generatorSyst.append((("ttbar","SR5JSShighxMu"), TtbarHadFragTheoSR5JSShighx))   
    generatorSyst.append((("ttbar","SR5JSShighxEM"), TtbarHadFragTheoSR5JSShighx))
    generatorSyst.append((("ttbar","VR5JSShighx_mtEl"), TtbarHadFragTheoVR5JSShighx_mt))
    generatorSyst.append((("ttbar","VR5JSShighx_mtMu"), TtbarHadFragTheoVR5JSShighx_mt))    
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityEl"), TtbarHadFragTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityMu"), TtbarHadFragTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("ttbar","VR5JSShighx_mtEM"), TtbarHadFragTheoVR5JSShighx_mt))
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityEM"), TtbarHadFragTheoVR5JSShighx_aplanarity))



    generatorSyst.append((("ttbar","SR4JSSx12El"), TtbarHardScatGenTheoSR4JSSx12))
    generatorSyst.append((("ttbar","SR4JSSx12Mu"), TtbarHardScatGenTheoSR4JSSx12))   
    generatorSyst.append((("ttbar","SR4JSSx12EM"), TtbarHardScatGenTheoSR4JSSx12))     
    generatorSyst.append((("ttbar","VR4JSSx12_mtEl"), TtbarHardScatGenTheoVR4JSSx12_mt))
    generatorSyst.append((("ttbar","VR4JSSx12_mtMu"), TtbarHardScatGenTheoVR4JSSx12_mt))    
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityEl"), TtbarHardScatGenTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityMu"), TtbarHardScatGenTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("ttbar","VR4JSSx12_mtEM"), TtbarHardScatGenTheoVR4JSSx12_mt))
    generatorSyst.append((("ttbar","VR4JSSx12_aplanarityEM"), TtbarHardScatGenTheoVR4JSSx12_aplanarity))
   
    generatorSyst.append((("ttbar","SR5JSSx12El"), TtbarHardScatGenTheoSR5JSSx12))
    generatorSyst.append((("ttbar","SR5JSSx12Mu"), TtbarHardScatGenTheoSR5JSSx12))   
    generatorSyst.append((("ttbar","SR5JSSx12EM"), TtbarHardScatGenTheoSR5JSSx12))
    generatorSyst.append((("ttbar","VR5JSSx12_mtEl"), TtbarHardScatGenTheoVR5JSSx12_mt))
    generatorSyst.append((("ttbar","VR5JSSx12_mtMu"), TtbarHardScatGenTheoVR5JSSx12_mt))    
    generatorSyst.append((("ttbar","VR5JSSx12_metEl"), TtbarHardScatGenTheoVR5JSSx12_met))
    generatorSyst.append((("ttbar","VR5JSSx12_metMu"), TtbarHardScatGenTheoVR5JSSx12_met))    
    generatorSyst.append((("ttbar","VR5JSSx12_mtEM"), TtbarHardScatGenTheoVR5JSSx12_mt))
    generatorSyst.append((("ttbar","VR5JSSx12_metEM"), TtbarHardScatGenTheoVR5JSSx12_met))

    generatorSyst.append((("ttbar","SR4JSSlowxEl"), TtbarHardScatGenTheoSR4JSSlowx))
    generatorSyst.append((("ttbar","SR4JSSlowxMu"), TtbarHardScatGenTheoSR4JSSlowx))   
    generatorSyst.append((("ttbar","SR4JSSlowxEM"), TtbarHardScatGenTheoSR4JSSlowx))  
    generatorSyst.append((("ttbar","SR4JSSlowxnoBJetEM"), TtbarHardScatGenTheoSR4JSSlowx))    
    generatorSyst.append((("ttbar","VR4JSSlowx_mtEl"), TtbarHardScatGenTheoVR4JSSlowx_mt))
    generatorSyst.append((("ttbar","VR4JSSlowx_mtMu"), TtbarHardScatGenTheoVR4JSSlowx_mt))    
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityEl"), TtbarHardScatGenTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityMu"), TtbarHardScatGenTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("ttbar","VR4JSSlowx_mtEM"), TtbarHardScatGenTheoVR4JSSlowx_mt))
    generatorSyst.append((("ttbar","VR4JSSlowx_aplanarityEM"), TtbarHardScatGenTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("ttbar","SR5JSShighxEl"), TtbarHardScatGenTheoSR5JSShighx))
    generatorSyst.append((("ttbar","SR5JSShighxMu"), TtbarHardScatGenTheoSR5JSShighx))   
    generatorSyst.append((("ttbar","SR5JSShighxEM"), TtbarHardScatGenTheoSR5JSShighx))
    generatorSyst.append((("ttbar","VR5JSShighx_mtEl"), TtbarHardScatGenTheoVR5JSShighx_mt))
    generatorSyst.append((("ttbar","VR5JSShighx_mtMu"), TtbarHardScatGenTheoVR5JSShighx_mt))    
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityEl"), TtbarHardScatGenTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityMu"), TtbarHardScatGenTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("ttbar","VR5JSShighx_mtEM"), TtbarHardScatGenTheoVR5JSShighx_mt))
    generatorSyst.append((("ttbar","VR5JSShighx_aplanarityEM"), TtbarHardScatGenTheoVR5JSShighx_aplanarity))

    generatorSyst.append((("ttbar","VR5JSShighx_DBEl"), TtbarHardScatGenTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBMu"), TtbarHardScatGenTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBEM"), TtbarHardScatGenTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBEl"), TtbarHadFragTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBMu"), TtbarHadFragTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBEM"), TtbarHadFragTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBEl"), TtbarRadQCDScalesTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBMu"), TtbarRadQCDScalesTheoVR5JSShighx_DB))
    generatorSyst.append((("ttbar","VR5JSShighx_DBEM"), TtbarRadQCDScalesTheoVR5JSShighx_DB))

    generatorSyst.append((("ttbar","VR5JSSx12_DBEl"), TtbarHardScatGenTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBMu"), TtbarHardScatGenTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBEM"), TtbarHardScatGenTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBEl"), TtbarHadFragTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBMu"), TtbarHadFragTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBEM"), TtbarHadFragTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBEl"), TtbarRadQCDScalesTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBMu"), TtbarRadQCDScalesTheoVR5JSSx12_DB))
    generatorSyst.append((("ttbar","VR5JSSx12_DBEM"), TtbarRadQCDScalesTheoVR5JSSx12_DB))

    generatorSyst.append((("ttbar","VR4JSSlowx_DBEl"), TtbarHardScatGenTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBMu"), TtbarHardScatGenTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBEM"), TtbarHardScatGenTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBEl"), TtbarHadFragTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBMu"), TtbarHadFragTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBEM"), TtbarHadFragTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBEl"), TtbarRadQCDScalesTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBMu"), TtbarRadQCDScalesTheoVR4JSSlowx_DB))
    generatorSyst.append((("ttbar","VR4JSSlowx_DBEM"), TtbarRadQCDScalesTheoVR4JSSlowx_DB))

    generatorSyst.append((("ttbar","VR4JSSx12_DBEl"), TtbarHardScatGenTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBMu"), TtbarHardScatGenTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBEM"), TtbarHardScatGenTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBEl"), TtbarHadFragTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBMu"), TtbarHadFragTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBEM"), TtbarHadFragTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBEl"), TtbarRadQCDScalesTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBMu"), TtbarRadQCDScalesTheoVR4JSSx12_DB))
    generatorSyst.append((("ttbar","VR4JSSx12_DBEM"), TtbarRadQCDScalesTheoVR4JSSx12_DB))








    
   

  
    return generatorSyst
