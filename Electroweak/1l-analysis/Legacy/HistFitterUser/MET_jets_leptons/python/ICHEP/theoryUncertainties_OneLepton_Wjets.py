import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


wjets_Sherpa22GenTheoSR2J = Systematic("wjets_Sherpa22GenTheo_SR2J",configMgr.weights,1.+0.14,1.-0.14 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR2J_1 = Systematic("wjets_Sherpa22GenTheo_VR2J_1",configMgr.weights,1.+0.14,1.-0.14, "user","userOverallSys")
wjets_Sherpa22GenTheoVR2J_2 = Systematic("wjets_Sherpa22GenTheo_VR2J_2",configMgr.weights,1.+0.11,1.-0.11, "user","userOverallSys")

wjets_Sherpa22GenTheoSR6JGGx12 = Systematic("wjets_Sherpa22GenTheo_SR6JGGx12", configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR6JGGx12_mt = Systematic("wjets_Sherpa22GenTheo_VR6JGGx12_mt",configMgr.weights,1.+0.50,1.-0.50, "user","userOverallSys")
wjets_Sherpa22GenTheoVR6JGGx12_aplanarity = Systematic("wjets_Sherpa22GenTheo_VR6JGGx12_aplanarity",configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")


wjets_Sherpa22GenTheoSR6JGGx12HM = Systematic("wjets_Sherpa22GenTheo_SR6JGGx12HM", configMgr.weights,1.+0.39,1.-0.39 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR6JGGx12HM_mt = Systematic("wjets_Sherpa22GenTheo_VR6JGGx12HM_mt",configMgr.weights,1.+0.39,1.-0.39 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR6JGGx12HM_aplanarity = Systematic("wjets_Sherpa22GenTheo_VR6JGGx12HM_aplanarity",configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")


wjets_Sherpa22GenTheoSR4JhighxGG = Systematic("wjets_Sherpa22GenTheo_SR4JhighxGG", configMgr.weights,1.+0.23,1.-0.23 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JhighxGG_mt = Systematic("wjets_Sherpa22GenTheo_VR4JhighxGG_mt",configMgr.weights,1.+0.09,1.-0.09, "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JhighxGG_metovermeff = Systematic("wjets_Sherpa22GenTheo_VR4JhighxGG_aplanarity",configMgr.weights,1.+0.23,1.-0.23, "user","userOverallSys")


wjets_Sherpa22GenTheoSR4JlowxGGexcl = Systematic("wjets_Sherpa22GenTheo_SR4JlowxGGexcl", configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
wjets_Sherpa22GenTheoSR4JlowxGGdisc = Systematic("wjets_Sherpa22GenTheo_SR4JlowxGGdisc", configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JlowxGG_mt = Systematic("wjets_Sherpa22GenTheo_VR4JlowxGG_mt",configMgr.weights,1.+0.30,1.-0.30, "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JlowxGG_aplanarity = Systematic("wjets_Sherpa22GenTheo_VR4JlowxGG_aplanarity",configMgr.weights,1.+0.26,1.-0.26, "user","userOverallSys")








wjets_Sherpa22ResummationTheoSR2J = Systematic("wjets_Sherpa22ResummationTheo_SR2J", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR2J_1 = Systematic("wjets_Sherpa22ResummationTheo_VR2J_1",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR2J_2 = Systematic("wjets_Sherpa22ResummationTheo_VR2J_2",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22ResummationTheoSR6JGGx12 = Systematic("wjets_Sherpa22ResummationTheo_SR6JGGx12", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR6JGGx12_mt = Systematic("wjets_Sherpa22ResummationTheo_VR6JGGx12_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR6JGGx12_aplanarity = Systematic("wjets_Sherpa22ResummationTheo_VR6JGGx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")


wjets_Sherpa22ResummationTheoSR6JGGx12HM = Systematic("wjets_Sherpa22ResummationTheo_SR6JGGx12HM", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR6JGGx12HM_mt = Systematic("wjets_Sherpa22ResummationTheo_VR6JGGx12HM_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR6JGGx12HM_aplanarity = Systematic("wjets_Sherpa22ResummationTheo_VR6JGGx12HM_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")


wjets_Sherpa22ResummationTheoSR4JhighxGG = Systematic("wjets_Sherpa22ResummationTheo_SR4JhighxGG", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JhighxGG_mt = Systematic("wjets_Sherpa22ResummationTheo_VR4JhighxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JhighxGG_metovermeff = Systematic("wjets_Sherpa22ResummationTheo_VR4JhighxGG_metovermeff",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")


wjets_Sherpa22ResummationTheoSR4JlowxGGexcl = Systematic("wjets_Sherpa22ResummationTheo_SR4JlowxGGdisc", configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22ResummationTheoSR4JlowxGGdisc = Systematic("wjets_Sherpa22ResummationTheo_SR4JlowxGGexcl", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JlowxGG_mt = Systematic("wjets_Sherpa22ResummationTheo_VR4JlowxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JlowxGG_aplanarity = Systematic("wjets_Sherpa22ResummationTheo_VR4JlowxGG_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")





wjets_Sherpa22RenormTheoSR2J = Systematic("wjets_Sherpa22RenormTheo_SR2J", configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR2J_1 = Systematic("wjets_Sherpa22RenormTheo_VR2J_1",configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR2J_2 = Systematic("wjets_Sherpa22RenormTheo_VR2J_2",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22RenormTheoSR6JGGx12 = Systematic("wjets_Sherpa22RenormTheo_SR6JGGx12", configMgr.weights,1.+0.03,1.-0.03, "user","userOverallSys")
wjets_Sherpa22RenormTheoVR6JGGx12_mt = Systematic("wjets_Sherpa22RenormTheo_VR6JGGx12_mt",configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR6JGGx12_aplanarity = Systematic("wjets_Sherpa22RenormTheo_VR6JGGx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")


wjets_Sherpa22RenormTheoSR6JGGx12HM = Systematic("wjets_Sherpa22RenormTheo_SR6JGGx12HM", configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR6JGGx12HM_mt = Systematic("wjets_Sherpa22RenormTheo_VR6JGGx12HM_mt",configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR6JGGx12HM_aplanarity = Systematic("wjets_Sherpa22RenormTheo_VR6JGGx12HM_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")


wjets_Sherpa22RenormTheoSR4JhighxGG = Systematic("wjets_Sherpa22RenormTheo_SR4JhighxGG", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JhighxGG_mt = Systematic("wjets_Sherpa22RenormTheo_VR4JhighxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JhighxGG_metovermeff = Systematic("wjets_Sherpa22RenormTheo_VR4JhighxGG_metovermeff",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")


wjets_Sherpa22RenormTheoSR4JlowxGGexcl = Systematic("wjets_Sherpa22RenormTheo_SR4JlowxGGdisc", configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22RenormTheoSR4JlowxGGdisc = Systematic("wjets_Sherpa22RenormTheo_SR4JlowxGGexcl", configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JlowxGG_mt = Systematic("wjets_Sherpa22RenormTheo_VR4JlowxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JlowxGG_aplanarity = Systematic("wjets_Sherpa22RenormTheo_VR4JlowxGG_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")





wjets_Sherpa22FacTheoSR2J = Systematic("wjets_Sherpa22FacTheo_SR2J", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR2J_1 = Systematic("wjets_Sherpa22FacTheo_VR2J_1",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR2J_2 = Systematic("wjets_Sherpa22FacTheo_VR2J_2",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22FacTheoSR6JGGx12 = Systematic("wjets_Sherpa22FacTheo_SR6JGGx12", configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR6JGGx12_mt = Systematic("wjets_Sherpa22FacTheo_VR6JGGx12_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR6JGGx12_aplanarity = Systematic("wjets_Sherpa22FacTheo_VR6JGGx12_aplanarity",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")


wjets_Sherpa22FacTheoSR6JGGx12HM = Systematic("wjets_Sherpa22FacTheo_SR6JGGx12HM", configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR6JGGx12HM_mt = Systematic("wjets_Sherpa22FacTheo_VR6JGGx12HM_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR6JGGx12HM_aplanarity = Systematic("wjets_Sherpa22FacTheo_VR6JGGx12HM_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")


wjets_Sherpa22FacTheoSR4JhighxGG = Systematic("wjets_Sherpa22FacTheo_SR4JhighxGG", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JhighxGG_mt = Systematic("wjets_Sherpa22FacTheo_VR4JhighxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JhighxGG_metovermeff = Systematic("wjets_Sherpa22FacTheo_VR4JhighxGG_metovermeff",configMgr.weights,1.+0.02,1.-0.02, "user","userOverallSys")


wjets_Sherpa22FacTheoSR4JlowxGGexcl = Systematic("wjets_Sherpa22FacTheo_SR4JlowxGGdisc", configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22FacTheoSR4JlowxGGdisc = Systematic("wjets_Sherpa22FacTheo_SR4JlowxGGexcl", configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JlowxGG_mt = Systematic("wjets_Sherpa22FacTheo_VR4JlowxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JlowxGG_aplanarity = Systematic("wjets_Sherpa22FacTheo_VR4JlowxGG_aplanarity",configMgr.weights,1.+0.03,1.-0.03, "user","userOverallSys")





wjets_Sherpa22CKKWTheoSR2J = Systematic("wjets_Sherpa22CKKWTheo_SR2J", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR2J_1 = Systematic("wjets_Sherpa22CKKWTheo_VR2J_1",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR2J_2 = Systematic("wjets_Sherpa22CKKWTheo_VR2J_2",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22CKKWTheoSR6JGGx12 = Systematic("wjets_Sherpa22CKKWTheo_SR6JGGx12", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR6JGGx12_mt = Systematic("wjets_Sherpa22CKKWTheo_VR6JGGx12_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR6JGGx12_aplanarity = Systematic("wjets_Sherpa22CKKWTheo_VR6JGGx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")


wjets_Sherpa22CKKWTheoSR6JGGx12HM = Systematic("wjets_Sherpa22CKKWTheo_SR6JGGx12HM", configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR6JGGx12HM_mt = Systematic("wjets_Sherpa22CKKWTheo_VR6JGGx12HM_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR6JGGx12HM_aplanarity = Systematic("wjets_Sherpa22CKKWTheo_VR6JGGx12HM_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")


wjets_Sherpa22CKKWTheoSR4JhighxGG = Systematic("wjets_Sherpa22CKKWTheo_SR4JhighxGG", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JhighxGG_mt = Systematic("wjets_Sherpa22CKKWTheo_VR4JhighxGG_mt",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JhighxGG_metovermeff = Systematic("wjets_Sherpa22CKKWTheo_VR4JhighxGG_metovermeff",configMgr.weights,1.+0.02,1.-0.02, "user","userOverallSys")


wjets_Sherpa22CKKWTheoSR4JlowxGGexcl = Systematic("wjets_Sherpa22CKKWTheo_SR4JlowxGGdisc", configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoSR4JlowxGGdisc = Systematic("wjets_Sherpa22CKKWTheo_SR4JlowxGGexcl", configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JlowxGG_mt = Systematic("wjets_Sherpa22CKKWTheo_VR4JlowxGG_mt",configMgr.weights,1.+0.02,1.-0.02, "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JlowxGG_aplanarity = Systematic("wjets_Sherpa22CKKWTheo_VR4JlowxGG_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")






#squarks
wjets_Sherpa22GenTheoSR4JSSx12 = Systematic("wjets_Sherpa22GenTheo_SR4JSSx12", configMgr.weights,1.+0.17,1.-0.06, "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JSSx12_mt = Systematic("wjets_Sherpa22GenTheo_VR4JSSx12_mt",configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JSSx12_aplanarity = Systematic("wjets_Sherpa22GenTheo_VR4JSSx12_aplanarity",configMgr.weights,1.+0.17,1.-0.17 , "user","userOverallSys")

wjets_Sherpa22GenTheoSR5JSSx12 = Systematic("wjets_Sherpa22GenTheo_SR5JSSx12", configMgr.weights,1.+0.16,1.-0.16 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR5JSSx12_mt = Systematic("wjets_Sherpa22GenTheo_VR5JSSx12_mt",configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR5JSSx12_met = Systematic("wjets_Sherpa22GenTheo_VR5JSSx12_met",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")

wjets_Sherpa22GenTheoSR4JSSlowx = Systematic("wjets_Sherpa22GenTheo_SR4JSSlowx", configMgr.weights,1.+0.17,1.-0.17 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JSSlowx_mt = Systematic("wjets_Sherpa22GenTheo_VR4JSSlowx_mt",configMgr.weights,1.+0.07,1.-0.07 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JSSlowx_aplanarity = Systematic("wjets_Sherpa22GenTheo_VR4JSSlowx_aplanarity",configMgr.weights,1.+0.17,1.-0.17, "user","userOverallSys")

wjets_Sherpa22GenTheoSR5JSShighx = Systematic("wjets_Sherpa22GenTheo_SR5JSShighx", configMgr.weights,1.+0.17,1.-0.17 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR5JSShighx_mt = Systematic("wjets_Sherpa22GenTheo_VR5JSShighx_mt",configMgr.weights,1.+0.17,1.-0.17 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR5JSShighx_aplanarity = Systematic("wjets_Sherpa22GenTheo_VR5JSShighx_aplanarity",configMgr.weights,1.+0.10,1.-0.10 , "user","userOverallSys")


wjets_Sherpa22GenTheoVR4JSSx12_DB = Systematic("wjets_Sherpa22GenTheo_VR4JSSx12_DB",configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR5JSSx12_DB = Systematic("wjets_Sherpa22GenTheo_VR5JSSx12_DB",configMgr.weights,1.+0.19,1.-0.19 , "user","userOverallSys")
wjets_Sherpa22GenTheoVR4JSSlowx_DB = Systematic("wjets_Sherpa22GenTheo_VR4JSSlowx_DB",configMgr.weights,1.+0.06,1.-0.06, "user","userOverallSys")
wjets_Sherpa22GenTheoVR5JSShighx_DB = Systematic("wjets_Sherpa22GenTheo_VR5JSShighx_DB",configMgr.weights,1.+0.17,1.-0.17, "user","userOverallSys")




wjets_Sherpa22ResummationTheoSR4JSSx12 = Systematic("wjets_Sherpa22ResummationTheo_SR4JSSx12", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JSSx12_mt = Systematic("wjets_Sherpa22ResummationTheo_VR4JSSx12_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JSSx12_aplanarity = Systematic("wjets_Sherpa22ResummationTheo_VR4JSSx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22ResummationTheoSR5JSSx12 = Systematic("wjets_Sherpa22ResummationTheo_SR5JSSx12", configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR5JSSx12_mt = Systematic("wjets_Sherpa22ResummationTheo_VR5JSSx12_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR5JSSx12_met = Systematic("wjets_Sherpa22ResummationTheo_VR5JSSx12_met",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22ResummationTheoSR4JSSlowx = Systematic("wjets_Sherpa22ResummationTheo_SR4JSSlowx", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JSSlowx_mt = Systematic("wjets_Sherpa22ResummationTheo_VR4JSSlowx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JSSlowx_aplanarity = Systematic("wjets_Sherpa22ResummationTheo_VR4JSSlowx_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")

wjets_Sherpa22ResummationTheoSR5JSShighx = Systematic("wjets_Sherpa22ResummationTheo_SR5JSShighx", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR5JSShighx_mt = Systematic("wjets_Sherpa22ResummationTheo_VR5JSShighx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR5JSShighx_aplanarity = Systematic("wjets_Sherpa22ResummationTheo_VR5JSShighx_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22ResummationTheoVR4JSSx12_DB = Systematic("wjets_Sherpa22ResummationTheo_VR4JSSx12_DB",configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR5JSSx12_DB = Systematic("wjets_Sherpa22ResummationTheo_VR5JSSx12_DB",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR4JSSlowx_DB = Systematic("wjets_Sherpa22ResummationTheo_VR4JSSlowx_DB",configMgr.weights,1.+0.03,1.-0.03, "user","userOverallSys")
wjets_Sherpa22ResummationTheoVR5JSShighx_DB = Systematic("wjets_Sherpa22ResummationTheo_VR5JSShighx_DB",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")



wjets_Sherpa22RenormTheoSR4JSSx12 = Systematic("wjets_Sherpa22RenormTheo_SR4JSSx12", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JSSx12_mt = Systematic("wjets_Sherpa22RenormTheo_VR4JSSx12_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JSSx12_aplanarity = Systematic("wjets_Sherpa22RenormTheo_VR4JSSx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")


wjets_Sherpa22RenormTheoSR5JSSx12 = Systematic("wjets_Sherpa22RenormTheo_SR5JSSx12", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR5JSSx12_mt = Systematic("wjets_Sherpa22RenormTheo_VR5JSSx12_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR5JSSx12_met = Systematic("wjets_Sherpa22RenormTheo_VR5JSSx12_met",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")


wjets_Sherpa22RenormTheoSR4JSSlowx = Systematic("wjets_Sherpa22RenormTheo_SR4JSSlowx", configMgr.weights,1.+0.01 ,1.-0.01  , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JSSlowx_mt = Systematic("wjets_Sherpa22RenormTheo_VR4JSSlowx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JSSlowx_aplanarity = Systematic("wjets_Sherpa22RenormTheo_VR4JSSlowx_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")

wjets_Sherpa22RenormTheoSR5JSShighx = Systematic("wjets_Sherpa22RenormTheo_SR5JSShighx", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR5JSShighx_mt = Systematic("wjets_Sherpa22RenormTheo_VR5JSShighx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR5JSShighx_aplanarity = Systematic("wjets_Sherpa22RenormTheo_VR5JSShighx_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22RenormTheoVR4JSSx12_DB = Systematic("wjets_Sherpa22RenormTheo_VR4JSSx12_DB",configMgr.weights,1.+0.07,1.-0.07 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR5JSSx12_DB = Systematic("wjets_Sherpa22RenormTheo_VR5JSSx12_DB",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22RenormTheoVR4JSSlowx_DB = Systematic("wjets_Sherpa22RenormTheo_VR4JSSlowx_DB",configMgr.weights,1.+0.06,1.-0.06, "user","userOverallSys")
wjets_Sherpa22RenormTheoVR5JSShighx_DB = Systematic("wjets_Sherpa22RenormTheo_VR5JSShighx_DB",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")



wjets_Sherpa22FacTheoSR4JSSx12 = Systematic("wjets_Sherpa22FacTheo_SR4JSSx12", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JSSx12_mt = Systematic("wjets_Sherpa22FacTheo_VR4JSSx12_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JSSx12_aplanarity = Systematic("wjets_Sherpa22FacTheo_VR4JSSx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22FacTheoSR5JSSx12 = Systematic("wjets_Sherpa22FacTheo_SR5JSSx12", configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR5JSSx12_mt = Systematic("wjets_Sherpa22FacTheo_VR5JSSx12_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR5JSSx12_met = Systematic("wjets_Sherpa22FacTheo_VR5JSSx12_met",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")

wjets_Sherpa22FacTheoSR4JSSlowx = Systematic("wjets_Sherpa22FacTheo_SR4JSSlowx", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JSSlowx_mt = Systematic("wjets_Sherpa22FacTheo_VR4JSSlowx_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JSSlowx_aplanarity = Systematic("wjets_Sherpa22FacTheo_VR4JSSlowx_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")

wjets_Sherpa22FacTheoSR5JSShighx = Systematic("wjets_Sherpa22FacTheo_SR5JSShighx", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR5JSShighx_mt = Systematic("wjets_Sherpa22FacTheo_VR5JSShighx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR5JSShighx_aplanarity = Systematic("wjets_Sherpa22FacTheo_VR5JSShighx_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22FacTheoVR4JSSx12_DB = Systematic("wjets_Sherpa22FacTheo_VR4JSSx12_DB",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR5JSSx12_DB = Systematic("wjets_Sherpa22FacTheo_VR5JSSx12_DB",configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
wjets_Sherpa22FacTheoVR4JSSlowx_DB = Systematic("wjets_Sherpa22FacTheo_VR4JSSlowx_DB",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")
wjets_Sherpa22FacTheoVR5JSShighx_DB = Systematic("wjets_Sherpa22FacTheo_VR5JSShighx_DB",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")



wjets_Sherpa22CKKWTheoSR4JSSx12 = Systematic("wjets_Sherpa22CKKWTheo_SR4JSSx12", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JSSx12_mt = Systematic("wjets_Sherpa22CKKWTheo_VR4JSSx12_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JSSx12_aplanarity = Systematic("wjets_Sherpa22CKKWTheo_VR4JSSx12_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22CKKWTheoSR5JSSx12 = Systematic("wjets_Sherpa22CKKWTheo_SR5JSSx12", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR5JSSx12_mt = Systematic("wjets_Sherpa22CKKWTheo_VR5JSSx12_mt",configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR5JSSx12_met = Systematic("wjets_Sherpa22CKKWTheo_VR5JSSx12_met",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22CKKWTheoSR4JSSlowx = Systematic("wjets_Sherpa22CKKWTheo_SR4JSSlowx", configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JSSlowx_mt = Systematic("wjets_Sherpa22CKKWTheo_VR4JSSlowx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JSSlowx_aplanarity = Systematic("wjets_Sherpa22CKKWTheo_VR4JSSlowx_aplanarity",configMgr.weights,1.+0.01,1.-0.01, "user","userOverallSys")

wjets_Sherpa22CKKWTheoSR5JSShighx = Systematic("wjets_Sherpa22CKKWTheo_SR5JSShighx", configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR5JSShighx_mt = Systematic("wjets_Sherpa22CKKWTheo_VR5JSShighx_mt",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR5JSShighx_aplanarity = Systematic("wjets_Sherpa22CKKWTheo_VR5JSShighx_aplanarity",configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

wjets_Sherpa22CKKWTheoVR4JSSx12_DB = Systematic("wjets_Sherpa22CKKWTheo_VR4JSSx12_DB",configMgr.weights,1.+0.09,1.-0.09 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR5JSSx12_DB = Systematic("wjets_Sherpa22CKKWTheo_VR5JSSx12_DB",configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR4JSSlowx_DB = Systematic("wjets_Sherpa22CKKWTheo_VR4JSSlowx_DB",configMgr.weights,1.+0.08,1.-0.08, "user","userOverallSys")
wjets_Sherpa22CKKWTheoVR5JSShighx_DB = Systematic("wjets_Sherpa22CKKWTheo_VR5JSShighx_DB",configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")



def TheorUnc(generatorSyst):
   
    generatorSyst.append((("wjets_Sherpa221","SR2JEl"), wjets_Sherpa22GenTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","SR2JMu"), wjets_Sherpa22GenTheoSR2J))   
    generatorSyst.append((("wjets_Sherpa221","SR2JEM"), wjets_Sherpa22GenTheoSR2J))   
    generatorSyst.append((("wjets_Sherpa221","VR2J_1El"), wjets_Sherpa22GenTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1Mu"), wjets_Sherpa22GenTheoVR2J_1))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_2El"), wjets_Sherpa22GenTheoVR2J_2))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2Mu"), wjets_Sherpa22GenTheoVR2J_2))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_1EM"), wjets_Sherpa22GenTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2EM"), wjets_Sherpa22GenTheoVR2J_2))
    
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12El"), wjets_Sherpa22GenTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12Mu"), wjets_Sherpa22GenTheoSR6JGGx12))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12EM"), wjets_Sherpa22GenTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEl"), wjets_Sherpa22GenTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtMu"), wjets_Sherpa22GenTheoVR6JGGx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEl"), wjets_Sherpa22GenTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityMu"), wjets_Sherpa22GenTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEM"), wjets_Sherpa22GenTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEM"), wjets_Sherpa22GenTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEl"), wjets_Sherpa22GenTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMMu"), wjets_Sherpa22GenTheoSR6JGGx12HM))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEM"), wjets_Sherpa22GenTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEl"), wjets_Sherpa22GenTheoVR6JGGx12HM_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtMu"), wjets_Sherpa22GenTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEM"), wjets_Sherpa22GenTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEl"), wjets_Sherpa22GenTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityMu"), wjets_Sherpa22GenTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEM"), wjets_Sherpa22GenTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEl"), wjets_Sherpa22GenTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGMu"), wjets_Sherpa22GenTheoSR4JhighxGG))   
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEM"), wjets_Sherpa22GenTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEl"), wjets_Sherpa22GenTheoVR4JhighxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtMu"), wjets_Sherpa22GenTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEM"), wjets_Sherpa22GenTheoVR4JhighxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEl"), wjets_Sherpa22GenTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffMu"), wjets_Sherpa22GenTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEM"), wjets_Sherpa22GenTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEl"), wjets_Sherpa22GenTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclMu"), wjets_Sherpa22GenTheoSR4JlowxGGexcl))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEM"), wjets_Sherpa22GenTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEl"), wjets_Sherpa22GenTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscMu"), wjets_Sherpa22GenTheoSR4JlowxGGdisc))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEM"), wjets_Sherpa22GenTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEl"), wjets_Sherpa22GenTheoVR4JlowxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtMu"), wjets_Sherpa22GenTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEM"), wjets_Sherpa22GenTheoVR4JlowxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEl"), wjets_Sherpa22GenTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffMu"), wjets_Sherpa22GenTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEM"), wjets_Sherpa22GenTheoVR4JlowxGG_aplanarity))
    
    generatorSyst.append((("wjets_Sherpa221","SR2JEl"), wjets_Sherpa22ResummationTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","SR2JMu"), wjets_Sherpa22ResummationTheoSR2J))   
    generatorSyst.append((("wjets_Sherpa221","SR2JEM"), wjets_Sherpa22ResummationTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1El"), wjets_Sherpa22ResummationTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1Mu"), wjets_Sherpa22ResummationTheoVR2J_1))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_2El"), wjets_Sherpa22ResummationTheoVR2J_2))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2Mu"), wjets_Sherpa22ResummationTheoVR2J_2))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_1EM"), wjets_Sherpa22ResummationTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2EM"), wjets_Sherpa22ResummationTheoVR2J_2))
    
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12El"), wjets_Sherpa22ResummationTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12Mu"), wjets_Sherpa22ResummationTheoSR6JGGx12))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12EM"), wjets_Sherpa22ResummationTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEl"), wjets_Sherpa22ResummationTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtMu"), wjets_Sherpa22ResummationTheoVR6JGGx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEl"), wjets_Sherpa22ResummationTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityMu"), wjets_Sherpa22ResummationTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEM"), wjets_Sherpa22ResummationTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEM"), wjets_Sherpa22ResummationTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEl"), wjets_Sherpa22ResummationTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMMu"), wjets_Sherpa22ResummationTheoSR6JGGx12HM))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEM"), wjets_Sherpa22ResummationTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEl"), wjets_Sherpa22ResummationTheoVR6JGGx12HM_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtMu"), wjets_Sherpa22ResummationTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEM"), wjets_Sherpa22ResummationTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEl"), wjets_Sherpa22ResummationTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityMu"), wjets_Sherpa22ResummationTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEM"), wjets_Sherpa22ResummationTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEl"), wjets_Sherpa22ResummationTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGMu"), wjets_Sherpa22ResummationTheoSR4JhighxGG))   
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEM"), wjets_Sherpa22ResummationTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEl"), wjets_Sherpa22ResummationTheoVR4JhighxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtMu"), wjets_Sherpa22ResummationTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEM"), wjets_Sherpa22ResummationTheoVR4JhighxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEl"), wjets_Sherpa22ResummationTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffMu"), wjets_Sherpa22ResummationTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEM"), wjets_Sherpa22ResummationTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEl"), wjets_Sherpa22ResummationTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclMu"), wjets_Sherpa22ResummationTheoSR4JlowxGGexcl))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEM"), wjets_Sherpa22ResummationTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEl"), wjets_Sherpa22ResummationTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscMu"), wjets_Sherpa22ResummationTheoSR4JlowxGGdisc))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEM"), wjets_Sherpa22ResummationTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEl"), wjets_Sherpa22ResummationTheoVR4JlowxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtMu"), wjets_Sherpa22ResummationTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEM"), wjets_Sherpa22ResummationTheoVR4JlowxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEl"), wjets_Sherpa22ResummationTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffMu"), wjets_Sherpa22ResummationTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEM"), wjets_Sherpa22ResummationTheoVR4JlowxGG_aplanarity))
    
   
    generatorSyst.append((("wjets_Sherpa221","SR2JEl"), wjets_Sherpa22RenormTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","SR2JMu"), wjets_Sherpa22RenormTheoSR2J))   
    generatorSyst.append((("wjets_Sherpa221","SR2JEM"), wjets_Sherpa22RenormTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1El"), wjets_Sherpa22RenormTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1Mu"), wjets_Sherpa22RenormTheoVR2J_1))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_2El"), wjets_Sherpa22RenormTheoVR2J_2))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2Mu"), wjets_Sherpa22RenormTheoVR2J_2))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_1EM"), wjets_Sherpa22RenormTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2EM"), wjets_Sherpa22RenormTheoVR2J_2))
    
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12El"), wjets_Sherpa22RenormTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12Mu"), wjets_Sherpa22RenormTheoSR6JGGx12))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12EM"), wjets_Sherpa22RenormTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEl"), wjets_Sherpa22RenormTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtMu"), wjets_Sherpa22RenormTheoVR6JGGx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEl"), wjets_Sherpa22RenormTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityMu"), wjets_Sherpa22RenormTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEM"), wjets_Sherpa22RenormTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEM"), wjets_Sherpa22RenormTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEl"), wjets_Sherpa22RenormTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMMu"), wjets_Sherpa22RenormTheoSR6JGGx12HM))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEM"), wjets_Sherpa22RenormTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEl"), wjets_Sherpa22RenormTheoVR6JGGx12HM_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtMu"), wjets_Sherpa22RenormTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEM"), wjets_Sherpa22RenormTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEl"), wjets_Sherpa22RenormTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityMu"), wjets_Sherpa22RenormTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEM"), wjets_Sherpa22RenormTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEl"), wjets_Sherpa22RenormTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGMu"), wjets_Sherpa22RenormTheoSR4JhighxGG))   
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEM"), wjets_Sherpa22RenormTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEl"), wjets_Sherpa22RenormTheoVR4JhighxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtMu"), wjets_Sherpa22RenormTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEM"), wjets_Sherpa22RenormTheoVR4JhighxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEl"), wjets_Sherpa22RenormTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffMu"), wjets_Sherpa22RenormTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEM"), wjets_Sherpa22RenormTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEl"), wjets_Sherpa22RenormTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclMu"), wjets_Sherpa22RenormTheoSR4JlowxGGexcl))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEM"), wjets_Sherpa22RenormTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEl"), wjets_Sherpa22RenormTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscMu"), wjets_Sherpa22RenormTheoSR4JlowxGGdisc))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEM"), wjets_Sherpa22RenormTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEl"), wjets_Sherpa22RenormTheoVR4JlowxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtMu"), wjets_Sherpa22RenormTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEM"), wjets_Sherpa22RenormTheoVR4JlowxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEl"), wjets_Sherpa22RenormTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffMu"), wjets_Sherpa22RenormTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEM"), wjets_Sherpa22RenormTheoVR4JlowxGG_aplanarity))
   
    
    generatorSyst.append((("wjets_Sherpa221","SR2JEl"), wjets_Sherpa22FacTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","SR2JMu"), wjets_Sherpa22FacTheoSR2J))   
    generatorSyst.append((("wjets_Sherpa221","SR2JEM"), wjets_Sherpa22FacTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1El"), wjets_Sherpa22FacTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1Mu"), wjets_Sherpa22FacTheoVR2J_1))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_2El"), wjets_Sherpa22FacTheoVR2J_2))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2Mu"), wjets_Sherpa22FacTheoVR2J_2))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_1EM"), wjets_Sherpa22FacTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2EM"), wjets_Sherpa22FacTheoVR2J_2))
    
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12El"), wjets_Sherpa22FacTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12Mu"), wjets_Sherpa22FacTheoSR6JGGx12))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12EM"), wjets_Sherpa22FacTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEl"), wjets_Sherpa22FacTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtMu"), wjets_Sherpa22FacTheoVR6JGGx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEl"), wjets_Sherpa22FacTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityMu"), wjets_Sherpa22FacTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEM"), wjets_Sherpa22FacTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEM"), wjets_Sherpa22FacTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEl"), wjets_Sherpa22FacTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMMu"), wjets_Sherpa22FacTheoSR6JGGx12HM))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEM"), wjets_Sherpa22FacTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEl"), wjets_Sherpa22FacTheoVR6JGGx12HM_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtMu"), wjets_Sherpa22FacTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEM"), wjets_Sherpa22FacTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEl"), wjets_Sherpa22FacTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityMu"), wjets_Sherpa22FacTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEM"), wjets_Sherpa22FacTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEl"), wjets_Sherpa22FacTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGMu"), wjets_Sherpa22FacTheoSR4JhighxGG))   
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEM"), wjets_Sherpa22FacTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEl"), wjets_Sherpa22FacTheoVR4JhighxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtMu"), wjets_Sherpa22FacTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEM"), wjets_Sherpa22FacTheoVR4JhighxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEl"), wjets_Sherpa22FacTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffMu"), wjets_Sherpa22FacTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEM"), wjets_Sherpa22FacTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEl"), wjets_Sherpa22FacTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclMu"), wjets_Sherpa22FacTheoSR4JlowxGGexcl))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEM"), wjets_Sherpa22FacTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEl"), wjets_Sherpa22FacTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscMu"), wjets_Sherpa22FacTheoSR4JlowxGGdisc))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEM"), wjets_Sherpa22FacTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEl"), wjets_Sherpa22FacTheoVR4JlowxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtMu"), wjets_Sherpa22FacTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEM"), wjets_Sherpa22FacTheoVR4JlowxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEl"), wjets_Sherpa22FacTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffMu"), wjets_Sherpa22FacTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEM"), wjets_Sherpa22FacTheoVR4JlowxGG_aplanarity))
    
    
    
    
    
    generatorSyst.append((("wjets_Sherpa221","SR2JEl"), wjets_Sherpa22CKKWTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","SR2JMu"), wjets_Sherpa22CKKWTheoSR2J))   
    generatorSyst.append((("wjets_Sherpa221","SR2JEM"), wjets_Sherpa22CKKWTheoSR2J))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1El"), wjets_Sherpa22CKKWTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_1Mu"), wjets_Sherpa22CKKWTheoVR2J_1))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_2El"), wjets_Sherpa22CKKWTheoVR2J_2))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2Mu"), wjets_Sherpa22CKKWTheoVR2J_2))    
    generatorSyst.append((("wjets_Sherpa221","VR2J_1EM"), wjets_Sherpa22CKKWTheoVR2J_1))
    generatorSyst.append((("wjets_Sherpa221","VR2J_2EM"), wjets_Sherpa22CKKWTheoVR2J_2))
    
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12El"), wjets_Sherpa22CKKWTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12Mu"), wjets_Sherpa22CKKWTheoSR6JGGx12))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12EM"), wjets_Sherpa22CKKWTheoSR6JGGx12))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEl"), wjets_Sherpa22CKKWTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtMu"), wjets_Sherpa22CKKWTheoVR6JGGx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEl"), wjets_Sherpa22CKKWTheoVR6JGGx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityMu"), wjets_Sherpa22CKKWTheoVR6JGGx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_mtEM"), wjets_Sherpa22CKKWTheoVR6JGGx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12_aplanarityEM"), wjets_Sherpa22CKKWTheoVR6JGGx12_aplanarity))

    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEl"), wjets_Sherpa22CKKWTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMMu"), wjets_Sherpa22CKKWTheoSR6JGGx12HM))   
    generatorSyst.append((("wjets_Sherpa221","SR6JGGx12HMEM"), wjets_Sherpa22CKKWTheoSR6JGGx12HM))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEl"), wjets_Sherpa22CKKWTheoVR6JGGx12HM_mt))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtMu"), wjets_Sherpa22CKKWTheoVR6JGGx12HM_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_mtEM"), wjets_Sherpa22CKKWTheoVR6JGGx12HM_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEl"), wjets_Sherpa22CKKWTheoVR6JGGx12HM_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityMu"), wjets_Sherpa22CKKWTheoVR6JGGx12HM_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR6JGGx12HM_aplanarityEM"), wjets_Sherpa22CKKWTheoVR6JGGx12HM_aplanarity))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEl"), wjets_Sherpa22CKKWTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGMu"), wjets_Sherpa22CKKWTheoSR4JhighxGG))   
    generatorSyst.append((("wjets_Sherpa221","SR4JhighxGGEM"), wjets_Sherpa22CKKWTheoSR4JhighxGG))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEl"), wjets_Sherpa22CKKWTheoVR4JhighxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtMu"), wjets_Sherpa22CKKWTheoVR4JhighxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_mtEM"), wjets_Sherpa22CKKWTheoVR4JhighxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEl"), wjets_Sherpa22CKKWTheoVR4JhighxGG_metovermeff))
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffMu"), wjets_Sherpa22CKKWTheoVR4JhighxGG_metovermeff))    
    generatorSyst.append((("wjets_Sherpa221","VR4JhighxGG_metovermeffEM"), wjets_Sherpa22CKKWTheoVR4JhighxGG_metovermeff))
    
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEl"), wjets_Sherpa22CKKWTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclMu"), wjets_Sherpa22CKKWTheoSR4JlowxGGexcl))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGexclEM"), wjets_Sherpa22CKKWTheoSR4JlowxGGexcl))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEl"), wjets_Sherpa22CKKWTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscMu"), wjets_Sherpa22CKKWTheoSR4JlowxGGdisc))   
    generatorSyst.append((("wjets_Sherpa221","SR4JlowxGGdiscEM"), wjets_Sherpa22CKKWTheoSR4JlowxGGdisc))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEl"), wjets_Sherpa22CKKWTheoVR4JlowxGG_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtMu"), wjets_Sherpa22CKKWTheoVR4JlowxGG_mt)) 
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_mtEM"), wjets_Sherpa22CKKWTheoVR4JlowxGG_mt))   
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEl"), wjets_Sherpa22CKKWTheoVR4JlowxGG_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffMu"), wjets_Sherpa22CKKWTheoVR4JlowxGG_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JlowxGG_metovermeffEM"), wjets_Sherpa22CKKWTheoVR4JlowxGG_aplanarity))

    
       
    
    
    
    
    
    


    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12El"), wjets_Sherpa22GenTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12Mu"), wjets_Sherpa22GenTheoSR4JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12EM"), wjets_Sherpa22GenTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEl"), wjets_Sherpa22GenTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtMu"), wjets_Sherpa22GenTheoVR4JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEl"), wjets_Sherpa22GenTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityMu"), wjets_Sherpa22GenTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEM"), wjets_Sherpa22GenTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEM"), wjets_Sherpa22GenTheoVR4JSSx12_aplanarity))
   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12El"), wjets_Sherpa22GenTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12Mu"), wjets_Sherpa22GenTheoSR5JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12EM"), wjets_Sherpa22GenTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEl"), wjets_Sherpa22GenTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtMu"), wjets_Sherpa22GenTheoVR5JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEl"), wjets_Sherpa22GenTheoVR5JSSx12_met))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metMu"), wjets_Sherpa22GenTheoVR5JSSx12_met))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEM"), wjets_Sherpa22GenTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEM"), wjets_Sherpa22GenTheoVR5JSSx12_met))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEl"), wjets_Sherpa22GenTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxMu"), wjets_Sherpa22GenTheoSR4JSSlowx))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEM"), wjets_Sherpa22GenTheoSR4JSSlowx))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxnoBJetEM"), wjets_Sherpa22GenTheoSR4JSSlowx))

    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEl"), wjets_Sherpa22GenTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtMu"), wjets_Sherpa22GenTheoVR4JSSlowx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEl"), wjets_Sherpa22GenTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityMu"), wjets_Sherpa22GenTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEM"), wjets_Sherpa22GenTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEM"), wjets_Sherpa22GenTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEl"), wjets_Sherpa22GenTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxMu"), wjets_Sherpa22GenTheoSR5JSShighx))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEM"), wjets_Sherpa22GenTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEl"), wjets_Sherpa22GenTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtMu"), wjets_Sherpa22GenTheoVR5JSShighx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEl"), wjets_Sherpa22GenTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityMu"), wjets_Sherpa22GenTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEM"), wjets_Sherpa22GenTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEM"), wjets_Sherpa22GenTheoVR5JSShighx_aplanarity))
    


    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12El"), wjets_Sherpa22ResummationTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12Mu"), wjets_Sherpa22ResummationTheoSR4JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12EM"), wjets_Sherpa22ResummationTheoSR4JSSx12))      
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEl"), wjets_Sherpa22ResummationTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtMu"), wjets_Sherpa22ResummationTheoVR4JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEl"), wjets_Sherpa22ResummationTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityMu"), wjets_Sherpa22ResummationTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEM"), wjets_Sherpa22ResummationTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEM"), wjets_Sherpa22ResummationTheoVR4JSSx12_aplanarity))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12El"), wjets_Sherpa22ResummationTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12Mu"), wjets_Sherpa22ResummationTheoSR5JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12EM"), wjets_Sherpa22ResummationTheoSR5JSSx12))
    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEl"), wjets_Sherpa22ResummationTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtMu"), wjets_Sherpa22ResummationTheoVR5JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEl"), wjets_Sherpa22ResummationTheoVR5JSSx12_met))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metMu"), wjets_Sherpa22ResummationTheoVR5JSSx12_met))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEM"), wjets_Sherpa22ResummationTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEM"), wjets_Sherpa22ResummationTheoVR5JSSx12_met))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEl"), wjets_Sherpa22ResummationTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxMu"), wjets_Sherpa22ResummationTheoSR4JSSlowx))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEM"), wjets_Sherpa22ResummationTheoSR4JSSlowx))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxnoBJetEM"), wjets_Sherpa22ResummationTheoSR4JSSlowx))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEl"), wjets_Sherpa22ResummationTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtMu"), wjets_Sherpa22ResummationTheoVR4JSSlowx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEl"), wjets_Sherpa22ResummationTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityMu"), wjets_Sherpa22ResummationTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEM"), wjets_Sherpa22ResummationTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEM"), wjets_Sherpa22ResummationTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEl"), wjets_Sherpa22ResummationTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxMu"), wjets_Sherpa22ResummationTheoSR5JSShighx))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEM"), wjets_Sherpa22ResummationTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEl"), wjets_Sherpa22ResummationTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtMu"), wjets_Sherpa22ResummationTheoVR5JSShighx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEl"), wjets_Sherpa22ResummationTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityMu"), wjets_Sherpa22ResummationTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEM"), wjets_Sherpa22ResummationTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEM"), wjets_Sherpa22ResummationTheoVR5JSShighx_aplanarity))



    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12El"), wjets_Sherpa22RenormTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12Mu"), wjets_Sherpa22RenormTheoSR4JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12EM"), wjets_Sherpa22RenormTheoSR4JSSx12))     
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEl"), wjets_Sherpa22RenormTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtMu"), wjets_Sherpa22RenormTheoVR4JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEl"), wjets_Sherpa22RenormTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityMu"), wjets_Sherpa22RenormTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEM"), wjets_Sherpa22RenormTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEM"), wjets_Sherpa22RenormTheoVR4JSSx12_aplanarity))
   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12El"), wjets_Sherpa22RenormTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12Mu"), wjets_Sherpa22RenormTheoSR5JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12EM"), wjets_Sherpa22RenormTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEl"), wjets_Sherpa22RenormTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtMu"), wjets_Sherpa22RenormTheoVR5JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEl"), wjets_Sherpa22RenormTheoVR5JSSx12_met))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metMu"), wjets_Sherpa22RenormTheoVR5JSSx12_met))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEM"), wjets_Sherpa22RenormTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEM"), wjets_Sherpa22RenormTheoVR5JSSx12_met))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEl"), wjets_Sherpa22RenormTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxMu"), wjets_Sherpa22RenormTheoSR4JSSlowx))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEM"), wjets_Sherpa22RenormTheoSR4JSSlowx))  
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxnoBJetEM"), wjets_Sherpa22RenormTheoSR4JSSlowx))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEl"), wjets_Sherpa22RenormTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtMu"), wjets_Sherpa22RenormTheoVR4JSSlowx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEl"), wjets_Sherpa22RenormTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityMu"), wjets_Sherpa22RenormTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEM"), wjets_Sherpa22RenormTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEM"), wjets_Sherpa22RenormTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEl"), wjets_Sherpa22RenormTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxMu"), wjets_Sherpa22RenormTheoSR5JSShighx))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEM"), wjets_Sherpa22RenormTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEl"), wjets_Sherpa22RenormTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtMu"), wjets_Sherpa22RenormTheoVR5JSShighx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEl"), wjets_Sherpa22RenormTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityMu"), wjets_Sherpa22RenormTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEM"), wjets_Sherpa22RenormTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEM"), wjets_Sherpa22RenormTheoVR5JSShighx_aplanarity))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12El"), wjets_Sherpa22FacTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12Mu"), wjets_Sherpa22FacTheoSR4JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12EM"), wjets_Sherpa22FacTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEl"), wjets_Sherpa22FacTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtMu"), wjets_Sherpa22FacTheoVR4JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEl"), wjets_Sherpa22FacTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityMu"), wjets_Sherpa22FacTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEM"), wjets_Sherpa22FacTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEM"), wjets_Sherpa22FacTheoVR4JSSx12_aplanarity))
   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12El"), wjets_Sherpa22FacTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12Mu"), wjets_Sherpa22FacTheoSR5JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12EM"), wjets_Sherpa22FacTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEl"), wjets_Sherpa22FacTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtMu"), wjets_Sherpa22FacTheoVR5JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEl"), wjets_Sherpa22FacTheoVR5JSSx12_met))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metMu"), wjets_Sherpa22FacTheoVR5JSSx12_met))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEM"), wjets_Sherpa22FacTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEM"), wjets_Sherpa22FacTheoVR5JSSx12_met))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEl"), wjets_Sherpa22FacTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxMu"), wjets_Sherpa22FacTheoSR4JSSlowx))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEM"), wjets_Sherpa22FacTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxnoBJetEM"), wjets_Sherpa22FacTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEl"), wjets_Sherpa22FacTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtMu"), wjets_Sherpa22FacTheoVR4JSSlowx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEl"), wjets_Sherpa22FacTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityMu"), wjets_Sherpa22FacTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEM"), wjets_Sherpa22FacTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEM"), wjets_Sherpa22FacTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEl"), wjets_Sherpa22FacTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxMu"), wjets_Sherpa22FacTheoSR5JSShighx))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEM"), wjets_Sherpa22FacTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEl"), wjets_Sherpa22FacTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtMu"), wjets_Sherpa22FacTheoVR5JSShighx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEl"), wjets_Sherpa22FacTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityMu"), wjets_Sherpa22FacTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEM"), wjets_Sherpa22FacTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEM"), wjets_Sherpa22FacTheoVR5JSShighx_aplanarity))
    
    
    
    
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12El"), wjets_Sherpa22CKKWTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12Mu"), wjets_Sherpa22CKKWTheoSR4JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSx12EM"), wjets_Sherpa22CKKWTheoSR4JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEl"), wjets_Sherpa22CKKWTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtMu"), wjets_Sherpa22CKKWTheoVR4JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEl"), wjets_Sherpa22CKKWTheoVR4JSSx12_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityMu"), wjets_Sherpa22CKKWTheoVR4JSSx12_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_mtEM"), wjets_Sherpa22CKKWTheoVR4JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_aplanarityEM"), wjets_Sherpa22CKKWTheoVR4JSSx12_aplanarity))
   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12El"), wjets_Sherpa22CKKWTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12Mu"), wjets_Sherpa22CKKWTheoSR5JSSx12))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSSx12EM"), wjets_Sherpa22CKKWTheoSR5JSSx12))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEl"), wjets_Sherpa22CKKWTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtMu"), wjets_Sherpa22CKKWTheoVR5JSSx12_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEl"), wjets_Sherpa22CKKWTheoVR5JSSx12_met))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metMu"), wjets_Sherpa22CKKWTheoVR5JSSx12_met))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_mtEM"), wjets_Sherpa22CKKWTheoVR5JSSx12_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_metEM"), wjets_Sherpa22CKKWTheoVR5JSSx12_met))

    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEl"), wjets_Sherpa22CKKWTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxMu"), wjets_Sherpa22CKKWTheoSR4JSSlowx))   
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxEM"), wjets_Sherpa22CKKWTheoSR4JSSlowx))
    generatorSyst.append((("wjets_Sherpa221","SR4JSSlowxnoBJetEM"), wjets_Sherpa22CKKWTheoSR4JSSlowx))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEl"), wjets_Sherpa22CKKWTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtMu"), wjets_Sherpa22CKKWTheoVR4JSSlowx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEl"), wjets_Sherpa22CKKWTheoVR4JSSlowx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityMu"), wjets_Sherpa22CKKWTheoVR4JSSlowx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_mtEM"), wjets_Sherpa22CKKWTheoVR4JSSlowx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_aplanarityEM"), wjets_Sherpa22CKKWTheoVR4JSSlowx_aplanarity))


    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEl"), wjets_Sherpa22CKKWTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxMu"), wjets_Sherpa22CKKWTheoSR5JSShighx))   
    generatorSyst.append((("wjets_Sherpa221","SR5JSShighxEM"), wjets_Sherpa22CKKWTheoSR5JSShighx))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEl"), wjets_Sherpa22CKKWTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtMu"), wjets_Sherpa22CKKWTheoVR5JSShighx_mt))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEl"), wjets_Sherpa22CKKWTheoVR5JSShighx_aplanarity))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityMu"), wjets_Sherpa22CKKWTheoVR5JSShighx_aplanarity))    
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_mtEM"), wjets_Sherpa22CKKWTheoVR5JSShighx_mt))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_aplanarityEM"), wjets_Sherpa22CKKWTheoVR5JSShighx_aplanarity))


    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEl"), wjets_Sherpa22RenormTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBMu"), wjets_Sherpa22RenormTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEM"), wjets_Sherpa22RenormTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEl"), wjets_Sherpa22FacTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBMu"), wjets_Sherpa22FacTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEM"), wjets_Sherpa22FacTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEl"), wjets_Sherpa22CKKWTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBMu"), wjets_Sherpa22CKKWTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEM"), wjets_Sherpa22CKKWTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEl"), wjets_Sherpa22ResummationTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBMu"), wjets_Sherpa22ResummationTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEM"), wjets_Sherpa22ResummationTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEl"), wjets_Sherpa22GenTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBMu"), wjets_Sherpa22GenTheoVR4JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSx12_DBEM"), wjets_Sherpa22GenTheoVR4JSSx12_DB))


    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEl"), wjets_Sherpa22RenormTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBMu"), wjets_Sherpa22RenormTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEM"), wjets_Sherpa22RenormTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEl"), wjets_Sherpa22FacTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBMu"), wjets_Sherpa22FacTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEM"), wjets_Sherpa22FacTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEl"), wjets_Sherpa22CKKWTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBMu"), wjets_Sherpa22CKKWTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEM"), wjets_Sherpa22CKKWTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEl"), wjets_Sherpa22ResummationTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBMu"), wjets_Sherpa22ResummationTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEM"), wjets_Sherpa22ResummationTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEl"), wjets_Sherpa22GenTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBMu"), wjets_Sherpa22GenTheoVR5JSSx12_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSSx12_DBEM"), wjets_Sherpa22GenTheoVR5JSSx12_DB))

    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEl"), wjets_Sherpa22RenormTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBMu"), wjets_Sherpa22RenormTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEM"), wjets_Sherpa22RenormTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEl"), wjets_Sherpa22FacTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBMu"), wjets_Sherpa22FacTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEM"), wjets_Sherpa22FacTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEl"), wjets_Sherpa22CKKWTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBMu"), wjets_Sherpa22CKKWTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEM"), wjets_Sherpa22CKKWTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEl"), wjets_Sherpa22ResummationTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBMu"), wjets_Sherpa22ResummationTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEM"), wjets_Sherpa22ResummationTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEl"), wjets_Sherpa22GenTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBMu"), wjets_Sherpa22GenTheoVR4JSSlowx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR4JSSlowx_DBEM"), wjets_Sherpa22GenTheoVR4JSSlowx_DB))

    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEl"), wjets_Sherpa22RenormTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBMu"), wjets_Sherpa22RenormTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEM"), wjets_Sherpa22RenormTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEl"), wjets_Sherpa22FacTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBMu"), wjets_Sherpa22FacTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEM"), wjets_Sherpa22FacTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEl"), wjets_Sherpa22CKKWTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBMu"), wjets_Sherpa22CKKWTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEM"), wjets_Sherpa22CKKWTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEl"), wjets_Sherpa22ResummationTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBMu"), wjets_Sherpa22ResummationTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEM"), wjets_Sherpa22ResummationTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEl"), wjets_Sherpa22GenTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBMu"), wjets_Sherpa22GenTheoVR5JSShighx_DB))
    generatorSyst.append((("wjets_Sherpa221","VR5JSShighx_DBEM"), wjets_Sherpa22GenTheoVR5JSShighx_DB))





    

   

  
    return generatorSyst
