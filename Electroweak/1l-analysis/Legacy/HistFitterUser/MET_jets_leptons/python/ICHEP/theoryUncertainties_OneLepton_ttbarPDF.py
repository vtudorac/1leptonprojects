import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


TtbarPDFTheoList = [
    ("SR2J",                        (1+0.09, 1-0.08)),
    ("SR4JSSlowx",                  (1+0.07, 1-0.05)),
    ("SR4JSSx12",                   (1+0.06, 1-0.03)),
    ("SR4JhighxGG",                 (1+0.15, 1-0.14)),
    ("SR4JlowxGGdisc",              (1+0.11, 1-0.19)),
    ("SR4JlowxGGexcl",              (1+0.10, 1-0.12)),
    ("SR5JSShighx",                 (1+0.05, 1-0.06)),
    ("SR5JSSx12",                   (1+0.06, 1-0.04)),
    ("SR6JGGx12",                   (1+0.04, 1-0.05)),
    ("SR6JGGx12HM",                 (1+0.11, 1-0.09)),
    ("TVR4JhighxGGnometovermeff",   (1+0.00, 1-0.00)),
    ("TVR4JhighxGGnomt",            (1+0.02, 1-0.02)),
    ("TVR4JlowxGGnoaplanarity",     (1+0.02, 1-0.01)),
    ("TVR4JlowxGGnomt",             (1+0.01, 1-0.01)),
    ("VR2J1nomet",                  (1+0.01, 1-0.00)),
    ("VR2J2nomt",                   (1+0.01, 1-0.01)),
    ("VR2J_1",                      (1+0.10, 1-0.06)),
    ("VR2J_2",                      (1+0.03, 1-0.02)),
    ("VR4JSSlowx_DR",               (1+0.01, 1-0.01)),
    ("VR4JSSlowx_aplanarity",       (1+0.07, 1-0.07)),
    ("VR4JSSlowx_mt",               (1+0.01, 1-0.01)),
    ("VR4JSSlowxnoaplanarity",      (1+0.05, 1-0.05)),
    ("VR4JSSlowxnomt",              (1+0.00, 1-0.01)),
    ("VR4JSSx12_DR",                (1+0.02, 1-0.02)),
    ("VR4JSSx12_aplanarity",        (1+0.07, 1-0.05)),
    ("VR4JSSx12_mt",                (1+0.07, 1-0.05)),
    ("VR4JSSx12noaplanarity",       (1+0.06, 1-0.06)),
    ("VR4JSSx12nomt",               (1+0.05, 1-0.04)),
    ("VR4JhighxGG_metovermeff",     (1+0.04, 1-0.02)),
    ("VR4JhighxGG_mt",              (1+0.14, 1-0.07)),
    ("VR4JhighxGGnometovermeff",    (1+0.00, 1-0.00)),
    ("VR4JhighxGGnomt",             (1+0.02, 1-0.02)),
    ("VR4JlowxGG_aplanarity",       (1+0.07, 1-0.02)),
    ("VR4JlowxGG_mt",               (1+0.05, 1-0.07)),
    ("VR4JlowxGGnoaplanarity",      (1+0.02, 1-0.01)),
    ("VR4JlowxGGnomt",              (1+0.01, 1-0.01)),
    ("VR5JSShighx_DR",              (1+0.01, 1-0.01)),
    ("VR5JSShighx_aplanarity",      (1+0.12, 1-0.12)),
    ("VR5JSShighx_mt",              (1+0.04, 1-0.04)),
    ("VR5JSShighxnoaplanarity",     (1+0.10, 1-0.09)),
    ("VR5JSShighxnomt",             (1+0.03, 1-0.03)),
    ("VR5JSSx12_DR",                (1+0.01, 1-0.01)),
    ("VR5JSSx12_met",               (1+0.07, 1-0.08)),
    ("VR5JSSx12_mt",                (1+0.02, 1-0.03)),
    ("VR5JSSx12nomet",              (1+0.02, 1-0.03)),
    ("VR5JSSx12nomt",               (1+0.01, 1-0.01)),
    ("VR6JGGx12HM_aplanarity",      (1+0.02, 1-0.02)),
    ("VR6JGGx12HM_mt",              (1+0.02, 1-0.02)),
    ("VR6JGGx12HMnoaplanarity",     (1+0.02, 1-0.02)),
    ("VR6JGGx12HMnomt",             (1+0.01, 1-0.01)),
    ("VR6JGGx12_aplanarity",        (1+0.04, 1-0.05)),
    ("VR6JGGx12_mt",                (1+0.02, 1-0.03)),
    ("VR6JGGx12noaplanarity",       (1+0.01, 1-0.01)),
    ("VR6JGGx12nomt",               (1+0.01, 1-0.01)),
]

def TheorUnc(generatorSyst):
    for regionName, (errUp, errDown) in TtbarPDFTheoList:
        for elmu in ["El", "Mu", "EM"]:
            generatorSyst.append(
                (
                    ("ttbar", regionName+elmu),
                    Systematic("TtbarPDFTheo_"+regionName,
                               configMgr.weights,
                               configMgr.weights+["{0:.2f}".format(errUp)],
                               configMgr.weights+["{0:.2f}".format(errDown)],
                               "weight","overallSys")
                )
            )
