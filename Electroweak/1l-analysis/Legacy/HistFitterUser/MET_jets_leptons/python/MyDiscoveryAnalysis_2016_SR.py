################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import Measurement,Sample
from systematic import Systematic
from math import sqrt

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

##########################

# Set observed and expected number of events in counting experiment
ndata     =  999. 	# Number of events observed in data
nbkg      =  999.	# Number of predicted bkg events
nsig      =  1.  	# Number of predicted signal events
nbkgErr   =  0. #XXX	# (Absolute) Statistical error on bkg estimate
nsigErr    =  0. #XXX          # (Absolute) Statistical error on signal estimate
lumiError = 0.029 	# Relative luminosity uncertainty
SR = "XXX"

try:
    pickedSRs
except NameError:
    pickedSRs = None
    print "\n Choose a SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
    
if pickedSRs != None and len(pickedSRs) > 1:
    print "\n Choose a single(!) SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
elif pickedSRs != None and len(pickedSRs) == 1:
    SR = pickedSRs[0]
    print "\n SR chosen from command line: ", SR

if SR == "SR6JGGx12EM":
    ndata = 31.
    nbkg = 21.39
    nbkgErr = 4.2/nbkg
elif  SR == "SR6JGGx12El":
    ndata = 10.
    nbkg = 10.1
    nbkgErr = 1.987/nbkg
elif SR == "SR6JGGx12Mu":
    ndata = 21.
    nbkg = 11.308
    nbkgErr = 2.5/nbkg
elif SR == "SR6JGGx12EM_2015":
    ndata = 9.
    nbkg = 4.53
    nbkgErr = 0.94/nbkg
elif  SR == "SR6JGGx12El_2015":
    ndata = 3.
    nbkg = 2.02
    nbkgErr = 0.45/nbkg
elif SR == "SR6JGGx12Mu_2015":
    ndata = 6.
    nbkg = 2.5
    nbkgErr = 0.53/nbkg 
## GG
elif SR == "SR2JEM_2016":
    ndata = 37.
    nbkg = 41.545
    nbkgErr = 6.581/nbkg
elif  SR == "SR2JMu_2016":
    ndata = 17.
    nbkg = 24.449
    nbkgErr = 4.015/nbkg
elif SR == "SR2JEl_2016":
    ndata = 20.
    nbkg = 17.096
    nbkgErr = 2.885/nbkg     
elif SR == "SR6JGGx12EM_2016":
    ndata = 31.
    nbkg = 21.388
    nbkgErr = 4.212/nbkg
elif  SR == "SR6JGGx12Mu_2016":
    ndata = 21.
    nbkg = 11.308
    nbkgErr = 2.531/nbkg
elif SR == "SR6JGGx12El_2016":
    ndata = 10.
    nbkg = 10.080
    nbkgErr = 1.987/nbkg     
elif SR == "SR6JGGx12HMEM_2016":
    ndata = 3.
    nbkg = 3.416
    nbkgErr = 1.036/nbkg
elif  SR == "SR6JGGx12HMMu_2016":
    ndata = 3.
    nbkg = 1.830
    nbkgErr = 0.623/nbkg
elif SR == "SR6JGGx12HMEl_2016":
    ndata = 0.
    nbkg = 1.586
    nbkgErr = 0.617/nbkg     
elif SR == "SR4JGGhighxEM_2016":
    ndata = 1.
    nbkg = 3.09
    nbkgErr = 0.81/nbkg     
elif SR == "SR4JGGhighxEl_2016":
    ndata = 0.
    nbkg = 1.10
    nbkgErr = 0.37/nbkg  
elif SR == "SR4JGGhighxMu_2016":
    ndata = 1.
    nbkg = 2.00
    nbkgErr = 0.57/nbkg
elif SR == "SR4JGGlowxEM_2016":
    ndata = 4.
    nbkg = 5.32
    nbkgErr = 1.41/nbkg     
elif SR == "SR4JGGlowxEl_2016":
    ndata = 1.
    nbkg = 2.59
    nbkgErr = 0.81/nbkg  
elif SR == "SR4JGGlowxMu_2016":
    ndata = 3.
    nbkg = 2.72
    nbkgErr = 0.79/nbkg        

## SS
elif SR == "SR4JSSx12EM_2016":
    ndata = 4.
    nbkg = 2.998
    nbkgErr = 0.888/nbkg
elif SR == "SR4JSSx12El_2016":
    ndata = 0.
    nbkg = 1.578
    nbkgErr = 0.572/nbkg
elif SR == "SR4JSSx12Mu_2016":
    ndata = 4.
    nbkg = 1.420
    nbkgErr = 0.422/nbkg
    
elif SR == "SR5JSSx12EM_2016":
    ndata = 5.
    nbkg = 7.686
    nbkgErr = 1.520/nbkg
elif SR == "SR5JSSx12El_2016":
    ndata = 1.
    nbkg = 3.835
    nbkgErr = 0.821/nbkg
elif SR == "SR5JSSx12Mu_2016":
    ndata = 4.
    nbkg = 3.850
    nbkgErr = 0.817/nbkg
    
elif SR == "SR4JSSlowxEM_2016":
    ndata = 5.
    nbkg = 6.607
    nbkgErr = 1.666/nbkg
elif SR == "SR4JSSlowxEl_2016":
    ndata = 2.
    nbkg = 3.557
    nbkgErr = 0.984/nbkg
elif SR == "SR4JSSlowxMu_2016":
    ndata = 3.
    nbkg = 3.049
    nbkgErr = 0.825/nbkg
    
elif SR == "SR5JSShighxEM_2016":
    ndata = 3.
    nbkg = 3.033
    nbkgErr = 0.832/nbkg
elif SR == "SR5JSShighxEl_2016":
    ndata = 0.
    nbkg = 1.598
    nbkgErr = 0.648/nbkg
elif SR == "SR5JSShighxMu_2016":
    ndata = 3.
    nbkg = 1.435
    nbkgErr = 0.337/nbkg

print 1. + nbkgErr    

# correlated systematic between background and signal (1 +- relative uncertainties)
#corb = Systematic("cor",configMgr.weights, [1. + nbkgErr],[1. - nbkgErr], "user","userHistoSys")  # AK: 0.97/3.63=0.26
corb = Systematic("cor",configMgr.weights, (1. + nbkgErr),(1. - nbkgErr), "user","userOverallSys")  # AK: 0.97/3.63=0.26

##########################

# Setting the parameters of the hypothesis test
#configMgr.nTOYs=5000
configMgr.calculatorType=0 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
configMgr.nTOYs=2000

##########################

# Give the analysis a name MyDiscoveryAnalysis_Moriond_SoftLepton_SR1L3j.py
configMgr.analysisName = "MyDiscoveryAnalysis_Paper2015_HardLepton_"+SR
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

# Define cuts
configMgr.cutsDict[SR] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto([nbkg],SR,"cuts")
#bkgSample.buildStatErrors([nbkgErr],"SR5JEl","cuts")
bkgSample.addSystematic(corb)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_"+SR,1.,0.,30.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto([nsig],SR,"cuts")
#sigSample.buildStatErrors([nsigErr],"SR5JEl","cuts")

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([ndata],SR,"cuts")

# Define top-level
ana = configMgr.addTopLevelXML("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="BasicMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_"+SR)

#meas.addParamSetting("Lumi","const",1.0)

# Add the channel
chan = ana.addChannel("cuts",[SR],1,0.,1.)
ana.setSignalChannels([chan])

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
