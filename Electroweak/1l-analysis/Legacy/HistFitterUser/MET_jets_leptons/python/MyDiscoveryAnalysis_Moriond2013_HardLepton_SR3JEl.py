################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
from systematic import Systematic
from math import sqrt

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

##########################

# Set observed and expected number of events in counting experiment
ndata     =  4. 	# Number of events observed in data
nbkg      =  3.89	# Number of predicted bkg events
nsig      =  1.  	# Number of predicted signal events
nbkgErr   =  0. #XXX	# (Absolute) Statistical error on bkg estimate
nsigErr    =  0. #XXX          # (Absolute) Statistical error on signal estimate
lumiError = 0.028 	# Relative luminosity uncertainty

# correlated systematic between background and signal (1 +- relative uncertainties)
corb = Systematic("cor",configMgr.weights, [1.25],[0.75], "user","userHistoSys")  # AK: 0.98/3.89=0.25

##########################

# Setting the parameters of the hypothesis test
#configMgr.nTOYs=5000
configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
configMgr.nTOYs=2000

##########################

# Give the analysis a name MyDiscoveryAnalysis_Moriond_SoftLepton_SR1L3j.py
configMgr.analysisName = "MyDiscoveryAnalysis_Moriond2013_HardLepton_SR3JEl"
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

# Define cuts
configMgr.cutsDict["SR3JEl"] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto([nbkg],"SR3JEl","cuts")
#bkgSample.buildStatErrors([nbkgErr],"SR3JEl","cuts")
bkgSample.addSystematic(corb)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_SR3JEl",1.,0.,10.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto([nsig],"SR3JEl","cuts")
#sigSample.buildStatErrors([nsigErr],"SR3JEl","cuts")

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([ndata],"SR3JEl","cuts")

# Define top-level
ana = configMgr.addTopLevelXML("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="BasicMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_SR3JEl")

#meas.addParamSetting("Lumi","const",1.0)

# Add the channel
chan = ana.addChannel("cuts",["SR3JEl"],1,0.,1.)
ana.setSignalChannels([chan])

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
