################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
from systematic import Systematic
from math import sqrt

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

##########################

# Set observed and expected number of events in counting experiment
ndata     =  999. 	# Number of events observed in data
nbkg      =  999.	# Number of predicted bkg events
nsig      =  1.  	# Number of predicted signal events
nbkgErr   =  0. #XXX	# (Absolute) Statistical error on bkg estimate
nsigErr    =  0. #XXX          # (Absolute) Statistical error on signal estimate
lumiError = 0.028 	# Relative luminosity uncertainty
SR = "XXX"

try:
    pickedSRs
except NameError:
    pickedSRs = None
    print "\n Choose a SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
    
if pickedSRs != None and len(pickedSRs) > 1:
    print "\n Choose a single(!) SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
elif pickedSRs != None and len(pickedSRs) == 1:
    SR = pickedSRs[0]
    print "\n SR chosen from command line: ", SR

if SR == "SR3JElMuDisc":
    ndata = 9.
    nbkg = 8.44
    nbkgErr = 2.91/nbkg
elif  SR == "SR3JElMuExcl":
    ndata = 72.
    nbkg = 79.64
    nbkgErr = 8.92/nbkg
elif SR == "SR5JElMuDisc":
    ndata = 5.
    nbkg = 6.94
    nbkgErr = 2.63/nbkg
elif  SR == "SR5JElMuExcl":
    ndata = 16.
    nbkg = 17.09
    nbkgErr = 4.13/nbkg
elif SR == "SR6JElMuDisc":
    ndata = 2.
    nbkg = 4.6
    nbkgErr = 2.15/nbkg
elif  SR == "SR6JElMuExcl":
    ndata = 12.
    nbkg = 16.8
    nbkgErr = 4.10/nbkg
# Electron channels
elif SR == "SR3JElDisc":
    ndata = 4.
    nbkg = 3.89
    nbkgErr = 0.98/nbkg
elif  SR == "SR3JElExcl":
    ndata = 45.
    nbkg = 46.36
    nbkgErr = 8.02/nbkg
elif SR == "SR5JElDisc":
    ndata = 4.
    nbkg = 3.63
    nbkgErr = 0.98/nbkg
elif  SR == "SR5JElExcl":
    ndata = 12.
    nbkg = 12.18
    nbkgErr = 5.23/nbkg
elif SR == "SR6JElDisc":
    ndata = 2.
    nbkg = 2.04
    nbkgErr = 0.74/nbkg
elif  SR == "SR6JElExcl":
    ndata = 7.
    nbkg = 9.73
    nbkgErr = 2.01/nbkg
# Muon channels
elif SR == "SR3JMuDisc":
    ndata = 5.
    nbkg = 2.65
    nbkgErr = 0.86/nbkg
elif  SR == "SR3JMuExcl":
    ndata = 28.
    nbkg = 38.14
    nbkgErr = 5.77/nbkg
elif SR == "SR5JMuDisc":
    ndata = 6.
    nbkg = 5.93
    nbkgErr = 1.48/nbkg
elif  SR == "SR5JMuExcl":
    ndata = 7.
    nbkg = 7.05
    nbkgErr = 1.61/nbkg
elif SR == "SR6JMuDisc":
    ndata = 0.
    nbkg = 1.67
    nbkgErr = 0.56/nbkg
elif  SR == "SR6JMuExcl":
    ndata = 7.
    nbkg = 7.42
    nbkgErr = 1.66/nbkg




# correlated systematic between background and signal (1 +- relative uncertainties)
#corb = Systematic("cor",configMgr.weights, [1. + nbkgErr],[1. - nbkgErr], "user","userHistoSys")  # AK: 0.97/3.63=0.26
corb = Systematic("cor",configMgr.weights, (1. + nbkgErr),(1. - nbkgErr), "user","userOverallSys")  # AK: 0.97/3.63=0.26

##########################

# Setting the parameters of the hypothesis test
#configMgr.nTOYs=5000
configMgr.calculatorType=0 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
configMgr.nTOYs=2000

##########################

# Give the analysis a name MyDiscoveryAnalysis_Moriond_SoftLepton_SR1L3j.py
configMgr.analysisName = "MyDiscoveryAnalysis_Paper2013_HardLepton_"+SR
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

# Define cuts
configMgr.cutsDict[SR] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto([nbkg],SR,"cuts")
#bkgSample.buildStatErrors([nbkgErr],"SR5JEl","cuts")
bkgSample.addSystematic(corb)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_"+SR,1.,0.,150.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto([nsig],SR,"cuts")
#sigSample.buildStatErrors([nsigErr],"SR5JEl","cuts")

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([ndata],SR,"cuts")

# Define top-level
ana = configMgr.addTopLevelXML("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="BasicMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_"+SR)

#meas.addParamSetting("Lumi","const",1.0)

# Add the channel
chan = ana.addChannel("cuts",[SR],1,0.,1.)
ana.setSignalChannels([chan])

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
