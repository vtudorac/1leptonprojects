################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import Measurement,Sample
from systematic import Systematic
from math import sqrt

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

##########################

# Set observed and expected number of events in counting experiment
ndata     =  999. 	# Number of events observed in data
nbkg      =  999.	# Number of predicted bkg events
nsig      =  1.  	# Number of predicted signal events
nbkgErr   =  0. #XXX	# (Absolute) Statistical error on bkg estimate
nsigErr    =  0. #XXX          # (Absolute) Statistical error on signal estimate
lumiError = 0.05 	# Relative luminosity uncertainty
SR = "XXX"

try:
    pickedSRs
except NameError:
    pickedSRs = None
    print "\n Choose a SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
    
if pickedSRs != None and len(pickedSRs) > 1:
    print "\n Choose a single(!) SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
elif pickedSRs != None and len(pickedSRs) == 1:
    SR = pickedSRs[0]
    print "\n SR chosen from command line: ", SR

if SR == "SR4JlowxEM":
    ndata = 1.
    nbkg = 1.27
    nbkgErr = 0.50/nbkg
elif  SR == "SR4JlowxEl":
    ndata = 0.
    nbkg = 0.69
    nbkgErr = 0.35/nbkg
elif SR == "SR4JlowxMu":
    ndata = 1.
    nbkg = 0.57
    nbkgErr = 0.25/nbkg
elif SR == "SR4JhighxEM":
    ndata = 0.
    nbkg = 0.92
    nbkgErr = 0.5/nbkg
elif  SR == "SR4JhighxEl":
    ndata = 0.
    nbkg = 0.24
    nbkgErr = 0.10/nbkg
elif SR == "SR4JhighxMu":
    ndata = 0.
    nbkg = 0.68
    nbkgErr = 0.46/nbkg
elif SR == "SR5JEM":
    ndata = 0.
    nbkg = 1.27
    nbkgErr = 0.55/nbkg
elif  SR == "SR5JEl":
    ndata = 0.
    nbkg = 0.39
    nbkgErr = 0.17/nbkg
elif SR == "SR5JMu":
    ndata = 0.
    nbkg = 0.88
    nbkgErr = 0.50/nbkg
elif SR == "SR6JEM":
    ndata = 10.
    nbkg = 4.37
    nbkgErr = 1.01/nbkg
elif  SR == "SR6JEl":
    ndata = 2.
    nbkg = 1.89
    nbkgErr = 0.55/nbkg
elif SR == "SR6JMu":
    ndata = 8.
    nbkg = 2.48
    nbkgErr = 0.74/nbkg
elif SR == "SR2JElsoft":
    ndata = 1.
    nbkg = 1.222
    nbkgErr = 0.292/nbkg
elif  SR == "SR2JMusoft":
    ndata = 1.
    nbkg = 2.379
    nbkgErr = 0.542/nbkg
elif SR == "SR2JEMsoft":
    ndata = 2.
    nbkg = 3.601
    nbkgErr = 0.713/nbkg
elif SR == "SR5JElsoft":
    ndata = 5.
    nbkg = 2.170
    nbkgErr = 0.619/nbkg
elif  SR == "SR5JMusoft":
    ndata = 4.
    nbkg = 5.509
    nbkgErr = 1.634/nbkg
elif SR == "SR5JEMsoft":
    ndata = 9.
    nbkg = 7.680
    nbkgErr = 1.888/nbkg    
    
print 1. + nbkgErr    

# correlated systematic between background and signal (1 +- relative uncertainties)
#corb = Systematic("cor",configMgr.weights, [1. + nbkgErr],[1. - nbkgErr], "user","userHistoSys")  # AK: 0.97/3.63=0.26
corb = Systematic("cor",configMgr.weights, (1. + nbkgErr),(1. - nbkgErr), "user","userOverallSys")  # AK: 0.97/3.63=0.26

##########################

# Setting the parameters of the hypothesis test
#configMgr.nTOYs=5000
configMgr.calculatorType=0 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
configMgr.nTOYs=2000

##########################

# Give the analysis a name MyDiscoveryAnalysis_Moriond_SoftLepton_SR1L3j.py
configMgr.analysisName = "MyDiscoveryAnalysis_Paper2015_HardLepton_"+SR
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

# Define cuts
configMgr.cutsDict[SR] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto([nbkg],SR,"cuts")
#bkgSample.buildStatErrors([nbkgErr],"SR5JEl","cuts")
bkgSample.addSystematic(corb)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_"+SR,1.,0.,30.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto([nsig],SR,"cuts")
#sigSample.buildStatErrors([nsigErr],"SR5JEl","cuts")

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([ndata],SR,"cuts")

# Define top-level
ana = configMgr.addTopLevelXML("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="BasicMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_"+SR)

#meas.addParamSetting("Lumi","const",1.0)

# Add the channel
chan = ana.addChannel("cuts",[SR],1,0.,1.)
ana.setSignalChannels([chan])

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
