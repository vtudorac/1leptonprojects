
################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
#from configWriter import TopLevelXML,Measurement,ChannelXML,Sample 
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

from logger import Logger
log = Logger('HardLepton')

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupChannels_Hack(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            #  print "\n XXX SetupChannels_Hack  syst = ", syst.name
            chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
            chan.getSample("SherpaWMassiveBC").addSystematic(syst)        
            chan.getSample("ttbarV").addSystematic(syst)  
            chan.getSample("SingleTop").addSystematic(syst) 		
            chan.getSample("SherpaDibosonsMassiveBC").addSystematic(syst) 
            chan.getSample("SherpaZMassiveBC").addSystematic(syst) 
    return

def SetupChannels_Hack_noWinSR(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            if ("SR5J" in chan.name) or ("SR6J" in chan.name):
                print "\n\n XXX \n in SR without W:", chan.name
                chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
            else:
                print "\n\n XXX-2 in SR with W:", chan.name
                chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
                chan.getSample("SherpaWMassiveBC").addSystematic(syst)
    return
    
def SetupChannels_nofiles(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
            chan.getSample("SherpaWMassiveBC").addSystematic(syst)
            chan.getSample("ttbarV").addSystematic(syst)  
            chan.getSample("SingleTop").addSystematic(syst) 		
            chan.getSample("SherpaDibosonsMassiveBC").addSystematic(syst) 
            chan.getSample("SherpaZMassiveBC").addSystematic(syst) 	    
    return   

    
def SetupChannels_nofiles_noWinSR(channels,systList):
    for chan in channels:
        for syst in systList:
            if ("5J" in chan.name) or ("6J" in chan.name):
                print "\n\n XXX \n in SR without W:", chan.name
                chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
            else:
                print "\n\n XXX-2 in SR with W:", chan.name
                chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
                chan.getSample("SherpaWMassiveBC").addSystematic(syst)
    return   
# ********************************************************************* #
#                              Debug
# ********************************************************************* #


# ********************************************************************* #
#                              Main part
# ********************************************************************* #
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

useStat=True
useNJetNormFac=True
usettbarreweighting=False

SystList=[]
SystList.append("JES")      # Jet Energy Scale (common)
doSmallJESPars=False
SystList.append("JER")      # Jet Energy Resolution (common)
#SystList.append("LepEff")   # Lepton efficiency (e&m) #####negligible (1 %)
#SystList.append("LepTrig")  # Trigger efficiency (e&m)
SystList.append("ResoSt")   # CellOut energy resolution uncertainty
SystList.append("ScaleSt")  # CellOut energy scale uncertainty 
#SystList.append("EES")      # Electron Energy Scale (e only)
#SystList.append("MER")      # Muon Energy Resolution (m only)
SystList.append("PileUp")      # Pile-up
SystList.append("BTag")        # b-tagging uncertainty
SystList.append("JVF")        

SystList.append("GenW")     # Generator Systematics W    (common)
SystList.append("GenTTbar") # Generator Systematics TTbar(common)

#SystList.append("PythAlpTTbar") # Pythia/Alpgen generators difference TTbar
#SystList.append("PythSherpaTTbar") # Pythia/Sherpa generators difference TTbar
#SystList.append("PowhegJimmyTTbar") # Pythia/PowhegJimmy generators difference TTbar
SystList.append("GenPowhegJimmyTTbar") # Pythia/PowhegJimmy generators difference TTbar

SystList.append("GenDB") # Generator Systematics DB    (common)
SystList.append("GenZjets") # Generator Systematics Z+jets    (common)
SystList.append("GenDY") # Generator Systematics DY    (common)

SystList.append("GenTTbarV") # Generator Systematics ttbarV    (common)
SystList.append("GenSingleTop")# Generator Systematics SingleTop   (common)
SystList.append("GenAlpgenJimmyTTbar")# Generator Systematics SingleTop   (common)



doTableInputs = False #This effectively means no validation plots but only validation tables (but is 100x faster)
ValidRegList={}
ValidRegList["SRTight"] = False
ValidRegList["VR3JhighMET"] = False
ValidRegList["VR3JhighMT"] = False
ValidRegList["VR5JhighMET"] = False
ValidRegList["VR5JhighMT"] = False
ValidRegList["VR6JhighMET"] = False
ValidRegList["VR6JhighMT"] = False
ValidRegList["INCL"] = False
ValidRegList["WRV"] = False
ValidRegList["TRV"] = False

doDiscoveryMode=False

CRregions = ["3J","5J","6J"]
#CRregions = ["6J"]

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 3J,5J,6J option)"

for cr in CRregions:
    if "3J" in cr and doTableInputs:
        ValidRegList["VR3JhighMET"] = True
        ValidRegList["VR3JhighMT"] = True
    elif "5J" in cr and doTableInputs:
        ValidRegList["VR5JhighMET"] = True
        ValidRegList["VR5JhighMT"] = True
    elif "6J" in cr and doTableInputs:
        ValidRegList["VR6JhighMET"] = True
        ValidRegList["VR6JhighMT"] = True
 
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr

doSignalOnly=False #Remove all bkgs for signal histo creation step
if configMgr.executeHistFactory:
    doSignalOnly=False

if not 'sigSamples' in dir():
    sigSamples=["pMSSM_123092720_0_10_P"]


if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix += '_GG1stepx12'
    elif 'GG1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix += '_GG1stepgridx'
    elif 'SS1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix += '_SS1stepx12'
    elif 'SS1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix += '_SS1stepgridx' 
    elif 'GG2WWZZ' in sigSamples[0]:
        analysissuffix += '_GG2WWZZ'
    elif 'GG2CNsl' in sigSamples[0]:
        analysissuffix += '_GG2CNsl'    
    elif 'SS2WWZZ' in sigSamples[0]:
        analysissuffix += '_SS2WWZZ'
    elif 'SS2CNsl' in sigSamples[0]:
        analysissuffix += '_SS2CNsl' 
    elif 'pMSSM' in sigSamples[0]:
        analysissuffix += '_pMSSM'                
    elif 'HiggsSU' in sigSamples[0]:
        analysissuffix += '_HiggsSU'
    elif 'Gtt' in sigSamples[0]:
        analysissuffix += '_Gtt'
    elif 'GttCharm' in sigSamples[0]:
        analysissuffix += '_GttCharm' 
    elif 'NUHMG' in sigSamples[0]:
        analysissuffix += '_NUHMG'
	
# First define HistFactory attributes
configMgr.analysisName = "OneHardLepton_ForCombination_pMSSM"+analysissuffix # Name to give the analysis
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.3 
configMgr.setLumiUnits("fb-1")

#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStatType=3
configMgr.nPoints=20

configMgr.writeXML = True

configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#Split bdgFiles per channel
sigFiles_e = []
sigFiles_m = []
sigFiles_em = []
#inputDir="root://eosatlas//eos/atlas/user/k/koutsman/trees/slimSkim/v12_1_5/"
inputDir="root://eosatlas//eos/atlas/user/k/khramov/trees/slimSkim/v12_1_5/"
inputDirSig="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v12_1_5/"
inputDirSig2="root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/hard/v120111/"
#inputDir_data="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v12_1_5/"
inputDir_data="root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/hard/v120105_fixCln/"
inputDir_Gtt="root://eosatlas//eos/atlas/user/v/vtudorac/v12trees/"
inputDir_GttCharm="root://eosatlas//eos/atlas/user/v/vtudorac/v12trees/"
inputDir_Ttbar="root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/hard/v120105/"
inputDir_QCD="root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/hard/v120105_fixCln/"
inputDirSig_pMSSM="root://eosatlas//eos/atlas/user/v/vtudorac/pMSSM/"

onLxplus=True

if not onLxplus:
    print "INFO : Running locally...\n"
    inputDir="/project/etp2/jlorenz/HistFitterTrees/v12_1_5/"
    inputDirSig="/project/etp2/jlorenz/HistFitterTrees/v12_1_5/"
    inputDir_data=inputDir
    inputDir_Ttbar=inputDir
    inputDir_QCD=inputDir
else:
    print "INFO : Running on lxplus... \n"
    
missing_points=["SM_SS2CNsl_205_125_85",
"SM_SS2CNsl_245_125_65",
"SM_SS2CNsl_225_185_165",
"SM_SS2CNsl_265_185_145",
"SM_SS2CNsl_305_185_125",
"SM_SS2CNsl_345_185_105",
"SM_SS2CNsl_325_245_205",
"SM_SS2CNsl_365_245_185",
"SM_SS2CNsl_405_245_165",
"SM_SS2CNsl_445_245_145",
"SM_SS2CNsl_485_245_125",
"SM_SS2CNsl_425_305_245",
"SM_SS2CNsl_465_305_225",
"SM_SS2CNsl_505_305_205",
"SM_SS2CNsl_545_305_185",
"SM_SS2WWZZ_205_125_45",
"SM_SS2WWZZ_245_125_5",
"SM_SS2WWZZ_225_185_145",
"SM_SS2WWZZ_265_185_105",
"SM_SS2WWZZ_305_185_65",
"SM_SS2WWZZ_345_185_25",
"SM_SS2WWZZ_325_245_165",
"SM_SS2WWZZ_365_245_125",
"SM_SS2WWZZ_405_245_85",
"SM_SS2WWZZ_445_245_45",
"SM_SS2WWZZ_485_245_5",
"SM_SS2WWZZ_425_305_185",
"SM_SS2WWZZ_465_305_145",
"SM_SS2WWZZ_505_305_105",
"SM_SS2WWZZ_545_305_65",
"SM_GG2CNsl_905_865_845",
"SM_GG2CNsl_945_785_705",
"SM_GG2CNsl_1025_945_905",
"SM_GG2CNsl_185_145_125",
"SM_GG2CNsl_265_225_205",
"SM_GG2CNsl_345_225_165",
"SM_SS1step_300_98_60",
"SM_SS1step_300_132_60",
"SM_SS1step_400_145_60",
"SM_SS1step_400_213_60"
]

# Set the files to read from
## bgdFiles_e = [inputDir+"bkgtree_HardEle.root"] 
## bgdFiles_m = [inputDir+"bkgtree_HardMuo.root"]
## bgdFiles_em = [inputDir+"bkgtree_HardEle.root",inputDir+"bkgtree_HardMuo.root"]
bgdFiles_e = [inputDir+"HardEle_BjetSystFix.root"] 
bgdFiles_m = [inputDir+"HardMuo_BjetSystFix.root"]
bgdFiles_em = [inputDir+"HardEle_BjetSystFix.root",inputDir+"HardMuo_BjetSystFix.root"]

AlpgenTTbarFiles = [inputDir_Ttbar+"HardEle_AlpgenTTbar_NoSys.root", inputDir_Ttbar+"HardMuo_AlpgenTTbar_NoSys.root"]
SherpaTTbarFiles = [inputDir_data+"HardEle_SherpaTTbar_NoSys.root", inputDir_data+"HardMuo_SherpaTTbar_NoSys.root"]
PowhegJimmyTTbarFiles=[inputDir_Ttbar+"HardEle_PowhegJimmyTTbar_NoSys.root", inputDir_Ttbar+"HardMuo_PowhegJimmyTTbar_NoSys.root"]

if myFitType==FitType.Exclusion:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigtree_prefix=""
        if sigpoint in missing_points: sigtree_prefix="missing_"
        if sigpoint in ["SM_SS1step_300_70_60","SM_SS1step_300_110_60"]: sigtree_prefix="replace_"
        sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_HiggsSU_JVF.root",inputDirSig+"sigtree_HardEle_HiggsSU.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_HiggsSU_JVF.root",inputDirSig+"sigtree_HardMuo_HiggsSU.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_HiggsSU_JVF.root",inputDirSig+"sigtree_HardEle_HiggsSU.root",inputDirSig+"sigtree_HardMuo_HiggsSU_JVF.root",inputDirSig+"sigtree_HardMuo_HiggsSU.root"]
        if 'SM_SS1step' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_SS1step_JVF.root",inputDirSig+"sigtree_HardEle_SM_SS1step.root"]
            sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_SS1step_JVF.root",inputDirSig+"sigtree_HardMuo_SM_SS1step.root"]
            sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_SM_SS1step_JVF.root",inputDirSig+"sigtree_HardEle_SM_SS1step.root",inputDirSig+"sigtree_HardMuo_SM_SS1step_JVF.root",inputDirSig+"sigtree_HardMuo_SM_SS1step.root"]
            if not sigtree_prefix=="":
                sigFiles_e[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_SS1step.root"]
                sigFiles_m[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_SS1step.root"]
                sigFiles_em[sigpoint] = [inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_SS1step.root",inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_SS1step.root"]	    
            print "Using simplified models SS onestepCC"
        if 'SM_GG1step' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_GG1step_JVF.root",inputDirSig+"sigtree_HardEle_SM_GG1step.root"]
            sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_GG1step_JVF.root",inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
            #sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_GG1step.root"]
            #sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
            sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_SM_GG1step_JVF.root",inputDirSig+"sigtree_HardEle_SM_GG1step.root",inputDirSig+"sigtree_HardMuo_SM_GG1step_JVF.root",inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
            print "Using simplified models GG onestepCC"
        if 'pMSSM' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig_pMSSM+"sigtree_HardEle_pMSSM.root"]
            sigFiles_m[sigpoint]=[inputDirSig_pMSSM+"sigtree_HardMuo_pMSSM.root"]
            sigFiles_em[sigpoint] = [inputDirSig_pMSSM+"sigtree_HardEle_pMSSM.root",inputDirSig_pMSSM+"sigtree_HardMuo_pMSSM.root"]
            print "Using pMSSM signal model"
        if 'GG2WWZZ' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_GG2WWZZ_JVF.root",inputDirSig+"sigtree_HardEle_SM_GG2WWZZ.root"]
            sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ_JVF.root",inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ.root"]
            sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_SM_GG2WWZZ_JVF.root",rinputDirSig+"sigtree_HardEle_SM_GG2WWZZ.root",inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ_JVF.root",inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ.root"]
            print "Using simplified models GG two step with WWZZ"
        if 'SS2WWZZ' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_SS2WWZZ_JVF.root",inputDirSig+"sigtree_HardEle_SM_SS2WWZZ.root"]
            sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ_JVF.root",inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ.root"]
            sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_SM_SS2WWZZ_JVF.root",inputDirSig+"sigtree_HardEle_SM_SS2WWZZ.root",inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ_JVF.root",inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ.root"]	    
            if not sigtree_prefix=="":
                sigFiles_e[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_SS2WWZZ.root"]
                sigFiles_m[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_SS2WWZZ.root"]
                sigFiles_em[sigpoint] = [inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_SS2WWZZ.root",inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_SS2WWZZ.root"]
            print "Using simplified models SS two step with WWZZ"   
        if 'GG2CNsl' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_GG2CNsl_JVF.root",inputDirSig+"sigtree_HardEle_SM_GG2CNsl.root"]
            sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_GG2CNsl_JVF.root",inputDirSig+"sigtree_HardMuo_SM_GG2CNsl.root"]
            sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_SM_GG2CNsl_JVF.root",inputDirSig+"sigtree_HardEle_SM_GG2CNsl.root",inputDirSig+"sigtree_HardMuo_SM_GG2CNsl_JVF.root",inputDirSig+"sigtree_HardMuo_SM_GG2CNsl.root"]
            if not sigtree_prefix=="":
                sigFiles_e[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_GG2CNslroot"]
                sigFiles_m[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_GG2CNsl.root"]
                sigFiles_em[sigpoint] = [inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_GG2CNsl.root",inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_GG2CNsl.root"]	    
            print "Using simplified models GG two step with sleptons"
        if 'SS2CNsl' in sigpoint:
            sigFiles_e[sigpoint]=[inputDirSig+"sigtree_HardEle_SM_SS2CNsl_JVF.root",inputDirSig+"sigtree_HardEle_SM_SS2CNsl.root"]
            sigFiles_m[sigpoint]=[inputDirSig+"sigtree_HardMuo_SM_SS2CNsl_JVF.root",inputDirSig+"sigtree_HardMuo_SM_SS2CNsl.root"]
            sigFiles_em[sigpoint] = [inputDirSig+"sigtree_HardEle_SM_SS2CNsl_JVF.root",inputDirSig+"sigtree_HardEle_SM_SS2CNsl.root",inputDirSig+"sigtree_HardMuo_SM_SS2CNsl_JVF.root",inputDirSig+"sigtree_HardMuo_SM_SS2CNsl.root"]
            if not sigtree_prefix=="":
                sigFiles_e[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_SS2CNsl.root"]
                sigFiles_m[sigpoint]=[inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_SS2CNsl.root"]
                sigFiles_em[sigpoint] = [inputDirSig2+sigtree_prefix+"sigtree_HardEle_SM_SS2CNsl.root",inputDirSig2+sigtree_prefix+"sigtree_HardMuo_SM_SS2CNsl.root"]	    
            print "Using simplified models SS two step with sleptons"
        if 'Gtt' in sigpoint:
            sigFiles_e[sigpoint]=[inputDir_Gtt+"sigtree_HardEle_SM_Gtt.root"]
            sigFiles_m[sigpoint]=[inputDir_Gtt+"sigtree_HardMuo_SM_Gtt.root"]
            sigFiles_em[sigpoint] = [inputDir_Gtt+"sigtree_HardEle_SM_Gtt.root",inputDir_Gtt+"sigtree_HardMuo_SM_Gtt.root"]	
            print "Using Gtt simplified models"
        if 'GttCharm' in sigpoint:
            sigFiles_e[sigpoint]=[inputDir_GttCharm+"sigtree_HardEle_SM_GttCharm.root"]
            sigFiles_m[sigpoint]=[inputDir_GttCharm+"sigtree_HardMuo_SM_GttCharm.root"]
            sigFiles_em[sigpoint] = [inputDir_GttCharm+"sigtree_HardEle_SM_GttCharm.root",inputDir_GttCharm+"sigtree_HardMuo_SM_GttCharm.root"]	
            print "Using GttCharm simplified models"	
        if 'NUHMG' in sigpoint:
            sigFiles_e[sigpoint]=["root://eosatlas//eos/atlas/user/v/vtudorac/v12trees/sigtree_HardEle_NUHMG.root"]
            sigFiles_m[sigpoint]=["root://eosatlas//eos/atlas/user/v/vtudorac/v12trees/sigtree_HardMuo_NUHMG.root"]
            sigFiles_em[sigpoint]= ["root://eosatlas//eos/atlas/user/v/vtudorac/v12trees/sigtree_HardEle_NUHMG.root","root://eosatlas//eos/atlas/user/v/vtudorac/v12trees/sigtree_HardMuo_NUHMG.root"]	
            print "Using NUHMG model"  

CommonSelection = "&& lep2Pt<10 && lep1Pt>25 && jet1Pt_JVF25pt50>80"

configMgr.cutsDict["h1L_WR3J"]="nJet30_JVF25pt50>2 && jet2Pt_JVF25pt50>80 && jet3Pt_JVF25pt50>30 && jet5Pt_JVF25pt50<30 && met>150 && met<300 &&  nB3Jet30_MV1_60p_JVF25pt50==0 && mt<150 && mt>80 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_TR3J"]="nJet30_JVF25pt50>2 && jet2Pt_JVF25pt50>80 && jet3Pt_JVF25pt50>30 && jet5Pt_JVF25pt50<30 && met>150 && met<300 && nB3Jet30_MV1_60p_JVF25pt50>0 && mt<150 && mt>80 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_WR5J"]="nJet30_JVF25pt50>4 && jet2Pt_JVF25pt50>50 && jet5Pt_JVF25pt50>30 && jet6Pt_JVF25pt50<30 && met>150 && met<300 && nB3Jet30_MV1_60p_JVF25pt50==0 && mt<150 && mt>60 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_TR5J"]="nJet30_JVF25pt50>4 && jet2Pt_JVF25pt50>50 && jet5Pt_JVF25pt50>30 && jet6Pt_JVF25pt50<30 && met>150 && met<300 && nB3Jet30_MV1_60p_JVF25pt50>0 && mt<150 && mt>60 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_WR6J"]="nJet30_JVF25pt50>5 && jet2Pt_JVF25pt50>50 && jet6Pt_JVF25pt50>30 && met>100 && met<200 && nB3Jet30_MV1_60p_JVF25pt50==0 && mt<80 && mt>40 && meffInc30_JVF25pt50>600" + CommonSelection
configMgr.cutsDict["h1L_TR6J"]="nJet30_JVF25pt50>5 && jet2Pt_JVF25pt50>50 && jet6Pt_JVF25pt50>30 && met>150 && met<250 && nB3Jet30_MV1_60p_JVF25pt50>0 && mt<150 && mt>40 && meffInc30_JVF25pt50>600" + CommonSelection
configMgr.cutsDict["h1L_VR3JhighMET"]="nJet30_JVF25pt50>2 && jet2Pt_JVF25pt50>80 && jet3Pt_JVF25pt50>30 && met>300 && met<500 && mt<150 && mt>60 && jet5Pt_JVF25pt50<40 && meffInc30_JVF25pt50>800"  + CommonSelection
configMgr.cutsDict["h1L_VR5JhighMET"]="nJet40_JVF25pt50>4 && jet2Pt_JVF25pt50>50 && jet5Pt_JVF25pt50>40 && met>300 && met<500 && mt<150 && mt>60 && jet6Pt_JVF25pt50<40 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_VR6JhighMET"]="nJet40_JVF25pt50>5 && jet2Pt_JVF25pt50>50 && jet6Pt_JVF25pt50>40 && met>250 && met<500 && mt<150 && mt>60 && meffInc30_JVF25pt50>600" + CommonSelection
configMgr.cutsDict["h1L_VR3JhighMT"]="nJet30_JVF25pt50>2 && jet2Pt_JVF25pt50>80 && jet3Pt_JVF25pt50>30 && met>150 && met<300 && mt<320 && mt>150  && jet5Pt_JVF25pt50<40 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_VR5JhighMT"]="nJet40_JVF25pt50>4 && jet2Pt_JVF25pt50>50 && jet5Pt_JVF25pt50>40 && met>150 && met<300 && mt<320 && mt>150  && jet6Pt_JVF25pt50<40 && meffInc30_JVF25pt50>800" + CommonSelection
configMgr.cutsDict["h1L_VR6JhighMT"]="nJet40_JVF25pt50>5 && jet2Pt_JVF25pt50>50 && jet6Pt_JVF25pt50>40 && met>150 && met<250 && mt<320 && mt>150 && meffInc30_JVF25pt50>600" + CommonSelection

configMgr.cutsDict["h1L_SR3J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>80 && jet3Pt_JVF25pt50>30 && jet5Pt_JVF25pt50<40 && met/meff3Jet30_JVF25pt50>0.3 && meffInc30_JVF25pt50>800"
configMgr.cutsDict["h1L_SR5J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>50 && jet5Pt_JVF25pt50>40 && jet6Pt_JVF25pt50<40 && meffInc30_JVF25pt50>800"
configMgr.cutsDict["h1L_SR6J"]="lep2Pt<10 && lep1Pt>25 && met>250 && mt>150 && jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>50 && jet6Pt_JVF25pt50>40 && meffInc30_JVF25pt50>600"

configMgr.cutsDict["h1L_SR3Jdiscovery"]="lep2Pt<10 && lep1Pt>25 && met>500 && mt>150 && jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>80 && jet3Pt_JVF25pt50>30 && met/meff3Jet30_JVF25pt50>0.3 && meffInc30_JVF25pt50>1400"
configMgr.cutsDict["h1L_SR5Jdiscovery"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>200 && jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>50 && jet5Pt_JVF25pt50>40 && meffInc30_JVF25pt50>1400"
configMgr.cutsDict["h1L_SR6Jdiscovery"]="lep2Pt<10 && lep1Pt>25 && met>350 && mt>150 && jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>50 && jet6Pt_JVF25pt50>40 && meffInc30_JVF25pt50>600"

d=configMgr.cutsDict
OneEleSelection = "&& AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && isNoCrackElectron==1 && (abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))"
OneMuoSelection = "&& AnalysisType==2 && ( EF_mu24_j65_a4tchad_EFxe40_tclcw ) && isNoCrackElectron==1"
OneLepSelection = "&& ( (AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && isNoCrackElectron==1 && (abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta)) ) || (AnalysisType==2 && ( EF_mu24_j65_a4tchad_EFxe40_tclcw ) && isNoCrackElectron==1 ) )"

configMgr.cutsDict["h1L_TR3JEl"] = d["h1L_TR3J"]+OneEleSelection
configMgr.cutsDict["h1L_WR3JEl"] = d["h1L_WR3J"]+OneEleSelection
configMgr.cutsDict["h1L_TR3JMu"] = d["h1L_TR3J"]+OneMuoSelection
configMgr.cutsDict["h1L_WR3JMu"] = d["h1L_WR3J"]+OneMuoSelection
configMgr.cutsDict["h1L_TR5JEl"] = d["h1L_TR5J"]+OneEleSelection
configMgr.cutsDict["h1L_WR5JEl"] = d["h1L_WR5J"]+OneEleSelection
configMgr.cutsDict["h1L_TR5JMu"] = d["h1L_TR5J"]+OneMuoSelection
configMgr.cutsDict["h1L_WR5JMu"] = d["h1L_WR5J"]+OneMuoSelection
configMgr.cutsDict["h1L_TR6JEl"] = d["h1L_TR6J"]+OneEleSelection
configMgr.cutsDict["h1L_WR6JEl"] = d["h1L_WR6J"]+OneEleSelection
configMgr.cutsDict["h1L_TR6JMu"] = d["h1L_TR6J"]+OneMuoSelection
configMgr.cutsDict["h1L_WR6JMu"] = d["h1L_WR6J"]+OneMuoSelection

configMgr.cutsDict["h1L_TR3JEM"] = d["h1L_TR3J"]+OneLepSelection
configMgr.cutsDict["h1L_TR5JEM"] = d["h1L_TR5J"]+OneLepSelection
configMgr.cutsDict["h1L_TR6JEM"] = d["h1L_TR6J"]+OneLepSelection
configMgr.cutsDict["h1L_WR3JEM"] = d["h1L_WR3J"]+OneLepSelection
configMgr.cutsDict["h1L_WR5JEM"] = d["h1L_WR5J"]+OneLepSelection
configMgr.cutsDict["h1L_WR6JEM"] = d["h1L_WR6J"]+OneLepSelection

configMgr.cutsDict["h1L_VR3JhighMETEl"] = d["h1L_VR3JhighMET"]+OneEleSelection
configMgr.cutsDict["h1L_VR3JhighMETMu"] = d["h1L_VR3JhighMET"]+OneMuoSelection
configMgr.cutsDict["h1L_VR5JhighMETEl"] = d["h1L_VR5JhighMET"]+OneEleSelection
configMgr.cutsDict["h1L_VR5JhighMETMu"] = d["h1L_VR5JhighMET"]+OneMuoSelection
configMgr.cutsDict["h1L_VR6JhighMETEl"] = d["h1L_VR6JhighMET"]+OneEleSelection
configMgr.cutsDict["h1L_VR6JhighMETMu"] = d["h1L_VR6JhighMET"]+OneMuoSelection

configMgr.cutsDict["h1L_VR3JhighMTEl"] = d["h1L_VR3JhighMT"]+OneEleSelection
configMgr.cutsDict["h1L_VR3JhighMTMu"] = d["h1L_VR3JhighMT"]+OneMuoSelection
configMgr.cutsDict["h1L_VR5JhighMTEl"] = d["h1L_VR5JhighMT"]+OneEleSelection
configMgr.cutsDict["h1L_VR5JhighMTMu"] = d["h1L_VR5JhighMT"]+OneMuoSelection
configMgr.cutsDict["h1L_VR6JhighMTEl"] = d["h1L_VR6JhighMT"]+OneEleSelection
configMgr.cutsDict["h1L_VR6JhighMTMu"] = d["h1L_VR6JhighMT"]+OneMuoSelection

configMgr.cutsDict["h1L_VR3JhighMETEM"] = d["h1L_VR3JhighMET"]+OneLepSelection
configMgr.cutsDict["h1L_VR5JhighMETEM"] = d["h1L_VR5JhighMET"]+OneLepSelection
configMgr.cutsDict["h1L_VR6JhighMETEM"] = d["h1L_VR6JhighMET"]+OneLepSelection
configMgr.cutsDict["h1L_VR3JhighMTEM"] = d["h1L_VR3JhighMT"]+OneLepSelection
configMgr.cutsDict["h1L_VR5JhighMTEM"] = d["h1L_VR5JhighMT"]+OneLepSelection
configMgr.cutsDict["h1L_VR6JhighMTEM"] = d["h1L_VR6JhighMT"]+OneLepSelection

configMgr.cutsDict["h1L_SR3JEl"] = d["h1L_SR3J"]+OneEleSelection
configMgr.cutsDict["h1L_SR3JMu"] = d["h1L_SR3J"]+OneMuoSelection
configMgr.cutsDict["h1L_SR5JEl"] = d["h1L_SR5J"]+OneEleSelection
configMgr.cutsDict["h1L_SR5JMu"] = d["h1L_SR5J"]+OneMuoSelection
configMgr.cutsDict["h1L_SR6JEl"] = d["h1L_SR6J"]+OneEleSelection
configMgr.cutsDict["h1L_SR6JMu"] = d["h1L_SR6J"]+OneMuoSelection

configMgr.cutsDict["h1L_SR3JdiscoveryEl"] = d["h1L_SR3Jdiscovery"]+OneEleSelection
configMgr.cutsDict["h1L_SR3JdiscoveryMu"] = d["h1L_SR3Jdiscovery"]+OneMuoSelection
configMgr.cutsDict["h1L_SR5JdiscoveryEl"] = d["h1L_SR5Jdiscovery"]+OneEleSelection
configMgr.cutsDict["h1L_SR5JdiscoveryMu"] = d["h1L_SR5Jdiscovery"]+OneMuoSelection
configMgr.cutsDict["h1L_SR6JdiscoveryEl"] = d["h1L_SR6Jdiscovery"]+OneEleSelection
configMgr.cutsDict["h1L_SR6JdiscoveryMu"] = d["h1L_SR6Jdiscovery"]+OneMuoSelection

configMgr.cutsDict["h1L_SR3JEM"] = d["h1L_SR3J"]+OneLepSelection
configMgr.cutsDict["h1L_SR5JEM"] = d["h1L_SR5J"]+OneLepSelection
configMgr.cutsDict["h1L_SR6JEM"] = d["h1L_SR6J"]+OneLepSelection
configMgr.cutsDict["h1L_SR3JdiscoveryEM"] = d["h1L_SR3Jdiscovery"]+OneLepSelection
configMgr.cutsDict["h1L_SR5JdiscoveryEM"] = d["h1L_SR5Jdiscovery"]+OneLepSelection
configMgr.cutsDict["h1L_SR6JdiscoveryEM"] = d["h1L_SR6Jdiscovery"]+OneLepSelection

######################
## Lists of weights ##
######################

if usettbarreweighting:
    ttbarWbin = [0.99707,0.93957,0.73434,0.57014]
    ttbarReweight = "( 1 + (DatasetNumber==105860 || DatasetNumber==105861 || DatasetNumber==117050 )*((VecSumTTbarPt<40)*%e + (VecSumTTbarPt>40 && VecSumTTbarPt<170)*%e + (VecSumTTbarPt>170 && VecSumTTbarPt<340)*%e + (VecSumTTbarPt>340)*%e - 1) )" % (ttbarWbin[0],ttbarWbin[1],ttbarWbin[2],ttbarWbin[3])

    print ttbarReweight

    weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","bTagWeight[14]",ttbarReweight]
else:
    weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","bTagWeight[14]"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight[14]","bTagWeightBUp[14]")
    bTagLowWeights = replaceWeight(weights,"bTagWeight[14]","bTagWeightBDown[14]")

    cTagHighWeights = replaceWeight(weights,"bTagWeight[14]","bTagWeightCUp[14]")
    cTagLowWeights = replaceWeight(weights,"bTagWeight[14]","bTagWeightCDown[14]")

    mTagHighWeights = replaceWeight(weights,"bTagWeight[14]","bTagWeightMUp[14]")
    mTagLowWeights = replaceWeight(weights,"bTagWeight[14]","bTagWeightMDown[14]")

    bTagHighWeights_30 = replaceWeight(weights,"bTagWeight[14]","bTagWeightBUp[14]")
    bTagLowWeights_30 = replaceWeight(weights,"bTagWeight[14]","bTagWeightBDown[14]")

    cTagHighWeights_30 = replaceWeight(weights,"bTagWeight[14]","bTagWeightCUp[14]")
    cTagLowWeights_30 = replaceWeight(weights,"bTagWeight[14]","bTagWeightCDown[14]")

    mTagHighWeights_30 = replaceWeight(weights,"bTagWeight[14]","bTagWeightMUp[14]")
    mTagLowWeights_30 = replaceWeight(weights,"bTagWeight[14]","bTagWeightMDown[14]")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights,"leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

#########################
## List of systematics ##
#########################

# Signal XSec uncertainty as overallSys (pure yield affect) DEPRECATED
xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig","_NoSys","_JESup","_JESdown","tree","overallHistoSys")

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
#BGerr = {}
#BGerr_singletop = {}
#BGerr_diboson = {}
qfacW = {}
ktfacW = {}
iqoptW = {}
#qfacDY = {}
#ktfacDY = {}

#pdfIntraSyst = {}
#pdfInterSyst = {}
#PythAlpTTbarSyst = {}
PythSherpaTTbarSyst = {}
qfacZ = {}
ktfacZ = {}
ttbarZWXsec = {}
ttbarWWXsec = {}
DBWWXsec = {}
DBWZXsec = {}
#PowhegJimmyTTbarSyst = {}
SingleTopWtXsec = {}
SingleTopSXsec = {}
SingleTopTXsec ={}
SingleToptZXsec ={}

for region in CRregions:
 
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []

    # if "JES"     in SystList :basicChanSyst[region].append(Systematic("JES","_NoSys","_JESup","_JESdown","tree","overallNormHistoSys")) # JES uncertainty - for low pt jets
    
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EffectiveNP1","_NoSys","_EffectiveNP1up","_EffectiveNP1down","tree","overallNormHistoSys")) # JES uncertainty
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EffectiveNP2","_NoSys","_EffectiveNP2up","_EffectiveNP2down","tree","overallNormHistoSys")) # JES uncertainty
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EtaIntercalibrationModelling","_NoSys","_EtaIntercalibrationModellingup","_EtaIntercalibrationModellingdown","tree","overallNormHistoSys")) # JES uncertainty:
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_PileupRhoTopology","_NoSys","_PileupRhoTopologyup","_PileupRhoTopologydown","tree","overallNormHistoSys")) # JES uncertainty: 
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_FlavorCompUncert","_NoSys","_FlavorCompUncertup","_FlavorCompUncertdown","tree","overallNormHistoSys")) # JES uncertainty: 
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_FlavorResponseUncert","_NoSys","_FlavorResponseUncertup","_FlavorResponseUncertdown","tree","overallNormHistoSys")) # JES uncertainty: 
    if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_BJes","_NoSys","_BJesup","_BJesdown","tree","overallNormHistoSys")) # JES uncertainty: 
    
    if doSmallJESPars:
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EffectiveNP3","_NoSys","_EffectiveNP3up","_EffectiveNP3down","tree","overallNormHistoSys")) # JES uncertainty
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EffectiveNP4","_NoSys","_EffectiveNP4up","_EffectiveNP4down","tree","overallNormHistoSys")) # JES uncertainty
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EffectiveNP5","_NoSys","_EffectiveNP5up","_EffectiveNP5down","tree","overallNormHistoSys")) # JES uncertainty
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EffectiveNP6","_NoSys","_EffectiveNP6up","_EffectiveNP6down","tree","overallNormHistoSys")) # JES uncertainty: 
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_EtaIntercalibrationStatAndMethod","_NoSys","_EtaIntercalibrationStatAndMethodup","_EtaIntercalibrationStatAndMethoddown","tree","overallNormHistoSys")) # JES uncertainty: 
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_PileupOffsetMu","_NoSys","_PileupOffsetMuup","_PileupOffsetMudown","tree","overallNormHistoSys")) # JES uncertainty: 
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_PileupOffsetNPV","_NoSys","_PileupOffsetNPVup","_PileupOffsetNPVdown","tree","overallNormHistoSys")) # JES uncertainty: 
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_PileupPtTerm","_NoSys","_PileupPtTermup","_PileupPtTermdown","tree","overallNormHistoSys")) # JES uncertainty: 
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_SingleParticleHighPt","_NoSys","_SingleParticleHighPtup","_SingleParticleHighPtdown","tree","overallNormHistoSys")) # JES uncertainty: 
        if "JES" in SystList :basicChanSyst[region].append(Systematic("JES_RelativeNonClosureMCTYPE","_NoSys","_RelativeNonClosureMCTYPEup","_RelativeNonClosureMCTYPEdown","tree","overallNormHistoSys")) # JES uncertainty: 

  
    if "JER"     in SystList : basicChanSyst[region].append(Systematic("JER","_NoSys","_JER","_JER","tree","overallNormHistoSysOneSide")) #overallNormHistoSysOneSideSym
    #if "JER"     in SystList : basicChanSyst[region].append(Systematic("JER_"+region,"_NoSys","_JER","_JER","tree","overallNormHistoSysOneSide")) #overallNormHistoSysOneSideSym    
     	
    if "JVF" in SystList: 
        basicChanSyst[region].append(Systematic("h1L_JVF","_NoSys","_JVFup","_JVFdown","tree", "overallNormHistoSys"))  

    ## MET soft term resolution and scale
    if "ScaleSt" in SystList : basicChanSyst[region].append(Systematic("h1L_SCALEST","_NoSys","_SCALESTup","_SCALESTdown","tree","overallHistoSys")) #overallNormHistoSys
    if "ResoSt"  in SystList : basicChanSyst[region].append(Systematic( "h1L_RESOST","_NoSys","_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSide")) #overallNormHistoSysOneSideSym

    ## pile-up
    if "PileUp" in SystList : basicChanSyst[region].append( Systematic("h1L_pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys"))#"overallSys")) #overallNormHistoSys

    # Lepton weight uncertainty
    if "LepEff" in SystList : basicChanSyst[region].append( Systematic("h1L_LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys"))

    # Trigger efficiency
    if "LepTrig" in SystList:
        elChanSyst[region].append(Systematic("h1L_TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
        muChanSyst[region].append(Systematic("h1L_TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))

    # Electron energy scale uncertainty
    if "EES" in SystList:
        elChanSyst[region].append(Systematic("h1L_egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys"))
        elChanSyst[region].append(Systematic("h1L_egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallSys"))
        elChanSyst[region].append(Systematic("h1L_egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys"))
        elChanSyst[region].append(Systematic("h1L_eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys"))
        elChanSyst[region].append(Systematic("h1L_egres","_NoSys","_EGRESup","_EGRESdown","tree","overallSys"))

    # Muon energy resolutions
    if "MER" in SystList:
        muChanSyst[region].append(Systematic("h1L_LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallSys"))
        muChanSyst[region].append(Systematic("h1L_LRImu","_NoSys","_MIDup","_MIDdown","tree","overallSys")) 

    if "BTag" in SystList:
        bTagSyst[region] = Systematic("h1L_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region] = Systematic("h1L_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region] = Systematic("h1L_MisT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")


    #BGerr[region] = Systematic("h1L_errBG", configMgr.weights,1.3 ,0.7, "user","userOverallSys") ## ttbarV, SherpaZMassiveBC
    #BGerr_singletop[region] = Systematic("h1L_errST", configMgr.weights,1.5 ,0.5, "user","userOverallSys")
    #BGerr_diboson[region] = Systematic("h1L_errDB", configMgr.weights,1.5 ,0.5, "user","userOverallSys")



    # Generator Systematics
    generatorSyst = []

    # W+jets generator systematics
    qfacW[region]  = Systematic("h1L_qfacW",configMgr.weights,configMgr.weights+["qfacUpWeightW"],configMgr.weights+["qfacDownWeightW"],"weight","overallNormHistoSys")
    ktfacW[region] = Systematic("h1L_ktfacW",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","overallNormHistoSys")
    iqoptW[region] = Systematic("h1L_iqoptW",configMgr.weights,configMgr.weights+["iqopt2WeightW"],configMgr.weights+["iqopt3WeightW"],"weight","overallNormHistoSys")#

    
    # pdf uncertainties
    #pdfIntraSyst[region] = Systematic("h1L_pdfIntra",configMgr.weights,configMgr.weights+["pdfWeightVars[0]"],configMgr.weights+["pdfWeightVars[1]"],"weight","overallNormHistoSys")
    #pdfInterSyst[region] = Systematic("h1L_pdfInter",configMgr.weights,configMgr.weights+["pdfWeight"],configMgr.weights+["pdfWeight"],"weight","overallNormHistoSysOneSide") #overallNormHistoSysOneSideSym

## Pythia/Alpgen generator difference  ## ttbar
  #  if "PythAlpTTbar" in SystList:
   #     PythAlpTTbarSyst[region] = Systematic("h1L_PythAlpTTbar", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
    #    PythAlpTTbarSyst[region].setFileList("PowhegPythiaTTbar", AlpgenTTbarFiles)
     #   PythAlpTTbarSyst[region].setTreeName("PowhegPythiaTTbar", "AlpgenTTbar_NoSys")

## Pythia/Alpgen generator difference  ## ttbar
    if "PythSherpaTTbar" in SystList:
        PythSherpaTTbarSyst[region] = Systematic("h1L_PythSherpaTTbar", "_NoSys", "_NoSys", "_NoSys", "tree", "normHistoSysOneSideSym")
        PythSherpaTTbarSyst[region].setFileList("PowhegPythiaTTbar", SherpaTTbarFiles)
        PythSherpaTTbarSyst[region].setTreeName("PowhegPythiaTTbar", "SherpaTTbar_NoSys")

# Scale variation/matching in Z+jets
    if "GenZjets" in SystList:
        qfacZ[region]  = Systematic("h1L_qfacZ",configMgr.weights,configMgr.weights+["qfacUpWeightW"],configMgr.weights+["qfacDownWeightW"],"weight","histoSys")
        ktfacZ[region] = Systematic("h1L_ktfacZ",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","histoSys")

# theory uncertainties in ttbarV 
    if "GenTTbarV" in SystList:
        ttbarZWsel = "(DatasetNumber==119353 || DatasetNumber==174830 || DatasetNumber==174831 || DatasetNumber==119355 || DatasetNumber==174832 || DatasetNumber==174833)"
        ttbarWWsel = "(DatasetNumber==119583)"
        ttbarZWXsec[region] = Systematic("h1L_ttbarZWXsec", configMgr.weights,configMgr.weights+["( (0.22) * %s + 1 )" % ttbarZWsel],configMgr.weights+["( (-0.22) * %s + 1 )" % ttbarZWsel], "weight","overallSys")
        ttbarWWXsec[region] = Systematic("h1L_ttbarWWXsec", configMgr.weights,configMgr.weights+["( (0.5) * %s + 1 )" % ttbarWWsel],configMgr.weights+["( (-0.5) * %s + 1 )" % ttbarWWsel], "weight","overallSys")


# theory uncertainties in SingleTop 
    if "GenSingleTop" in SystList:
    #Wt channels
        SingleTopWt = "(DatasetNumber==110140)"
        SingleTopWtXsec[region] = Systematic("h1L_SingleTopWtXsec", configMgr.weights,configMgr.weights+["( (0.068) * %s + 1 )" % SingleTopWt],configMgr.weights+["( (-0.068) * %s + 1 )" % SingleTopWt], "weight","overallSys")
    #t channel
        SingleTopT = "(DatasetNumber==110101)"
        SingleTopTXsec[region] = Systematic("h1L_SingleTopTXsec", configMgr.weights,configMgr.weights+["( (0.039) * %s + 1 )" % SingleTopT],configMgr.weights+["( (-0.022) * %s + 1 )" % SingleTopT], "weight","overallSys")
    #s channels
        SingleTopS = "(DatasetNumber==110119)"
        SingleTopSXsec[region] = Systematic("h1L_SingleTopSXsec", configMgr.weights,configMgr.weights+["( (0.039) * %s + 1 )" % SingleTopS],configMgr.weights+["( (-0.039) * %s + 1 )" % SingleTopS], "weight","overallSys")
    #t+Z channels
        SingleToptZ = "(DatasetNumber==179991 || DatasetNumber==179992)"        
        SingleToptZXsec[region] = Systematic("h1L_SingleTopZXsec", configMgr.weights,configMgr.weights+["( (0.5) * %s + 1 )" % SingleToptZ],configMgr.weights+["( (-0.5) * %s + 1 )" % SingleToptZ], "weight","overallSys")
	
         
##Pythia/powhegJimmy generator
   # if "PowhegJimmyTTbar" in SystList:
    #    PowhegJimmyTTbarSyst[region] = Systematic("h1L_PowhegJimmyTTbar", "_NoSys", "_NoSys", "_NoSys", "tree", "normHistoSysOneSideSym")
     #   PowhegJimmyTTbarSyst[region].setFileList("PowhegPythiaTTbar", PowhegJimmyTTbarFiles)
      #  PowhegJimmyTTbarSyst[region].setTreeName("PowhegPythiaTTbar", "PowhegJimmyTTbar_NoSys")
        
    if "GenDB" in SystList:
        # both DB and DBMassiveBC
        dibosonWWZZsel = "(DatasetNumber==126892 || DatasetNumber==126894 || DatasetNumber==126895 || DatasetNumber==177997 || DatasetNumber==177999 || DatasetNumber==183586 || DatasetNumber==183588 || DatasetNumber==183590 || DatasetNumber==183734 || DatasetNumber==183736 || DatasetNumber==183738)"
        dibosonWZsel = "!%s" % dibosonWWZZsel
        DBWWXsec[region] = Systematic("h1L_DBWWXsec", configMgr.weights,configMgr.weights+["( (0.05) * %s + 1 )" % dibosonWWZZsel],configMgr.weights+["( (-0.05) * %s + 1 )" % dibosonWWZZsel], "weight","overallSys")
        DBWZXsec[region] = Systematic("h1L_DBWZXsec", configMgr.weights,configMgr.weights+["( (0.07) * %s + 1 )" % dibosonWZsel],configMgr.weights+["( (-0.07) * %s + 1 )" % dibosonWZsel], "weight","overallSys")

# theory uncertainties in Diboson
if "GenDB" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_DB2
    theoryUncertainties_hardLepton_Moriond2013_DB2.TheorUnc(generatorSyst)
    import theoryUncertainties_hardLepton_Moriond2013_PDFDB2
    theoryUncertainties_hardLepton_Moriond2013_PDFDB2.TheorUnc(generatorSyst)

# theory uncertainties in Z+jets 
if "GenZjets" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_Zjets2
    theoryUncertainties_hardLepton_Moriond2013_Zjets2.TheorUnc(generatorSyst)
    import theoryUncertainties_hardLepton_Moriond2013_PDFZjets2
    theoryUncertainties_hardLepton_Moriond2013_PDFZjets2.TheorUnc(generatorSyst)
     
# theory uncertainties in DY     
if "GenDY" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_DY2
    theoryUncertainties_hardLepton_Moriond2013_DY2.TheorUnc(generatorSyst)
    
# theory uncertainties in ttbarV 
if "GenTTbarV" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_ttbarV2
    theoryUncertainties_hardLepton_Moriond2013_ttbarV2.TheorUnc(generatorSyst)
    import theoryUncertainties_hardLepton_Moriond2013_PDFttbarV2
    theoryUncertainties_hardLepton_Moriond2013_PDFttbarV2.TheorUnc(generatorSyst)

# theory uncertainties in ttbar 
if "GenPowhegJimmyTTbar" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_PowhegPythia_PowhegJimmy2
    theoryUncertainties_hardLepton_Moriond2013_PowhegPythia_PowhegJimmy2.TheorUnc(generatorSyst)
    
# theory uncertainties in ttbar 
if "GenAlpgenJimmyTTbar" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_PowhegJimmy_AlpgenJimmy2
    theoryUncertainties_hardLepton_Moriond2013_PowhegJimmy_AlpgenJimmy2.TheorUnc(generatorSyst)
        
# theory uncertainties in SingleTop 
if "GenSingleTop" in SystList:
    import theoryUncertainties_hardLepton_Moriond2013_SingleTop2
    theoryUncertainties_hardLepton_Moriond2013_SingleTop2.TheorUnc(generatorSyst)
    import theoryUncertainties_hardLepton_Moriond2013_PDFSingleTop2
    theoryUncertainties_hardLepton_Moriond2013_PDFSingleTop2.TheorUnc(generatorSyst)
   
    
    

# theory uncertainties kept in separate file
#from theoryUncertainties_hardLepton_Moriond2013_single_wid import *
if ("GenW" in SystList) or ("GenTTbar" in SystList):
    import theoryUncertainties_hardLepton_Moriond2013_single_wid2
    if "GenW"   in SystList:
        theoryUncertainties_hardLepton_Moriond2013_single_wid2.TheorUncW(generatorSyst)
        import theoryUncertainties_hardLepton_Moriond2013_PDFWjets2
        theoryUncertainties_hardLepton_Moriond2013_PDFWjets2.TheorUnc(generatorSyst)
    if "GenTTbar" in SystList:
        theoryUncertainties_hardLepton_Moriond2013_single_wid2.TheorUncTT(generatorSyst)
        import theoryUncertainties_hardLepton_Moriond2013_PDFttbar2
        theoryUncertainties_hardLepton_Moriond2013_PDFttbar2.TheorUnc(generatorSyst)

#############
## Samples ##
#############

configMgr.nomName = "_NoSys"

WSampleName = "SherpaWMassiveBC"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("h1L_mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
#
TTbarSampleName = "PowhegPythiaTTbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("h1L_mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
#
DibosonsSampleName = "SherpaDibosonsMassiveBC"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
SingleTopSampleName = "SingleTop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "SherpaZMassiveBC"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
#

DYSampleName = "SherpaDY"
DYSample = Sample(DYSampleName,kBlue+4)
DYSample.setStatConfig(useStat)
DYSample.setNormByTheory()
#


#
ttbarVSampleName = "ttbarV"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
QCDSample = Sample("QCD",kYellow)
QCDSample.setFileList([inputDir_QCD+"HardEle_QCD.root",inputDir_QCD+"HardMuo_QCD.root"]) #_QCD.
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
#
DataSample = Sample("Data",kBlack)
#DataSample.setFileList([inputDir_data+"datatree_HardEle.root",inputDir_data+"datatree_HardMuo.root"])
DataSample.setFileList([inputDir_data+"HardEle_Data.root",inputDir_data+"HardMuo_Data.root"])
DataSample.setData()


################
# Bkg-only fit #
################

bkgOnly = configMgr.addFitConfig("bkgonly")
if not doSignalOnly:
    bkgOnly.addSamples([ttbarVSample,DibosonsSample,SingleTopSample,ZSample,DYSample,QCDSample,TTbarSample,WSample,DataSample])

if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.028)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
bReqChans["3J"] = []
bVetoChans["3J"] = []
bReqChans["5J"] = []
bVetoChans["5J"] = []
bReqChans["6J"] = []
bVetoChans["6J"] = []
bAgnosticChans["3J"] = []
bAgnosticChans["5J"] = []
bAgnosticChans["6J"] = []

#lepton flavor classification of channels
elChans = {}
muChans = {}
elChans["3J"] = []
muChans["3J"] = []
elChans["5J"] = []
muChans["5J"] = []
elChans["6J"] = []
muChans["6J"] = []

elmuChans = {}
elmuChans["3J"] = []
elmuChans["5J"] = []
elmuChans["6J"] = []


######################################################
# Add channels to Bkg-only configuration with exclusive single bin 3J,5J,6J regions              #
######################################################
    
nBins3JCR = 1  
nBins5JCR = 1  
nBins6JCR = 1  

#-----3JET--------#
if "3J" in CRregions:
    """
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR3JEl"],nBins3JCR,0.5,1.5),[elChans["3J"],bVetoChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR3JMu"],nBins3JCR,0.5,1.5),[muChans["3J"],bVetoChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR3JEl"],nBins3JCR,0.5,1.5),[elChans["3J"],bReqChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR3JMu"],nBins3JCR,0.5,1.5),[muChans["3J"],bReqChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    """
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR3JEM"],nBins3JCR,0.5,1.5),[elmuChans["3J"],bVetoChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR3JEM"],nBins3JCR,0.5,1.5),[elmuChans["3J"],bReqChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

#-----5JET--------#
if "5J" in CRregions:
    """
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR5JEl"],nBins5JCR,0.5,1.5),[elChans["5J"],bVetoChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR5JMu"],nBins5JCR,0.5,1.5),[muChans["5J"],bVetoChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR5JEl"],nBins5JCR,0.5,1.5),[elChans["5J"],bReqChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR5JMu"],nBins5JCR,0.5,1.5),[muChans["5J"],bReqChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    """
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR5JEM"],nBins5JCR,0.5,1.5),[elmuChans["5J"],bVetoChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR5JEM"],nBins5JCR,0.5,1.5),[elmuChans["5J"],bReqChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

#-----6JET--------#
if "6J" in CRregions:
    """
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR6JEl"],nBins6JCR,0.5,1.5),[elChans["6J"],bVetoChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR6JMu"],nBins6JCR,0.5,1.5),[muChans["6J"],bVetoChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR6JEl"],nBins6JCR,0.5,1.5),[elChans["6J"],bReqChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR6JMu"],nBins6JCR,0.5,1.5),[muChans["6J"],bReqChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    """
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_WR6JEM"],nBins6JCR,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["h1L_TR6JEM"],nBins6JCR,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    
    
######################################################
# Bkg-only configuration is finished.                #
# Move on with validation config from bkgOnly clone. #
######################################################
Met_Min = 90.
Met_Max = 200.
Met_bin = 22
Lep_Min = 0.
Lep_Max = 400.
Lep_bin = 20
Jet_Min = 0.
Jet_Max = 800.
Jet_bin = 20
Mt_Min = 35.
Mt_Max = 90.
Mt_bin = 22
W_Min = 100.
W_Max = 500.
W_bin = 20

meffNBinsSR = 4
meffBinLowSR = 800.
meffBinHighSR = 1600.

metNBinsSR = 3
metBinLowSR = 250.
metBinHighSR = 550.

meffMax=100000.
metMax=100000.

ValidRegList["OneLep"]  = ValidRegList["VR3JhighMET"] or ValidRegList["VR3JhighMT"] or ValidRegList["VR5JhighMET"] or ValidRegList["VR5JhighMT"] or ValidRegList["VR6JhighMET"] or ValidRegList["VR6JhighMT"]

validation = None
if doTableInputs or ValidRegList["SRTight"] or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans["3J"])
        appendIfMatchName(c,bVetoChans["3J"])
        appendIfMatchName(c,bAgnosticChans["3J"])
        appendIfMatchName(c,elChans["3J"])
        appendIfMatchName(c,muChans["3J"])
        appendIfMatchName(c,bReqChans["5J"])
        appendIfMatchName(c,bVetoChans["5J"])
        appendIfMatchName(c,bAgnosticChans["5J"])
        appendIfMatchName(c,elChans["5J"])
        appendIfMatchName(c,muChans["5J"])	
        appendIfMatchName(c,bReqChans["6J"])
        appendIfMatchName(c,bVetoChans["6J"])
        appendIfMatchName(c,bAgnosticChans["6J"])
        appendIfMatchName(c,elChans["6J"])
        appendIfMatchName(c,muChans["6J"])
        appendIfMatchName(c,elmuChans["3J"])
        appendIfMatchName(c,elmuChans["5J"])
        appendIfMatchName(c,elmuChans["6J"])

    if ValidRegList["SRTight"] or doTableInputs:
        if doTableInputs:
            if "5J" in CRregions:
                appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR5JEl"],1,0.,meffMax),[bAgnosticChans["5J"],elChans["5J"]])
                appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR5JMu"],1,0.,meffMax),[bAgnosticChans["5J"],muChans["5J"]])
                appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR5JEM"],1,0.,meffMax),[bAgnosticChans["5J"],elmuChans["5J"]])
            if "3J" in CRregions:
                appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR3JEl"],1,0.,meffMax),[bAgnosticChans["3J"],elChans["3J"]])
                appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR3JMu"],1,0.,meffMax),[bAgnosticChans["3J"],muChans["3J"]]) 
                appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR3JEM"],1,0.,meffMax),[bAgnosticChans["3J"],elmuChans["3J"]])
            if "6J" in CRregions: 
                appendTo(validation.addValidationChannel("met",["h1L_SR6JEl"],1,0.,metMax),[bAgnosticChans["6J"],elChans["6J"]])
                appendTo(validation.addValidationChannel("met",["h1L_SR6JMu"],1,0.,metMax),[bAgnosticChans["6J"],muChans["6J"]])
                appendTo(validation.addValidationChannel("met",["h1L_SR6JEM"],1,0.,metMax),[bAgnosticChans["6J"],elmuChans["6J"]])
            if "5J" in CRregions:
                appendTo(validation.addValidationChannel("cuts",["h1L_SR5JdiscoveryEl"],1,0.5,1.5),[bAgnosticChans["5J"],elChans["5J"]])
                appendTo(validation.addValidationChannel("cuts",["h1L_SR5JdiscoveryMu"],1,0.5,1.5),[bAgnosticChans["5J"],muChans["5J"]])
                appendTo(validation.addValidationChannel("cuts",["h1L_SR5JdiscoveryEM"],1,0.5,1.5),[bAgnosticChans["5J"],elmuChans["5J"]])
            if "3J" in CRregions:
                appendTo(validation.addValidationChannel("cuts",["h1L_SR3JdiscoveryEl"],1,0.5,1.5),[bAgnosticChans["3J"],elChans["3J"]])
                appendTo(validation.addValidationChannel("cuts",["h1L_SR3JdiscoveryMu"],1,0.5,1.5),[bAgnosticChans["3J"],muChans["3J"]])
                appendTo(validation.addValidationChannel("cuts",["h1L_SR3JdiscoveryEM"],1,0.5,1.5),[bAgnosticChans["3J"],elmuChans["3J"]])
            if "6J" in CRregions:
                appendTo(validation.addValidationChannel("cuts",["h1L_SR6JdiscoveryEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
                appendTo(validation.addValidationChannel("cuts",["h1L_SR6JdiscoveryMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
                appendTo(validation.addValidationChannel("cuts",["h1L_SR6JdiscoveryEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
        else:
            SR_channels0=[]
            if "3J" in CRregions:
                tmp = appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR3JEl"],4,meffBinLowSR,meffBinHighSR),[bAgnosticChans["3J"],elChans["3J"]])
                SR_channels0.append(tmp)
                tmp = appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR3JMu"],4,meffBinLowSR,meffBinHighSR),[bAgnosticChans["3J"],muChans["3J"]])
                SR_channels0.append(tmp)
                tmp = appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR3JEM"],4,meffBinLowSR,meffBinHighSR),[bAgnosticChans["3J"],elmuChans["3J"]])
                SR_channels0.append(tmp)
            if "5J" in CRregions:
                tmp = appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR5JEl"],4,meffBinLowSR,meffBinHighSR),[bAgnosticChans["5J"],elChans["5J"]])
                SR_channels0.append(tmp)
                tmp = appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR5JMu"],4,meffBinLowSR,meffBinHighSR),[bAgnosticChans["5J"],muChans["5J"]])
                SR_channels0.append(tmp)
                tmp = appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",["h1L_SR5JEM"],4,meffBinLowSR,meffBinHighSR),[bAgnosticChans["5J"],elmuChans["5J"]])
                SR_channels0.append(tmp)
            if "6J" in CRregions:
                tmp = appendTo(validation.addValidationChannel("met",["h1L_SR6JEl"],3,metBinLowSR,metBinHighSR),[bAgnosticChans["6J"],elChans["6J"]])
                SR_channels0.append(tmp)
                tmp = appendTo(validation.addValidationChannel("met",["h1L_SR6JMu"],3,metBinLowSR,metBinHighSR),[bAgnosticChans["6J"],muChans["6J"]])
                SR_channels0.append(tmp)
                tmp = appendTo(validation.addValidationChannel("met",["h1L_SR6JEM"],3,metBinLowSR,metBinHighSR),[bAgnosticChans["6J"],elmuChans["6J"]])
                SR_channels0.append(tmp)
            for item in SR_channels0:
                item.useOverflowBin=True

    if ValidRegList["OneLep"] or doTableInputs:

        # Validation plots
        # Binning
        # nBins, min, max
        if doTableInputs:
            # In table inputs mode, all bins are summed up to 1bin.
            meffBinsVR     = ( 1,   0., meffMax)
            pass
        else:
            metBinsVR     = (20,   200.,600.)
            meffBinsVR    = (15,   500.,2000.)
            lep1PtBinsVR  = (25,   0.,500.)
            WptBinsVR     = (25,   0.,500.)
            njets         = (10,   0.,10.)
            nbjets         = (10,   0.,10.)
            jet1ptBinsVR  = (21,   80.,500.)
            mtBinsVR      = (23,   40.,500.)
            metmeff =  (20, 0.,1.)
            pass
        
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        if ValidRegList["VR3JhighMET"] : 
            ProcessRegions.append(["h1L_VR3JhighMET" ,bAgnosticChans["3J"]])
        if ValidRegList["VR3JhighMT"] : 
            ProcessRegions.append(["h1L_VR3JhighMT" ,bAgnosticChans["3J"]])
        if ValidRegList["VR5JhighMET"] : 
            ProcessRegions.append(["h1L_VR5JhighMET" ,bAgnosticChans["5J"]])
        if ValidRegList["VR5JhighMT"] : 
            ProcessRegions.append(["h1L_VR5JhighMT" ,bAgnosticChans["5J"]])     
        if ValidRegList["VR6JhighMET"] : 
            ProcessRegions.append(["h1L_VR6JhighMET" ,bAgnosticChans["6J"]])
        if ValidRegList["VR6JhighMT"] : 
            ProcessRegions.append(["h1L_VR6JhighMT" ,bAgnosticChans["6J"]]) 

        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            sys_region = ""
            if "3J" in CutPrefix: sys_region = "3J"
            elif "5J" in CutPrefix: sys_region = "5J"
            elif "6J" in CutPrefix: sys_region = "6J"
            else: 
                print "Unknown region! - Take systematics from 3J regions."
                sys_region = "3J"
            #for chan in [["El",elChans],["Mu",muChans],["EM",elmuChans]]:
            for chan in [["EM",elmuChans]]:
                ChanSuffix = chan[0]
                FlavorList_pre = chan[1]
                FlavorList = FlavorList_pre[sys_region]
                if not doTableInputs:
                    if '3J' in reg[0]:
                        appendTo(validation.addValidationChannel("met/meff3Jet30_JVF25pt50",[CutPrefix+ChanSuffix],  metmeff[0],  metmeff[1],  metmeff[2]),[bChanKind,FlavorList])
                    else:
                        appendTo(validation.addValidationChannel("met/meffInc30_JVF25pt50",[CutPrefix+ChanSuffix],  metmeff[0],  metmeff[1],  metmeff[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("meffInc30_JVF25pt50",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
                    if ("MET" in reg[0]) or ("INCL" in reg[0]):
                        appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
                    if ("MT" in reg[0]) or ("INCL" in reg[0]):
                        appendTo(validation.addValidationChannel("mt"    ,[CutPrefix+ChanSuffix],   mtBinsVR[0],   mtBinsVR[1],   mtBinsVR[2]),[bChanKind,FlavorList])
                else:
                    if ("VR" in reg[0]) or ("INCL" in reg[0]):
                        appendTo(validation.addValidationChannel("cuts",[CutPrefix+ChanSuffix],   1,   0.5, 1.5),[bChanKind,FlavorList])

#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    # SRs=["h1L_SR3JEl","h1L_SR3JMu","h1L_SR5JEl","h1L_SR5JMu","h1L_SR6JEl","h1L_SR6JMu"]
    SRs=["h1L_SR3JEM","h1L_SR5JEM","h1L_SR6JEM"]

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans["3J"])
            appendIfMatchName(c,bVetoChans["3J"])
            appendIfMatchName(c,bAgnosticChans["3J"])
            appendIfMatchName(c,elChans["3J"])
            appendIfMatchName(c,muChans["3J"])
            appendIfMatchName(c,bReqChans["5J"])
            appendIfMatchName(c,bVetoChans["5J"])
            appendIfMatchName(c,bAgnosticChans["5J"])
            appendIfMatchName(c,elChans["5J"])
            appendIfMatchName(c,muChans["5J"])
            appendIfMatchName(c,bReqChans["6J"])
            appendIfMatchName(c,bVetoChans["6J"])
            appendIfMatchName(c,bAgnosticChans["6J"])
            appendIfMatchName(c,elChans["6J"])
            appendIfMatchName(c,muChans["6J"])
            appendIfMatchName(c,elmuChans["3J"])
            appendIfMatchName(c,elmuChans["5J"])
            appendIfMatchName(c,elmuChans["6J"])

        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        #        sigSample.addSystematic(jesSignal)
        sigSample.addSystematic(xsecSig)
        if sig.startswith("SM"):
            # ISR uncertainty (SS and GG grids)
            massSet = sig.split('_')
            if len(massSet) != 5:
                print "Unrecognized point format! - ISR uncertainties will be wrong."
            if '2CNsl' in sig:
                DeltaM = 2*(float(massSet[-3]) - float(massSet[-2]))
            else:
                DeltaM = float(massSet[-3]) - float(massSet[-1])

            if DeltaM<=0: 
                log.fatal("Invalid value of DeltaM : %f" % DeltaM)
                
            if 'GG' in sig: 
                eisr3 = exp(-1.4-0.013*DeltaM)
                eisr5 = exp(-1.2-0.005*DeltaM)
                if eisr3<0.06: eisr3=0.06
                if eisr5<0.06: eisr5=0.06
                pass
            elif 'SS' in sig: 
                eisr3 = 0.06+exp(0.8-0.1*DeltaM)
                eisr5 = 0.06+exp(-1.5-0.005*DeltaM)
                pass
            else: 
                eisr3 = exp(-1.4-0.013*DeltaM)
                eisr5 = exp(-1.2-0.005*DeltaM)
                if eisr3<0.06: eisr3=0.06
                if eisr5<0.06: eisr5=0.06
                pass	
            print eisr3, eisr5
            isr3j = Systematic("isr3j", configMgr.weights, 1.+eisr3, 1.00-eisr3, "user", "userOverallSys")
            isr5j = Systematic("isr5j", configMgr.weights, 1.+eisr5, 1.00-eisr5, "user", "userOverallSys")
            pass
            
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if ValidRegList["SRTight"] or doTableInputs:
                #don't re-create already existing channel, but unset as Validation and set as Signal channel
                #Not working at the moment
                channame = "meffInc30_JVF25pt50_" +sr
                if channame in myTopLvl.channels:
                    ch = myTopLvl.getChannel("meffInc30_JVF25pt50",[sr])
                    iPop=myTopLvl.validationChannels.index(sr+"_meffInc30_JVF25pt50")
                    myTopLvl.validationChannels.pop(iPop)
            else:            
                if sr=="h1L_SR5JEl" or sr=="h1L_SR5JMu" or sr=="h1L_SR3JEl" or sr=="h1L_SR3JMu" or sr=="h1L_SR5JEM" or sr=="h1L_SR3JEM":
                    ch = myTopLvl.addChannel("meffInc30_JVF25pt50",[sr],meffNBinsSR,meffBinLowSR,meffBinHighSR)
                elif sr=="h1L_SR6JEl" or sr=="h1L_SR6JMu"or sr=="h1L_SR6JEM":
                    ch = myTopLvl.addChannel("met",[sr],metNBinsSR,metBinLowSR,metBinHighSR) 
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                
                
                if sr=="h1L_SR3JEl": 
                    elChans["3J"].append(ch)
                    bAgnosticChans["3J"].append(ch)
                elif sr=="h1L_SR5JEl": 
                    elChans["5J"].append(ch) 
                    bAgnosticChans["5J"].append(ch)
                elif sr=="h1L_SR6JEl": 
                    elChans["6J"].append(ch) 
                    bAgnosticChans["6J"].append(ch)       
                elif sr=="h1L_SR3JMu": 
                    muChans["3J"].append(ch)
                    bAgnosticChans["3J"].append(ch)
                elif sr=="h1L_SR5JMu": 
                    muChans["5J"].append(ch) 
                    bAgnosticChans["5J"].append(ch)
                elif sr=="h1L_SR6JMu": 
                    muChans["6J"].append(ch) 
                    bAgnosticChans["6J"].append(ch)
                elif sr=="h1L_SR3JEM":
                    elmuChans["3J"].append(ch)
                    bAgnosticChans["3J"].append(ch)
                elif sr=="h1L_SR5JEM":
                    elmuChans["5J"].append(ch)
                    bAgnosticChans["5J"].append(ch)
                elif sr=="h1L_SR6JEM":
                    elmuChans["6J"].append(ch)
                    bAgnosticChans["6J"].append(ch)
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                pass
                ''' 
                if 'SM' in sig:
                    if sr=="h1L_SR3JEl" or sr=="h1L_SR3JMu"or sr=="h1L_SR3JEM":
                        ch.getSample(sig).addSystematic(isr3j)
                    else:
                        ch.getSample(sig).addSystematic(isr5j)
                    ch.getSample(sig).mergeOverallSysSet = ['SigXSec','isr'] ## post-processing
                '''
            
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.useOverflowBin=True # -AK
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)
        

##############################
# Finalize fit configs setup #
##############################

AllChannels = {}
for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
AllChannels_all = bReqChans["5J"] + bVetoChans["5J"] + bAgnosticChans["3J"] + bAgnosticChans["5J"] + bAgnosticChans["6J"] + bReqChans["3J"] + bVetoChans["3J"] + bReqChans["6J"] + bVetoChans["6J"] 

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan = tgt[1]
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
        if tgtchan=="All" or tgtchan in chan.name:
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))	    

# Only adding the el, mu systematics to the ttbar and W+jets samples + adding other systematics to ttbar and W+jets samples
for region in CRregions:
    SetupChannels_Hack(elChans[region],bgdFiles_e, elChanSyst[region])
    SetupChannels_Hack(muChans[region],bgdFiles_m, muChanSyst[region])
    SetupChannels_nofiles(elChans[region]+muChans[region],basicChanSyst[region])
    SetupChannels_Hack(elmuChans[region],bgdFiles_em, elChanSyst[region])
    SetupChannels_nofiles(elmuChans[region], muChanSyst[region])
    SetupChannels_nofiles(elmuChans[region], basicChanSyst[region])

    for chan in AllChannels[region]:
        chan.getSample(WSampleName).addSystematic(qfacW[region])
        chan.getSample(WSampleName).addSystematic(ktfacW[region])
        #chan.getSample(WSampleName).addSystematic(iqoptW[region])
        if "GenZjets" in SystList: 
            chan.getSample(ZSampleName).addSystematic(qfacZ[region])
            chan.getSample(ZSampleName).addSystematic(ktfacZ[region])
    
	    
    #for chan in AllChannels[region]:
    #    chan.getSample(DibosonsSampleName).setFileList([inputDir_DB+"bugFixedDibosons_HardEle.root",inputDir_DB+"bugFixedDibosons_HardMuo.root"])
    """
    for chan in AllChannels[region]:
        # ttbar
        chan.getSample(TTbarSampleName).addSystematic(pdfIntraSyst[region])
        chan.getSample(TTbarSampleName).addSystematic(pdfInterSyst[region])
        chan.getSample(TTbarSampleName).mergeOverallSysSet = [pdfIntraSyst[region].name,pdfInterSyst[region].name] ## post-processing
        # W+jets
        chan.getSample(WSampleName).addSystematic(pdfIntraSyst[region])
        chan.getSample(WSampleName).addSystematic(pdfInterSyst[region])
        chan.getSample(WSampleName).mergeOverallSysSet =[pdfIntraSyst[region].name,pdfInterSyst[region].name]  ## post-processing
        meas.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)
        if validation:
            meas_valid  = validation.getMeasurement("BasicMeasurement")
            meas_valid.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)
        if myFitType==FitType.Exclusion:
            for sig in sigSamples:
                meas_excl=configMgr.getFitConfig("Sig_%s"%sig).getMeasurement("BasicMeasurement") 
                meas_excl.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)  
    """ 
######################################################
# Add separate Normalization Factors for ttbar and W+jets for each CR                               #
#   destroying the connection by nJet shape from MC                                                             #
######################################################
if useNJetNormFac:
    
    for chan in AllChannels_all:
        mu_W_Xj = "h1L_mu_W_XJ"
        mu_Top_Xj = "h1L_mu_Top_XJ"
        if "3J" in chan.name:
            mu_W_Xj = "h1L_mu_W_3J"
            mu_Top_Xj = "h1L_mu_Top_3J"
        elif "5J" in chan.name:
            mu_W_Xj = "h1L_mu_W_5J"
            mu_Top_Xj = "h1L_mu_Top_5J"
        elif "6J" in chan.name:
            mu_W_Xj = "h1L_mu_W_6J"
            mu_Top_Xj = "h1L_mu_Top_6J"
        else:
            log.warning("Channel %s gets no nJet separated normalization factor" % chan.name)
        chan.getSample(WSampleName).addNormFactor(mu_W_Xj,1.,3.,0.1)
        chan.getSample(TTbarSampleName).addNormFactor(mu_Top_Xj,1.,3.,0.1)
        log.info("Adding additional nJet normalization factors (%s, %s) to channel (%s)" %(mu_W_Xj, mu_Top_Xj, chan.name))

    meas.addParamSetting("h1L_mu_W","const",1.0)
    meas.addParamSetting("h1L_mu_Top","const",1.0)

    if validation:
        meas_valid  = validation.getMeasurement("BasicMeasurement")
        meas_valid.addParamSetting("h1L_mu_W","const",1.0)
        meas_valid.addParamSetting("h1L_mu_Top","const",1.0)
        #meas.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)

        
    if myFitType==FitType.Exclusion:
        for sig in sigSamples:
            meas_excl=configMgr.getFitConfig("Sig_%s"%sig).getMeasurement("BasicMeasurement")
            meas_excl.addParamSetting("h1L_mu_W","const",1.0)
            meas_excl.addParamSetting("h1L_mu_Top","const",1.0)
     
        
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig) 
            sys_region = ""
            if "3J" in chan.name: sys_region = "3J"
            elif "5J" in chan.name: sys_region = "5J"
            elif "6J" in chan.name: sys_region = "6J"
            else: 
                print "Unknown region! - Take systematics from 3J regions."
                sys_region = "3J"
            for syst in basicChanSyst[sys_region]:
                theSample.addSystematic(syst)   
                #            theSample.removeSystematic("JES")
            if not any("JVF" in f for f in sigFiles_em):
                theSample.removeSystematic("h1L_JVF")
            theSigFiles=[]
            if chan in elChans["3J"]+elChans["5J"]+elChans["6J"]:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans["3J"]+muChans["5J"]+muChans["6J"]:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans["3J"]+elmuChans["5J"]+elmuChans["6J"]:
                theSigFiles = sigFiles_em[sig]

            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        chan.hasBQCD = True
        #chan.addSystematic(bTagSyst)
        if "BTag" in SystList:
            for bg in ["PowhegPythiaTTbar","SherpaWMassiveBC","ttbarV","SingleTop","SherpaDibosonsMassiveBC","SherpaZMassiveBC"]:
                chan.getSample(bg).addSystematic(bTagSyst[region])
                chan.getSample(bg).addSystematic(cTagSyst[region])    
                chan.getSample(bg).addSystematic(mTagSyst[region])


    for chan in bVetoChans[region]:
        chan.hasBQCD = False
        if "BTag" in SystList:
            for bg in ["PowhegPythiaTTbar","SherpaWMassiveBC","ttbarV","SingleTop","SherpaDibosonsMassiveBC","SherpaZMassiveBC"]:
                chan.getSample(bg).addSystematic(bTagSyst[region])
                chan.getSample(bg).addSystematic(cTagSyst[region])    
                chan.getSample(bg).addSystematic(mTagSyst[region])	
 

    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        #chan.getSample("SherpaZMassiveBC").addSystematic(BGerr[region])
        #chan.getSample("SingleTop").addSystematic(BGerr_singletop[region])
        #chan.getSample("SherpaDibosons").addSystematic(BGerr_diboson[region])
        #chan.getSample("ttbarV").addSystematic(BGerr[region])
        if "GenTTbarV" in SystList:
            chan.getSample("ttbarV").addSystematic(ttbarZWXsec[region])
            chan.getSample("ttbarV").addSystematic(ttbarWWXsec[region])
        if "GenSingleTop" in SystList:
            chan.getSample("SingleTop").addSystematic(SingleTopSXsec[region])
            chan.getSample("SingleTop").addSystematic(SingleTopWtXsec[region])
            chan.getSample("SingleTop").addSystematic(SingleTopTXsec[region])
            chan.getSample("SingleTop").addSystematic(SingleToptZXsec[region])
	              
        if "GenDB" in SystList:
            chan.getSample("SherpaDibosonsMassiveBC").addSystematic(DBWWXsec[region])
            chan.getSample("SherpaDibosonsMassiveBC").addSystematic(DBWZXsec[region])
        #if "PythAlpTTbar" in SystList: chan.getSample("PowhegPythiaTTbar").addSystematic(PythAlpTTbarSyst[region])
        if "PythSherpaTTbar" in SystList: chan.getSample("PowhegPythiaTTbar").addSystematic(PythSherpaTTbarSyst[region])
        #if "PowhegJimmyTTbar" in SystList: chan.getSample("PowhegPythiaTTbar").addSystematic(PowhegJimmyTTbarSyst[region])
	#chan.getSample("PowhegPythiaTTbar").setNormRegions([("h1L_TR"+region+"El","cuts"),("h1L_TR"+region+"Mu","cuts"),("h1L_WR"+region+"El","cuts"),("h1L_WR"+region+"Mu","cuts")])
        #chan.getSample("SherpaWMassiveBC").setNormRegions([("h1L_WR"+region+"El","cuts"),("h1L_WR"+region+"Mu","cuts"),("h1L_TR"+region+"El","cuts"),("h1L_TR"+region+"Mu","cuts")])
        chan.getSample("PowhegPythiaTTbar").setNormRegions([("h1L_WR"+region+"EM","cuts"),("h1L_TR"+region+"EM","cuts")])
        chan.getSample("SherpaWMassiveBC").setNormRegions([("h1L_WR"+region+"EM","cuts"),("h1L_TR"+region+"EM","cuts")])
        chan.getSample("ttbarV").setNormRegions([("h1L_WR"+region+"EM","cuts"),("h1L_TR"+region+"EM","cuts")])
        chan.getSample("SingleTop").setNormRegions([("h1L_WR"+region+"EM","cuts"),("h1L_TR"+region+"EM","cuts")])		
        chan.getSample("SherpaDibosonsMassiveBC").setNormRegions([("h1L_WR"+region+"EM","cuts"),("h1L_TR"+region+"EM","cuts")])
        chan.getSample("SherpaZMassiveBC").setNormRegions([("h1L_WR"+region+"EM","cuts"),("h1L_TR"+region+"EM","cuts")])
	
for chan in (bAgnosticChans["3J"]+bAgnosticChans["5J"]+bAgnosticChans["6J"]):
    chan.hasBQCD = False
    chan.removeWeight("bTagWeight[14]")
           
#######################
## Cosmetic Settings ##
#######################
# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","t#bar{t} Powheg","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","DY","lf")
entry.SetLineColor(DYSample.color)
entry.SetFillColor(DYSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Dibosons","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Multijet","lf")
entry.SetLineColor(QCDSample.color)
entry.SetFillColor(QCDSample.color)
entry.SetFillStyle(compFillStyle)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation : validation.tLegend = leg
if myFitType==FitType.Exclusion: 
    entry = leg.AddEntry("","Signal","lf")
    entry.SetLineColor(sigSample.color)
    entry.SetFillColor(sigSample.color)
    entry.SetFillStyle(compFillStyle)
    myTopLvl.tLegend = leg
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion: chan.logY = True
    if chan.logY:
        chan.minY = 0.02
        chan.maxY = 1000000
    else:
        chan.minY = 0.05 
        chan.maxY = 3000
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Entries"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.15
            chan.ATLASLabelY = 0.83
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True


