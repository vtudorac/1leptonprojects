################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #


# ********************************************************************* #
#                              Main part
# ********************************************************************* #
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

useHardLepCR=True
useStat=True
doExclusion_mSUGRA=False

SystList=[]
#SystList.append("JES")      # Jet Energy Scale (common)
#SystList.append("JER")      # Jet Energy Resolution (common)
#SystList.append("LepEff")   # Lepton efficiency (e&m)
#SystList.append("LepTrig")  # Trigger efficiency (e&m)
#SystList.append("GenW")     # Generator Systematics W    (common)
#SystList.append("GenTTbar") # Generator Systematics TTbar(common)
#if not doExclusion_mSUGRA:
#SystList.append("ResoSt")   # Resolution of SoftTerm (common)
#SystList.append("ScaleSt")  # Scale of SoftTerm (common)
#SystList.append("EES")      # Electron Energy Scale (e only)
#SystList.append("MER")      # Muon Energy Resolution (m only)

doTableInputs=False #This effectively means no validation plots but only validation tables (but is 100x faster)
ValidRegList={}
ValidRegList["SRTight"] = False
ValidRegList["OneLep1"] = False
ValidRegList["OneLep2"] = False
ValidRegList["OneLep3"] = False
ValidRegList["OneLep4"] = False


doDiscovery=True
#doExclusion_mSUGRA=True
doSignalOnly=False #Remove all bkgs for signal histo creation step
if configMgr.executeHistFactory:
    doSignalOnly=False
    
if not 'sigSamples' in dir():
    sigSamples=["SU_400_500_0_10_P"]

# First define HistFactory attributes
configMgr.analysisName = "OneLeptonModirond2013" # Name to give the analysis
configMgr.outputFileName = "results/OneLeptonMoriond2013.root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 14.3
configMgr.setLumiUnits("fb-1")

#configMgr.doHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

#Split bdgFiles per channel
sigFiles_e = []
sigFiles_m = []
inputDir="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_1/"
inputDirSig="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v3_2/"
#inputDir="/data/maxi101/.tmp/ysasaki/Group/2012_11_09_v3_02/treeMaker/v3_2/"
#inputDirSig="/data/maxi101/.tmp/ysasaki/Group/2012_11_09_v3_02/treeMaker/v3_2/"

# Set the files to read from
bgdFiles_e = [inputDir+"bkgtree_HardEle.root",inputDir+"datatree_HardEle.root"]
bgdFiles_m = [inputDir+"bkgtree_HardMuo.root",inputDir+"datatree_HardMuo.root"]
if doExclusion_mSUGRA:
    sigFiles_e+=[inputDirSig+"sigtree_HardEle_SU.root"]
    sigFiles_m+=[inputDirSig+"sigtree_HardMuo_SU.root"]

# Map regions to cut strings
configMgr.cutsDict["WR"]="lep2Pt<10 && met>100 && met<180 && mt>100 && jet4Pt>80 && meffInc30>500 && nB4Jet30==0"
configMgr.cutsDict["TR"]="lep2Pt<10 && met>100 && met<180 && mt>100 && jet4Pt>80 && meffInc30>500 && nB4Jet30>0"

configMgr.cutsDict["TWR"]="lep2Pt<10 && met>100 && met<180 && mt > 100 && jet4Pt>80 && meffInc30>500"

configMgr.cutsDict["VR1"]="lep2Pt<10 && met>180 && met<250 && mt>100 && jet2Pt > 80 && jet4Pt> 40 && meffInc30>500"
configMgr.cutsDict["VR2"]="lep2Pt<10 && met>100 && met<250 && mt>80 && mt<100 && jet2Pt > 80 && jet4Pt> 40 && meffInc30>500"
configMgr.cutsDict["VR3"]="lep2Pt<10 && met>100 && met<180 && mt>40 && mt < 80 && jet2Pt > 80 && jet4Pt> 40 && meffInc30>500"
configMgr.cutsDict["VR4"]="lep2Pt<10 && met>180 && met<250 && mt>100 && jet2Pt > 80 && jet4Pt> 40 && met/meff4Jet30>0.2 && meffInc30>800"

configMgr.cutsDict["SR4jT"]="lep2Pt<10 && met>250 && mt>100 && jet4Pt>80 && met/meff4Jet30>0.2 && meffInc30>800"

d=configMgr.cutsDict
OneEleSelection = "&& AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && NumOfBaseline==1 && NumOfSignal==1"
OneMuoSelection = "&& AnalysisType==2 && ( EF_mu24_j65_a4tchad_EFxe40_tclcw ) && NumOfBaseline==1 && NumOfSignal==1"
configMgr.cutsDict["TREl"] = d["TR"]+OneEleSelection
configMgr.cutsDict["WREl"] = d["WR"]+OneEleSelection
configMgr.cutsDict["TRMu"] = d["TR"]+OneMuoSelection
configMgr.cutsDict["WRMu"] = d["WR"]+OneMuoSelection

configMgr.cutsDict["TWREl"]=d["TWR"]+OneEleSelection
configMgr.cutsDict["TWRMu"]=d["TWR"]+OneMuoSelection

configMgr.cutsDict["TVR1"] = d["VR1"]+"&& nB4Jet30>0"
configMgr.cutsDict["TVR1El"] = d["TVR1"]+OneEleSelection
configMgr.cutsDict["TVR1Mu"] = d["TVR1"]+OneMuoSelection

configMgr.cutsDict["TVR2"] = d["VR2"]+"&& nB4Jet30>0"
configMgr.cutsDict["TVR2El"] = d["TVR2"]+OneEleSelection
configMgr.cutsDict["TVR2Mu"] = d["TVR2"]+OneMuoSelection

configMgr.cutsDict["TVR3"] = d["VR3"]+"&& nB4Jet30>0"
configMgr.cutsDict["TVR3El"] = d["TVR3"]+OneEleSelection
configMgr.cutsDict["TVR3Mu"] = d["TVR3"]+OneMuoSelection

configMgr.cutsDict["WVR1"] = d["VR1"]+"&& nB4Jet30==0"
configMgr.cutsDict["WVR1El"] = d["WVR1"]+OneEleSelection
configMgr.cutsDict["WVR1Mu"] = d["WVR1"]+OneMuoSelection

configMgr.cutsDict["WVR2"] = d["VR2"]+"&& nB4Jet30==0"
configMgr.cutsDict["WVR2El"] = d["WVR2"]+OneEleSelection
configMgr.cutsDict["WVR2Mu"] = d["WVR2"]+OneMuoSelection

configMgr.cutsDict["WVR3"] = d["VR3"]+"&& nB4Jet30==0"
configMgr.cutsDict["WVR3El"] = d["WVR3"]+OneEleSelection
configMgr.cutsDict["WVR3Mu"] = d["WVR3"]+OneMuoSelection

configMgr.cutsDict["VR4El"] = d["VR4"]+OneEleSelection
configMgr.cutsDict["VR4Mu"] = d["VR4"]+OneMuoSelection

configMgr.cutsDict["SR4jTEl"] = d["SR4jT"]+OneEleSelection
configMgr.cutsDict["SR4jTMu"] = d["SR4jT"]+OneMuoSelection

## Lists of weights 
weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","bTagWeight[6]"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

bTagHighWeights = replaceWeight(weights,"bTagWeight[6]","bTagWeightBUp[6]")
bTagLowWeights = replaceWeight(weights,"bTagWeight[6]","bTagWeightBDown[6]")


trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights,"leptonWeight","leptonWeightDown")

#########################
## List of systematics ##
#########################

# Signal XSec uncertainty as overallSys (pure yeild affect) DEPRECATED
xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig","_NoSys","_JESup","_JESdown","tree","histoSys")

basicChanSyst = []
#if "JES"     in SystList :basicChanSyst.append(Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","overallSys")) # JES uncertainty - for low pt jets
#if "JES"     in SystList :basicChanSyst.append(Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","overallSys")) # JES uncertainty - for medium pt jets
#if "JES"     in SystList :basicChanSyst.append(Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","overallSys")) # JES uncertainty - for high pt jets

if "JES"     in SystList :basicChanSyst.append(Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys")) # JES uncertainty - for low pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys")) # JES uncertainty - for medium pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys")) # JES uncertainty - for high pt jets

#if "JES"     in SystList : basicChanSyst.append(Systematic(    "JES","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
#if "JES"     in SystList : basicChanSyst.append(Systematic(    "JES","_NoSys","_JESup"    ,"_JESdown"    ,"tree","overallSys"))
if "JER"     in SystList : basicChanSyst.append(Systematic(    "JER","_NoSys","_JER"      ,"_JER"        ,"tree","histoSysOneSide"))
#if "JER"     in SystList : basicChanSyst.append(Systematic(    "JER","_NoSys","_JER"      ,"_NoSys"        ,"tree","overallSys"))
if "ScaleSt" in SystList : basicChanSyst.append(Systematic("SCALEST","_NoSys","_SCALESTup","_SCALESTdown","tree","histoSys"))
if "ResoSt"  in SystList : basicChanSyst.append(Systematic( "RESOST","_NoSys","_RESOSTup" ,"_RESOSTdown" ,"tree","histoSys"))

# Generator Systematics
generatorSyst = []

if "GenW"   in SystList:
    SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
    generatorSyst.append((("SherpaWMassiveB","meffInc30_SR4jTEl"), SystGenW)) # Only applied to SR.
    generatorSyst.append((("SherpaWMassiveB","meffInc30_SR4jTMu"), SystGenW)) # Only applied to SR.
if "GenTTbar" in SystList:
    SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")
    generatorSyst.append((("PowhegPythiaTTbar","meffInc30_SR4jTEl"), SystGenTTbar)) # Only applied to SR.
    generatorSyst.append((("PowhegPythiaTTbar","meffInc30_SR4jTMu"), SystGenTTbar)) # Only applied to SR.

elChanSyst = []
muChanSyst = []
# Lepton weight uncertainty
if "LepEff" in SystList:
    elChanSyst.append(Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")) 
    muChanSyst.append(Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))

# Trigger efficiency
if "LepTrig" in SystList:
    elChanSyst.append(Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
    muChanSyst.append(Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))

# Electron energy scale uncertainty
if "EES" in SystList:
    elChanSyst.append(Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")) 

# Muon energy resolutions
if "MER" in SystList:
    muChanSyst.append(Systematic("LRMmu","_NoSys","_LERIDup","_LERIDdown","tree","overallSys"))
    muChanSyst.append(Systematic("LRImu","_NoSys","_LERMSup","_LERMSdown","tree","overallSys"))

bTagSyst = Systematic("BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallSys")

# This calculation is valid only for nTruthB4Jet == 0 or 1.
BTagEfficiency   = 0.6 # default MV1
SherpaCorrection = 0.4 # efficiency goes down as small as ~40% (conservative)

## bTagWeightForSherpa  = replaceWeight(weights,"bTagWeight4Jet","( 1 + ( ( ( 1 / %f ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(SherpaCorrection))
## bTagSystForSherpa    = Systematic("SBT",configMgr.weights,bTagWeightForSherpa,bTagWeightForSherpa,"weight","histoSysOneSide")
## bVetoWeightForSherpa = replaceWeight(weights,"bTagWeight4Jet","( 1 + ( ( ( ( 1 - %f ) / ( 1 - %f * %f ) ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(BTagEfficiency,SherpaCorrection,BTagEfficiency))
## bVetoSystForSherpa   = Systematic("SBT",configMgr.weights,bVetoWeightForSherpa,bVetoWeightForSherpa,"weight","histoSysOneSide")
#############
## Samples ##
#############

configMgr.nomName = "_NoSys"

DibosonsSample = Sample("Dibosons",kOrange-8)
DibosonsSample.addSystematic(Systematic("errBG", configMgr.weights,1.2 ,0.8, "user","userOverallSys"))
DibosonsSample.setStatConfig(useStat)

SingleTopSample = Sample("SingleTop",kGreen-5)
SingleTopSample.addSystematic(Systematic("errBG", configMgr.weights,1.2 ,0.8, "user","userOverallSys"))
SingleTopSample.setStatConfig(useStat)

TTbarSample = Sample("PowhegPythiaTTbar",kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)

WSampleName = "SherpaWMassiveB"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_WZ",1.,0.,5.)
WSample.setStatConfig(useStat)

ZSampleName = "SherpaZMassiveB"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setNormFactor("mu_WZ",1.,0.,5.)
ZSample.setStatConfig(useStat)

QCDSample = Sample("QCD",kGray+1)
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(useStat)

DataSample = Sample("Data",kBlack)
DataSample.setData()



################
# Bkg-only fit #
################
bkgOnly = configMgr.addTopLevelXML("bkgonly")
if not doSignalOnly:
    bkgOnly.addSamples([DibosonsSample,SingleTopSample,TTbarSample,WSample,ZSample,QCDSample,DataSample])
    #bkgOnly.addSamples([topSample,wzSample,qcdSample,dataSample])
if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.037)
meas.addPOI("mu_SIG")

#Add common systematics
for syst in basicChanSyst:
    bkgOnly.addSystematic(syst)

#b-tag classification of channels
bReqChans = []
bVetoChans = []
bAgnosticChans = []

#lepton flavor classification of channels
elChans = []
muChans = []

meffNBinsSR = 4
meffBinLowSR = 800.
meffBinHighSR = 1600.

meffNBinsCR = 4
meffBinLowCR = 500.
meffBinHighCR = 1300.


meffMax=100000.
metMax=100000.

if useHardLepCR:
    #  single ele
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["WREl"],meffNBinsCR,meffBinLowCR,meffBinHighCR),[elChans,bReqChans])
    #tmp.getSample(wzSampleName).addSystematic(bVetoSystForSherpa)
    #top = tmp.getSample("TopMcAtNlo")
    #WZ = tmp.getSample(wzSampleName)
    #BG = tmp.getSample("BG")
    #top.removeSystematic("JES")
    #WZ.removeSystematic("JES")
    #BG.removeSystematic("JER")
    #WZ.addSystematic(Systematic("JES_WZ_WREl","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #top.addSystematic(Systematic("JES_Top_WREl","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #BG.addSystematic(Systematic("JER_BG_WREl","_NoSys","_JER"    ,"_JER"    ,"tree","histoSysOneSide"))
    #tmp.useOverflowBin=True
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("meffInc30",["TREl"],meffNBinsCR,meffBinLowCR,meffBinHighCR),[elChans,bVetoChans])
    #tmp.getSample(wzSampleName).addSystematic(bTagSystForSherpa)
    #tmp.getSample(wzSampleName).removeSystematic("JES")
    #tmp.useOverflowBin=True
    bkgOnly.setBkgConstrainChannels(tmp)
    
    
    #  single muo
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["WRMu"],meffNBinsCR,meffBinLowCR,meffBinHighCR),[muChans,bReqChans])
    #tmp.getSample(wzSampleName).addSystematic(bVetoSystForSherpa)
    #top = tmp.getSample("TopMcAtNlo")
    #WZ = tmp.getSample(wzSampleName)
    #BG = tmp.getSample("BG")
    #top.removeSystematic("JES")
    #WZ.removeSystematic("JES")  
    #BG.removeSystematic("JES")  
    #WZ.addSystematic(Systematic("JES_WZ_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","overallSys"))
    #top.addSystematic(Systematic("JES_Top_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","overallSys"))
    #BG.addSystematic(Systematic("JES_BG_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #tmp.useOverflowBin=True
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("meffInc30",["TRMu"],meffNBinsCR,meffBinLowCR,meffBinHighCR),[muChans,bVetoChans])
    #tmp.getSample(wzSampleName).addSystematic(bTagSystForSherpa)
    #top = tmp.getSample("TopMcAtNlo")
    #WZ = tmp.getSample(wzSampleName)
    #BG = tmp.getSample("BG")
    #top.removeSystematic("JES")
    #WZ.removeSystematic("JES")  
    #BG.removeSystematic("JES")  
    #WZ.addSystematic(Systematic("JES_WZ_TRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #top.addSystematic(Systematic("JES_Top_TRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #BG.addSystematic(Systematic("JES_BG_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #tmp.useOverflowBin=True
    #tmp.getSample(wzSampleName).removeSystematic("JES")
    bkgOnly.setBkgConstrainChannels(tmp)

## Add JES systematic per channel as shapeSys
#for chan in bkgOnly.channels:
#    jesname = "JES_" + chan.name
#    tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#    chan.addSystematic(tmpJES)
        
#for chan in bkgOnly.channels:
#    jesname = "JES_" + chan.name
#    if 'TR' in chan.name:
#        for sam in chan.sampleList:
#            if 'WZ' in sam.name:
#                sam.removeSystematic(jesname)
#    if 'WREl' in chan.name:
#        for sam in chan.sampleList:
#            if 'BG' in sam.name:
#                sam.removeSystematic(jesname)	
        


# Add JES and JER systematic per channel as histoSys
#for chan in bkgOnly.channels:
#    jesname = "JES_" + chan.name
#    jername = "JER_" + chan.name
#    tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys")
#    chan.addSystematic(tmpJES) 
#    tmpJER = Systematic( jername,"_NoSys","_JER","_NoSys","tree","histoSys")
#    chan.addSystematic(tmpJER)        
    
######################################################
# Bkg-only configuration is finished.                #
# Move on with validation config from bkgOnly clone. #
######################################################

ValidRegList["OneLep"]  = ValidRegList["OneLep1"] or ValidRegList["OneLep2"] or ValidRegList["OneLep3"] or ValidRegList["OneLep4"]
validation = None
if doTableInputs or ValidRegList["SRTight"] or ValidRegList["OneLep"]:
    validation = configMgr.addTopLevelXMLClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans)
        appendIfMatchName(c,bVetoChans)
        appendIfMatchName(c,bAgnosticChans)
        appendIfMatchName(c,elChans)
        appendIfMatchName(c,muChans)

    if ValidRegList["SRTight"] or doTableInputs:
    #if ValidRegList["SRTight"]:
        if doTableInputs:
            appendTo(validation.addValidationChannel("meffInc30",["SR4jTEl"],1,0.,meffMax),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("meffInc30",["SR4jTMu"],1,0.,meffMax),[bAgnosticChans,muChans])
        else:
            appendTo(validation.addValidationChannel("meffInc30",["SR4jTEl"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("meffInc30",["SR4jTMu"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans,muChans])

    if doTableInputs:
        appendTo(validation.addValidationChannel("met",["WREl"],1,0.,metMax),[bVetoChans,elChans])
        appendTo(validation.addValidationChannel("met",["WRMu"],1,0.,metMax),[bVetoChans,muChans])
        appendTo(validation.addValidationChannel("met",["TREl"],1,0.,metMax),[bReqChans,elChans])
        appendTo(validation.addValidationChannel("met",["TRMu"],1,0.,metMax),[bReqChans,muChans])

    if ValidRegList["OneLep"] or doTableInputs:

        # Validation plots
        # Binning
        # nBins, min, max
        if doTableInputs:
            # In table inputs mode, all bins are summed up to 1bin.
            metBinsVR     = ( 1,   0., metMax)
            pass
        else:
            metBinsVR     = (10,   0.,1000.)
            meffBinsVR    = (15,   0.,3000.)
            lep1PtBinsVR  = (10,   0.,1000.)
            WptBinsVR     = (10,   0.,1000.)
            njets         = (10,   0.,10.)
            nbjets         = (10,   0.,10.)            
            pass
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        if ValidRegList["OneLep1"] : 
            ProcessRegions.append(["WVR1",bVetoChans])
            ProcessRegions.append(["TVR1",bReqChans])
        #if ValidRegList["OneLep2"] : 
        #    ProcessRegions.append(["WVR2",bVetoChans])
        #    ProcessRegions.append(["TVR2",bReqChans])
        #if ValidRegList["OneLep3"] : 
        #    ProcessRegions.append(["WVR3",bVetoChans])
        #    ProcessRegions.append(["TVR3",bReqChans])
        if ValidRegList["OneLep4"] : 
            ProcessRegions.append(["VR4" ,bAgnosticChans])
            ProcessRegions.append(["TWR" ,bAgnosticChans])   

        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            for chan in [["El",elChans],["Mu",muChans]]:
                ChanSuffix = chan[0]
                FlavorList = chan[1]
                appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
                if not doTableInputs:
                    appendTo(validation.addValidationChannel("meffInc30",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("lep1Pt" ,[CutPrefix+ChanSuffix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("Wpt"    ,[CutPrefix+ChanSuffix],   WptBinsVR[0],   WptBinsVR[1],   WptBinsVR[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("nB4Jet30",[CutPrefix+ChanSuffix],   nbjets[0],   nbjets[1],   nbjets[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("nJet40" ,[CutPrefix+ChanSuffix],   njets[0],   njets[1],   njets[2]),[bChanKind,FlavorList])

#    # add JES to each channel as ShapeSys
#    for chan in validation.channels:
#        jesname = "JES_" + chan.name
#        #if not chan.getSystematic(jesname):
#        if jesname not in chan.systDict.keys():
#            tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#            chan.addSystematic(tmpJES)
        
    
#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
if doExclusion_mSUGRA:                
    SRs=["SR4jTEl","SR4jTMu",]

    for sig in sigSamples:
        myTopLvl = configMgr.addTopLevelXMLClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans)
            appendIfMatchName(c,bVetoChans)
            appendIfMatchName(c,bAgnosticChans)
            appendIfMatchName(c,elChans)
            appendIfMatchName(c,muChans)
            
        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(jesSignal)
        sigSample.addSystematic(xsecSig)
        if sig.startswith("SM"):
            from SystematicsUtils import getISRSyst
            isrSyst = getISRSyst(sig)
            sigSample.addSystematic(isrSyst)
            pass
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if ValidRegList["SRTight"] or doTableInputs:
                #don't re-create already existing channel, but unset as Validation and set as Signal channel
                channame = sr + "_meffInc30"
                if channame in myTopLvl.channels:
                    ch = myTopLvl.getChannel("meffInc30",[sr])
                    iPop=myTopLvl.validationChannels.index(sr+"_meffInc30")
                    myTopLvl.validationChannels.pop(iPop)
            else:            
                if sr=="SR4jTEl" or sr=="SR4jTMu":
                    ch = myTopLvl.addChannel("meffInc30",[sr],meffNBinsSR,meffBinLowSR,meffBinHighSR)
		    #for theSample in ch.sampleList:          
                    #    theSample.removeSystematic("JHigh")
                    #    theSample.removeSystematic("JMedium")
                    #    theSample.removeSystematic("JLow")  
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                
                
                if sr=="SR4jTEl":
                    elChans.append(ch)
                elif sr=="SR4jTMu":
                    muChans.append(ch)
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                pass
            
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.useOverflowBin=True
            bAgnosticChans.append(ch)

#            # add JES as ShapeSys to each channel      
#            jesname = "JES_" + ch.name
#            if jesname not in ch.systDict.keys():
#                tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#                ch.addSystematic(tmpJES)
        
            

##############################
# Finalize fit configs setup #
##############################

# b-tag reg/veto/agnostic channels
for chan in bReqChans:
    chan.hasBQCD = True
    chan.addSystematic(bTagSyst)

for chan in bVetoChans:
    chan.hasBQCD = False
    chan.addSystematic(bTagSyst)

for chan in bAgnosticChans:
    chan.hasBQCD = False
    chan.removeWeight("bTagWeight4Jet")

AllChannels = bReqChans + bVetoChans + bAgnosticChans

# Generator Systematics for each sample,channel
print "** Generator Systematics **"
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan   = tgt[1]
    for chan in AllChannels:
        if tgtchan=="All" or tgtchan==chan.name:
            chan.getSample(tgtsample).addSystematic(syst)
            print "Add Generator Systematics (",syst.name,") to (",chan.name,")"


SetupChannels(elChans,bgdFiles_e, elChanSyst)
SetupChannels(muChans,bgdFiles_m, muChanSyst)

##Final semi-hacks for signal samples in exclusion fits
"""
if doExclusion_mSUGRA:
    for sig in sigSamples:
        myTopLvl=configMgr.getTopLevelXML("Sig_%s"%sig)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)           
            theSample.removeSystematic("JHigh")
            theSample.removeSystematic("JMedium")
            theSample.removeSystematic("JLow")              
            theSigFiles=[]
            if chan in elChans:
                theSigFiles = sigFiles_e
            elif chan in muChans:
                theSigFiles = sigFiles_m
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
"""
#######################
## Cosmetic Settings ##
#######################
# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","p")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)

#
entry = leg.AddEntry("","multijets (data estimate)","lf")
entry.SetLineColor(QCDSample.color)
entry.SetFillColor(QCDSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","single top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)


# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation : validation.tLegend = leg
c.Close()

# Plot "ATLAS" label
for chan in AllChannels:
    chan.titleY = "Entires"
    chan.logY   = False
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.85
    chan.ATLASLabelText = "work in progress"
    chan.ShowLumi = True
