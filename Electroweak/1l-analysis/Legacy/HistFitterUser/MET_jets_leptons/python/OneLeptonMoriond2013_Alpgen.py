################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
#from configWriter import TopLevelXML,Measurement,ChannelXML,Sample 
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #


# ********************************************************************* #
#                              Main part
# ********************************************************************* #
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

useHardLepCR=True
useStat=True
doExclusion_mSUGRA=False

SystList=[]
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("JER")      # Jet Energy Resolution (common)
#SystList.append("LepEff")   # Lepton efficiency (e&m)
#SystList.append("LepTrig")  # Trigger efficiency (e&m)
#SystList.append("GenW")     # Generator Systematics W    (common)
#SystList.append("GenTTbar") # Generator Systematics TTbar(common)
#if not doExclusion_mSUGRA:
#SystList.append("ResoSt")   # Resolution of SoftTerm (common)
#SystList.append("ScaleSt")  # Scale of SoftTerm (common)
#SystList.append("EES")      # Electron Energy Scale (e only)
#SystList.append("MER")      # Muon Energy Resolution (m only)

doTableInputs=False #This effectively means no validation plots but only validation tables (but is 100x faster)
ValidRegList={}
ValidRegList["SRTight"] = False
ValidRegList["OneLep1"] = False
ValidRegList["OneLep2"] = False
ValidRegList["OneLep3"] = False
ValidRegList["OneLep4"] = False
ValidRegList["WRV"] = True
ValidRegList["TRV"] = True

doDiscovery=True
#doExclusion_mSUGRA=True
doSignalOnly=False #Remove all bkgs for signal histo creation step
if configMgr.executeHistFactory:
    doSignalOnly=False
    
if not 'sigSamples' in dir():
    sigSamples=["SU_400_500_0_10_P"]

analysissuffix = ''
if doExclusion_mSUGRA:
    if 'GG1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix = '_GG1stepx12'
    elif 'GG1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix = '_GG1stepgridx'
    elif 'SS1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix = '_SS1stepx12'
    elif 'SS1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix = '_SS1stepgridx' 
    elif 'GG2WWZZ' in sigSamples[0]:
        analysissuffix = '_GG2WWZZ'
    elif 'GG2CNsl' in sigSamples[0]:
        analysissuffix = '_GG2CNsl'    
    elif 'SS2WWZZ' in sigSamples[0]:
        analysissuffix = '_SS2WWZZ'
    elif 'SS2CNsl' in sigSamples[0]:
        analysissuffix = '_SS2CNsl' 
    elif 'pMSSM' in sigSamples[0]:
        analysissuffix = '_pMSSM'                
    elif 'HiggsSU' in sigSamples[0]:
        analysissuffix = '_HiggsSU' 

# First define HistFactory attributes
configMgr.analysisName = "OneLeptonModirond2013"+analysissuffix # Name to give the analysis
configMgr.outputFileName = "results/OneLeptonMoriond2013"+analysissuffix+".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 14.3
configMgr.setLumiUnits("fb-1")

#configMgr.daoHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

#Split bdgFiles per channel
sigFiles_e = []
sigFiles_m = []
inputDir="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_4/"
inputDirSig="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_4/"
#inputDir="/data/maxi101/.tmp/ysasaki/Group/2012_11_09_v3_02/treeMaker/v3_2/"
#inputDirSig="/data/maxi101/.tmp/ysasaki/Group/2012_11_09_v3_02/treeMaker/v3_2/"

if not onLxplus:
    print "INFO : Running local...\n"

#    inputDir="/atlas_tmp/urrejola/SUSYFitterTrees/V4_1_2_Alpgen_lnlnqqqq_WHF/"
#    inputDir2="/atlas_tmp/urrejola/SUSYFitterTrees/V4_1_2_Alpgen_lnlnqqqq_WHF/"
#    inputDirSLBkg="/atlas_tmp/urrejola/SUSYFitterTrees/V4_1_2_Alpgen_lnlnqqqq_WHF/"
#    inputDirSig="/atlas_tmp/urrejola/SUSYFitterTrees/V4_1_2_Alpgen_lnlnqqqq_WHF/"
    inputDir="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4/"
    inputDir2="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4/"
    inputDirSLBkg="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4/"
    inputDirSig="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4/"
else:
    print "INFO : Running on lxplus... \n"

# Set the files to read from
bgdFiles_e = [inputDir+"bkgtree_HardEle.root"]#,inputDir+"datatree_HardEle.root"]
bgdFiles_m = [inputDir+"bkgtree_HardMuo.root"]#,inputDir+"datatree_HardMuo.root"]
if doExclusion_mSUGRA:
    sigFiles_e=[inputDirSig+"sigtree_HardEle_SU.root"]
    sigFiles_m=[inputDirSig+"sigtree_HardMuo_SU.root"]
    if 'SM_SS1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS1step.root"]
        print "Using simplified models SS onestepCC"
    if 'SM_GG1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
        print "Using simplified models GG onestepCC"
    if 'pMSSM' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_pMSSM.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_pMSSM.root"]
        print "Using pMSSM signal model"
    if 'GG2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ.root"]
        print "Using simplified models GG two step with WWZZ"
    if 'SS2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ.root"]
        print "Using simplified models SS two step with WWZZ"   
    if 'GG2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2CNsl.root"]
        print "Using simplified models GG two step with sleptons"
    if 'SS2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2CNsl.root"]
        print "Using simplified models SS two step with sleptons" 

# Map regions to cut strings
#configMgr.cutsDict["WR"]="lep2Pt<10 && met>100 && met<180 && mt>100 && jet4Pt>80 && meffInc40>500 && nB4Jet==0"
configMgr.cutsDict["WR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["TR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["WR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<150 && mt>80 && meffInc30>500"
configMgr.cutsDict["TR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<150 && mt>80 && meffInc30>500"
configMgr.cutsDict["INCL"]="nJet30>2 && lep2Pt<0 && lep1Pt>25" 
#configMgr.cutsDict["WR"]="nJet30>2 && lep2Pt<0 && lep1Pt>25 && jet1Pt>30 && jet2Pt>30 && jet3Pt>30 && met>50 && met<180 && nB3Jet30==0 && mt>40"
#configMgr.cutsDict["TR"]="nJet30>2 && lep2Pt<0 && lep1Pt>25 && jet1Pt>30 && jet2Pt>30 && jet3Pt>30 && met>50 && met<180 && nB3Jet30>0 && mt>40"
#configMgr.cutsDict["WRV"]"nJet30>2 && lep2Pt<0 && lep1Pt>25 && jet1Pt>80 && jet2Pt>30 && jet3Pt>30 && met>100 && nB3Jet30==0 && mt>40"
#configMgr.cutsDict["TWR"]="lep2Pt<10 && met>100 && met<180 && mt > 100 && jet4Pt>80 && meffInc40>500"

#configMgr.cutsDict["VR1"]="lep2Pt<10 && met>180 && met<250 && mt>100 && jet2Pt > 80 && jet4Pt> 40 && meffInc40>500"
#configMgr.cutsDict["VR2"]="lep2Pt<10 && met>100 && met<250 && mt>80 && mt<100 && jet2Pt > 80 && jet4Pt> 40 && meffInc40>500"
#configMgr.cutsDict["VR3"]="lep2Pt<10 && met>100 && met<180 && mt>40 && mt < 80 && jet2Pt > 80 && jet4Pt> 40 && meffInc40>500"
#configMgr.cutsDict["VR4"]="lep2Pt<10 && met>180 && met<250 && mt>100 && jet2Pt > 80 && jet4Pt> 40 && met/meff4Jet40>0.2 && meffInc40>800"

configMgr.cutsDict["SR3jT"]="lep2Pt<10 && met>250 && mt>150 && jet1Pt>80 && jet2Pt>80 & jet3Pt>30 && jet5Pt<40 && met/meff3Jet30>0.3 && meffInc30>800"
configMgr.cutsDict["SR5jT"]="lep2Pt<10 && met>300 && mt>200 && jet1Pt>80 && jet2Pt>50 & jet5Pt>40 && jet6Pt<40 && meffInc30>800"
configMgr.cutsDict["SR6jT"]="lep2Pt<10 && met>250 && mt>150 && jet1Pt>80 && jet2Pt>50 & jet6Pt>40 && meffInc30>600"

d=configMgr.cutsDict
OneEleSelection = "&& AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && acos(1.0-0.5*mt*mt/lep1Pt/met)<2.8"
OneMuoSelection = "&& AnalysisType==2 && ( EF_mu24_j65_a4tchad_EFxe40_tclcw )"
#OneEleSelection = "&& AnalysisType==1 && EF_e24vhi_medium1"
#OneMuoSelection = "&& AnalysisType==2 && EF_mu24i_tight"
 
configMgr.cutsDict["TR3JEl"] = d["TR3J"]+OneEleSelection
configMgr.cutsDict["WR3JEl"] = d["WR3J"]+OneEleSelection
configMgr.cutsDict["TR3JMu"] = d["TR3J"]+OneMuoSelection
configMgr.cutsDict["WR3JMu"] = d["WR3J"]+OneMuoSelection
configMgr.cutsDict["TR5JEl"] = d["TR5J"]+OneEleSelection
configMgr.cutsDict["WR5JEl"] = d["WR5J"]+OneEleSelection
configMgr.cutsDict["TR5JMu"] = d["TR5J"]+OneMuoSelection
configMgr.cutsDict["WR5JMu"] = d["WR5J"]+OneMuoSelection
configMgr.cutsDict["INCLMu"] = d["INCL"]+OneMuoSelection
configMgr.cutsDict["INCLEl"] = d["INCL"]+OneEleSelection
configMgr.cutsDict["SR3jTEl"] = d["SR3jT"]+OneEleSelection
configMgr.cutsDict["SR3jTMu"] = d["SR3jT"]+OneMuoSelection
configMgr.cutsDict["SR5jTEl"] = d["SR5jT"]+OneEleSelection
configMgr.cutsDict["SR5jTMu"] = d["SR5jT"]+OneMuoSelection
configMgr.cutsDict["SR6jTEl"] = d["SR6jT"]+OneEleSelection
configMgr.cutsDict["SR6jTMu"] = d["SR6jT"]+OneMuoSelection


#configMgr.cutsDict["TWREl"]=d["TWR"]+OneEleSelection
#configMgr.cutsDict["TWRMu"]=d["TWR"]+OneMuoSelection

#configMgr.cutsDict["TVR1"] = d["VR1"]+"&& nB4Jet>0"
#configMgr.cutsDict["TVR1El"] = d["TVR1"]+OneEleSelection
#configMgr.cutsDict["TVR1Mu"] = d["TVR1"]+OneMuoSelection

#configMgr.cutsDict["TVR2"] = d["VR2"]+"&& nB4Jet>0"
#configMgr.cutsDict["TVR2El"] = d["TVR2"]+OneEleSelection
#configMgr.cutsDict["TVR2Mu"] = d["TVR2"]+OneMuoSelection

#configMgr.cutsDict["TVR3"] = d["VR3"]+"&& nB4Jet>0"
#configMgr.cutsDict["TVR3El"] = d["TVR3"]+OneEleSelection
#configMgr.cutsDict["TVR3Mu"] = d["TVR3"]+OneMuoSelection

#configMgr.cutsDict["WVR1"] = d["VR1"]+"&& nB4Jet==0"
#configMgr.cutsDict["WVR1El"] = d["WVR1"]+OneEleSelection
#configMgr.cutsDict["WVR1Mu"] = d["WVR1"]+OneMuoSelection

#configMgr.cutsDict["WVR2"] = d["VR2"]+"&& nB4Jet==0"
#configMgr.cutsDict["WVR2El"] = d["WVR2"]+OneEleSelection
#configMgr.cutsDict["WVR2Mu"] = d["WVR2"]+OneMuoSelection

#configMgr.cutsDict["WVR3"] = d["VR3"]+"&& nB4Jet==0"
#configMgr.cutsDict["WVR3El"] = d["WVR3"]+OneEleSelection
#configMgr.cutsDict["WVR3Mu"] = d["WVR3"]+OneMuoSelection

#configMgr.cutsDict["VR4El"] = d["VR4"]+OneEleSelection
#configMgr.cutsDict["VR4Mu"] = d["VR4"]+OneMuoSelection


## Lists of weights 
weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","bTagWeight[3]"]
#weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#bTagHighWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeight_up[3]")
#bTagLowWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeight_down[3]")

bTagHighWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightBUp[3]")
bTagLowWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightBDown[3]")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights,"leptonWeight","leptonWeightDown")


#########################
## List of systematics ##
#########################

# Signal XSec uncertainty as overallSys (pure yeild affect) DEPRECATED
xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
#jesSignal = Systematic("JSig","_NoSys","_JESup","_JESdown","tree","histoSys")

basicChanSyst = []
#if "JES"     in SystList :basicChanSyst.append(Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","overallSys")) # JES uncertainty - for low pt jets
#if "JES"     in SystList :basicChanSyst.append(Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","overallSys")) # JES uncertainty - for medium pt jets
#if "JES"     in SystList :basicChanSyst.append(Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","overallSys")) # JES uncertainty - for high pt jets

if "JES"     in SystList :basicChanSyst.append(Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys")) # JES uncertainty - for low pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys")) # JES uncertainty - for medium pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys")) # JES uncertainty - for high pt jets

#if "JES"     in SystList : basicChanSyst.append(Systematic(    "JES","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
#if "JES"     in SystList : basicChanSyst.append(Systematic(    "JES","_NoSys","_JESup"    ,"_JESdown"    ,"tree","overallSys"))
if "JER"     in SystList : basicChanSyst.append(Systematic(    "JER","_NoSys","_JER"      ,"_JER"        ,"tree","histoSysOneSide"))
#if "JER"     in SystList : basicChanSyst.append(Systematic(    "JER","_NoSys","_JER"      ,"_NoSys"        ,"tree","overallSys"))
if "ScaleSt" in SystList : basicChanSyst.append(Systematic("SCALEST","_NoSys","_SCALESTup","_SCALESTdown","tree","histoSys"))
if "ResoSt"  in SystList : basicChanSyst.append(Systematic( "RESOST","_NoSys","_RESOSTup" ,"_RESOSTdown" ,"tree","histoSys"))

# Generator Systematics
generatorSyst = []

#if "GenW"   in SystList:
#    SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#    generatorSyst.append((("SherpaWMassiveB","meffInc40_SR4jTEl"), SystGenW)) # Only applied to SR.
#    generatorSyst.append((("SherpaWMassiveB","meffInc40_SR4jTMu"), SystGenW)) # Only applied to SR.
#if "GenTTbar" in SystList:
#    SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")
#    generatorSyst.append((("PowhegPythiaTTbar","meffInc40_SR4jTEl"), SystGenTTbar)) # Only applied to SR.
#    generatorSyst.append((("PowhegPythiaTTbar","meffInc40_SR4jTMu"), SystGenTTbar)) # Only applied to SR.

elChanSyst = []
muChanSyst = []
# Lepton weight uncertainty
if "LepEff" in SystList:
    elChanSyst.append(Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")) 
    muChanSyst.append(Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))

# Trigger efficiency
if "LepTrig" in SystList:
    elChanSyst.append(Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
    muChanSyst.append(Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))

# Electron energy scale uncertainty
if "EES" in SystList:
    elChanSyst.append(Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")) 

# Muon energy resolutions
if "MER" in SystList:
    muChanSyst.append(Systematic("LRMmu","_NoSys","_LERIDup","_LERIDdown","tree","overallSys"))
    muChanSyst.append(Systematic("LRImu","_NoSys","_LERMSup","_LERMSdown","tree","overallSys"))

bTagSyst = Systematic("BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallSys")

# This calculation is valid only for nTruthB4Jet == 0 or 1.
BTagEfficiency   = 0.6 # default MV1
SherpaCorrection = 0.4 # efficiency goes down as small as ~40% (conservative)

## bTagWeightForSherpa  = replaceWeight(weights,"bTagWeight4Jet","( 1 + ( ( ( 1 / %f ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(SherpaCorrection))
## bTagSystForSherpa    = Systematic("SBT",configMgr.weights,bTagWeightForSherpa,bTagWeightForSherpa,"weight","histoSysOneSide")
## bVetoWeightForSherpa = replaceWeight(weights,"bTagWeight4Jet","( 1 + ( ( ( ( 1 - %f ) / ( 1 - %f * %f ) ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(BTagEfficiency,SherpaCorrection,BTagEfficiency))
## bVetoSystForSherpa   = Systematic("SBT",configMgr.weights,bVetoWeightForSherpa,bVetoWeightForSherpa,"weight","histoSysOneSide")
#############
## Samples ##
#############

configMgr.nomName = "_NoSys"

W0SampleName = "AlpgenW_Np0"
W0Sample = Sample(W0SampleName,kAzure+2)
W0Sample.setNormFactor("mu_W0",1.,0.,5.)
W0Sample.setStatConfig(useStat)

W1SampleName = "AlpgenW_Np1"
W1Sample = Sample(W1SampleName,kAzure+3)
W1Sample.setNormFactor("mu_W1",1.,0.,5.)
W1Sample.setStatConfig(useStat)

W2SampleName = "AlpgenW_Np2"
W2Sample = Sample(W2SampleName,kAzure+2)
W2Sample.setNormFactor("mu_W2",1.,0.,5.)
W2Sample.setStatConfig(useStat)

W3SampleName = "AlpgenW_Np3"
W3Sample = Sample(W3SampleName,kAzure+1)
W3Sample.setNormFactor("mu_W3",1.,0.,5.)
W3Sample.setStatConfig(useStat)

W4SampleName = "AlpgenW_Np4"
W4Sample = Sample(W4SampleName,kAzure+6)
W4Sample.setNormFactor("mu_W4",1.,0.,5.)
W4Sample.setStatConfig(useStat)

W5SampleName = "AlpgenW_Np5"
W5Sample = Sample(W5SampleName,kAzure-9)
W5Sample.setNormFactor("mu_W5",1.,0.,5.)
W5Sample.setStatConfig(useStat)

TTbarlnqq0SampleName = "AlpgenTTbar_lnqqNp0"
TTbarlnqq0Sample = Sample(TTbarlnqq0SampleName,kOrange+9)
TTbarlnqq0Sample.setNormFactor("mu_Top0",1.,0.,5.)
TTbarlnqq0Sample.setStatConfig(useStat)

TTbarlnqq1SampleName = "AlpgenTTbar_lnqqNp1"
TTbarlnqq1Sample = Sample(TTbarlnqq1SampleName,kOrange+7)
TTbarlnqq1Sample.setNormFactor("mu_Top1",1.,0.,5.)
TTbarlnqq1Sample.setStatConfig(useStat)

TTbarlnqq2SampleName = "AlpgenTTbar_lnqqNp2"
TTbarlnqq2Sample = Sample(TTbarlnqq2SampleName,kOrange+6)
TTbarlnqq2Sample.setNormFactor("mu_Top2",1.,0.,5.)
TTbarlnqq2Sample.setStatConfig(useStat)

TTbarlnqq3SampleName = "AlpgenTTbar_lnqqNp3"
TTbarlnqq3Sample = Sample(TTbarlnqq3SampleName,kOrange-2)
TTbarlnqq3Sample.setNormFactor("mu_Top3",1.,0.,5.)
TTbarlnqq3Sample.setStatConfig(useStat)

TTbarlnln0SampleName = "AlpgenTTbar_lnlnNp0"
TTbarlnln0Sample = Sample(TTbarlnln0SampleName,kOrange+9)
TTbarlnln0Sample.setNormFactor("mu_Top0",1.,0.,5.)
TTbarlnln0Sample.setStatConfig(useStat)
    
TTbarlnln1SampleName = "AlpgenTTbar_lnlnNp1"
TTbarlnln1Sample = Sample(TTbarlnln1SampleName,kOrange+7)
TTbarlnln1Sample.setNormFactor("mu_Top1",1.,0.,5.)
TTbarlnln1Sample.setStatConfig(useStat)

TTbarlnln2SampleName = "AlpgenTTbar_lnlnNp2"
TTbarlnln2Sample = Sample(TTbarlnln2SampleName,kOrange+6)
TTbarlnln2Sample.setNormFactor("mu_Top2",1.,0.,5.)
TTbarlnln2Sample.setStatConfig(useStat)

TTbarlnln3SampleName = "AlpgenTTbar_lnlnNp3"
TTbarlnln3Sample = Sample(TTbarlnln3SampleName,kOrange-2)
TTbarlnln3Sample.setNormFactor("mu_Top3",1.,0.,5.)
TTbarlnln3Sample.setStatConfig(useStat)

DibosonsSampleName = "Dibosons"
DibosonsSample = Sample(DibosonsSampleName,kSpring-5)
DibosonsSample.setNormFactor("mu_Dibosons",1.,0.,5.)
DibosonsSample.setStatConfig(useStat)
#
SingleTopSampleName = "SingleTop"
SingleTopSample = Sample(SingleTopSampleName,kSpring-5)
SingleTopSample.setNormFactor("mu_SingleTop",1.,0.,5.)
SingleTopSample.setStatConfig(useStat)
#
ZSampleName = "AlpgenZ"
ZSample = Sample(ZSampleName,kSpring-5)
ZSample.setNormFactor("mu_Z",1.,0.,5.)
ZSample.setStatConfig(useStat)

QCDSample = Sample("QCD",kGray+1)
QCDSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"])
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(useStat)

DataSample = Sample("Data",kBlack)
DataSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"])
DataSample.setData()


################
# Bkg-only fit #
################
bkgOnly = configMgr.addTopLevelXML("bkgonly")
if not doSignalOnly:
    #bkgOnly.addSamples([DibosonsSample,SingleTopSample,TTbarSample,WSample,ZSample,QCDSample,DataSample])
    #bkgOnly.addSamples([topSample,wzSample,qcdSample,dataSample])
#    bkgOnly.addSamples([DibosonsSample,SingleTopSample,TTbar0Sample,TTbar1Sample,TTbar2Sample,TTbar3Sample,ZSample,W0Sample,W1Sample,W2Sample,W3Sample,W4Sample,W5Sample,DataSample])
    bkgOnly.addSamples([QCDSample,DibosonsSample,ZSample,SingleTopSample,TTbarlnln0Sample,TTbarlnqq0Sample,TTbarlnln1Sample,TTbarlnqq1Sample,TTbarlnln2Sample,TTbarlnqq2Sample,TTbarlnln3Sample,TTbarlnqq3Sample,W0Sample,W1Sample,W2Sample,W3Sample,W4Sample,W5Sample,DataSample])
#    bkgOnly.addSamples([TTbarlnln0Sample,TTbarlnqq0Sample,TTbarlnln1Sample,TTbarlnqq1Sample,TTbarlnln2Sample,TTbarlnqq2Sample,TTbarlnln3Sample,TTbarlnqq3Sample,W1Sample,W2Sample,W3Sample,W4Sample,W5Sample,DataSample])
if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.037)
meas.addPOI("mu_SIG")

meas.addParamSetting("mu_W0","const",1.0)
meas.addParamSetting("mu_W1","const",1.0)
meas.addParamSetting("mu_W2","const",1.0)
#meas.addParamSetting("mu_W3","const",1.0)
#meas.addParamSetting("mu_W4","const",1.0)
#meas.addParamSetting("mu_W5","const",1.0)
#meas.addParamSetting("mu_Top0","const",1.0)
#meas.addParamSetting("mu_Top1","const",1.0)
#meas.addParamSetting("mu_Top2","const",1.0)
#meas.addParamSetting("mu_Top3","const",1.0)
meas.addParamSetting("mu_Dibosons","const",1.0)
meas.addParamSetting("mu_SingleTop","const",1.0)
meas.addParamSetting("mu_Z","const",1.0)

#Add common systematics
for syst in basicChanSyst:
    bkgOnly.addSystematic(syst)

#b-tag classification of channels
bReqChans = []
bVetoChans = []
bAgnosticChans = []

#lepton flavor classification of channels
elChans = []
muChans = []

meffNBinsSR = 4
meffBinLowSR = 800.
meffBinHighSR = 1600.

metNBinsSR = 5
metBinLowSR = 250.
metBinHighSR = 500.


#
#meffNBinsCR = 4
#meffBinLowCR = 500.
#meffBinHighCR = 1300.
#
#
meffMax=100000.
metMax=100000.

if useHardLepCR:

    # INCL ele
    #tmp = appendTo(bkgOnly.addChannel("nJet30",["INCLEl"],7,3,10),[elChans,bAgnosticChans])
    #bkgOnly.setBkgConstrainChannels(tmp)    
    #tmp.minY = 0.05
    #tmp.maxY = 50000

    # INCL mu
    #tmp = appendTo(bkgOnly.addChannel("nJet30",["INCLMu"],7,3,10),[muChans,bAgnosticChans])
    #bkgOnly.setBkgConstrainChannels(tmp)    
    #tmp.minY = 0.05
    #tmp.maxY = 50000


#-----3JET--------#

    #  single ele
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR3JEl"],7,3,10),[elChans,bVetoChans])
    #tmp.getSample(wzSampleName).addSystematic(bVetoSystForSherpa)
    #top = tmp.getSample("TopMcAtNlo")
    #WZ = tmp.getSample(wzSampleName)
    #BG = tmp.getSample("BG")
    #top.removeSystematic("JES")
    #W2Z.removeSystematic("JES")
    #BG.removeSystematic("JER")
    #WZ.addSystematic(Systematic("JES_WZ_WREl","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #top.addSystematic(Systematic("JES_Top_WREl","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #BG.addSystematic(Systematic("JER_BG_WREl","_NoSys","_JER"    ,"_JER"    ,"tree","histoSysOneSide"))
    #tmp.useOverflowBin=True
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 0.05
    tmp.maxY = 50000

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR3JEl"],7,3,10),[elChans,bReqChans])
    #tmp.getSample(wzSampleName).addSystematic(bTagSystForSherpa)
    #tmp.getSample(wzSampleName).removeSystematic("JES")
    #tmp.useOverflowBin=True
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 0.05
    tmp.maxY = 50000
   
    #  single muo
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR3JMu"],7,3,10),[muChans,bVetoChans])
    #tmp.getSample(wzSampleName).addSystematic(bVetoSystForSherpa)
    #top = tmp.getSample("TopMcAtNlo")
    #WZ = tmp.getSample(wzSampleName)
    #BG = tmp.getSample("BG")
    #top.removeSystematic("JES")
    #WZ.removeSystematic("JES")  
    #BG.removeSystematic("JES")  
    #WZ.addSystematic(Systematic("JES_WZ_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","overallSys"))
    #top.addSystematic(Systematic("JES_Top_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","overallSys"))
    #BG.addSystematic(Systematic("JES_BG_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #tmp.useOverflowBin=True
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 0.05
    tmp.maxY = 50000

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR3JMu"],7,3,10),[muChans,bReqChans])
    #tmp.getSample(wzSampleName).addSystematic(bTagSystForSherpa)
    #top = tmp.getSample("TopMcAtNlo")
    #WZ = tmp.getSample(wzSampleName)
    #BG = tmp.getSample("BG")
    #top.removeSystematic("JES")
    #WZ.removeSystematic("JES")  
    #BG.removeSystematic("JES")  
    #WZ.addSystematic(Systematic("JES_WZ_TRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #top.addSystematic(Systematic("JES_Top_TRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #BG.addSystematic(Systematic("JES_BG_WRMu","_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys"))
    #tmp.useOverflowBin=True
    #tmp.getSample(wzSampleName).removeSystematic("JES")
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 0.05
    tmp.maxY = 50000



#-----5JET--------#
    #  single ele
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR5JEl"],5,5,10),[elChans,bVetoChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 0.05
    tmp.maxY = 50000

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR5JEl"],5,5,10),[elChans,bReqChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 0.05
    tmp.maxY = 50000

    #  single muo
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR5JMu"],5,5,10),[muChans,bVetoChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 0.05
    tmp.maxY = 50000

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR5JMu"],5,5,10),[muChans,bReqChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 0.05
    tmp.maxY = 50000




## Add JES systematic per channel as shapeSys
#for chan in bkgOnly.channels:
#    jesname = "JES_" + chan.name
#    tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#    chan.addSystematic(tmpJES)
        
#for chan in bkgOnly.channels:
#    jesname = "JES_" + chan.name
#    if 'TR' in chan.name:
#        for sam in chan.sampleList:
#            if 'WZ' in sam.name:
#                sam.removeSystematic(jesname)
#    if 'WREl' in chan.name:
#        for sam in chan.sampleList:
#            if 'BG' in sam.name:
#                sam.removeSystematic(jesname)	
        


# Add JES and JER systematic per channel as histoSys
#for chan in bkgOnly.channels:
#    jesname = "JES_" + chan.name
#    jername = "JER_" + chan.name
#    tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","histoSys")
#    chan.addSystematic(tmpJES) 
#    tmpJER = Systematic( jername,"_NoSys","_JER","_NoSys","tree","histoSys")
#    chan.addSystematic(tmpJER)        
    
######################################################
# Bkg-only configuration is finished.                #
# Move on with validation config from bkgOnly clone. #
######################################################
Met_Min = 90.
Met_Max = 200.
Met_bin = 22
Lep_Min = 0.
Lep_Max = 400.
Lep_bin = 20
Jet_Min = 0.
Jet_Max = 800.
Jet_bin = 20
Mt_Min = 35.
Mt_Max = 90.
Mt_bin = 22
W_Min = 100.
W_Max = 500.
W_bin = 20

ValidRegList["OneLep"]  = ValidRegList["OneLep1"] or ValidRegList["OneLep2"] or ValidRegList["OneLep3"] or ValidRegList["OneLep4"]

validation = None
#if doTableInputs or ValidRegList["SRTight"] or ValidRegList["OneLep"]:
if doTableInputs or ValidRegList["WRV"] or ValidRegList["TRV"]:
    validation = configMgr.addTopLevelXMLClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans)
        appendIfMatchName(c,bVetoChans)
        appendIfMatchName(c,bAgnosticChans)
        appendIfMatchName(c,elChans)
        appendIfMatchName(c,muChans)

    if ValidRegList["SRTight"] or doTableInputs:
    #if ValidRegList["SRTight"]:
        if doTableInputs:
            appendTo(validation.addValidationChannel("meffInc40",["SR4jTEl"],1,0.,meffMax),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("meffInc40",["SR4jTMu"],1,0.,meffMax),[bAgnosticChans,muChans])
        else:
            appendTo(validation.addValidationChannel("meffInc40",["SR4jTEl"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("meffInc40",["SR4jTMu"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans,muChans])

    if doTableInputs:
        appendTo(validation.addValidationChannel("met",["WREl"],1,0.,metMax),[bVetoChans,elChans])
        appendTo(validation.addValidationChannel("met",["WRMu"],1,0.,metMax),[bVetoChans,muChans])
        appendTo(validation.addValidationChannel("met",["TREl"],1,0.,metMax),[bReqChans,elChans])
        appendTo(validation.addValidationChannel("met",["TRMu"],1,0.,metMax),[bReqChans,muChans])

#    if ValidRegList["WRV"] or ValidRegList["TRV"]:
#        appendTo(validation.addValidationChannel("met",["WREl"],Met_bin,Met_Min,Met_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("met",["WRMu"],Met_bin,Met_Min,Met_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("met",["TREl"],Met_bin,Met_Min,Met_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("met",["TRMu"],Met_bin,Met_Min,Met_Max),[bReqChans,muChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["WREl"],1,0.,meffMax),[bAgnosticChans,elChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["WRMu"],1,0.,meffMax),[bAgnosticChans,elChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["TREl"],1,0.,meffMax),[bAgnosticChans,elChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["TRMu"],1,0.,meffMax),[bAgnosticChans,elChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["WREl"],Lep_bin,Lep_Min,Lep_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["WRMu"],Lep_bin,Lep_Min,Lep_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["TREl"],Lep_bin,Lep_Min,Lep_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["TRMu"],Lep_bin,Lep_Min,Lep_Max),[bReqChans,muChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["WREl"],Jet_bin,Jet_Min,Jet_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["WRMu"],Jet_bin,Jet_Min,Jet_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["TREl"],Jet_bin,Jet_Min,Jet_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["TRMu"],Jet_bin,Jet_Min,Jet_Max),[bReqChans,muChans])
#        appendTo(validation.addValidationChannel("Wpt",["WREl"],W_bin,W_Min,W_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("Wpt",["WRMu"],W_bin,W_Min,W_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("Wpt",["TREl"],W_bin,W_Min,W_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("Wpt",["TRMu"],W_bin,W_Min,W_Max),[bReqChans,muChans])
# 	 appendTo(validation.addValidationChannel("mt",["WREl"],Mt_bin,Mt_Min,Mt_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("mt",["WRMu"],Mt_bin,Mt_Min,Mt_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("mt",["TREl"],Mt_bin,Mt_Min,Mt_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("mt",["TRMu"],Mt_bin,Mt_Min,Mt_Max),[bReqChans,muChans])
#

    if ValidRegList["OneLep"] or doTableInputs:

        # Validation plots
        # Binning
        # nBins, min, max
        if doTableInputs:
            # In table inputs mode, all bins are summed up to 1bin.
            metBinsVR     = ( 1,   0., metMax)
            pass
        else:
            metBinsVR     = (10,   0.,1000.)
            meffBinsVR    = (15,   0.,3000.)
            lep1PtBinsVR  = (10,   0.,1000.)
            WptBinsVR     = (10,   0.,1000.)
            njets         = (10,   0.,10.)
            nbjets         = (10,   0.,10.)            
            pass
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        if ValidRegList["OneLep1"] : 
            ProcessRegions.append(["WVR1",bVetoChans])
            ProcessRegions.append(["TVR1",bReqChans])
        #if ValidRegList["OneLep2"] : 
        #    ProcessRegions.append(["WVR2",bVetoChans])
        #    ProcessRegions.append(["TVR2",bReqChans])
        #if ValidRegList["OneLep3"] : 
        #    ProcessRegions.append(["WVR3",bVetoChans])
        #    ProcessRegions.append(["TVR3",bReqChans])
        if ValidRegList["OneLep4"] : 
            ProcessRegions.append(["VR4" ,bAgnosticChans])
            ProcessRegions.append(["TWR" ,bAgnosticChans])   
        
        if ValidRegList["WRV"] :
            ProcessRegions.append(["VR4" ,bAgnosticChans])
            ProcessRegions.append(["TWR" ,bAgnosticChans])

        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            for chan in [["El",elChans],["Mu",muChans]]:
                ChanSuffix = chan[0]
                FlavorList = chan[1]
#                appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
                if not doTableInputs:
#                    appendTo(validation.addValidationChannel("meffInc30",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
#                    appendTo(validation.addValidationChannel("lep1Pt" ,[CutPrefix+ChanSuffix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
#                    appendTo(validation.addValidationChannel("Wpt"    ,[CutPrefix+ChanSuffix],   WptBinsVR[0],   WptBinsVR[1],   WptBinsVR[2]),[bChanKind,FlavorList])
#                    appendTo(validation.addValidationChannel("nB4Jet",[CutPrefix+ChanSuffix],   nbjets[0],   nbjets[1],   nbjets[2]),[bChanKind,FlavorList])
#                    appendTo(validation.addValidationChannel("nJet30" ,[CutPrefix+ChanSuffix],   njets[0],   njets[1],   njets[2]),[bChanKind,FlavorList])
                    appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])

#    # add JES to each channel as ShapeSys
#    for chan in validation.channels:
#        jesname = "JES_" + chan.name
#        #if not chan.getSystematic(jesname):
#        if jesname not in chan.systDict.keys():
#            tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#            chan.addSystematic(tmpJES)
        
#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
if doExclusion_mSUGRA:                
    SRs=["SR3jTEl","SR3jTMu","SR5jTEl","SR5jTMu","SR6jTEl","SR6jTMu"]

    for sig in sigSamples:
        myTopLvl = configMgr.addTopLevelXMLClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans)
            appendIfMatchName(c,bVetoChans)
            appendIfMatchName(c,bAgnosticChans)
            appendIfMatchName(c,elChans)
            appendIfMatchName(c,muChans)
            
        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(jesSignal)
        sigSample.addSystematic(xsecSig)
        if sig.startswith("SM"):
            from SystematicsUtils import getISRSyst
            isrSyst = getISRSyst(sig)
            sigSample.addSystematic(isrSyst)
            pass
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if ValidRegList["SRTight"] or doTableInputs:
                #don't re-create already existing channel, but unset as Validation and set as Signal channel
                channame = sr + "_meffInc30"
                if channame in myTopLvl.channels:
                    ch = myTopLvl.getChannel("meffInc30",[sr])
                    iPop=myTopLvl.validationChannels.index(sr+"_meffInc30")
                    myTopLvl.validationChannels.pop(iPop)
            else:            
                if sr=="SR5jTEl" or sr=="SR5jTMu":
                    ch = myTopLvl.addChannel("meffInc30",[sr],meffNBinsSR,meffBinLowSR,meffBinHighSR)
                elif sr=="SR3jTEl" or sr=="SR3jTMu" or sr=="SR6jTEl" or sr=="SR6jTMu":
                    ch = bkgOnly.addChannel("met",[sr],metNBinsSR,metBinLowSR,metBinHighSR)

                    #for theSample in ch.sampleList:          
                    #    theSample.removeSystematic("JHigh")
                    #    theSample.removeSystematic("JMedium")
                    #    theSample.removeSystematic("JLow")  
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                
                
                if sr=="SR3jTEl" or sr=="SR5jTEl" or sr=="SR6jTEl":
                    elChans.append(ch)
                elif sr=="SR4jTMu" or sr=="SR5jTMu" or sr=="SR6jTMu":
                    muChans.append(ch)
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                pass
            
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.useOverflowBin=True
            bAgnosticChans.append(ch)

#            # add JES as ShapeSys to each channel      
#            jesname = "JES_" + ch.name
#            if jesname not in ch.systDict.keys():
#                tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#                ch.addSystematic(tmpJES)
        

##############################
# Finalize fit configs setup #
##############################

# b-tag reg/veto/agnostic channels
for chan in bReqChans:
    chan.hasBQCD = True
    chan.addSystematic(bTagSyst)

for chan in bVetoChans:
    chan.hasBQCD = False
    chan.addSystematic(bTagSyst)

for chan in bAgnosticChans:
    chan.hasBQCD = False
    chan.removeWeight("bTagWeight[3]")

AllChannels = bReqChans + bVetoChans + bAgnosticChans

# Generator Systematics for each sample,channel
print "** Generator Systematics **"
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan   = tgt[1]
    for chan in AllChannels:
        if tgtchan=="All" or tgtchan==chan.name:
            chan.getSample(tgtsample).addSystematic(syst)
            print "Add Generator Systematics (",syst.name,") to (",chan.name,")"


SetupChannels(elChans,bgdFiles_e, elChanSyst)
SetupChannels(muChans,bgdFiles_m, muChanSyst)

##Final semi-hacks for signal samples in exclusion fits

if doExclusion_mSUGRA:
    for sig in sigSamples:
        myTopLvl=configMgr.getTopLevelXML("Sig_%s"%sig)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)           
            theSample.removeSystematic("JHigh")
            theSample.removeSystematic("JMedium")
            theSample.removeSystematic("JLow")              
            theSigFiles=[]
            if chan in elChans:
                theSigFiles = sigFiles_e
            elif chan in muChans:
                theSigFiles = sigFiles_m

            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)




#######################
## Cosmetic Settings ##
#######################
# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","p")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)

#
#entry = leg.AddEntry("","multijets (data estimate)","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
###
#entry = leg.AddEntry("","W+jets","lf")
#entry.SetLineColor(WSample.color)
#entry.SetFillColor(WSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Z+jets","lf")
#entry.SetLineColor(ZSample.color)
#entry.SetFillColor(ZSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","t#bar{t}","lf")
#entry.SetLineColor(TTbarSample.color)
#entry.SetFillColor(TTbarSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","single top","lf")
#entry.SetLineColor(SingleTopSample.color)
#entry.SetFillColor(SingleTopSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","diboson","lf")
#entry.SetLineColor(DibosonsSample.color)
#entry.SetFillColor(DibosonsSample.color)
#entry.SetFillStyle(compFillStyle)
#

#entry = leg.AddEntry("","W0jet","lf")
#entry.SetLineColor(W0Sample.color)
#entry.SetFillColor(W0Sample.color)
#entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","W1jet","lf")
entry.SetLineColor(W1Sample.color)
entry.SetFillColor(W1Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","W2jet","lf")
entry.SetLineColor(W2Sample.color)
entry.SetFillColor(W2Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","W3jet","lf")
entry.SetLineColor(W3Sample.color)
entry.SetFillColor(W3Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","W4jet","lf")
entry.SetLineColor(W4Sample.color)
entry.SetFillColor(W4Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","W5jet","lf")
entry.SetLineColor(W5Sample.color)
entry.SetFillColor(W5Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t}0jet","lf")
entry.SetLineColor(TTbarlnln0Sample.color)
entry.SetFillColor(TTbarlnln0Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t}1jet","lf")
entry.SetLineColor(TTbarlnln1Sample.color)
entry.SetFillColor(TTbarlnln1Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t}2jet","lf")
entry.SetLineColor(TTbarlnln2Sample.color)
entry.SetFillColor(TTbarlnln2Sample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t}3jet","lf")
entry.SetLineColor(TTbarlnln3Sample.color)
entry.SetFillColor(TTbarlnln3Sample.color)
entry.SetFillStyle(compFillStyle)

#entry = leg.AddEntry("","Z","lf")
#entry.SetLineColor(ZSample.color)
#entry.SetFillColor(ZSample.color)
#entry.SetFillStyle(compFillStyle)
##
#entry = leg.AddEntry("","ST","lf")
#entry.SetLineColor(SingleTopSample.color)
#entry.SetFillColor(SingleTopSample.color)
#entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","Dib./ST/Z","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","Multijet","lf")
entry.SetLineColor(QCDSample.color)
entry.SetFillColor(QCDSample.color)
entry.SetFillStyle(compFillStyle)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation : validation.tLegend = leg
c.Close()

# Plot "ATLAS" label
for chan in AllChannels:
    chan.titleY = "Entires"
    chan.logY   = False
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.85
    chan.ATLASLabelText = "work in progress"
    chan.ShowLumi = True
