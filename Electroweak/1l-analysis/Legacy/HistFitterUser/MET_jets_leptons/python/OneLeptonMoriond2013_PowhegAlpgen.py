################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
#from configWriter import TopLevelXML,Measurement,ChannelXML,Sample 
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #


# ********************************************************************* #
#                              Main part
# ********************************************************************* #
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

useHardLepCR=True
useStat=True
doExclusion_mSUGRA=False

SystList=[]
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("LepEff")   # Lepton efficiency (e&m)
SystList.append("LepTrig")  # Trigger efficiency (e&m)
#SystList.append("GenW")     # Generator Systematics W    (common)
#SystList.append("GenTTbar") # Generator Systematics TTbar(common)
#if not doExclusion_mSUGRA:
SystList.append("ResoSt")   # Resolution of SoftTerm (common)
SystList.append("ScaleSt")  # Scale of SoftTerm (common)
SystList.append("EES")      # Electron Energy Scale (e only)
SystList.append("MER")      # Muon Energy Resolution (m only)
SystList.append("PileUp")      # Pile-up
#SystList.append("BTag")        # Flat Btag uncertainty of 15% for now

doTableInputs=False #This effectively means no validation plots but only validation tables (but is 100x faster)
ValidRegList={}
ValidRegList["SRTight"] = False
ValidRegList["VR3JhighMET"] = False
ValidRegList["VR3JhighMT"] = False
ValidRegList["VR5JhighMET"] = False
ValidRegList["VR5JhighMT"] = False
ValidRegList["VR6JhighMET"] = False
ValidRegList["VR6JhighMT"] = False
ValidRegList["WRV"] = False
ValidRegList["TRV"] = False

doDiscovery=True
#doExclusion_mSUGRA=True
doSignalOnly=False #Remove all bkgs for signal histo creation step
if configMgr.executeHistFactory:
    doSignalOnly=False
    
if not 'sigSamples' in dir():
    sigSamples=["SU_400_500_0_10_P"]

analysissuffix = ''
if doExclusion_mSUGRA:
    if 'GG1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix = '_GG1stepx12'
    elif 'GG1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix = '_GG1stepgridx'
    elif 'SS1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix = '_SS1stepx12'
    elif 'SS1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix = '_SS1stepgridx' 
    elif 'GG2WWZZ' in sigSamples[0]:
        analysissuffix = '_GG2WWZZ'
    elif 'GG2CNsl' in sigSamples[0]:
        analysissuffix = '_GG2CNsl'    
    elif 'SS2WWZZ' in sigSamples[0]:
        analysissuffix = '_SS2WWZZ'
    elif 'SS2CNsl' in sigSamples[0]:
        analysissuffix = '_SS2CNsl' 
    elif 'pMSSM' in sigSamples[0]:
        analysissuffix = '_pMSSM'                
    elif 'HiggsSU' in sigSamples[0]:
        analysissuffix = '_HiggsSU' 

# First define HistFactory attributes
configMgr.analysisName = "OneLeptonMoriond2013_PowhegAlpgen"+analysissuffix # Name to give the analysis
configMgr.outputFileName = "results/OneLeptonMoriond2013_PowhegAlpgen"+analysissuffix+".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 14.3
configMgr.setLumiUnits("fb-1")

#configMgr.daoHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

configMgr.writeXML = True

configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#Split bdgFiles per channel
sigFiles_e = []
sigFiles_m = []
#inputDir="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v3_2/"
#inputDir="/afs/cern.ch/user/k/koutsman/HistFitterUser/MET_jets_leptons/data/trees/v4_4/"
#inputDir="/afs/cern.ch/user/k/koutsman/work/Moriond2013/data/trees/v4_4_3/"
#inputDir="/afs/cern.ch/user/k/koutsman/scratch0/data/trees/v4_4/"
inputDir="/afs/cern.ch/work/k/koutsman/public/Moriond2013/data/trees/v4_4_6/"
inputDirSig="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_4_6/"

#onLxplus=True

if not onLxplus:
    print "INFO : Running locally...\n"
    inputDir="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_3_PowhegAlpgen/"
#    inputDir2="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_3_PowhegAlpgen/"
#    inputDirSLBkg="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_3_PowhegAlpgen/"
    inputDirSig="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_3_PowhegAlpgen/"
else:
    print "INFO : Running on lxplus... \n"

# Set the files to read from
bgdFiles_e = [inputDir+"bkgtree_HardEle.root"] #,inputDir+"bkgtree_HardEle_AlpgenW.root",inputDir+"bkgtree_HardEle_Powheg.root", inputDir+"bkgtree_HardEle_SmallBkg.root"] #bkgtree_HardEle.root", ]#,inputDir+"datatree_HardEle.root"]
#bgdFiles_m = [inputDir+"bkgtree_HardMuo.root"]#,inputDir+"datatree_HardMuo.root"]
bgdFiles_m = [inputDir+"bkgtree_HardMuo.root"] #inputDir+"bkgtree_HardMuo_AlpgenW.root",inputDir+"bkgtree_HardMuo_Powheg.root", inputDir+"bkgtree_HardMuo_SmallBkg.root"] #bkgtree_HardMuo.root", ]#,inputDir+"datatree_HardMuo.root"]
if doExclusion_mSUGRA:
    sigFiles_e=[inputDirSig+"sigtree_HardEle_SU.root"]
    sigFiles_m=[inputDirSig+"sigtree_HardMuo_SU.root"]
    if 'SM_SS1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS1step.root"]
        print "Using simplified models SS onestepCC"
    if 'SM_GG1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
        print "Using simplified models GG onestepCC"
    if 'pMSSM' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_pMSSM.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_pMSSM.root"]
        print "Using pMSSM signal model"
    if 'GG2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ.root"]
        print "Using simplified models GG two step with WWZZ"
    if 'SS2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ.root"]
        print "Using simplified models SS two step with WWZZ"   
    if 'GG2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2CNsl.root"]
        print "Using simplified models GG two step with sleptons"
    if 'SS2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2CNsl.root"]
        print "Using simplified models SS two step with sleptons" 

# Map regions to cut strings
#configMgr.cutsDict["WR"]="lep2Pt<10 && met>100 && met<180 && mt>100 && jet4Pt>80 && meffInc40>500 && nB4Jet==0"
configMgr.cutsDict["WR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["TR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["WR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<150 && mt>80 && meffInc30>500"
configMgr.cutsDict["TR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<150 && mt>80 && meffInc30>500"
## configMgr.cutsDict["WR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<90 && mt>80 && meffInc30>500"
## configMgr.cutsDict["TR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<90 && mt>80 && meffInc30>500"
## configMgr.cutsDict["WR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<150 && mt>90 && meffInc30>500"
## configMgr.cutsDict["TR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<150 && mt>90 && meffInc30>500"
configMgr.cutsDict["INCL"]="nJet30>2 && lep2Pt<0 && lep1Pt>25" 
#configMgr.cutsDict["WR"]="nJet30>2 && lep2Pt<0 && lep1Pt>25 && jet1Pt>30 && jet2Pt>30 && jet3Pt>30 && met>50 && met<180 && nB3Jet30==0 && mt>40"
#configMgr.cutsDict["TR"]="nJet30>2 && lep2Pt<0 && lep1Pt>25 && jet1Pt>30 && jet2Pt>30 && jet3Pt>30 && met>50 && met<180 && nB3Jet30>0 && mt>40"
#configMgr.cutsDict["WRV"]"nJet30>2 && lep2Pt<0 && lep1Pt>25 && jet1Pt>80 && jet2Pt>30 && jet3Pt>30 && met>100 && nB3Jet30==0 && mt>40"
#configMgr.cutsDict["TWR"]="lep2Pt<10 && met>100 && met<180 && mt > 100 && jet4Pt>80 && meffInc40>500"
#configMgr.cutsDict["VR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && mt<80 && mt>40 && meffInc30>500"
#configMgr.cutsDict["VR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && mt<150 && mt>80 && meffInc30>500"


configMgr.cutsDict["VR3JhighMET"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>200 && met<500 && mt<80 && mt>40 && meffInc30>500 && jet5Pt<40"
configMgr.cutsDict["VR5JhighMET"]="nJet40>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet5Pt>40 && met>100 && met<500 && mt<80 && mt>40 && meffInc30>500 && jet6Pt<40"
configMgr.cutsDict["VR6JhighMET"]="nJet40>5 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet6Pt>40 && met>100 && met<500 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["VR3JhighMT"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<250 && mt<250 && mt>80 && meffInc30>500 && jet5Pt<40"
configMgr.cutsDict["VR5JhighMT"]="nJet40>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet5Pt>40 && met>200 && met<250 && mt<250 && mt>80 && meffInc30>500 && jet6Pt<40"
configMgr.cutsDict["VR6JhighMT"]="nJet40>5 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet6Pt>40 && met>200 && met<250 && mt<250 && mt>80 && meffInc30>500"

configMgr.cutsDict["WR3JMET"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && nB3Jet30==0 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["TR3JMET"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && nB3Jet30>0 && mt<80 && mt>40 && meffInc30>500"
configMgr.cutsDict["WR5JMET"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && nB3Jet30==0 && mt<150 && mt>80 && meffInc30>500"
configMgr.cutsDict["TR5JMET"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && nB3Jet30>0 && mt<150 && mt>80 && meffInc30>500"
configMgr.cutsDict["WR3JMT"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30==0 && meffInc30>500"
configMgr.cutsDict["TR3JMT"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30>0 && meffInc30>500"
configMgr.cutsDict["WR5JMT"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30==0 && meffInc30>500"
configMgr.cutsDict["TR5JMT"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30>0 && meffInc30>500"

configMgr.cutsDict["SR3jT"]="lep2Pt<10 && met>250 && mt>150 && jet1Pt>80 && jet2Pt>80 & jet3Pt>30 && jet5Pt<40 && met/meff3Jet30>0.3 && meffInc30>800"
configMgr.cutsDict["SR5jT"]="lep2Pt<10 && met>300 && mt>200 && jet1Pt>80 && jet2Pt>50 & jet5Pt>40 && jet6Pt<40 && meffInc30>800"
configMgr.cutsDict["SR6jT"]="lep2Pt<10 && met>250 && mt>150 && jet1Pt>80 && jet2Pt>50 & jet6Pt>40 && meffInc30>600"

d=configMgr.cutsDict
#OneEleSelection = "&& AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && acos(1.0-0.5*mt*mt/lep1Pt/met)<2.8"
OneEleSelection = "&& AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && isNoCrackElectron==1"
OneMuoSelection = "&& AnalysisType==2 && ( EF_mu24_j65_a4tchad_EFxe40_tclcw )"
#OneEleSelection = "&& AnalysisType==1 && EF_e24vhi_medium1"
#OneMuoSelection = "&& AnalysisType==2 && EF_mu24i_tight"
 
configMgr.cutsDict["TR3JEl"] = d["TR3J"]+OneEleSelection
configMgr.cutsDict["WR3JEl"] = d["WR3J"]+OneEleSelection
configMgr.cutsDict["TR3JMu"] = d["TR3J"]+OneMuoSelection
configMgr.cutsDict["WR3JMu"] = d["WR3J"]+OneMuoSelection
configMgr.cutsDict["TR5JEl"] = d["TR5J"]+OneEleSelection
configMgr.cutsDict["WR5JEl"] = d["WR5J"]+OneEleSelection
configMgr.cutsDict["TR5JMu"] = d["TR5J"]+OneMuoSelection
configMgr.cutsDict["WR5JMu"] = d["WR5J"]+OneMuoSelection
configMgr.cutsDict["INCLMu"] = d["INCL"]+OneMuoSelection
configMgr.cutsDict["INCLEl"] = d["INCL"]+OneEleSelection

#configMgr.cutsDict["VR3JEl"] = d["VR3J"]+OneEleSelection
#configMgr.cutsDict["VR3JMu"] = d["VR3J"]+OneMuoSelection
#configMgr.cutsDict["VR5JEl"] = d["VR5J"]+OneEleSelection
#configMgr.cutsDict["VR5JMu"] = d["VR5J"]+OneMuoSelection

configMgr.cutsDict["VR3JhighMETEl"] = d["VR3JhighMET"]+OneEleSelection
configMgr.cutsDict["VR3JhighMETMu"] = d["VR3JhighMET"]+OneMuoSelection
configMgr.cutsDict["VR5JhighMETEl"] = d["VR5JhighMET"]+OneEleSelection
configMgr.cutsDict["VR5JhighMETMu"] = d["VR5JhighMET"]+OneMuoSelection
configMgr.cutsDict["VR6JhighMETEl"] = d["VR6JhighMET"]+OneEleSelection
configMgr.cutsDict["VR6JhighMETMu"] = d["VR6JhighMET"]+OneMuoSelection

configMgr.cutsDict["VR3JhighMTEl"] = d["VR3JhighMT"]+OneEleSelection
configMgr.cutsDict["VR3JhighMTMu"] = d["VR3JhighMT"]+OneMuoSelection
configMgr.cutsDict["VR5JhighMTEl"] = d["VR5JhighMT"]+OneEleSelection
configMgr.cutsDict["VR5JhighMTMu"] = d["VR5JhighMT"]+OneMuoSelection
configMgr.cutsDict["VR6JhighMTEl"] = d["VR6JhighMT"]+OneEleSelection
configMgr.cutsDict["VR6JhighMTMu"] = d["VR6JhighMT"]+OneMuoSelection

configMgr.cutsDict["TR3JMETEl"] = d["TR3JMET"]+OneEleSelection
configMgr.cutsDict["WR3JMETEl"] = d["WR3JMET"]+OneEleSelection
configMgr.cutsDict["TR3JMETMu"] = d["TR3JMET"]+OneMuoSelection
configMgr.cutsDict["WR3JMETMu"] = d["WR3JMET"]+OneMuoSelection
configMgr.cutsDict["TR5JMETEl"] = d["TR5JMET"]+OneEleSelection
configMgr.cutsDict["WR5JMETEl"] = d["WR5JMET"]+OneEleSelection
configMgr.cutsDict["TR5JMETMu"] = d["TR5JMET"]+OneMuoSelection
configMgr.cutsDict["WR5JMETMu"] = d["WR5JMET"]+OneMuoSelection

configMgr.cutsDict["TR3JMTEl"] = d["TR3JMT"]+OneEleSelection
configMgr.cutsDict["WR3JMTEl"] = d["WR3JMT"]+OneEleSelection
configMgr.cutsDict["TR3JMTMu"] = d["TR3JMT"]+OneMuoSelection
configMgr.cutsDict["WR3JMTMu"] = d["WR3JMT"]+OneMuoSelection
configMgr.cutsDict["TR5JMTEl"] = d["TR5JMT"]+OneEleSelection
configMgr.cutsDict["WR5JMTEl"] = d["WR5JMT"]+OneEleSelection
configMgr.cutsDict["TR5JMTMu"] = d["TR5JMT"]+OneMuoSelection
configMgr.cutsDict["WR5JMTMu"] = d["WR5JMT"]+OneMuoSelection

configMgr.cutsDict["SR3jTEl"] = d["SR3jT"]+OneEleSelection
configMgr.cutsDict["SR3jTMu"] = d["SR3jT"]+OneMuoSelection
configMgr.cutsDict["SR5jTEl"] = d["SR5jT"]+OneEleSelection
configMgr.cutsDict["SR5jTMu"] = d["SR5jT"]+OneMuoSelection
configMgr.cutsDict["SR6jTEl"] = d["SR6jT"]+OneEleSelection
configMgr.cutsDict["SR6jTMu"] = d["SR6jT"]+OneMuoSelection

## ttbar Reweighting
ttbarReweight = "1"
#ttbarReweight = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.298216 ) * ( 1 + ( -1.347724e-03 ) * SquAvgTTbarPt ) ) ) )"

## Lists of weights 
weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight",ttbarReweight] #,"bTagWeight[5]"]
#weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#bTagHighWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeight_up[3]")
#bTagLowWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeight_down[3]")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights,"leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

#########################
## List of systematics ##
#########################

# Signal XSec uncertainty as overallSys (pure yeild affect) DEPRECATED
xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig","_NoSys","_JESup","_JESdown","tree","overallHistoSys")

basicChanSyst = []

## if "JES"     in SystList :basicChanSyst.append(Systematic("JES","_NoSys","_JESup","_JESdown","tree","overallNormHistoSys")) # JES uncertainty - for low pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","overallNormHistoSys")) # JES uncertainty - for low pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","overallNormHistoSys")) # JES uncertainty - for medium pt jets
if "JES"     in SystList :basicChanSyst.append(Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","overallNormHistoSys")) # JES uncertainty - for high pt jets

#if "JER"     in SystList : basicChanSyst.append(Systematic(    "JER","_NoSys","_JER"      ,"_JER"        ,"tree","histoSysOneSide"))
if "JER"     in SystList : basicChanSyst.append(Systematic(    "JER","_NoSys","_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym"))

## MET soft term resolution and scale
if "ScaleSt" in SystList : basicChanSyst.append(Systematic("SCALEST","_NoSys","_SCALESTup","_SCALESTdown","tree","overallNormHistoSys"))
if "ResoSt"  in SystList : basicChanSyst.append(Systematic( "RESOST","_NoSys","_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym"))

## pile-up
if "PileUp" in SystList : basicChanSyst.append( Systematic("pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallSys"))

## b-tagging, flat 15% for now as proposed by Yuichi
if "BTag" in SystList : basicChanSyst.append( Systematic("BT", configMgr.weights, 1.15, 0.85, "user", "userOverallSys"))

# Generator Systematics
generatorSyst = []

#if "GenW"   in SystList:
#    SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#    generatorSyst.append((("SherpaWMassiveB","meffInc40_SR4jTEl"), SystGenW)) # Only applied to SR.
#    generatorSyst.append((("SherpaWMassiveB","meffInc40_SR4jTMu"), SystGenW)) # Only applied to SR.
#if "GenTTbar" in SystList:
#    SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")
#    generatorSyst.append((("PowhegPythiaTTbar","meffInc40_SR4jTEl"), SystGenTTbar)) # Only applied to SR.
#    generatorSyst.append((("PowhegPythiaTTbar","meffInc40_SR4jTMu"), SystGenTTbar)) # Only applied to SR.

# Lepton weight uncertainty
if "LepEff" in SystList : basicChanSyst.append( Systematic("LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys"))

elChanSyst = []
muChanSyst = []
# Lepton weight uncertainty
#if "LepEff" in SystList:
#    elChanSyst.append(Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")) 
#    muChanSyst.append(Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
lepEff= Systematic("LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys")

# Trigger efficiency
if "LepTrig" in SystList:
    elChanSyst.append(Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
    muChanSyst.append(Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))

# Electron energy scale uncertainty
if "EES" in SystList:
    #    elChanSyst.append(Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")) 
    elChanSyst.append(Systematic("egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys"))
    elChanSyst.append(Systematic("egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallSys"))
    elChanSyst.append(Systematic("egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys"))
    elChanSyst.append(Systematic("eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys"))
    elChanSyst.append(Systematic("egres","_NoSys","_EGRESup","_EGRESdown","tree","overallSys"))

# Muon energy resolutions
if "MER" in SystList:
    muChanSyst.append(Systematic("LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallSys"))
    muChanSyst.append(Systematic("LRImu","_NoSys","_MIDup","_MIDdown","tree","overallSys")) 

#bTagSyst = Systematic("BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallSys")

# This calculation is valid only for nTruthB4Jet == 0 or 1.
#BTagEfficiency   = 0.6 # default MV1
#SherpaCorrection = 0.4 # efficiency goes down as small as ~40% (conservative)

## bTagWeightForSherpa  = replaceWeight(weights,"bTagWeight4Jet","( 1 + ( ( ( 1 / %f ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(SherpaCorrection))
## bTagSystForSherpa    = Systematic("SBT",configMgr.weights,bTagWeightForSherpa,bTagWeightForSherpa,"weight","histoSysOneSide")
## bVetoWeightForSherpa = replaceWeight(weights,"bTagWeight4Jet","( 1 + ( ( ( ( 1 - %f ) / ( 1 - %f * %f ) ) -1 ) * ( nTruthB4Jet > 0 ) ) )"%(BTagEfficiency,SherpaCorrection,BTagEfficiency))
## bVetoSystForSherpa   = Systematic("SBT",configMgr.weights,bVetoWeightForSherpa,bVetoWeightForSherpa,"weight","histoSysOneSide")


#############
## Samples ##
#############

configMgr.nomName = "_NoSys"

WSampleName = "AlpgenW"
WSample = Sample(WSampleName,kAzure-4)
#WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.setNormRegions([("TR3JEl","nJet30"),("WR3JEl","nJet30"),("TR5JEl","nJet30"),("WR5JEl","nJet30"),("TR3JMu","nJet30"),("WR3JMu","nJet30"),("TR5JMu","nJet30"),("WR5JMu","nJet30")])
#WSample.setNormRegions([("TR5JEl","nJet30"),("WR5JEl","nJet30"),("TR5JMu","nJet30"),("WR5JMu","nJet30")])

TTbarSampleName = "PowhegPythiaTTbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
TTbarSample.setNormRegions([("TR3JEl","nJet30"),("WR3JEl","nJet30"),("TR5JEl","nJet30"),("WR5JEl","nJet30"),("TR3JMu","nJet30"),("WR3JMu","nJet30"),("TR5JMu","nJet30"),("WR5JMu","nJet30")])

DibosonsSampleName = "Dibosons"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setNormFactor("mu_Dibosons",1.,0.,5.)
DibosonsSample.setStatConfig(useStat)
#
SingleTopSampleName = "SingleTop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setNormFactor("mu_SingleTop",1.,0.,5.)
SingleTopSample.setStatConfig(useStat)
#
ZSampleName = "AlpgenZ"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setNormFactor("mu_Z",1.,0.,5.)
ZSample.setStatConfig(useStat)

QCDSample = Sample("QCD",kYellow)
QCDSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"]) #_QCD.
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(useStat)

DataSample = Sample("Data",kBlack)
DataSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"])
DataSample.setData()


################
# Bkg-only fit #
################
#bkgOnly = configMgr.addTopLevelXML("bkgonly")
bkgOnly = configMgr.addFitConfig("bkgonly")
if not doSignalOnly:
    #bkgOnly.addSamples([DibosonsSample,SingleTopSample,TTbarSample,WSample,ZSample,QCDSample,DataSample])
    #bkgOnly.addSamples([topSample,wzSample,qcdSample,dataSample])
#    bkgOnly.addSamples([DibosonsSample,SingleTopSample,TTbar0Sample,TTbar1Sample,TTbar2Sample,TTbar3Sample,ZSample,W0Sample,W1Sample,W2Sample,W3Sample,W4Sample,W5Sample,DataSample])
#    bkgOnly.addSamples([QCDSample,DibosonsSample,ZSample,SingleTopSample,TTbarlnln0Sample,TTbarlnqq0Sample,TTbarlnln1Sample,TTbarlnqq1Sample,TTbarlnln2Sample,TTbarlnqq2Sample,TTbarlnln3Sample,TTbarlnqq3Sample,W0Sample,W1Sample,W2Sample,W3Sample,W4Sample,W5Sample,DataSample])
    bkgOnly.addSamples([DibosonsSample,SingleTopSample,ZSample,QCDSample,TTbarSample,WSample,DataSample])

if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.037)
meas.addPOI("mu_SIG")

#meas.addParamSetting("mu_W0","const",1.0)
#meas.addParamSetting("mu_W1","const",1.0)
#meas.addParamSetting("mu_W2","const",1.0)
#meas.addParamSetting("mu_W3","const",1.0)
#meas.addParamSetting("mu_W4","const",1.0)
#meas.addParamSetting("mu_W5","const",1.0)
#meas.addParamSetting("mu_Top0","const",1.0)
#meas.addParamSetting("mu_Top1","const",1.0)
#meas.addParamSetting("mu_Top2","const",1.0)
#meas.addParamSetting("mu_Top3","const",1.0)
meas.addParamSetting("mu_Dibosons","const",1.0)
meas.addParamSetting("mu_SingleTop","const",1.0)
meas.addParamSetting("mu_Z","const",1.0)

#Add common systematics
for syst in basicChanSyst:
    bkgOnly.addSystematic(syst)

#b-tag classification of channels
bReqChans = []
bVetoChans = []
bAgnosticChans = []

#lepton flavor classification of channels
elChans = []
muChans = []

meffNBinsSR = 4
meffBinLowSR = 800.
meffBinHighSR = 1600.

metNBinsSR = 5
metBinLowSR = 250.
metBinHighSR = 500.


#
#meffNBinsCR = 4
#meffBinLowCR = 500.
#meffBinHighCR = 1300.
#
#
meffMax=100000.
metMax=100000.

if useHardLepCR:

    nBins3JCR = 7  #7
    nBins5JCR = 5  #5

#-----3JET--------#

    #  single ele
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR3JEl"],nBins3JCR,3,10),[elChans,bVetoChans])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 1.5
    tmp.maxY = 10000

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR3JEl"],nBins3JCR,3,10),[elChans,bReqChans])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 1.5
    tmp.maxY = 10000

    #  single muo
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR3JMu"],nBins3JCR,3,10),[muChans,bVetoChans])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 1.5
    tmp.maxY = 10000
 

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR3JMu"],nBins3JCR,3,10),[muChans,bReqChans])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp.minY = 1.5
    tmp.maxY = 10000
    


#-----5JET--------#
    #  single ele
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR5JEl"],nBins5JCR,5,10),[elChans,bVetoChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 1.5
    tmp.maxY = 10000
    

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR5JEl"],nBins5JCR,5,10),[elChans,bReqChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 1.5
    tmp.maxY = 10000
    

    #  single muo
    tmp = appendTo(bkgOnly.addChannel("nJet30",["WR5JMu"],nBins5JCR,5,10),[muChans,bVetoChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 1.5
    tmp.maxY = 10000
    

    tmp = appendTo(bkgOnly.addChannel("nJet30",["TR5JMu"],nBins5JCR,5,10),[muChans,bReqChans])
    bkgOnly.setBkgConstrainChannels(tmp)
    tmp.minY = 1.5
    tmp.maxY = 10000

######################################################
# Bkg-only configuration is finished.                #
# Move on with validation config from bkgOnly clone. #
######################################################
Met_Min = 90.
Met_Max = 200.
Met_bin = 22
Lep_Min = 0.
Lep_Max = 400.
Lep_bin = 20
Jet_Min = 0.
Jet_Max = 800.
Jet_bin = 20
Mt_Min = 35.
Mt_Max = 90.
Mt_bin = 22
W_Min = 100.
W_Max = 500.
W_bin = 20

ValidRegList["OneLep"]  = ValidRegList["WRV"] or ValidRegList["TRV"] or ValidRegList["VR3JhighMET"] or ValidRegList["VR3JhighMT"] or ValidRegList["VR5JhighMET"] or ValidRegList["VR5JhighMT"] or ValidRegList["VR6JhighMET"] or ValidRegList["VR6JhighMT"]

validation = None
#if doTableInputs or ValidRegList["SRTight"] or ValidRegList["OneLep"]:
if doTableInputs or ValidRegList["OneLep"]:
#    validation = configMgr.addTopLevelXMLClone(bkgOnly,"Validation")
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans)
        appendIfMatchName(c,bVetoChans)
        appendIfMatchName(c,bAgnosticChans)
        appendIfMatchName(c,elChans)
        appendIfMatchName(c,muChans)

    if ValidRegList["SRTight"] or doTableInputs:
    #if ValidRegList["SRTight"]:
        if doTableInputs:
            appendTo(validation.addValidationChannel("meffInc30",["SR5jTEl"],1,0.,meffMax),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("meffInc30",["SR5jTMu"],1,0.,meffMax),[bAgnosticChans,muChans])
            appendTo(validation.addValidationChannel("met",["SR3jTEl"],1,0.,metMax),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("met",["SR3jTMu"],1,0.,metMax),[bAgnosticChans,muChans])
            appendTo(validation.addValidationChannel("met",["SR6jTEl"],1,0.,metMax),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("met",["SR6jTMu"],1,0.,metMax),[bAgnosticChans,muChans])
        else:
            appendTo(validation.addValidationChannel("meffInc30",["SR4jTEl"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans,elChans])
            appendTo(validation.addValidationChannel("meffInc30",["SR4jTMu"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans,muChans])

    if doTableInputs:
        appendTo(validation.addValidationChannel("met",["WR3JEl"],1,0.,metMax),[bVetoChans,elChans])
        appendTo(validation.addValidationChannel("met",["WR3JMu"],1,0.,metMax),[bVetoChans,muChans])
        appendTo(validation.addValidationChannel("met",["TR3JEl"],1,0.,metMax),[bReqChans,elChans])
        appendTo(validation.addValidationChannel("met",["TR3JMu"],1,0.,metMax),[bReqChans,muChans])
        appendTo(validation.addValidationChannel("met",["WR5JEl"],1,0.,metMax),[bVetoChans,elChans])
        appendTo(validation.addValidationChannel("met",["WR5JMu"],1,0.,metMax),[bVetoChans,muChans])
        appendTo(validation.addValidationChannel("met",["TR5JEl"],1,0.,metMax),[bReqChans,elChans])
        appendTo(validation.addValidationChannel("met",["TR5JMu"],1,0.,metMax),[bReqChans,muChans])

    #if ValidRegList["WRV"] or ValidRegList["TRV"]:
        #appendTo(validation.addValidationChannel("met",["WREl"],Met_bin,Met_Min,Met_Max),[bVetoChans,elChans])
        #appendTo(validation.addValidationChannel("met",["WRMu"],Met_bin,Met_Min,Met_Max),[bVetoChans,muChans])
        #appendTo(validation.addValidationChannel("met",["TREl"],Met_bin,Met_Min,Met_Max),[bReqChans,elChans])
        #appendTo(validation.addValidationChannel("met",["TRMu"],Met_bin,Met_Min,Met_Max),[bReqChans,muChans])
        #appendTo(validation.addValidationChannel("meffInc30",["WREl"],1,0.,meffMax),[bAgnosticChans,elChans])
        #appendTo(validation.addValidationChannel("meffInc30",["WRMu"],1,0.,meffMax),[bAgnosticChans,elChans])
        #appendTo(validation.addValidationChannel("meffInc30",["TREl"],1,0.,meffMax),[bAgnosticChans,elChans])
        #appendTo(validation.addValidationChannel("meffInc30",["TRMu"],1,0.,meffMax),[bAgnosticChans,elChans])
        #appendTo(validation.addValidationChannel("lep1Pt",["WREl"],Lep_bin,Lep_Min,Lep_Max),[bVetoChans,elChans])
        #appendTo(validation.addValidationChannel("lep1Pt",["WRMu"],Lep_bin,Lep_Min,Lep_Max),[bVetoChans,muChans])
        #appendTo(validation.addValidationChannel("lep1Pt",["TREl"],Lep_bin,Lep_Min,Lep_Max),[bReqChans,elChans])
        #appendTo(validation.addValidationChannel("lep1Pt",["TRMu"],Lep_bin,Lep_Min,Lep_Max),[bReqChans,muChans])
        #appendTo(validation.addValidationChannel("jet1Pt",["WREl"],Jet_bin,Jet_Min,Jet_Max),[bVetoChans,elChans])
        #appendTo(validation.addValidationChannel("jet1Pt",["WRMu"],Jet_bin,Jet_Min,Jet_Max),[bVetoChans,muChans])
        #appendTo(validation.addValidationChannel("jet1Pt",["TREl"],Jet_bin,Jet_Min,Jet_Max),[bReqChans,elChans])
        #appendTo(validation.addValidationChannel("jet1Pt",["TRMu"],Jet_bin,Jet_Min,Jet_Max),[bReqChans,muChans])
        #appendTo(validation.addValidationChannel("Wpt",["WREl"],W_bin,W_Min,W_Max),[bVetoChans,elChans])
        #appendTo(validation.addValidationChannel("Wpt",["WRMu"],W_bin,W_Min,W_Max),[bVetoChans,muChans])
        #appendTo(validation.addValidationChannel("Wpt",["TREl"],W_bin,W_Min,W_Max),[bReqChans,elChans])
        #appendTo(validation.addValidationChannel("Wpt",["TRMu"],W_bin,W_Min,W_Max),[bReqChans,muChans])
        #appendTo(validation.addValidationChannel("mt",["WREl"],Mt_bin,Mt_Min,Mt_Max),[bVetoChans,elChans])
        #appendTo(validation.addValidationChannel("mt",["WRMu"],Mt_bin,Mt_Min,Mt_Max),[bVetoChans,muChans])
        #appendTo(validation.addValidationChannel("mt",["TREl"],Mt_bin,Mt_Min,Mt_Max),[bReqChans,elChans])
        #appendTo(validation.addValidationChannel("mt",["TRMu"],Mt_bin,Mt_Min,Mt_Max),[bReqChans,muChans])

    if ValidRegList["OneLep"] or doTableInputs:

        # Validation plots
        # Binning
        # nBins, min, max
        if doTableInputs:
            # In table inputs mode, all bins are summed up to 1bin.
            meffBinsVR     = ( 1,   0., meffMax)
            pass
        else:
            metBinsVR     = (20,   100.,500.)
            meffBinsVR    = (20,   500.,2500.)
            lep1PtBinsVR  = (25,   0.,500.)
            WptBinsVR     = (25,   0.,500.)
            njets         = (10,   0.,10.)
            nbjets        = (10,   0.,10.)
            jet1ptBinsVR  = (21,   80.,500.)
            mtBinsVR      = (25,   0.,500.)
            pass
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        if ValidRegList["VR3JhighMET"] : 
            ProcessRegions.append(["VR3JhighMET" ,bAgnosticChans])
        if ValidRegList["VR3JhighMT"] : 
            ProcessRegions.append(["VR3JhighMT" ,bAgnosticChans]) 
        if ValidRegList["VR5JhighMET"] : 
            ProcessRegions.append(["VR5JhighMET" ,bAgnosticChans])
        if ValidRegList["VR5JhighMT"] : 
            ProcessRegions.append(["VR5JhighMT" ,bAgnosticChans])
        if ValidRegList["VR6JhighMET"] : 
            ProcessRegions.append(["VR6JhighMET" ,bAgnosticChans])
        if ValidRegList["VR6JhighMT"] : 
            ProcessRegions.append(["VR6JhighMT" ,bAgnosticChans])
            ProcessRegions.append(["WVR1",bVetoChans])
            ProcessRegions.append(["TVR1",bReqChans])
        if ValidRegList["WRV"] :
            ProcessRegions.append(["WR3J" ,bVetoChans])
            ProcessRegions.append(["WR5J" ,bVetoChans])
            ProcessRegions.append(["WR3JMET" ,bVetoChans])
            ProcessRegions.append(["WR5JMET" ,bVetoChans])
            ProcessRegions.append(["WR3JMT" ,bVetoChans])
            ProcessRegions.append(["WR5JMT" ,bVetoChans])
        if ValidRegList["TRV"] :
            ProcessRegions.append(["TR3J" ,bReqChans])
            ProcessRegions.append(["TR5J" ,bReqChans])
            ProcessRegions.append(["TR3JMET" ,bReqChans])
            ProcessRegions.append(["TR5JMET" ,bReqChans])
            ProcessRegions.append(["TR3JMT" ,bReqChans])
            ProcessRegions.append(["TR5JMT" ,bReqChans])

        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            for chan in [["El",elChans],["Mu",muChans]]:
                ChanSuffix = chan[0]
                FlavorList = chan[1]
                if not doTableInputs:
                    if not ("MET" in reg[0]) and not ("MT" in reg[0]):
                        appendTo(validation.addValidationChannel("meffInc30",[CutPrefix+ChanSuffix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
                        appendTo(validation.addValidationChannel("lep1Pt" ,[CutPrefix+ChanSuffix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
                        appendTo(validation.addValidationChannel("jet1Pt"    ,[CutPrefix+ChanSuffix],   jet1ptBinsVR[0],   jet1ptBinsVR[1],   jet1ptBinsVR[2]),[bChanKind,FlavorList])
                        appendTo(validation.addValidationChannel("jet2Pt" ,[CutPrefix+ChanSuffix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
                        appendTo(validation.addValidationChannel("Wpt"    ,[CutPrefix+ChanSuffix],   WptBinsVR[0],   WptBinsVR[1],   WptBinsVR[2]),[bChanKind,FlavorList])
                        appendTo(validation.addValidationChannel("nBJet30",[CutPrefix+ChanSuffix],   nbjets[0],   nbjets[1],   nbjets[2]),[bChanKind,FlavorList])
                        #if "3J" in reg[0]:
                        #    appendTo(validation.addValidationChannel("jet3Pt" ,[CutPrefix+ChanSuffix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
                        #elif "5J" in reg[0]:
                        #    appendTo(validation.addValidationChannel("jet5Pt" ,[CutPrefix+ChanSuffix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
                    elif "MET" in reg[0]:
                        appendTo(validation.addValidationChannel("met"    ,[CutPrefix+ChanSuffix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
                    elif "MT" in reg[0]:
                        appendTo(validation.addValidationChannel("mt"    ,[CutPrefix+ChanSuffix],   mtBinsVR[0],   mtBinsVR[1],   mtBinsVR[2]),[bChanKind,FlavorList])

#    # add JES to each channel as ShapeSys
#    for chan in validation.channels:
#        jesname = "JES_" + chan.name
#        #if not chan.getSystematic(jesname):
#        if jesname not in chan.systDict.keys():
#            tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#            chan.addSystematic(tmpJES)
        
#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
if doExclusion_mSUGRA:                
    SRs=["SR3jTEl","SR3jTMu","SR5jTEl","SR5jTMu","SR6jTEl","SR6jTMu"]

    for sig in sigSamples:
        myTopLvl = configMgr.addTopLevelXMLClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans)
            appendIfMatchName(c,bVetoChans)
            appendIfMatchName(c,bAgnosticChans)
            appendIfMatchName(c,elChans)
            appendIfMatchName(c,muChans)
            
        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(jesSignal)
        sigSample.addSystematic(xsecSig)
        if sig.startswith("SM"):
            from SystematicsUtils import getISRSyst
            isrSyst = getISRSyst(sig)
            sigSample.addSystematic(isrSyst)
            pass
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if ValidRegList["SRTight"] or doTableInputs:
                #don't re-create already existing channel, but unset as Validation and set as Signal channel
                channame = sr + "_meffInc30"
                if channame in myTopLvl.channels:
                    ch = myTopLvl.getChannel("meffInc30",[sr])
                    iPop=myTopLvl.validationChannels.index(sr+"_meffInc30")
                    myTopLvl.validationChannels.pop(iPop)
            else:            
                if sr=="SR5jTEl" or sr=="SR5jTMu":
                    ch = myTopLvl.addChannel("meffInc30",[sr],meffNBinsSR,meffBinLowSR,meffBinHighSR)
                elif sr=="SR3jTEl" or sr=="SR3jTMu" or sr=="SR6jTEl" or sr=="SR6jTMu":
                    ch = bkgOnly.addChannel("met",[sr],metNBinsSR,metBinLowSR,metBinHighSR)

                    #for theSample in ch.sampleList:          
                    #    theSample.removeSystematic("JHigh")
                    #    theSample.removeSystematic("JMedium")
                    #    theSample.removeSystematic("JLow")  
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                
                
                if sr=="SR3jTEl" or sr=="SR5jTEl" or sr=="SR6jTEl":
                    elChans.append(ch)
                elif sr=="SR4jTMu" or sr=="SR5jTMu" or sr=="SR6jTMu":
                    muChans.append(ch)
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                pass
            
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.useOverflowBin=True
            bAgnosticChans.append(ch)

#            # add JES as ShapeSys to each channel      
#            jesname = "JES_" + ch.name
#            if jesname not in ch.systDict.keys():
#                tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#                ch.addSystematic(tmpJES)
        

##############################
# Finalize fit configs setup #
##############################

# b-tag reg/veto/agnostic channels
for chan in bReqChans:
    chan.hasBQCD = True
#    chan.addSystematic(bTagSyst)

for chan in bVetoChans:
    chan.hasBQCD = False
#    chan.addSystematic(bTagSyst)

for chan in bAgnosticChans:
    chan.hasBQCD = False
    #chan.removeWeight("bTagWeight[3]")

AllChannels = bReqChans + bVetoChans + bAgnosticChans

# Generator Systematics for each sample,channel
print "** Generator Systematics **"
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan   = tgt[1]
    for chan in AllChannels:
        if tgtchan=="All" or tgtchan==chan.name:
            chan.getSample(tgtsample).addSystematic(syst)
            print "Add Generator Systematics (",syst.name,") to (",chan.name,")"


SetupChannels(elChans,bgdFiles_e, elChanSyst)
SetupChannels(muChans,bgdFiles_m, muChanSyst)

##Final semi-hacks for signal samples in exclusion fits

if doExclusion_mSUGRA:
    for sig in sigSamples:
        myTopLvl=configMgr.getTopLevelXML("Sig_%s"%sig)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)           
            theSample.removeSystematic("JHigh")
            theSample.removeSystematic("JMedium")
            theSample.removeSystematic("JLow")              
            theSigFiles=[]
            if chan in elChans:
                theSigFiles = sigFiles_e
            elif chan in muChans:
                theSigFiles = sigFiles_m

            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)




#######################
## Cosmetic Settings ##
#######################
# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)

#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)

#entry.SetFillColor(bkgOnly.errorFillColor)
#entry.SetFillStyle(bkgOnly.errorFillStyle)
#
#entry = leg.AddEntry("","multijets (data estimate)","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
###
#entry = leg.AddEntry("","W+jets","lf")
#entry.SetLineColor(WSample.color)
#entry.SetFillColor(WSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Z+jets","lf")
#entry.SetLineColor(ZSample.color)
#entry.SetFillColor(ZSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","t#bar{t}","lf")
#entry.SetLineColor(TTbarSample.color)
#entry.SetFillColor(TTbarSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","single top","lf")
#entry.SetLineColor(SingleTopSample.color)
#entry.SetFillColor(SingleTopSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","diboson","lf")
#entry.SetLineColor(DibosonsSample.color)
#entry.SetFillColor(DibosonsSample.color)
#entry.SetFillStyle(compFillStyle)
#

#entry = leg.AddEntry("","W0jet","lf")
#entry.SetLineColor(W0Sample.color)
#entry.SetFillColor(W0Sample.color)
#entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","Wjet","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t} Powheg","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}0jet","lf")
## entry.SetLineColor(TTbarlnln0Sample.color)
## entry.SetFillColor(TTbarlnln0Sample.color)
## entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}1jet","lf")
## entry.SetLineColor(TTbarlnln1Sample.color)
## entry.SetFillColor(TTbarlnln1Sample.color)
## entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}2jet","lf")
## entry.SetLineColor(TTbarlnln2Sample.color)
## entry.SetFillColor(TTbarlnln2Sample.color)
## entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}3jet","lf")
## entry.SetLineColor(TTbarlnln3Sample.color)
## entry.SetFillColor(TTbarlnln3Sample.color)
## entry.SetFillStyle(compFillStyle)

#entry = leg.AddEntry("","Z","lf")
#entry.SetLineColor(ZSample.color)
#entry.SetFillColor(ZSample.color)
#entry.SetFillStyle(compFillStyle)
##
#entry = leg.AddEntry("","ST","lf")
#entry.SetLineColor(SingleTopSample.color)
#entry.SetFillColor(SingleTopSample.color)
#entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","Dib./ST/Z","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","Multijet","lf")
entry.SetLineColor(QCDSample.color)
entry.SetFillColor(QCDSample.color)
entry.SetFillStyle(compFillStyle)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation : validation.tLegend = leg
c.Close()

# Plot "ATLAS" label
for chan in AllChannels:
    chan.titleY = "Entries"
    chan.logY = True
    if chan.logY:
        chan.minY = 1.5
        chan.maxY = 100000
    else:
        chan.minY = 0.05 
        chan.maxY = 3000
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "internal"
    chan.ShowLumi = True
