
################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
#from configWriter import TopLevelXML,Measurement,ChannelXML,Sample 
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

from logger import Logger
log = Logger('HardLepton')

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupChannels_Hack(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
#            chan.getSample("AlpgenW").addSystematic(syst)
            chan.getSample("AlpgenJimmyW").addSystematic(syst)
    return
    
def SetupChannels_nofiles(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.getSample("PowhegPythiaTTbar").addSystematic(syst)
#            chan.getSample("AlpgenW").addSystematic(syst)
            chan.getSample("AlpgenJimmyW").addSystematic(syst)
    return   

# ********************************************************************* #
#                              Debug
# ********************************************************************* #


# ********************************************************************* #
#                              Main part
# ********************************************************************* #
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

useHardLepCR=True
useStat=True
#doExclusion_mSUGRA=True # not needed anymore  - use exclusion fit option in HistFitter
useNJetNormFac=True
useTTbarReweighting=False
remapOfSR = False

SystList=[]
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("JER")      # Jet Energy Resolution (common)
#SystList.append("LepEff")   # Lepton efficiency (e&m) #####negligible (1 %)
#SystList.append("LepTrig")  # Trigger efficiency (e&m)
SystList.append("ResoSt")   # CellOut energy resolution uncertainty
SystList.append("ScaleSt")  # CellOut energy scale uncertainty 
#SystList.append("EES")      # Electron Energy Scale (e only)
#SystList.append("MER")      # Muon Energy Resolution (m only)
SystList.append("PileUp")      # Pile-up
SystList.append("BTag")        # Flat Btag uncertainty of 15% for now

#SystList.append("GenW")     # Generator Systematics W    (common)
SystList.append("GenTTbar") # Generator Systematics TTbar(common)

doTableInputs=False #This effectively means no validation plots but only validation tables (but is 100x faster)
ValidRegList={}
ValidRegList["SRTight"] = False
ValidRegList["VR3JhighMET"] = False
ValidRegList["VR3JhighMT"] = False
ValidRegList["VR5JhighMET"] = False
ValidRegList["VR5JhighMT"] = False
ValidRegList["VR6JhighMET"] = False
ValidRegList["VR6JhighMT"] = False
ValidRegList["INCL"] = False
ValidRegList["WRV"] = False
ValidRegList["TRV"] = False

#CRregions = ["3J","5J","6J"]
CRregions = ["3J"]

analysissuffix = ''
for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr

doDiscovery=True
doSignalOnly=False #Remove all bkgs for signal histo creation step
if configMgr.executeHistFactory:
    doSignalOnly=False
    
if not 'sigSamples' in dir():
    sigSamples=["SM_GG1step_1025_545_65"]


if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix += '_GG1stepx12'
    elif 'GG1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix += '_GG1stepgridx'
    elif 'SS1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix += '_SS1stepx12'
    elif 'SS1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix += '_SS1stepgridx' 
    elif 'GG2WWZZ' in sigSamples[0]:
        analysissuffix += '_GG2WWZZ'
    elif 'GG2CNsl' in sigSamples[0]:
        analysissuffix += '_GG2CNsl'    
    elif 'SS2WWZZ' in sigSamples[0]:
        analysissuffix += '_SS2WWZZ'
    elif 'SS2CNsl' in sigSamples[0]:
        analysissuffix += '_SS2CNsl' 
    elif 'pMSSM' in sigSamples[0]:
        analysissuffix += '_pMSSM'                
    elif 'HiggsSU' in sigSamples[0]:
        analysissuffix += '_HiggsSU' 

# First define HistFactory attributes
configMgr.analysisName = "OneLeptonMoriond2013_SingleBin_sepsys_elmu"+analysissuffix # Name to give the analysis
configMgr.outputFileName = "results/OneLeptonMoriond2013_SingleBin_sepsys_elmu"+analysissuffix+".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.7 #20.7
configMgr.setLumiUnits("fb-1")

#configMgr.daoHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

configMgr.writeXML = True

configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#Split bdgFiles per channel
sigFiles_e = []
sigFiles_m = []
#inputDir="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v3_2/"
#inputDir="/afs/cern.ch/user/k/koutsman/HistFitterUser/MET_jets_leptons/data/trees/v4_4/"
#inputDir="/afs/cern.ch/user/k/koutsman/work/Moriond2013/data/trees/v4_4_3/"
#inputDir="/afs/cern.ch/user/k/koutsman/scratch0/data/trees/v4_4/"
inputDir="/afs/cern.ch/work/k/koutsman/public/Moriond2013/data/trees/v8_2_3/"
#inputDir="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v5_5_1/"
inputDirSig="root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_2_3/"

onLxplus=True

if not onLxplus:
    print "INFO : Running locally...\n"
    inputDir="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_6_PowhegAlpgen/"
#    inputDir2="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_3_PowhegAlpgen/"
#    inputDirSLBkg="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_3_PowhegAlpgen/"
    inputDirSig="/atlas_tmp/urrejola/SUSYFitterTrees/v4_4_6_PowhegAlpgen/"
else:
    print "INFO : Running on lxplus... \n"

# Set the files to read from
bgdFiles_em = [inputDir+"bkgtree_HardEle.root",inputDir+"bkgtree_HardMuo.root"]
bgdFiles_e = [inputDir+"bkgtree_HardEle.root"] #,inputDir+"bkgtree_HardEle_AlpgenW.root",inputDir+"bkgtree_HardEle_Powheg.root", inputDir+"bkgtree_HardEle_SmallBkg.root"] #bkgtree_HardEle.root", ]#,inputDir+"datatree_HardEle.root"]
#bgdFiles_m = [inputDir+"bkgtree_HardMuo.root"]#,inputDir+"datatree_HardMuo.root"]
bgdFiles_m = [inputDir+"bkgtree_HardMuo.root"] #inputDir+"bkgtree_HardMuo_AlpgenW.root",inputDir+"bkgtree_HardMuo_Powheg.root", inputDir+"bkgtree_HardMuo_SmallBkg.root"] #bkgtree_HardMuo.root", ]#,inputDir+"datatree_HardMuo.root"]
if myFitType==FitType.Exclusion:
    sigFiles_e=[inputDirSig+"sigtree_HardEle_SU.root"]
    sigFiles_m=[inputDirSig+"sigtree_HardMuo_SU.root"]
    if 'SM_SS1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS1step.root"]
        print "Using simplified models SS onestepCC"
    if 'SM_GG1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
        print "Using simplified models GG onestepCC"
    if 'pMSSM' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_pMSSM.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_pMSSM.root"]
        print "Using pMSSM signal model"
    if 'GG2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ.root"]
        print "Using simplified models GG two step with WWZZ"
    if 'SS2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ.root"]
        print "Using simplified models SS two step with WWZZ"   
    if 'GG2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2CNsl.root"]
        print "Using simplified models GG two step with sleptons"
    if 'SS2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2CNsl.root"]
        print "Using simplified models SS two step with sleptons" 

# Map regions to cut strings
## #configMgr.cutsDict["WR"]="lep2Pt<10 && met>100 && met<180 && mt>100 && jet4Pt>80 && meffInc40>500 && nB4Jet==0"
## configMgr.cutsDict["WR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<80 && mt>40 && meffInc30>500"
## configMgr.cutsDict["TR3J"]="nJet30>2 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<80 && mt>40 && meffInc30>500"
## configMgr.cutsDict["WR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<150 && mt>80 && meffInc30>500"
## configMgr.cutsDict["TR5J"]="nJet30>4 && lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && jet2Pt>50 && jet3Pt>30 && jet4Pt>30 && jet5Pt>30 && met>100 && met<200 && nB3Jet30>0 && mt<150 && mt>80 && meffInc30>500"
## #
CommonSelection = "&& lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && met>100 && met<200 && meffInc30>500"
CommonSelection2 = "&& lep2Pt<10 && lep1Pt>25 && jet1Pt>80 && meffInc30>500"
OneEleMuoSelection = "&& ( ( AnalysisType==1 && ( EF_e24vh_medium1_EFxe35_tclcw || EF_e60_medium1 ) && isNoCrackElectron==1 && (abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta)) ) || ( AnalysisType==2 && ( EF_mu24_j65_a4tchad_EFxe40_tclcw ) && isNoCrackElectron==1 ) )"

configMgr.cutsDict["WR3J"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<30 &&  nB3Jet30==0 && mt<120 && mt>80" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["TR3J"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<30 && nB3Jet30>0 && mt<120 && mt>80" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["WR5J"]="nJet30>4 && jet2Pt>50 && jet5Pt>30  && jet6Pt<30  && nB3Jet30==0 && mt<120 && mt>60" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["TR5J"]="nJet30>4 && jet2Pt>50 && jet5Pt>30 && jet6Pt<30 && nB3Jet30>0 && mt<120 && mt>60" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["WR6J"]="nJet30>5 && jet2Pt>50 && jet6Pt>30 && nB3Jet30==0 && mt<120 && mt>40" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["TR6J"]="nJet30>5 && jet2Pt>50 && jet6Pt>30 && nB3Jet30>0 && mt<120 && mt>40" + CommonSelection + OneEleMuoSelection

configMgr.cutsDict["VR3JhighMET"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && met>200 && met<500 && mt<120 && mt>100 && jet5Pt<40"  + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["VR5JhighMET"]="nJet40>4 && jet2Pt>50 && jet5Pt>40 && met>200 && met<500 && mt<120 && mt>60 && jet6Pt<40" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["VR6JhighMET"]="nJet40>5 && jet2Pt>50 && jet6Pt>40 && met>200 && met<500 && mt<120 && mt>60" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["VR3JhighMT"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && met>100 && met<250 && mt<320 && mt>120  && jet5Pt<40" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["VR5JhighMT"]="nJet40>4 && jet2Pt>50 && jet5Pt>40 && met>100 && met<250 && mt<320 && mt>120  && jet6Pt<40" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["VR6JhighMT"]="nJet40>5 && jet2Pt>50 && jet6Pt>40 && met>100 && met<250 && mt<320 && mt>120" + CommonSelection2 + OneEleMuoSelection

configMgr.cutsDict["WR3JMET"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 &&  nB3Jet30==0 && mt<120 && mt>100" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["TR3JMET"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 && nB3Jet30>0 && mt<120 && mt>100" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["WR5JMET"]="nJet30>4 && jet2Pt>50 && jet5Pt>40  && jet6Pt<40  && nB3Jet30==0 && mt<120 && mt>60" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["TR5JMET"]="nJet30>4 && jet2Pt>50 && jet5Pt>40 && jet6Pt<40 && nB3Jet30>0 && mt<120 && mt>60" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["WR6JMET"]="nJet30>5 && jet2Pt>50 && jet6Pt>40 && nB3Jet30==0 && mt<120 && mt>60" + CommonSelection2 + OneEleMuoSelection
configMgr.cutsDict["TR6JMET"]="nJet30>5 && jet2Pt>50 && jet6Pt>40 && nB3Jet30>0 && mt<120 && mt>60" + CommonSelection2 + OneEleMuoSelection

configMgr.cutsDict["WR3JMT"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 &&  nB3Jet30==0" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["TR3JMT"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 && nB3Jet30>0" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["WR5JMT"]="nJet30>4 && jet2Pt>50 && jet5Pt>40  && jet6Pt<40  && nB3Jet30==0" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["TR5JMT"]="nJet30>4 && jet2Pt>50 && jet5Pt>40 && jet6Pt<40 && nB3Jet30>0" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["WR6JMT"]="nJet30>5 && jet2Pt>50 && jet6Pt>40 && nB3Jet30==0" + CommonSelection + OneEleMuoSelection
configMgr.cutsDict["TR6JMT"]="nJet30>5 && jet2Pt>50 && jet6Pt>40 && nB3Jet30>0" + CommonSelection + OneEleMuoSelection

configMgr.cutsDict["INCL"]="lep2Pt<10 && lep1Pt>25" + OneEleMuoSelection

trigger_bias_bug = "&& met>100"

configMgr.cutsDict["SR3J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && jet5Pt<30 && met/meff3Jet30>0.3 && meffInc30>800" + OneEleMuoSelection
configMgr.cutsDict["SR5J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt>80 && jet2Pt>50 && jet5Pt>30 && jet6Pt<30 && meffInc30>800" + OneEleMuoSelection
configMgr.cutsDict["SR6J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt>80 && jet2Pt>50 && jet6Pt>30 && meffInc30>600" + OneEleMuoSelection

configMgr.cutsDict["SR3Jloose"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>120 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 && met/meff3Jet30>0.3 && meffInc30>800" + OneEleMuoSelection
configMgr.cutsDict["SR5Jloose"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>120 && jet1Pt>80 && jet2Pt>50 && jet5Pt>40 && jet6Pt<40 && meffInc30>800" + OneEleMuoSelection
configMgr.cutsDict["SR6Jloose"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>120 && jet1Pt>80 && jet2Pt>50 && jet6Pt>40 && meffInc30>600" + OneEleMuoSelection


################
#     TTbar reweighting     #
################

## ttbar Reweighting
#ttbarReweight3JetNomWeight = "1"
#ttbarReweight5JetNomWeight = "1"
# p1181
# 3Jet
#ttbarReweight3JetPos = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.337832 ) * ( 1 + ( -1.0e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt <  1000) ) ) )"
#ttbarReweight3JetNom = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.609560 ) * ( 1 + ( -1.5e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt <   666) ) ) )"
#ttbarReweight3JetNeg = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 2.015725 ) * ( 1 + ( -2.0e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt <   500) ) ) )"
# 5Jet
#ttbarReweight5JetPos = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.266848 ) * ( 1 + ( -1.0e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt <  1000) ) ) )"
#ttbarReweight5JetNom = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.461678 ) * ( 1 + ( -1.5e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt <   666) ) ) )"
#ttbarReweight5JetNeg = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.725811 ) * ( 1 + ( -2.0e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt <   500) ) ) )"

# p1328
#ttbarReweight3JetNomWeight = "( 1 + ( DatasetNumber==105861 ) * ( -1 + ( ( 1.300856e+00 ) * ( 1 + ( -0.900000e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt < 1111 ) ) ) )"

## Lists of weights 

bTagWeight_Dummy = "1."
cTagWeight_Dummy = "1."
mTagWeight_Dummy = "1."

weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","bTagWeight[3]"]#,ttbarReweight3JetNomWeight] #,"bTagWeight[5]"]
#weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

#ttbarNoReweight = replaceWeight(weights,ttbarReweight3JetNomWeight,"1")

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#bTagHighWeights = replaceWeight(weights,bTagWeight_Dummy,"bTagWeightBUp[3]")
#bTagLowWeights = replaceWeight(weights,bTagWeight_Dummy,"bTagWeightBDown[3]")

#bTagHighWeights = replaceWeight(weights,bTagWeight_Dummy,"(1.+((bTagWeightBUp[4]-bTagWeightBDown[4])/(bTagWeightBUp[4]+bTagWeightBDown[4])))")
#bTagLowWeights = replaceWeight(weights,bTagWeight_Dummy,"(1.-((bTagWeightBUp[4]-bTagWeightBDown[4])/(bTagWeightBUp[4]+bTagWeightBDown[4])))")

#cTagHighWeights = replaceWeight(weights,cTagWeight_Dummy,"(1.+((bTagWeightCUp[4]-bTagWeightCDown[4])/(bTagWeightCUp[4]+bTagWeightCDown[4])))")
#cTagLowWeights = replaceWeight(weights,cTagWeight_Dummy,"(1.-((bTagWeightCUp[4]-bTagWeightCDown[4])/(bTagWeightCUp[4]+bTagWeightCDown[4])))")

#mTagHighWeights = replaceWeight(weights,mTagWeight_Dummy,"(1.+((bTagWeightMUp[4]-bTagWeightMDown[4])/(bTagWeightMUp[4]+bTagWeightMDown[4])))")
#mTagLowWeights = replaceWeight(weights,mTagWeight_Dummy,"(1.-((bTagWeightMUp[4]-bTagWeightMDown[4])/(bTagWeightMUp[4]+bTagWeightMDown[4])))")

#bTagHighWeights_30 = replaceWeight(weights,bTagWeight_Dummy,"(1.+((bTagWeightBUp[3]-bTagWeightBDown[3])/(bTagWeightBUp[3]+bTagWeightBDown[3])))")
#bTagLowWeights_30 = replaceWeight(weights,bTagWeight_Dummy,"(1.-((bTagWeightBUp[3]-bTagWeightBDown[3])/(bTagWeightBUp[3]+bTagWeightBDown[3])))")

#cTagHighWeights_30 = replaceWeight(weights,cTagWeight_Dummy,"(1.+((bTagWeightCUp[3]-bTagWeightCDown[3])/(bTagWeightCUp[3]+bTagWeightCDown[3])))")
#cTagLowWeights_30 = replaceWeight(weights,cTagWeight_Dummy,"(1.-((bTagWeightCUp[3]-bTagWeightCDown[3])/(bTagWeightCUp[3]+bTagWeightCDown[3])))")

#mTagHighWeights_30 = replaceWeight(weights,mTagWeight_Dummy,"(1.+((bTagWeightMUp[3]-bTagWeightMDown[3])/(bTagWeightMUp[3]+bTagWeightMDown[3])))")
#mTagLowWeights_30 = replaceWeight(weights,mTagWeight_Dummy,"(1.-((bTagWeightMUp[3]-bTagWeightMDown[3])/(bTagWeightMUp[3]+bTagWeightMDown[3])))")

#bTagHighWeights = replaceWeight(weights,bTagWeight_Dummy,"(1.+((bTagWeightBUp[3]-bTagWeight[3])/bTagWeight[3]))")
#bTagLowWeights = replaceWeight(weights,bTagWeight_Dummy,"(1.+((bTagWeightBDown[3]-bTagWeight[3])/bTagWeight[3]))")

#bTagHighWeights = replaceWeight(weights,bTagWeight_Dummy,"(1.+ sqrt( pow( ((bTagWeightBUp[3]-bTagWeight[3])/bTagWeight[3]),2) +  pow( ((bTagWeightCUp[3]-bTagWeight[3])/bTagWeight[3]),2) ) )")
#bTagLowWeights = replaceWeight(weights,bTagWeight_Dummy,"(1.- sqrt( pow(((bTagWeightBDown[3]-bTagWeight[3])/bTagWeight[3]),2) +  pow(((bTagWeightCDown[3]-bTagWeight[3])/bTagWeight[3]),2) ) )")

#cTagHighWeights = replaceWeight(weights,cTagWeight_Dummy,"(1.+((bTagWeightCUp[3]-bTagWeight[3])/bTagWeight[3]))")
#cTagLowWeights = replaceWeight(weights,cTagWeight_Dummy,"(1.+((bTagWeightCDown[3]-bTagWeight[3])/bTagWeight[3]))")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightBUp[3]")
    bTagLowWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightBDown[3]")

    cTagHighWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightCUp[3]")
    cTagLowWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightCDown[3]")

    mTagHighWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightMUp[3]")
    mTagLowWeights = replaceWeight(weights,"bTagWeight[3]","bTagWeightMDown[3]")

    bTagHighWeights_30 = replaceWeight(weights,"bTagWeight[3]","bTagWeightBUp[3]")
    bTagLowWeights_30 = replaceWeight(weights,"bTagWeight[3]","bTagWeightBDown[3]")

    cTagHighWeights_30 = replaceWeight(weights,"bTagWeight[3]","bTagWeightCUp[3]")
    cTagLowWeights_30 = replaceWeight(weights,"bTagWeight[3]","bTagWeightCDown[3]")

    mTagHighWeights_30 = replaceWeight(weights,"bTagWeight[3]","bTagWeightMUp[3]")
    mTagLowWeights_30 = replaceWeight(weights,"bTagWeight[3]","bTagWeightMDown[3]")


#mTagHighWeights = replaceWeight(weights,mTagWeight_Dummy,"(1.+((bTagWeightMUp[3]-bTagWeight[3])/bTagWeight[3]))")
#mTagLowWeights = replaceWeight(weights,mTagWeight_Dummy,"(1.+((bTagWeightMDown[3]-bTagWeight[3])/bTagWeight[3]))")

#bTagHighWeights_30 = replaceWeight(weights,bTagWeight_Dummy,"(1.+((bTagWeightBUp[3]-bTagWeight[3])/bTagWeight[3]))")
#bTagLowWeights_30 = replaceWeight(weights,bTagWeight_Dummy,"(1.+((bTagWeightBDown[3]-bTagWeight[3])/bTagWeight[3]))")

#bTagHighWeights_30 = replaceWeight(weights,bTagWeight_Dummy,"(1.+ sqrt( pow(((bTagWeightBUp[3]-bTagWeight[3])/bTagWeight[3]),2) +  pow(((bTagWeightCUp[3]-bTagWeight[3])/bTagWeight[3]),2) ) )")
#bTagLowWeights_30 = replaceWeight(weights,bTagWeight_Dummy,"(1.- sqrt( pow(((bTagWeightBDown[3]-bTagWeight[3])/bTagWeight[3]),2) +  pow(((bTagWeightCDown[3]-bTagWeight[3])/bTagWeight[3]),2) ) )")

#cTagHighWeights_30 = replaceWeight(weights,cTagWeight_Dummy,"(1.+((bTagWeightCUp[3]-bTagWeight[3])/bTagWeight[3]))")
#cTagLowWeights_30 = replaceWeight(weights,cTagWeight_Dummy,"(1.+((bTagWeightCDown[3]-bTagWeight[3])/bTagWeight[3]))")

#mTagHighWeights_30 = replaceWeight(weights,mTagWeight_Dummy,"(1.+((bTagWeightMUp[3]-bTagWeight[3])/bTagWeight[3]))")
#mTagLowWeights_30 = replaceWeight(weights,mTagWeight_Dummy,"(1.+((bTagWeightMDown[3]-bTagWeight[3])/bTagWeight[3]))")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights,"leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

#########################
## List of systematics ##
#########################

# Signal XSec uncertainty as overallSys (pure yeild affect) DEPRECATED
xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig","_NoSys","_JESup","_JESdown","tree","overallHistoSys")

#temporary JES for the ttbar samples


basicChanSyst = {}
elmuChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
BGerr = {}
qfacW = {}
ktfacW = {}
iqoptW = {}

for region in CRregions:
    
    basicChanSyst[region] = []
    elmuChanSyst[region] = []

    if "JES"     in SystList :basicChanSyst[region].append(Systematic("JES_"+region,"_NoSys","_JESup","_JESdown","tree","overallNormHistoSys")) # JES uncertainty - for low pt jets
    #if "JES"     in SystList :basicChanSyst[region].append(Systematic("J1_"+region,"_NoSys","_JESLowup","_JESLowdown","tree","overallNormHistoSys")) # JES uncertainty - for low pt jets #overallNormHistoSys
    #if "JES"     in SystList :basicChanSyst[region].append(Systematic("J2_"+region,"_NoSys","_JESMediumup","_JESMediumdown","tree","overallNormHistoSys")) # JES uncertainty - for medium pt jets
    #if "JES"     in SystList :basicChanSyst[region].append(Systematic("J3_"+region,"_NoSys","_JESHighup","_JESHighdown","tree","overallNormHistoSys")) # JES uncertainty - for high pt jets
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP1_"+region,"_NoSys","_EffectiveNP1up","_EffectiveNP1down","tree","overallNormHistoSys")) # JES uncertainty
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP2_"+region,"_NoSys","_EffectiveNP2up","_EffectiveNP2down","tree","overallNormHistoSys")) # JES uncertainty
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP3_",+region"_NoSys","_EffectiveNP3up","_EffectiveNP3down","tree","overallNormHistoSys")) # JES uncertainty
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP4_"+region,"_NoSys","_EffectiveNP4up","_EffectiveNP4down","tree","overallNormHistoSys")) # JES uncertainty
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP5_"+region,"_NoSys","_EffectiveNP5up","_EffectiveNP5down","tree","overallNormHistoSys")) # JES uncertainty
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP6_"+region,"_NoSys","_EffectiveNP6up","_EffectiveNP6down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_6restTerm
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP7_",+region"_NoSys","_EffectiveNP7up","_EffectiveNP7down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_EtaIntercalibration_Modelling
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP8_"+region,"_NoSys","_EffectiveNP8up","_EffectiveNP8down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_EtaIntercalibration_StatAndMethod
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP9_"+region,"_NoSys","_EffectiveNP9up","_EffectiveNP9down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_Pileup_OffsetMu
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP10_"+region,"_NoSys","_EffectiveNP10up","_EffectiveNP10down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_Pileup_OffsetNPV
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP11_"+region,"_NoSys","_EffectiveNP11up","_EffectiveNP11down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_Pileup_PtTerm
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP12_"+region,"_NoSys","_EffectiveNP12up","_EffectiveNP12down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_Pileup_RhoTopology
    ## #if "JES"     in SystList :basicChanSyst[region].append(Systematic("EffectiveNP13_"+region,"_NoSys","_EffectiveNP13up","_EffectiveNP13down","tree","overallNormHistoSys")) # JES uncertainty: EffectiveNP_SingleParticle_HighPt
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("BJes_"+region,"_NoSys","_BJesup","_BJesdown","tree","overallNormHistoSys")) # Multi - JES uncertainty: to be applied to b-jets only, alternative to JESFlavour
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("JESFlavour_"+region,"_NoSys","_JESFlavourup","_JESFlavourdown","tree","overallNormHistoSys")) # Multi - JES uncertainty
    ## if "JES"     in SystList :basicChanSyst[region].append(Systematic("JESCloseBy_"+region,"_NoSys","_JESCloseByup","_JESCloseBydown","tree","overallNormHistoSys")) # Multi - JES uncertainty

    #if "JER"     in SystList : basicChanSyst[region].append(Systematic(    "JER_"+region,"_NoSys","_JER"      ,"_JER"        ,"tree","histoSysOneSide"))
    if "JER"     in SystList : basicChanSyst[region].append(Systematic(    "JER_"+region,"_NoSys","_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym")) #overallNormHistoSysOneSideSym

    ## MET soft term resolution and scale
    if "ScaleSt" in SystList : basicChanSyst[region].append(Systematic("SCALEST_"+region,"_NoSys","_SCALESTup","_SCALESTdown","tree","overallNormHistoSys")) #overallNormHistoSys
    if "ResoSt"  in SystList : basicChanSyst[region].append(Systematic( "RESOST_"+region,"_NoSys","_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym")) #overallNormHistoSysOneSideSym
    #if "ScaleSt" in SystList : basicChanSyst[region].append(Systematic("SCALEST_"+region,"_NoSys","_SCALESTup","_SCALESTdown","tree","histoSys"))
    #if "ResoSt"  in SystList : basicChanSyst[region].append(Systematic( "RESOST_"+region,"_NoSys","_RESOST" ,"_RESOST" ,"tree","histoSysOneSide"))

    ## pile-up
    if "PileUp" in SystList : basicChanSyst[region].append( Systematic("pileup_"+region, configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys"))#"overallSys")) #overallNormHistoSys
    #if "PileUp" in SystList : basicChanSyst[region].append( Systematic("pileup_"+region, configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "histoSys"))#"overallSys"))

    ## b-tagging, flat 15% for now as proposed by Yuichi
    #if "BTag" in SystList : basicChanSyst[region].append( Systematic("BT_"+region, configMgr.weights, 1.15, 0.85, "user", "userOverallSys"))

    # Lepton weight uncertainty
    if "LepEff" in SystList : basicChanSyst[region].append( Systematic("LE_"+region,configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys"))

    # Lepton weight uncertainty
    #if "LepEff" in SystList:
    #    elChanSyst[region].append(Systematic("LEel_"+region,configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")) 
    #    muChanSyst[region].append(Systematic("LEmu_"+region,configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys"))
    #lepEff= Systematic("LE_"+region,configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys")

    # Trigger efficiency
    if "LepTrig" in SystList:
        elmuChanSyst[region].append(Systematic("TEel_"+region,configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))
        #muChanSyst[region].append(Systematic("TEmu_"+region,configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys"))

    # Electron energy scale uncertainty
    #if "EES" in SystList:
    #    #    elChanSyst[region].append(Systematic("LESel_"+region,"_NoSys","_LESup","_LESdown","tree","overallSys")) 
    #    elChanSyst[region].append(Systematic("egzee_"+region,"_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys"))
    #    elChanSyst[region].append(Systematic("egmat_"+region,"_NoSys","_EGMATup","_EGMATdown","tree","overallSys"))
    #    elChanSyst[region].append(Systematic("egps_"+region, "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys"))
    #    elChanSyst[region].append(Systematic("eglow_"+region,"_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys"))
    #    elChanSyst[region].append(Systematic("egres_"+region,"_NoSys","_EGRESup","_EGRESdown","tree","overallSys"))

    # Muon energy resolutions
    #if "MER" in SystList:
    #    muChanSyst[region].append(Systematic("LRMmu_"+region,"_NoSys","_MMSup","_MMSdown","tree","overallSys"))
    #    muChanSyst[region].append(Systematic("LRImu_"+region,"_NoSys","_MIDup","_MIDdown","tree","overallSys")) 

    if "BTag" in SystList:
        bTagSyst[region] = Systematic("BT_"+region,configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region] = Systematic("CT_"+region,configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region] = Systematic("MisT_"+region,configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")

    BGerr[region] = Systematic("errBG_"+region, configMgr.weights,1.2 ,0.8, "user","userOverallSys")



    # Generator Systematics
    generatorSyst = []
    #topPtMin30HLCR = Systematic("PtMinTop",configMgr.weights,[1.08,1.05,1.003,1.001,1.001,1.004,1.001],[0.999,0.999,0.999,0.98,0.95,0.999,0.9],"user","userNormHistoSys")
    #wzPtMin30HLCR = Systematic("PtMinWZ",configMgr.weights,[1.001,1.001,1.006,1.06,1.2,1.02,1.06],[0.999,0.98,0.999,0.999,0.999,0.999,0.999],"user","userNormHistoSys")

    ## bin1 = 1. - 493/(493 + 469)/2  # -0.03
    ## bin2 = 1. - 1045/(1045 + 1069)/2. # 0.05
    ## bin3 = 1. - 1075/(1075 + 1143)/2. 
    ## bin4 = 1. - 657/(657 + 695)/2. 
    ## bin5 = 1. - 146/(146 + 165)/2. 

    ## topTheoRenScWR3JEl = Systematic("topTheoRenSc",configMgr.weights,[1.-bin1,1.-bin2,1.-bin3,1.-bin4,1.-bin5],[1.+bin1,1.+bin2,1.+bin3,1.+bin4,1.+bin5],"user","userNormHistoSys")
    #topTheo = Systematic("topTheo",configMgr.weights,0.75,1.25,"user","userOverallSys")

    # W+jets generator systematics
    qfacW[region]  = Systematic("qfacW_"+region,configMgr.weights,configMgr.weights+["qfacUpWeightW"],configMgr.weights+["qfacDownWeightW"],"weight","overallNormHistoSys")
    ktfacW[region] = Systematic("ktfacW_"+region,configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","overallNormHistoSys")
    iqoptW[region] = Systematic("iqoptW_"+region,configMgr.weights,configMgr.weights+["iqopt2WeightW"],configMgr.weights+["iqopt3WeightW"],"weight","overallNormHistoSys")#

# ttbar reweighting systematic
#ttbarRew = Systematic("ttbarReweight", configMgr.weights, ttbarNoReweight, ttbarNoReweight, "weight", "histoSysOneSideSym")

# theory uncertainties kept in separate file
from theoryUncertainties_hardLepton_Moriond2013_single import *

if "GenW"   in SystList:
    #    SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
    #    generatorSyst.append((("AlpgenJimmyW","meffInc40_SR4jTEl"), SystGenW)) # Only applied to SR.
    #    generatorSyst.append((("AlpgenJimmyW","meffInc40_SR4jTMu"), SystGenW)) # Only applied to SR.
    
    # AlpgenW systematic varying the ptMin parameter (Wmunu samples 177047-177052)
    # AK: used here as dummy, as Sherpa scale uncertainties not yet produced
    # only Wmunu samples are produced with ptmin variation, take for el the same uncertainty
    generatorSyst.append((("AlpgenJimmyW","WR3J"), WTheoPtMinWR3JMu))
    generatorSyst.append((("AlpgenJimmyW","TR3J"), WTheoPtMinTR3JMu))
    generatorSyst.append((("AlpgenJimmyW","WR5J"), WTheoPtMinWR5JMu))
    generatorSyst.append((("AlpgenJimmyW","TR5J"), WTheoPtMinTR5JMu))
    generatorSyst.append((("AlpgenJimmyW","WR6J"), WTheoPtMinWR6JMu))
    generatorSyst.append((("AlpgenJimmyW","TR6J"), WTheoPtMinTR6JMu))

    generatorSyst.append((("AlpgenJimmyW","SR5J"), WTheoPtMinSR5JMu))
    generatorSyst.append((("AlpgenJimmyW","SR3J"), WTheoPtMinSR3JMu))
    generatorSyst.append((("AlpgenJimmyW","SR6J"), WTheoPtMinSR6JMu))

    generatorSyst.append((("AlpgenJimmyW","VR3JhighMET"), WTheoPtMinVR3JhighMETMu))
    generatorSyst.append((("AlpgenJimmyW","VR3JhighMT"), WTheoPtMinVR3JhighMTMu))
    generatorSyst.append((("AlpgenJimmyW","VR5JhighMET"), WTheoPtMinVR5JhighMETMu))
    generatorSyst.append((("AlpgenJimmyW","VR5JhighMT"), WTheoPtMinVR5JhighMTMu))
    generatorSyst.append((("AlpgenJimmyW","VR6JhighMET"), WTheoPtMinVR6JhighMETMu))
    generatorSyst.append((("AlpgenJimmyW","VR6JhighMT"), WTheoPtMinVR6JhighMTMu))

    # Finite Npartons: samples 176064-176066 (= 4 extra partons) to be compare with 166994-166997 (4-5 extra partons) 
    #generatorSyst.append((("AlpgenJimmyW","WR3JEl"), WTheoNpartWR3JEl))
    #generatorSyst.append((("AlpgenJimmyW","WR3JMu"), WTheoNpartWR3JMu))
    #generatorSyst.append((("AlpgenJimmyW","TR3JEl"), WTheoNpartTR3JEl))
    #generatorSyst.append((("AlpgenJimmyW","TR3JMu"), WTheoNpartTR3JMu))
    #generatorSyst.append((("AlpgenJimmyW","WR5JEl"), WTheoNpartWR5JEl))
    #generatorSyst.append((("AlpgenJimmyW","WR5JMu"), WTheoNpartWR5JMu))
    #generatorSyst.append((("AlpgenJimmyW","TR5JEl"), WTheoNpartTR5JEl))
    #generatorSyst.append((("AlpgenJimmyW","TR5JMu"), WTheoNpartTR5JMu))
    #generatorSyst.append((("AlpgenJimmyW","WR6JEl"), WTheoNpartWR6JEl))
    #generatorSyst.append((("AlpgenJimmyW","WR6JMu"), WTheoNpartWR6JMu))
    #generatorSyst.append((("AlpgenJimmyW","TR6JEl"), WTheoNpartTR6JEl))
    #generatorSyst.append((("AlpgenJimmyW","TR6JMu"), WTheoNpartTR6JMu))

    #generatorSyst.append((("AlpgenJimmyW","SR5JEl"), WTheoNpartSR5JEl))
    #generatorSyst.append((("AlpgenJimmyW","SR5JMu"), WTheoNpartSR5JMu))
    #generatorSyst.append((("AlpgenJimmyW","SR3JEl"), WTheoNpartSR3JEl))
    #generatorSyst.append((("AlpgenJimmyW","SR3JMu"), WTheoNpartSR3JMu))
    #generatorSyst.append((("AlpgenJimmyW","SR6JEl"), WTheoNpartSR6JEl))
    #generatorSyst.append((("AlpgenJimmyW","SR6JMu"), WTheoNpartSR6JMu))

    #generatorSyst.append((("AlpgenJimmyW","VR3JhighMETEl"), WTheoNpartVR3JhighMETEl))
    #generatorSyst.append((("AlpgenJimmyW","VR3JhighMETMu"), WTheoNpartVR3JhighMETMu))
    #generatorSyst.append((("AlpgenJimmyW","VR3JhighMTEl"), WTheoNpartVR3JhighMTEl))
    #generatorSyst.append((("AlpgenJimmyW","VR3JhighMTMu"), WTheoNpartVR3JhighMTMu))
    #generatorSyst.append((("AlpgenJimmyW","VR5JhighMETEl"), WTheoNpartVR5JhighMETEl))
    #generatorSyst.append((("AlpgenJimmyW","VR5JhighMETMu"), WTheoNpartVR5JhighMETMu))
    #generatorSyst.append((("AlpgenJimmyW","VR5JhighMTEl"), WTheoNpartVR5JhighMTEl))
    #generatorSyst.append((("AlpgenJimmyW","VR5JhighMTMu"), WTheoNpartVR5JhighMTMu))
    #generatorSyst.append((("AlpgenJimmyW","VR6JhighMETEl"), WTheoNpartVR6JhighMETEl))
    #generatorSyst.append((("AlpgenJimmyW","VR6JhighMETMu"), WTheoNpartVR6JhighMETMu))
    #generatorSyst.append((("AlpgenJimmyW","VR6JhighMTEl"), WTheoNpartVR6JhighMTEl))
    #generatorSyst.append((("AlpgenJimmyW","VR6JhighMTMu"), WTheoNpartVR6JhighMTMu))

if "GenTTbar" in SystList:

    # ISR/FSR uncertainty calculated by comparing AcerMCPythia with morePS (117209) and lessPS (117210) 
    generatorSyst.append((("PowhegPythiaTTbar","WR3J"), topTheoPSWR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR3J"), topTheoPSTR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","WR5J"), topTheoPSWR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR5J"), topTheoPSTR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","WR6J"), topTheoPSWR6JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR6J"), topTheoPSTR6JMu))

    generatorSyst.append((("PowhegPythiaTTbar","SR5J"), topTheoPSSR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","SR3J"), topTheoPSSR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","SR6J"), topTheoPSSR6JMu))

    generatorSyst.append((("PowhegPythiaTTbar","VR3JhighMET"), topTheoPSVR3JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR3JhighMT"), topTheoPSVR3JhighMTMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR5JhighMET"), topTheoPSVR5JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR5JhighMT"), topTheoPSVR5JhighMTMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR6JhighMET"), topTheoPSVR6JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR6JhighMT"), topTheoPSVR6JhighMTMu))

    # Renormalization Scale variation
    generatorSyst.append((("PowhegPythiaTTbar","WR3J"), topTheoRenScWR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR3J"), topTheoRenScTR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","WR5J"), topTheoRenScWR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR5J"), topTheoRenScTR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","WR6J"), topTheoRenScWR6JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR6J"), topTheoRenScTR6JMu))

    generatorSyst.append((("PowhegPythiaTTbar","SR5J"), topTheoRenScSR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","SR3J"), topTheoRenScSR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","SR6J"), topTheoRenScSR6JMu))

    generatorSyst.append((("PowhegPythiaTTbar","VR3JhighMET"), topTheoRenScVR3JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR3JhighMT"), topTheoRenScVR3JhighMTMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR5JhighMET"), topTheoRenScVR5JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR5JhighMT"), topTheoRenScVR5JhighMTMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR6JhighMET"), topTheoRenScVR6JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR6JhighMT"), topTheoRenScVR6JhighMTMu))

    # Factorization Scale variation
    generatorSyst.append((("PowhegPythiaTTbar","WR3J"), topTheoFacScWR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR3J"), topTheoFacScTR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","WR5J"), topTheoFacScWR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR5J"), topTheoFacScTR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","WR6J"), topTheoFacScWR6JMu))
    generatorSyst.append((("PowhegPythiaTTbar","TR6J"), topTheoFacScTR6JMu))

    generatorSyst.append((("PowhegPythiaTTbar","SR5J"), topTheoFacScSR5JMu))
    generatorSyst.append((("PowhegPythiaTTbar","SR3J"), topTheoFacScSR3JMu))
    generatorSyst.append((("PowhegPythiaTTbar","SR6J"), topTheoFacScSR6JMu))

    generatorSyst.append((("PowhegPythiaTTbar","VR3JhighMET"), topTheoFacScVR3JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR3JhighMT"), topTheoFacScVR3JhighMTMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR5JhighMET"), topTheoFacScVR5JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR5JhighMT"), topTheoFacScVR5JhighMTMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR6JhighMET"), topTheoFacScVR6JhighMETMu))
    generatorSyst.append((("PowhegPythiaTTbar","VR6JhighMT"), topTheoFacScVR6JhighMTMu))
    
#    SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")
#    generatorSyst.append((("PowhegPythiaTTbar","meffInc40_SR4jTEl"), SystGenTTbar)) # Only applied to SR.
#    generatorSyst.append((("PowhegPythiaTTbar","meffInc40_SR4jTMu"), SystGenTTbar)) # Only applied to SR.




#############
## Samples ##
#############

configMgr.nomName = "_NoSys"

#WSampleName = "AlpgenW"
WSampleName = "AlpgenJimmyW"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
#WSample.setNormRegions([("WR3JEl","cuts"),("WR5JEl","cuts"),("WR6JEl","cuts"),("WR3JMu","cuts"),("WR5JMu","cuts"),("WR6JMu","cuts")])
#W5Sample.setNormRegions([("TR5JEl","nJet30"),("WR5JEl","nJet30"),("TR5JMu","nJet30"),("WR5JMu","nJet30")])

TTbarSampleName = "PowhegPythiaTTbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
#TTbarSample.setNormRegions([("TR3JEl","cuts"),("TR5JEl","cuts"),("TR6JEl","cuts"),("TR3JMu","cuts"),("TR5JMu","cuts"),("TR6JMu","cuts")])
#TTbarSample.setNormRegions([("TR5JEl","nJet30"),("WR5JEl","nJet30"),("TR5JMu","nJet30"),("WR5JMu","nJet30")])

# TTBAR reweighting http://www.icepp.s.u-tokyo.ac.jp/~ysasaki/secret/susy/2013_01_17_Reweight/plots/cut2/SquAvgPt1/index.cgi
# func = (p0) * ( 1 + (p1) * SquAvgTTbarPt )
# -> Normalized p0 = 1.298216e+00 +/- 3.162855e-02
# p1 = -1.347724e-03 +/- 5.785181e-05
#TTbarSample.addWeight("(1.298216) * ( 1 + (-1.347724e-03) * SquAvgTTbarPt )")
## TTbarSample.addSampleSpecificWeight(ttbarReweight3JetNomWeight)
## ttbarweights = TTbarSample.getWeights()

DibosonsSampleName = "Dibosons"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
#DibosonsSample.setNormFactor("mu_Dibosons",1.,0.,5.)
#DibosonsSample.addSystematic(Systematic("errDB", configMgr.weights,2. ,0.1, "user","userOverallSys"))
DibosonsSample.setStatConfig(useStat)
#
SingleTopSampleName = "SingleTop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
#SingleTopSample.setNormFactor("mu_SingleTop",1.,0.,5.)
#SingleTopSample.addSystematic(Systematic("errST", configMgr.weights,1.2 ,0.8, "user","userOverallSys"))
SingleTopSample.setStatConfig(useStat)
#
ZSampleName = "AlpgenJimmyZ"
ZSample = Sample(ZSampleName,kBlue+3)
#ZSample.setNormFactor("mu_Z",1.,0.,5.)
#ZSample.addSystematic(Systematic("errZ", configMgr.weights,1.2 ,0.8, "user","userOverallSys"))
ZSample.setStatConfig(useStat)
#
ttbarVSampleName = "ttbarV"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
#ttbarVSample.setNormFactor("mu_ttbarV",1.,0.,5.)
#ttbarVSample.addSystematic(Systematic("errTTV", configMgr.weights,1.2 ,0.8, "user","userOverallSys"))
ttbarVSample.setStatConfig(useStat)
#
QCDSample = Sample("QCD",kYellow)
QCDSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"]) #_QCD.
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
#
DataSample = Sample("Data",kBlack)
DataSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"])
DataSample.setData()


################
# Bkg-only fit #
################
#bkgOnly = configMgr.addTopLevelXML("bkgonly")
bkgOnly = configMgr.addFitConfig("bkgonly")
if not doSignalOnly:
    bkgOnly.addSamples([ttbarVSample,DibosonsSample,SingleTopSample,ZSample,QCDSample,TTbarSample,WSample,DataSample])

if useStat:
    bkgOnly.statErrThreshold=0.05 
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.037)
meas.addPOI("mu_SIG")

#Add common systematics
#for syst in basicChanSyst:
#    bkgOnly.getSample(TTbarSampleName).addSystematic(syst)
#    bkgOnly.getSample(WSampleName).addSystematic(syst)

#bkgOnly.getSample(WSampleName).addSystematic(qfacW)
#bkgOnly.getSample(WSampleName).addSystematic(ktfacW)
#bkgOnly.getSample(WSampleName).addSystematic(iqoptW)

#bkgOnly.getSample("PowhegPythiaTTbar").addSystematic(topTheo)
#bkgOnly.getSample("PowhegPythiaTTbar").addSystematic(ttbarRew)

#b-tag classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
bReqChans["3J"] = []
bVetoChans["3J"] = []
bReqChans["5J"] = []
bVetoChans["5J"] = []
bReqChans["6J"] = []
bVetoChans["6J"] = []
bAgnosticChans["3J"] = []
bAgnosticChans["5J"] = []
bAgnosticChans["6J"] = []

#lepton flavor classification of channels
elmuChans = {}
elmuChans["3J"] = []
elmuChans["5J"] = []
elmuChans["6J"] = []



######################################################
# Add channels to Bkg-only configuration with exclusive single bin 3J,5J,6J regions              #
######################################################
    
nBins3JCR = 1  
nBins5JCR = 1  
nBins6JCR = 1  

#-----3JET--------#
if "3J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR3J"],nBins3JCR,0.5,1.5),[elmuChans["3J"],bVetoChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["TR3J"],nBins3JCR,0.5,1.5),[elmuChans["3J"],bReqChans["3J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

#-----5JET--------#
if "5J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR5J"],nBins5JCR,0.5,1.5),[elmuChans["5J"],bVetoChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["TR5J"],nBins5JCR,0.5,1.5),[elmuChans["5J"],bReqChans["5J"]])
    bkgOnly.setBkgConstrainChannels(tmp)


#-----6JET--------#
if "6J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6J"],nBins6JCR,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6J"],nBins6JCR,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])
    bkgOnly.setBkgConstrainChannels(tmp)


    
    
######################################################
# Bkg-only configuration is finished.                #
# Move on with validation config from bkgOnly clone. #
######################################################
Met_Min = 90.
Met_Max = 200.
Met_bin = 22
Lep_Min = 0.
Lep_Max = 400.
Lep_bin = 20
Jet_Min = 0.
Jet_Max = 800.
Jet_bin = 20
Mt_Min = 35.
Mt_Max = 90.
Mt_bin = 22
W_Min = 100.
W_Max = 500.
W_bin = 20

meffNBinsSR = 4
meffBinLowSR = 800.
meffBinHighSR = 1600.

metNBinsSR = 3
metBinLowSR = 300.
metBinHighSR = 500.

#
#meffNBinsCR = 4
#meffBinLowCR = 500.
#meffBinHighCR = 1300.
#
#
meffMax=100000.
metMax=100000.

ValidRegList["OneLep"]  = ValidRegList["INCL"] or ValidRegList["WRV"] or ValidRegList["TRV"] or ValidRegList["VR3JhighMET"] or ValidRegList["VR3JhighMT"] or ValidRegList["VR5JhighMET"] or ValidRegList["VR5JhighMT"] or ValidRegList["VR6JhighMET"] or ValidRegList["VR6JhighMT"]

validation = None
if doTableInputs or ValidRegList["SRTight"] or ValidRegList["OneLep"]:
#if doTableInputs or ValidRegList["WRV"] or ValidRegList["TRV"]:
#    validation = configMgr.addTopLevelXMLClone(bkgOnly,"Validation")
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
        appendIfMatchName(c,bReqChans["3J"])
        appendIfMatchName(c,bVetoChans["3J"])
        appendIfMatchName(c,bAgnosticChans["3J"])
        appendIfMatchName(c,elmuChans["3J"])
        appendIfMatchName(c,bReqChans["5J"])
        appendIfMatchName(c,bVetoChans["5J"])
        appendIfMatchName(c,bAgnosticChans["5J"])
        appendIfMatchName(c,elmuChans["5J"])	
        appendIfMatchName(c,bReqChans["6J"])
        appendIfMatchName(c,bVetoChans["6J"])
        appendIfMatchName(c,bAgnosticChans["6J"])
        appendIfMatchName(c,elmuChans["6J"])

    if ValidRegList["SRTight"] or doTableInputs:
    #if ValidRegList["SRTight"]:
        if doTableInputs:
            if "5J" in CRregions:
                appendTo(validation.addValidationChannel("meffInc30",["SR5J"],1,0.,meffMax),[bAgnosticChans["5J"],elmuChans["5J"]])
            if "3J" in CRregions:
                appendTo(validation.addValidationChannel("met",["SR3J"],1,0.,metMax),[bAgnosticChans["3J"],elmuChans["3J"]])
            if "6J" in CRregions: 
                appendTo(validation.addValidationChannel("met",["SR6J"],1,0.,metMax),[bAgnosticChans["6J"],elmuChans["6J"]])
        else:
            if "3J" in CRregions:
                appendTo(validation.addValidationChannel("meffInc30",["SR3J"],1,meffBinLowSR,meffBinHighSR),[bAgnosticChans["3J"],elmuChans["3J"]])
            if "5J" in CRregions:
                appendTo(validation.addValidationChannel("meffInc30",["SR5J"],1,meffBinLowSR,meffBinHighSR),[bAgnosticChans["5J"],elmuChans["5J"]])
            if "6J" in CRregions:
                appendTo(validation.addValidationChannel("met",["SR6J"],1,metBinLowSR,metBinHighSR),[bAgnosticChans["6J"],elmuChans["6J"]])

##     if doTableInputs:
##         appendTo(validation.addValidationChannel("met",["WR3JEl"],1,0.,metMax),[bVetoChans,elChans])
##         appendTo(validation.addValidationChannel("met",["WR3JMu"],1,0.,metMax),[bVetoChans,muChans])
##         appendTo(validation.addValidationChannel("met",["TR3JEl"],1,0.,metMax),[bReqChans,elChans])
##         appendTo(validation.addValidationChannel("met",["TR3JMu"],1,0.,metMax),[bReqChans,muChans])
##         appendTo(validation.addValidationChannel("met",["WR5JEl"],1,0.,metMax),[bVetoChans,elChans])
##         appendTo(validation.addValidationChannel("met",["WR5JMu"],1,0.,metMax),[bVetoChans,muChans])
##         appendTo(validation.addValidationChannel("met",["TR5JEl"],1,0.,metMax),[bReqChans,elChans])
##         appendTo(validation.addValidationChannel("met",["TR5JMu"],1,0.,metMax),[bReqChans,muChans])
##         appendTo(validation.addValidationChannel("met",["WR6JEl"],1,0.,metMax),[bVetoChans,elChans])
##         appendTo(validation.addValidationChannel("met",["WR6JMu"],1,0.,metMax),[bVetoChans,muChans])
##         appendTo(validation.addValidationChannel("met",["TR6JEl"],1,0.,metMax),[bReqChans,elChans])
##         appendTo(validation.addValidationChannel("met",["TR6JMu"],1,0.,metMax),[bReqChans,muChans])

#    if ValidRegList["WRV"] or ValidRegList["TRV"]:
#        appendTo(validation.addValidationChannel("met",["WREl"],Met_bin,Met_Min,Met_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("met",["WRMu"],Met_bin,Met_Min,Met_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("met",["TREl"],Met_bin,Met_Min,Met_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("met",["TRMu"],Met_bin,Met_Min,Met_Max),[bReqChans,muChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["WREl"],1,0.,meffMax),[bAgnosticChans,elChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["WRMu"],1,0.,meffMax),[bAgnosticChans,elChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["TREl"],1,0.,meffMax),[bAgnosticChans,elChans])
#        #appendTo(validation.addValidationChannel("meffInc30",["TRMu"],1,0.,meffMax),[bAgnosticChans,elChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["WREl"],Lep_bin,Lep_Min,Lep_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["WRMu"],Lep_bin,Lep_Min,Lep_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["TREl"],Lep_bin,Lep_Min,Lep_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("lep1Pt",["TRMu"],Lep_bin,Lep_Min,Lep_Max),[bReqChans,muChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["WREl"],Jet_bin,Jet_Min,Jet_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["WRMu"],Jet_bin,Jet_Min,Jet_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["TREl"],Jet_bin,Jet_Min,Jet_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("jet1Pt",["TRMu"],Jet_bin,Jet_Min,Jet_Max),[bReqChans,muChans])
#        appendTo(validation.addValidationChannel("Wpt",["WREl"],W_bin,W_Min,W_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("Wpt",["WRMu"],W_bin,W_Min,W_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("Wpt",["TREl"],W_bin,W_Min,W_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("Wpt",["TRMu"],W_bin,W_Min,W_Max),[bReqChans,muChans])
# 	 appendTo(validation.addValidationChannel("mt",["WREl"],Mt_bin,Mt_Min,Mt_Max),[bVetoChans,elChans])
#        appendTo(validation.addValidationChannel("mt",["WRMu"],Mt_bin,Mt_Min,Mt_Max),[bVetoChans,muChans])
#        appendTo(validation.addValidationChannel("mt",["TREl"],Mt_bin,Mt_Min,Mt_Max),[bReqChans,elChans])
#        appendTo(validation.addValidationChannel("mt",["TRMu"],Mt_bin,Mt_Min,Mt_Max),[bReqChans,muChans])
#

    if ValidRegList["OneLep"] or doTableInputs:

        # Validation plots
        # Binning
        # nBins, min, max
        if doTableInputs:
            # In table inputs mode, all bins are summed up to 1bin.
            meffBinsVR     = ( 1,   0., meffMax)
            pass
        else:
            metBinsVR     = (20,   100.,500.)
            meffBinsVR    = (20,   500.,2500.)
            lep1PtBinsVR  = (25,   0.,500.)
            WptBinsVR     = (25,   0.,500.)
            njets         = (10,   0.,10.)
            nbjets         = (10,   0.,10.)
            jet1ptBinsVR  = (21,   80.,500.)
            mtBinsVR      = (25,   0.,500.)
            pass
        
        # Set all plots for VR1, VR2, VR3, VR4.
        ProcessRegions = []
        if ValidRegList["VR3JhighMET"] : 
            ProcessRegions.append(["VR3JhighMET" ,bAgnosticChans["3J"]])
        if ValidRegList["VR3JhighMT"] : 
            ProcessRegions.append(["VR3JhighMT" ,bAgnosticChans["3J"]]) 
        if ValidRegList["VR5JhighMET"] : 
            ProcessRegions.append(["VR5JhighMET" ,bAgnosticChans["5J"]])
        if ValidRegList["VR5JhighMT"] : 
            ProcessRegions.append(["VR5JhighMT" ,bAgnosticChans["5J"]])    
        if ValidRegList["VR6JhighMET"] : 
            ProcessRegions.append(["VR6JhighMET" ,bAgnosticChans["6J"]])
        if ValidRegList["VR6JhighMT"] : 
            ProcessRegions.append(["VR6JhighMT" ,bAgnosticChans["6J"]])            
            #ProcessRegions.append(["WVR1",bVetoChans])
            #ProcessRegions.append(["TVR1",bReqChans])
        #if ValidRegList["OneLep2"] : 
        #    ProcessRegions.append(["WVR2",bVetoChans])
        #    ProcessRegions.append(["TVR2",bReqChans])
        #if ValidRegList["OneLep3"] : 
        #    ProcessRegions.append(["WVR3",bVetoChans])
        #    ProcessRegions.append(["TVR3",bReqChans])
        #if ValidRegList["OneLep4"] : 
        #    ProcessRegions.append(["VR4" ,bAgnosticChans])
        #    ProcessRegions.append(["TWR" ,bAgnosticChans])   
        
        #if ValidRegList["WRV"] :
        #    ProcessRegions.append(["VR4" ,bAgnosticChans])
        #    ProcessRegions.append(["TWR" ,bAgnosticChans])

        if ValidRegList["INCL"] :
            ProcessRegions.append(["INCL" ,bAgnosticChans["3J"]])

        if ValidRegList["WRV"] :
            if "3J" in CRregions:
                ProcessRegions.append(["WR3J" ,bVetoChans["3J"]])
                ProcessRegions.append(["WR3JMET" ,bVetoChans["3J"]])
                ProcessRegions.append(["WR3JMT" ,bVetoChans["3J"]])
            if "5J" in CRregions:	
                ProcessRegions.append(["WR5J" ,bVetoChans["5J"]]) 
                ProcessRegions.append(["WR5JMET" ,bVetoChans["5J"]])
                ProcessRegions.append(["WR5JMT" ,bVetoChans["5J"]])
            if "6J" in CRregions:
                ProcessRegions.append(["WR6J" ,bVetoChans["6J"]])
                ProcessRegions.append(["WR6JMET" ,bVetoChans["6J"]])
                ProcessRegions.append(["WR6JMT" ,bVetoChans["6J"]])
        if ValidRegList["TRV"] :
            if "3J" in CRregions:
                ProcessRegions.append(["TR3J" ,bReqChans["3J"]])
                ProcessRegions.append(["TR3JMET" ,bReqChans["3J"]])
                ProcessRegions.append(["TR3JMT" ,bReqChans["3J"]])
            if "5J" in CRregions:
                ProcessRegions.append(["TR5J" ,bReqChans["5J"]])
                ProcessRegions.append(["TR5JMET" ,bReqChans["5J"]])
                ProcessRegions.append(["TR5JMT" ,bReqChans["5J"]])
            if "6J" in CRregions:	
                ProcessRegions.append(["TR6J" ,bReqChans["6J"]])
                ProcessRegions.append(["TR6JMET" ,bReqChans["6J"]])
                ProcessRegions.append(["TR6JMT" ,bReqChans["6J"]])

        for reg in ProcessRegions:
            CutPrefix = reg[0]
            bChanKind = reg[1]
            sys_region = ""
            if "3J" in CutPrefix: sys_region = "3J"
            elif "5J" in CutPrefix: sys_region = "5J"
            elif "6J" in CutPrefix: sys_region = "6J"
            else: 
                print "Unknown region! - Take systematics from 3J regions."
                sys_region = "3J"
            FlavorList = elmuChans[sys_region]
            if not doTableInputs:
            	#if not ("JM" in reg[0]):
            	appendTo(validation.addValidationChannel("meffInc30",[CutPrefix],  meffBinsVR[0],  meffBinsVR[1],  meffBinsVR[2]),[bChanKind,FlavorList])
            	    #appendTo(validation.addValidationChannel("lep1Pt" ,[CutPrefix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
            	    #appendTo(validation.addValidationChannel("jet1Pt"    ,[CutPrefix],   jet1ptBinsVR[0],	jet1ptBinsVR[1],   jet1ptBinsVR[2]),[bChanKind,FlavorList])
            	    #appendTo(validation.addValidationChannel("jet2Pt" ,[CutPrefix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
            	    #appendTo(validation.addValidationChannel("Wpt"    ,[CutPrefix],   WptBinsVR[0],   WptBinsVR[1],   WptBinsVR[2]),[bChanKind,FlavorList])
            	    #appendTo(validation.addValidationChannel("nBJet30",[CutPrefix],   nbjets[0],   nbjets[1],   nbjets[2]),[bChanKind,FlavorList])
            	    #if "3J" in reg[0]:
            		#appendTo(validation.addValidationChannel("jet3Pt" ,[CutPrefix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
            	    #elif "5J" in reg[0]:
            		#appendTo(validation.addValidationChannel("jet5Pt" ,[CutPrefix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
            	    #elif "6J" in reg[0]:
            		#appendTo(validation.addValidationChannel("jet6Pt" ,[CutPrefix],lep1PtBinsVR[0],lep1PtBinsVR[1],lep1PtBinsVR[2]),[bChanKind,FlavorList])
            	if ("MET" in reg[0]) or ("INCL" in reg[0]):
            	    appendTo(validation.addValidationChannel("met"    ,[CutPrefix],   metBinsVR[0],   metBinsVR[1],   metBinsVR[2]),[bChanKind,FlavorList])
            	if ("MT" in reg[0]) or ("INCL" in reg[0]):
            	    appendTo(validation.addValidationChannel("mt"    ,[CutPrefix],	mtBinsVR[0],   mtBinsVR[1],   mtBinsVR[2]),[bChanKind,FlavorList])
            	if ("VR" in reg[0]) or ("INCL" in reg[0]):
            	    appendTo(validation.addValidationChannel("nJet30",[CutPrefix],	njets[0],   njets[1],	njets[2]),[bChanKind,FlavorList])
            else:
            	if ("VR" in reg[0]) or ("INCL" in reg[0]):
            	    appendTo(validation.addValidationChannel("cuts",[CutPrefix],   1,   0.5, 1.5),[bChanKind,FlavorList])

#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    SRs=["SR3J","SR5J","SR6J"]

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            appendIfMatchName(c,bReqChans["3J"])
            appendIfMatchName(c,bVetoChans["3J"])
            appendIfMatchName(c,bAgnosticChans["3J"])
            appendIfMatchName(c,elmuChans["3J"])
            appendIfMatchName(c,bReqChans["5J"])
            appendIfMatchName(c,bVetoChans["5J"])
            appendIfMatchName(c,bAgnosticChans["5J"])
            appendIfMatchName(c,elmuChans["5J"])
            appendIfMatchName(c,bReqChans["6J"])
            appendIfMatchName(c,bVetoChans["6J"])
            appendIfMatchName(c,bAgnosticChans["6J"])
            appendIfMatchName(c,elmuChans["6J"])
            
        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)

        #signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(jesSignal)
        sigSample.addSystematic(xsecSig)
        if sig.startswith("SM"):
            # old ISR uncertainties- outdated
            #from SystematicsUtils import getISRSyst
            #isrSyst = getISRSyst(sig)
            #sigSample.addSystematic(isrSyst)
            #pass
            # ISR uncertainty (SS and GG grids)
            massSet = sig.split('_')
            if len(massSet) != 5:
                print "Unrecognized point format! - ISR uncertainties will be wrong."
            DeltaM = float(massSet[-3]) - float(massSet[-1])

            if DeltaM<=0: 
                log.fatal("Invalid value of DeltaM : %f" % DeltaM)
                
            if 'GG' in sig: 
                eisr3 = exp(-1.4-0.013*DeltaM)
                eisr5 = exp(-1.2-0.005*DeltaM)
                if eisr3<0.06: eisr3=0.06
                if eisr5<0.06: eisr5=0.06
                pass
            elif 'SS' in sig: 
                eisr3 = 0.06+exp(0.8-0.1*DeltaM)
                eisr5 = 0.06+exp(-1.5-0.005*DeltaM)
                pass
            #print 'TEST'
            print eisr3, eisr5
            isr3j = Systematic("isr", configMgr.weights, 1.+eisr3, 1.00-eisr3, "user", "userOverallSys")
            isr5j = Systematic("isr", configMgr.weights, 1.+eisr5, 1.00-eisr5, "user", "userOverallSys")
            pass
            
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if ValidRegList["SRTight"] or doTableInputs:
                #don't re-create already existing channel, but unset as Validation and set as Signal channel
                #Not working at the moment
                channame = "meffInc30_" +sr
                if channame in myTopLvl.channels:
                    ch = myTopLvl.getChannel("meffInc30",[sr])
                    iPop=myTopLvl.validationChannels.index(sr+"_meffInc30")
                    myTopLvl.validationChannels.pop(iPop)
            else:            
                if sr=="SR5J" or sr=="SR3J" :
                    ch = myTopLvl.addChannel("meffInc30",[sr],meffNBinsSR,meffBinLowSR,meffBinHighSR)
                elif sr=="SR6J":
                    ch = myTopLvl.addChannel("met",[sr],metNBinsSR,metBinLowSR,metBinHighSR)

                    #for theSample in ch.sampleList:          
                    #    theSample.removeSystematic("JHigh")
                    #    theSample.removeSystematic("JMedium")
                    #    theSample.removeSystematic("JLow")  
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                
                
                if sr=="SR3J": 
                    elmuChans["3J"].append(ch)
                    bAgnosticChans["3J"].append(ch)
                elif sr=="SR5J": 
                    elmuChans["5J"].append(ch) 
                    bAgnosticChans["5J"].append(ch)
                elif sr=="SR6J": 
                    elmuChans["6J"].append(ch) 
                    bAgnosticChans["6J"].append(ch)             
                else:
                    raise RuntimeError("Unexpected signal region %s"%sr)
                pass
            
                if 'SM' in sig:
                    if sr=="SR3J":
                        ch.getSample(sig).addSystematic(isr3j)
                    else:
                        ch.getSample(sig).addSystematic(isr5j)
            
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ch.useOverflowBin=True
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)
        
        if remapOfSR:    
            tmp = appendTo(myTopLvl.addValidationChannel("meffInc30",["SR3Jloose"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans["3J"],elmuChans["3J"]])
            tmp.useOverflowBin=True
            tmp = appendTo(myTopLvl.addValidationChannel("meffInc30",["SR5Jloose"],meffNBinsSR,meffBinLowSR,meffBinHighSR),[bAgnosticChans["5J"],elmuChans["5J"]])
            tmp.useOverflowBin=True
            tmp = appendTo(myTopLvl.addValidationChannel("met",["SR6Jloose"],metNBinsSR,metBinLowSR,metBinHighSR),[bAgnosticChans["6J"],elmuChans["6J"]])
            tmp.useOverflowBin=True
         
            
#            # add JES as ShapeSys to each channel      
#            jesname = "JES_" + ch.name
#            if jesname not in ch.systDict.keys():
#                tmpJES = Systematic( jesname,"_NoSys","_JESup"    ,"_JESdown"    ,"tree","shapeSys")
#                ch.addSystematic(tmpJES)
        

##############################
# Finalize fit configs setup #
##############################

AllChannels = bReqChans["5J"] + bVetoChans["5J"] + bAgnosticChans["3J"] + bAgnosticChans["5J"] + bAgnosticChans["6J"] + bReqChans["3J"] + bVetoChans["3J"] + bReqChans["6J"] + bVetoChans["6J"] 

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan = tgt[1]
    for chan in AllChannels:
        #        if tgtchan=="All" or tgtchan==chan.name:
        if tgtchan=="All" or tgtchan in chan.name:
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
	    
#bkgOnly.getSample(WSampleName).addSystematic(qfacW)
#bkgOnly.getSample(WSampleName).addSystematic(ktfacW)
#bkgOnly.getSample(WSampleName).addSystematic(iqoptW)

# Only adding the el, mu systematics to the ttbar and W+jets samples + adding other systematics to ttbar and W+jets samples
for region in CRregions:
    SetupChannels_Hack(elmuChans[region],bgdFiles_em, elmuChanSyst[region])
    SetupChannels_nofiles(elmuChans[region],basicChanSyst[region])
    #for chan in AllChannels:
    #    chan.getSample(WSampleName).addSystematic(qfacW[region])
        #chan.getSample(WSampleName).addSystematic(ktfacW[region])
        #chan.getSample(WSampleName).addSystematic(iqoptW[region])




  
######################################################
# Add separate Normalization Factors for ttbar and W+jets for each CR                               #
#   destroying the connection by nJet shape from MC                                                             #
######################################################
if useNJetNormFac:
    
    for chan in AllChannels:
        mu_W_Xj = "mu_W_XJ"
        mu_Top_Xj = "mu_Top_XJ"
        if "3J" in chan.name:
            mu_W_Xj = "mu_W_3J"
            mu_Top_Xj = "mu_Top_3J"
        elif "5J" in chan.name:
            mu_W_Xj = "mu_W_5J"
            mu_Top_Xj = "mu_Top_5J"
        elif "6J" in chan.name:
            mu_W_Xj = "mu_W_6J"
            mu_Top_Xj = "mu_Top_6J"
        else:
            log.warning("Channel %s gets no nJet separated normalization factor" % chan.name)
        chan.getSample(WSampleName).addNormFactor(mu_W_Xj,1.,3.,0.1)
        chan.getSample(TTbarSampleName).addNormFactor(mu_Top_Xj,1.,3.,0.1)
        log.info("Adding additional nJet normalization factors (%s, %s) to channel (%s)" %(mu_W_Xj, mu_Top_Xj, chan.name))

    meas.addParamSetting("mu_W","const",1.0)
    meas.addParamSetting("mu_Top","const",1.0)

    if validation:
        meas_valid  = validation.getMeasurement("BasicMeasurement")
        meas_valid.addParamSetting("mu_W","const",1.0)
        meas_valid.addParamSetting("mu_Top","const",1.0)
        
    if myFitType==FitType.Exclusion:
        for sig in sigSamples:
            meas_excl=configMgr.getFitConfig("Sig_%s"%sig).getMeasurement("BasicMeasurement")
            meas_excl.addParamSetting("mu_W","const",1.0)
            meas_excl.addParamSetting("mu_Top","const",1.0)

######################################################
#  Use TTbar <pT2> reweighting a la Yuichi                                                              #
######################################################
## Evgeny 19/02/2013
## 3J: ( 1.399251e+00 ) * ( 1 + ( -1.259160e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt < 7.941802e+02 )
## 5J: ( 1.413804e+00 ) * ( 1 + ( -1.259160e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt < 7.941802e+02 )
## 6J: ( 1.448975e+00 ) * ( 1 + ( -1.259160e-03 ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt < 7.941802e+02 )

if useTTbarReweighting:
    
    for chan in AllChannels:
        ## TTbarSample.addSampleSpecificWeight(ttbarReweight3JetNomWeight)
        p1 =  "-1.259160e-03"
        #p1 =  "0."
        p0 = "1."
        ttbarReweight = "1."
        p0 = GetP0(chan.name)
        if p0=="1.":
            log.warning("Channel %s gets no ttbar reweighting" % chan.name)
            p1 = "0."
##         if "3J" in chan.name:
##             p0 = "0.1"
##         if "5J" in chan.name:
##             p0 = "10."     
        ttbarReweight = "(" + p0 + ") * ( 1 + (" + p1 + " ) * SquAvgTTbarPt ) * ( SquAvgTTbarPt < 7.941802e+02 )"
        chan.getSample(TTbarSampleName).addSampleSpecificWeight(ttbarReweight)
        log.info("Adding ttbar reweighting (%s) to channel (%s)" %(ttbarReweight, chan.name))

     
        
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig) 
            sys_region = ""
            if "3J" in chan.name: sys_region = "3J"
            elif "5J" in chan.name: sys_region = "5J"
            elif "6J" in chan.name: sys_region = "6J"
            else: 
                print "Unknown region! - Take systematics from 3J regions."
                sys_region = "3J"
            for syst in basicChanSyst[sys_region]:
                theSample.addSystematic(syst)   
            theSample.removeSystematic("JES_"+sys_region)
            #theSample.removeSystematic("JHigh_"+sys_region)
            #theSample.removeSystematic("JMedium_"+sys_region)
            #theSample.removeSystematic("JLow_"+sys_region)              
            theSigFiles=[]
            if chan in elmuChans["3J"]+elmuChans["5J"]+elmuChans["6J"]:
                theSigFiles = sigFiles_e+sigFiles_m
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "WARNING no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                
        if remapOfSR:        
            myTopLvl.getChannel("meffInc30",["SR3J"]).remapSystChanName = 'meffInc30_SR3Jloose'
            myTopLvl.getChannel("meffInc30",["SR5J"]).remapSystChanName = 'meffInc30_SR5Jloose'
            myTopLvl.getChannel("met",["SR6J"]).remapSystChanName = 'met_SR6Jloose'
            
        #for chan in SR_channels[sig]:
        #    chan.getSample("AlpgenJimmyW").removeSystematic("RESOST")
        #    chan.getSample("AlpgenJimmyW").removeSystematic("SCALEST")
        #    chan.getSample("PowhegPythiaTTbar").removeSystematic("RESOST")
        #    chan.getSample("PowhegPythiaTTbar").removeSystematic("SCALEST")
        #    if chan.getSample(sig): chan.getSample(sig).removeSystematic("RESOST")
        #    if chan.getSample(sig): chan.getSample(sig).removeSystematic("SCALEST")



# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        chan.hasBQCD = True
        #chan.addSystematic(bTagSyst)
        if "BTag" in SystList:
            chan.getSample("PowhegPythiaTTbar").addSystematic(bTagSyst[region])
            chan.getSample("AlpgenJimmyW").addSystematic(bTagSyst[region])
            chan.getSample("PowhegPythiaTTbar").addSystematic(cTagSyst[region])
            chan.getSample("AlpgenJimmyW").addSystematic(cTagSyst[region])
            #chan.getSample("PowhegPythiaTTbar").addSystematic(mTagSyst[region])
            #chan.getSample("AlpgenJimmyW").addSystematic(mTagSyst[region])

    for chan in bVetoChans[region]:
        chan.hasBQCD = False
        if "BTag" in SystList:
            chan.getSample("PowhegPythiaTTbar").addSystematic(bTagSyst[region])
            chan.getSample("AlpgenJimmyW").addSystematic(bTagSyst[region])
            chan.getSample("PowhegPythiaTTbar").addSystematic(cTagSyst[region])
            chan.getSample("AlpgenJimmyW").addSystematic(cTagSyst[region])
            #chan.getSample("PowhegPythiaTTbar").addSystematic(mTagSyst[region])
            #chan.getSample("AlpgenJimmyW").addSystematic(mTagSyst[region])
        #chan.addSystematic(bTagSyst)    

    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        chan.getSample("AlpgenJimmyZ").addSystematic(BGerr[region])
        chan.getSample("SingleTop").addSystematic(BGerr[region])
        chan.getSample("Dibosons").addSystematic(BGerr[region])
        chan.getSample("ttbarV").addSystematic(BGerr[region])
        chan.getSample("PowhegPythiaTTbar").setNormRegions([("TR"+region,"cuts"),("WR"+region,"cuts")])
        chan.getSample("AlpgenJimmyW").setNormRegions([("WR"+region,"cuts"),("TR"+region,"cuts")])

for chan in (bAgnosticChans["3J"]+bAgnosticChans["5J"]+bAgnosticChans["6J"]):
    chan.hasBQCD = False
    chan.removeWeight("bTagWeight[3]")


           
#######################
## Cosmetic Settings ##
#######################
# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)

#
#entry = leg.AddEntry("","multijets (data estimate)","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
###
#entry = leg.AddEntry("","W+jets","lf")
#entry.SetLineColor(WSample.color)
#entry.SetFillColor(WSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Z+jets","lf")
#entry.SetLineColor(ZSample.color)
#entry.SetFillColor(ZSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","t#bar{t}","lf")
#entry.SetLineColor(TTbarSample.color)
#entry.SetFillColor(TTbarSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","single top","lf")
#entry.SetLineColor(SingleTopSample.color)
#entry.SetFillColor(SingleTopSample.color)
#entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","diboson","lf")
#entry.SetLineColor(DibosonsSample.color)
#entry.SetFillColor(DibosonsSample.color)
#entry.SetFillStyle(compFillStyle)
#

#entry = leg.AddEntry("","W0jet","lf")
#entry.SetLineColor(W0Sample.color)
#entry.SetFillColor(W0Sample.color)
#entry.SetFillStyle(compFillStyle)

#entry = leg.AddEntry("","W1jet","lf")
#entry.SetLineColor(W1Sample.color)
#entry.SetFillColor(W1Sample.color)
#entry.SetFillStyle(compFillStyle)

#entry = leg.AddEntry("","W2jet","lf")
#entry.SetLineColor(W2Sample.color)
#entry.SetFillColor(W2Sample.color)
#entry.SetFillStyle(compFillStyle)

#entry = leg.AddEntry("","W3jet","lf")
#entry.SetLineColor(W3Sample.color)
#entry.SetFillColor(W3Sample.color)
#entry.SetFillStyle(compFillStyle)

#entry = leg.AddEntry("","W4jet","lf")
#entry.SetLineColor(W4Sample.color)
#entry.SetFillColor(W4Sample.color)
#entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t} Powheg","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}0jet","lf")
## entry.SetLineColor(TTbarlnln0Sample.color)
## entry.SetFillColor(TTbarlnln0Sample.color)
## entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}1jet","lf")
## entry.SetLineColor(TTbarlnln1Sample.color)
## entry.SetFillColor(TTbarlnln1Sample.color)
## entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}2jet","lf")
## entry.SetLineColor(TTbarlnln2Sample.color)
## entry.SetFillColor(TTbarlnln2Sample.color)
## entry.SetFillStyle(compFillStyle)

## entry = leg.AddEntry("","t#bar{t}3jet","lf")
## entry.SetLineColor(TTbarlnln3Sample.color)
## entry.SetFillColor(TTbarlnln3Sample.color)
## entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","Dibosons","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Multijet","lf")
entry.SetLineColor(QCDSample.color)
entry.SetFillColor(QCDSample.color)
entry.SetFillStyle(compFillStyle)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation : validation.tLegend = leg
if myFitType==FitType.Exclusion: 
    entry = leg.AddEntry("","Signal","lf")
    entry.SetLineColor(sigSample.color)
    entry.SetFillColor(sigSample.color)
    entry.SetFillStyle(compFillStyle)
    myTopLvl.tLegend = leg
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion: chan.logY = True
    if chan.logY:
        chan.minY = 1.5
        chan.maxY = 1000000
    else:
        chan.minY = 0.05 
        chan.maxY = 3000
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.ShowLumi = True

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Entries"
            chan.minY = 0.05 
            chan.maxY = 30
            chan.ATLASLabelX = 0.15
            chan.ATLASLabelY = 0.83
            chan.ATLASLabelText = "Internal"
            chan.ShowLumi = True


