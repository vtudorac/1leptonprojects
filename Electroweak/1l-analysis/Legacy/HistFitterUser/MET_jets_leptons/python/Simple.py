################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
#from configWriter import TopLevelXML,Measurement,ChannelXML,Sample 
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy,copy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys

from logger import Logger
log = Logger('HardLepton')

"""
def DeepCopy(top,channels):
    def coDeepCopy():

    result = []
    for chanName in channels:
        for tchan in top.channels:
            print chanName,tchan.name
            if tchan.name == chanName:
                result.append(tchan)
    #assert len(result)==len(channels)
    print result
    return result
"""


def InitDictWithRegions(Regions):
    mydic = {}
    for cr in Regions:
        mydic[cr] = []
    return mydic

def SetFileList(topCfg,channels,samples,fileList):
    for chanName in channels:
        chan = None
        for c in topCfg.channels:
            if c.name == chanName:
                chan = c
                break
        for sam in samples:
            chan.getSample(sam).setFileList(fileList)
    return

def AddSystematics(topCfg,channels,samples,systList):
    for chanName in channels:
        chan = None
        for c in topCfg.channels:
            if c.name == chanName:
                chan = c
                break
        for sam in samples:
            for syst in systList:
                chan.getSample(sam).addSystematic(syst)
    return


def SumChans(ChanList):
    res = []
    for c in ChanList:
        for item in c:
            if item in res: continue # avoid duplication
            res.append(item)
    return res

def AndChans(ChanList):
    res = []
    for c in ChanList:
        for item in c:
            if item in res: continue
            isOK = True
            for k in ChanList:
                if not item in k: isOK=False
            if isOK : res.append(item)
    return res

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def appChanTo(item, toList):
    for to in toList:
        to.append(item.name)
    return item

def SetupChannels(channels,Files,systList=[]):
    for chan in channels:
        chan.setFileList(Files)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Main part
# ********************************************************************* #
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1]

useHardLepCR=True
useStat=True
#doExclusion_mSUGRA=True # not needed anymore  - use exclusion fit option in HistFitter
useNJetNormFac=True
useTTbarReweighting=False
remapOfSR = False

#SystList=[]
#SystList.append("JES")      # Jet Energy Scale (common)

doTableInputs=False#This effectively means no validation plots but only validation tables (but is 100x faster)

doDiscoveryMode=False

#Regions = ["3J","5J","6J"]
Regions = ["3J"]
 
analysissuffix = ''

for cr in Regions:
    analysissuffix += "_"
    analysissuffix += cr
    
if not 'sigSamples' in dir():
    sigSamples=["SM_GG1step_1025_545_65"]

if myFitType==FitType.Exclusion:
    if 'GG1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix += '_GG1stepx12'
    elif 'GG1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix += '_GG1stepgridx'
    elif 'SS1step' in sigSamples[0] and not sigSamples[0].endswith('_60'):
        analysissuffix += '_SS1stepx12'
    elif 'SS1step' in sigSamples[0] and sigSamples[0].endswith('_60'):
        analysissuffix += '_SS1stepgridx' 
    elif 'GG2WWZZ' in sigSamples[0]:
        analysissuffix += '_GG2WWZZ'
    elif 'GG2CNsl' in sigSamples[0]:
        analysissuffix += '_GG2CNsl'    
    elif 'SS2WWZZ' in sigSamples[0]:
        analysissuffix += '_SS2WWZZ'
    elif 'SS2CNsl' in sigSamples[0]:
        analysissuffix += '_SS2CNsl' 
    elif 'pMSSM' in sigSamples[0]:
        analysissuffix += '_pMSSM'                
    elif 'HiggsSU' in sigSamples[0]:
        analysissuffix += '_HiggsSU' 

# First define HistFactory attributes
configMgr.analysisName = "Simple"+analysissuffix # Name to give the analysis
configMgr.outputFileName = "results/Simple"+analysissuffix+".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.3 #20.7
configMgr.setLumiUnits("fb-1")

#configMgr.daoHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

configMgr.writeXML = True

configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

#Split bdgFiles per channel
sigFiles_e = []
sigFiles_m = []

inputDir="/gpfs/fs2001/ysasaki/Thesis/trees/v9_1_2/"
inputDirSig="/gpfs/fs2001/ysasaki/Thesis/trees/v9_1_2/"

# Set the files to read from
bkgFiles_e = [inputDir+"bkgtree_HardEle.root"]
bkgFiles_m = [inputDir+"bkgtree_HardMuo.root"]
if myFitType==FitType.Exclusion:
    sigFiles_e=[inputDirSig+"sigtree_HardEle_HiggsSU.root"]
    sigFiles_m=[inputDirSig+"sigtree_HardMuo_HiggsSU.root"]
    if 'SM_SS1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS1step.root"]
        print "Using simplified models SS onestepCC"
    if 'SM_GG1step' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG1step.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG1step.root"]
        print "Using simplified models GG onestepCC"
    if 'pMSSM' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_pMSSM.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_pMSSM.root"]
        print "Using pMSSM signal model"
    if 'GG2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2WWZZ.root"]
        print "Using simplified models GG two step with WWZZ"
    if 'SS2WWZZ' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2WWZZ.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2WWZZ.root"]
        print "Using simplified models SS two step with WWZZ"   
    if 'GG2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_GG2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_GG2CNsl.root"]
        print "Using simplified models GG two step with sleptons"
    if 'SS2CNsl' in sigSamples[0]:
        sigFiles_e=[inputDirSig+"sigtree_HardEle_SM_SS2CNsl.root"]
        sigFiles_m=[inputDirSig+"sigtree_HardMuo_SM_SS2CNsl.root"]
        print "Using simplified models SS two step with sleptons" 

bkgFiles = {'El':bkgFiles_e,'Mu':bkgFiles_m}
sigFiles = {'El':sigFiles_e,'Mu':sigFiles_m}

# Map regions to cut strings
CommonSelection = "&& lep2Pt<10 && lep1Pt>25 && jet1Pt>80"
CommonSelection2 = "&& lep2Pt<10 && lep1Pt>25 && jet1Pt>80"

configMgr.cutsDict["WR3J"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<30 && met>150 && met<300 &&  nB3Jet30==0 && mt<150 && mt>80 && meffInc30>800" + CommonSelection
configMgr.cutsDict["TR3J"]="nJet30>2 && jet2Pt>80 && jet3Pt>30 && jet5Pt<30 && met>150 && met<300 && nB3Jet30>0 && mt<150 && mt>80 && meffInc30>800" + CommonSelection
configMgr.cutsDict["WR5J"]="nJet30>4 && jet2Pt>50 && jet5Pt>30 && jet6Pt<30 && met>150 && met<300 && nB3Jet30==0 && mt<150 && mt>60 && meffInc30>800" + CommonSelection
configMgr.cutsDict["TR5J"]="nJet30>4 && jet2Pt>50 && jet5Pt>30 && jet6Pt<30 && met>150 && met<300 && nB3Jet30>0 && mt<150 && mt>60 && meffInc30>800" + CommonSelection
configMgr.cutsDict["WR6J"]="nJet30>5 && jet2Pt>50 && jet6Pt>30 && met>100 && met<200 && nB3Jet30==0 && mt<80 && mt>40 && meffInc30>600" + CommonSelection
configMgr.cutsDict["TR6J"]="nJet30>5 && jet2Pt>50 && jet6Pt>30 && met>150 && met<250 && nB3Jet30>0 && mt<150 && mt>40 && meffInc30>600" + CommonSelection

configMgr.cutsDict["INCL"]="lep2Pt<10 && lep1Pt>25"

trigger_bias_bug = "&& met>100"

configMgr.cutsDict["SR3J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 && met/meff3Jet30>0.3 && meffInc30>800"
configMgr.cutsDict["SR5J"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>150 && jet1Pt>80 && jet2Pt>50 && jet5Pt>40 && jet6Pt<40 && meffInc30>800"
configMgr.cutsDict["SR6J"]="lep2Pt<10 && lep1Pt>25 && met>250 && mt>150 && jet1Pt>80 && jet2Pt>50 && jet6Pt>40 && meffInc30>600"

configMgr.cutsDict["SR3Jdiscovery"]="lep2Pt<10 && lep1Pt>25 && met>500 && mt>150 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && met/meff3Jet30>0.3 && meffInc30>1400"
configMgr.cutsDict["SR5Jdiscovery"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>200 && jet1Pt>80 && jet2Pt>50 && jet5Pt>40 && meffInc30>1400"
configMgr.cutsDict["SR6Jdiscovery"]="lep2Pt<10 && lep1Pt>25 && met>350 && mt>150 && jet1Pt>80 && jet2Pt>50 && jet6Pt>40 && meffInc30>600"

configMgr.cutsDict["SR3Jloose"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>120 && jet1Pt>80 && jet2Pt>80 && jet3Pt>30 && jet5Pt<40 && met/meff3Jet30>0.3 && meffInc30>800"
configMgr.cutsDict["SR5Jloose"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>120 && jet1Pt>80 && jet2Pt>50 && jet5Pt>40 && jet6Pt<40 && meffInc30>800"
configMgr.cutsDict["SR6Jloose"]="lep2Pt<10 && lep1Pt>25 && met>300 && mt>120 && jet1Pt>80 && jet2Pt>50 && jet6Pt>40 && meffInc30>600"

d=configMgr.cutsDict
OneEleSelection = "&& AnalysisType==1 && isNoCrackElectron"
OneMuoSelection = "&& AnalysisType==2 && isNoCrackElectron"

for i in copy(configMgr.cutsDict):
    configMgr.cutsDict[i+'El'] = d[i]+OneEleSelection
    configMgr.cutsDict[i+'Mu'] = d[i]+OneMuoSelection

weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]

configMgr.weights = weights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

#########################
## List of systematics ##
#########################

bkgSyst = InitDictWithRegions(Regions)
sigSyst = InitDictWithRegions(Regions)

# Signal XSec uncertainty as overallSys (pure yeild affect) DEPRECATED
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")
xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
for cr in Regions:
    sigSyst[cr].append(xsecSig)

# JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
for cr in Regions:
    comSystSig = Systematic("CommSig_"+cr, configMgr.weights, 1.30, 0.70, "user", "userOverallSys")
    sigSyst[cr] = comSystSig
    comSystBkg = Systematic("CommBkg_"+cr, configMgr.weights, 1.30, 0.70, "user", "userOverallSys")
    bkgSyst[cr] = comSystBkg

#############
## Samples ##
#############

bkgSampleNames = []

configMgr.nomName = "_NoSys"

WSampleName = "SherpaWMassiveBC"; bkgSampleNames.append(WSampleName)
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)

TTbarSampleName = "PowhegPythiaTTbar"; bkgSampleNames.append(TTbarSampleName)
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)

DibosonsSampleName = "SherpaDibosons"; bkgSampleNames.append(DibosonsSampleName)
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
SingleTopSampleName = "SingleTop"; bkgSampleNames.append(SingleTopSampleName)
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "SherpaZMassiveBC"; bkgSampleNames.append(ZSampleName)
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
#
ttbarVSampleName = "ttbarV"; bkgSampleNames.append(ttbarVSampleName)
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
QCDSample = Sample("QCD",kYellow)
QCDSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"]) #_QCD.
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
#
DataSample = Sample("Data",kBlack)
DataSample.setFileList([inputDir+"datatree_HardEle.root",inputDir+"datatree_HardMuo.root"])
DataSample.setData()


################
# Bkg-only fit #
################
bkgOnly = configMgr.addFitConfig("bkgonly")
bkgOnly.addSamples([ttbarVSample,DibosonsSample,SingleTopSample,ZSample,QCDSample,TTbarSample,WSample,DataSample])

if useStat:bkgOnly.statErrThreshold=0.05 
else      :bkgOnly.statErrThreshold=None

#Add Measurement
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.028)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)


#classification of channels
FlvChans        = {'El':[],'Mu':[]}
BtagChans       = {'bVeto':[],'bReq':[],'bAgnostic':[]}
RegionChans     = InitDictWithRegions(Regions)
CVSChans        = {'SR':[],'CR':[],'VR':[]}
AllChans        = []

######################################################
# Add channels to Bkg-only configuration with exclusive single bin 3J,5J,6J regions              #
######################################################
for reg in Regions:
    for flv in ['El','Mu']:
        tmp = appChanTo(bkgOnly.addChannel("cuts",["WR%s%s"%(reg,flv)],1,0.5,1.5),[FlvChans[flv],BtagChans['bVeto'],RegionChans[reg],CVSChans['CR'],AllChans])
        bkgOnly.setBkgConstrainChannels(tmp)
        tmp = appChanTo(bkgOnly.addChannel("cuts",["TR%s%s"%(reg,flv)],1,0.5,1.5),[FlvChans[flv],BtagChans['bReq'] ,RegionChans[reg],CVSChans['CR'],AllChans])
        bkgOnly.setBkgConstrainChannels(tmp)

## Set Files
for flv in bkgFiles:SetFileList(topCfg=bkgOnly,channels=FlvChans[flv],samples=bkgSampleNames,fileList=bkgFiles[flv])

## Set Systs
# No Systematics is applied in the CRs.
# Additinal Systematics will be added later in our SRs.

#-------------------------------------------------
# Exclusion fit
#-------------------------------------------------
if myFitType==FitType.Exclusion:     
    for sig in sigSamples:
        #SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)

        # Copy All Channels
        aFlvChans    = copy(FlvChans)
        aBtagChans   = copy(BtagChans)
        aRegionChans = copy(RegionChans)
        aCVSChans    = copy(CVSChans)
        aAllChans    = copy(AllChans)

        #Create channels for each SR
        for reg in Regions:
            ch = {}
            flvList = ['El','Mu']
            if reg=='3J' or reg=='5J':
                for flv in flvList:
                    ch[flv] = appChanTo(myTopLvl.addChannel("meffInc30",['SR%s%s'%(reg,flv)],4,800,1600),[aFlvChans[flv],aBtagChans['bAgnostic'],aRegionChans[reg],aCVSChans['SR'],aAllChans])
            elif reg=='6J':                                                                                                                                          
                for flv in flvList:
                    ch[flv] = appChanTo(myTopLvl.addChannel("met"      ,['SR%s%s'%(reg,flv)],3,250,550) ,[aFlvChans[flv],aBtagChans['bAgnostic'],aRegionChans[reg],aCVSChans['SR'],aAllChans])
            else:
                raise 'Error'
            #setup the SR channel
            for flv in ch:
                ch[flv].useOverflowBin=True
                myTopLvl.setSignalChannels(ch[flv])

        #Create signal sample and add to the whole fit config
        sigSample = Sample(sig,kPink)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        """
        for flv in aFlvChans:
            for chan in aFlvChans[flv]:
                print flv,chan.name,[x.name for x in chan.sampleList]
        for chan in myTopLvl.channels:
            print chan.name,[x.name for x in chan.sampleList]
        """
        # Set All filelist again
        for flv in bkgFiles:SetFileList(topCfg=myTopLvl,channels=aFlvChans[flv],samples=[sig]         ,fileList=sigFiles[flv])
        for flv in sigFiles:SetFileList(topCfg=myTopLvl,channels=aFlvChans[flv],samples=bkgSampleNames,fileList=bkgFiles[flv])


        # Add systematics
        # 1, Overall Systematics
        # sig
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(xsecSig)
        # SR Syst (30%)
        for reg in Regions:
            AddSystematics(topCfg=myTopLvl,channels=aCVSChans['SR'],samples=[sig]         ,systList=[sigSyst[reg]])
            AddSystematics(topCfg=myTopLvl,channels=aCVSChans['SR'],samples=bkgSampleNames,systList=[bkgSyst[reg]])

"""
# Systematics related to all SR+CR+VR
for cr in Regions:
    for chan in bVetoChans[cr]:
        chan.hasBQCD = True
    for chan in bReqChans[cr]:
        chan.hasBQCD = True
    for chan in bAgnosticChans[cr]:
        chan.hasBQCD = False
        chan.removeWeight("bTagWeight[3]")
"""
           
#######################
## Cosmetic Settings ##
#######################
# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.6,0.5,0.88,0.90,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total pdf","lf")
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)

entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","t#bar{t} Powheg","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)

entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","Dibosons","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
##
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Multijet","lf")
entry.SetLineColor(QCDSample.color)
entry.SetFillColor(QCDSample.color)
entry.SetFillStyle(compFillStyle)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if myFitType==FitType.Exclusion: 
    entry = leg.AddEntry("","Signal","lf")
    entry.SetLineColor(sigSample.color)
    entry.SetFillColor(sigSample.color)
    entry.SetFillStyle(compFillStyle)
    myTopLvl.tLegend = leg
    
c.Close()

# Plot "ATLAS" label
"""
for chan in AllChans:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion: chan.logY = True
    if chan.logY:
        chan.minY = 1.5
        chan.maxY = 1000000
    else:
        chan.minY = 0.05 
        chan.maxY = 3000
    chan.ATLASLabelX = 0.15
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.ShowLumi = True

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Entries"
            chan.minY = 0.05 
            chan.maxY = 30
            chan.ATLASLabelX = 0.15
            chan.ATLASLabelY = 0.83
            chan.ATLASLabelText = "Internal"
            chan.ShowLumi = True


"""
