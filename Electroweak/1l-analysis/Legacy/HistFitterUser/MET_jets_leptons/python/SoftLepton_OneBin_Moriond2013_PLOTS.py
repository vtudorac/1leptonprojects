################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
import ROOT
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import sqrt
import pickle
from os import sys
from math import exp

from logger import Logger
log = Logger('SoftLepton')

# ********************************************************************* #
# some useful helper functions
# ********************************************************************* #

def myreplace(l1,l2,element):
    idx=l1.index(element)
    if idx>=0:
        return l1[:idx] + l2 + l1[idx+1:]
    else:
        print "WARNING idx negative: %d" % idx
        return l1

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupSamples(samples,systList):
    for sample in samples:
        for syst in systList:
            sample.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #

#print sys.argv

# ********************************************************************* #
#                              Analysis parameters
# ********************************************************************* #

useBenchmarkInPlots = True

if not 'useStat' in dir():
    useStat=True
if not 'chn' in dir():
    chn=1                   # analysis channel 0=A,1=B,..
if not 'grid' in dir():
    grid="" # "SU"               # only grid implemented up to now
if not 'gridspec' in dir():
    gridspec="_" # "SU"               # only grid implemented up to now
if not 'suffix' in dir():
    suffix="_NoSys"
if not 'sigSamples' in dir():
    sigSamples=None #["1000_250"]
if not 'anaName' in dir():
    anaName = 'SRs1L'
if not 'includeSyst' in dir():
    includeSyst = True
if not 'dobtag' in dir():
    dobtag = True
if not 'doBlinding' in dir():
    doBlinding = False


configMgr.fixSigXSec=True

# ********************************************************************* #
#                              Options
# ********************************************************************* #

# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    if pickedSRs[0]=="SR3j":
        chn=0
    elif pickedSRs[0]=="SR5j":
        chn=7
    elif pickedSRs[0]=="SRs1L":
        chn=1
    elif pickedSRs[0]=="SR1a":
        chn=2
    elif pickedSRs[0]=="SR1b":
        chn=3
    elif pickedSRs[0]=="SR2a":
        chn=4
    elif pickedSRs[0]=="SR2b":
        chn=5
    elif pickedSRs[0]=="SRs2L":
        chn=6
    else:
        print "WARNING: analysis not defined"
        sys.exit()
    anaName=pickedSRs[0]
    # set the grid
    if len(pickedSRs) >= 2: 
        grid = pickedSRs[1] 
        pass
    if len(pickedSRs) >= 3: 
        gridspec = '_'+pickedSRs[2]+'_'
        pass

if chn>7 or chn<0:
    print "ERROR analysis not defined!!!"
    print chn
    print pickedSRs
    sys.exit()

allpoints=['500_400_60']

if chn==0 or chn==1 or chn==7:
    # traditional soft 1-lepton analysis
    if grid=="SM_GG1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
    elif grid=="SM_SS1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_1190_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
elif chn>=2 and chn<=5:
    # stop soft 1-lepton analysis
    if grid=="StopBCharDeg" and gridspec=="_5gev_":
        allpoints=['150_105_100', '150_140_135', '200_105_100', '200_155_150', '200_190_185', '250_105_100', '250_155_150', '250_205_200', '250_240_235', '300_105_100', '300_155_150', '300_205_200', '300_255_250', '300_290_285', '350_155_150', '350_205_200', '350_255_250', '350_305_300', '350_340_335', '400_105_100', '400_205_200', '400_255_250', '400_305_300', '400_355_350', '400_390_385', '450_155_150', '450_255_250', '450_305_300', '450_355_350', '450_405_400', '450_440_435', '500_105_100', '500_205_200', '500_255_250', '500_305_300', '500_355_350', '500_405_400', '500_455_450', '500_490_485', '550_105_100', '550_155_150', '550_205_200', '550_255_250', '550_305_300', '550_355_350', '550_405_400', '550_455_450', '600_105_100', '600_155_150', '600_205_200', '600_255_250', '600_305_300', '600_355_350', '600_405_400', '600_455_450', '650_105_100', '650_155_150', '650_205_200', '650_255_250', '650_305_300', '650_355_350', '650_405_400', '650_455_450', '700_105_100', '700_155_150', '700_205_200', '700_255_250', '700_305_300', '700_355_350', '700_405_400', '700_455_450', '750_105_100', '750_155_150', '750_205_200', '750_255_250', '750_305_300', '750_355_350', '750_405_400', '750_455_450', '800_105_100', '800_155_150', '800_205_200', '800_255_250', '800_305_300', '800_355_350', '800_405_400', '800_455_450']
    elif grid=="StopBCharDeg" and gridspec=="_20gev_":
        allpoints=['150_140_120', '150_95_75', '200_120_100', '200_170_150', '200_190_170', '200_95_75', '250_120_100', '250_170_150', '250_220_200', '250_240_220', '250_95_75', '300_120_100', '300_170_150', '300_220_200', '300_270_250', '300_290_270', '350_170_150', '350_220_200', '350_270_250', '350_320_300', '350_340_320', '350_95_75', '400_120_100', '400_220_200', '400_270_250', '400_320_300', '400_370_350', '400_390_370', '450_170_150', '450_220_200', '450_270_250', '450_320_300', '450_370_350', '450_420_400', '450_440_420', '450_95_75', '500_120_100', '500_220_200', '500_270_250', '500_320_300', '500_370_350', '500_420_400', '500_470_450', '500_490_470', '550_120_100', '550_170_150', '550_220_200', '550_270_250', '550_320_300', '550_370_350', '550_420_400', '550_470_450', '550_95_75', '600_120_100', '600_170_150', '600_220_200', '600_270_250', '600_320_300', '600_370_350', '600_420_400', '600_470_450', '600_95_75', '650_120_100', '650_170_150', '650_220_200', '650_270_250', '650_320_300', '650_370_350', '650_420_400', '650_470_450', '650_95_75', '700_120_100', '700_170_150', '700_220_200', '700_270_250', '700_320_300', '700_370_350', '700_420_400', '700_470_450', '700_95_75', '750_120_100', '750_170_150', '750_220_200', '750_270_250', '750_320_300', '750_370_350', '750_420_400', '750_470_450', '750_95_75', '800_120_100', '800_170_150', '800_220_200', '800_270_250', '800_320_300', '800_370_350', '800_420_400', '800_470_450', '800_95_75']
    pass
elif chn==6:
    # ued soft 2-lepton analysis
    if grid=="mUED2Lfilter":
        allpoints=['1000_10', '1000_3', '1000_40', '1100_10', '1100_3', '1100_40', '1200_10', '1200_3', '1200_40', '1300_10', '1300_3', '1300_40', '700_10', '700_3', '700_40', '800_10', '800_3', '800_40', '900_10', '900_3', '900_40']
    pass

# No input signal for discovery and bkg fit
if myFitType==FitType.Discovery:
    allpoints=["Discovery"]
    grid=""
    pass
elif myFitType==FitType.Background:
    allpoints=["Background"]
    grid=""
    pass

# sigSamples is set by the "-g" HistFitter option. Overwrites allpoints, used below.
try:
    sigSamples
except NameError:
    sigSamples = None
    
if sigSamples!=None:
    allpoints=sigSamples

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

#configMgr.doHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

# ********************************************************************* #
# Main part - now we start to build the data model
# ********************************************************************* #

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.490 # 14.3 #21.0
configMgr.setLumiUnits("fb-1")

inputDirData_p1328 = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v7_1_1/" # "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v5_6_1/" 
inputDirSlim_p1328 = "root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v7_1_1_slim/" # "/eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v5_5_1_slim_01/"
inputDirSig_p1328  = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v7_1_1/" # "/eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v5_5_1/"
inputDirQCD_p1328  = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v7_1_1/"

#Split bdgFiles per channel
sigFiles = []
if myFitType==FitType.Exclusion:
    if (chn>=0 and chn<=5) or chn==7: # soft 1-lepton
        #if chn>=0 and chn<=5: # soft 1-lepton
        sigFiles += [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
    elif chn==6:          # soft 2-lepton
        sigFiles += [inputDirSig_p1328+"sigtree_Soft2Lep_"+grid+".root"]
    if len(sigFiles)==0:
        print "WARNING: signal grid files not defined"
        sys.exit()
    pass

# Set the files to read from
if (chn>=0 and chn<=5) or chn==7: # soft 1-lepton
    if True:
        dataFiles              = [inputDirData_p1328+"datatree_Soft1Lep.root"] #"Soft1Lep_Data.root"]
        qcdFiles               = [inputDirQCD_p1328 +"datatree_Soft1Lep.root"] #"Soft1Lep_QCD.root"]
        AlpgenDYFiles          = [inputDirSlim_p1328+"AlpgenDY_Soft1Lep.root"]
        AlpgenWFiles           = [inputDirSlim_p1328+"AlpgenJimmyW_Soft1Lep.root"] #"AlpgenW_Soft1Lep.root"]
        AlpgenZFiles           = [inputDirSlim_p1328+"AlpgenZ_Soft1Lep.root"]
        SherpaDibosonsFiles    = [inputDirSlim_p1328+"SherpaDibosons_Soft1Lep.root"]
        DibosonsFiles          = [inputDirSlim_p1328+"Dibosons_Soft1Lep.root"]
        PowhegPythiaTTbarFiles = [inputDirSlim_p1328+"PowhegPythiaTTbar_Soft1Lep.root"]
        PowhegJimmyTTbarFiles  = [inputDirSlim_p1328+"PowhegJimmyTTbar_Soft1Lep.root"]
        AlpgenTTbarFiles       = [inputDirSlim_p1328+"AlpgenTTbar_Soft1Lep.root"]
        SherpaWMassiveBFiles   = [inputDirSlim_p1328+"SherpaWMassiveB_Soft1Lep.root"]
        SherpaZMassiveBFiles   = [inputDirSlim_p1328+"SherpaZMassiveB_Soft1Lep.root"]
        SingleTopFiles         = [inputDirSlim_p1328+"SingleTop_Soft1Lep.root"]
        ttbarVFiles            = [inputDirSlim_p1328+"ttbarV_Soft1Lep.root"]
        pass
elif chn==6:          # soft 2-lepton
    dataFiles              = [inputDirData_p1328+"datatree_Soft2Lep.root"]
    qcdFiles               = [inputDirQCD_p1328 +"datatree_Soft2Lep.root"] # "Soft2Lep_QCD.root"]
    AlpgenDYFiles          = [inputDirSlim_p1328+"AlpgenDY_Soft2Lep.root"]
    AlpgenWFiles           = [inputDirSlim_p1328+"AlpgenJimmyW_Soft2Lep.root"] #"AlpgenW_Soft2Lep.root"]
    AlpgenZFiles           = [inputDirSlim_p1328+"AlpgenZ_Soft2Lep.root"]
    SherpaDibosonsFiles    = [inputDirSlim_p1328+"SherpaDibosons_Soft2Lep.root"]
    #
    DibosonsFiles          = [inputDirSlim_p1328+"Dibosons_Soft2Lep.root"]
    PowhegPythiaTTbarFiles = [inputDirSlim_p1328+"PowhegPythiaTTbar_Soft2Lep.root"]
    PowhegJimmyTTbarFiles  = [inputDirSlim_p1328+"PowhegJimmyTTbar_Soft2Lep.root"]
    AlpgenTTbarFiles       = [inputDirSlim_p1328+"AlpgenTTbar_Soft2Lep.root"]
    SherpaWMassiveBFiles   = [inputDirSlim_p1328+"SherpaWMassiveB_Soft2Lep.root"]
    SherpaZMassiveBFiles   = [inputDirSlim_p1328+"SherpaZMassiveB_Soft2Lep.root"]
    SingleTopFiles         = [inputDirSlim_p1328+"SingleTop_Soft2Lep.root"]
    ttbarVFiles            = [inputDirSlim_p1328+"ttbarV_Soft2Lep.root"]
    pass


    
########################################
# Analysis description
########################################

## Lists of weights 
weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","SherpaWweight"]

toreplace = ""
if chn==0 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==7 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==1 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==2 and dobtag:
    #weights += ["bTagWeight[7]"]
    bweights = weights + ["bTagWeight[7]"]
    toreplace = "bTagWeight[7]"
    pass
elif chn==3 and dobtag:
    #weights += ["bTagWeight[7]"]
    bweights = weights + ["bTagWeight[7]"]
    toreplace = "bTagWeight[7]"
    pass
elif chn==4 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==5 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==6 and dobtag:
    #weights += ["bTagWeight[10]"]
    bweights = weights + ["bTagWeight[10]"]
    toreplace = "bTagWeight[10]"
    pass

configMgr.weights = weights # bweights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights, "leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

if dobtag:
    bTagHighWeights9 = replaceWeight(bweights,toreplace,"bTagWeightBUp[8]")
    bTagLowWeights9  = replaceWeight(bweights,toreplace,"bTagWeightBDown[8]")
    bTagHighWeights8 = replaceWeight(bweights,toreplace,"bTagWeightBUp[7]")
    bTagLowWeights8  = replaceWeight(bweights,toreplace,"bTagWeightBDown[7]")
    bTagHighWeights11 = replaceWeight(bweights,toreplace,"bTagWeightBUp[10]")
    bTagLowWeights11  = replaceWeight(bweights,toreplace,"bTagWeightBDown[10]")


#-------------------------------------------------------------------------
# Blinding of SR
#-------------------------------------------------------------------------

configMgr.blindSR = doBlinding
configMgr.blindVR = False # doBlinding

#--------------------------------------------------------------------------
# List of systematics
#--------------------------------------------------------------------------

configMgr.nomName = "_NoSys"

## JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig",   "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallHistoSys")
jes       = Systematic("JES",    "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallNormHistoSys") # JES uncertainty - for low pt jets
jer       = Systematic("JER",    "_NoSys", "_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym")

jlow      = Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys") # JES uncertainty - for low pt jets
jmed      = Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys") # JES uncertainty - for medium pt jets
jhigh     = Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys") # JES uncertainty - for high pt jets

## MET uncertainty
scalest   = Systematic("SCALEST","_NoSys", "_SCALESTup","_SCALESTdown","tree","overallNormHistoSys")
resost    = Systematic( "RESOST","_NoSys", "_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym")

## pile-up
pileup = Systematic("pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys")

## b-tagging
if dobtag:
    bTagSyst9  = Systematic("BT",bweights,bTagHighWeights9,bTagLowWeights9,"weight","overallNormHistoSys")
    bTagSyst8  = Systematic("BT",bweights,bTagHighWeights8,bTagLowWeights8,"weight","overallNormHistoSys")
    bTagSyst11 = Systematic("BT",bweights,bTagHighWeights11,bTagLowWeights11,"weight","overallNormHistoSys")

## Trigger efficiency --> should be taken from data/mc trigger study
#trEl = Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#trMu = Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#####trEff= Systematic("TE",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallNormHistoSys")
trEff = Systematic("TE",configMgr.weights,1.05,0.95,"user","userOverallSys")

## MC theoretical uncertainties
## Signal XSec uncertainty as overallSys (pure yeild affect) 
xsecSig = Systematic("SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys" )

## generator level uncertainties
#SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")

# qfacUpWeightW:qfacDownWeightW:ktfacUpWeightW:ktfacDownWeightW

qfacW  = Systematic("qfacW", configMgr.weights,configMgr.weights+["qfacUpWeightW"], configMgr.weights+["qfacDownWeightW"], "weight","overallNormHistoSys")
ktfacW = Systematic("ktfacW",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","overallNormHistoSys")

wbb    = Systematic("wbb", configMgr.weights,1.24 ,0.76, "user","userOverallSys")

## qcd stat/syst weights automatically picked up.

## Lepton weight uncertainty
#elEff = Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys") 
#muEff = Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")
lepEff= Systematic("LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallHistoSys")

## Electron energy scale uncertainty
#ees = Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")
egzee = Systematic("egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys")
egmat = Systematic("egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallSys")
egps  = Systematic("egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys")
eglow = Systematic("eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys")
egres = Systematic("egres","_NoSys","_EGRESup","_EGRESdown","tree","overallSys")

## Muon energy resolutions
merm = Systematic("LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallSys")
meri = Systematic("LRImu","_NoSys","_MIDup","_MIDdown","tree","overallSys")

## muon energy scale systematics
mes = Systematic("mes","_NoSys","_MSCALEup","_MSCALEdown","tree","overallSys")

## sherpa / alpgen generator difference
sherpgen = Systematic("sherpgen", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
sherpgen.setFileList('SherpaWMassiveB', AlpgenWFiles)
sherpgen.setTreeName('SherpaWMassiveB','AlpgenJimmyW_NoSys')

## powheg pythia/herwig parton shower difference
pythwig = Systematic("pythwig", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
pythwig.setFileList('PowhegPythiaTTbar', PowhegJimmyTTbarFiles)
pythwig.setTreeName('PowhegPythiaTTbar','PowhegJimmyTTbar_NoSys')

## powheg alpgen ttbar generator difference
pythgen = Systematic("pythgen", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
pythgen.setFileList('PowhegPythiaTTbar', AlpgenTTbarFiles)
pythgen.setTreeName('PowhegPythiaTTbar','AlpgenTTbar_NoSys')


## pdf
pdfIntraSyst = Systematic("pdfIntra",configMgr.weights,configMgr.weights+["pdfWeightVars[0]"],configMgr.weights+["pdfWeightVars[1]"],"weight","overallNormHistoSys")
pdfInterSyst = Systematic("pdfInter",configMgr.weights,configMgr.weights+["pdfWeight"],configMgr.weights+["pdfWeight"],"weight","overallNormHistoSysOneSideSym")

generatorSyst = []
generatorSystW = []

# Import top theory uncertainties from Lily
from topTheoryUncertainties import *

if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoPSSR1L3j     ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoPSCRT3j      ))
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoPSCRW3j      ))
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L3jMET"),         topTheoPSSR1L3j     ))
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L3jMETMEFF"),         topTheoPSSR1L3j     ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoPSSR1L5j     ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoPSCRT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoPSCRW5j      ))    
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L5jMET"),         topTheoPSSR1L5j     ))
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L5jMETMEFF"),         topTheoPSSR1L5j     ))
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoPSSR1L3j     ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoPSCRT3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoPSCRW3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoPSSR1L5j     ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRT5j"),          topTheoPSCRT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW5j"),          topTheoPSCRW5j      ))    
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Ba"),        topTheoPSSR1L2Ba    ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),     topTheoPSCRWbb1L2Ba ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoPSCRW1L2Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoPSCRT1L2Ba  ))         
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L2BaMCT"),        topTheoPSSR1L2Ba    ))    
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Bc"),       topTheoPSSR1L2Bc ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),    topTheoPSCRWbb1L2Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),      topTheoPSCRW1L2Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),      topTheoPSCRT1L2Bc  ))       
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L2BcMCT"),       topTheoPSSR1L2Bc ))         
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Ba"),       topTheoPSSR1L1Ba  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),    topTheoPSCRWbb1L1Ba  ))     
    generatorSyst.append((("PowhegPythiaTTbar","cuts_CRW"),      topTheoPSCRW1L1Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),      topTheoPSCRT1L1Ba  ))       
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BaMET"),       topTheoPSSR1L1Ba  ))        
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BaMETMEFF"),       topTheoPSSR1L1Ba  ))        
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Bc"),       topTheoPSSR1L1Bc  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),    topTheoPSCRWbb1L1Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),      topTheoPSCRW1L1Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),      topTheoPSCRT1L1Bc  ))       
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BcMET"),       topTheoPSSR1L1Bc  ))        
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BcMETMEFF"),       topTheoPSSR1L1Bc  ))
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","metmeffInc25_jvf25_SR2LMETMEFF"),          topTheoPSSR2LBins  ))           
    generatorSyst.append((("PowhegPythiaTTbar","met_SR2LMET"),          topTheoPSSR2LBins  ))           
    generatorSyst.append((("PowhegPythiaTTbar","cuts_SR2L"),          topTheoPSSR2L  ))           
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),         topTheoPSCRT2L  ))          
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR2LMET"),          topTheoPSSR2L  ))           
##     generatorSyst.append((("PowhegPythiaTTbar","SR2LMETMEFF"),          topTheoPSSR2L  ))        
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),         topTheoPSVR3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),        topTheoPSVRT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),        topTheoPSVRW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),         topTheoPSVR3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),        topTheoPSVRT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),        topTheoPSVRW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),         topTheoPSVR3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),        topTheoPSVRT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),        topTheoPSVRW3j3  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),         topTheoPSVR5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),        topTheoPSVRT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),        topTheoPSVRW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),         topTheoPSVR5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),        topTheoPSVRT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),        topTheoPSVRW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),         topTheoPSVR5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),        topTheoPSVRT5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),        topTheoPSVRW5j3  ))         
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),         topTheoPSVR3j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),        topTheoPSVRT3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),        topTheoPSVRW3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),         topTheoPSVR3j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),        topTheoPSVRT3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),        topTheoPSVRW3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),         topTheoPSVR3j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),        topTheoPSVRT3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),        topTheoPSVRW3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),         topTheoPSVR5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),        topTheoPSVRT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),        topTheoPSVRW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),         topTheoPSVR5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),        topTheoPSVRT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),        topTheoPSVRW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),         topTheoPSVR5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),        topTheoPSVRT5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),        topTheoPSVRW5j3  ))         
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L2Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L2Ba2  ))       
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L2Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L2Bc2  ))       
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L1Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L1Ba2  ))       
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L1Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L1Bc2  ))       
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR2L1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR2L2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),         topTheoPSVR2L3  ))               

if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),       topTheoRenScSR1L3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),        topTheoRenScCRW3j  ))
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L3jMET"),       topTheoRenScSR1L3j  ))
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L3jMETMEFF"),       topTheoRenScSR1L3j  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),       topTheoRenScSR1L5j  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),        topTheoRenScCRW5j  ))        
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L5jMET"),       topTheoRenScSR1L5j  ))
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L5jMETMEFF"),       topTheoRenScSR1L5j  ))
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),       topTheoRenScSR1L3j  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),        topTheoRenScCRW3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),       topTheoRenScSR1L5j  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT5j"),        topTheoRenScCRT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW5j"),        topTheoRenScCRW5j  ))        
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Ba"),      topTheoRenScSR1L2Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),   topTheoRenScCRWbb1L2Ba  ))   
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L2Ba  ))     
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L2BaMCT"),      topTheoRenScSR1L2Ba  ))      
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Bc"),      topTheoRenScSR1L2Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),   topTheoRenScCRWbb1L2Bc  ))   
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L2Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),     topTheoRenScCRT1L2Bc  ))     
##    #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L2BcMCT"),      topTheoRenScSR1L2Bc  ))
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Ba"),      topTheoRenScSR1L1Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),   topTheoRenScCRWbb1L1Ba  ))   
    generatorSyst.append((("PowhegPythiaTTbar","cuts_CRW"),     topTheoRenScCRW1L1Ba  ))     
    generatorSyst.append((("PowhegPythiaTTbar","cuts_CRT"),     topTheoRenScCRT1L1Ba  ))     
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BaMET"),      topTheoRenScSR1L1Ba  ))      
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BaMETMEFF"),      topTheoRenScSR1L1Ba  ))      
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Bc"),      topTheoRenScSR1L1Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),   topTheoRenScCRWbb1L1Bc  ))   
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L1Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),     topTheoRenScCRT1L1Bc  ))     
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BcMET"),      topTheoRenScSR1L1Bc  ))      
##     generatorSyst.append((("PowhegPythiaTTbar","SR1L1BcMETMEFF"),      topTheoRenScSR1L1Bc  ))
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","SR2L"),         topTheoRenScSR2L  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT2L  ))        
##     #
##     generatorSyst.append((("PowhegPythiaTTbar","SR2LMET"),         topTheoRenScSR2L  ))         
##     generatorSyst.append((("PowhegPythiaTTbar","SR2LMETMEFF"),         topTheoRenScSR2L  ))         
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),        topTheoRenScVR3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),       topTheoRenScVRT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),       topTheoRenScVRW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),        topTheoRenScVR3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),       topTheoRenScVRT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),       topTheoRenScVRW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),        topTheoRenScVR3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),       topTheoRenScVRT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),       topTheoRenScVRW3j3  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),        topTheoRenScVR5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),       topTheoRenScVRT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),       topTheoRenScVRW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),        topTheoRenScVR5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),       topTheoRenScVRT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),       topTheoRenScVRW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),        topTheoRenScVR5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),       topTheoRenScVRT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),       topTheoRenScVRW5j3  ))       
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),        topTheoRenScVR3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),       topTheoRenScVRT3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),       topTheoRenScVRW3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),        topTheoRenScVR3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),       topTheoRenScVRT3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),       topTheoRenScVRW3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),        topTheoRenScVR3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),       topTheoRenScVRT3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),       topTheoRenScVRW3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),        topTheoRenScVR5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),       topTheoRenScVRT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),       topTheoRenScVRW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),        topTheoRenScVR5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),       topTheoRenScVRT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),       topTheoRenScVRW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),        topTheoRenScVR5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),       topTheoRenScVRT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),       topTheoRenScVRW5j3  ))       
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L2Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L2Ba2  ))     
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L2Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L2Bc2  ))     
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L1Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L1Ba2  ))     
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L1Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L1Bc2  ))     
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),        topTheoRenScVR2L1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),        topTheoRenScVR2L2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),        topTheoRenScVR2L3  ))        

if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoFacScSR1L3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoFacScCRW3j  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoFacScSR1L5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoFacScCRW5j  ))         
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoFacScSR1L3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoFacScCRW3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoFacScSR1L5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRT5j"),          topTheoFacScCRT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW5j"),          topTheoFacScCRW5j  ))         
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Ba"),        topTheoFacScSR1L2Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),     topTheoFacScCRWbb1L2Ba  ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L2Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L2Ba  ))      
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Bc"),        topTheoFacScSR1L2Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),     topTheoFacScCRWbb1L2Bc  ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L2Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L2Bc  ))      
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Ba"),        topTheoFacScSR1L1Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),     topTheoFacScCRWbb1L1Ba  ))    
    generatorSyst.append((("PowhegPythiaTTbar","cuts_CRW"),       topTheoFacScCRW1L1Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","cuts_CRT"),       topTheoFacScCRT1L1Ba  ))      
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Bc"),        topTheoFacScSR1L1Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRWbb"),     topTheoFacScCRWbb1L1Bc  ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L1Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L1Bc  ))      
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","SR2L"),           topTheoFacScSR2L  ))          
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT2L  ))         
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),          topTheoFacScVR3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),         topTheoFacScVRT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),         topTheoFacScVRW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),          topTheoFacScVR3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),         topTheoFacScVRT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),         topTheoFacScVRW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),          topTheoFacScVR3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),         topTheoFacScVRT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),         topTheoFacScVRW3j3  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),          topTheoFacScVR5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),         topTheoFacScVRT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),         topTheoFacScVRW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),          topTheoFacScVR5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),         topTheoFacScVRT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),         topTheoFacScVRW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),          topTheoFacScVR5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),         topTheoFacScVRT5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),         topTheoFacScVRW5j3  ))        
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),          topTheoFacScVR3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),         topTheoFacScVRT3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),         topTheoFacScVRW3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),          topTheoFacScVR3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),         topTheoFacScVRT3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),         topTheoFacScVRW3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),          topTheoFacScVR3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),         topTheoFacScVRT3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),         topTheoFacScVRW3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),          topTheoFacScVR5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),         topTheoFacScVRT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),         topTheoFacScVRW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),          topTheoFacScVR5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),         topTheoFacScVRT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),         topTheoFacScVRW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),          topTheoFacScVR5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),         topTheoFacScVRT5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),         topTheoFacScVRW5j3  ))        
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L2Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L2Ba2  ))      
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L2Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L2Bc2  ))      
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L1Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L1Ba2  ))      
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L1Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L1Bc2  ))      
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),          topTheoFacScVR2L1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),          topTheoFacScVR2L2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),          topTheoFacScVR2L3  ))         

if chn==0:
    generatorSystW.append((("SherpaWMassiveB","SR1L3j"),       WTheoNpartSR1L3j       ))
    generatorSystW.append((("SherpaWMassiveB","CRT"),        WTheoNpartCRT3j        ))
    generatorSystW.append((("SherpaWMassiveB","CRW"),        WTheoNpartCRW3j        ))
##     #
##     generatorSystW.append((("SherpaWMassiveB","SR1L3jMET"),       WTheoNpartSR1L3j       ))
##     generatorSystW.append((("SherpaWMassiveB","SR1L3jMETMEFF"),       WTheoNpartSR1L3j       ))
if chn==7:
    generatorSystW.append((("SherpaWMassiveB","SR1L5j"),       WTheoNpartSR1L5j       ))
    generatorSystW.append((("SherpaWMassiveB","CRT"),        WTheoNpartCRT5j        ))
    generatorSystW.append((("SherpaWMassiveB","CRW"),        WTheoNpartCRW5j        ))
##     #
##     generatorSystW.append((("SherpaWMassiveB","SR1L5jMET"),       WTheoNpartSR1L5j       ))
##     generatorSystW.append((("SherpaWMassiveB","SR1L5jMETMEFF"),       WTheoNpartSR1L5j       ))
    pass
if chn==1:
    generatorSystW.append((("SherpaWMassiveB","SR1L3j"),       WTheoNpartSR1L3j       ))
    generatorSystW.append((("SherpaWMassiveB","CRT"),        WTheoNpartCRT3j        ))
    generatorSystW.append((("SherpaWMassiveB","CRW"),        WTheoNpartCRW3j        ))
if chn==2:
    generatorSystW.append((("SherpaWMassiveB","SR1L2Ba"),      WTheoNpartSR1L2Ba      ))
    generatorSystW.append((("SherpaWMassiveB","CRWbb"),   WTheoNpartCRWbb1L2Ba   ))
    generatorSystW.append((("SherpaWMassiveB","CRW"),     WTheoNpartCRW1L2Ba     ))
    generatorSystW.append((("SherpaWMassiveB","CRT"),     WTheoNpartCRT1L2Ba     ))
##     #
##     generatorSystW.append((("SherpaWMassiveB","SR1L2BaMCT"),      WTheoNpartSR1L2Ba      ))
if chn==3:
    generatorSystW.append((("SherpaWMassiveB","SR1L2Bc"),      WTheoNpartSR1L2Bc      ))
    generatorSystW.append((("SherpaWMassiveB","CRWbb"),   WTheoNpartCRWbb1L2Bc   ))
    generatorSystW.append((("SherpaWMassiveB","CRW"),     WTheoNpartCRW1L2Bc     ))
    generatorSystW.append((("SherpaWMassiveB","CRT"),     WTheoNpartCRT1L2Bc     ))
##     #
##     generatorSystW.append((("SherpaWMassiveB","SR1L2BcMCT"),      WTheoNpartSR1L2Bc      ))
if chn==4:
    generatorSystW.append((("SherpaWMassiveB","SR1L1Ba"),      WTheoNpartSR1L1Ba      ))
    generatorSystW.append((("SherpaWMassiveB","CRWbb"),   WTheoNpartCRWbb1L1Ba   ))
    generatorSystW.append((("SherpaWMassiveB","cuts_CRW"),     WTheoNpartCRW1L1Ba     ))
    generatorSystW.append((("SherpaWMassiveB","cuts_CRT"),     WTheoNpartCRT1L1Ba     ))
##     #
##     generatorSystW.append((("SherpaWMassiveB","SR1L1BaMET"),      WTheoNpartSR1L1Ba      ))
##     generatorSystW.append((("SherpaWMassiveB","SR1L1BaMETMEFF"),      WTheoNpartSR1L1Ba      ))
if chn==5:
    generatorSystW.append((("SherpaWMassiveB","SR1L1Bc"),      WTheoNpartSR1L1Bc      ))
    generatorSystW.append((("SherpaWMassiveB","CRWbb"),   WTheoNpartCRWbb1L1Bc   ))
    generatorSystW.append((("SherpaWMassiveB","CRW"),     WTheoNpartCRW1L1Bc     ))
    generatorSystW.append((("SherpaWMassiveB","CRT"),     WTheoNpartCRT1L1Bc     ))
##     #
##     generatorSystW.append((("SherpaWMassiveB","SR1L1BcMET"),      WTheoNpartSR1L1Bc      ))
##     generatorSystW.append((("SherpaWMassiveB","SR1L1BcMETMEFF"),      WTheoNpartSR1L1Bc      ))
#if chn==6:
#    generatorSystW.append((("SherpaWMassiveB","SR2L"),         WTheoNpartSR2L         ))
#    generatorSystW.append((("SherpaWMassiveB","CRT"),        WTheoNpartCRT2L        ))
if chn==0:
    generatorSystW.append((("SherpaWMassiveB","VR3j1"),        WTheoNpartVR3j1        ))
    generatorSystW.append((("SherpaWMassiveB","VRT3j1"),       WTheoNpartVRT3j1       ))
    generatorSystW.append((("SherpaWMassiveB","VRW3j1"),       WTheoNpartVRW3j1       ))
    generatorSystW.append((("SherpaWMassiveB","VR3j2"),        WTheoNpartVR3j2        ))
    generatorSystW.append((("SherpaWMassiveB","VRT3j2"),       WTheoNpartVRT3j2       ))
    generatorSystW.append((("SherpaWMassiveB","VRW3j2"),       WTheoNpartVRW3j2       ))
    generatorSystW.append((("SherpaWMassiveB","VR3j3"),        WTheoNpartVR3j3        ))
    generatorSystW.append((("SherpaWMassiveB","VRT3j3"),       WTheoNpartVRT3j3       ))
    generatorSystW.append((("SherpaWMassiveB","VRW3j3"),       WTheoNpartVRW3j3       ))
if chn==7:
    generatorSystW.append((("SherpaWMassiveB","VR5j1"),        WTheoNpartVR5j1        ))
    generatorSystW.append((("SherpaWMassiveB","VRT5j1"),       WTheoNpartVRT5j1       ))
    generatorSystW.append((("SherpaWMassiveB","VRW5j1"),       WTheoNpartVRW5j1       ))
    generatorSystW.append((("SherpaWMassiveB","VR5j2"),        WTheoNpartVR5j2        ))
    generatorSystW.append((("SherpaWMassiveB","VRT5j2"),       WTheoNpartVRT5j2       ))
    generatorSystW.append((("SherpaWMassiveB","VRW5j2"),       WTheoNpartVRW5j2       ))
    generatorSystW.append((("SherpaWMassiveB","VR5j3"),        WTheoNpartVR5j3        ))
    generatorSystW.append((("SherpaWMassiveB","VRT5j3"),       WTheoNpartVRT5j3       ))
    generatorSystW.append((("SherpaWMassiveB","VRW5j3"),       WTheoNpartVRW5j3       ))
    pass
if chn==1:
    generatorSystW.append((("SherpaWMassiveB","VR3j1"),        WTheoNpartVR3j1        ))
    generatorSystW.append((("SherpaWMassiveB","VRT3j1"),       WTheoNpartVRT3j1       ))
    generatorSystW.append((("SherpaWMassiveB","VRW3j1"),       WTheoNpartVRW3j1       ))
    generatorSystW.append((("SherpaWMassiveB","VR3j2"),        WTheoNpartVR3j2        ))
    generatorSystW.append((("SherpaWMassiveB","VRT3j2"),       WTheoNpartVRT3j2       ))
    generatorSystW.append((("SherpaWMassiveB","VRW3j2"),       WTheoNpartVRW3j2       ))
    generatorSystW.append((("SherpaWMassiveB","VR3j3"),        WTheoNpartVR3j3        ))
    generatorSystW.append((("SherpaWMassiveB","VRT3j3"),       WTheoNpartVRT3j3       ))
    generatorSystW.append((("SherpaWMassiveB","VRW3j3"),       WTheoNpartVRW3j3       ))
    generatorSystW.append((("SherpaWMassiveB","VR5j1"),        WTheoNpartVR5j1        ))
    generatorSystW.append((("SherpaWMassiveB","VRT5j1"),       WTheoNpartVRT5j1       ))
    generatorSystW.append((("SherpaWMassiveB","VRW5j1"),       WTheoNpartVRW5j1       ))
    generatorSystW.append((("SherpaWMassiveB","VR5j2"),        WTheoNpartVR5j2        ))
    generatorSystW.append((("SherpaWMassiveB","VRT5j2"),       WTheoNpartVRT5j2       ))
    generatorSystW.append((("SherpaWMassiveB","VRW5j2"),       WTheoNpartVRW5j2       ))
    generatorSystW.append((("SherpaWMassiveB","VR5j3"),        WTheoNpartVR5j3        ))
    generatorSystW.append((("SherpaWMassiveB","VRT5j3"),       WTheoNpartVRT5j3       ))
    generatorSystW.append((("SherpaWMassiveB","VRW5j3"),       WTheoNpartVRW5j3       ))
if chn==2:
    generatorSystW.append((("SherpaWMassiveB","VR1"),     WTheoNpartVR1L2Ba1     ))
    generatorSystW.append((("SherpaWMassiveB","VR2"),     WTheoNpartVR1L2Ba2     ))
if chn==3:
    generatorSystW.append((("SherpaWMassiveB","VR1"),     WTheoNpartVR1L2Bc1     ))
    generatorSystW.append((("SherpaWMassiveB","VR2"),     WTheoNpartVR1L2Bc2     ))
if chn==4:
    generatorSystW.append((("SherpaWMassiveB","VR1"),     WTheoNpartVR1L1Ba1     ))
    generatorSystW.append((("SherpaWMassiveB","VR2"),     WTheoNpartVR1L1Ba2     ))
if chn==5:
    generatorSystW.append((("SherpaWMassiveB","VR1"),     WTheoNpartVR1L1Bc1     ))
    generatorSystW.append((("SherpaWMassiveB","VR2"),     WTheoNpartVR1L1Bc2     ))
#if chn==6:
#    generatorSystW.append((("SherpaWMassiveB","VR1"),        WTheoNpartVR2L1        ))
#    generatorSystW.append((("SherpaWMassiveB","VR2"),        WTheoNpartVR2L2        ))
#    generatorSystW.append((("SherpaWMassiveB","VR3"),        WTheoNpartVR2L3        ))

generatorSyst += generatorSystW


SystList = []
if True:
    SystList.append(jes)
    SystList.append(jer)
    SystList.append(lepEff)
    SystList.append(scalest)
    SystList.append(resost)
    SystList.append(pileup)
    SystList.append(trEff)
    SystList.append(egzee)
    SystList.append(egmat)
    SystList.append(egps)
    SystList.append(eglow)
    SystList.append(egres)
    SystList.append(merm)
    SystList.append(meri)
    SystList.append(mes)

## do these per sample/channel
#SystList.append(bTagSyst)
#SystList.append(qfacT)
#SystList.append(qfacW)
#SystList.append(ktfacT)
#SystList.append(ktfacW)
#SystList.append(iqoptW)

#--------------------------------------------------------------------------
# List of channel selections
#--------------------------------------------------------------------------

TriggerSelection = "&& (AnalysisType==5) "
LeptonSelection  = TriggerSelection + "&& (lep1Pt<25) && ((lep1Pt>10&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))"
HardLeptonSelection = "&& (lep1Pt>25) && (lep2Pt<10) && ((lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Flavor==-1))"
RelaxedLeptonSelection = "&& (lep1Pt>10) && (lep2Pt<10) && ((lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Flavor==-1))"
    
if chn==0:
    LeptonSelection                  += " && dRminLepJet25_jvf25>1.0 " 
    HardLeptonSelection              += " && dRminLepJet25_jvf25>1.0 "
    Jet3Selection                    = "&& jet1Pt_jvf25>180 && nJet25_jvf25>=3 && jet5Pt_jvf25<25" # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    configMgr.cutsDict["SR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["SR1L3jMET"]     = "( met>300 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["SR1L3jMETMEFF"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0. )"             + Jet3Selection + LeptonSelection
    pass
    #
elif chn==7:
    LeptonSelection                  += " && dRminLepJet25_jvf25>1.0 "
    HardLeptonSelection              += " && dRminLepJet25_jvf25>1.0 "
    Jet5Selection                    = "&& (jet1Pt_jvf25>180 && jet5Pt_jvf25>25)"   # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25 && jetJVF[3]>0.5 && jetJVF[4]>0.25)"   
    # 
    configMgr.cutsDict["SR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict["CRT"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict["VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict["SR1L5jMET"]     = "( met>200 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict["SR1L5jMETMEFF"]     = "( met>300 && mt>100 && (met/(meffInc25_jvf25))>0. )"             + Jet5Selection + LeptonSelection
    pass
elif chn==1:
    LeptonSelection                  += " && dRminLepJet25_jvf25>1.0 "
    HardLeptonSelection              += " && dRminLepJet25_jvf25>1.0 "
    Jet3Selection                    = "&& jet1Pt_jvf25>180 && nJet25_jvf25>=3 && jet5Pt_jvf25<25" # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    Jet5Selection                    = "&& (jet1Pt_jvf25>180 && jet5Pt_jvf25>25)"   # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25 && jetJVF[3]>0.5 && jetJVF[4]>0.25)"   
    #
    configMgr.cutsDict["SR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["SR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict["CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["CRT5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict["VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    pass
    #
elif chn==2:
    # (lep1Pt<25. && met>200. && nB2Jet60==2 && nJet50_jvf25_wo2jets==0 && mctcorr>150. && ht_wo2jets<50. && dphimin>0.4);
    JetSelection               = "&& (nJet50_jvf25_wo2jets==0 && dphimin>0.4)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)" 
    BJetSelection              = "&& nB2Jet60==2" # "&& (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    BVetoSelection             = "&& nB2Jet60==0" # "&& (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    configMgr.cutsDict["SR1L2Ba"] = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict["CRWbb"] = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>200 && ht_wo2jets<50 && mctcorr>150)" + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && ht_wo2jets<50 && mctcorr<150)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met<200 && met>150 && ht_wo2jets<50 && mctcorr>150)" + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection
    #
    configMgr.cutsDict["SR1L2BaMCT"] = "(met>200 && ht_wo2jets<50 && mctcorr>100)"            + JetSelection + BJetSelection + LeptonSelection
    pass
elif chn==3:
    JetSelection               = "&& (nJet50_jvf25_wo2jets==0 && dphimin>0.4)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)" 
    BJetSelection              = "&& nB2Jet60==2" # "&&  (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    BVetoSelection             = "&& nB2Jet60==0"
    configMgr.cutsDict["SR1L2Bc"] = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + LeptonSelection 
    configMgr.cutsDict["CRWbb"] = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>300 && ht_wo2jets<50 && mctcorr>200)" + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && mctcorr<200)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<300 && mctcorr>200)" + JetSelection + BJetSelection + LeptonSelection 
    configMgr.cutsDict["VR2"]  = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection
    #
    configMgr.cutsDict["SR1L2BcMCT"] = "(met>300 && mctcorr>100)"            + JetSelection + BJetSelection + LeptonSelection 
    pass
elif chn==4:
    # (lep1Pt<25. && met>300. && mt>100. && jet1Pt_jvf25>180. && nJet25_jvf25>=3 && jet1IsB_MV1_70p_jvf25 && nBJet25_MV1_70p_jvf25>0 && met/(meffInc25_jvf25)>0.36 );
    LeptonSelection                  += " && dRminLepJet25_jvf25>1.0 "
    HardLeptonSelection              += " && dRminLepJet25_jvf25>1.0 "
    JetSelection               = "&& (jet1Pt_jvf25>180 && nJet40_jvf25>=3)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    BJetSelection              = "&& jet1IsB_MV1_70p_jvf25==0 && nBJet40_MV1_70p_jvf25>0" # (jetMV1[1]>0.772 || jetMV1[2]>0.772))" # nBJet40>0
    BVetoSelection             = "&& (nBJet40_MV1_70p_jvf25==0)"
    #BJetSelection = "&& (jetMV1[0]<0.772 && nBJet40>0)" # (jetMV1[1]>0.772 || jetMV1[2]>0.772))" # nBJet40>0
    configMgr.cutsDict["SR1L1Ba"] = "(met>250 && met/(meffInc40_jvf25)>0.32 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["CRWbb"] = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>250 && met/(meffInc40_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>100)"            + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<250 && met/(meffInc40_jvf25)>0.32 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    #
    configMgr.cutsDict["SR1L1BaMET"] = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["SR1L1BaMETMEFF"] = "(met>250 && met/(meffInc40_jvf25)>0. && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    #
    configMgr.cutsDict["CRWbbMT"] = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>40 && mt<100)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRWbbMET"] = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRWbbMETMEFF"] = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRWbbLEPPT"] = "(met>150 && met/(meffInc40_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + RelaxedLeptonSelection
    #
    configMgr.cutsDict["VR1MET"]  = "(met>100 && met/(meffInc40_jvf25)>0.32 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR1MT"]  = "(met>150 && met<250 && met/(meffInc40_jvf25)>0.32 && mt>80)" + JetSelection + BJetSelection  + LeptonSelection
    #
    configMgr.cutsDict["VR2MET"]  = "(met>100 && met/(meffInc40_jvf25)>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR2MT"]  = "(met>100 && met/(meffInc40_jvf25)>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + RelaxedLeptonSelection
    configMgr.cutsDict["VR2LEPPT"]  = "(met>100 && met/(meffInc40_jvf25)>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + RelaxedLeptonSelection
    pass
elif chn==5:
    LeptonSelection                  += " && dRminLepJet25_jvf25>1.0 "
    HardLeptonSelection              += " && dRminLepJet25_jvf25>1.0 "
    JetSelection               = "&& (jet1Pt_jvf25>180 && nJet25_jvf25>=3)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)"
    BJetSelection              = "&& jet1IsB_MV1_70p_jvf25==0 && nBJet25_MV1_70p_jvf25>0" # "&& (jetMV1[0]>0.772 || jetMV1[1]>0.772 || jetMV1[2]>0.772)" # nBJet40>0
    BVetoSelection             = "&& (nBJet25_MV1_70p_jvf25==0)"
    configMgr.cutsDict["SR1L1Bc"] = "(met>300 && met/(meffInc25_jvf25)>0.32 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["CRWbb"] = "(met>150 && met/(meffInc25_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>300 && met/(meffInc25_jvf25)>0.32 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met/(meffInc25_jvf25)>0.32 && mt>100)"            + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<300 && met/(meffInc25_jvf25)>0.32 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met/(meffInc25_jvf25)>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    #
    configMgr.cutsDict["SR1L1BcMET"] = "(met>200 && met/(meffInc25_jvf25)>0.32 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["SR1L1BcMETMEFF"] = "(met>300 && met/(meffInc25_jvf25)>0. && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    pass
elif chn==6: # soft 2-lepton analysis
    DiLeptonSelection          = TriggerSelection + "&& (lep1Pt>6 && lep2Pt>6) && (lep1Flavor==-1) && (lep2Flavor==-1) && (lep1Charge*lep2Charge<0)" ## os dimuon
    DiLeptonSelection          += " && dRminLepJet25_jvf25>1.0 "
    JetSelection               = "&& (jet1Pt_jvf25>100 && jet2Pt_jvf25>25)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)"
    # these B-jet selections are okay
    BVetoSelection             = "&& nB3Jet25_MV1_80p_jvf25==0 " ## !(jetMV1[0]>0.595 || jetMV1[1]>0.595 || (jet3Pt>40 && jetJVF[2]>0.25 && jetMV1[2]>0.595))" # 75%
    BJetSelection              = "&& nB3Jet25_MV1_80p_jvf25>0  " ## (jetMV1[0]>0.595 || jetMV1[1]>0.595 || (jet3Pt>40 && jetJVF[2]>0.25 && jetMV1[2]>0.595))"
    configMgr.cutsDict["SR2L"] = "(met>200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6 && lep1Pt<25 && met/(meffInc25_jvf25)>0.3)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep2Pt>25)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep2Pt>25)" + JetSelection + DiLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>25  && lep2Pt<25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6   && lep1Pt<25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict["VR3"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>25  && lep2Pt<25)" + JetSelection  + BVetoSelection + DiLeptonSelection
    #
    configMgr.cutsDict["SR2LMET"] = "(met>150 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6 && lep1Pt<25 && met/(meffInc25_jvf25)>0.3)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["SR2LMETMEFF"] = "(met>200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6 && lep1Pt<25 && met/(meffInc25_jvf25)>0.)" + JetSelection + BVetoSelection + DiLeptonSelection
    pass

#############
## Samples ##
#############

errHigh = 1.3
errLow  = 0.7

## not yet there ...
## to add AlpgenDY and ttbarV
AlpgenDYSample = Sample("AlpgenDY",kOrange-2)
AlpgenDYSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
AlpgenDYSample.setStatConfig(useStat)
AlpgenDYSample.setFileList(AlpgenDYFiles)
AlpgenDYSample.setNormByTheory()


DibosonsSample = Sample("SherpaDibosons",kOrange-8)
DibosonsSample.addSystematic(Systematic("errDB", configMgr.weights, 1.5, 0.5, "user","userOverallSys"))
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setFileList(SherpaDibosonsFiles)
DibosonsSample.setNormByTheory()


ttbarVSample = Sample("ttbarV",kGreen-8)
ttbarVSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setFileList(ttbarVFiles)
ttbarVSample.setNormByTheory()


SingleTopSample = Sample("SingleTop",kGreen-5)
SingleTopSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setFileList(SingleTopFiles)
SingleTopSample.setNormByTheory()
if chn==6:
    SingleTopSample.addSampleSpecificWeight("(DatasetNumber==108346)")


TTbarSampleName = 'PowhegPythiaTTbar'
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
TTbarSample.setFileList(PowhegPythiaTTbarFiles)
TTbarSample.addSystematic(pdfIntraSyst)
TTbarSample.addSystematic(pdfInterSyst)
TTbarSample.mergeOverallSysSet = ['pdfIntra','pdfInter'] ## post-processing
TTbarSample.addSystematic(pythwig) ## MB: Turned up for now ...
TTbarSample.addSystematic(pythgen) ## MB: Turned up for now ...

if (chn>=0 and chn<=7) and chn!=2 and chn!=6:
    TTbarSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
elif chn==2:
    TTbarSample.setNormRegions([("CRT","cuts")])
elif chn==6:
    TTbarSample.setNormRegions([("CRT","cuts")])
if chn==6:
    TTbarSample.addSampleSpecificWeight("(DecayIndexTTbar==1)")
    pass

doSherpa = True
if doSherpa and (chn>=0 and chn<=7): # stop
    WSampleName = "SherpaWMassiveB"
else:
    WSampleName = "AlpgenW"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_WZ",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.addSystematic(pdfIntraSyst)
WSample.addSystematic(pdfInterSyst)
WSample.mergeOverallSysSet = ['pdfIntra','pdfInter'] ## post-processing
if (chn>=0 and chn<=7) and chn!=2 and chn!=6:
    WSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
elif chn==2:
    WSample.setNormRegions([("CRW","cuts")])
if doSherpa and (chn>=0 and chn<=7): # stop
    WSample.setFileList(SherpaWMassiveBFiles) 
    WSample.addSystematic(sherpgen)  ## turned off for now
else:
    WSample.setFileList(AlpgenWFiles) 


if doSherpa and (chn>=0 and chn<=7): # stop
    ZSampleName = "SherpaZMassiveB"
else:
    ZSampleName = "AlpgenZ"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setNormFactor("mu_WZ",1.,0.,5.)
ZSample.setStatConfig(useStat)
ZSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
if (chn>=0 and chn<=7) and chn!=2 and chn!=6:
    ZSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
elif chn==2:
    ZSample.setNormRegions([("CRW","cuts")])
if doSherpa and (chn>=0 and chn<=7): # stop
    ZSample.setFileList(SherpaZMassiveBFiles)
else:
    ZSample.setFileList(AlpgenZFiles) ###(SherpaZMassiveBFiles)


#QCDSample = Sample("QCD",kYellow+1)
QCDSample = Sample("QCD",kYellow)
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
QCDSample.setFileList(qcdFiles)
#QCDSample.addSampleSpecificWeight("abs(qcdWeight)<10")

DataSample = Sample("Data",kBlack)
DataSample.setData()
DataSample.setFileList(dataFiles)


#######################
## Systematics (1/2) ##
#######################

#bkgMCSamples = [DibosonsSample,SingleTopSample,TTbarSample,ZSample,ttbarVSample,AlpgenDYSample]
#bkgMCSamples = [ZSample,WSample,TTbarSample,QCDSample,SingleTopSample,ttbarVSample,AlpgenDYSample,DibosonsSample]
bkgMCSamples = [DibosonsSample,AlpgenDYSample,ttbarVSample,SingleTopSample,QCDSample,TTbarSample,WSample,ZSample]
#if chn!=6: bkgMCSamples += [WSample] # absorbed in QCD
if chn==6: bkgMCSamples.remove(WSample) # absorbed in QCD

if includeSyst:
    SetupSamples( bkgMCSamples, SystList )
    #SetupSamples( [TTbarSample], [qfacT,ktfacT] )
    SetupSamples( [WSample], [qfacW,ktfacW] )

## more systematics below

##################
# The fit setup  #
##################

# First define HistFactory attributes
configMgr.analysisName   = "SoftLeptonMoriond2013_"+anaName+"_"+grid+gridspec+allpoints[0] # Name to give the analysis
if useBenchmarkInPlots:
    configMgr.analysisName = configMgr.analysisName + "_withSignal"
configMgr.outputFileName = "results/"+configMgr.analysisName+".root"
configMgr.histCacheFile  = "data/"+configMgr.analysisName+".root"

for point in allpoints:
    if point=="": continue

    # Fit config instance
    name="Fit_"+anaName+"_"+grid+gridspec+point
    myFitConfig = configMgr.addFitConfig(name)
    if useStat:
        myFitConfig.statErrThreshold=0.05
    else:
        myFitConfig.statErrThreshold=None

    #Add Measurement
    meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.036)
    meas.addParamSetting("alpha_pdfInter",True,0)
    
    if myFitType==FitType.Background: 
        meas.addPOI("mu_SIG")
        meas.addParamSetting("Lumi",True,1)
        #meas.addParamSetting("gamma_stat_CRW_cuts_bin_0",True,1)

    #meas.addParamSetting("mu_Diboson",True,1) # fix diboson to MC prediction

    if chn==2:
        #meas.addParamSetting("alpha_QCDNorm_CRW",True,0)
        pass
    #meas.addParamSetting("alpha_QCDNorm_WR",True,0)
    #meas.addParamSetting("alpha_QCDNorm_TR",True,0)
    #if chn==1:
    #    meas.addParamSetting("alpha_QCDNorm_WR5",True,0)
    #    meas.addParamSetting("alpha_QCDNorm_TR5",True,0)
    #meas.addParamSetting("alpha_QCDNorm_SS",True,0)
    if chn==6:
        meas.addParamSetting("mu_WZ",True,1) # fix diboson to MC prediction

    # x-section uncertainties stop and ued
    if chn>=2 and chn<=5:
        xsecSig = Systematic("SigXSec", configMgr.weights, 1.16, 0.84, "user", "userOverallSys")
        pass
    if chn==6:
        xsecSig = Systematic("SigXSec", configMgr.weights, 1.25, 0.75, "user", "userOverallSys")
        pass
    
    # ISR uncertainty (SS and GG grids)
    if myFitType==FitType.Exclusion:
      if (chn>=0 and chn<=5) or chn==7:
        massSet = point.split('_')
        if len(massSet)!=3: 
            log.fatal("Invalid grid point: %s" % point)
        DeltaM = float(massSet[0]) - float(massSet[2])
        if DeltaM<=0: 
            log.fatal("Invalid value of DeltaM : %f" % DeltaM)
        #
        eisr3 = 0.00
        eisr5 = 0.00
        if grid=="SM_GG1step": 
            eisr3 = exp(-1.4-0.013*DeltaM)
            eisr5 = exp(-1.2-0.005*DeltaM)
            if eisr3<0.06: eisr3=0.06
            if eisr5<0.06: eisr5=0.06
            pass
        elif ((grid=="SM_SS1step") or (grid=="StopBCharDeg")): 
            eisr3 = 0.06+exp(0.8-0.1*DeltaM)
            eisr5 = 0.06+exp(-1.5-0.005*DeltaM)
            pass
        isr3j = Systematic("isr", configMgr.weights, 1.00+eisr3, 1.00-eisr3, "user", "userOverallSys")
        isr5j = Systematic("isr", configMgr.weights, 1.00+eisr5, 1.00-eisr5, "user", "userOverallSys")
        pass
      elif chn==6:
        isrmm = Systematic("isr", configMgr.weights, 1.25, 0.75, "user", "userOverallSys")

    
    #-------------------------------------------------
    # First add the (grid point specific) signal sample
    #-------------------------------------------------
    sigSampleName=grid+"_"+point
    if myFitType==FitType.Exclusion:
        sigSample = Sample(sigSampleName,kRed)
        sigSample.setFileList(sigFiles)
        #sigSample.setTreeName(grid+"_"+point+suffix)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1,0.,100.)
        SetupSamples( [sigSample], SystList+[xsecSig] ) ## systematics
        #sigSample.addSystematic(xsecSig)      ## systematic not working?
        sigSample.setStatConfig(useStat)
        sigSample.mergeOverallSysSet = ['SigXSec','isr'] ## post-processing
        myFitConfig.addSamples(sigSample)
        myFitConfig.setSignalSample(sigSample)
        meas.addPOI("mu_SIG")

#    myFitConfig.addSamples(bkgMCSamples+[QCDSample,DataSample]) # DibosonsSample
    myFitConfig.addSamples(bkgMCSamples+[DataSample]) # DibosonsSample

    #MyConfigExample.py:nJetWS.hasBQCD = False
    #MyConfigExample.py:nJetTS.hasBQCD = True

    ## TR sofar defined for every channel
    TR = myFitConfig.addChannel("cuts",["CRT"],1,0.5,1.5)
    myFitConfig.setBkgConstrainChannels(TR)
    ###if chn!=6:
    TR.hasBQCD = True # b-tag applied
    BRset = [TR] 
    ## WR = second control region
    ##########if chn!=6: # di-lepton region only has control region
    if chn>=0 and chn<=5 or chn==7:
        WR = myFitConfig.addChannel("cuts",["CRW"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]
    if chn>=2 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            WRbb = myFitConfig.addChannel("cuts",["CRWbb"],1,0.5,1.5)
            WRbb.hasBQCD = True # b-tag applied
            BRset = [TR,WR,WRbb]
            myFitConfig.setValidationChannels(WRbb)

    ## FitType
    SRset = []
    if chn==0:
        SR3j = myFitConfig.addChannel("cuts",["SR1L3j"],1,0.5,1.5)
        SR3jMET = myFitConfig.addChannel("met",["SR1L3jMET"],4,300.,700.)
        SR3jMETMEFF = myFitConfig.addChannel("met/meffInc25_jvf25",["SR1L3jMETMEFF"],5,0.00001,1.) 
        SR3jMET.useOverflowBin=True
        SR3jMETMEFF.useOverflowBin=True
        SRset = [SR3j,SR3jMET,SR3jMETMEFF]
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==1:
        SR3j = myFitConfig.addChannel("cuts",["SR1L3j"],1,0.5,1.5)
        SR5j = myFitConfig.addChannel("cuts",["SR1L5j"],1,0.5,1.5)
        WR5j = myFitConfig.addChannel("cuts",["CRW5j"],1,0.5,1.5)
        TR5j = myFitConfig.addChannel("cuts",["CRT5j"],1,0.5,1.5)
        TR5j.hasBQCD = True
        BRset += [WR5j,TR5j]
        myFitConfig.setBkgConstrainChannels(WR5j)
        myFitConfig.setBkgConstrainChannels(TR5j)
        SRset = [SR3j,SR5j]
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass
    elif chn==7:
        SR5j = myFitConfig.addChannel("cuts",["SR1L5j"],1,0.5,1.5)
        SR5jMET = myFitConfig.addChannel("met",["SR1L5jMET"],4,200.,600.)
        SR5jMETMEFF = myFitConfig.addChannel("met/meffInc25_jvf25",["SR1L5jMETMEFF"],4,0.00001,1.) 
        SR5jMET.useOverflowBin=True
        SR5jMETMEFF.useOverflowBin=True
        SRset = [SR5j,SR5jMET,SR5jMETMEFF]
        if myFitType==FitType.Exclusion:
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass
    elif chn==2:
        SR1a = myFitConfig.addChannel("cuts",["SR1L2Ba"],1,0.5,1.5)
        SR1aMCT = myFitConfig.addChannel("mctcorr",["SR1L2BaMCT"],4,100.,300.)
        SR1aMCT.useOverflowBin=True
        SR1a.hasBQCD = True
        SR1aMCT.hasBQCD = True
        SRset = [SR1a,SR1aMCT]
        if myFitType==FitType.Exclusion:
            SR1a.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==3:
        SR1b = myFitConfig.addChannel("cuts",["SR1L2Bc"],1,0.5,1.5)
        SR1bMCT = myFitConfig.addChannel("mctcorr",["SR1L2BcMCT"],4,100.,500.)
        SR1bMCT.useOverflowBin=True
        SR1b.hasBQCD = True
        SR1bMCT.hasBQCD = True
        SRset = [SR1b,SR1bMCT]
        if myFitType==FitType.Exclusion:
            SR1b.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==4:
        SR2a = myFitConfig.addChannel("cuts",["SR1L1Ba"],1,0.5,1.5)
        SR2aMET = myFitConfig.addChannel("met",["SR1L1BaMET"],4,150.,550.)
        SR2aMETMEFF = myFitConfig.addChannel("met/meffInc25_jvf25",["SR1L1BaMETMEFF"],4,0.00001,1.)
        #
        CRWbbMT = myFitConfig.addChannel("mt",["CRWbbMT"],4,40.,100.)
        CRWbbMET = myFitConfig.addChannel("met",["CRWbbMET"],4,150.,250.)
        CRWbbMETMEFF = myFitConfig.addChannel("met/meffInc40_jvf25",["CRWbbMETMEFF"],4,0.0001,1.)
        CRWbbLEPPT = myFitConfig.addChannel("lep1Pt",["CRWbbLEPPT"],4,10,30.)
        #
        VR1MET = myFitConfig.addChannel("met",["VR1MET"],4,100.,300.)
        VR1MT = myFitConfig.addChannel("mt",["VR1MT"],4,80.,160.)
        #
        VR2MET = myFitConfig.addChannel("met",["VR2MET"],4,100.,300.)
        VR2MT = myFitConfig.addChannel("mt",["VR2MT"],4,80.,100.)
        VR2LEPPT = myFitConfig.addChannel("lep1Pt",["VR2LEPPT"],4,10.,30.)
        #
        SR2aMET.useOverflowBin=True
        SR2aMETMEFF.useOverflowBin=True
        CRWbbMT.userOverflowBin = True
        CRWbbMET.userOverflowBin = True
        CRWbbMETMEFF.userOverflowBin = True
        CRWbbLEPPT.userOverflowBin = True
        VR1MET.userOverflowBin = True
        VR1MT.userOverflowBin = True
        VR2MET.userOverflowBin = True
        VR2MT.userOverflowBin = True
        VR2LEPPT.userOverflowBin = True
        #
        SR2a.hasBQCD = True
        SR2aMET.hasBQCD = True
        SR2aMETMEFF.hasBQCD = True
        CRWbbMT.hasBQCD = True
        CRWbbMET.hasBQCD = True
        CRWbbMETMEFF.hasBQCD = True
        CRWbbLEPPT.hasBQCD = True
        #
        VR1MET.hasBQCD = True
        VR1MT.hasBQCD = True
        #
        VR2MET.hasBQCD = True
        VR2MT.hasBQCD = True
        VR2LEPPT.hasBQCD = True
        SRset = [SR2a,SR2aMET,SR2aMETMEFF,CRWbbMT,CRWbbMET,CRWbbMETMEFF,CRWbbLEPPT,VR1MET,VR1MT,VR2MET,VR2MT,VR2LEPPT]
        if myFitType==FitType.Exclusion:
            SR2a.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==5:
        SR2b = myFitConfig.addChannel("cuts",["SR1L1Bc"],1,0.5,1.5)
        SR2bMET = myFitConfig.addChannel("met",["SR1L1BcMET"],4,200.,600.)
        SR2bMETMEFF = myFitConfig.addChannel("met/meffInc25_jvf25",["SR1L1BcMETMEFF"],4,0.00001,1.) 
        SR2bMET.useOverflowBin=True
        SR2bMETMEFF.useOverflowBin=True
        SR2b.hasBQCD = True
        SR2bMET.hasBQCD = True
        SR2bMETMEFF.hasBQCD = True
        SRset = [SR2b,SR2bMET,SR2bMETMEFF]
        #SR2b.hasBQCD = True
        #SRset = [SR2b]
        if myFitType==FitType.Exclusion:
            SR2b.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==6:
        SR2l = myFitConfig.addChannel("cuts",["SR2L"],1,0.5,1.5)
        SR2lMET = myFitConfig.addChannel("met",["SR2LMET"],4,150.,350.)
        SR2lMETMEFF = myFitConfig.addChannel("met/meffInc25_jvf25",["SR2LMETMEFF"],4,0.00001,1.) 
        SR2lMET.useOverflowBin=True
        SR2lMETMEFF.useOverflowBin=True
        SR2l.hasBQCD = True
        SR2lMET.hasBQCD = True
        SR2lMETMEFF.hasBQCD = True
        SRset = [SR2l,SR2lMET,SR2lMETMEFF]
        #SR2l.hasBQCD = True
        #SRset = [SR2l]
        if myFitType==FitType.Exclusion:
            SR2l.getSample(sigSampleName).addSystematic(isrmm)
        pass
    
    ## AK: add signal sample, only for plot in SR
    if useBenchmarkInPlots:
        if chn==0:
            grid = "SM_GG1step"
            point = "625_545_465"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("met",["SR1L3jMET"]).addSample(sigSample)
            myFitConfig.getChannel("met/meffInc25_jvf25",["SR1L3jMETMEFF"]).addSample(sigSample)
            pass
        elif chn==1:
            pass
        elif chn==7:
            grid = "SM_GG1step"
            point = "625_545_465"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("met",["SR1L5jMET"]).addSample(sigSample)
            myFitConfig.getChannel("met/meffInc25_jvf25",["SR1L5jMETMEFF"]).addSample(sigSample)
            pass
        elif chn==2:
            grid = "StopBCharDeg"
            point = "300_120_100"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("mctcorr",["SR1L2BaMCT"]).addSample(sigSample)
            pass
        elif chn==3:
            grid = "StopBCharDeg"
            point = "450_170_150"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("mctcorr",["SR1L2BcMCT"]).addSample(sigSample)
            pass
        elif chn==4:
            grid = "StopBCharDeg"
            #            point = "400_200_200"
            point = "150_140_120"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("met",["SR1L1BaMET"]).addSample(sigSample)
            myFitConfig.getChannel("met/meffInc25_jvf25",["SR1L1BaMETMEFF"]).addSample(sigSample)
            myFitConfig.getChannel("mt",["CRWbbMT"]).addSample(sigSample)
            myFitConfig.getChannel("met",["CRWbbMET"]).addSample(sigSample)
            myFitConfig.getChannel("met/meffInc40_jvf25",["CRWbbMETMEFF"]).addSample(sigSample)
            myFitConfig.getChannel("lep1Pt",["CRWbbLEPPT"]).addSample(sigSample)
            myFitConfig.getChannel("met",["VR1MET"]).addSample(sigSample)
            myFitConfig.getChannel("mt",["VR1MT"]).addSample(sigSample)
            myFitConfig.getChannel("met",["VR2MET"]).addSample(sigSample)
            myFitConfig.getChannel("mt",["VR2MT"]).addSample(sigSample)
            myFitConfig.getChannel("lep1Pt",["VR2LEPPT"]).addSample(sigSample)
            #
            pass
        elif chn==5:
            grid = "StopBCharDeg"
            #            point = "500_270_250"
            point = "200_190_170"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("met",["SR1L1BcMET"]).addSample(sigSample)
            myFitConfig.getChannel("met/meffInc25_jvf25",["SR1L1BcMETMEFF"]).addSample(sigSample)
            pass
        elif chn==6:
            grid = "mUED2Lfilter"
            point = "900_40"
            sigSampleName=grid+"_"+point
            sigSample = Sample(sigSampleName,kMagenta)
            sigFiles = [inputDirSig_p1328+"sigtree_Soft2Lep_"+grid+".root"]
            sigSample.setFileList(sigFiles)
            myFitConfig.getChannel("met",["SR2LMET"]).addSample(sigSample)
            myFitConfig.getChannel("met/meffInc25_jvf25",["SR2LMETMEFF"]).addSample(sigSample)
            pass


    ## systematics (2/2)
    if includeSyst and dobtag:
        if chn==0 or chn==7:
            SetupChannels([WR,TR], [bTagSyst9])
            pass
        if chn==1:
            SetupChannels([WR,TR], [bTagSyst9])
            SetupChannels([WR5j,TR5j], [bTagSyst9])
            pass
        elif chn==2:
            SetupChannels(BRset+SRset, [bTagSyst8]) # VR
            pass
        elif chn==3:
            SetupChannels(BRset+SRset, [bTagSyst8]) # VR
            pass
        elif chn==4:
            SetupChannels(BRset+SRset, [bTagSyst9]) # VR
            pass
        elif chn==5:
            SetupChannels(BRset+SRset, [bTagSyst9]) # VR
            pass
        elif chn==6:
            SetupChannels(BRset+SRset, [bTagSyst11]) # VR1,VR2
            pass

    if chn>=2 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            WRbb.getSample(WSampleName).addSystematic(wbb)

    if chn>=2 and chn<=5:
        TR.getSample(WSampleName).addSystematic(wbb)
        for sr in SRset: 
             sr.getSample(WSampleName).addSystematic(wbb)

    ## W and top theory uncertainties in SRs 
    #for sr in SRset:
    #    if chn!=6:
    #        sr.getSample(WSampleName).addSystematic( Systematic("TheoW", configMgr.weights, 1.25, 0.75, "user", "userOverallSys") )
    #for sr in SRset:
    #    sr.getSample(TTbarSampleName).addSystematic( Systematic("TheoT", configMgr.weights, 1.25, 0.75, "user", "userOverallSys") )

    ## Fit type specifics
    if myFitType!=FitType.Background:
        myFitConfig.setSignalChannels(SRset)
    else:
        for sr in SRset:
            sr.doBlindingOverwrite = doBlinding
        myFitConfig.setValidationChannels(SRset)
    if myFitType==FitType.Discovery:
        #meas.addParamSetting("Lumi",True,1)
        for SR in SRset:
            SR.addDiscoverySamples([SR.name],[1.],[0.],[100.],[kMagenta])
            meas.addPOI("mu_%s" % SR.name)

    vSet = []
    if doValidation:
        if chn==0:
            #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
            vSet = [v1,v2] + [v4,v5] + [v7,v8]
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
##         if chn==0:
## ##             #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
## ##             v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
## ##             v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
##             #v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
##             v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
##             v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
## ##             #v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
## ##             v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
## ##             v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
## ##             vSet = [v3,v6] + [v4,v7] + [v5,v8]
##             # AK +
##             #configMgr.cutsDict["SR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
##             v9 = myFitConfig.addChannel("met_fix",["SR1L3j"],3,400.,1000.)
##             v9.useOverflowBin=True
##             #            vSet = [v3,v6] + [v4,v7] + [v5,v8]
##             vSet = [v4,v5] + [v9]
##             # AK -
##             #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
##             myFitConfig.setValidationChannels(vSet)
##             pass
        if chn==7:
            #v9  = myFitConfig.addChannel("cuts",["VR5j1"],1,0.5,1.5)
            v10 = myFitConfig.addChannel("cuts",["VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True
            v11 = myFitConfig.addChannel("cuts",["VRW5j1"],1,0.5,1.5)
            #v12 = myFitConfig.addChannel("cuts",["VR5j2"],1,0.5,1.5)
            v13 = myFitConfig.addChannel("cuts",["VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            v14 = myFitConfig.addChannel("cuts",["VRW5j2"],1,0.5,1.5)
            #v15 = myFitConfig.addChannel("cuts",["VR5j3"],1,0.5,1.5)
            v16 = myFitConfig.addChannel("cuts",["VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            v17 = myFitConfig.addChannel("cuts",["VRW5j3"],1,0.5,1.5)
            vSet = [v10,v13,v16] + [v11,v14,v17] #+ [v9,v12,v15]
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==1:
            #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
            #v9  = myFitConfig.addChannel("cuts",["VR5j1"],1,0.5,1.5)
            v10 = myFitConfig.addChannel("cuts",["VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True 
            v11 = myFitConfig.addChannel("cuts",["VRW5j1"],1,0.5,1.5)
            #v12 = myFitConfig.addChannel("cuts",["VR5j2"],1,0.5,1.5)
            v13 = myFitConfig.addChannel("cuts",["VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            v14 = myFitConfig.addChannel("cuts",["VRW5j2"],1,0.5,1.5)
            #v15 = myFitConfig.addChannel("cuts",["VR5j3"],1,0.5,1.5)
            v16 = myFitConfig.addChannel("cuts",["VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            v17 = myFitConfig.addChannel("cuts",["VRW5j3"],1,0.5,1.5)
            vSet = [v1,v4,v7,v10,v13,v16] + [v2,v5,v8,v11,v14,v17] #+ [v0,v3,v6,v9,v12,v15]
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
 ##            if chn==1:
##             #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
##             v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
##             v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
## ##           #v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
## ##             v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
## ##             v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
## ##             #v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
## ##             v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
## ##             v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
## ##             #v9  = myFitConfig.addChannel("cuts",["VR5j1"],1,0.5,1.5)
## ##             v10 = myFitConfig.addChannel("cuts",["VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True 
## ##             v11 = myFitConfig.addChannel("cuts",["VRW5j1"],1,0.5,1.5)
## ##             #v12 = myFitConfig.addChannel("cuts",["VR5j2"],1,0.5,1.5)
## ##             v13 = myFitConfig.addChannel("cuts",["VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
## ##             v14 = myFitConfig.addChannel("cuts",["VRW5j2"],1,0.5,1.5)
## ##             #v15 = myFitConfig.addChannel("cuts",["VR5j3"],1,0.5,1.5)
## ##             v16 = myFitConfig.addChannel("cuts",["VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
## ##             v17 = myFitConfig.addChannel("cuts",["VRW5j3"],1,0.5,1.5)
##             #vSet = [v1,v4,v7,v10,v13,v16] + [v2,v5,v8,v11,v14,v17] #+ [v0,v3,v6,v9,v12,v15]
##             #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
##             # AK +
##             #configMgr.cutsDict["SR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
##             v999 = myFitConfig.addChannel("met_fix",["SR1L3j"],3,400.,1000.)
##             v999.useOverflowBin=True
##             #            vSet = [v3,v6] + [v4,v7] + [v5,v8]
##             vSet = [v1,v2] + [v999]
##             # AK -
##             myFitConfig.setValidationChannels(vSet)
##             pass
        if chn==2:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==3:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==4:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==5:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==6:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VR3"],1,0.5,1.5)
            v2.hasBQCD = True
            vSet = [v0,v1,v2]
            myFitConfig.setValidationChannels(vSet)
            pass

    if doValidation and includeSyst and dobtag:
        if chn==0 or chn==7:
            SetupChannels(vSet, [bTagSyst9])
            pass
        if chn==1:
            SetupChannels(vSet, [bTagSyst9])
            pass
        elif chn==2:
            SetupChannels(vSet, [bTagSyst8]) # VR
            pass
        elif chn==3:
            SetupChannels(vSet, [bTagSyst8]) # VR
            pass
        elif chn==4:
            SetupChannels(vSet, [bTagSyst9]) # VR
            pass
        elif chn==5:
            SetupChannels(vSet, [bTagSyst9]) # VR
            pass
        elif chn==6:
            SetupChannels(vSet, [bTagSyst11]) # VR1,VR2
            pass

    # Generator Systematics for each sample,channel
    log.info("** Generator Systematics **")
    AllChannels = BRset+SRset+vSet
    for tgt,syst in generatorSyst:
        tgtsample = tgt[0]
        tgtchan = tgt[1]
        for chan in AllChannels:
           # print " tgtsample = ", tgtsample
           # print " chan.name = ", chan.name
           # print " tgtchan = ", tgtchan
            #if 'cuts_'+tgtchan == chan.name:
            if tgtchan=="All" or (tgtchan in chan.name):
                chan.getSample(tgtsample).addSystematic(syst)
                log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
            
            
            
    # #######################
    # Cosmetic Settings ##
    # #######################
    # Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
    c = ROOT.TCanvas()
    compFillStyle = 1001 # see ROOT for Fill styles
    leg = ROOT.TLegend(0.575,0.5,0.875,0.9,"")
    leg.SetFillStyle(0)
    leg.SetFillColor(0)
    leg.SetBorderSize(0)
    #
    entry = ROOT.TLegendEntry()
    #    entry = leg.AddEntry("","Data 2012 (#sqrt{s}=8 TeV)","lp")
    entry = leg.AddEntry("","Data (2012)","lp")
    entry.SetMarkerColor(myFitConfig.dataColor)
    entry.SetMarkerStyle(20)
    #   #
    #     entry = leg.AddEntry("","Total pdf","lf")
    #     entry.SetLineColor(bkgOnly.totalPdfColor)
    #     entry.SetLineWidth(2)
    #     entry.SetFillColor(bkgOnly.errorFillColor)
    #     entry.SetFillStyle(bkgOnly.errorFillStyle)
    #     #
    entry = leg.AddEntry("","Z+jets","lf")
    entry.SetLineColor(ZSample.color)
    entry.SetFillColor(ZSample.color)
    entry.SetFillStyle(compFillStyle)
    #     #
    entry = leg.AddEntry("","W+jets","lf")
    entry.SetLineColor(WSample.color)
    entry.SetFillColor(WSample.color)
    entry.SetFillStyle(compFillStyle)
    #
    entry = leg.AddEntry("","t#bar{t}","lf")
    entry.SetLineColor(TTbarSample.color)
    entry.SetFillColor(TTbarSample.color)
    entry.SetFillStyle(compFillStyle)
    #
    #    entry = leg.AddEntry("","QCD multijet","lf")
    entry = leg.AddEntry("","Fake leptons","lf")
    entry.SetLineColor(QCDSample.color)
    entry.SetFillColor(QCDSample.color)
    entry.SetFillStyle(compFillStyle)
    #
    entry = leg.AddEntry("","Single Top","lf")
    entry.SetLineColor(SingleTopSample.color)
    entry.SetFillColor(SingleTopSample.color)
    entry.SetFillStyle(compFillStyle)
    #
    entry = leg.AddEntry("","t#bar{t}+V","lf")
    entry.SetLineColor(ttbarVSample.color)
    entry.SetFillColor(ttbarVSample.color)
    entry.SetFillStyle(compFillStyle)
    #
    entry = leg.AddEntry("","DY","lf")
    entry.SetLineColor(AlpgenDYSample.color)
    entry.SetFillColor(AlpgenDYSample.color)
    entry.SetFillStyle(compFillStyle)
    #
    entry = leg.AddEntry("","Dibosons","lf")
    entry.SetLineColor(DibosonsSample.color)
    entry.SetFillColor(DibosonsSample.color)
    entry.SetFillStyle(compFillStyle)

    if myFitType==FitType.Exclusion: 
        entry = leg.AddEntry("","Signal","lf")
        entry.SetLineColor(sigSample.color)
        entry.SetFillColor(sigSample.color)
        entry.SetFillStyle(compFillStyle)

    if useBenchmarkInPlots:
        if chn==0:
            grid = "GG1step"
            point = "625_545_465"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
        elif chn==7:
            grid = "GG1step"
            point = "625_545_465"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
            pass
        elif chn==2:
            grid = "StopBChar"
            point = "300_120_100"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
            pass
        elif chn==3:
            grid = "StopBChar"
            point = "450_170_150"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
            pass
        elif chn==4:
            grid = "StopBChar"
            #            point = "400_220_200"
            point = "150_140_120"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
            pass
        elif chn==5:
            grid = "StopBChar"
            point = "200_190_170"
            #            point = "500_270_250"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
            pass
        elif chn==6:
            grid = "mUED"
            point = "900_40"
            sigSampleName=grid+"_"+point
            entry = leg.AddEntry("",sigSampleName,"l")
            entry.SetLineColor(kBlack)
            entry.SetLineStyle(kDashed)
            pass
        
    # Set legend for TopLevelXML
    myFitConfig.tLegend = leg

    # Plot "ATLAS" label
    for chan in AllChannels:
        chan.logY = False
        # if not myFitType==FitType.Exclusion:
        #     chan.logY = True
        if chan.logY:
            chan.minY = 0.5
            chan.maxY = 1000000
        else:
            chan.minY = 0. 
            chan.maxY = 60
        chan.ATLASLabelX = 0.25
        chan.ATLASLabelY = 0.85
        chan.ATLASLabelText = "Internal"
        chan.showLumi = True
        # axis titles
        if "METMEFF" in chan.name:
            chan.titleY = "Entries"
            chan.titleX = "E_{T}^{miss} / m_{eff}"
        elif "MET" in chan.name:
            chan.titleY = "Entries /  100 GeV"
            chan.titleX = "E_{T}^{miss} [GeV]"
        elif "MCT" in chan.name:
            chan.titleY = "Entries /  100 GeV"
            chan.titleX = "m_{CT} [GeV]"
        elif "MT" in chan.name:
            chan.titleY = "Entries /  20 GeV"
            chan.titleX = "m_{T} [GeV]"
        elif "LEPPT" in chan.name:
            chan.titleY = "Entries /  5 GeV"
            chan.titleX = "lepton p_{T} [GeV]"

        if "SR2LMETMEFF" in chan.name:
            chan.minY = 0. 
            chan.maxY = 15 #25
        elif "SR2LMET" in chan.name:
            chan.minY = 0. 
            chan.maxY = 30 #60
            chan.titleY = "Entries /  50 GeV"
            chan.titleX = "E_{T}^{miss} [GeV]"
        elif "SR1L3jMETMEFF" in chan.name:
            chan.minY = 0. 
            chan.maxY = 15 #25
        elif "SR1L3jMET" in chan.name:
            chan.minY = 0. 
            chan.maxY = 50 #80
        elif "SR1L5jMETMEFF" in chan.name:
            chan.minY = 0. 
            chan.maxY = 20  #40
        elif "SR1L5jMET" in chan.name:
            chan.minY = 0. 
            chan.maxY = 30 #60
        elif "SR1L2BaMCT" in chan.name:
            chan.minY = 0. 
            chan.maxY = 90  #60
        elif "SR1L2BcMCT" in chan.name:
            chan.minY = 0. 
            chan.maxY = 10 #20
        elif "SR1L1BcMETMEFF" in chan.name:
            chan.minY = 0. 
            chan.maxY = 30 #40
        elif "SR1L1BcMET" in chan.name:
            chan.minY = 0. 
            chan.maxY = 60 #80
        elif "SR1L1BaMETMEFF" in chan.name:
            chan.minY = 0. 
            chan.maxY = 40 #80
        elif "SR1L1BaMET" in chan.name:
            chan.minY = 0. 
            chan.maxY = 30 #60
        elif "CRWbbMT" in chan.name:
            chan.minY = 0. 
            chan.maxY = 400 #60\
        elif "CRWbb" in chan.name:
            chan.minY = 0. 
            chan.maxY = 250 #60
        elif "VR1MT" in chan.name:
            chan.minY = 0. 
            chan.maxY = 100 #60
        elif "VR2MT" in chan.name:
            chan.minY = 0. 
            chan.maxY = 200 #60
        elif "VR2" in chan.name:
            chan.minY = 0. 
            chan.maxY = 100 #60
