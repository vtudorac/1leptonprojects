################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
import ROOT
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import sqrt
import pickle
from os import sys
from math import exp

from logger import Logger
log = Logger('SoftLepton')

# ********************************************************************* #
# some useful helper functions
# ********************************************************************* #

def myreplace(l1,l2,element):
    idx=l1.index(element)
    if idx>=0:
        return l1[:idx] + l2 + l1[idx+1:]
    else:
        print "WARNING idx negative: %d" % idx
        return l1

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupSamples(samples,systList):
    for sample in samples:
        for syst in systList:
            sample.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #

#print sys.argv

# ********************************************************************* #
#                              Analysis parameters
# ********************************************************************* #

if not 'useStat' in dir():
    useStat=True
if not 'chn' in dir():
    chn=1                   # analysis channel 0=A,1=B,..
if not 'grid' in dir():
    grid="" # "SU"               # only grid implemented up to now
if not 'gridspec' in dir():
    gridspec="_" # "SU"               # only grid implemented up to now
if not 'suffix' in dir():
    suffix="_NoSys"
if not 'sigSamples' in dir():
    sigSamples=None #["1000_250"]
if not 'anaName' in dir():
    anaName = 'SRs1L'
if not 'includeSyst' in dir():
    includeSyst = True
if not 'dobtag' in dir():
    dobtag = True
if not 'doBlinding' in dir():
    doBlinding = False


configMgr.fixSigXSec=True

# ********************************************************************* #
#                              Options
# ********************************************************************* #

# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    if pickedSRs[0]=="SR3j":
        chn=0
    elif pickedSRs[0]=="SR5j":
        chn=7
    elif pickedSRs[0]=="SRs1L":
        chn=1
    elif pickedSRs[0]=="SR1a":
        chn=2
    elif pickedSRs[0]=="SR1b":
        chn=3
    elif pickedSRs[0]=="SR2a":
        chn=4
    elif pickedSRs[0]=="SR2b":
        chn=5
    elif pickedSRs[0]=="SRs2L":
        chn=6
    else:
        print "WARNING: analysis not defined"
        sys.exit()
    anaName=pickedSRs[0]
    # set the grid
    if len(pickedSRs) >= 2: 
        grid = pickedSRs[1] 
        pass
    if len(pickedSRs) >= 3: 
        gridspec = '_'+pickedSRs[2]+'_'
        pass

if chn>7 or chn<0:
    print "ERROR analysis not defined!!!"
    print chn
    print pickedSRs
    sys.exit()

allpoints=['500_400_60']

if chn==0 or chn==1 or chn==7:
    # traditional soft 1-lepton analysis
    if grid=="SM_GG1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
    elif grid=="SM_SS1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_1190_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
elif chn>=2 and chn<=5:
    # stop soft 1-lepton analysis
    if grid=="StopBCharDeg" and gridspec=="_5gev_":
        allpoints=['150_105_100', '150_140_135', '200_105_100', '200_155_150', '200_190_185', '250_105_100', '250_155_150', '250_205_200', '250_240_235', '300_105_100', '300_155_150', '300_205_200', '300_255_250', '300_290_285', '350_155_150', '350_205_200', '350_255_250', '350_305_300', '350_340_335', '400_105_100', '400_205_200', '400_255_250', '400_305_300', '400_355_350', '400_390_385', '450_155_150', '450_255_250', '450_305_300', '450_355_350', '450_405_400', '450_440_435', '500_105_100', '500_205_200', '500_255_250', '500_305_300', '500_355_350', '500_405_400', '500_455_450', '500_490_485', '550_105_100', '550_155_150', '550_205_200', '550_255_250', '550_305_300', '550_355_350', '550_405_400', '550_455_450', '600_105_100', '600_155_150', '600_205_200', '600_255_250', '600_305_300', '600_355_350', '600_405_400', '600_455_450', '650_105_100', '650_155_150', '650_205_200', '650_255_250', '650_305_300', '650_355_350', '650_405_400', '650_455_450', '700_105_100', '700_155_150', '700_205_200', '700_255_250', '700_305_300', '700_355_350', '700_405_400', '700_455_450', '750_105_100', '750_155_150', '750_205_200', '750_255_250', '750_305_300', '750_355_350', '750_405_400', '750_455_450', '800_105_100', '800_155_150', '800_205_200', '800_255_250', '800_305_300', '800_355_350', '800_405_400', '800_455_450']
    elif grid=="StopBCharDeg" and gridspec=="_20gev_":
        allpoints=['150_140_120', '150_95_75', '200_120_100', '200_170_150', '200_190_170', '200_95_75', '250_120_100', '250_170_150', '250_220_200', '250_240_220', '250_95_75', '300_120_100', '300_170_150', '300_220_200', '300_270_250', '300_290_270', '350_170_150', '350_220_200', '350_270_250', '350_320_300', '350_340_320', '350_95_75', '400_120_100', '400_220_200', '400_270_250', '400_320_300', '400_370_350', '400_390_370', '450_170_150', '450_220_200', '450_270_250', '450_320_300', '450_370_350', '450_420_400', '450_440_420', '450_95_75', '500_120_100', '500_220_200', '500_270_250', '500_320_300', '500_370_350', '500_420_400', '500_470_450', '500_490_470', '550_120_100', '550_170_150', '550_220_200', '550_270_250', '550_320_300', '550_370_350', '550_420_400', '550_470_450', '550_95_75', '600_120_100', '600_170_150', '600_220_200', '600_270_250', '600_320_300', '600_370_350', '600_420_400', '600_470_450', '600_95_75', '650_120_100', '650_170_150', '650_220_200', '650_270_250', '650_320_300', '650_370_350', '650_420_400', '650_470_450', '650_95_75', '700_120_100', '700_170_150', '700_220_200', '700_270_250', '700_320_300', '700_370_350', '700_420_400', '700_470_450', '700_95_75', '750_120_100', '750_170_150', '750_220_200', '750_270_250', '750_320_300', '750_370_350', '750_420_400', '750_470_450', '750_95_75', '800_120_100', '800_170_150', '800_220_200', '800_270_250', '800_320_300', '800_370_350', '800_420_400', '800_470_450', '800_95_75']
    pass
elif chn==6:
    # ued soft 2-lepton analysis
    if grid=="mUED2Lfilter":
        allpoints=['1000_10', '1000_3', '1000_40', '1100_10', '1100_3', '1100_40', '1200_10', '1200_3', '1200_40', '1300_10', '1300_3', '1300_40', '700_10', '700_3', '700_40', '800_10', '800_3', '800_40', '900_10', '900_3', '900_40']
    pass

# No input signal for discovery and bkg fit
if myFitType==FitType.Discovery:
    allpoints=["Discovery"]
    grid=""
    pass
elif myFitType==FitType.Background:
    allpoints=["Background"]
    grid=""
    pass

# sigSamples is set by the "-g" HistFitter option. Overwrites allpoints, used below.
try:
    sigSamples
except NameError:
    sigSamples = None
    
if sigSamples!=None:
    allpoints=sigSamples

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

#configMgr.doHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

# ********************************************************************* #
# Main part - now we start to build the data model
# ********************************************************************* #

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.3   ####20.490 # 14.3 #21.0
configMgr.setLumiUnits("fb-1")

inputDirData_p1328 = "root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v8_2_1/" # /eos/atlas/user/t/tnobe/p1328/v7_1_1/" 
inputDirSlim_p1328 = "root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v8_2_1_slim/"  # /eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v7_1_1_slim/" 
inputDirSig_p1328  = "root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v8_2_1/" # /eos/atlas/user/t/tnobe/p1328/v7_1_1/" 
inputDirQCD_p1328  = "root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/p1328/v8_2_1/" # /eos/atlas/user/t/tnobe/p1328/v7_1_1/"

#Split bdgFiles per channel
sigFiles = []
if myFitType==FitType.Exclusion:
    if (chn>=0 and chn<=5) or chn==7: # soft 1-lepton
        sigFiles += [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
    elif chn==6:          # soft 2-lepton
        sigFiles += [inputDirSig_p1328+"sigtree_Soft2Lep_"+grid+".root"]
    if len(sigFiles)==0:
        print "WARNING: signal grid files not defined"
        sys.exit()
    pass

# Set the files to read from
if (chn>=0 and chn<=5) or chn==7: # soft 1-lepton
    if True:
        dataFiles              = [inputDirData_p1328+"Soft1Lep_Data.root"] #"Soft1Lep_Data.root"]
        qcdFiles               = [inputDirQCD_p1328 +"Soft1Lep_QCD.root"] #"Soft1Lep_QCD.root"]
        AlpgenDYFiles          = [inputDirSlim_p1328+"AlpgenDY_Soft1Lep.root"]
        AlpgenWFiles           = [inputDirSlim_p1328+"AlpgenJimmyW_Soft1Lep.root"] #"AlpgenW_Soft1Lep.root"]
        AlpgenZFiles           = [inputDirSlim_p1328+"AlpgenZ_Soft1Lep.root"]
        SherpaDibosonsFiles    = [inputDirSlim_p1328+"SherpaDibosons_Soft1Lep.root"]
        DibosonsFiles          = [inputDirSlim_p1328+"Dibosons_Soft1Lep.root"]
        PowhegPythiaTTbarFiles = [inputDirSlim_p1328+"PowhegPythiaTTbar_Soft1Lep.root"]
        PowhegJimmyTTbarFiles  = [inputDirSlim_p1328+"PowhegJimmyTTbar_Soft1Lep.root"]
        AlpgenTTbarFiles       = [inputDirSlim_p1328+"AlpgenTTbar_Soft1Lep.root"]
        SherpaWMassiveBCFiles   = [inputDirSlim_p1328+"SherpaWMassiveBC_Soft1Lep.root"]
        SherpaZMassiveBCFiles   = [inputDirSlim_p1328+"SherpaZMassiveBC_Soft1Lep.root"]
        SingleTopFiles         = [inputDirSlim_p1328+"SingleTop_Soft1Lep.root"]
        ttbarVFiles            = [inputDirSlim_p1328+"ttbarV_Soft1Lep.root"]
        PowhegDibosonsFiles    = [inputDirSlim_p1328+"PowhegDibosons_Soft1Lep.root"]
        pass
elif chn==6:          # soft 2-lepton
    dataFiles              = [inputDirData_p1328+"Soft2Lep_Data.root"]
    qcdFiles               = [inputDirQCD_p1328 +"Soft2Lep_QCD.root"] # "Soft2Lep_QCD.root"]
    AlpgenDYFiles          = [inputDirSlim_p1328+"AlpgenDY_Soft2Lep.root"]
    AlpgenWFiles           = [inputDirSlim_p1328+"AlpgenJimmyW_Soft2Lep.root"] #"AlpgenW_Soft2Lep.root"]
    AlpgenZFiles           = [inputDirSlim_p1328+"AlpgenZ_Soft2Lep.root"]
    SherpaDibosonsFiles    = [inputDirSlim_p1328+"SherpaDibosons_Soft2Lep.root"]
    #
    DibosonsFiles          = [inputDirSlim_p1328+"Dibosons_Soft2Lep.root"]
    PowhegPythiaTTbarFiles = [inputDirSlim_p1328+"PowhegPythiaTTbar_Soft2Lep.root"]
    PowhegJimmyTTbarFiles  = [inputDirSlim_p1328+"PowhegJimmyTTbar_Soft2Lep.root"]
    AlpgenTTbarFiles       = [inputDirSlim_p1328+"AlpgenTTbar_Soft2Lep.root"]
    SherpaWMassiveBCFiles   = [inputDirSlim_p1328+"SherpaWMassiveBC_Soft2Lep.root"]
    SherpaZMassiveBCFiles   = [inputDirSlim_p1328+"SherpaZMassiveBC_Soft2Lep.root"]
    SingleTopFiles         = [inputDirSlim_p1328+"SingleTop_Soft2Lep.root"]
    ttbarVFiles            = [inputDirSlim_p1328+"ttbarV_Soft2Lep.root"]
    PowhegDibosonsFiles    = [inputDirSlim_p1328+"PowhegDibosons_Soft2Lep.root"]
    pass


    
########################################
# Analysis description
########################################

## Lists of weights 
weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","SherpaWweight"]

toreplace = ""
if chn==0 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==7 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==1 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==2 and dobtag:
    #weights += ["bTagWeight[7]"]
    bweights = weights + ["bTagWeight[7]"]
    toreplace = "bTagWeight[7]"
    pass
elif chn==3 and dobtag:
    #weights += ["bTagWeight[7]"]
    bweights = weights + ["bTagWeight[7]"]
    toreplace = "bTagWeight[7]"
    pass
elif chn==4 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==5 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==6 and dobtag:
    #weights += ["bTagWeight[10]"]
    bweights = weights + ["bTagWeight[10]"]
    toreplace = "bTagWeight[10]"
    pass

#configMgr.weights = weights # bweights
configMgr.weights = bweights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights, "leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

if dobtag:
    bTagHighWeights9 = replaceWeight(bweights,toreplace,"bTagWeightBUp[8]")
    bTagLowWeights9  = replaceWeight(bweights,toreplace,"bTagWeightBDown[8]")
    bTagHighWeights8 = replaceWeight(bweights,toreplace,"bTagWeightBUp[7]")
    bTagLowWeights8  = replaceWeight(bweights,toreplace,"bTagWeightBDown[7]")
    bTagHighWeights11 = replaceWeight(bweights,toreplace,"bTagWeightBUp[10]")
    bTagLowWeights11  = replaceWeight(bweights,toreplace,"bTagWeightBDown[10]")

#-------------------------------------------------------------------------
# Blinding of SR
#-------------------------------------------------------------------------

configMgr.blindSR = False #doBlinding
configMgr.blindVR = False # doBlinding

#--------------------------------------------------------------------------
# List of systematics
#--------------------------------------------------------------------------

configMgr.nomName = "_NoSys"

## JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig",   "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallHistoSys")
jes       = Systematic("JES",    "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallNormHistoSys") # JES uncertainty - for low pt jets
jer       = Systematic("JER",    "_NoSys", "_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym")

jlow      = Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys") # JES uncertainty - for low pt jets
jmed      = Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys") # JES uncertainty - for medium pt jets
jhigh     = Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys") # JES uncertainty - for high pt jets

## MET uncertainty
scalest   = Systematic("SCALEST","_NoSys", "_SCALESTup","_SCALESTdown","tree","overallNormHistoSys")
resost    = Systematic( "RESOST","_NoSys", "_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym")

## pile-up
pileup = Systematic("pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys")

## b-tagging
if dobtag:
    bTagSyst9  = Systematic("BT",bweights,bTagHighWeights9,bTagLowWeights9,"weight","overallNormHistoSys")
    bTagSyst8  = Systematic("BT",bweights,bTagHighWeights8,bTagLowWeights8,"weight","overallNormHistoSys")
    bTagSyst11 = Systematic("BT",bweights,bTagHighWeights11,bTagLowWeights11,"weight","overallNormHistoSys")

## Trigger efficiency --> should be taken from data/mc trigger study
#trEl = Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#trMu = Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#####trEff= Systematic("TE",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallNormHistoSys")
trEff = Systematic("TE",configMgr.weights,1.05,0.95,"user","userOverallSys")

## MC theoretical uncertainties
## Signal XSec uncertainty as overallSys (pure yeild affect) 
xsecSig = Systematic("SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys" )

## generator level uncertainties
#SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")

# qfacUpWeightW:qfacDownWeightW:ktfacUpWeightW:ktfacDownWeightW

qfacW  = Systematic("qfacW", configMgr.weights,configMgr.weights+["qfacUpWeightW"], configMgr.weights+["qfacDownWeightW"], "weight","overallNormHistoSys")
ktfacW = Systematic("ktfacW",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","overallNormHistoSys")

wbb    = Systematic("wbb", configMgr.weights,1.24 ,0.76, "user","userOverallSys")

## qcd stat/syst weights automatically picked up.

## Lepton weight uncertainty
#elEff = Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys") 
#muEff = Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")
lepEff= Systematic("LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallHistoSys")

## Electron energy scale uncertainty
#ees = Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")
egzee = Systematic("egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys")
egmat = Systematic("egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallSys")
egps  = Systematic("egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys")
eglow = Systematic("eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys")
egres = Systematic("egres","_NoSys","_EGRESup","_EGRESdown","tree","overallSys")

## Muon energy resolutions
merm = Systematic("LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallSys")
meri = Systematic("LRImu","_NoSys","_MIDup","_MIDdown","tree","overallSys")

## muon energy scale systematics
mes = Systematic("mes","_NoSys","_MSCALEup","_MSCALEdown","tree","overallSys")

## sherpa / alpgen generator difference
sherpgen = Systematic("sherpgen", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
sherpgen.setFileList('SherpaWMassiveBC', AlpgenWFiles)
sherpgen.setTreeName('SherpaWMassiveBC','AlpgenJimmyW_NoSys')

## powheg pythia/herwig parton shower difference
pythwig = Systematic("pythwig", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
pythwig.setFileList('PowhegPythiaTTbar', PowhegJimmyTTbarFiles)
pythwig.setTreeName('PowhegPythiaTTbar','PowhegJimmyTTbar_NoSys')

## powheg alpgen ttbar generator difference
pythgen = Systematic("pythgen", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
pythgen.setFileList('PowhegPythiaTTbar', AlpgenTTbarFiles)
pythgen.setTreeName('PowhegPythiaTTbar','AlpgenTTbar_NoSys')

## sherpa powheg generator difference
sherheg = Systematic("sherheg", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
sherheg.setFileList("SherpaDibosons", PowhegDibosonsFiles )
sherheg.setTreeName("SherpaDibosons", 'PowhegDibosons_NoSys')

## pdf
pdfIntraSyst = Systematic("pdfIntra",configMgr.weights,configMgr.weights+["pdfWeightVars[0]"],configMgr.weights+["pdfWeightVars[1]"],"weight","overallNormHistoSys")
pdfInterSyst = Systematic("pdfInter",configMgr.weights,configMgr.weights+["pdfWeight"],configMgr.weights+["pdfWeight"],"weight","overallNormHistoSysOneSideSym")

generatorSyst = []
generatorSystW = []

# Import top theory uncertainties from Lily
from topTheoryUncertainties    import *
from topTheoryUncertainties_el import *
from topTheoryUncertainties_mu import *

# no topTheoryUncertainties for el/mu channel yet? 
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoPSSR1L3j     ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoPSCRT3j      ))
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoPSCRW3j      ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),          topTheoPSCRelT3j      ))
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),          topTheoPSCRelW3j      ))
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),          topTheoPSCRmuT3j      ))
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),          topTheoPSCRmuW3j      ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoPSSR1L5j     ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoPSCRT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoPSCRW5j      ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),          topTheoPSCRelT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),          topTheoPSCRelW5j      ))
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),          topTheoPSCRmuT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),          topTheoPSCRmuW5j      ))
  
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoPSSR1L3j     ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoPSCRT3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoPSCRW3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoPSSR1L5j     ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRT5j"),          topTheoPSCRT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW5j"),          topTheoPSCRW5j      ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),          topTheoPSCRelT3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),          topTheoPSCRelW3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRelT5j"),          topTheoPSCRelT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRelW5j"),          topTheoPSCRelW5j      ))
 
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),          topTheoPSCRmuT3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),          topTheoPSCRmuW3j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT5j"),          topTheoPSCRmuT5j      ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW5j"),          topTheoPSCRmuW5j      ))
 
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Ba"),        topTheoPSSR1L2Ba    ))    
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoPSCRW1L2Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoPSCRT1L2Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),       topTheoPSCRelW1L2Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),       topTheoPSCRelT1L2Ba  ))
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),       topTheoPSCRmuW1L2Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),       topTheoPSCRmuT1L2Ba  ))
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Bc"),       topTheoPSSR1L2Bc ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),      topTheoPSCRW1L2Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),      topTheoPSCRT1L2Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),      topTheoPSCRelW1L2Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),      topTheoPSCRelT1L2Bc  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),      topTheoPSCRmuW1L2Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),      topTheoPSCRmuT1L2Bc  ))

if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Ba"),       topTheoPSSR1L1Ba  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),      topTheoPSCRW1L1Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),      topTheoPSCRT1L1Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),      topTheoPSCRelW1L1Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),      topTheoPSCRelT1L1Ba  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),      topTheoPSCRmuW1L1Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),      topTheoPSCRmuT1L1Ba  ))

if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Bc"),       topTheoPSSR1L1Bc  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),      topTheoPSCRW1L1Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),      topTheoPSCRT1L1Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),      topTheoPSCRelW1L1Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),      topTheoPSCRelT1L1Bc  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),      topTheoPSCRmuW1L1Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),      topTheoPSCRmuT1L1Bc  ))

if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","SR2L"),          topTheoPSSR2L  ))           
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),         topTheoPSCRT2L  ))          

   
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),         topTheoPSVR3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),        topTheoPSVRT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),        topTheoPSVRW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),         topTheoPSVR3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),        topTheoPSVRT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),        topTheoPSVRW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),         topTheoPSVR3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),        topTheoPSVRT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),        topTheoPSVRW3j3  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j1"),         topTheoPSVRel3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j1"),        topTheoPSVRelT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j1"),        topTheoPSVRelW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j2"),         topTheoPSVRel3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j2"),        topTheoPSVRelT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j2"),        topTheoPSVRelW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j3"),         topTheoPSVRel3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j3"),        topTheoPSVRelT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j3"),        topTheoPSVRelW3j3  ))
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j1"),         topTheoPSVRmu3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j1"),        topTheoPSVRmuT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j1"),        topTheoPSVRmuW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j2"),         topTheoPSVRmu3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j2"),        topTheoPSVRmuT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j2"),        topTheoPSVRmuW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j3"),         topTheoPSVRmu3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j3"),        topTheoPSVRmuT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j3"),        topTheoPSVRmuW3j3  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),         topTheoPSVR5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),        topTheoPSVRT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),        topTheoPSVRW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),         topTheoPSVR5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),        topTheoPSVRT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),        topTheoPSVRW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),         topTheoPSVR5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),        topTheoPSVRT5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),        topTheoPSVRW5j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j1"),         topTheoPSVRel5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j1"),        topTheoPSVRelT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j1"),        topTheoPSVRelW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j2"),         topTheoPSVRel5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j2"),        topTheoPSVRelT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j2"),        topTheoPSVRelW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j3"),         topTheoPSVRel5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j3"),        topTheoPSVRelT5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j3"),        topTheoPSVRelW5j3  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j1"),         topTheoPSVRmu5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j1"),        topTheoPSVRmuT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j1"),        topTheoPSVRmuW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j2"),         topTheoPSVRmu5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j2"),        topTheoPSVRmuT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j2"),        topTheoPSVRmuW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j3"),         topTheoPSVRmu5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j3"),        topTheoPSVRmuT5j3  ))         
    #generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j3"),        topTheoPSVRmuW5j3  ))

    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),         topTheoPSVR3j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),        topTheoPSVRT3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),        topTheoPSVRW3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),         topTheoPSVR3j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),        topTheoPSVRT3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),        topTheoPSVRW3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),         topTheoPSVR3j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),        topTheoPSVRT3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),        topTheoPSVRW3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),         topTheoPSVR5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),        topTheoPSVRT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),        topTheoPSVRW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),         topTheoPSVR5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),        topTheoPSVRT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),        topTheoPSVRW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),         topTheoPSVR5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),        topTheoPSVRT5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),        topTheoPSVRW5j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j1"),         topTheoPSVRel3j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j1"),        topTheoPSVRelT3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j1"),        topTheoPSVRelW3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j2"),         topTheoPSVRel3j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j2"),        topTheoPSVRelT3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j2"),        topTheoPSVRelW3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j3"),         topTheoPSVRel3j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j3"),        topTheoPSVRelT3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j3"),        topTheoPSVRelW3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j1"),         topTheoPSVRel5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j1"),        topTheoPSVRelT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j1"),        topTheoPSVRelW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j2"),         topTheoPSVRel5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j2"),        topTheoPSVRelT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j2"),        topTheoPSVRelW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j3"),         topTheoPSVRel5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j3"),        topTheoPSVRelT5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j3"),        topTheoPSVRelW5j3  ))         
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j1"),         topTheoPSVRmu3j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j1"),        topTheoPSVRmuT3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j1"),        topTheoPSVRmuW3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j2"),         topTheoPSVRmu3j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j2"),        topTheoPSVRmuT3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j2"),        topTheoPSVRmuW3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j3"),         topTheoPSVRmu3j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j3"),        topTheoPSVRmuT3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j3"),        topTheoPSVRmuW3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j1"),         topTheoPSVRmu5j1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j1"),        topTheoPSVRmuT5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j1"),        topTheoPSVRmuW5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j2"),         topTheoPSVRmu5j2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j2"),        topTheoPSVRmuT5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j2"),        topTheoPSVRmuW5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j3"),         topTheoPSVRmu5j3  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j3"),        topTheoPSVRmuT5j3  ))         
    #generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j3"),        topTheoPSVRmuW5j3  ))         
    
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L2Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),       topTheoPSCRWbb1L2Ba ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),         topTheoPSVRel1L2Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),         topTheoPSVRel1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),       topTheoPSCRelWbb1L2Ba ))  
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),         topTheoPSVRmu1L2Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),         topTheoPSVRmu1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),       topTheoPSCRmuWbb1L2Ba ))
    
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L2Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L2Bc2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),       topTheoPSCRWbb1L2Bc  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),         topTheoPSVRel1L2Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),         topTheoPSVRel1L2Bc2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),       topTheoPSCRelWbb1L2Bc  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),         topTheoPSVRmu1L2Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),         topTheoPSVRmu1L2Bc2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),       topTheoPSCRmuWbb1L2Bc  ))

if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L1Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L1Ba2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),       topTheoPSCRWbb1L1Ba  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),         topTheoPSVRel1L1Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),         topTheoPSVRel1L1Ba2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),       topTheoPSCRelWbb1L1Ba  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),         topTheoPSVRmu1L1Ba1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),         topTheoPSVRmu1L1Ba2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),       topTheoPSCRmuWbb1L1Ba  ))

if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR1L1Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR1L1Bc2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),       topTheoPSCRWbb1L1Bc  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),         topTheoPSVRel1L1Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),         topTheoPSVRel1L1Bc2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),       topTheoPSCRelWbb1L1Bc  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),         topTheoPSVRmu1L1Bc1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),         topTheoPSVRmu1L1Bc2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),       topTheoPSCRmuWbb1L1Bc  ))

if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),         topTheoPSVR2L1  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),         topTheoPSVR2L2  ))          
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),         topTheoPSVR2L3  ))

#############################################################################################################
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),       topTheoRenScSR1L3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),        topTheoRenScCRW3j  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),        topTheoRenScCRelT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),        topTheoRenScCRelW3j  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),        topTheoRenScCRmuT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),        topTheoRenScCRmuW3j  ))

if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),       topTheoRenScSR1L5j  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),        topTheoRenScCRW5j  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),        topTheoRenScCRelT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),        topTheoRenScCRelW5j  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),        topTheoRenScCRmuT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),        topTheoRenScCRmuW5j  ))

    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),       topTheoRenScSR1L3j  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),        topTheoRenScCRW3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),       topTheoRenScSR1L5j  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRT5j"),        topTheoRenScCRT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRW5j"),        topTheoRenScCRW5j  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),        topTheoRenScCRelT3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),        topTheoRenScCRelW3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRelT5j"),        topTheoRenScCRelT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRelW5j"),        topTheoRenScCRelW5j  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),        topTheoRenScCRmuT3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),        topTheoRenScCRmuW3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT5j"),        topTheoRenScCRmuT5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW5j"),        topTheoRenScCRmuW5j  ))

if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Ba"),      topTheoRenScSR1L2Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L2Ba  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),     topTheoRenScCRT1L2Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),     topTheoRenScCRelW1L2Ba  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),     topTheoRenScCRelT1L2Ba  ))
        
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),     topTheoRenScCRmuW1L2Ba  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),     topTheoRenScCRmuT1L2Ba  ))
   
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Bc"),      topTheoRenScSR1L2Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L2Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),     topTheoRenScCRT1L2Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),     topTheoRenScCRelW1L2Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),     topTheoRenScCRelT1L2Bc  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),     topTheoRenScCRmuW1L2Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),     topTheoRenScCRmuT1L2Bc  ))

if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Ba"),      topTheoRenScSR1L1Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L1Ba  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),     topTheoRenScCRT1L1Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),     topTheoRenScCRelW1L1Ba  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),     topTheoRenScCRelT1L1Ba  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),     topTheoRenScCRmuW1L1Ba  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),     topTheoRenScCRmuT1L1Ba  ))

if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Bc"),      topTheoRenScSR1L1Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),     topTheoRenScCRW1L1Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),     topTheoRenScCRT1L1Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),     topTheoRenScCRelW1L1Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),     topTheoRenScCRelT1L1Bc  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),     topTheoRenScCRmuW1L1Bc  ))     
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),     topTheoRenScCRmuT1L1Bc  ))

if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","SR2L"),         topTheoRenScSR2L  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),        topTheoRenScCRT2L  ))
    
    
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),        topTheoRenScVR3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),       topTheoRenScVRT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),       topTheoRenScVRW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),        topTheoRenScVR3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),       topTheoRenScVRT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),       topTheoRenScVRW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),        topTheoRenScVR3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),       topTheoRenScVRT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),       topTheoRenScVRW3j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j1"),        topTheoRenScVRel3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j1"),       topTheoRenScVRelT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j1"),       topTheoRenScVRelW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j2"),        topTheoRenScVRel3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j2"),       topTheoRenScVRelT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j2"),       topTheoRenScVRelW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j3"),        topTheoRenScVRel3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j3"),       topTheoRenScVRelT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j3"),       topTheoRenScVRelW3j3  ))
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j1"),        topTheoRenScVRmu3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j1"),       topTheoRenScVRmuT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j1"),       topTheoRenScVRmuW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j2"),        topTheoRenScVRmu3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j2"),       topTheoRenScVRmuT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j2"),       topTheoRenScVRmuW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j3"),        topTheoRenScVRmu3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j3"),       topTheoRenScVRmuT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j3"),       topTheoRenScVRmuW3j3  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),        topTheoRenScVR5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),       topTheoRenScVRT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),       topTheoRenScVRW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),        topTheoRenScVR5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),       topTheoRenScVRT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),       topTheoRenScVRW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),        topTheoRenScVR5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),       topTheoRenScVRT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),       topTheoRenScVRW5j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j1"),        topTheoRenScVRel5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j1"),       topTheoRenScVRelT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j1"),       topTheoRenScVRelW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j2"),        topTheoRenScVRel5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j2"),       topTheoRenScVRelT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j2"),       topTheoRenScVRelW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j3"),        topTheoRenScVRel5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j3"),       topTheoRenScVRelT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j3"),       topTheoRenScVRelW5j3  ))  
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j1"),        topTheoRenScVRmu5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j1"),       topTheoRenScVRmuT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j1"),       topTheoRenScVRmuW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j2"),        topTheoRenScVRmu5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j2"),       topTheoRenScVRmuT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j2"),       topTheoRenScVRmuW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j3"),        topTheoRenScVRmu5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j3"),       topTheoRenScVRmuT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j3"),       topTheoRenScVRmuW5j3  ))  
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),        topTheoRenScVR3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),       topTheoRenScVRT3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),       topTheoRenScVRW3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),        topTheoRenScVR3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),       topTheoRenScVRT3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),       topTheoRenScVRW3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),        topTheoRenScVR3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),       topTheoRenScVRT3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),       topTheoRenScVRW3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),        topTheoRenScVR5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),       topTheoRenScVRT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),       topTheoRenScVRW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),        topTheoRenScVR5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),       topTheoRenScVRT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),       topTheoRenScVRW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),        topTheoRenScVR5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),       topTheoRenScVRT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),       topTheoRenScVRW5j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j1"),        topTheoRenScVRel3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j1"),       topTheoRenScVRelT3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j1"),       topTheoRenScVRelW3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j2"),        topTheoRenScVRel3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j2"),       topTheoRenScVRelT3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j2"),       topTheoRenScVRelW3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j3"),        topTheoRenScVRel3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j3"),       topTheoRenScVRelT3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j3"),       topTheoRenScVRelW3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j1"),        topTheoRenScVRel5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j1"),       topTheoRenScVRelT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j1"),       topTheoRenScVRelW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j2"),        topTheoRenScVRel5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j2"),       topTheoRenScVRelT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j2"),       topTheoRenScVRelW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j3"),        topTheoRenScVRel5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j3"),       topTheoRenScVRelT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j3"),       topTheoRenScVRelW5j3  ))      
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j1"),        topTheoRenScVRmu3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j1"),       topTheoRenScVRmuT3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j1"),       topTheoRenScVRmuW3j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j2"),        topTheoRenScVRmu3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j2"),       topTheoRenScVRmuT3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j2"),       topTheoRenScVRmuW3j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j3"),        topTheoRenScVRmu3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j3"),       topTheoRenScVRmuT3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j3"),       topTheoRenScVRmuW3j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j1"),        topTheoRenScVRmu5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j1"),       topTheoRenScVRmuT5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j1"),       topTheoRenScVRmuW5j1  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j2"),        topTheoRenScVRmu5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j2"),       topTheoRenScVRmuT5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j2"),       topTheoRenScVRmuW5j2  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j3"),        topTheoRenScVRmu5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j3"),       topTheoRenScVRmuT5j3  ))       
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j3"),       topTheoRenScVRmuW5j3  ))      
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L2Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),   topTheoRenScCRWbb1L2Ba  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),     topTheoRenScVRel1L2Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),     topTheoRenScVRel1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),   topTheoRenScCRelWbb1L2Ba  ))   
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),     topTheoRenScVRmu1L2Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),     topTheoRenScVRmu1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),   topTheoRenScCRmuWbb1L2Ba  ))   
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L2Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L2Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),   topTheoRenScCRWbb1L2Bc  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),     topTheoRenScVRel1L2Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),     topTheoRenScVRel1L2Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),   topTheoRenScCRelWbb1L2Bc  )) 
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),     topTheoRenScVRmu1L2Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),     topTheoRenScVRmu1L2Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),   topTheoRenScCRmuWbb1L2Bc  )) 
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L1Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L1Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),   topTheoRenScCRWbb1L1Ba  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),     topTheoRenScVRel1L1Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),     topTheoRenScVRel1L1Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),   topTheoRenScCRelWbb1L1Ba  ))   
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),     topTheoRenScVRmu1L1Ba1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),     topTheoRenScVRmu1L1Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),   topTheoRenScCRmuWbb1L1Ba  ))   
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),     topTheoRenScVR1L1Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),     topTheoRenScVR1L1Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),   topTheoRenScCRWbb1L1Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),     topTheoRenScVRel1L1Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),     topTheoRenScVRel1L1Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),   topTheoRenScCRelWbb1L1Bc  ))   
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),     topTheoRenScVRmu1L1Bc1  ))     
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),     topTheoRenScVRmu1L1Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),   topTheoRenScCRmuWbb1L1Bc  ))   
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),        topTheoRenScVR2L1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),        topTheoRenScVR2L2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),        topTheoRenScVR2L3  ))        


##################################################################################################
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoFacScSR1L3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoFacScCRW3j  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),          topTheoFacScCRelT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),          topTheoFacScCRelW3j  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),          topTheoFacScCRmuT3j  ))
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),          topTheoFacScCRmuW3j  ))

if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoFacScSR1L5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoFacScCRW5j  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),          topTheoFacScCRelT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),          topTheoFacScCRelW5j  ))

    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),          topTheoFacScCRmuT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),          topTheoFacScCRmuW5j  ))

    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L3j"),         topTheoFacScSR1L3j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),          topTheoFacScCRW3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","SR1L5j"),         topTheoFacScSR1L5j  ))        
    generatorSyst.append((("PowhegPythiaTTbar","CRT5j"),          topTheoFacScCRT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRW5j"),          topTheoFacScCRW5j  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),          topTheoFacScCRelT3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),          topTheoFacScCRelW3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRelT5j"),          topTheoFacScCRelT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRelW5j"),          topTheoFacScCRelW5j  ))
 
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),          topTheoFacScCRmuT3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),          topTheoFacScCRmuW3j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT5j"),          topTheoFacScCRmuT5j  ))         
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW5j"),          topTheoFacScCRmuW5j  ))
 
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Ba"),        topTheoFacScSR1L2Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L2Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L2Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),       topTheoFacScCRelW1L2Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),       topTheoFacScCRelT1L2Ba  ))
  
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),       topTheoFacScCRmuW1L2Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),       topTheoFacScCRmuT1L2Ba  ))
  
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L2Bc"),        topTheoFacScSR1L2Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L2Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L2Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),       topTheoFacScCRelW1L2Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),       topTheoFacScCRelT1L2Bc  ))
  
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),       topTheoFacScCRmuW1L2Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),       topTheoFacScCRmuT1L2Bc  ))
  
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Ba"),        topTheoFacScSR1L1Ba  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L1Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L1Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),       topTheoFacScCRelW1L1Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),       topTheoFacScCRelT1L1Ba  ))
 
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),       topTheoFacScCRmuW1L1Ba  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),       topTheoFacScCRmuT1L1Ba  ))
 
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","SR1L1Bc"),        topTheoFacScSR1L1Bc  ))       
    generatorSyst.append((("PowhegPythiaTTbar","CRW"),       topTheoFacScCRW1L1Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),       topTheoFacScCRT1L1Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","CRelW"),       topTheoFacScCRelW1L1Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRelT"),       topTheoFacScCRelT1L1Bc  ))
 
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","CRmuW"),       topTheoFacScCRmuW1L1Bc  ))      
    generatorSyst.append((("PowhegPythiaTTbar","CRmuT"),       topTheoFacScCRmuT1L1Bc  ))
 
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","SR2L"),           topTheoFacScSR2L  ))          
    generatorSyst.append((("PowhegPythiaTTbar","CRT"),          topTheoFacScCRT2L  ))
    
if chn==0:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),          topTheoFacScVR3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),         topTheoFacScVRT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),         topTheoFacScVRW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),          topTheoFacScVR3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),         topTheoFacScVRT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),         topTheoFacScVRW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),          topTheoFacScVR3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),         topTheoFacScVRT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),         topTheoFacScVRW3j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j1"),          topTheoFacScVRel3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j1"),         topTheoFacScVRelT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j1"),         topTheoFacScVRelW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j2"),          topTheoFacScVRel3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j2"),         topTheoFacScVRelT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j2"),         topTheoFacScVRelW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j3"),          topTheoFacScVRel3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j3"),         topTheoFacScVRelT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j3"),         topTheoFacScVRelW3j3  ))
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j1"),          topTheoFacScVRmu3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j1"),         topTheoFacScVRmuT3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j1"),         topTheoFacScVRmuW3j1  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j2"),          topTheoFacScVRmu3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j2"),         topTheoFacScVRmuT3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j2"),         topTheoFacScVRmuW3j2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j3"),          topTheoFacScVRmu3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j3"),         topTheoFacScVRmuT3j3  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j3"),         topTheoFacScVRmuW3j3  ))
if chn==7:
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),          topTheoFacScVR5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),         topTheoFacScVRT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),         topTheoFacScVRW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),          topTheoFacScVR5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),         topTheoFacScVRT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),         topTheoFacScVRW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),          topTheoFacScVR5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),         topTheoFacScVRT5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),         topTheoFacScVRW5j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j1"),          topTheoFacScVRel5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j1"),         topTheoFacScVRelT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j1"),         topTheoFacScVRelW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j2"),          topTheoFacScVRel5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j2"),         topTheoFacScVRelT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j2"),         topTheoFacScVRelW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j3"),          topTheoFacScVRel5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j3"),         topTheoFacScVRelT5j3  ))        
    #generatorSyst.append((("PowhegPythiaTTbar","VRelW5j3"),         topTheoFacScVRelW5j3  ))   
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j1"),          topTheoFacScVRmu5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j1"),         topTheoFacScVRmuT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j1"),         topTheoFacScVRmuW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j2"),          topTheoFacScVRmu5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j2"),         topTheoFacScVRmuT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j2"),         topTheoFacScVRmuW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j3"),          topTheoFacScVRmu5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j3"),         topTheoFacScVRmuT5j3  ))        
    #generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j3"),         topTheoFacScVRmuW5j3  ))   
    pass
if chn==1:
    generatorSyst.append((("PowhegPythiaTTbar","VR3j1"),          topTheoFacScVR3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j1"),         topTheoFacScVRT3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j1"),         topTheoFacScVRW3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR3j2"),          topTheoFacScVR3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j2"),         topTheoFacScVRT3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j2"),         topTheoFacScVRW3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR3j3"),          topTheoFacScVR3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT3j3"),         topTheoFacScVRT3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW3j3"),         topTheoFacScVRW3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j1"),          topTheoFacScVR5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j1"),         topTheoFacScVRT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j1"),         topTheoFacScVRW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j2"),          topTheoFacScVR5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j2"),         topTheoFacScVRT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j2"),         topTheoFacScVRW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VR5j3"),          topTheoFacScVR5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRT5j3"),         topTheoFacScVRT5j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRW5j3"),         topTheoFacScVRW5j3  ))

    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j1"),          topTheoFacScVRel3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j1"),         topTheoFacScVRelT3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j1"),         topTheoFacScVRelW3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j2"),          topTheoFacScVRel3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j2"),         topTheoFacScVRelT3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j2"),         topTheoFacScVRelW3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel3j3"),          topTheoFacScVRel3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT3j3"),         topTheoFacScVRelT3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW3j3"),         topTheoFacScVRelW3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j1"),          topTheoFacScVRel5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j1"),         topTheoFacScVRelT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j1"),         topTheoFacScVRelW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j2"),          topTheoFacScVRel5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j2"),         topTheoFacScVRelT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRelW5j2"),         topTheoFacScVRelW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRel5j3"),          topTheoFacScVRel5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRelT5j3"),         topTheoFacScVRelT5j3  ))        
    #generatorSyst.append((("PowhegPythiaTTbar","VRelW5j3"),         topTheoFacScVRelW5j3  ))       
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j1"),          topTheoFacScVRmu3j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j1"),         topTheoFacScVRmuT3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j1"),         topTheoFacScVRmuW3j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j2"),          topTheoFacScVRmu3j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j2"),         topTheoFacScVRmuT3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j2"),         topTheoFacScVRmuW3j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3j3"),          topTheoFacScVRmu3j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT3j3"),         topTheoFacScVRmuT3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW3j3"),         topTheoFacScVRmuW3j3  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j1"),          topTheoFacScVRmu5j1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j1"),         topTheoFacScVRmuT5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j1"),         topTheoFacScVRmuW5j1  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j2"),          topTheoFacScVRmu5j2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j2"),         topTheoFacScVRmuT5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j2"),         topTheoFacScVRmuW5j2  ))        
    generatorSyst.append((("PowhegPythiaTTbar","VRmu5j3"),          topTheoFacScVRmu5j3  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VRmuT5j3"),         topTheoFacScVRmuT5j3  ))        
    #generatorSyst.append((("PowhegPythiaTTbar","VRmuW5j3"),         topTheoFacScVRmuW5j3  ))       
if chn==2:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L2Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),     topTheoFacScCRWbb1L2Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),       topTheoFacScVRel1L2Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),       topTheoFacScVRel1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),     topTheoFacScCRelWbb1L2Ba  ))    
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),       topTheoFacScVRmu1L2Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),       topTheoFacScVRmu1L2Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),     topTheoFacScCRmuWbb1L2Ba  ))    
if chn==3:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L2Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L2Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),     topTheoFacScCRWbb1L2Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),       topTheoFacScVRel1L2Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),       topTheoFacScVRel1L2Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),     topTheoFacScCRelWbb1L2Bc  )) 
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),       topTheoFacScVRmu1L2Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),       topTheoFacScVRmu1L2Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),     topTheoFacScCRmuWbb1L2Bc  )) 
if chn==4:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L1Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L1Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),     topTheoFacScCRWbb1L1Ba  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),       topTheoFacScVRel1L1Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),       topTheoFacScVRel1L1Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),     topTheoFacScCRelWbb1L1Ba  ))    
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),       topTheoFacScVRmu1L1Ba1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),       topTheoFacScVRmu1L1Ba2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),     topTheoFacScCRmuWbb1L1Ba  ))    
if chn==5:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),       topTheoFacScVR1L1Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),       topTheoFacScVR1L1Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),     topTheoFacScCRWbb1L1Bc  ))
    #el
    generatorSyst.append((("PowhegPythiaTTbar","VRel1"),       topTheoFacScVRel1L1Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRel2"),       topTheoFacScVRel1L1Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRel3"),     topTheoFacScCRelWbb1L1Bc  ))  
    #mu
    generatorSyst.append((("PowhegPythiaTTbar","VRmu1"),       topTheoFacScVRmu1L1Bc1  ))      
    generatorSyst.append((("PowhegPythiaTTbar","VRmu2"),       topTheoFacScVRmu1L1Bc2  ))
    generatorSyst.append((("PowhegPythiaTTbar","VRmu3"),     topTheoFacScCRmuWbb1L1Bc  ))  
if chn==6:
    generatorSyst.append((("PowhegPythiaTTbar","VR1"),          topTheoFacScVR2L1  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR2"),          topTheoFacScVR2L2  ))         
    generatorSyst.append((("PowhegPythiaTTbar","VR3"),          topTheoFacScVR2L3  ))         

#############################################################################################################

if chn==0:
    generatorSystW.append((("SherpaWMassiveBC","SR1L3j"),       WTheoNpartSR1L3j       ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),        WTheoNpartCRT3j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),        WTheoNpartCRW3j        ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),        WTheoNpartCRelT3j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),        WTheoNpartCRelW3j        ))
 
    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),        WTheoNpartCRmuT3j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),        WTheoNpartCRmuW3j        ))
 
if chn==7:
    generatorSystW.append((("SherpaWMassiveBC","SR1L5j"),       WTheoNpartSR1L5j       ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),        WTheoNpartCRT5j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),        WTheoNpartCRW5j        ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),        WTheoNpartCRelT5j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),        WTheoNpartCRelW5j        ))

    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),        WTheoNpartCRmuT5j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),        WTheoNpartCRmuW5j        ))

    pass
if chn==1:
    generatorSystW.append((("SherpaWMassiveBC","SR1L3j"),       WTheoNpartSR1L3j       ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),        WTheoNpartCRT3j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),        WTheoNpartCRW3j        ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),        WTheoNpartCRelT3j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),        WTheoNpartCRelW3j        ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),        WTheoNpartCRmuT3j        ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),        WTheoNpartCRmuW3j        ))

if chn==2:
    generatorSystW.append((("SherpaWMassiveBC","SR1L2Ba"),      WTheoNpartSR1L2Ba      ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),     WTheoNpartCRW1L2Ba     ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),     WTheoNpartCRT1L2Ba     ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),     WTheoNpartCRelW1L2Ba     ))
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),     WTheoNpartCRelT1L2Ba     ))
  
    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),     WTheoNpartCRmuW1L2Ba     ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),     WTheoNpartCRmuT1L2Ba     ))
  
if chn==3:
    generatorSystW.append((("SherpaWMassiveBC","SR1L2Bc"),      WTheoNpartSR1L2Bc      ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),     WTheoNpartCRW1L2Bc     ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),     WTheoNpartCRT1L2Bc     ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),     WTheoNpartCRelW1L2Bc     ))
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),     WTheoNpartCRelT1L2Bc     ))
 
    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),     WTheoNpartCRmuW1L2Bc     ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),     WTheoNpartCRmuT1L2Bc     ))
 
if chn==4:
    generatorSystW.append((("SherpaWMassiveBC","SR1L1Ba"),      WTheoNpartSR1L1Ba      ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),     WTheoNpartCRW1L1Ba     ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),     WTheoNpartCRT1L1Ba     ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),     WTheoNpartCRelW1L1Ba     ))
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),     WTheoNpartCRelT1L1Ba     ))
  
    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),     WTheoNpartCRmuW1L1Ba     ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),     WTheoNpartCRmuT1L1Ba     ))
  
if chn==5:
    generatorSystW.append((("SherpaWMassiveBC","SR1L1Bc"),      WTheoNpartSR1L1Bc      ))
    generatorSystW.append((("SherpaWMassiveBC","CRW"),     WTheoNpartCRW1L1Bc     ))
    generatorSystW.append((("SherpaWMassiveBC","CRT"),     WTheoNpartCRT1L1Bc     ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","CRelW"),     WTheoNpartCRelW1L1Bc     ))
    generatorSystW.append((("SherpaWMassiveBC","CRelT"),     WTheoNpartCRelT1L1Bc     ))

    #mu
    generatorSystW.append((("SherpaWMassiveBC","CRmuW"),     WTheoNpartCRmuW1L1Bc     ))
    generatorSystW.append((("SherpaWMassiveBC","CRmuT"),     WTheoNpartCRmuT1L1Bc     ))

#if chn==6:
#    generatorSystW.append((("SherpaWMassiveBC","SR2L"),         WTheoNpartSR2L         ))
#    generatorSystW.append((("SherpaWMassiveBC","CRT"),        WTheoNpartCRT2L        ))


if chn==0:
    generatorSystW.append((("SherpaWMassiveBC","VR3j1"),        WTheoNpartVR3j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT3j1"),       WTheoNpartVRT3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW3j1"),       WTheoNpartVRW3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VR3j2"),        WTheoNpartVR3j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT3j2"),       WTheoNpartVRT3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW3j2"),       WTheoNpartVRW3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VR3j3"),        WTheoNpartVR3j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT3j3"),       WTheoNpartVRT3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW3j3"),       WTheoNpartVRW3j3       ))

    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel3j1"),        WTheoNpartVRel3j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT3j1"),       WTheoNpartVRelT3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW3j1"),       WTheoNpartVRelW3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3j2"),        WTheoNpartVRel3j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT3j2"),       WTheoNpartVRelT3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW3j2"),       WTheoNpartVRelW3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3j3"),        WTheoNpartVRel3j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT3j3"),       WTheoNpartVRelT3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW3j3"),       WTheoNpartVRelW3j3       ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu3j1"),        WTheoNpartVRmu3j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT3j1"),       WTheoNpartVRmuT3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW3j1"),       WTheoNpartVRmuW3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3j2"),        WTheoNpartVRmu3j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT3j2"),       WTheoNpartVRmuT3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW3j2"),       WTheoNpartVRmuW3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3j3"),        WTheoNpartVRmu3j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT3j3"),       WTheoNpartVRmuT3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW3j3"),       WTheoNpartVRmuW3j3       ))
if chn==7:
    generatorSystW.append((("SherpaWMassiveBC","VR5j1"),        WTheoNpartVR5j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT5j1"),       WTheoNpartVRT5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW5j1"),       WTheoNpartVRW5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VR5j2"),        WTheoNpartVR5j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT5j2"),       WTheoNpartVRT5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW5j2"),       WTheoNpartVRW5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VR5j3"),        WTheoNpartVR5j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT5j3"),       WTheoNpartVRT5j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW5j3"),       WTheoNpartVRW5j3       ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel5j1"),        WTheoNpartVRel5j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT5j1"),       WTheoNpartVRelT5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW5j1"),       WTheoNpartVRelW5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel5j2"),        WTheoNpartVRel5j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT5j2"),       WTheoNpartVRelT5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW5j2"),       WTheoNpartVRelW5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel5j3"),        WTheoNpartVRel5j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT5j3"),       WTheoNpartVRelT5j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW5j3"),       WTheoNpartVRelW5j3       ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu5j1"),        WTheoNpartVRmu5j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT5j1"),       WTheoNpartVRmuT5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW5j1"),       WTheoNpartVRmuW5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu5j2"),        WTheoNpartVRmu5j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT5j2"),       WTheoNpartVRmuT5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW5j2"),       WTheoNpartVRmuW5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu5j3"),        WTheoNpartVRmu5j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT5j3"),       WTheoNpartVRmuT5j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW5j3"),       WTheoNpartVRmuW5j3       ))
    pass
if chn==1:
    generatorSystW.append((("SherpaWMassiveBC","VR3j1"),        WTheoNpartVR3j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT3j1"),       WTheoNpartVRT3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW3j1"),       WTheoNpartVRW3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VR3j2"),        WTheoNpartVR3j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT3j2"),       WTheoNpartVRT3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW3j2"),       WTheoNpartVRW3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VR3j3"),        WTheoNpartVR3j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT3j3"),       WTheoNpartVRT3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW3j3"),       WTheoNpartVRW3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VR5j1"),        WTheoNpartVR5j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT5j1"),       WTheoNpartVRT5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW5j1"),       WTheoNpartVRW5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VR5j2"),        WTheoNpartVR5j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT5j2"),       WTheoNpartVRT5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW5j2"),       WTheoNpartVRW5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VR5j3"),        WTheoNpartVR5j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRT5j3"),       WTheoNpartVRT5j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRW5j3"),       WTheoNpartVRW5j3       ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel3j1"),        WTheoNpartVRel3j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT3j1"),       WTheoNpartVRelT3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW3j1"),       WTheoNpartVRelW3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3j2"),        WTheoNpartVRel3j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT3j2"),       WTheoNpartVRelT3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW3j2"),       WTheoNpartVRelW3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3j3"),        WTheoNpartVRel3j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT3j3"),       WTheoNpartVRelT3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW3j3"),       WTheoNpartVRelW3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel5j1"),        WTheoNpartVRel5j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT5j1"),       WTheoNpartVRelT5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW5j1"),       WTheoNpartVRelW5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel5j2"),        WTheoNpartVRel5j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT5j2"),       WTheoNpartVRelT5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW5j2"),       WTheoNpartVRelW5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRel5j3"),        WTheoNpartVRel5j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRelT5j3"),       WTheoNpartVRelT5j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRelW5j3"),       WTheoNpartVRelW5j3       ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu3j1"),        WTheoNpartVRmu3j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT3j1"),       WTheoNpartVRmuT3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW3j1"),       WTheoNpartVRmuW3j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3j2"),        WTheoNpartVRmu3j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT3j2"),       WTheoNpartVRmuT3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW3j2"),       WTheoNpartVRmuW3j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3j3"),        WTheoNpartVRmu3j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT3j3"),       WTheoNpartVRmuT3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW3j3"),       WTheoNpartVRmuW3j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu5j1"),        WTheoNpartVRmu5j1        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT5j1"),       WTheoNpartVRmuT5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW5j1"),       WTheoNpartVRmuW5j1       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu5j2"),        WTheoNpartVRmu5j2        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT5j2"),       WTheoNpartVRmuT5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW5j2"),       WTheoNpartVRmuW5j2       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu5j3"),        WTheoNpartVRmu5j3        ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuT5j3"),       WTheoNpartVRmuT5j3       ))
    generatorSystW.append((("SherpaWMassiveBC","VRmuW5j3"),       WTheoNpartVRmuW5j3       ))
if chn==2:
    generatorSystW.append((("SherpaWMassiveBC","VR1"),     WTheoNpartVR1L2Ba1     ))
    generatorSystW.append((("SherpaWMassiveBC","VR2"),     WTheoNpartVR1L2Ba2     ))
    generatorSystW.append((("SherpaWMassiveBC","VR3"),   WTheoNpartCRWbb1L2Ba   ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel1"),     WTheoNpartVRel1L2Ba1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel2"),     WTheoNpartVRel1L2Ba2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3"),   WTheoNpartCRelWbb1L2Ba   ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu1"),     WTheoNpartVRmu1L2Ba1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu2"),     WTheoNpartVRmu1L2Ba2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3"),   WTheoNpartCRmuWbb1L2Ba   ))
if chn==3:
    generatorSystW.append((("SherpaWMassiveBC","VR1"),     WTheoNpartVR1L2Bc1     ))
    generatorSystW.append((("SherpaWMassiveBC","VR2"),     WTheoNpartVR1L2Bc2     ))
    generatorSystW.append((("SherpaWMassiveBC","VR3"),   WTheoNpartCRWbb1L2Bc   ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel1"),     WTheoNpartVRel1L2Bc1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel2"),     WTheoNpartVRel1L2Bc2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3"),   WTheoNpartCRelWbb1L2Bc   ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu1"),     WTheoNpartVRmu1L2Bc1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu2"),     WTheoNpartVRmu1L2Bc2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3"),   WTheoNpartCRmuWbb1L2Bc   ))
if chn==4:
    generatorSystW.append((("SherpaWMassiveBC","VR1"),     WTheoNpartVR1L1Ba1     ))
    generatorSystW.append((("SherpaWMassiveBC","VR2"),     WTheoNpartVR1L1Ba2     ))
    generatorSystW.append((("SherpaWMassiveBC","VR3"),   WTheoNpartCRWbb1L1Ba   ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel1"),     WTheoNpartVRel1L1Ba1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel2"),     WTheoNpartVRel1L1Ba2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3"),   WTheoNpartCRelWbb1L1Ba   ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu1"),     WTheoNpartVRmu1L1Ba1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu2"),     WTheoNpartVRmu1L1Ba2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3"),   WTheoNpartCRmuWbb1L1Ba   ))
if chn==5:
    generatorSystW.append((("SherpaWMassiveBC","VR1"),     WTheoNpartVR1L1Bc1     ))
    generatorSystW.append((("SherpaWMassiveBC","VR2"),     WTheoNpartVR1L1Bc2     ))
    generatorSystW.append((("SherpaWMassiveBC","VR3"),   WTheoNpartCRWbb1L1Bc   ))
    #el
    generatorSystW.append((("SherpaWMassiveBC","VRel1"),     WTheoNpartVRel1L1Bc1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel2"),     WTheoNpartVRel1L1Bc2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRel3"),   WTheoNpartCRelWbb1L1Bc   ))
    #mu
    generatorSystW.append((("SherpaWMassiveBC","VRmu1"),     WTheoNpartVRmu1L1Bc1     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu2"),     WTheoNpartVRmu1L1Bc2     ))
    generatorSystW.append((("SherpaWMassiveBC","VRmu3"),   WTheoNpartCRmuWbb1L1Bc   ))

#if chn==6:
#    generatorSystW.append((("SherpaWMassiveBC","VR1"),        WTheoNpartVR2L1        ))
#    generatorSystW.append((("SherpaWMassiveBC","VR2"),        WTheoNpartVR2L2        ))
#    generatorSystW.append((("SherpaWMassiveBC","VR3"),        WTheoNpartVR2L3        ))

generatorSyst += generatorSystW


SystList = []
if True:
    SystList.append(jes)
    SystList.append(jer)
    SystList.append(lepEff)
    SystList.append(scalest)
    SystList.append(resost)
    SystList.append(pileup)
    SystList.append(trEff)
    SystList.append(egzee)
    SystList.append(egmat)
    SystList.append(egps)
    SystList.append(eglow)
    SystList.append(egres)
    SystList.append(merm)
    SystList.append(meri)
    SystList.append(mes)

## do these per sample/channel
#SystList.append(bTagSyst)
#SystList.append(qfacT)
#SystList.append(qfacW)
#SystList.append(ktfacT)
#SystList.append(ktfacW)
#SystList.append(iqoptW)

#--------------------------------------------------------------------------
# List of channel selections
#--------------------------------------------------------------------------

TriggerSelection = "&& (AnalysisType==5) "
LeptonSelection  = TriggerSelection + "&& (lep1Pt<25) && ((lep1Pt>10&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))"
HardLeptonSelection = "&& (lep1Pt>25) && (lep2Pt<10) && ((lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Flavor==-1))"

ElectronSelection  = TriggerSelection + "&& (lep1Pt<25) && (lep1Pt>10&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))"
MuonSelection  = TriggerSelection + "&& (lep1Pt<25) && (lep1Pt>6&&lep1Flavor==-1)"

HardElectronSelection = "&& (lep1Pt>25) && (lep2Pt<10) && (lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))"
HardMuonSelection = "&& (lep1Pt>25) && (lep2Pt<10) && (lep1Flavor==-1)"

    
if chn==0:

    Jet3Selection                    = "&& jet1Pt_jvf25>180 && nJet25_jvf25>=3 && jet5Pt_jvf25<25 && dRminLepJet20>1.0 "

    configMgr.cutsDict["SR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    #el
    configMgr.cutsDict["CRelT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection +  ElectronSelection
    configMgr.cutsDict["CRelW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection +  ElectronSelection
    configMgr.cutsDict["VRel3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRel3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRel3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + ElectronSelection
    #mu
    configMgr.cutsDict["CRmuT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection +  MuonSelection
    configMgr.cutsDict["CRmuW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection +  MuonSelection
    configMgr.cutsDict["VRmu3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmu3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmu3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + MuonSelection


    pass
    #
elif chn==7:

    Jet5Selection                    = "&& (jet1Pt_jvf25>180 && jet5Pt_jvf25>25)"  #no dRmin for 5Jet
    # 
    configMgr.cutsDict["SR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict["CRT"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict["VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    #el
    configMgr.cutsDict["CRelT"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + ElectronSelection
    configMgr.cutsDict["CRelW"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRel5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRel5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRel5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + ElectronSelection
    #mu
    configMgr.cutsDict["CRmuT"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + MuonSelection
    configMgr.cutsDict["CRmuW"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmu5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmu5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmu5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + MuonSelection


    pass
elif chn==1:

    Jet3Selection                    = "&& jet1Pt_jvf25>180 && nJet25_jvf25>=3 && jet5Pt_jvf25<25 && dRminLepJet20>1.0"
    Jet5Selection                    = "&& (jet1Pt_jvf25>180 && jet5Pt_jvf25>25)"  #no dRmin for 5Jet
    #
    configMgr.cutsDict["SR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["SR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_jvf25))>0.3 )"             + Jet5Selection + LeptonSelection

    configMgr.cutsDict["CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["CRT5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict["VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + LeptonSelection

    #el
    configMgr.cutsDict["CRelT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection + ElectronSelection
    configMgr.cutsDict["CRelW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["CRelT5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + ElectronSelection
    configMgr.cutsDict["CRelW5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + ElectronSelection
    #mu
    configMgr.cutsDict["CRmuT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet3Selection + MuonSelection
    configMgr.cutsDict["CRmuW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["CRmuT5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25>0 )"   + Jet5Selection + MuonSelection
    configMgr.cutsDict["CRmuW5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_jvf25==0 )"  + Jet5Selection + MuonSelection



   #el
    configMgr.cutsDict["VRel3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRel3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRel3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + ElectronSelection
    configMgr.cutsDict["VRelW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + ElectronSelection
    #mu
    configMgr.cutsDict["VRmu3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmu3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmu3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet3Selection + MuonSelection
    configMgr.cutsDict["VRmuW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet3Selection + MuonSelection
   #el
    configMgr.cutsDict["VRel5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRel5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRel5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + ElectronSelection
    configMgr.cutsDict["VRelW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + ElectronSelection
    #mu
    configMgr.cutsDict["VRmu5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmu5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmu5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25>0 )"  + Jet5Selection + MuonSelection
    configMgr.cutsDict["VRmuW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_jvf25==0 )" + Jet5Selection + MuonSelection

    pass
    #
elif chn==2:
    # (lep1Pt<25. && met>200. && nB2Jet60==2 && nJet50_jvf25_wo2jets==0 && mctcorr>150. && ht_wo2jets<50. && dphimin>0.4);
    JetSelection               = "&& (nJet50_jvf25_wo2jets==0 && dphimin>0.4)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)" 
    BJetSelection              = "&& nB2Jet60==2" # "&& (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    BVetoSelection             = "&& nB2Jet60==0" # "&& (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    configMgr.cutsDict["SR1L2Ba"] = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + LeptonSelection

    configMgr.cutsDict["CRW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>200 && ht_wo2jets<50 && mctcorr>150)" + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection

    configMgr.cutsDict["VR1"]  = "(met<200 && met>150 && ht_wo2jets<50 && mctcorr>150)" + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["VR3"] = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection
    #el
    configMgr.cutsDict["CRelW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>200 && ht_wo2jets<50 && mctcorr>150)" + BVetoSelection + HardElectronSelection
    configMgr.cutsDict["CRelT"]   = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardElectronSelection

    configMgr.cutsDict["VRel1"]  = "(met<200 && met>150 && ht_wo2jets<50 && mctcorr>150)" + JetSelection + BJetSelection + ElectronSelection
    configMgr.cutsDict["VRel2"]  = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardElectronSelection
    configMgr.cutsDict["VRel3"] = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardElectronSelection
    #mu
    configMgr.cutsDict["CRmuW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>200 && ht_wo2jets<50 && mctcorr>150)" + BVetoSelection + HardMuonSelection
    configMgr.cutsDict["CRmuT"]   = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardMuonSelection
    
    configMgr.cutsDict["VRmu1"]  = "(met<200 && met>150 && ht_wo2jets<50 && mctcorr>150)" + JetSelection + BJetSelection + MuonSelection
    configMgr.cutsDict["VRmu2"]  = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardMuonSelection
    configMgr.cutsDict["VRmu3"] = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardMuonSelection
    pass
elif chn==3:
    JetSelection               = "&& (nJet50_jvf25_wo2jets==0 && dphimin>0.4)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)" 
    BJetSelection              = "&& nB2Jet60==2" # "&&  (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    BVetoSelection             = "&& nB2Jet60==0"
    configMgr.cutsDict["SR1L2Bc"] = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + LeptonSelection 
    configMgr.cutsDict["CRW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>300 && ht_wo2jets<50 && mctcorr>200)" + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection

    configMgr.cutsDict["VR1"]  = "(met>150 && met<300 && mctcorr>200)" + JetSelection + BJetSelection + LeptonSelection 
    configMgr.cutsDict["VR2"]  = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["VR3"] = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection
    #el
    configMgr.cutsDict["CRelW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>300 && ht_wo2jets<50 && mctcorr>200)" + BVetoSelection + HardElectronSelection
    configMgr.cutsDict["CRelT"]   = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardElectronSelection

    configMgr.cutsDict["VRel1"]  = "(met>150 && met<300 && mctcorr>200)" + JetSelection + BJetSelection + ElectronSelection 
    configMgr.cutsDict["VRel2"]  = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + HardElectronSelection
    configMgr.cutsDict["VRel3"] = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardElectronSelection
    #mu
    configMgr.cutsDict["CRmuW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>300 && ht_wo2jets<50 && mctcorr>200)" + BVetoSelection + HardMuonSelection
    configMgr.cutsDict["CRmuT"]   = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardMuonSelection

    configMgr.cutsDict["VRmu1"]  = "(met>150 && met<300 && mctcorr>200)" + JetSelection + BJetSelection + MuonSelection 
    configMgr.cutsDict["VRmu2"]  = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + HardMuonSelection
    configMgr.cutsDict["VRmu3"] = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardMuonSelection
  
    pass
elif chn==4:
    # (lep1Pt<25. && met>300. && mt>100. && jet1Pt_jvf25>180. && nJet25_jvf25>=3 && jet1IsB_MV1_70p_jvf25 && nBJet25_MV1_70p_jvf25>0 && met/(meffInc25_jvf25)>0.36 );
    LeptonSelection                  += " && dRminLepJet20>1.0 "
    HardLeptonSelection              += " && dRminLepJet20>1.0 "
    ElectronSelection                  += " && dRminLepJet20>1.0 " 
    HardElectronSelection              += " && dRminLepJet20>1.0 "
    MuonSelection                  += " && dRminLepJet20>1.0 " 
    HardMuonSelection              += " && dRminLepJet20>1.0 "
   

    JetSelection               = "&& (jet1Pt_jvf25>180 && nJet40_jvf25>=3)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    BJetSelection              = "&& jet1IsB_MV1_70p_jvf25==0 && nBJet40_MV1_70p_jvf25>0" # (jetMV1[1]>0.772 || jetMV1[2]>0.772))" # nBJet40>0
    BVetoSelection             = "&& (nBJet40_MV1_70p_jvf25==0)"
    #BJetSelection = "&& (jetMV1[0]<0.772 && nBJet40>0)" # (jetMV1[1]>0.772 || jetMV1[2]>0.772))" # nBJet40>0
    configMgr.cutsDict["SR1L1Ba"] = "(met>250 && met/(meffInc40_jvf25)>0.35 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
 
    configMgr.cutsDict["CRW"]   = "(met>250 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && mt>100)"            + JetSelection + BJetSelection  + HardLeptonSelection

    configMgr.cutsDict["VR1"]  = "(met>150 && met<250 && met/(meffInc40_jvf25)>0.35 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met/(meffInc40_jvf25)>0.35 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR3"] = "(met>150 && met/(meffInc40_jvf25)>0.35 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    #el
    configMgr.cutsDict["CRelW"]   = "(met>250 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardElectronSelection
    configMgr.cutsDict["CRelT"]   = "(met>150 && mt>100)"            + JetSelection + BJetSelection  + HardElectronSelection

    configMgr.cutsDict["VRel1"]  = "(met>150 && met<250 && met/(meffInc40_jvf25)>0.35 && mt>100)" + JetSelection + BJetSelection  + ElectronSelection
    configMgr.cutsDict["VRel2"]  = "(met>150 && met/(meffInc40_jvf25)>0.35 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardElectronSelection
    configMgr.cutsDict["VRel3"] = "(met>150 && met/(meffInc40_jvf25)>0.35 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardElectronSelection
    #mu
    configMgr.cutsDict["CRmuW"]   = "(met>250 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardMuonSelection
    configMgr.cutsDict["CRmuT"]   = "(met>150 && mt>100)"            + JetSelection + BJetSelection  + HardMuonSelection

    configMgr.cutsDict["VRmu1"]  = "(met>150 && met<250 && met/(meffInc40_jvf25)>0.35 && mt>100)" + JetSelection + BJetSelection  + MuonSelection
    configMgr.cutsDict["VRmu2"]  = "(met>150 && met/(meffInc40_jvf25)>0.35 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardMuonSelection
    configMgr.cutsDict["VRmu3"] = "(met>150 && met/(meffInc40_jvf25)>0.35 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardMuonSelection

    
    pass
elif chn==5:
    LeptonSelection                  += " && dRminLepJet20>1.0 "
    HardLeptonSelection              += " && dRminLepJet20>1.0 "
    ElectronSelection                  += " && dRminLepJet20>1.0 " 
    HardElectronSelection              += " && dRminLepJet20>1.0 "
    MuonSelection                  += " && dRminLepJet20>1.0 " 
    HardMuonSelection              += " && dRminLepJet20>1.0 "
   

    JetSelection               = "&& (jet1Pt_jvf25>180 && nJet25_jvf25>=3)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)"
    BJetSelection              = "&& jet1IsB_MV1_70p_jvf25==0 && nBJet25_MV1_70p_jvf25>0" # "&& (jetMV1[0]>0.772 || jetMV1[1]>0.772 || jetMV1[2]>0.772)" # nBJet40>0
    BVetoSelection             = "&& (nBJet25_MV1_70p_jvf25==0)"
    configMgr.cutsDict["SR1L1Bc"] = "(met>300 && met/(meffInc25_jvf25)>0.35 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection

    configMgr.cutsDict["CRW"]   = "(met>300  && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150  && mt>100)"            + JetSelection + BJetSelection  + HardLeptonSelection

    configMgr.cutsDict["VR1"]  = "(met>150 && met<300 && met/(meffInc25_jvf25)>0.35 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met/(meffInc25_jvf25)>0.35 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR3"] = "(met>150 && met/(meffInc25_jvf25)>0.35 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    #el
    configMgr.cutsDict["CRelW"]   = "(met>300  && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardElectronSelection
    configMgr.cutsDict["CRelT"]   = "(met>150  && mt>100)"            + JetSelection + BJetSelection  + HardElectronSelection
 
    configMgr.cutsDict["VRel1"]  = "(met>150 && met<300 && met/(meffInc25_jvf25)>0.35 && mt>100)" + JetSelection + BJetSelection  + ElectronSelection
    configMgr.cutsDict["VRel2"]  = "(met>150 && met/(meffInc25_jvf25)>0.35 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardElectronSelection
    configMgr.cutsDict["VRel3"] = "(met>150 && met/(meffInc25_jvf25)>0.35 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardElectronSelection
    #mu
    configMgr.cutsDict["CRmuW"]   = "(met>300 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardMuonSelection
    configMgr.cutsDict["CRmuT"]   = "(met>150 && mt>100)"            + JetSelection + BJetSelection  + HardMuonSelection

    configMgr.cutsDict["VRmu1"]  = "(met>150 && met<300 && met/(meffInc25_jvf25)>0.35 && mt>100)" + JetSelection + BJetSelection  + MuonSelection
    configMgr.cutsDict["VRmu2"]  = "(met>150 && met/(meffInc25_jvf25)>0.35 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardMuonSelection
    configMgr.cutsDict["VRmu3"] = "(met>150 && met/(meffInc25_jvf25)>0.35 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardMuonSelection

    pass
elif chn==6: # soft 2-lepton analysis
    DiLeptonSelection          = TriggerSelection + "&& (lep1Pt>6 && lep2Pt>6) && (lep1Flavor==-1) && (lep2Flavor==-1) && (lep1Charge*lep2Charge<0)" ## os dimuon
    DiLeptonSelection          += " && dRminLep2Jet20>1.0 " ## Lep2 AND JET: only for chn==6
    JetSelection               = "&& (jet1Pt_jvf25>100 && jet2Pt_jvf25>25)"
    # these B-jet selections are okay
    BVetoSelection             = "&& nB3Jet25_MV1_80p_jvf25==0 "
    BJetSelection              = "&& nB3Jet25_MV1_80p_jvf25>0  "
    configMgr.cutsDict["SR2L"] = "(met>200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6 && lep1Pt<25 && met/(meffInc25_jvf25)>0.3)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep2Pt>25)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep2Pt>25)" + JetSelection + DiLeptonSelection

    configMgr.cutsDict["VR1"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>25  && lep2Pt<25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6   && lep1Pt<25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict["VR3"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>25  && lep2Pt<25)" + JetSelection  + BVetoSelection + DiLeptonSelection
 
    pass

#############
## Samples ##
#############

errHigh = 1.3
errLow  = 0.7

## not yet there ...
## to add AlpgenDY and ttbarV
AlpgenDYSample = Sample("AlpgenDY",kOrange-8)
AlpgenDYSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
AlpgenDYSample.setStatConfig(useStat)
AlpgenDYSample.setFileList(AlpgenDYFiles)
AlpgenDYSample.setNormByTheory()


DibosonsSample = Sample("SherpaDibosons",kOrange-8)
DibosonsSample.addSystematic(Systematic("errDB", configMgr.weights, 1.5, 0.5, "user","userOverallSys"))
#DibosonsSample.addSystematic( sherheg )
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setFileList(SherpaDibosonsFiles)
DibosonsSample.setNormByTheory()


ttbarVSample = Sample("ttbarV",kYellow-8)
ttbarVSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setFileList(ttbarVFiles)
ttbarVSample.setNormByTheory()


SingleTopSample = Sample("SingleTop",kGreen-5)
SingleTopSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setFileList(SingleTopFiles)
SingleTopSample.setNormByTheory()
if chn==6:
    SingleTopSample.addSampleSpecificWeight("(DatasetNumber==108346)")


TTbarSampleName = 'PowhegPythiaTTbar'
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
TTbarSample.setFileList(PowhegPythiaTTbarFiles)
TTbarSample.addSystematic(pdfIntraSyst)
TTbarSample.addSystematic(pdfInterSyst)
TTbarSample.mergeOverallSysSet = ['pdfIntra','pdfInter'] ## post-processing
TTbarSample.addSystematic(pythwig) ## MB: Turned up for now ...
#TTbarSample.addSystematic(pythgen) ## MB: Turned off for now ...

if (chn>=0 and chn<=7) and chn!=2 and chn!=6:
    #    TTbarSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
    TTbarSample.setNormRegions([("CRelT","cuts"),("CRmuT","cuts"),("CRelW","cuts"),("CRmuW","cuts")])
elif chn==2:
    #TTbarSample.setNormRegions([("CRT","cuts")])
    TTbarSample.setNormRegions([("CRelT","cuts"),("CRmuT","cuts")])
elif chn==6:
    TTbarSample.setNormRegions([("CRT","cuts")])
if chn==6:
    TTbarSample.addSampleSpecificWeight("(DecayIndexTTbar==1)")
    pass

doSherpa = True
if doSherpa and (chn>=0 and chn<=7): # stop
    WSampleName = "SherpaWMassiveBC"
else:
    WSampleName = "AlpgenW"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_WZ",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.addSystematic(pdfIntraSyst)
WSample.addSystematic(pdfInterSyst)
WSample.mergeOverallSysSet = ['pdfIntra','pdfInter'] ## post-processing
if (chn>=0 and chn<=7) and chn!=2 and chn!=6:
    #WSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
    WSample.setNormRegions([("CRelT","cuts"),("CRmuT","cuts"),("CRelW","cuts"),("CRmuW","cuts")])
elif chn==2:
    #WSample.setNormRegions([("CRW","cuts")])
    WSample.setNormRegions([("CRelW","cuts"),("CRmuW","cuts")])
if doSherpa and (chn>=0 and chn<=7): # stop
    WSample.setFileList(SherpaWMassiveBCFiles) 
    #WSample.addSystematic(sherpgen)  ## turned off for now
else:
    WSample.setFileList(AlpgenWFiles) 


if doSherpa and (chn>=0 and chn<=7): # stop
    ZSampleName = "SherpaZMassiveBC"
else:
    ZSampleName = "AlpgenZ"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setNormFactor("mu_WZ",1.,0.,5.)
ZSample.setStatConfig(useStat)
ZSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
if (chn>=0 and chn<=7) and chn!=2 and chn!=6:
    #ZSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
    ZSample.setNormRegions([("CRelT","cuts"),("CRmuT","cuts"),("CRelW","cuts"),("CRmuW","cuts")])
elif chn==2:
    #ZSample.setNormRegions([("CRW","cuts")])
    ZSample.setNormRegions([("CRelW","cuts"),("CRmuW","cuts")])
if doSherpa and (chn>=0 and chn<=7): # stop
    ZSample.setFileList(SherpaZMassiveBCFiles)
else:
    ZSample.setFileList(AlpgenZFiles) ###(SherpaZMassiveBCFiles)


QCDSample = Sample("QCD",kGray+1)
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
QCDSample.setFileList(qcdFiles)
#QCDSample.addSampleSpecificWeight("abs(qcdWeight)<10")

DataSample = Sample("Data",kBlack)
DataSample.setData()
DataSample.setFileList(dataFiles)


#######################
## Systematics (1/2) ##
#######################

bkgMCSamples = [DibosonsSample,SingleTopSample,TTbarSample,ZSample,ttbarVSample,AlpgenDYSample]
if chn!=6: bkgMCSamples += [WSample] # absorbed in QCD

if includeSyst:
    SetupSamples( bkgMCSamples, SystList )
    #SetupSamples( [TTbarSample], [qfacT,ktfacT] )
    SetupSamples( [WSample], [qfacW,ktfacW] )

## more systematics below

##################
# The fit setup  #
##################

# First define HistFactory attributes
configMgr.analysisName   = "SoftLeptonMoriond2013_"+anaName+"_"+grid+gridspec+allpoints[0] # Name to give the analysis
configMgr.outputFileName = "results/"+configMgr.analysisName+".root"
configMgr.histCacheFile  = "data/"+configMgr.analysisName+".root"

for point in allpoints:
    if point=="": continue

    # Fit config instance
    name="Fit_"+anaName+"_"+grid+gridspec+point
    myFitConfig = configMgr.addFitConfig(name)
    if useStat:
        myFitConfig.statErrThreshold=0.05
    else:
        myFitConfig.statErrThreshold=None

    #Add Measurement
    #meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.036
    meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.028)
    meas.addParamSetting("alpha_pdfInter",True,0)
    
    if myFitType==FitType.Background: 
        meas.addPOI("mu_SIG")
        meas.addParamSetting("Lumi",True,1)
        #meas.addParamSetting("gamma_stat_CRW_cuts_bin_0",True,1)

    #meas.addParamSetting("mu_Diboson",True,1) # fix diboson to MC prediction

    if chn==2:
        #meas.addParamSetting("alpha_QCDNorm_CRW",True,0)
        pass
    #meas.addParamSetting("alpha_QCDNorm_WR",True,0)
    #meas.addParamSetting("alpha_QCDNorm_TR",True,0)
    #if chn==1:
    #    meas.addParamSetting("alpha_QCDNorm_WR5",True,0)
    #    meas.addParamSetting("alpha_QCDNorm_TR5",True,0)
    #meas.addParamSetting("alpha_QCDNorm_SS",True,0)
    if chn==6:
        meas.addParamSetting("mu_WZ",True,1) # fix diboson to MC prediction

    # x-section uncertainties stop and ued
    if chn>=2 and chn<=5:
        xsecSig = Systematic("SigXSec", configMgr.weights, 1.16, 0.84, "user", "userOverallSys")
        pass
    if chn==6:
        xsecSig = Systematic("SigXSec", configMgr.weights, 1.25, 0.75, "user", "userOverallSys")
        pass
    
    # ISR uncertainty (SS and GG grids)
    if myFitType==FitType.Exclusion:
      if (chn>=0 and chn<=5) or chn==7:
        massSet = point.split('_')
        if len(massSet)!=3: 
            log.fatal("Invalid grid point: %s" % point)
        DeltaM = float(massSet[0]) - float(massSet[2])
        if DeltaM<=0: 
            log.fatal("Invalid value of DeltaM : %f" % DeltaM)
        #
        eisr3 = 0.00
        eisr5 = 0.00
        if grid=="SM_GG1step": 
            eisr3 = exp(-1.4-0.013*DeltaM)
            eisr5 = exp(-1.2-0.005*DeltaM)
            if eisr3<0.06: eisr3=0.06
            if eisr5<0.06: eisr5=0.06
            pass
        elif (grid=="SM_SS1step"): 
            eisr3 = 0.06+exp(0.8-0.1*DeltaM)
            eisr5 = 0.06+exp(-1.5-0.005*DeltaM)
            pass
        elif (grid=="StopBCharDeg") and (chn>=4 and chn<=5):
            eisr3 = exp(-0.0073*DeltaM - 1.93)
            eisr5 = exp(-0.0073*DeltaM - 1.93)
            pass               
        isr3j = Systematic("isr", configMgr.weights, 1.00+eisr3, 1.00-eisr3, "user", "userOverallSys")
        isr5j = Systematic("isr", configMgr.weights, 1.00+eisr5, 1.00-eisr5, "user", "userOverallSys")
        pass
      elif chn==6:
        isrmm = Systematic("isr", configMgr.weights, 1.25, 0.75, "user", "userOverallSys")

    
    #-------------------------------------------------
    # First add the (grid point specific) signal sample
    #-------------------------------------------------
    sigSampleName=grid+"_"+point
    if myFitType==FitType.Exclusion:
        sigSample = Sample(sigSampleName,kRed)
        sigSample.setFileList(sigFiles)
        #sigSample.setTreeName(grid+"_"+point+suffix)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1,0.,100.)
        SetupSamples( [sigSample], SystList+[xsecSig] ) ## systematics
        #sigSample.addSystematic(xsecSig)      ## systematic not working?
        sigSample.setStatConfig(useStat)
        sigSample.mergeOverallSysSet = ['SigXSec','isr'] ## post-processing
        myFitConfig.addSamples(sigSample)
        myFitConfig.setSignalSample(sigSample)
        meas.addPOI("mu_SIG")

    myFitConfig.addSamples(bkgMCSamples+[QCDSample,DataSample]) # DibosonsSample

    #MyConfigExample.py:nJetWS.hasBQCD = False
    #MyConfigExample.py:nJetTS.hasBQCD = True

    ## TR sofar defined for every channel
    if chn==6:
        TR = myFitConfig.addChannel("cuts",["CRT"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(TR)
        TR.hasBQCD = True
        BRset = [TR]
    if chn!=6:
        TRel = myFitConfig.addChannel("cuts",["CRelT"],1,0.5,1.5)
        TRmu = myFitConfig.addChannel("cuts",["CRmuT"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(TRel)
        myFitConfig.setBkgConstrainChannels(TRmu)
        TRel.hasBQCD = True # b-tag applied
        TRmu.hasBQCD = True # b-tag applied
        BRset = [TRel,TRmu] 
    ## WR = second control region
    ##########if chn!=6: # di-lepton region only has control region
    if chn>=0 and chn<=5 or chn==7:
        #WR = myFitConfig.addChannel("cuts",["CRW"],1,0.5,1.5)
        WRel = myFitConfig.addChannel("cuts",["CRelW"],1,0.5,1.5)
        WRmu = myFitConfig.addChannel("cuts",["CRmuW"],1,0.5,1.5)
        #myFitConfig.setBkgConstrainChannels(WR)
        myFitConfig.setBkgConstrainChannels(WRel)
        myFitConfig.setBkgConstrainChannels(WRmu)
        BRset = [TRel,TRmu,WRel,WRmu]
    if chn>=2 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            #WRbb = myFitConfig.addChannel("cuts",["VR3"],1,0.5,1.5)
            #WRbb.hasBQCD = True # b-tag applied

            WRelbb = myFitConfig.addChannel("cuts",["VRel3"],1,0.5,1.5)
            WRelbb.hasBQCD = True # b-tag applied

            WRmubb = myFitConfig.addChannel("cuts",["VRmu3"],1,0.5,1.5)
            WRmubb.hasBQCD = True # b-tag applied
            
            #BRset = [TR,WR,WRbb]
            BRset = [TRel,TRmu,WRel,WRmu,WRelbb,WRmubb]
            myFitConfig.setValidationChannels(WRelbb)
            myFitConfig.setValidationChannels(WRmubb)

    ## FitType
    SRset = []
    if chn==0:
        SR3j = myFitConfig.addChannel("cuts",["SR1L3j"],1,0.5,1.5)
        SRset = [SR3j]
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==1:
        SR3j = myFitConfig.addChannel("cuts",["SR1L3j"],1,0.5,1.5)
        SR5j = myFitConfig.addChannel("cuts",["SR1L5j"],1,0.5,1.5)
        #WR5j = myFitConfig.addChannel("cuts",["CRW5j"],1,0.5,1.5)
        WRel5j = myFitConfig.addChannel("cuts",["CRelW5j"],1,0.5,1.5)
        WRmu5j = myFitConfig.addChannel("cuts",["CRmuW5j"],1,0.5,1.5)
        #TR5j = myFitConfig.addChannel("cuts",["CRT5j"],1,0.5,1.5)
        TRel5j = myFitConfig.addChannel("cuts",["CRelT5j"],1,0.5,1.5)
        TRmu5j = myFitConfig.addChannel("cuts",["CRmuT5j"],1,0.5,1.5)
        #TR5j.hasBQCD = True
        TRel5j.hasBQCD = True
        TRmu5j.hasBQCD = True
        #BRset += [WR5j,TR5j]
        BRset += [WRel5j,WRmu5j,TRel5j,TRmu5j]
        #myFitConfig.setBkgConstrainChannels(WR5j)
        myFitConfig.setBkgConstrainChannels(WRel5j)
        myFitConfig.setBkgConstrainChannels(WRmu5j)
        #myFitConfig.setBkgConstrainChannels(TR5j)
        myFitConfig.setBkgConstrainChannels(TRel5j)
        myFitConfig.setBkgConstrainChannels(TRmu5j)
        SRset = [SR3j,SR5j]
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass
    elif chn==7:
        SR5j = myFitConfig.addChannel("cuts",["SR1L5j"],1,0.5,1.5)
        SRset = [SR5j]
        if myFitType==FitType.Exclusion:
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass
    elif chn==2:
        SR1a = myFitConfig.addChannel("cuts",["SR1L2Ba"],1,0.5,1.5)
        SR1a.hasBQCD = True
        SRset = [SR1a]
        if myFitType==FitType.Exclusion:
            SR1a.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==3:
        SR1b = myFitConfig.addChannel("cuts",["SR1L2Bc"],1,0.5,1.5)
        SR1b.hasBQCD = True
        SRset = [SR1b]
        if myFitType==FitType.Exclusion:
            SR1b.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==4:
        SR2a = myFitConfig.addChannel("cuts",["SR1L1Ba"],1,0.5,1.5)
        SR2a.hasBQCD = True
        SRset = [SR2a]
        if myFitType==FitType.Exclusion:
            SR2a.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==5:
        SR2b = myFitConfig.addChannel("cuts",["SR1L1Bc"],1,0.5,1.5)
        SR2b.hasBQCD = True
        SRset = [SR2b]
        if myFitType==FitType.Exclusion:
            SR2b.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==6:
        SR2l = myFitConfig.addChannel("cuts",["SR2L"],1,0.5,1.5)
        SR2l.hasBQCD = True
        SRset = [SR2l]
        if myFitType==FitType.Exclusion:
            SR2l.getSample(sigSampleName).addSystematic(isrmm)
        pass

    ## systematics (2/2)
    if includeSyst and dobtag:
        if chn==0 or chn==7:
            #SetupChannels([WR,TR], [bTagSyst9])
            SetupChannels([WRel,WRmu,TRel,TRmu], [bTagSyst9])
            pass
        if chn==1:
            #SetupChannels([WR,TR], [bTagSyst9])
            SetupChannels([WRel,WRmu,TRel,TRmu], [bTagSyst9])
            #SetupChannels([WR5j,TR5j], [bTagSyst9])
            SetupChannels([WRel5j,WRmu5j,TRel5j,TRmu5j], [bTagSyst9])
            pass
        elif chn==2:
            SetupChannels(BRset+SRset, [bTagSyst8]) # VR
            pass
        elif chn==3:
            SetupChannels(BRset+SRset, [bTagSyst8]) # VR
            pass
        elif chn==4:
            SetupChannels(BRset+SRset, [bTagSyst9]) # VR
            pass
        elif chn==5:
            SetupChannels(BRset+SRset, [bTagSyst9]) # VR
            pass
        elif chn==6:
            SetupChannels(BRset+SRset, [bTagSyst11]) # VR1,VR2
            pass

    if chn>=2 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            #WRbb.getSample(WSampleName).addSystematic(wbb)
            WRelbb.getSample(WSampleName).addSystematic(wbb)
            WRmubb.getSample(WSampleName).addSystematic(wbb)

    if chn>=2 and chn<=5:
        #TR.getSample(WSampleName).addSystematic(wbb)
        TRel.getSample(WSampleName).addSystematic(wbb)
        TRmu.getSample(WSampleName).addSystematic(wbb)
        for sr in SRset: 
             sr.getSample(WSampleName).addSystematic(wbb)

    ## W and top theory uncertainties in SRs 
    #for sr in SRset:
    #    if chn!=6:
    #        sr.getSample(WSampleName).addSystematic( Systematic("TheoW", configMgr.weights, 1.25, 0.75, "user", "userOverallSys") )
    #for sr in SRset:
    #    sr.getSample(TTbarSampleName).addSystematic( Systematic("TheoT", configMgr.weights, 1.25, 0.75, "user", "userOverallSys") )

    ## Fit type specifics
    if myFitType!=FitType.Background:
        myFitConfig.setSignalChannels(SRset)
    else:
        for sr in SRset:
            sr.doBlindingOverwrite = doBlinding
        myFitConfig.setValidationChannels(SRset)
    if myFitType==FitType.Discovery:
        #meas.addParamSetting("Lumi",True,1)
        for SR in SRset:
            SR.addDiscoverySamples([SR.name],[1.],[0.],[100.],[kMagenta])
            meas.addPOI("mu_%s" % SR.name)

    vSet = []
    if doValidation:
        if chn==0:
            #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            #v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            #v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
            #v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            #v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
            #v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            #v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
        
            #vel0  = myFitConfig.addChannel("cuts",["VRel3j1"],1,0.5,1.5)
            vel1  = myFitConfig.addChannel("cuts",["VRelT3j1"],1,0.5,1.5) ; vel1.hasBQCD = True
            vel2  = myFitConfig.addChannel("cuts",["VRelW3j1"],1,0.5,1.5)
            #vel3  = myFitConfig.addChannel("cuts",["VRel3j2"],1,0.5,1.5)
            vel4  = myFitConfig.addChannel("cuts",["VRelT3j2"],1,0.5,1.5) ; vel4.hasBQCD = True
            vel5  = myFitConfig.addChannel("cuts",["VRelW3j2"],1,0.5,1.5)
            #vel6  = myFitConfig.addChannel("cuts",["VRel3j3"],1,0.5,1.5)
            vel7  = myFitConfig.addChannel("cuts",["VRelT3j3"],1,0.5,1.5) ; vel7.hasBQCD = True
            vel8  = myFitConfig.addChannel("cuts",["VRelW3j3"],1,0.5,1.5)
                      
            #vmu0  = myFitConfig.addChannel("cuts",["VRmu3j1"],1,0.5,1.5)
            vmu1  = myFitConfig.addChannel("cuts",["VRmuT3j1"],1,0.5,1.5) ; vmu1.hasBQCD = True
            vmu2  = myFitConfig.addChannel("cuts",["VRmuW3j1"],1,0.5,1.5)
            #vmu3  = myFitConfig.addChannel("cuts",["VRmu3j2"],1,0.5,1.5)
            vmu4  = myFitConfig.addChannel("cuts",["VRmuT3j2"],1,0.5,1.5) ; vmu4.hasBQCD = True
            vmu5  = myFitConfig.addChannel("cuts",["VRmuW3j2"],1,0.5,1.5)
            #vmu6  = myFitConfig.addChannel("cuts",["VRmu3j3"],1,0.5,1.5)
            vmu7  = myFitConfig.addChannel("cuts",["VRmuT3j3"],1,0.5,1.5) ; vmu7.hasBQCD = True
            vmu8  = myFitConfig.addChannel("cuts",["VRmuW3j3"],1,0.5,1.5)

            #vSet = [v1,v2] + [v4,v5] + [v7,v8]
            vSet = [vel1,vel2] + [vel4,vel5] + [vel7,vel8] + [vmu1,vmu2] + [vmu4,vmu5] + [vmu7,vmu8]

            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==7:
            #v9  = myFitConfig.addChannel("cuts",["VR5j1"],1,0.5,1.5)
            #v10 = myFitConfig.addChannel("cuts",["VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True
            #v11 = myFitConfig.addChannel("cuts",["VRW5j1"],1,0.5,1.5)
            #v12 = myFitConfig.addChannel("cuts",["VR5j2"],1,0.5,1.5)
            #v13 = myFitConfig.addChannel("cuts",["VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            #v14 = myFitConfig.addChannel("cuts",["VRW5j2"],1,0.5,1.5)
            #v15 = myFitConfig.addChannel("cuts",["VR5j3"],1,0.5,1.5)
            #v16 = myFitConfig.addChannel("cuts",["VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            #v17 = myFitConfig.addChannel("cuts",["VRW5j3"],1,0.5,1.5)

            #vel9  = myFitConfig.addChannel("cuts",["VRel5j1"],1,0.5,1.5)
            vel10 = myFitConfig.addChannel("cuts",["VRelT5j1"],1,0.5,1.5) ; vel10.hasBQCD = True
            vel11 = myFitConfig.addChannel("cuts",["VRelW5j1"],1,0.5,1.5)
            #vel12 = myFitConfig.addChannel("cuts",["VRel5j2"],1,0.5,1.5)
            vel13 = myFitConfig.addChannel("cuts",["VRelT5j2"],1,0.5,1.5) ; vel13.hasBQCD = True
            vel14 = myFitConfig.addChannel("cuts",["VRelW5j2"],1,0.5,1.5)
            #vel15 = myFitConfig.addChannel("cuts",["VRel5j3"],1,0.5,1.5)
            vel16 = myFitConfig.addChannel("cuts",["VRelT5j3"],1,0.5,1.5) ; vel16.hasBQCD = True
            vel17 = myFitConfig.addChannel("cuts",["VRelW5j3"],1,0.5,1.5)

            #vmu9  = myFitConfig.addChannel("cuts",["VRmu5j1"],1,0.5,1.5)
            vmu10 = myFitConfig.addChannel("cuts",["VRmuT5j1"],1,0.5,1.5) ; vmu10.hasBQCD = True
            vmu11 = myFitConfig.addChannel("cuts",["VRmuW5j1"],1,0.5,1.5)
            #vmu12 = myFitConfig.addChannel("cuts",["VRmu5j2"],1,0.5,1.5)
            vmu13 = myFitConfig.addChannel("cuts",["VRmuT5j2"],1,0.5,1.5) ; vmu13.hasBQCD = True
            vmu14 = myFitConfig.addChannel("cuts",["VRmuW5j2"],1,0.5,1.5)
            #vmu15 = myFitConfig.addChannel("cuts",["VRmu5j3"],1,0.5,1.5)
            vmu16 = myFitConfig.addChannel("cuts",["VRmuT5j3"],1,0.5,1.5) ; vmu16.hasBQCD = True
            vmu17 = myFitConfig.addChannel("cuts",["VRmuW5j3"],1,0.5,1.5)

            vSet = [vel10,vel13,vel16] + [vel11,vel14,vel17]  + [vmu10,vmu13,vmu16] + [vmu11,vmu14,vmu17] 

            #vSet = [v10,v13,v16] + [v11,v14,v17] 
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==1:
            #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            #v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            #v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
            #v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            #v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
            #v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            #v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
            #v9  = myFitConfig.addChannel("cuts",["VR5j1"],1,0.5,1.5)
            #v10 = myFitConfig.addChannel("cuts",["VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True 
            #v11 = myFitConfig.addChannel("cuts",["VRW5j1"],1,0.5,1.5)
            #v12 = myFitConfig.addChannel("cuts",["VR5j2"],1,0.5,1.5)
            #v13 = myFitConfig.addChannel("cuts",["VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            #v14 = myFitConfig.addChannel("cuts",["VRW5j2"],1,0.5,1.5)
            #v15 = myFitConfig.addChannel("cuts",["VR5j3"],1,0.5,1.5)
            #v16 = myFitConfig.addChannel("cuts",["VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            #v17 = myFitConfig.addChannel("cuts",["VRW5j3"],1,0.5,1.5)


            #vel0  = myFitConfig.addChannel("cuts",["VRel3j1"],1,0.5,1.5)
            vel1  = myFitConfig.addChannel("cuts",["VRelT3j1"],1,0.5,1.5) ; vel1.hasBQCD = True
            vel2  = myFitConfig.addChannel("cuts",["VRelW3j1"],1,0.5,1.5)
            #vel3  = myFitConfig.addChannel("cuts",["VRel3j2"],1,0.5,1.5)
            vel4  = myFitConfig.addChannel("cuts",["VRelT3j2"],1,0.5,1.5) ; vel4.hasBQCD = True
            vel5  = myFitConfig.addChannel("cuts",["VRelW3j2"],1,0.5,1.5)
            #vel6  = myFitConfig.addChannel("cuts",["VRel3j3"],1,0.5,1.5)
            vel7  = myFitConfig.addChannel("cuts",["VRelT3j3"],1,0.5,1.5) ; vel7.hasBQCD = True
            vel8  = myFitConfig.addChannel("cuts",["VRelW3j3"],1,0.5,1.5)
            #vel9  = myFitConfig.addChannel("cuts",["VRel5j1"],1,0.5,1.5)
            vel10 = myFitConfig.addChannel("cuts",["VRelT5j1"],1,0.5,1.5) ; vel10.hasBQCD = True 
            vel11 = myFitConfig.addChannel("cuts",["VRelW5j1"],1,0.5,1.5)
            #vel12 = myFitConfig.addChannel("cuts",["VRel5j2"],1,0.5,1.5)
            vel13 = myFitConfig.addChannel("cuts",["VRelT5j2"],1,0.5,1.5) ; vel13.hasBQCD = True
            vel14 = myFitConfig.addChannel("cuts",["VRelW5j2"],1,0.5,1.5)
            #vel15 = myFitConfig.addChannel("cuts",["VRel5j3"],1,0.5,1.5)
            vel16 = myFitConfig.addChannel("cuts",["VRelT5j3"],1,0.5,1.5) ; vel16.hasBQCD = True
            vel17 = myFitConfig.addChannel("cuts",["VRelW5j3"],1,0.5,1.5)


            #vmu0  = myFitConfig.addChannel("cuts",["VRmu3j1"],1,0.5,1.5)
            vmu1  = myFitConfig.addChannel("cuts",["VRmuT3j1"],1,0.5,1.5) ; vmu1.hasBQCD = True
            vmu2  = myFitConfig.addChannel("cuts",["VRmuW3j1"],1,0.5,1.5)
            #vmu3  = myFitConfig.addChannel("cuts",["VRmu3j2"],1,0.5,1.5)
            vmu4  = myFitConfig.addChannel("cuts",["VRmuT3j2"],1,0.5,1.5) ; vmu4.hasBQCD = True
            vmu5  = myFitConfig.addChannel("cuts",["VRmuW3j2"],1,0.5,1.5)
            #vmu6  = myFitConfig.addChannel("cuts",["VRmu3j3"],1,0.5,1.5)
            vmu7  = myFitConfig.addChannel("cuts",["VRmuT3j3"],1,0.5,1.5) ; vmu7.hasBQCD = True
            vmu8  = myFitConfig.addChannel("cuts",["VRmuW3j3"],1,0.5,1.5)
            #vmu9  = myFitConfig.addChannel("cuts",["VRmu5j1"],1,0.5,1.5)
            vmu10 = myFitConfig.addChannel("cuts",["VRmuT5j1"],1,0.5,1.5) ; vmu10.hasBQCD = True 
            vmu11 = myFitConfig.addChannel("cuts",["VRmuW5j1"],1,0.5,1.5)
            #vmu12 = myFitConfig.addChannel("cuts",["VRmu5j2"],1,0.5,1.5)
            vmu13 = myFitConfig.addChannel("cuts",["VRmuT5j2"],1,0.5,1.5) ; vmu13.hasBQCD = True
            vmu14 = myFitConfig.addChannel("cuts",["VRmuW5j2"],1,0.5,1.5)
            #vmu15 = myFitConfig.addChannel("cuts",["VRmu5j3"],1,0.5,1.5)
            vmu16 = myFitConfig.addChannel("cuts",["VRmuT5j3"],1,0.5,1.5) ; vmu16.hasBQCD = True
            vmu17 = myFitConfig.addChannel("cuts",["VRmuW5j3"],1,0.5,1.5)

            vSet = [vmu1,vmu4,vmu7,vmu10,vmu13,vmu16] + [vmu2,vmu5,vmu8,vmu11,vmu14,vmu17] + [vel1,vel4,vel7,vel10,vel13,vel16] + [vel2,vel5,vel8,vel11,vel14,vel17]

            #vSet = [v1,v4,v7,v10,v13,v16] + [v2,v5,v8,v11,v14,v17] #+ [v0,v3,v6,v9,v12,v15]
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==2:
            #v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            #v0.hasBQCD = True
            #v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            #v1.hasBQCD = True

            vel0  = myFitConfig.addChannel("cuts",["VRel1"],1,0.5,1.5)
            vel0.hasBQCD = True
            vel1  = myFitConfig.addChannel("cuts",["VRel2"],1,0.5,1.5)
            vel1.hasBQCD = True

            vmu0  = myFitConfig.addChannel("cuts",["VRmu1"],1,0.5,1.5)
            vmu0.hasBQCD = True
            vmu1  = myFitConfig.addChannel("cuts",["VRmu2"],1,0.5,1.5)
            vmu1.hasBQCD = True

            vSet = [vel0,vel1]  +   [vmu0,vmu1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==3:
            #v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            #v0.hasBQCD = True
            #v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            #v1.hasBQCD = True

            vel0  = myFitConfig.addChannel("cuts",["VRel1"],1,0.5,1.5)
            vel0.hasBQCD = True
            vel1  = myFitConfig.addChannel("cuts",["VRel2"],1,0.5,1.5)
            vel1.hasBQCD = True

            vmu0  = myFitConfig.addChannel("cuts",["VRmu1"],1,0.5,1.5)
            vmu0.hasBQCD = True
            vmu1  = myFitConfig.addChannel("cuts",["VRmu2"],1,0.5,1.5)
            vmu1.hasBQCD = True

            vSet = [vel0,vel1]  +   [vmu0,vmu1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==4:
            #v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            #v0.hasBQCD = True
            #v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            #v1.hasBQCD = True

            vel0  = myFitConfig.addChannel("cuts",["VRel1"],1,0.5,1.5)
            vel0.hasBQCD = True
            vel1  = myFitConfig.addChannel("cuts",["VRel2"],1,0.5,1.5)
            vel1.hasBQCD = True

            vmu0  = myFitConfig.addChannel("cuts",["VRmu1"],1,0.5,1.5)
            vmu0.hasBQCD = True
            vmu1  = myFitConfig.addChannel("cuts",["VRmu2"],1,0.5,1.5)
            vmu1.hasBQCD = True

            vSet = [vel0,vel1]  +   [vmu0,vmu1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==5:
            #v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            #v0.hasBQCD = True
            #v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            #v1.hasBQCD = True

            vel0  = myFitConfig.addChannel("cuts",["VRel1"],1,0.5,1.5)
            vel0.hasBQCD = True
            vel1  = myFitConfig.addChannel("cuts",["VRel2"],1,0.5,1.5)
            vel1.hasBQCD = True

            vmu0  = myFitConfig.addChannel("cuts",["VRmu1"],1,0.5,1.5)
            vmu0.hasBQCD = True
            vmu1  = myFitConfig.addChannel("cuts",["VRmu2"],1,0.5,1.5)
            vmu1.hasBQCD = True

            vSet = [vel0,vel1]  +   [vmu0,vmu1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==6:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VR3"],1,0.5,1.5)
            v2.hasBQCD = True
            vSet = [v0,v1,v2]
            myFitConfig.setValidationChannels(vSet)
            pass

    if doValidation and includeSyst and dobtag:
        if chn==0 or chn==7:
            SetupChannels(vSet, [bTagSyst9])
            pass
        if chn==1:
            SetupChannels(vSet, [bTagSyst9])
            pass
        elif chn==2:
            SetupChannels(vSet, [bTagSyst8]) # VR
            pass
        elif chn==3:
            SetupChannels(vSet, [bTagSyst8]) # VR
            pass
        elif chn==4:
            SetupChannels(vSet, [bTagSyst9]) # VR
            pass
        elif chn==5:
            SetupChannels(vSet, [bTagSyst9]) # VR
            pass
        elif chn==6:
            SetupChannels(vSet, [bTagSyst11]) # VR1,VR2
            pass

    # Generator Systematics for each sample,channel
    log.info("** Generator Systematics **")
    AllChannels = BRset+SRset+vSet
    for tgt,syst in generatorSyst:
        tgtsample = tgt[0]
        tgtchan = tgt[1]
        for chan in AllChannels:
            # if tgtchan=="All" or tgtchan==chan.name:
            if 'cuts_'+tgtchan == chan.name:
                chan.getSample(tgtsample).addSystematic(syst)
                log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))

