
################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
import ROOT
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import sqrt
import pickle
from os import sys
from math import exp

from logger import Logger
log = Logger('SoftLepton')

# ********************************************************************* #
# some useful helper functions
# ********************************************************************* #

def myreplace(l1,l2,element):
    idx=l1.index(element)
    if idx>=0:
        return l1[:idx] + l2 + l1[idx+1:]
    else:
        print "WARNING idx negative: %d" % idx
        return l1

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupSamples(samples,systList):
    for sample in samples:
        for syst in systList:
            sample.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #

#print sys.argv

# ********************************************************************* #
#                              Analysis parameters
# ********************************************************************* #

if not 'useStat' in dir():
    useStat=True
if not 'chn' in dir():
    chn=1                   # analysis channel 0=A,1=B,..
if not 'grid' in dir():
    grid="" # "SU"               # only grid implemented up to now
if not 'gridspec' in dir():
    gridspec="_" # "SU"               # only grid implemented up to now
if not 'suffix' in dir():
    suffix="_NoSys"
if not 'sigSamples' in dir():
    sigSamples=None #["1000_250"]
if not 'anaName' in dir():
    anaName = "s1L_SRs1L"
if not 'includeSyst' in dir():
    includeSyst = True
if not 'dobtag' in dir():
    dobtag = True
if not 'doBlinding' in dir():
    doBlinding = False
if not 'doSystRemap' in dir():
    doSystRemap = False

doBlinding = False
configMgr.fixSigXSec=True
doShapeFit = False
useSRasCR = False
doMPJES = True #False #
doSmallJESParameters = False
if chn >=2 and chn <=5:
    doSmallJESParameters = True

doELMU = False # set for discovery fit

if myFitType==FitType.Background:
    doELMU = False #True 
elif myFitType==FitType.Discovery and chn!=8 and (chn<2 and chn>6):
    doELMU = True 
elif myFitType==FitType.Exclusion: 
    doELMU = False  # not e/mu seperated for exclusion fit
    pass

# ********************************************************************* #
#                              Options
# ********************************************************************* #

# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 

    if "s1L"  in pickedSRs[0]: 
        prefix = "s1L_"
    else: 
        prefix = ""
    
    if "Shape" in pickedSRs[0]:
        shapefix = "_Shape"
        doShapeFit = True
        print "INFO: Shape fit enabled"
    else:
        shapefix = ""
        doShapeFit = False
        
    if pickedSRs[0]==prefix+"SR3j"+shapefix:
        chn=0
    elif pickedSRs[0]==prefix+"SR5j"+shapefix:
        chn=7
    elif pickedSRs[0]==prefix+"SRs1L"+shapefix:
        chn=1
    elif pickedSRs[0]==prefix+"SR3jIncl"+shapefix:
        chn=9
    elif pickedSRs[0]==prefix+"SR1a"+shapefix:
        chn=2
    elif pickedSRs[0]==prefix+"SR1b"+shapefix:
        chn=3
    elif pickedSRs[0]==prefix+"SR2a"+shapefix:
        chn=4
    elif pickedSRs[0]==prefix+"SR2b"+shapefix:
        chn=5
    elif pickedSRs[0]==prefix+"SRs2La"+shapefix:
        chn=6
    elif pickedSRs[0]==prefix+"SRs2Lb"+shapefix:
        chn=8
    else:
        print "WARNING: analysis not defined"
        sys.exit()
    anaName=pickedSRs[0]
    # set the grid
    if len(pickedSRs) >= 2: 
        grid = pickedSRs[1] 
        pass
    if len(pickedSRs) >= 3: 
        gridspec = '_'+pickedSRs[2]+'_'
        pass

if chn>9 or chn<0:
    print "ERROR analysis not defined!!!"
    print chn
    print pickedSRs
    sys.exit()

allpoints=['500_400_60']

if chn==0 or chn==1 or chn==7 or chn==9:
    # traditional soft 1-lepton analysis
    if grid=="SM_GG1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
    elif grid=="SM_SS1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_1190_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905','300_98_60','300_132_60','400_145_60','400_213_60']
        pass
elif chn>=2 and chn<=5:
    # stop soft 1-lepton analysis
    if grid=="StopBCharDeg" and gridspec=="_5gev_": # for conf-note
        allpoints=['150_105_100', '150_140_135', '200_105_100', '200_155_150', '200_190_185', '250_105_100', '250_155_150', '250_205_200', '250_240_235', '300_105_100', '300_155_150', '300_205_200', '300_255_250', '300_290_285', '350_155_150', '350_205_200', '350_255_250', '350_305_300', '350_340_335', '400_105_100', '400_205_200', '400_255_250', '400_305_300', '400_355_350', '400_390_385', '450_155_150', '450_255_250', '450_305_300', '450_355_350', '450_405_400', '450_440_435', '500_105_100', '500_205_200', '500_255_250', '500_305_300', '500_355_350', '500_405_400', '500_455_450', '500_490_485', '550_105_100', '550_155_150', '550_205_200', '550_255_250', '550_305_300', '550_355_350', '550_405_400', '550_455_450', '600_105_100', '600_155_150', '600_205_200', '600_255_250', '600_305_300', '600_355_350', '600_405_400', '600_455_450', '650_105_100', '650_155_150', '650_205_200', '650_255_250', '650_305_300', '650_355_350', '650_405_400', '650_455_450', '700_105_100', '700_155_150', '700_205_200', '700_255_250', '700_305_300', '700_355_350', '700_405_400', '700_455_450', '750_105_100', '750_155_150', '750_205_200', '750_255_250', '750_305_300', '750_355_350', '750_405_400', '750_455_450', '800_105_100', '800_155_150', '800_205_200', '800_255_250', '800_305_300', '800_355_350', '800_405_400', '800_455_450']
    elif grid=="StopBCharDeg" and gridspec=="_20gev_":  # for conf-note
        allpoints=['150_140_120', '150_95_75', '200_120_100', '200_170_150', '200_190_170', '200_95_75', '250_120_100', '250_170_150', '250_220_200', '250_240_220', '250_95_75', '300_120_100', '300_170_150', '300_220_200', '300_270_250', '300_290_270', '350_170_150', '350_220_200', '350_270_250', '350_320_300', '350_340_320', '350_95_75', '400_120_100', '400_220_200', '400_270_250', '400_320_300', '400_370_350', '400_390_370', '450_170_150', '450_220_200', '450_270_250', '450_320_300', '450_370_350', '450_420_400', '450_440_420', '450_95_75', '500_120_100', '500_220_200', '500_270_250', '500_320_300', '500_370_350', '500_420_400', '500_470_450', '500_490_470', '550_120_100', '550_170_150', '550_220_200', '550_270_250', '550_320_300', '550_370_350', '550_420_400', '550_470_450', '550_95_75', '600_120_100', '600_170_150', '600_220_200', '600_270_250', '600_320_300', '600_370_350', '600_420_400', '600_470_450', '600_95_75', '650_120_100', '650_170_150', '650_220_200', '650_270_250', '650_320_300', '650_370_350', '650_420_400', '650_470_450', '650_95_75', '700_120_100', '700_170_150', '700_220_200', '700_270_250', '700_320_300', '700_370_350', '700_420_400', '700_470_450', '700_95_75', '750_120_100', '750_170_150', '750_220_200', '750_270_250', '750_320_300', '750_370_350', '750_420_400', '750_470_450', '750_95_75', '800_120_100', '800_170_150', '800_220_200', '800_270_250', '800_320_300', '800_370_350', '800_420_400', '800_470_450', '800_95_75']
    elif grid=="StopBCharDeg" and gridspec=="_t150_": #fixed mass 1lep filter                                  
        allpoints=['150_106_1','150_100_75','150_106_60','150_120_25','150_100_90','150_106_95','150_100_25','150_120_50','150_120_75','150_120_110']
    elif grid=="StopBCharDeg" and gridspec=="_t300_": #fixed mass 1lep filter 
        allpoints=['300_155_150','300_205_200','300_255_250','300_290_285','300_100_90','300_150_140','300_200_190','300_250_240','300_120_100','300_170_150','300_195_175','300_220_200','300_245_225','300_270_250','300_290_270','300_150_100','300_200_150','300_250_200','300_150_50','300_250_150','300_200_50','300_250_100','300_250_50']
    elif grid=="StopBCharDeg" and gridspec=="_c150_": #fixed mass 1lep filter                                                                
        allpoints=['160_150_25','500_150_1','160_150_1','600_150_1','700_150_1','700_150_25','700_150_50','700_150_100','800_150_1','800_150_25','800_150_50','160_150_50','160_150_75','800_150_100','160_150_140','350_150_75','160_150_100','200_150_25','350_150_50','350_150_100','200_150_50','200_150_100','200_150_140','250_150_25','250_150_50','250_150_100','250_150_140','300_150_25','400_150_25','400_150_50','400_150_100','400_150_140','500_150_25','500_150_50','500_150_100','500_150_140','600_150_25','600_150_50','600_150_100','600_150_140']
    elif grid=="StopBCharDeg" and gridspec=="_c106_": #fixed mass 1lep filter                                                                                                       
        allpoints=['130_106_95','150_106_1','150_106_60','170_106_1','190_106_60','150_106_95','170_106_60','170_106_95','170_106_35','190_106_35','190_106_75','170_106_75','190_106_95','210_106_1','190_106_1','210_106_35','210_106_60','210_106_75','210_106_95','230_106_1','230_106_35','230_106_60','230_106_75','230_106_95','250_106_1','250_106_35','250_106_60','250_106_75','250_106_95','300_106_1','300_106_35','300_106_60','300_106_75','300_106_95','350_106_1','350_106_35','350_106_60','350_106_75','350_106_95','400_106_1','400_106_35','400_106_60','400_106_75','400_106_95','500_106_1','500_106_35','500_106_60','500_106_75','500_106_95','600_106_1','600_106_35','600_106_60','600_106_75','600_106_95','700_106_1','700_106_35','700_106_60','700_106_75','700_106_95']
    elif grid=="StopBCharDeg" and gridspec=="_0lept300_": #fixed mass 0lep filter                                                                                                   
        allpoints=['300_100_1','300_100_50','300_200_100','300_200_150','300_250_150','300_250_200','300_290_200']
    elif grid=="StopBCharDeg" and gridspec=="_0lepc150_": #fixed mass 0lep filter                                                                                                                  
        allpoints=['160_150_50','200_150_50','250_150_50','400_150_50','300_150_50','350_150_50','500_150_50','600_150_50','700_150_50','800_150_50','160_150_100','200_150_100','250_150_100','300_150_100','350_150_100','400_150_100','500_150_100','600_150_100','700_150_100','800_150_100']
    elif grid=="StopBCharDeg" and gridspec=="_combt300_": #fixed mass 01lep filter                                                                                                                
        allpoints=['300_200_150','300_250_150','300_250_200']
    elif grid=="StopBCharDeg" and gridspec=="_combc150_": #fixed mass 01lep filter                                                                                                                 
        allpoints=['160_150_50','200_150_50','250_150_50','400_150_50','350_150_50','500_150_50','600_150_50','700_150_50','800_150_50','160_150_100','200_150_100','250_150_100','350_150_100','400_150_100','500_150_100','600_150_100','700_150_100','800_150_100']

    pass
elif chn==6 or chn==8:
    # ued soft 2-lepton analysis
    if grid=="mUED2Lfilter":
        allpoints=['1000_10', '1000_3', '1000_40', '1100_10', '1100_3', '1100_40', '1200_10', '1200_3', '1200_40', '1300_10', '1300_3', '1300_40', '700_10', '700_3', '700_40', '800_10', '800_3', '800_40', '900_10', '900_3', '900_40','1000_5', '1000_2', '1000_20', '1100_5', '1100_2', '1100_20',  '700_5', '700_2', '700_20', '800_5', '800_2', '800_20', '900_5', '900_2', '900_20']
    elif grid=="SM_GG2CNsl":
#        allpoints=['287_275_268_262','317_305_298_292','345_305_285_265','425_305_245_185','505_305_205_105','397_385_378_372','425_385_365_345','465_385_345_305','505_385_325_265','585_385_285_185','665_385_245_105','477_465_458_452','505_465_445_425','545_465_425_385','585_465_405_345','625_465_385_305','665_465_365_265','745_465_325_185','825_465_285_105','557_545_538_532','585_545_525_505','625_545_505_465','665_545_485_425','705_545_465_385','745_545_445_345','825_545_405_265','865_545_385_225','905_545_365_185','945_545_345_145','985_545_325_105','1025_545_305_65','1065_545_285_25','637_625_618_612','665_625_605_585','705_625_585_545','745_625_565_505','785_625_545_465','825_625_525_425','905_625_485_345','945_625_465_305','985_625_445_265','1025_625_425_225','1065_625_405_185','1105_625_385_145','1145_625_365_105','1185_625_345_65','1225_625_325_25','717_705_698_692','745_705_685_665','825_705_645_585','865_705_625_545','905_705_605_505','945_705_585_465','985_705_565_425','1025_705_545_385','1065_705_525_345','1105_705_505_305','1145_705_485_265','1185_705_465_225','1225_705_445_185','1265_705_425_145','1305_705_405_105','1345_705_385_65','1385_705_365_25','797_785_778_772','825_785_765_745','985_785_685_585','1025_785_665_545','1065_785_645_505','1105_785_625_465','1145_785_605_425','1185_785_585_385','1225_785_565_345','1265_785_545_305','1305_785_525_265','1345_785_505_225','1385_785_485_185','1425_785_465_145','1465_785_445_105','985_865_805_745','1065_865_765_665','1145_865_725_585','1185_865_705_545','1225_865_685_505','1265_865_665_465','1305_865_645_425','1345_865_625_385','1385_865_605_345','1425_865_585_305','1465_865_565_265','1105_945_865_785','1185_945_825_705','1265_945_785_625','1305_945_765_585','1345_945_745_545','1385_945_725_505','1425_945_705_465','1465_945_685_425','1465_1025_805_585']
        allpoints=['287_275_268','317_305_298','345_305_285','425_305_245','505_305_205','397_385_378','425_385_365','465_385_345','505_385_325','585_385_285','665_385_245','477_465_458','505_465_445','545_465_425','585_465_405','625_465_385','665_465_365','745_465_325','825_465_285','557_545_538','585_545_525','625_545_505','665_545_485','705_545_465','745_545_445','825_545_405','865_545_385','905_545_365','945_545_345','985_545_325','1025_545_305','1065_545_285','637_625_618','665_625_605','705_625_585','745_625_565','785_625_545','825_625_525','905_625_485','945_625_465','985_625_445','1025_625_425','1065_625_405','1105_625_385','1145_625_365','1185_625_345','1225_625_325','717_705_698','745_705_685','825_705_645','865_705_625','905_705_605','945_705_585','985_705_565','1025_705_545','1065_705_525','1105_705_505','1145_705_485','1185_705_465','1225_705_445','1265_705_425','1305_705_405','1345_705_385','1385_705_365','797_785_778','825_785_765','985_785_685','1025_785_665','1065_785_645','1105_785_625','1145_785_605','1185_785_585','1225_785_565','1265_785_545','1305_785_525','1345_785_505','1385_785_485','1425_785_465','1465_785_445','985_865_805','1065_865_765','1145_865_725','1185_865_705','1225_865_685','1265_865_665','1305_865_645','1345_865_625','1385_865_605','1425_865_585','1465_865_565','1105_945_865','1185_945_825','1265_945_785','1305_945_765','1345_945_745','1385_945_725','1425_945_705','1465_945_685','1465_1025_805']
    elif grid=="SM_GG2WWZZ":
        allpoints=['287_275_262','317_305_292','345_305_265','397_385_372','425_385_345','465_385_305','505_385_265','477_465_452','505_465_425','545_465_385','585_465_345','625_465_305','665_465_265','557_545_532','585_545_505','705_545_385','745_545_345','785_545_305','825_545_265','865_545_225','905_545_185','945_545_145','985_545_105','1025_545_65','665_625_585','705_625_545','745_625_505','785_625_465','825_625_425','865_625_385','905_625_345','945_625_305','985_625_265','1025_625_225','1065_625_185','1105_625_145','1185_625_65','1225_625_25','717_705_692','745_705_665','825_705_585','865_705_545','905_705_505','945_705_465','985_705_425','1025_705_385','1065_705_345','1105_705_305','1145_705_265','1185_705_225','1225_705_185','1265_705_145','1305_705_105','1345_705_65','1385_705_25','797_785_772','825_785_745','905_785_665','985_785_585','1025_785_545','1065_785_505','1105_785_465','1145_785_425','1185_785_385','1225_785_345','1345_785_225','1385_785_185','1465_785_105','985_865_745','1065_865_665','1145_865_585','1185_865_545','1225_865_505','1265_865_465','1305_865_425','1345_865_385','1385_865_345','1425_865_305','1465_865_265','1105_945_785','1185_945_705','1265_945_625','1305_945_585','1385_945_505','1425_945_465','1465_945_425','1465_1025_585']

    pass

# No input signal for discovery and bkg fit
if myFitType==FitType.Discovery:
    allpoints=["Discovery"]
    grid=""
    pass
elif myFitType==FitType.Background:
    allpoints=["Background"]
    grid=""
    pass

# sigSamples is set by the "-g" HistFitter option. Overwrites allpoints, used below.
try:
    sigSamples
except NameError:
    sigSamples = None
    
if sigSamples!=None:
    allpoints=sigSamples

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

#configMgr.doHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStatType=3
configMgr.nPoints=10

# ********************************************************************* #
# Main part - now we start to build the data model
# ********************************************************************* #

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.3   #####20.490 # 14.3 #21.0
configMgr.setLumiUnits("fb-1")
 
if chn>=2 and chn<=5:
    inputDirSlimOld_p1328 = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_1_5_ebOR_slim/"
    inputDirSlim_p1328    = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_bugfix_ebOR/" #### this is a BJes/FlavorCompUncert/FlavorResponseUncert bug-fixed sample
    inputDirSig_p1328     = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_1_5_ebOR/"
    inputDirSyst_p1328    = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_1_5_ebOR/"
    inputDirData_p1328    = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_bugfix_ebOR/" #### this is a JetMETCleaning bug-fixed sample 
    inputDirQCD_p1328     = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_qcdEZ-ebOR/"
    #inputDirQCD_p1328     = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_1_5_ebOR/"
    inputDirSherpaDibosonsMassiveBC = inputDirSlim_p1328

else:
    inputDirSlim_p1328    = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_bugfix_test/" #### this is a BJes/FlavorCompUncert/FlavorResponseUncert bug-fixed sample
    inputDirSlimTest_p1328= "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_bugfix_test/"
    inputDirSig_p1328     = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_1_5/" #"root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/softLeptonInputTrees/v12_1_5/"
    inputDirSyst_p1328    = "root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/softLeptonInputTrees/v12_1_5/"
    inputDirData_p1328    = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_bugfix_test/" #### this is a JetMETCleaning bug-fixed sample 
    inputDirQCD_p1328     = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_qcdEZ/"
    #inputDirQCD_p1328     = "root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/softLeptonInputTrees/v12_1_5/"
    inputDirSherpaDibosonsMassiveBC = inputDirSlim_p1328
    inputDirSlimOld_p1328    = "root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/softLeptonInputTrees/v12_1_5_slim/"

inputDirQCD2MU_p1328  = "root://eosatlas//eos/atlas/atlaslocalgroupdisk/susy/softLeptonInputTrees/v12_1_7/"
#inputDirQCD2MUsys_p1328  = "root://eosatlas//eos/atlas/user/l/lily/v12QCDsys_30p/DEBUG/"
inputDirQCD2MUsys_p1328  = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v12_qcdEZ_30pSyst/"

#Split bdgFiles per channel
sigFiles = []
if myFitType==FitType.Exclusion: 
    if (chn>=0 and chn<=5) or chn==7 or chn==9: # soft 1-lepton
        sigFiles += [inputDirSig_p1328+"sigtree_Soft1Lep_"+grid+".root"]
    elif chn==6 or chn==8:          # soft 2-lepton
        sigFiles += [inputDirSig_p1328+"sigtree_Soft2Lep_"+grid+".root"]
    if len(sigFiles)==0:
        print "WARNING: signal grid files not defined"
        sys.exit()
    pass

# Set the files to read from
if (chn>=0 and chn<=5) or chn==7 or chn==9: # soft 1-lepton
    if True:
        dataFiles              = [inputDirData_p1328+"Soft1Lep_Data.root"] 
        qcdFiles               = [inputDirQCD_p1328 +"Soft1Lep_QCD.root"]
        AlpgenDYFiles          = [inputDirSlim_p1328+"Soft1Lep_AlpgenDY.root"]#AlpgenDY_Soft1Lep.root"]
        #AlpgenWFiles           = [inputDirSlim_p1328+"AlpgenJimmyW_Soft1Lep.root"] 
        #AlpgenZFiles           = [inputDirSlim_p1328+"AlpgenZ_Soft1Lep.root"]
        PowhegDibosonsFiles    = [inputDirSlimOld_p1328+"Soft1Lep_PowhegDibosons.root"]#
        #SherpaDibosonsFiles    = [inputDirSlim_p1328+"Soft1Lep_SherpaDibosons.root"]
        SherpaDibosonsMassiveBCFiles    = [inputDirSherpaDibosonsMassiveBC+"Soft1Lep_SherpaDibosonsMassiveBC.root"]
        PowhegPythiaTTbarFiles = [inputDirSlim_p1328+"Soft1Lep_PowhegPythiaTTbar.root"]#PowhegPythiaTTbar_Soft1Lep.root"]
        PowhegJimmyTTbarFiles  = [inputDirSyst_p1328+"Soft1Lep_PowhegJimmyTTbar_NoSys.root"]#Soft1Lep_PowhegJimmyTTbar.root"]
        AlpgenTTbarFiles        = [inputDirSyst_p1328+"Soft1Lep_AlpgenTTbar_NoSys.root"]#Soft1Lep_PowhegJimmyTTbar.root"]
        SherpaWMassiveBCFiles  = [inputDirSlim_p1328+"Soft1Lep_SherpaWMassiveBC.root"]#SherpaWMassiveBC_Soft1Lep.root"]
        SherpaZMassiveBCFiles  = [inputDirSlim_p1328+"Soft1Lep_SherpaZMassiveBC.root"]#SherpaZMassiveBC_Soft1Lep.root"]
        SingleTopFiles         = [inputDirSlim_p1328+"Soft1Lep_SingleTop.root"]#SingleTop_Soft1Lep.root"]
        SingleTopSystFiles     = [inputDirSyst_p1328+"Soft1Lep_SingleTopSyst_NoSys.root"]#
        ttbarVFiles            = [inputDirSlim_p1328+"Soft1Lep_ttbarV.root"]#ttbarV_Soft1Lep.root"]
        AlpgenttbarVFiles      = [inputDirSyst_p1328+"Soft1Lep_ttbarVAlpgen_NoSys.root"]#Soft1Lep_ttbarVAlpgen.root"]
        pass
elif chn==6 or chn==8:          # soft 2-lepton
    dataFiles              = [inputDirData_p1328+"Soft2Lep_Data.root"]
    qcdFiles               = [inputDirQCD2MUsys_p1328 +"Soft2Lep_QCD.root"]
    AlpgenDYFiles          = [inputDirSlim_p1328+"Soft2Lep_AlpgenDY.root"]#AlpgenDY_Soft2Lep.root"]
    #AlpgenWFiles           = [inputDirSlim_p1328+"AlpgenJimmyW_Soft2Lep.root"] 
    #AlpgenZFiles           = [inputDirSlim_p1328+"AlpgenZ_Soft2Lep.root"]
    PowhegDibosonsFiles    = [inputDirSlim_p1328+"Soft2Lep_PowhegDibosons.root"]#
    #SherpaDibosonsFiles    = [inputDirSlim_p1328+"Soft2Lep_SherpaDibosons.root"]
    #PowhegPythiaTTbarFiles = [inputDirSlim_p1328+"Soft2Lep_PowhegPythiaTTbar.root"]#PowhegPythiaTTbar_Soft2Lep.root"]
    PowhegPythiaTTbarFiles = [inputDirSlimTest_p1328+"Soft2Lep_PowhegPythiaTTbar.root"]#PowhegPythiaTTbar_Soft2Lep.root"]
    PowhegJimmyTTbarFiles  = [inputDirSyst_p1328+"Soft2Lep_PowhegJimmyTTbar_NoSys.root"]#Soft2Lep_PowhegJimmyTTbar.root"]
    AlpgenTTbarFiles        = [inputDirSyst_p1328+"Soft2Lep_AlpgenTTbar_NoSys.root"]#Soft1Lep_PowhegJimmyTTbar.root"]
    SherpaWMassiveBCFiles  = [inputDirSlim_p1328+"Soft2Lep_SherpaWMassiveBC.root"]#SherpaWMassiveBC_Soft2Lep.root"]
    SherpaZMassiveBCFiles  = [inputDirSlim_p1328+"Soft2Lep_SherpaZMassiveBC.root"]#SherpaZMassiveBC_Soft2Lep.root"]
    SingleTopFiles         = [inputDirSlim_p1328+"Soft2Lep_SingleTopDiLept.root"]#SingleTopDiLept_Soft2Lep.root"]
    SingleTopSystFiles     = [inputDirSyst_p1328+"Soft2Lep_SingleTopSyst_NoSys.root"]#
    ttbarVFiles            = [inputDirSlim_p1328+"Soft2Lep_ttbarV.root"]#ttbarV_Soft2Lep.root"]
    AlpgenttbarVFiles      = [inputDirSyst_p1328+"Soft2Lep_ttbarVAlpgen_NoSys.root"]#Soft2Lep_ttbarVAlpgen.root"]
    pass


    
########################################
# Analysis description
########################################

## Lists of weights 

if (chn > 1 and chn < 6):
    ttbarWeight = "((1*(VecSumTTbarPt==-1)+(((1./1.01185 + 1./0.994193)/2.)  *(VecSumTTbarPt>=0 && VecSumTTbarPt<40) + ((1./1.09592 + 1./1.03448)/2.) *(VecSumTTbarPt>=40 && VecSumTTbarPt<170) + ((1./1.40728 + 1./1.31911)/2.)*(VecSumTTbarPt>=170 && VecSumTTbarPt<340) + ((1./1.79939 + 1./1.71078)/2.) *(VecSumTTbarPt>=340))))"
    weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight",ttbarWeight]     ######## turn off April-24 ,"SherpaWweight"]
else:
    weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]     ######## turn off April-24 ,"SherpaWweight"]



toreplace = ""
###  bTagWeight for new JVF criteria: please use bTagWeight[i+11]  ## @ OCT 2013
if chn==0 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[19]"]
    toreplace = "bTagWeight[19]"
    pass
elif chn==7 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[19]"]
    toreplace = "bTagWeight[19]"
    pass
elif chn==9 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[19]"]
    toreplace = "bTagWeight[19]"
    pass
elif chn==1 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[19]"]
    toreplace = "bTagWeight[19]"
    pass
elif chn==2 and dobtag:
    #weights += ["bTagWeight[7]"]
    bweights = weights + ["bTagWeight[18]"]
    toreplace = "bTagWeight[18]"
    pass
elif chn==3 and dobtag:
    #weights += ["bTagWeight[7]"]
    bweights = weights + ["bTagWeight[18]"]
    toreplace = "bTagWeight[18]"
    pass
elif chn==4 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[19]"]
    toreplace = "bTagWeight[19]"
    pass
elif chn==5 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[19]"]
    toreplace = "bTagWeight[19]"
    pass
elif (chn==6 or chn==8) and dobtag:
    #weights += ["bTagWeight[10]"]
    bweights = weights + ["bTagWeight[21]"]
    toreplace = "bTagWeight[21]"
    pass

##configMgr.weights = weights # bweights
configMgr.weights = bweights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights, "leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

###  bTagWeight for new JVF criteria: please use bTagWeight[i+11]  ## @ OCT 2013
if dobtag:
    bTagBHighWeights9 = replaceWeight(bweights,toreplace,"bTagWeightBUp[19]")
    bTagBLowWeights9  = replaceWeight(bweights,toreplace,"bTagWeightBDown[19]")
    bTagBHighWeights8 = replaceWeight(bweights,toreplace,"bTagWeightBUp[18]")
    bTagBLowWeights8  = replaceWeight(bweights,toreplace,"bTagWeightBDown[18]")
    bTagBHighWeights11 = replaceWeight(bweights,toreplace,"bTagWeightBUp[21]")
    bTagBLowWeights11  = replaceWeight(bweights,toreplace,"bTagWeightBDown[21]")
    bTagBHighWeights99 = replaceWeight(bweights,toreplace,"bTagWeightBUp[20]")
    bTagBLowWeights99  = replaceWeight(bweights,toreplace,"bTagWeightBDown[20]")



    bTagCHighWeights9 = replaceWeight(bweights,toreplace,"bTagWeightCUp[19]")
    bTagCLowWeights9  = replaceWeight(bweights,toreplace,"bTagWeightCDown[19]")
    bTagCHighWeights8 = replaceWeight(bweights,toreplace,"bTagWeightCUp[18]")
    bTagCLowWeights8  = replaceWeight(bweights,toreplace,"bTagWeightCDown[18]")
    bTagCHighWeights11 = replaceWeight(bweights,toreplace,"bTagWeightCUp[21]")
    bTagCLowWeights11  = replaceWeight(bweights,toreplace,"bTagWeightCDown[21]")
    bTagCHighWeights99 = replaceWeight(bweights,toreplace,"bTagWeightCUp[20]")
    bTagCLowWeights99  = replaceWeight(bweights,toreplace,"bTagWeightCDown[20]")


    bTagMHighWeights9 = replaceWeight(bweights,toreplace,"bTagWeightMUp[19]")
    bTagMLowWeights9  = replaceWeight(bweights,toreplace,"bTagWeightMDown[19]")
    bTagMHighWeights8 = replaceWeight(bweights,toreplace,"bTagWeightMUp[18]")
    bTagMLowWeights8  = replaceWeight(bweights,toreplace,"bTagWeightMDown[18]")
    bTagMHighWeights11 = replaceWeight(bweights,toreplace,"bTagWeightMUp[21]")
    bTagMLowWeights11  = replaceWeight(bweights,toreplace,"bTagWeightMDown[21]")
    bTagMHighWeights99 = replaceWeight(bweights,toreplace,"bTagWeightMUp[20]")
    bTagMLowWeights99  = replaceWeight(bweights,toreplace,"bTagWeightMDown[20]")

#-------------------------------------------------------------------------
# Blinding of SR
#-------------------------------------------------------------------------

configMgr.blindSR = doBlinding
configMgr.blindVR = False # doBlinding

#--------------------------------------------------------------------------
# List of systematics
#--------------------------------------------------------------------------

configMgr.nomName = "_NoSys"

## JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
#jesSignal = Systematic(prefix+"JSig",   "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallHistoSys")
jer       = Systematic(prefix+"JER",    "_NoSys", "_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym")


if doMPJES:
    jes_EffectiveNP_1 =                     Systematic(prefix+'EffectiveNP1',                      '_NoSys', '_EffectiveNP1up',                     '_EffectiveNP1down',                      'tree', 'overallNormHistoSys')
    jes_EffectiveNP_2 =                     Systematic(prefix+'EffectiveNP2',                      '_NoSys', '_EffectiveNP2up',                     '_EffectiveNP2down',                      'tree', 'overallNormHistoSys')
    jes_EtaIntercalibration_Modelling =     Systematic(prefix+'EtaIntercalibrationModelling',      '_NoSys', '_EtaIntercalibrationModellingup',     '_EtaIntercalibrationModellingdown',      'tree', 'overallNormHistoSys')
    jes_PileupRhoTopology =                 Systematic(prefix+'PileupRhoTopology',                 '_NoSys', '_PileupRhoTopologyup',                '_PileupRhoTopologydown',                 'tree', 'overallNormHistoSys')
    jes_FlavorCompUncert =                  Systematic(prefix+'FlavorCompUncert',                  '_NoSys', '_FlavorCompUncertup',                 '_FlavorCompUncertdown',                  'tree', 'overallNormHistoSys')
    jes_FlavorResponseUncert =              Systematic(prefix+'FlavorResponseUncert',              '_NoSys', '_FlavorResponseUncertup',             '_FlavorResponseUncertdown',              'tree', 'overallNormHistoSys')
    jes_BJes =                              Systematic(prefix+'BJes',                              '_NoSys', '_BJesup',                             '_BJesdown',                              'tree', 'overallNormHistoSys')
    jes_JVF =                               Systematic(prefix+'JVF',                               '_NoSys', '_JVFup',                              '_JVFdown',                               'tree', 'overallNormHistoSys')
    
    if doSmallJESParameters:
        jes_EffectiveNP_3 =                     Systematic(prefix+'EffectiveNP3',                      '_NoSys', '_EffectiveNP3up',                     '_EffectiveNP3down',                      'tree', 'overallNormHistoSys')
        jes_EffectiveNP_4 =                     Systematic(prefix+'EffectiveNP4',                      '_NoSys', '_EffectiveNP4up',                     '_EffectiveNP4down',                      'tree', 'overallNormHistoSys')
        jes_EffectiveNP_5 =                     Systematic(prefix+'EffectiveNP5',                      '_NoSys', '_EffectiveNP5up',                     '_EffectiveNP5down',                      'tree', 'overallNormHistoSys')
        jes_EffectiveNP_6 =                     Systematic(prefix+'EffectiveNP6',                      '_NoSys', '_EffectiveNP6up',                     '_EffectiveNP6down',                      'tree', 'overallNormHistoSys')
        
        jes_EtaIntercalibration_StatAndMethod = Systematic(prefix+'EtaIntercalibration_StatAndMethod', '_NoSys', '_EtaIntercalibrationStatAndMethodup', '_EtaIntercalibrationStatAndMethoddown',  'tree', 'overallNormHistoSys')
        jes_PileupOffsetTermMu =                Systematic(prefix+'PileupOffsetTermMu',                '_NoSys', '_PileupOffsetMuup',                   '_PileupOffsetMudown',                    'tree', 'overallNormHistoSys')
        jes_PileupPtTerm =                      Systematic(prefix+'PileupPtTerm',                      '_NoSys', '_PileupPtTermup',                     '_PileupPtTermdown',                      'tree', 'overallNormHistoSys')
        jes_PileupOffsetTermNPV =               Systematic(prefix+'PileupOffsetTermNPV',               '_NoSys', '_PileupOffsetNPVup',                  '_PileupOffsetNPVdown',                   'tree', 'overallNormHistoSys')
        jes_RelativeNonClosure_MCTYPE =         Systematic(prefix+'RelativeNonClosure_MCTYPE',         '_NoSys', '_RelativeNonClosureMCTYPEup',         '_RelativeNonClosureMCTYPEdown',          'tree', 'overallNormHistoSys')
        jes_SingleParticle_HighPt =             Systematic(prefix+'SingleParticle_HighPt',             '_NoSys', '_SingleParticleHighPtup',             '_SingleParticleHighPtdown',              'tree', 'overallNormHistoSys')

else:
    jes       = Systematic(prefix+"JES",    "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallNormHistoSys") # JES uncertainty - for low pt jets


## MET uncertainty
scalest   = Systematic(prefix+"SCALEST","_NoSys", "_SCALESTup","_SCALESTdown","tree","overallNormHistoSys")
resost    = Systematic( prefix+"RESOST","_NoSys", "_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym")

## pile-up
pileup = Systematic(prefix+"pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys")
#pileup.allowRemapOfSyst = doSystRemap

## b-tagging
if dobtag:
    bTagBSyst99 = Systematic(prefix+"BT",bweights,bTagBHighWeights99,bTagBLowWeights99,"weight","overallNormHistoSys")
    bTagBSyst9  = Systematic(prefix+"BT",bweights,bTagBHighWeights9,bTagBLowWeights9,"weight","overallNormHistoSys")
    bTagBSyst8  = Systematic(prefix+"BT",bweights,bTagBHighWeights8,bTagBLowWeights8,"weight","overallNormHistoSys")
    bTagBSyst11 = Systematic(prefix+"BT",bweights,bTagBHighWeights11,bTagBLowWeights11,"weight","overallNormHistoSys")

    bTagCSyst99 = Systematic(prefix+"CT",bweights,bTagCHighWeights99,bTagCLowWeights99,"weight","overallNormHistoSys")
    bTagCSyst9  = Systematic(prefix+"CT",bweights,bTagCHighWeights9,bTagCLowWeights9,"weight","overallNormHistoSys")
    bTagCSyst8  = Systematic(prefix+"CT",bweights,bTagCHighWeights8,bTagCLowWeights8,"weight","overallNormHistoSys")
    bTagCSyst11 = Systematic(prefix+"CT",bweights,bTagCHighWeights11,bTagCLowWeights11,"weight","overallNormHistoSys")

    bTagMSyst99 = Systematic(prefix+"MT",bweights,bTagMHighWeights99,bTagMLowWeights99,"weight","overallNormHistoSys")
    bTagMSyst9  = Systematic(prefix+"MT",bweights,bTagMHighWeights9,bTagMLowWeights9,"weight","overallNormHistoSys")
    bTagMSyst8  = Systematic(prefix+"MT",bweights,bTagMHighWeights8,bTagMLowWeights8,"weight","overallNormHistoSys")
    bTagMSyst11 = Systematic(prefix+"MT",bweights,bTagMHighWeights11,bTagMLowWeights11,"weight","overallNormHistoSys")

    bTagSyst99 = []
    bTagSyst99.append(bTagBSyst99)
    bTagSyst99.append(bTagCSyst99)
    bTagSyst99.append(bTagMSyst99)

    bTagSyst9 = []
    bTagSyst9.append(bTagBSyst9)
    bTagSyst9.append(bTagCSyst9)
    bTagSyst9.append(bTagMSyst9)

    bTagSyst8 = []
    bTagSyst8.append(bTagBSyst8)
    bTagSyst8.append(bTagCSyst8)
    bTagSyst8.append(bTagMSyst8)

    bTagSyst11 = []
    bTagSyst11.append(bTagBSyst11)
    bTagSyst11.append(bTagCSyst11)
    bTagSyst11.append(bTagMSyst11)

## Trigger efficiency --> should be taken from data/mc trigger study
#trEl = Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#trMu = Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#####trEff= Systematic("TE",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallNormHistoSys")
trEff = Systematic(prefix+"TE",configMgr.weights,1.05,0.95,"user","userOverallSys")

## MC theoretical uncertainties
## Signal XSec uncertainty as overallSys (pure yeild affect) 
xsecSig = Systematic(prefix+"SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys" )

## generator level uncertainties
#SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")

# qfacUpWeightW:qfacDownWeightW:ktfacUpWeightW:ktfacDownWeightW
qfacW  = Systematic(prefix+"qfacW", configMgr.weights,configMgr.weights+["qfacUpWeightW"], configMgr.weights+["qfacDownWeightW"], "weight","overallNormHistoSys")
ktfacW = Systematic(prefix+"ktfacW",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","overallNormHistoSys")
qfacZ  = Systematic(prefix+"qfacZ", configMgr.weights,configMgr.weights+["qfacUpWeightW"], configMgr.weights+["qfacDownWeightW"], "weight","histoSys")
ktfacZ = Systematic(prefix+"ktfacZ",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","histoSys")
wbb    = Systematic(prefix+"wbb", configMgr.weights,1.24 ,0.76, "user","userOverallSys")

## qcd stat/syst weights automatically picked up.

## Lepton weight uncertainty
#elEff = Systematic(prefix+"LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys") 
#muEff = Systematic(prefix+"LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")
#lepEff= Systematic(prefix+"LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallHistoSys")
lepEff= Systematic(prefix+"LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys")
#lepEff.allowRemapOfSyst = doSystRemap

## Electron energy scale uncertainty
##ees = Systematic(prefix+"LESel","_NoSys","_LESup","_LESdown","tree","overallSys")
'''
egzee = Systematic(prefix+"egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallNormHistoSys") ##
egmat = Systematic(prefix+"egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallNormHistoSys")
egps  = Systematic(prefix+"egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallNormHistoSys")##
eglow = Systematic(prefix+"eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallNormHistoSys")
egres = Systematic(prefix+"egres","_NoSys","_EGRESup","_EGRESdown","tree","overallNormHistoSys")##
'''
## Muon energy resolutions
#have to stay cos of Z
#merm = Systematic(prefix+"LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallNormHistoSys")
#meri = Systematic(prefix+"LRImu","_NoSys","_MIDup","_MIDdown","tree","overallNormHistoSys")

## muon energy scale systematics
#mes = Systematic(prefix+"mes","_NoSys","_MSCALEup","_MSCALEdown","tree","overallNormHistoSys") ##

## powheg pythia/powheg jimmy parton shower difference
tt_pythjim = Systematic(prefix+"tt_pythjim", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym")
tt_pythjim.setFileList('PowhegPythiaTTbar', PowhegJimmyTTbarFiles)
tt_pythjim.setTreeName('PowhegPythiaTTbar','PowhegJimmyTTbar_NoSys')
tt_pythjim.allowRemapOfSyst = doSystRemap

## powheg pythia/alpgen MC generator difference
#tt_pythalp = Systematic(prefix+"tt_pythalp", "_NoSys", "_NoSys", "_NoSys", "tree", "overallNormHistoSysOneSideSym") ## check!
#tt_pythalp.setFileList('PowhegPythiaTTbar', AlpgenTTbarFiles)
#tt_pythalp.setTreeName('PowhegPythiaTTbar','AlpgenTTbar_NoSys')
#tt_pythalp.allowRemapOfSyst = doSystRemap



## pythalp generator difference  ## ttbarV
#pythalp = Systematic(prefix+"ttV_pythalp", "_NoSys", "_NoSys", "_NoSys", "tree", "histoSysOneSideSym")
#pythalp.setFileList("ttbarV", AlpgenttbarVFiles )
#pythalp.setTreeName("ttbarV", 'ttbarVAlpgen_NoSys')


## pdf
##pdfIntraSyst = Systematic(prefix+"pdfIntra",configMgr.weights,configMgr.weights+["pdfWeightVars[0]"],configMgr.weights+["pdfWeightVars[1]"],"weight","overallNormHistoSys")
##pdfInterSyst = Systematic(prefix+"pdfInter",configMgr.weights,configMgr.weights+["pdfWeight"],configMgr.weights+["pdfWeight"],"weight","overallNormHistoSysOneSideSym")

#########################################################################################################
#########################################################################################################
generatorSyst = []
generatorSystW = []
generatorSystZ = []
generatorSystTTV = []
generatorSystDB = []
generatorSystST = []

doEnvelopeTTbar = True
#doTFuncertMinorBg = False #True
doLooseNRMinorBg = True ##else will take uncert normalised in TCR
#########################################################################################################
# Import top theory uncertainties from Lily
# Import PDF uncertainties from Takuya
if "s1L"  in pickedSRs[0]: 
    if doLooseNRMinorBg:
        from topTheoryUncertainties_LooseNR_s1L import *
    else:
        from topTheoryUncertainties_s1L import *
    from pdfUncertainties_s1L import *
else:
    if doLooseNRMinorBg:
        from topTheoryUncertainties_LooseNR import *
    else:
        from topTheoryUncertainties import *
    from pdfUncertainties import *
    pass
#########################################################################################################
if doELMU:
    theorySystList = ["Ele","Muo"]
else:
    theorySystList = ["Comb"]
    pass

#########################################################################################################
for doTheoryEM in theorySystList:
    if doTheoryEM=="Comb": prefixTheo = prefix
    if doTheoryEM=="Ele":  prefixTheo = prefix+'EL'
    if doTheoryEM=="Muo":  prefixTheo = prefix+'MU'

#########################################################################################################

    if doEnvelopeTTbar:

#### envelope
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topTheoSR1L3j       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoCRT3j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topTheoCRW3j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topTheoVR3j1        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topTheoVRT3j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topTheoVRW3j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topTheoVR3j2        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topTheoVRT3j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topTheoVRW3j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topTheoVR3j3        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topTheoVRT3j3       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topTheoVRW3j3       ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       topTheoSR1L5j       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoCRT5j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topTheoCRW5j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        topTheoVR5j1        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       topTheoVRT5j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       topTheoVRW5j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        topTheoVR5j2        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       topTheoVRT5j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       topTheoVRW5j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        topTheoVR5j3        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       topTheoVRT5j3       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       topTheoVRW5j3       ))
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topTheoSR1L3j       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoCRT3j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topTheoCRW3j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       topTheoSR1L5j       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT5j"),        topTheoCRT5j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW5j"),        topTheoCRW5j        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topTheoVR3j1        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topTheoVRT3j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topTheoVRW3j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topTheoVR3j2        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topTheoVRT3j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topTheoVRW3j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topTheoVR3j3        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topTheoVRT3j3       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topTheoVRW3j3       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        topTheoVR5j1        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       topTheoVRT5j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       topTheoVRW5j1       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        topTheoVR5j2        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       topTheoVRT5j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       topTheoVRW5j2       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        topTheoVR5j3        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       topTheoVRT5j3       ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       topTheoVRW5j3       ))
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Ba"),      topTheoSR1L2Ba      ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Ba"),     topTheoCRW1L2Ba     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Ba"),     topTheoCRT1L2Ba     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoVR1L2Ba1     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),     topTheoVR1L2Ba2     ))
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Bc"),      topTheoSR1L2Bc      ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Bc"),     topTheoCRW1L2Bc     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Bc"),     topTheoCRT1L2Bc     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoVR1L2Bc1     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),     topTheoVR1L2Bc2     ))
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Ba"),      topTheoSR1L1Ba      ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoVR1L1Ba3 ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Ba"),     topTheoCRW1L1Ba     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Ba"),     topTheoCRT1L1Ba     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoVR1L1Ba1     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     topTheoVR1L1Ba2     ))
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Bc"),      topTheoSR1L1Bc      ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoVR1L1Bc3   ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Bc"),     topTheoCRW1L1Bc     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Bc"),     topTheoCRT1L1Bc     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoVR1L1Bc1     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     topTheoVR1L1Bc2     ))
            pass
        if (chn==6 or chn==8):
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2La"),         topTheoSR2L         ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoCRT2L        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),        topTheoVR2L1        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),        topTheoVR2L2        ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),        topTheoVR2L3        ))
            pass
        if chn==9:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),      topTheoSR1L3jIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         topTheoCRTIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),         topTheoCRWIncl  ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         topTheoVSRIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),       topTheoVR3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),      topTheoVRT3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),      topTheoVRW3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),       topTheoVR3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),      topTheoVRT3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),      topTheoVRW3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),       topTheoVR3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),      topTheoVRT3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),      topTheoVRW3j3Incl  ))
            pass
    else:
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),         topTheoPSSR1L3j     ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoPSCRT3j      ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),          topTheoPSCRW3j      ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),         topTheoPSSR1L5j     ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoPSCRT5j      ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),          topTheoPSCRW5j      ))    
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),         topTheoPSSR1L3j     ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoPSCRT3j      ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),          topTheoPSCRW3j      ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),         topTheoPSSR1L5j     ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT5j"),          topTheoPSCRT5j      ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW5j"),          topTheoPSCRW5j      ))    
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Ba"),        topTheoPSSR1L2Ba    ))    
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoPSCRWbb1L2Ba ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Ba"),       topTheoPSCRW1L2Ba  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Ba"),       topTheoPSCRT1L2Ba  ))         
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Bc"),       topTheoPSSR1L2Bc ))         
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),    topTheoPSCRWbb1L2Bc  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Bc"),      topTheoPSCRW1L2Bc  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Bc"),      topTheoPSCRT1L2Bc  ))       
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Ba"),       topTheoPSSR1L1Ba  ))        
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),    topTheoPSCRWbb1L1Ba  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),    topTheoPSVR1L1Ba3  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Ba"),      topTheoPSCRW1L1Ba  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Ba"),      topTheoPSCRT1L1Ba  ))       
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Bc"),       topTheoPSSR1L1Bc  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),      topTheoPSVR1L1Bc3 ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Bc"),      topTheoPSCRW1L1Bc  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Bc"),      topTheoPSCRT1L1Bc  ))       
            pass
        if chn==6:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2La"),          topTheoPSSR2L  ))           
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         topTheoPSCRT2L  ))          
            pass
        if chn==8:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2Lb"),          topTheoPSSR2L  ))           
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         topTheoPSCRT2L  ))          
            pass
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),         topTheoPSVR3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),        topTheoPSVRT3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),        topTheoPSVRW3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),         topTheoPSVR3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),        topTheoPSVRT3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),        topTheoPSVRW3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),         topTheoPSVR3j3  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),        topTheoPSVRT3j3  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),        topTheoPSVRW3j3  ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),         topTheoPSVR5j1  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),        topTheoPSVRT5j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),        topTheoPSVRW5j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),         topTheoPSVR5j2  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),        topTheoPSVRT5j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),        topTheoPSVRW5j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),         topTheoPSVR5j3  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),        topTheoPSVRT5j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),        topTheoPSVRW5j3  ))         
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),         topTheoPSVR3j1  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),        topTheoPSVRT3j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),        topTheoPSVRW3j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),         topTheoPSVR3j2  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),        topTheoPSVRT3j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),        topTheoPSVRW3j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),         topTheoPSVR3j3  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),        topTheoPSVRT3j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),        topTheoPSVRW3j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),         topTheoPSVR5j1  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),        topTheoPSVRT5j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),        topTheoPSVRW5j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),         topTheoPSVR5j2  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),        topTheoPSVRT5j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),        topTheoPSVRW5j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),         topTheoPSVR5j3  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),        topTheoPSVRT5j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),        topTheoPSVRW5j3  ))
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),         topTheoPSVR1L2Ba1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         topTheoPSVR1L2Ba2  ))       
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),         topTheoPSVR1L2Bc1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         topTheoPSVR1L2Bc2  ))       
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),         topTheoPSVR1L1Ba1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),         topTheoPSVR1L1Ba2  ))       
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),         topTheoPSVR1L1Bc1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),         topTheoPSVR1L1Bc2  ))       
            pass
        if chn==6:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),         topTheoPSVR2L1  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),         topTheoPSVR2L2  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),         topTheoPSVR2L3  ))               
            pass
        if chn==8:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),         topTheoPSVR2L1  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),         topTheoPSVR2L2  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),         topTheoPSVR2L3  ))               
            pass
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topTheoRenScSR1L3j  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoRenScCRT3j  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topTheoRenScCRW3j  ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       topTheoRenScSR1L5j  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoRenScCRT5j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topTheoRenScCRW5j  ))        
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topTheoRenScSR1L3j  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoRenScCRT3j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topTheoRenScCRW3j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       topTheoRenScSR1L5j  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT5j"),        topTheoRenScCRT5j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW5j"),        topTheoRenScCRW5j  ))        
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Ba"),      topTheoRenScSR1L2Ba  ))      
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),   topTheoRenScCRWbb1L2Ba  ))   
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Ba"),     topTheoRenScCRW1L2Ba  ))     
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Bc"),      topTheoRenScSR1L2Bc  ))      
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),   topTheoRenScCRWbb1L2Bc  ))   
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Bc"),     topTheoRenScCRW1L2Bc  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Bc"),     topTheoRenScCRT1L2Bc  ))     
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Ba"),      topTheoRenScSR1L1Ba  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),   topTheoRenScVR1L1Ba3  ))   
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Ba"),     topTheoRenScCRW1L1Ba  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Ba"),     topTheoRenScCRT1L1Ba  ))     
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Bc"),      topTheoRenScSR1L1Bc  ))      
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),   topTheoRenScCRWbb1L1Bc  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),   topTheoRenScVR1L1Bc3  ))   
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Bc"),     topTheoRenScCRW1L1Bc  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Bc"),     topTheoRenScCRT1L1Bc  ))     
            pass
        if chn==6:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2La"),         topTheoRenScSR2L  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoRenScCRT2L  ))        
            pass
        if chn==8:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2Lb"),         topTheoRenScSR2L  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topTheoRenScCRT2L  ))        
            pass
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topTheoRenScVR3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topTheoRenScVRT3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topTheoRenScVRW3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topTheoRenScVR3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topTheoRenScVRT3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topTheoRenScVRW3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topTheoRenScVR3j3  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topTheoRenScVRT3j3  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topTheoRenScVRW3j3  ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        topTheoRenScVR5j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       topTheoRenScVRT5j1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       topTheoRenScVRW5j1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        topTheoRenScVR5j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       topTheoRenScVRT5j2  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       topTheoRenScVRW5j2  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        topTheoRenScVR5j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       topTheoRenScVRT5j3  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       topTheoRenScVRW5j3  ))       
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topTheoRenScVR3j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topTheoRenScVRT3j1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topTheoRenScVRW3j1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topTheoRenScVR3j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topTheoRenScVRT3j2  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topTheoRenScVRW3j2  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topTheoRenScVR3j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topTheoRenScVRT3j3  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topTheoRenScVRW3j3  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        topTheoRenScVR5j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       topTheoRenScVRT5j1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       topTheoRenScVRW5j1  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        topTheoRenScVR5j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       topTheoRenScVRT5j2  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       topTheoRenScVRW5j2  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        topTheoRenScVR5j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       topTheoRenScVRT5j3  ))       
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       topTheoRenScVRW5j3  ))
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoRenScVR1L2Ba1  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),     topTheoRenScVR1L2Ba2  ))     
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoRenScVR1L2Bc1  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),     topTheoRenScVR1L2Bc2  ))     
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoRenScVR1L1Ba1  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     topTheoRenScVR1L1Ba2  ))     
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topTheoRenScVR1L1Bc1  ))     
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     topTheoRenScVR1L1Bc2  ))     
            pass
        if chn==6:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),        topTheoRenScVR2L1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),        topTheoRenScVR2L2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),        topTheoRenScVR2L3  ))        
            pass
        if chn==8:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),        topTheoRenScVR2L1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),        topTheoRenScVR2L2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),        topTheoRenScVR2L3  ))        
            pass
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),         topTheoFacScSR1L3j  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoFacScCRT3j  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),          topTheoFacScCRW3j  ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),         topTheoFacScSR1L5j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoFacScCRT5j  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),          topTheoFacScCRW5j  ))         
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),         topTheoFacScSR1L3j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoFacScCRT3j  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),          topTheoFacScCRW3j  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),         topTheoFacScSR1L5j  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT5j"),          topTheoFacScCRT5j  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW5j"),          topTheoFacScCRW5j  ))         
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Ba"),        topTheoFacScSR1L2Ba  ))       
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoFacScCRWbb1L2Ba  ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Ba"),       topTheoFacScCRW1L2Ba  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Ba"),       topTheoFacScCRT1L2Ba  ))      
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Bc"),        topTheoFacScSR1L2Bc  ))       
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoFacScCRWbb1L2Bc  ))    
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Bc"),       topTheoFacScCRW1L2Bc  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Bc"),       topTheoFacScCRT1L2Bc  ))      
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Ba"),        topTheoFacScSR1L1Ba  ))       
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoFacScCRWbb1L1Ba  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),       topTheoFacScVR1L1Ba3  ))   
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Ba"),       topTheoFacScCRW1L1Ba  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Ba"),       topTheoFacScCRT1L1Ba  ))      
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Bc"),        topTheoFacScSR1L1Bc  ))       
                #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topTheoFacScCRWbb1L1Bc  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),       topTheoFacScVR1L1Bc3  ))   
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Bc"),       topTheoFacScCRW1L1Bc  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Bc"),       topTheoFacScCRT1L1Bc  ))      
            pass
        if chn==6:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2La"),           topTheoFacScSR2L  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoFacScCRT2L  ))         
            pass
        if chn==8:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2Lb"),           topTheoFacScSR2L  ))          
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),          topTheoFacScCRT2L  ))         
            pass
        if chn==0:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),          topTheoFacScVR3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),         topTheoFacScVRT3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),         topTheoFacScVRW3j1  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),          topTheoFacScVR3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),         topTheoFacScVRT3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),         topTheoFacScVRW3j2  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),          topTheoFacScVR3j3  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),         topTheoFacScVRT3j3  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),         topTheoFacScVRW3j3  ))
            pass
        if chn==7:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),          topTheoFacScVR5j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),         topTheoFacScVRT5j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),         topTheoFacScVRW5j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),          topTheoFacScVR5j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),         topTheoFacScVRT5j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),         topTheoFacScVRW5j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),          topTheoFacScVR5j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),         topTheoFacScVRT5j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),         topTheoFacScVRW5j3  ))        
            pass
        if chn==1:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),          topTheoFacScVR3j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),         topTheoFacScVRT3j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),         topTheoFacScVRW3j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),          topTheoFacScVR3j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),         topTheoFacScVRT3j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),         topTheoFacScVRW3j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),          topTheoFacScVR3j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),         topTheoFacScVRT3j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),         topTheoFacScVRW3j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),          topTheoFacScVR5j1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),         topTheoFacScVRT5j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),         topTheoFacScVRW5j1  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),          topTheoFacScVR5j2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),         topTheoFacScVRT5j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),         topTheoFacScVRW5j2  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),          topTheoFacScVR5j3  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),         topTheoFacScVRT5j3  ))        
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),         topTheoFacScVRW5j3  ))
            pass
        if chn==2:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),       topTheoFacScVR1L2Ba1  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),       topTheoFacScVR1L2Ba2  ))
            pass
        if chn==3:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),       topTheoFacScVR1L2Bc1  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),       topTheoFacScVR1L2Bc2  ))
            pass
        if chn==4:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),       topTheoFacScVR1L1Ba1  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),       topTheoFacScVR1L1Ba2  ))      
            pass
        if chn==5:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),       topTheoFacScVR1L1Bc1  ))      
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),       topTheoFacScVR1L1Bc2  ))
            pass
        if chn==6:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),          topTheoFacScVR2L1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),          topTheoFacScVR2L2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),          topTheoFacScVR2L3  ))
            pass
        if chn==8:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),          topTheoFacScVR2L1  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),          topTheoFacScVR2L2  ))         
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),          topTheoFacScVR2L3  ))         
            pass
        if chn==9:
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),      topTheoFacScSR1L3jIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         topTheoFacScCRTIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),         topTheoFacScCRWIncl  ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         topTheoFacScVSRIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),       topTheoFacScVR3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),      topTheoFacScVRT3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),      topTheoFacScVRW3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),       topTheoFacScVR3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),      topTheoFacScVRT3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),      topTheoFacScVRW3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),       topTheoFacScVR3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),      topTheoFacScVRT3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),      topTheoFacScVRW3j3Incl  ))
            
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),      topTheoPSSR1L3jIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         topTheoPSCRTIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),         topTheoPSCRWIncl  ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         topTheoPSVSRIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),       topTheoPSVR3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),      topTheoPSVRT3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),      topTheoPSVRW3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),       topTheoPSVR3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),      topTheoPSVRT3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),      topTheoPSVRW3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),       topTheoPSVR3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),      topTheoPSVRT3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),      topTheoPSVRW3j3Incl  ))
            
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),      topTheoRenScSR1L3jIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         topTheoRenScCRTIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),         topTheoRenScCRWIncl  ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         topTheoRenScVSRIncl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),       topTheoRenScVR3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),      topTheoRenScVRT3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),      topTheoRenScVRW3j1Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),       topTheoRenScVR3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),      topTheoRenScVRT3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),      topTheoRenScVRW3j2Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),       topTheoRenScVR3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),      topTheoRenScVRT3j3Incl  ))
            generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),      topTheoRenScVRW3j3Incl  ))
            pass
        pass #### doEnvelopeTTbar
##need only 1 indent from now on (before ifs) . right??
##ttbar Powheg-Alpgen from da
    if chn==9:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topGenSR1L3jIncl       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topGenCRT3jIncl        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topGenCRW3jIncl        ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topGenVR3j1Incl        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topGenVRT3j1Incl       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topGenVRW3j1Incl       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topGenVR3j2Incl        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topGenVRT3j2Incl       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topGenVRW3j2Incl       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topGenVR3j3Incl        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topGenVRT3j3Incl       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topGenVRW3j3Incl       ))
        pass
    if chn==0:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topGenSR1L3j       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topGenCRT3j        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topGenCRW3j        ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topGenVR3j1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topGenVRT3j1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topGenVRW3j1       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topGenVR3j2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topGenVRT3j2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topGenVRW3j2       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topGenVR3j3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topGenVRT3j3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topGenVRW3j3       ))
        pass

    if chn==7:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       topGenSR1L5j       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topGenCRT5j        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topGenCRW5j        ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        topGenVR5j1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       topGenVRT5j1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       topGenVRW5j1       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        topGenVR5j2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       topGenVRT5j2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       topGenVRW5j2       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        topGenVR5j3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       topGenVRT5j3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       topGenVRW5j3       ))
        pass
    
    if chn==1:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       topGenSR1L3j       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topGenCRT3j        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        topGenCRW3j        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       topGenSR1L5j       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT5j"),        topGenCRT5j        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW5j"),        topGenCRW5j        ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        topGenVR3j1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       topGenVRT3j1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       topGenVRW3j1       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        topGenVR3j2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       topGenVRT3j2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       topGenVRW3j2       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        topGenVR3j3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       topGenVRT3j3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       topGenVRW3j3       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        topGenVR5j1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       topGenVRT5j1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       topGenVRW5j1       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        topGenVR5j2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       topGenVRT5j2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       topGenVRW5j2       ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        topGenVR5j3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       topGenVRT5j3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       topGenVRW5j3       ))
        pass
    
    if chn==2:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Ba"),      topGenSR1L2Ba      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Ba"),     topGenCRW1L2Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Ba"),     topGenCRT1L2Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topGenVR1L2Ba1     ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),     topGenVR1L2Ba2     ))
        pass
    if chn==3:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Bc"),      topGenSR1L2Bc      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Bc"),     topGenCRW1L2Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Bc"),     topGenCRT1L2Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topGenVR1L2Bc1     ))
            #generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),     topGenVR1L2Bc2     ))
        pass
    if chn==4:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Ba"),      topGenSR1L1Ba      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topGenVR1L1Ba3 ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Ba"),     topGenCRW1L1Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Ba"),     topGenCRT1L1Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topGenVR1L1Ba1     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     topGenVR1L1Ba2     ))
        pass
    if chn==5:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Bc"),      topGenSR1L1Bc      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),     topGenVR1L1Bc3   ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Bc"),     topGenCRW1L1Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Bc"),     topGenCRT1L1Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     topGenVR1L1Bc1     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     topGenVR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2La"),         topGenSR2L         ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        topGenCRT2L        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),        topGenVR2L1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),        topGenVR2L2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),        topGenVR2L3        ))
        pass
    

###W syst
    if chn==0:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L3j"),       WTheoNpartSR1L3j       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),        WTheoNpartCRT3j        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),        WTheoNpartCRW3j        ))
        pass
    if chn==7:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L5j"),       WTheoNpartSR1L5j       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),        WTheoNpartCRT5j        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),        WTheoNpartCRW5j        ))
        pass
    if chn==1:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L3j"),       WTheoNpartSR1L3j       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),        WTheoNpartCRT3j        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),        WTheoNpartCRW3j        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L5j"),       WTheoNpartSR1L5j       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT5j"),        WTheoNpartCRT5j        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW5j"),        WTheoNpartCRW5j        ))
        pass
    if chn==2:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L2Ba"),      WTheoNpartSR1L2Ba      ))
            #    generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3"),   WTheoNpartCRWbb1L2Ba   ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L2Ba"),     WTheoNpartCRW1L2Ba     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L2Ba"),     WTheoNpartCRT1L2Ba     ))
        pass
    if chn==3:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L2Bc"),      WTheoNpartSR1L2Bc      ))
            #    generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3"),   WTheoNpartCRWbb1L2Bc   ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L2Bc"),     WTheoNpartCRW1L2Bc     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L2Bc"),     WTheoNpartCRT1L2Bc     ))
        pass
    if chn==4:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L1Ba"),      WTheoNpartSR1L1Ba      ))
            #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3"),   WTheoNpartCRWbb1L1Ba   ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3"),     WTheoNpartVR1L1Ba3 ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L1Ba"),     WTheoNpartCRW1L1Ba     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L1Ba"),     WTheoNpartCRT1L1Ba     ))
        pass
    if chn==5:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L1Bc"),      WTheoNpartSR1L1Bc      ))
            #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3"),   WTheoNpartCRWbb1L1Bc   ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3"),     WTheoNpartVR1L1Bc3   ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L1Bc"),     WTheoNpartCRW1L1Bc     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L1Bc"),     WTheoNpartCRT1L1Bc     ))
        pass
    if chn==0:
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j1"),        WTheoNpartVR3j1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j1"),       WTheoNpartVRT3j1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j1"),       WTheoNpartVRW3j1       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j2"),        WTheoNpartVR3j2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j2"),       WTheoNpartVRT3j2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j2"),       WTheoNpartVRW3j2       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j3"),        WTheoNpartVR3j3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j3"),       WTheoNpartVRT3j3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j3"),       WTheoNpartVRW3j3       ))
        pass
    if chn==7:
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j1"),        WTheoNpartVR5j1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j1"),       WTheoNpartVRT5j1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j1"),       WTheoNpartVRW5j1       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j2"),        WTheoNpartVR5j2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j2"),       WTheoNpartVRT5j2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j2"),       WTheoNpartVRW5j2       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j3"),        WTheoNpartVR5j3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j3"),       WTheoNpartVRT5j3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j3"),       WTheoNpartVRW5j3       ))
        pass
    if chn==1:
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j1"),        WTheoNpartVR3j1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j1"),       WTheoNpartVRT3j1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j1"),       WTheoNpartVRW3j1       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j2"),        WTheoNpartVR3j2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j2"),       WTheoNpartVRT3j2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j2"),       WTheoNpartVRW3j2       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j3"),        WTheoNpartVR3j3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j3"),       WTheoNpartVRT3j3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j3"),       WTheoNpartVRW3j3       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j1"),        WTheoNpartVR5j1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j1"),       WTheoNpartVRT5j1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j1"),       WTheoNpartVRW5j1       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j2"),        WTheoNpartVR5j2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j2"),       WTheoNpartVRT5j2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j2"),       WTheoNpartVRW5j2       ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j3"),        WTheoNpartVR5j3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j3"),       WTheoNpartVRT5j3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j3"),       WTheoNpartVRW5j3       ))
        pass
    if chn==2:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     WTheoNpartVR1L2Ba1     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VSR"),     WTheoNpartVR1L2Ba2     ))
        pass
    if chn==3:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     WTheoNpartVR1L2Bc1     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VSR"),     WTheoNpartVR1L2Bc2     ))
        pass
    if chn==4:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     WTheoNpartVR1L1Ba1     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR2"),     WTheoNpartVR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     WTheoNpartVR1L1Bc1     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR2"),     WTheoNpartVR1L1Bc2     ))
        pass
    
    
#### Z theo
    if chn==0:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L3j"),       ZTheoNpartSR1L3j       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        ZTheoNpartCRT3j        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),        ZTheoNpartCRW3j        ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j1"),        ZTheoNpartVR3j1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j1"),       ZTheoNpartVRT3j1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j1"),       ZTheoNpartVRW3j1       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j2"),        ZTheoNpartVR3j2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j2"),       ZTheoNpartVRT3j2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j2"),       ZTheoNpartVRW3j2       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j3"),        ZTheoNpartVR3j3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j3"),       ZTheoNpartVRT3j3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j3"),       ZTheoNpartVRW3j3       ))
        pass
    if chn==7:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L5j"),       ZTheoNpartSR1L5j       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        ZTheoNpartCRT5j        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),        ZTheoNpartCRW5j        ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j1"),        ZTheoNpartVR5j1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j1"),       ZTheoNpartVRT5j1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j1"),       ZTheoNpartVRW5j1       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j2"),        ZTheoNpartVR5j2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j2"),       ZTheoNpartVRT5j2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j2"),       ZTheoNpartVRW5j2       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j3"),        ZTheoNpartVR5j3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j3"),       ZTheoNpartVRT5j3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j3"),       ZTheoNpartVRW5j3       ))
        pass
    
    if chn==1:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L3j"),       ZTheoNpartSR1L3j       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        ZTheoNpartCRT3j        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),        ZTheoNpartCRW3j        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L5j"),       ZTheoNpartSR1L5j       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT5j"),        ZTheoNpartCRT5j        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW5j"),        ZTheoNpartCRW5j        ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j1"),        ZTheoNpartVR3j1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j1"),       ZTheoNpartVRT3j1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j1"),       ZTheoNpartVRW3j1       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j2"),        ZTheoNpartVR3j2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j2"),       ZTheoNpartVRT3j2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j2"),       ZTheoNpartVRW3j2       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j3"),        ZTheoNpartVR3j3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j3"),       ZTheoNpartVRT3j3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j3"),       ZTheoNpartVRW3j3       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j1"),        ZTheoNpartVR5j1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j1"),       ZTheoNpartVRT5j1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j1"),       ZTheoNpartVRW5j1       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j2"),        ZTheoNpartVR5j2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j2"),       ZTheoNpartVRT5j2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j2"),       ZTheoNpartVRW5j2       ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j3"),        ZTheoNpartVR5j3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j3"),       ZTheoNpartVRT5j3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j3"),       ZTheoNpartVRW5j3       ))
        pass
    
    if chn==2:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L2Ba"),      ZTheoNpartSR1L2Ba      ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L2Ba"),     ZTheoNpartCRW1L2Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L2Ba"),     ZTheoNpartCRT1L2Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     ZTheoNpartVR1L2Ba1     ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VSR"),     ZTheoNpartVR1L2Ba2     ))
        pass
    if chn==3:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L2Bc"),      ZTheoNpartSR1L2Bc      ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L2Bc"),     ZTheoNpartCRW1L2Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L2Bc"),     ZTheoNpartCRT1L2Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     ZTheoNpartVR1L2Bc1     ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VSR"),     ZTheoNpartVR1L2Bc2     ))
        pass
    if chn==4:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L1Ba"),      ZTheoNpartSR1L1Ba      ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3"),     ZTheoNpartVR1L1Ba3 ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L1Ba"),     ZTheoNpartCRW1L1Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L1Ba"),     ZTheoNpartCRT1L1Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     ZTheoNpartVR1L1Ba1     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR2"),     ZTheoNpartVR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L1Bc"),      ZTheoNpartSR1L1Bc      ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3"),     ZTheoNpartVR1L1Bc3   ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L1Bc"),     ZTheoNpartCRW1L1Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L1Bc"),     ZTheoNpartCRT1L1Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     ZTheoNpartVR1L1Bc1     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR2"),     ZTheoNpartVR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR2La"),         ZTheoNpartSR2L         ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        ZTheoNpartCRT2L        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),        ZTheoNpartVR2L1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR2"),        ZTheoNpartVR2L2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3"),        ZTheoNpartVR2L3        ))
        pass
    
    
#### ttbarV theo
    if chn==0:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L3j"),     ttbarVTheoSR1L3j       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        ttbarVTheoCRT3j        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),        ttbarVTheoCRW3j        ))
        pass
    if chn==7:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L5j"),       ttbarVTheoSR1L5j       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        ttbarVTheoCRT5j        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),        ttbarVTheoCRW5j        ))
        pass
    if chn==1:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L3j"),       ttbarVTheoSR1L3j       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        ttbarVTheoCRT3j        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),        ttbarVTheoCRW3j        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L5j"),       ttbarVTheoSR1L5j       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT5j"),        ttbarVTheoCRT5j        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW5j"),        ttbarVTheoCRW5j        ))
        pass
    if chn==2:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L2Ba"),      ttbarVTheoSR1L2Ba      ))
            #    generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),   ttbarVTheoCRWbb1L2Ba   ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L2Ba"),     ttbarVTheoCRW1L2Ba     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L2Ba"),     ttbarVTheoCRT1L2Ba     ))
        pass
    if chn==3:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L2Bc"),      ttbarVTheoSR1L2Bc      ))
            #    generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),   ttbarVTheoCRWbb1L2Bc   ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L2Bc"),     ttbarVTheoCRW1L2Bc     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L2Bc"),     ttbarVTheoCRT1L2Bc     ))
        pass
    if chn==4:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L1Ba"),      ttbarVTheoSR1L1Ba      ))
            #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),   ttbarVTheoCRWbb1L1Ba   ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),     ttbarVTheoVR1L1Ba3 ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L1Ba"),     ttbarVTheoCRW1L1Ba     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L1Ba"),     ttbarVTheoCRT1L1Ba     ))
        pass
    if chn==5:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L1Bc"),      ttbarVTheoSR1L1Bc      ))
            #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),   ttbarVTheoCRWbb1L1Bc   ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),     ttbarVTheoVR1L1Bc3   ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L1Bc"),     ttbarVTheoCRW1L1Bc     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L1Bc"),     ttbarVTheoCRT1L1Bc     ))
        pass
    if (chn==6 or chn==8):
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR2La"),         ttbarVTheoSR2L         ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        ttbarVTheoCRT2L        ))
        pass
    if chn==0:
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j1"),        ttbarVTheoVR3j1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j1"),       ttbarVTheoVRT3j1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j1"),       ttbarVTheoVRW3j1       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j2"),        ttbarVTheoVR3j2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j2"),       ttbarVTheoVRT3j2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j2"),       ttbarVTheoVRW3j2       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j3"),        ttbarVTheoVR3j3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j3"),       ttbarVTheoVRT3j3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j3"),       ttbarVTheoVRW3j3       ))
        pass
    if chn==7:
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j1"),        ttbarVTheoVR5j1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j1"),       ttbarVTheoVRT5j1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j1"),       ttbarVTheoVRW5j1       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j2"),        ttbarVTheoVR5j2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j2"),       ttbarVTheoVRT5j2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j2"),       ttbarVTheoVRW5j2       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j3"),        ttbarVTheoVR5j3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j3"),       ttbarVTheoVRT5j3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j3"),       ttbarVTheoVRW5j3       ))
        pass
    if chn==1:
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j1"),        ttbarVTheoVR3j1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j1"),       ttbarVTheoVRT3j1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j1"),       ttbarVTheoVRW3j1       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j2"),        ttbarVTheoVR3j2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j2"),       ttbarVTheoVRT3j2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j2"),       ttbarVTheoVRW3j2       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j3"),        ttbarVTheoVR3j3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j3"),       ttbarVTheoVRT3j3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j3"),       ttbarVTheoVRW3j3       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j1"),        ttbarVTheoVR5j1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j1"),       ttbarVTheoVRT5j1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j1"),       ttbarVTheoVRW5j1       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j2"),        ttbarVTheoVR5j2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j2"),       ttbarVTheoVRT5j2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j2"),       ttbarVTheoVRW5j2       ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j3"),        ttbarVTheoVR5j3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j3"),       ttbarVTheoVRT5j3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j3"),       ttbarVTheoVRW5j3       ))
        pass
    if chn==2:
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     ttbarVTheoVR1L2Ba1     ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VSR"),     ttbarVTheoVR1L2Ba2     ))
        pass
    if chn==3:
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     ttbarVTheoVR1L2Bc1     ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VSR"),     ttbarVTheoVR1L2Bc2     ))
        pass
    if chn==4:
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     ttbarVTheoVR1L1Ba1     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR2"),     ttbarVTheoVR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     ttbarVTheoVR1L1Bc1     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR2"),     ttbarVTheoVR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),        ttbarVTheoVR2L1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR2"),        ttbarVTheoVR2L2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),        ttbarVTheoVR2L3        ))
        pass
    
    
#### DB theo
    if chn==0:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L3j"),       dbTheoSR1L3j       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),        dbTheoCRT3j        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),        dbTheoCRW3j        ))
        pass
    if chn==7:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L5j"),       dbTheoSR1L5j       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),        dbTheoCRT5j        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),        dbTheoCRW5j        ))
        pass
    if chn==1:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L3j"),       dbTheoSR1L3j       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),        dbTheoCRT3j        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),        dbTheoCRW3j        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L5j"),       dbTheoSR1L5j       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT5j"),        dbTheoCRT5j        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW5j"),        dbTheoCRW5j        ))
        pass
    if chn==2:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L2Ba"),      dbTheoSR1L2Ba      ))
            #    generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3"),   dbTheoCRWbb1L2Ba   ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L2Ba"),     dbTheoCRW1L2Ba     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L2Ba"),     dbTheoCRT1L2Ba     ))
        pass
    if chn==3:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L2Bc"),      dbTheoSR1L2Bc      ))
            #    generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3"),   dbTheoCRWbb1L2Bc   ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L2Bc"),     dbTheoCRW1L2Bc     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L2Bc"),     dbTheoCRT1L2Bc     ))
        pass
    if chn==4:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L1Ba"),      dbTheoSR1L1Ba      ))
            #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3"),   dbTheoCRWbb1L1Ba   ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3"),     dbTheoVR1L1Ba3 ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L1Ba"),     dbTheoCRW1L1Ba     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L1Ba"),     dbTheoCRT1L1Ba     ))
        pass
    if chn==5:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L1Bc"),      dbTheoSR1L1Bc      ))
            #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3"),   dbTheoCRWbb1L1Bc   ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3"),     dbTheoVR1L1Bc3   ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L1Bc"),     dbTheoCRW1L1Bc     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L1Bc"),     dbTheoCRT1L1Bc     ))
        pass
    if (chn==6 or chn==8):
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"SR2La"),         dbTheoSR2L         ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"CRT"),        dbTheoCRT2L        ))
        pass
    if chn==0:
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j1"),        dbTheoVR3j1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j1"),       dbTheoVRT3j1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j1"),       dbTheoVRW3j1       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j2"),        dbTheoVR3j2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j2"),       dbTheoVRT3j2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j2"),       dbTheoVRW3j2       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j3"),        dbTheoVR3j3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j3"),       dbTheoVRT3j3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j3"),       dbTheoVRW3j3       ))
        pass
    if chn==7:
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j1"),        dbTheoVR5j1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j1"),       dbTheoVRT5j1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j1"),       dbTheoVRW5j1       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j2"),        dbTheoVR5j2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j2"),       dbTheoVRT5j2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j2"),       dbTheoVRW5j2       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j3"),        dbTheoVR5j3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j3"),       dbTheoVRT5j3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j3"),       dbTheoVRW5j3       ))
        pass
    if chn==1:
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j1"),        dbTheoVR3j1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j1"),       dbTheoVRT3j1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j1"),       dbTheoVRW3j1       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j2"),        dbTheoVR3j2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j2"),       dbTheoVRT3j2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j2"),       dbTheoVRW3j2       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j3"),        dbTheoVR3j3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j3"),       dbTheoVRT3j3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j3"),       dbTheoVRW3j3       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j1"),        dbTheoVR5j1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j1"),       dbTheoVRT5j1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j1"),       dbTheoVRW5j1       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j2"),        dbTheoVR5j2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j2"),       dbTheoVRT5j2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j2"),       dbTheoVRW5j2       ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j3"),        dbTheoVR5j3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j3"),       dbTheoVRT5j3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j3"),       dbTheoVRW5j3       ))
        pass
    if chn==2:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     dbTheoVR1L2Ba1     ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VSR"),     dbTheoVR1L2Ba2     ))
        pass
    if chn==3:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     dbTheoVR1L2Bc1     ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VSR"),     dbTheoVR1L2Bc2     ))
        pass
    if chn==4:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     dbTheoVR1L1Ba1     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR2"),     dbTheoVR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     dbTheoVR1L1Bc1     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR2"),     dbTheoVR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"VR1"),        dbTheoVR2L1        ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"VR2"),        dbTheoVR2L2        ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"VR3"),        dbTheoVR2L3        ))
        pass
    
    
    

#### ST theo
    if chn==0:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L3j"),       SingleTopTheoSR1L3j       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),        SingleTopTheoCRT3j        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),        SingleTopTheoCRW3j        ))
        pass
    if chn==7:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L5j"),       SingleTopTheoSR1L5j       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),        SingleTopTheoCRT5j        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),        SingleTopTheoCRW5j        ))
        pass
    if chn==1:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L3j"),       SingleTopTheoSR1L3j       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),        SingleTopTheoCRT3j        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),        SingleTopTheoCRW3j        ))
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L5j"),       SingleTopTheoSR1L5j       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT5j"),        SingleTopTheoCRT5j        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW5j"),        SingleTopTheoCRW5j        ))
        pass
    if chn==2:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L2Ba"),      SingleTopTheoSR1L2Ba      ))
            #    generatorSystST.append((("SingleTop",prefixTheo+"VR3"),   SingleTopTheoCRWbb1L2Ba   ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L2Ba"),     SingleTopTheoCRW1L2Ba     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L2Ba"),     SingleTopTheoCRT1L2Ba     ))
        pass
    if chn==3:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L2Bc"),      SingleTopTheoSR1L2Bc      ))
            #    generatorSystST.append((("SingleTop",prefixTheo+"VR3"),   SingleTopTheoCRWbb1L2Bc   ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L2Bc"),     SingleTopTheoCRW1L2Bc     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L2Bc"),     SingleTopTheoCRT1L2Bc     ))
        pass
    if chn==4:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L1Ba"),      SingleTopTheoSR1L1Ba      ))
            #generatorSystST.append((("SingleTop",prefixTheo+"VR3"),   SingleTopTheoCRWbb1L1Ba   ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3"),     SingleTopTheoVR1L1Ba3 ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L1Ba"),     SingleTopTheoCRW1L1Ba     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L1Ba"),     SingleTopTheoCRT1L1Ba     ))
        pass
    if chn==5:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L1Bc"),      SingleTopTheoSR1L1Bc      ))
            #generatorSystST.append((("SingleTop",prefixTheo+"VR3"),   SingleTopTheoCRWbb1L1Bc   ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3"),     SingleTopTheoVR1L1Bc3   ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L1Bc"),     SingleTopTheoCRW1L1Bc     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L1Bc"),     SingleTopTheoCRT1L1Bc     ))
        pass
    if (chn==6 or chn==8):
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"SR2La"),         SingleTopTheoSR2L         ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"CRT"),        SingleTopTheoCRT2L        ))
        pass
    if chn==0:
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j1"),        SingleTopTheoVR3j1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j1"),       SingleTopTheoVRT3j1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j1"),       SingleTopTheoVRW3j1       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j2"),        SingleTopTheoVR3j2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j2"),       SingleTopTheoVRT3j2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j2"),       SingleTopTheoVRW3j2       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j3"),        SingleTopTheoVR3j3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j3"),       SingleTopTheoVRT3j3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j3"),       SingleTopTheoVRW3j3       ))
        pass
    if chn==7:
        #generatorSystST.append((("SingleTop",prefixTheo+"VR5j1"),        SingleTopTheoVR5j1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j1"),       SingleTopTheoVRT5j1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j1"),       SingleTopTheoVRW5j1       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR5j2"),        SingleTopTheoVR5j2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j2"),       SingleTopTheoVRT5j2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j2"),       SingleTopTheoVRW5j2       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR5j3"),        SingleTopTheoVR5j3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j3"),       SingleTopTheoVRT5j3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j3"),       SingleTopTheoVRW5j3       ))
        pass
    if chn==1:
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j1"),        SingleTopTheoVR3j1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j1"),       SingleTopTheoVRT3j1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j1"),       SingleTopTheoVRW3j1       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j2"),        SingleTopTheoVR3j2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j2"),       SingleTopTheoVRT3j2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j2"),       SingleTopTheoVRW3j2       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j3"),        SingleTopTheoVR3j3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j3"),       SingleTopTheoVRT3j3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j3"),       SingleTopTheoVRW3j3       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR5j1"),        SingleTopTheoVR5j1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j1"),       SingleTopTheoVRT5j1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j1"),       SingleTopTheoVRW5j1       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR5j2"),        SingleTopTheoVR5j2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j2"),       SingleTopTheoVRT5j2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j2"),       SingleTopTheoVRW5j2       ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR5j3"),        SingleTopTheoVR5j3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j3"),       SingleTopTheoVRT5j3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j3"),       SingleTopTheoVRW5j3       ))
        pass
    if chn==2:
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     SingleTopTheoVR1L2Ba1     ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VSR"),     SingleTopTheoVR1L2Ba2     ))
        pass
    if chn==3:
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     SingleTopTheoVR1L2Bc1     ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VSR"),     SingleTopTheoVR1L2Bc2     ))
        pass
    if chn==4:
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     SingleTopTheoVR1L1Ba1     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR2"),     SingleTopTheoVR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     SingleTopTheoVR1L1Bc1     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR2"),     SingleTopTheoVR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"VR1"),        SingleTopTheoVR2L1        ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"VR2"),        SingleTopTheoVR2L2        ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"VR3"),        SingleTopTheoVR2L3        ))
        pass
    
        
## missing chn==9 here for SR3jIncl-- SS1step gridx ##
## make sure you didn't add them twice ##
    if chn==9:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L3j"),      WTheoNpartSR1L3jIncl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),         WTheoNpartCRTIncl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),         WTheoNpartCRWIncl  ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VSR"),         WTheoNpartVSRIncl  ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j1"),       WTheoNpartVR3j1Incl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j1"),      WTheoNpartVRT3j1Incl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j1"),      WTheoNpartVRW3j1Incl  ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j2"),       WTheoNpartVR3j2Incl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j2"),      WTheoNpartVRT3j2Incl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j2"),      WTheoNpartVRW3j2Incl  ))
        #generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j3"),       WTheoNpartVR3j3Incl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j3"),      WTheoNpartVRT3j3Incl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j3"),      WTheoNpartVRW3j3Incl  ))
        
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L3j"),      ZTheoNpartSR1L3jIncl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),         ZTheoNpartCRTIncl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),         ZTheoNpartCRWIncl  ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VSR"),         ZTheoNpartVSRIncl  ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j1"),       ZTheoNpartVR3j1Incl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j1"),      ZTheoNpartVRT3j1Incl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j1"),      ZTheoNpartVRW3j1Incl  ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j2"),       ZTheoNpartVR3j2Incl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j2"),      ZTheoNpartVRT3j2Incl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j2"),      ZTheoNpartVRW3j2Incl  ))
        #generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j3"),       ZTheoNpartVR3j3Incl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j3"),      ZTheoNpartVRT3j3Incl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j3"),      ZTheoNpartVRW3j3Incl  ))
        
        
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L3j"),      ttbarVTheoSR1L3jIncl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),         ttbarVTheoCRTIncl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),         ttbarVTheoCRWIncl  ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VSR"),         ttbarVTheoVSRIncl  ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j1"),       ttbarVTheoVR3j1Incl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j1"),      ttbarVTheoVRT3j1Incl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j1"),      ttbarVTheoVRW3j1Incl  ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j2"),       ttbarVTheoVR3j2Incl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j2"),      ttbarVTheoVRT3j2Incl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j2"),      ttbarVTheoVRW3j2Incl  ))
        #generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j3"),       ttbarVTheoVR3j3Incl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j3"),      ttbarVTheoVRT3j3Incl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j3"),      ttbarVTheoVRW3j3Incl  ))
        
        
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L3j"),      dbTheoSR1L3jIncl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),         dbTheoCRTIncl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),         dbTheoCRWIncl  ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VSR"),         dbTheoVSRIncl  ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j1"),       dbTheoVR3j1Incl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j1"),      dbTheoVRT3j1Incl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j1"),      dbTheoVRW3j1Incl  ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j2"),       dbTheoVR3j2Incl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j2"),      dbTheoVRT3j2Incl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j2"),      dbTheoVRW3j2Incl  ))
        #generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j3"),       dbTheoVR3j3Incl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j3"),      dbTheoVRT3j3Incl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j3"),      dbTheoVRW3j3Incl  ))
        
        
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L3j"),      SingleTopTheoSR1L3jIncl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),         SingleTopTheoCRTIncl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),         SingleTopTheoCRWIncl  ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VSR"),         SingleTopTheoVSRIncl  ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j1"),       SingleTopTheoVR3j1Incl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j1"),      SingleTopTheoVRT3j1Incl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j1"),      SingleTopTheoVRW3j1Incl  ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j2"),       SingleTopTheoVR3j2Incl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j2"),      SingleTopTheoVRT3j2Incl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j2"),      SingleTopTheoVRW3j2Incl  ))
        #generatorSystST.append((("SingleTop",prefixTheo+"VR3j3"),       SingleTopTheoVR3j3Incl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j3"),      SingleTopTheoVRT3j3Incl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j3"),      SingleTopTheoVRW3j3Incl  ))
        pass
    
##Z
    if chn==0:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L3j"),       PDF_ZJETS_SR1L3J       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        PDF_ZJETS_CRT1L3J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),        PDF_ZJETS_CRW1L3J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j1"),        PDF_ZJETS_VR1L3J1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j1"),       PDF_ZJETS_VRT1L3J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j1"),       PDF_ZJETS_VRW1L3J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j2"),        PDF_ZJETS_VR1L3J2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j2"),       PDF_ZJETS_VRT1L3J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j2"),       PDF_ZJETS_VRW1L3J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j3"),        PDF_ZJETS_VR1L3J3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j3"),       PDF_ZJETS_VRT1L3J3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j3"),       PDF_ZJETS_VRW1L3J3       ))
        pass
    if chn==7:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L5j"),       PDF_ZJETS_SR1L5J       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        PDF_ZJETS_CRT1L5J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),        PDF_ZJETS_CRW1L5J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j1"),        PDF_ZJETS_VR1L5J1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j1"),       PDF_ZJETS_VRT1L5J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j1"),       PDF_ZJETS_VRW1L5J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j2"),        PDF_ZJETS_VR1L5J2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j2"),       PDF_ZJETS_VRT1L5J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j2"),       PDF_ZJETS_VRW1L5J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j3"),        PDF_ZJETS_VR1L5J3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j3"),       PDF_ZJETS_VRT1L5J3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j3"),       PDF_ZJETS_VRW1L5J3       ))
        pass
    
    if chn==1:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L3j"),       PDF_ZJETS_SR1L3J       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        PDF_ZJETS_CRT1L3J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),        PDF_ZJETS_CRW1L3J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L5j"),       PDF_ZJETS_SR1L5J       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT5j"),        PDF_ZJETS_CRT1L5J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW5j"),        PDF_ZJETS_CRW1L5J        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j1"),        PDF_ZJETS_VR1L3J1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j1"),       PDF_ZJETS_VRT1L3J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j1"),       PDF_ZJETS_VRW1L3J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j2"),        PDF_ZJETS_VR1L3J2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j2"),       PDF_ZJETS_VRT1L3J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j2"),       PDF_ZJETS_VRW1L3J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j3"),        PDF_ZJETS_VR1L3J3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j3"),       PDF_ZJETS_VRT1L3J3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j3"),       PDF_ZJETS_VRW1L3J3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j1"),        PDF_ZJETS_VR1L5J1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j1"),       PDF_ZJETS_VRT1L5J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j1"),       PDF_ZJETS_VRW1L5J1       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j2"),        PDF_ZJETS_VR1L5J2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j2"),       PDF_ZJETS_VRT1L5J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j2"),       PDF_ZJETS_VRW1L5J2       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR5j3"),        PDF_ZJETS_VR1L5J3        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT5j3"),       PDF_ZJETS_VRT1L5J3       ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW5j3"),       PDF_ZJETS_VRW1L5J3       ))
        pass
    if chn==9:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L3j"),      PDF_ZJETS_SR1L3JIncl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),         PDF_ZJETS_CRT1L3JIncl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW"),         PDF_ZJETS_CRW1L3JIncl  ))
            #    generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VSR"),         PDF_ZJETS_VSRIncl  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j1"),       PDF_ZJETS_VR1L3JIncl1  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j1"),      PDF_ZJETS_VRT1L3JIncl1  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j1"),      PDF_ZJETS_VRW1L3JIncl1  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j2"),       PDF_ZJETS_VR1L3JIncl2  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j2"),      PDF_ZJETS_VRT1L3JIncl2  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j2"),      PDF_ZJETS_VRW1L3JIncl2  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3j3"),       PDF_ZJETS_VR1L3JIncl3  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRT3j3"),      PDF_ZJETS_VRT1L3JIncl3  ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VRW3j3"),      PDF_ZJETS_VRW1L3JIncl3  ))
        pass
    if chn==2:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L2Ba"),      PDF_ZJETS_SR1L2Ba      ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L2Ba"),     PDF_ZJETS_CRW1L2Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L2Ba"),     PDF_ZJETS_CRT1L2Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     PDF_ZJETS_VR1L2Ba     ))
        pass
    if chn==3:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L2Bc"),      PDF_ZJETS_SR1L2Bc      ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L2Bc"),     PDF_ZJETS_CRW1L2Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L2Bc"),     PDF_ZJETS_CRT1L2Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     PDF_ZJETS_VR1L2Bc     ))
        pass
    if chn==4:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L1Ba"),      PDF_ZJETS_SR1L1Ba      ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L1Ba"),     PDF_ZJETS_CRW1L1Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L1Ba"),     PDF_ZJETS_CRT1L1Ba     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     PDF_ZJETS_VR1L1Ba1     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR2"),     PDF_ZJETS_VR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR1L1Bc"),      PDF_ZJETS_SR1L1Bc      ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRW1L1Bc"),     PDF_ZJETS_CRW1L1Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT1L1Bc"),     PDF_ZJETS_CRT1L1Bc     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),     PDF_ZJETS_VR1L1Bc1     ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR2"),     PDF_ZJETS_VR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"SR2La"),         PDF_ZJETS_SR2MU         ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"CRT"),        PDF_ZJETS_CRT2MU        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR1"),        PDF_ZJETS_VR2MU1        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR2"),        PDF_ZJETS_VR2MU2        ))
        generatorSystZ.append((("SherpaZMassiveBC",prefixTheo+"VR3"),        PDF_ZJETS_VR2MU3        ))
        pass
###W
    if chn==0:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L3j"),       PDF_WJETS_SR1L3J       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),        PDF_WJETS_CRT1L3J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),        PDF_WJETS_CRW1L3J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j1"),        PDF_WJETS_VR1L3J1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j1"),       PDF_WJETS_VRT1L3J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j1"),       PDF_WJETS_VRW1L3J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j2"),        PDF_WJETS_VR1L3J2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j2"),       PDF_WJETS_VRT1L3J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j2"),       PDF_WJETS_VRW1L3J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j3"),        PDF_WJETS_VR1L3J3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j3"),       PDF_WJETS_VRT1L3J3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j3"),       PDF_WJETS_VRW1L3J3       ))
        pass
    if chn==7:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L5j"),       PDF_WJETS_SR1L5J       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),        PDF_WJETS_CRT1L5J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),        PDF_WJETS_CRW1L5J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j1"),        PDF_WJETS_VR1L5J1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j1"),       PDF_WJETS_VRT1L5J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j1"),       PDF_WJETS_VRW1L5J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j2"),        PDF_WJETS_VR1L5J2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j2"),       PDF_WJETS_VRT1L5J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j2"),       PDF_WJETS_VRW1L5J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j3"),        PDF_WJETS_VR1L5J3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j3"),       PDF_WJETS_VRT1L5J3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j3"),       PDF_WJETS_VRW1L5J3       ))
        pass
    
    if chn==1:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L3j"),       PDF_WJETS_SR1L3J       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),        PDF_WJETS_CRT1L3J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),        PDF_WJETS_CRW1L3J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L5j"),       PDF_WJETS_SR1L5J       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT5j"),        PDF_WJETS_CRT1L5J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW5j"),        PDF_WJETS_CRW1L5J        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j1"),        PDF_WJETS_VR1L3J1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j1"),       PDF_WJETS_VRT1L3J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j1"),       PDF_WJETS_VRW1L3J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j2"),        PDF_WJETS_VR1L3J2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j2"),       PDF_WJETS_VRT1L3J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j2"),       PDF_WJETS_VRW1L3J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j3"),        PDF_WJETS_VR1L3J3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j3"),       PDF_WJETS_VRT1L3J3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j3"),       PDF_WJETS_VRW1L3J3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j1"),        PDF_WJETS_VR1L5J1        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j1"),       PDF_WJETS_VRT1L5J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j1"),       PDF_WJETS_VRW1L5J1       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j2"),        PDF_WJETS_VR1L5J2        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j2"),       PDF_WJETS_VRT1L5J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j2"),       PDF_WJETS_VRW1L5J2       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR5j3"),        PDF_WJETS_VR1L5J3        ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT5j3"),       PDF_WJETS_VRT1L5J3       ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW5j3"),       PDF_WJETS_VRW1L5J3       ))
        pass
    if chn==9:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L3j"),      PDF_WJETS_SR1L3JIncl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT"),         PDF_WJETS_CRT1L3JIncl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW"),         PDF_WJETS_CRW1L3JIncl  ))
            #    generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VSR"),         PDF_WJETS_VSRIncl  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j1"),       PDF_WJETS_VR1L3JIncl1  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j1"),      PDF_WJETS_VRT1L3JIncl1  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j1"),      PDF_WJETS_VRW1L3JIncl1  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j2"),       PDF_WJETS_VR1L3JIncl2  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j2"),      PDF_WJETS_VRT1L3JIncl2  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j2"),      PDF_WJETS_VRW1L3JIncl2  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR3j3"),       PDF_WJETS_VR1L3JIncl3  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRT3j3"),      PDF_WJETS_VRT1L3JIncl3  ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VRW3j3"),      PDF_WJETS_VRW1L3JIncl3  ))
        pass
    if chn==2:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L2Ba"),      PDF_WJETS_SR1L2Ba      ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L2Ba"),     PDF_WJETS_CRW1L2Ba     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L2Ba"),     PDF_WJETS_CRT1L2Ba     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     PDF_WJETS_VR1L2Ba     ))
        pass
    if chn==3:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L2Bc"),      PDF_WJETS_SR1L2Bc      ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L2Bc"),     PDF_WJETS_CRW1L2Bc     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L2Bc"),     PDF_WJETS_CRT1L2Bc     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     PDF_WJETS_VR1L2Bc     ))
        pass
    if chn==4:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L1Ba"),      PDF_WJETS_SR1L1Ba      ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L1Ba"),     PDF_WJETS_CRW1L1Ba     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L1Ba"),     PDF_WJETS_CRT1L1Ba     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     PDF_WJETS_VR1L1Ba1     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR2"),     PDF_WJETS_VR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"SR1L1Bc"),      PDF_WJETS_SR1L1Bc      ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRW1L1Bc"),     PDF_WJETS_CRW1L1Bc     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"CRT1L1Bc"),     PDF_WJETS_CRT1L1Bc     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR1"),     PDF_WJETS_VR1L1Bc1     ))
        generatorSystW.append((("SherpaWMassiveBC",prefixTheo+"VR2"),     PDF_WJETS_VR1L1Bc2     ))
        pass
    
        
##TT
    if chn==0:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       PDF_TTBAR_SR1L3J       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        PDF_TTBAR_CRT1L3J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        PDF_TTBAR_CRW1L3J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        PDF_TTBAR_VR1L3J1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       PDF_TTBAR_VRT1L3J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       PDF_TTBAR_VRW1L3J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        PDF_TTBAR_VR1L3J2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       PDF_TTBAR_VRT1L3J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       PDF_TTBAR_VRW1L3J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        PDF_TTBAR_VR1L3J3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       PDF_TTBAR_VRT1L3J3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       PDF_TTBAR_VRW1L3J3       ))
        pass
    if chn==7:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       PDF_TTBAR_SR1L5J       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        PDF_TTBAR_CRT1L5J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        PDF_TTBAR_CRW1L5J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        PDF_TTBAR_VR1L5J1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       PDF_TTBAR_VRT1L5J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       PDF_TTBAR_VRW1L5J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        PDF_TTBAR_VR1L5J2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       PDF_TTBAR_VRT1L5J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       PDF_TTBAR_VRW1L5J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        PDF_TTBAR_VR1L5J3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       PDF_TTBAR_VRT1L5J3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       PDF_TTBAR_VRW1L5J3       ))
        pass
    
    if chn==1:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),       PDF_TTBAR_SR1L3J       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        PDF_TTBAR_CRT1L3J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),        PDF_TTBAR_CRW1L3J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L5j"),       PDF_TTBAR_SR1L5J       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT5j"),        PDF_TTBAR_CRT1L5J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW5j"),        PDF_TTBAR_CRW1L5J        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),        PDF_TTBAR_VR1L3J1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),       PDF_TTBAR_VRT1L3J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),       PDF_TTBAR_VRW1L3J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),        PDF_TTBAR_VR1L3J2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),       PDF_TTBAR_VRT1L3J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),       PDF_TTBAR_VRW1L3J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),        PDF_TTBAR_VR1L3J3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),       PDF_TTBAR_VRT1L3J3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),       PDF_TTBAR_VRW1L3J3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j1"),        PDF_TTBAR_VR1L5J1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j1"),       PDF_TTBAR_VRT1L5J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j1"),       PDF_TTBAR_VRW1L5J1       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j2"),        PDF_TTBAR_VR1L5J2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j2"),       PDF_TTBAR_VRT1L5J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j2"),       PDF_TTBAR_VRW1L5J2       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR5j3"),        PDF_TTBAR_VR1L5J3        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT5j3"),       PDF_TTBAR_VRT1L5J3       ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW5j3"),       PDF_TTBAR_VRW1L5J3       ))
        pass
    if chn==9:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L3j"),      PDF_TTBAR_SR1L3JIncl  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),         PDF_TTBAR_CRT1L3JIncl  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW"),         PDF_TTBAR_CRW1L3JIncl  ))
            #    generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VSR"),         PDF_TTBAR_VSRIncl  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j1"),       PDF_TTBAR_VR1L3JIncl1  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j1"),      PDF_TTBAR_VRT1L3JIncl1  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j1"),      PDF_TTBAR_VRW1L3JIncl1  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j2"),       PDF_TTBAR_VR1L3JIncl2  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j2"),      PDF_TTBAR_VRT1L3JIncl2  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j2"),      PDF_TTBAR_VRW1L3JIncl2  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3j3"),       PDF_TTBAR_VR1L3JIncl3  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRT3j3"),      PDF_TTBAR_VRT1L3JIncl3  ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VRW3j3"),      PDF_TTBAR_VRW1L3JIncl3  ))
        pass
    if chn==2:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Ba"),      PDF_TTBAR_SR1L2Ba      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Ba"),     PDF_TTBAR_CRW1L2Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Ba"),     PDF_TTBAR_CRT1L2Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     PDF_TTBAR_VR1L2Ba     ))
        pass
    if chn==3:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L2Bc"),      PDF_TTBAR_SR1L2Bc      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L2Bc"),     PDF_TTBAR_CRW1L2Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L2Bc"),     PDF_TTBAR_CRT1L2Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     PDF_TTBAR_VR1L2Bc     ))
        pass
    if chn==4:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Ba"),      PDF_TTBAR_SR1L1Ba      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Ba"),     PDF_TTBAR_CRW1L1Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Ba"),     PDF_TTBAR_CRT1L1Ba     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     PDF_TTBAR_VR1L1Ba1     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     PDF_TTBAR_VR1L1Ba2     ))
        pass
    if chn==5:
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR1L1Bc"),      PDF_TTBAR_SR1L1Bc      ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRW1L1Bc"),     PDF_TTBAR_CRW1L1Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT1L1Bc"),     PDF_TTBAR_CRT1L1Bc     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),     PDF_TTBAR_VR1L1Bc1     ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),     PDF_TTBAR_VR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"SR2La"),         PDF_TTBAR_SR2MU         ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"CRT"),        PDF_TTBAR_CRT2MU        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR1"),        PDF_TTBAR_VR2MU1        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR2"),        PDF_TTBAR_VR2MU2        ))
        generatorSyst.append((("PowhegPythiaTTbar",prefixTheo+"VR3"),        PDF_TTBAR_VR2MU3        ))
        pass
##TTV
    if chn==0:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L3j"),       PDF_TTBARV_SR1L3J       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        PDF_TTBARV_CRT1L3J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),        PDF_TTBARV_CRW1L3J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j1"),        PDF_TTBARV_VR1L3J1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j1"),       PDF_TTBARV_VRT1L3J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j1"),       PDF_TTBARV_VRW1L3J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j2"),        PDF_TTBARV_VR1L3J2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j2"),       PDF_TTBARV_VRT1L3J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j2"),       PDF_TTBARV_VRW1L3J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j3"),        PDF_TTBARV_VR1L3J3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j3"),       PDF_TTBARV_VRT1L3J3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j3"),       PDF_TTBARV_VRW1L3J3       ))
        pass
    if chn==7:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L5j"),       PDF_TTBARV_SR1L5J       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        PDF_TTBARV_CRT1L5J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),        PDF_TTBARV_CRW1L5J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j1"),        PDF_TTBARV_VR1L5J1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j1"),       PDF_TTBARV_VRT1L5J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j1"),       PDF_TTBARV_VRW1L5J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j2"),        PDF_TTBARV_VR1L5J2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j2"),       PDF_TTBARV_VRT1L5J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j2"),       PDF_TTBARV_VRW1L5J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j3"),        PDF_TTBARV_VR1L5J3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j3"),       PDF_TTBARV_VRT1L5J3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j3"),       PDF_TTBARV_VRW1L5J3       ))
        pass
    
    if chn==1:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L3j"),       PDF_TTBARV_SR1L3J       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        PDF_TTBARV_CRT1L3J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),        PDF_TTBARV_CRW1L3J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L5j"),       PDF_TTBARV_SR1L5J       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT5j"),        PDF_TTBARV_CRT1L5J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW5j"),        PDF_TTBARV_CRW1L5J        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j1"),        PDF_TTBARV_VR1L3J1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j1"),       PDF_TTBARV_VRT1L3J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j1"),       PDF_TTBARV_VRW1L3J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j2"),        PDF_TTBARV_VR1L3J2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j2"),       PDF_TTBARV_VRT1L3J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j2"),       PDF_TTBARV_VRW1L3J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j3"),        PDF_TTBARV_VR1L3J3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j3"),       PDF_TTBARV_VRT1L3J3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j3"),       PDF_TTBARV_VRW1L3J3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j1"),        PDF_TTBARV_VR1L5J1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j1"),       PDF_TTBARV_VRT1L5J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j1"),       PDF_TTBARV_VRW1L5J1       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j2"),        PDF_TTBARV_VR1L5J2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j2"),       PDF_TTBARV_VRT1L5J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j2"),       PDF_TTBARV_VRW1L5J2       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR5j3"),        PDF_TTBARV_VR1L5J3        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT5j3"),       PDF_TTBARV_VRT1L5J3       ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW5j3"),       PDF_TTBARV_VRW1L5J3       ))
        pass
    if chn==9:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L3j"),      PDF_TTBARV_SR1L3JIncl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),         PDF_TTBARV_CRT1L3JIncl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW"),         PDF_TTBARV_CRW1L3JIncl  ))
            #    generatorSystTTV.append((("ttbarV",prefixTheo+"VSR"),         PDF_TTBARV_VSRIncl  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j1"),       PDF_TTBARV_VR1L3JIncl1  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j1"),      PDF_TTBARV_VRT1L3JIncl1  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j1"),      PDF_TTBARV_VRW1L3JIncl1  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j2"),       PDF_TTBARV_VR1L3JIncl2  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j2"),      PDF_TTBARV_VRT1L3JIncl2  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j2"),      PDF_TTBARV_VRW1L3JIncl2  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3j3"),       PDF_TTBARV_VR1L3JIncl3  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRT3j3"),      PDF_TTBARV_VRT1L3JIncl3  ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VRW3j3"),      PDF_TTBARV_VRW1L3JIncl3  ))
        pass
    if chn==2:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L2Ba"),      PDF_TTBARV_SR1L2Ba      ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L2Ba"),     PDF_TTBARV_CRW1L2Ba     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L2Ba"),     PDF_TTBARV_CRT1L2Ba     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     PDF_TTBARV_VR1L2Ba     ))
        pass
    if chn==3:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L2Bc"),      PDF_TTBARV_SR1L2Bc      ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L2Bc"),     PDF_TTBARV_CRW1L2Bc     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L2Bc"),     PDF_TTBARV_CRT1L2Bc     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     PDF_TTBARV_VR1L2Bc     ))
        pass
    if chn==4:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L1Ba"),      PDF_TTBARV_SR1L1Ba      ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L1Ba"),     PDF_TTBARV_CRW1L1Ba     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L1Ba"),     PDF_TTBARV_CRT1L1Ba     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     PDF_TTBARV_VR1L1Ba1     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR2"),     PDF_TTBARV_VR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR1L1Bc"),      PDF_TTBARV_SR1L1Bc      ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRW1L1Bc"),     PDF_TTBARV_CRW1L1Bc     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT1L1Bc"),     PDF_TTBARV_CRT1L1Bc     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),     PDF_TTBARV_VR1L1Bc1     ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR2"),     PDF_TTBARV_VR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystTTV.append((("ttbarV",prefixTheo+"SR2La"),         PDF_TTBARV_SR2MU         ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"CRT"),        PDF_TTBARV_CRT2MU        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR1"),        PDF_TTBARV_VR2MU1        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR2"),        PDF_TTBARV_VR2MU2        ))
        generatorSystTTV.append((("ttbarV",prefixTheo+"VR3"),        PDF_TTBARV_VR2MU3        ))
        pass        
##ST
    if chn==0:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L3j"),       PDF_SINGLETOP_SR1L3J       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),        PDF_SINGLETOP_CRT1L3J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),        PDF_SINGLETOP_CRW1L3J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j1"),        PDF_SINGLETOP_VR1L3J1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j1"),       PDF_SINGLETOP_VRT1L3J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j1"),       PDF_SINGLETOP_VRW1L3J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j2"),        PDF_SINGLETOP_VR1L3J2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j2"),       PDF_SINGLETOP_VRT1L3J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j2"),       PDF_SINGLETOP_VRW1L3J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j3"),        PDF_SINGLETOP_VR1L3J3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j3"),       PDF_SINGLETOP_VRT1L3J3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j3"),       PDF_SINGLETOP_VRW1L3J3       ))
        pass
    if chn==7:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L5j"),       PDF_SINGLETOP_SR1L5J       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),        PDF_SINGLETOP_CRT1L5J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),        PDF_SINGLETOP_CRW1L5J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR5j1"),        PDF_SINGLETOP_VR1L5J1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j1"),       PDF_SINGLETOP_VRT1L5J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j1"),       PDF_SINGLETOP_VRW1L5J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR5j2"),        PDF_SINGLETOP_VR1L5J2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j2"),       PDF_SINGLETOP_VRT1L5J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j2"),       PDF_SINGLETOP_VRW1L5J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR5j3"),        PDF_SINGLETOP_VR1L5J3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j3"),       PDF_SINGLETOP_VRT1L5J3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j3"),       PDF_SINGLETOP_VRW1L5J3       ))
        pass
    
    if chn==1:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L3j"),       PDF_SINGLETOP_SR1L3J       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),        PDF_SINGLETOP_CRT1L3J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),        PDF_SINGLETOP_CRW1L3J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L5j"),       PDF_SINGLETOP_SR1L5J       ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT5j"),        PDF_SINGLETOP_CRT1L5J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW5j"),        PDF_SINGLETOP_CRW1L5J        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j1"),        PDF_SINGLETOP_VR1L3J1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j1"),       PDF_SINGLETOP_VRT1L3J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j1"),       PDF_SINGLETOP_VRW1L3J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j2"),        PDF_SINGLETOP_VR1L3J2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j2"),       PDF_SINGLETOP_VRT1L3J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j2"),       PDF_SINGLETOP_VRW1L3J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j3"),        PDF_SINGLETOP_VR1L3J3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j3"),       PDF_SINGLETOP_VRT1L3J3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j3"),       PDF_SINGLETOP_VRW1L3J3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR5j1"),        PDF_SINGLETOP_VR1L5J1        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j1"),       PDF_SINGLETOP_VRT1L5J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j1"),       PDF_SINGLETOP_VRW1L5J1       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR5j2"),        PDF_SINGLETOP_VR1L5J2        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j2"),       PDF_SINGLETOP_VRT1L5J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j2"),       PDF_SINGLETOP_VRW1L5J2       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR5j3"),        PDF_SINGLETOP_VR1L5J3        ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT5j3"),       PDF_SINGLETOP_VRT1L5J3       ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW5j3"),       PDF_SINGLETOP_VRW1L5J3       ))
        pass
    if chn==9:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L3j"),      PDF_SINGLETOP_SR1L3JIncl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT"),         PDF_SINGLETOP_CRT1L3JIncl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW"),         PDF_SINGLETOP_CRW1L3JIncl  ))
        #    generatorSystST.append((("SingleTop",prefixTheo+"VSR"),         PDF_SINGLETOP_VSRIncl  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j1"),       PDF_SINGLETOP_VR1L3JIncl1  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j1"),      PDF_SINGLETOP_VRT1L3JIncl1  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j1"),      PDF_SINGLETOP_VRW1L3JIncl1  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j2"),       PDF_SINGLETOP_VR1L3JIncl2  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j2"),      PDF_SINGLETOP_VRT1L3JIncl2  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j2"),      PDF_SINGLETOP_VRW1L3JIncl2  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR3j3"),       PDF_SINGLETOP_VR1L3JIncl3  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRT3j3"),      PDF_SINGLETOP_VRT1L3JIncl3  ))
        generatorSystST.append((("SingleTop",prefixTheo+"VRW3j3"),      PDF_SINGLETOP_VRW1L3JIncl3  ))
        pass
    if chn==2:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L2Ba"),      PDF_SINGLETOP_SR1L2Ba      ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L2Ba"),     PDF_SINGLETOP_CRW1L2Ba     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L2Ba"),     PDF_SINGLETOP_CRT1L2Ba     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     PDF_SINGLETOP_VR1L2Ba     ))
        pass
    if chn==3:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L2Bc"),      PDF_SINGLETOP_SR1L2Bc      ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L2Bc"),     PDF_SINGLETOP_CRW1L2Bc     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L2Bc"),     PDF_SINGLETOP_CRT1L2Bc     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     PDF_SINGLETOP_VR1L2Bc     ))
        pass
    if chn==4:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L1Ba"),      PDF_SINGLETOP_SR1L1Ba      ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L1Ba"),     PDF_SINGLETOP_CRW1L1Ba     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L1Ba"),     PDF_SINGLETOP_CRT1L1Ba     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     PDF_SINGLETOP_VR1L1Ba1     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR2"),     PDF_SINGLETOP_VR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystST.append((("SingleTop",prefixTheo+"SR1L1Bc"),      PDF_SINGLETOP_SR1L1Bc      ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRW1L1Bc"),     PDF_SINGLETOP_CRW1L1Bc     ))
        generatorSystST.append((("SingleTop",prefixTheo+"CRT1L1Bc"),     PDF_SINGLETOP_CRT1L1Bc     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR1"),     PDF_SINGLETOP_VR1L1Bc1     ))
        generatorSystST.append((("SingleTop",prefixTheo+"VR2"),     PDF_SINGLETOP_VR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"SR2La"),         PDF_SINGLETOP_SR2MU         ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"CRT"),        PDF_SINGLETOP_CRT2MU        ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"VR1"),        PDF_SINGLETOP_VR2MU1        ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"VR2"),        PDF_SINGLETOP_VR2MU2        ))
        generatorSystST.append((("SingleTopDiLept",prefixTheo+"VR3"),        PDF_SINGLETOP_VR2MU3        ))
        pass
##DB
    if chn==0:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L3j"),       PDF_DIBOSONS_SR1L3J       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),        PDF_DIBOSONS_CRT1L3J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),        PDF_DIBOSONS_CRW1L3J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j1"),        PDF_DIBOSONS_VR1L3J1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j1"),       PDF_DIBOSONS_VRT1L3J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j1"),       PDF_DIBOSONS_VRW1L3J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j2"),        PDF_DIBOSONS_VR1L3J2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j2"),       PDF_DIBOSONS_VRT1L3J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j2"),       PDF_DIBOSONS_VRW1L3J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j3"),        PDF_DIBOSONS_VR1L3J3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j3"),       PDF_DIBOSONS_VRT1L3J3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j3"),       PDF_DIBOSONS_VRW1L3J3       ))
        pass
    if chn==7:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L5j"),       PDF_DIBOSONS_SR1L5J       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),        PDF_DIBOSONS_CRT1L5J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),        PDF_DIBOSONS_CRW1L5J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j1"),        PDF_DIBOSONS_VR1L5J1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j1"),       PDF_DIBOSONS_VRT1L5J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j1"),       PDF_DIBOSONS_VRW1L5J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j2"),        PDF_DIBOSONS_VR1L5J2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j2"),       PDF_DIBOSONS_VRT1L5J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j2"),       PDF_DIBOSONS_VRW1L5J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j3"),        PDF_DIBOSONS_VR1L5J3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j3"),       PDF_DIBOSONS_VRT1L5J3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j3"),       PDF_DIBOSONS_VRW1L5J3       ))
        pass
    
    if chn==1:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L3j"),       PDF_DIBOSONS_SR1L3J       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),        PDF_DIBOSONS_CRT1L3J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),        PDF_DIBOSONS_CRW1L3J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L5j"),       PDF_DIBOSONS_SR1L5J       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT5j"),        PDF_DIBOSONS_CRT1L5J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW5j"),        PDF_DIBOSONS_CRW1L5J        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j1"),        PDF_DIBOSONS_VR1L3J1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j1"),       PDF_DIBOSONS_VRT1L3J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j1"),       PDF_DIBOSONS_VRW1L3J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j2"),        PDF_DIBOSONS_VR1L3J2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j2"),       PDF_DIBOSONS_VRT1L3J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j2"),       PDF_DIBOSONS_VRW1L3J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j3"),        PDF_DIBOSONS_VR1L3J3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j3"),       PDF_DIBOSONS_VRT1L3J3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j3"),       PDF_DIBOSONS_VRW1L3J3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j1"),        PDF_DIBOSONS_VR1L5J1        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j1"),       PDF_DIBOSONS_VRT1L5J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j1"),       PDF_DIBOSONS_VRW1L5J1       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j2"),        PDF_DIBOSONS_VR1L5J2        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j2"),       PDF_DIBOSONS_VRT1L5J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j2"),       PDF_DIBOSONS_VRW1L5J2       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR5j3"),        PDF_DIBOSONS_VR1L5J3        ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT5j3"),       PDF_DIBOSONS_VRT1L5J3       ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW5j3"),       PDF_DIBOSONS_VRW1L5J3       ))
        pass
    if chn==9:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L3j"),      PDF_DIBOSONS_SR1L3JIncl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT"),         PDF_DIBOSONS_CRT1L3JIncl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW"),         PDF_DIBOSONS_CRW1L3JIncl  ))
        #    generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VSR"),         PDF_DIBOSONS_VSRIncl  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j1"),       PDF_DIBOSONS_VR1L3JIncl1  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j1"),      PDF_DIBOSONS_VRT1L3JIncl1  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j1"),      PDF_DIBOSONS_VRW1L3JIncl1  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j2"),       PDF_DIBOSONS_VR1L3JIncl2  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j2"),      PDF_DIBOSONS_VRT1L3JIncl2  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j2"),      PDF_DIBOSONS_VRW1L3JIncl2  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR3j3"),       PDF_DIBOSONS_VR1L3JIncl3  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRT3j3"),      PDF_DIBOSONS_VRT1L3JIncl3  ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VRW3j3"),      PDF_DIBOSONS_VRW1L3JIncl3  ))
        pass
    if chn==2:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L2Ba"),      PDF_DIBOSONS_SR1L2Ba      ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L2Ba"),     PDF_DIBOSONS_CRW1L2Ba     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L2Ba"),     PDF_DIBOSONS_CRT1L2Ba     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     PDF_DIBOSONS_VR1L2Ba     ))
        pass
    if chn==3:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L2Bc"),      PDF_DIBOSONS_SR1L2Bc      ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L2Bc"),     PDF_DIBOSONS_CRW1L2Bc     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L2Bc"),     PDF_DIBOSONS_CRT1L2Bc     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     PDF_DIBOSONS_VR1L2Bc     ))
        pass
    if chn==4:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L1Ba"),      PDF_DIBOSONS_SR1L1Ba      ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L1Ba"),     PDF_DIBOSONS_CRW1L1Ba     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L1Ba"),     PDF_DIBOSONS_CRT1L1Ba     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     PDF_DIBOSONS_VR1L1Ba1     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR2"),     PDF_DIBOSONS_VR1L1Ba2     ))
        pass
    if chn==5:
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"SR1L1Bc"),      PDF_DIBOSONS_SR1L1Bc      ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRW1L1Bc"),     PDF_DIBOSONS_CRW1L1Bc     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"CRT1L1Bc"),     PDF_DIBOSONS_CRT1L1Bc     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR1"),     PDF_DIBOSONS_VR1L1Bc1     ))
        generatorSystDB.append((("SherpaDibosonsMassiveBC",prefixTheo+"VR2"),     PDF_DIBOSONS_VR1L1Bc2     ))
        pass
    if (chn==6 or chn==8):
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"SR2La"),      PDF_DIBOSONS_SR2MU         ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"CRT"),        PDF_DIBOSONS_CRT2MU        ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"VR1"),        PDF_DIBOSONS_VR2MU1        ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"VR2"),        PDF_DIBOSONS_VR2MU2        ))
        generatorSystDB.append((("PowhegDibosons",prefixTheo+"VR3"),        PDF_DIBOSONS_VR2MU3        ))
        pass
#########################################################################################################
#########################################################################################################
########### Finish the inerator above ######################    


############################END PDF UNC

generatorSyst += generatorSystW
generatorSyst += generatorSystZ
generatorSyst += generatorSystTTV
generatorSyst += generatorSystDB
generatorSyst += generatorSystST


SystList = []
if True:
    if doMPJES:
        SystList.append(jes_EffectiveNP_1)
        SystList.append(jes_EffectiveNP_2)
        SystList.append(jes_EtaIntercalibration_Modelling)        
        SystList.append(jes_PileupRhoTopology)
        SystList.append(jes_FlavorCompUncert)
        SystList.append(jes_FlavorResponseUncert)
        SystList.append(jes_BJes)
        SystList.append(jes_JVF)
        if doSmallJESParameters:
            SystList.append(jes_EffectiveNP_3)
            SystList.append(jes_EffectiveNP_4)
            SystList.append(jes_EffectiveNP_5)
            SystList.append(jes_EffectiveNP_6)
            SystList.append(jes_EtaIntercalibration_StatAndMethod)
            SystList.append(jes_PileupOffsetTermMu)
            SystList.append(jes_PileupPtTerm)
            SystList.append(jes_PileupOffsetTermNPV)
            SystList.append(jes_RelativeNonClosure_MCTYPE)
            SystList.append(jes_SingleParticle_HighPt)
    else:
        SystList.append(jes)

    SystList.append(jer)
    SystList.append(lepEff)
    SystList.append(scalest)
    SystList.append(resost)
    SystList.append(pileup)
    SystList.append(trEff)
    #SystList.append(egzee)
    #SystList.append(egmat)
    #SystList.append(egps)
    #SystList.append(eglow)
    #SystList.append(egres)
    #SystList.append(merm)
    #SystList.append(meri)
    #SystList.append(mes)

## do these per sample/channel
#SystList.append(bTagSyst)
#SystList.append(qfacT)
#SystList.append(qfacW)
#SystList.append(ktfacT)
#SystList.append(ktfacW)
#SystList.append(iqoptW)

#--------------------------------------------------------------------------
# List of channel selections
#--------------------------------------------------------------------------

TriggerSelection = "&& (AnalysisType==5) "
LeptonSelection  = TriggerSelection + "&& (lep1Pt<25) && ((lep1Pt>7&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))"
HardLeptonSelection = "&& (lep1Pt>25) && (lep2Pt<7) && ((lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Flavor==-1))"
HardAndSoftLeptonSelection = "&&  (((lep1Pt>25) && (lep2Pt<7) && ((lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Flavor==-1))) || ((AnalysisType==5) && (lep1Pt<25) && ((lep1Pt>7&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))))"
ELSelection = " && lep1Flavor==1"
MUSelection = " && lep1Flavor==-1"

if chn==0:

    LeptonSelection                  += " && dRminLepJet20_JVF25pt50>1.0 "

    #Jet3Selection                    = " && jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=3 && jet5Pt_JVF25pt50<25 "
    Jet3Selection                    = " && jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=3 && nJet25_JVF25pt50<5 "

    if not doShapeFit:#for bkg table# combined-emu-using exclusion SR; seperated e/mu using discovery SR
        configMgr.cutsDict[prefix+"SR1L3j"]     = "( met>400 && mt>100  )"             + Jet3Selection + LeptonSelection
        if doELMU:#for discovery check
            configMgr.cutsDict[prefix+"ELSR1L3j"]     = "( met>400 && mt>100  && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"MUSR1L3j"]     = "( met>400 && mt>100  && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet3Selection + LeptonSelection + MUSelection

            configMgr.cutsDict[prefix+"ELVRT3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT3j2"]      = "( met>250 && met<350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRT3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRT3j2"]      = "( met>250 && met<350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRT3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection+ MUSelection

            configMgr.cutsDict[prefix+"ELVRW3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW3j2"]      = "( met>250 && met<350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRW3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRW3j2"]      = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0)"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRW3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ MUSelection
 
            pass
    else:
        configMgr.cutsDict[prefix+"SR1L3j"]     = "( met>400 && mt>100 )"             + Jet3Selection + LeptonSelection

    configMgr.cutsDict[prefix+"CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict[prefix+"CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict[prefix+"VSR"]        = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
  
    pass
    #
elif chn==7:

    # no dRmin for 5-jet (not the ISR analysis)
    #Jet5Selection                    = "&& (jet1Pt_JVF25pt50>180 && jet5Pt_JVF25pt50>25)"   #no dRmin for 5Jet
    Jet5Selection                    = "&& (jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=5 )"
    # 
    if not doShapeFit:
        configMgr.cutsDict[prefix+"SR1L5j"]     = "( met>300 && mt>100 )"             + Jet5Selection + LeptonSelection
        if doELMU:
            configMgr.cutsDict[prefix+"ELSR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"MUSR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet5Selection + LeptonSelection + MUSelection

            configMgr.cutsDict[prefix+"ELVRT5j1"]      = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT5j2"]      = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT5j3"]      = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRT5j1"]      = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRT5j2"]      = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRT5j3"]      = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + MUSelection

            configMgr.cutsDict[prefix+"ELVRW5j1"]      = "( met>180 && met<250 && mt>80  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW5j2"]      = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW5j3"]      = "( met>350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRW5j1"]      = "( met>180 && met<250 && mt>80  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRW5j2"]      = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRW5j3"]      = "( met>350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + MUSelection

            pass
    else:
        configMgr.cutsDict[prefix+"SR1L5j"]     = "( met>300 && mt>100 )"             + Jet5Selection + LeptonSelection

    configMgr.cutsDict[prefix+"CRT"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict[prefix+"CRW"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict[prefix+"VSR"]        = "( met>250 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet5Selection + LeptonSelection
            
    pass

elif chn==9:
    #Jet3Selection                    =  "&&  (jet1Pt_JVF25pt50>130) && (jet2Pt_JVF25pt50>100) && (jet3Pt_JVF25pt50>25) "
    Jet3Selection                    =  "&&  (jet1Pt_JVF25pt50>130) && (jet2Pt_JVF25pt50>100) && (nJet25_JVF25pt50>=3) "
    

    BVetoSelection                   = "&& (nBJet25_MV1_60p_JVF25pt50==0)"
    if not doShapeFit:
          configMgr.cutsDict[prefix+"SR1L3j"]     = "( met>180 && mt>120 )"             + Jet3Selection + LeptonSelection + BVetoSelection
          if doELMU:
              configMgr.cutsDict[prefix+"ELSR1L3j"]     = "( met>180 && mt>120 )"             + Jet3Selection + LeptonSelection + BVetoSelection + ELSelection
              configMgr.cutsDict[prefix+"MUSR1L3j"]     = "( met>180 && mt>120 )"             + Jet3Selection + LeptonSelection + BVetoSelection + MUSelection
   
              configMgr.cutsDict[prefix+"ELVRT3j1"]      = "( met>180 && met<250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50>0  )"  + Jet3Selection + LeptonSelection+ ELSelection
              configMgr.cutsDict[prefix+"ELVRT3j2"]      = "( met>250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection+ ELSelection
              configMgr.cutsDict[prefix+"ELVRT3j3"]      = "( met>250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection+ ELSelection

              configMgr.cutsDict[prefix+"MUVRT3j1"]      = "( met>180 && met<250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50>0  )"  + Jet3Selection + LeptonSelection + MUSelection
              configMgr.cutsDict[prefix+"MUVRT3j2"]      = "( met>250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection + MUSelection
              configMgr.cutsDict[prefix+"MUVRT3j3"]      = "( met>250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection + MUSelection

              configMgr.cutsDict[prefix+"ELVRW3j1"]      = "( met>180 && met<250 && mt>80 && mt<120  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ ELSelection
              configMgr.cutsDict[prefix+"ELVRW3j2"]      = "( met>250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ ELSelection
              configMgr.cutsDict[prefix+"ELVRW3j3"]      = "( met>250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ ELSelection

              configMgr.cutsDict[prefix+"MUVRW3j1"]      = "( met>180 && met<250 && mt>80 && mt<120  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + MUSelection
              configMgr.cutsDict[prefix+"MUVRW3j2"]      = "( met>250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + MUSelection
              configMgr.cutsDict[prefix+"MUVRW3j3"]      = "( met>250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + MUSelection


              pass
    else:
        configMgr.cutsDict[prefix+"SR1L3j"]     = "( met>180 && mt>120 )"             + Jet3Selection + LeptonSelection + BVetoSelection
        
    configMgr.cutsDict[prefix+"CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"   + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict[prefix+"VSR"]        = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j1"]      = "( met>180 && met<250 && mt>80 && mt<120 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j1"]     = "( met>180 && met<250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j1"]     = "( met>180 && met<250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j2"]      = "( met>250 && mt>80 && mt<120)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j2"]     = "( met>250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j2"]     = "( met>250 && mt>80 && mt<120 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j3"]      = "( met>250 && mt>40 && mt<80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j3"]     = "( met>250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j3"]     = "( met>250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
 
    pass

elif chn==1:

    #Jet3Selection                    = "&& jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=3 && jet5Pt_JVF25pt50<25 && dRminLepJet20_JVF25pt50>1.0"
    Jet3Selection                    = "&& jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=3 && nJet25_JVF25pt50<5  && dRminLepJet20_JVF25pt50>1.0"
    
    #Jet5Selection                    = "&& (jet1Pt_JVF25pt50>180 && jet5Pt_JVF25pt50>25)"  #no dRmin for 5Jet
    Jet5Selection                    = "&& (jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=5)"  #no dRmin for 5Jet  
    #
    if not doShapeFit:
        configMgr.cutsDict[prefix+"SR1L3j"]     = "( met>400 && mt>100 )"             + Jet3Selection + LeptonSelection
        configMgr.cutsDict[prefix+"SR1L5j"]     = "( met>300 && mt>100 )"             + Jet5Selection + LeptonSelection
        if doELMU:
            configMgr.cutsDict[prefix+"ELSR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3)"             + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELSR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3)"             + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"MUSR1L3j"]     = "( met>400 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3)"             + Jet3Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUSR1L5j"]     = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3)"             + Jet5Selection + LeptonSelection + MUSelection
        
            #3J
            configMgr.cutsDict[prefix+"ELVRT3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT3j2"]      = "( met>250 && met<350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRT3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRT3j2"]      = "( met>250 && met<350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRT3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50>0)"  + Jet3Selection + LeptonSelection+ MUSelection

            configMgr.cutsDict[prefix+"ELVRW3j1"]      = "( met>180 && met<250 && mt>80&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW3j2"]      = "( met>250 && met<350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRW3j1"]      = "( met>180 && met<250 && mt>80  && nBJet25_MV1_60p_JVF25pt50==0)"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRW3j2"]      = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ MUSelection
            configMgr.cutsDict[prefix+"MUVRW3j3"]      = "( met>350 && mt>40 && mt<100&& nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection+ MUSelection
 
            #5J
            configMgr.cutsDict[prefix+"ELVRT5j1"]      = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT5j2"]      = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRT5j3"]      = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRT5j1"]      = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRT5j2"]      = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRT5j3"]      = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection + MUSelection

            configMgr.cutsDict[prefix+"ELVRW5j1"]      = "( met>180 && met<250 && mt>80  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW5j2"]      = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + ELSelection
            configMgr.cutsDict[prefix+"ELVRW5j3"]      = "( met>350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + ELSelection

            configMgr.cutsDict[prefix+"MUVRW5j1"]      = "( met>180 && met<250 && mt>80  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRW5j2"]      = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + MUSelection
            configMgr.cutsDict[prefix+"MUVRW5j3"]      = "( met>350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection + MUSelection

            pass
    else:
        configMgr.cutsDict[prefix+"SR1L3j"]     = "( met>400 && mt>100 )"             + Jet3Selection + LeptonSelection
        configMgr.cutsDict[prefix+"SR1L5j"]     = "( met>300 && mt>100 )"             + Jet5Selection + LeptonSelection

    configMgr.cutsDict[prefix+"CRT"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict[prefix+"CRW"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"CRT5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict[prefix+"CRW5j"]      = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_60p_JVF25pt50==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict[prefix+"VSR"]        = "( met>300 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VSR5j"]      = "( met>250 && mt>100 && (met/(meffInc25_JVF25pt50))>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict[prefix+"VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict[prefix+"VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_60p_JVF25pt50==0 )" + Jet5Selection + LeptonSelection
  
    pass
    #

elif chn==2:
    JetSelection                  = "&& (jet2Pt_JVF25pt50>60 && nJet50_JVF25pt50_wo2jets==0 && dphimin>0.4)"
    BJetSelection                 = "&& nB2Jet60_JVF25pt50==2" 
    BVetoSelection                = "&& nB2Jet60_JVF25pt50==0"

    if not doShapeFit:
        configMgr.cutsDict[prefix+"SR1L2Ba"] = "(aMT2_BM_JVF25pt50 > 170 && mjj_JVF25pt50>150 && met>150 && ht_wo2jets_JVF25pt50<50)" + JetSelection + BJetSelection                  +     LeptonSelection
    else:
        configMgr.cutsDict[prefix+"SR1L2Ba"] = "(mjj_JVF25pt50>150 && met>150 && ht_wo2jets_JVF25pt50<50)" + JetSelection + BJetSelection                  +     LeptonSelection    
    
    configMgr.cutsDict[prefix+"CRW1L2Ba"]     = "(mt > 40 && mt < 80 && met>150 && ht_wo2jets_JVF25pt50<50)" + JetSelection                 + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict[prefix+"CRT1L2Ba"]     = "(           met>150 && ht_wo2jets_JVF25pt50<50)" + JetSelection + BJetSelection                  + HardLeptonSelection
    configMgr.cutsDict[prefix+"VR1"]     = "(mjj_JVF25pt50<150 && met>150 && ht_wo2jets_JVF25pt50<50)" + JetSelection + BJetSelection                  +     LeptonSelection
    configMgr.cutsDict[prefix+"VSR"]     = "(           met>150 && ht_wo2jets_JVF25pt50<50)" + JetSelection + BJetSelection                  +     LeptonSelection
    pass

elif chn==3:
    JetSelection                  = "&& (jet2Pt_JVF25pt50>60 && dphimin>0.4)"
    BJetSelection                 = "&& nB2Jet60_JVF25pt50==2"
    BVetoSelection                = "&& nB2Jet60_JVF25pt50==0"

    if not doShapeFit:
        configMgr.cutsDict[prefix+"SR1L2Bc"] = "(aMT2_BM_JVF25pt50 > 200 && mjj_JVF25pt50>150 && met>250)" + JetSelection + BJetSelection                  +     LeptonSelection
    else:
        configMgr.cutsDict[prefix+"SR1L2Bc"] = "(mjj_JVF25pt50>150 && met>250)" + JetSelection + BJetSelection                  +     LeptonSelection
        
    configMgr.cutsDict[prefix+"CRW1L2Bc"]     = "(mt > 40 && mt < 80 && met>250)" + JetSelection                 + BVetoSelection + HardLeptonSelection 
    configMgr.cutsDict[prefix+"CRT1L2Bc"]     = "(           met>150)" + JetSelection + BJetSelection                  + HardLeptonSelection
    configMgr.cutsDict[prefix+"VR1"]     = "(mjj_JVF25pt50<150 && met>250)" + JetSelection + BJetSelection                  +     LeptonSelection 
    configMgr.cutsDict[prefix+"VSR"]     = "(mjj_JVF25pt50>150 && met>150)" + JetSelection + BJetSelection                  +     LeptonSelection 
    pass

elif chn==4:
    JetSelection                  = "&& (jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=3)" 
    BJetSelection                 = "&& (jet1IsB_MV1_70p_JVF25pt50==0 && nBJet25_MV1_70p_JVF25pt50>0)"
    BVetoSelection                = "&& (nBJet25_MV1_70p_JVF25pt50==0)"

    if not doShapeFit:
        configMgr.cutsDict[prefix+"SR1L1Ba"] = "(met>300            && met/(meffInc25_JVF25pt50)>0.3 && mt>100)"          + JetSelection + BJetSelection                  +     LeptonSelection
    else:
        configMgr.cutsDict[prefix+"SR1L1Ba"] = "(met>300            && met/(meffInc25_JVF25pt50)>0.3 && mt>100)"          + JetSelection + BJetSelection                  +     TriggerSelection + "&& ((lep1Pt>7&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))"
    
    configMgr.cutsDict[prefix+"CRW1L1Ba"]         = "(met>200 && met<250 && mt>100 && mt < 120)"  + JetSelection + BVetoSelection + HardAndSoftLeptonSelection
    configMgr.cutsDict[prefix+"CRT1L1Ba"]         = "(met>200 && met<250 && mt>100 && mt < 120)"  + JetSelection + BJetSelection  + HardAndSoftLeptonSelection
    configMgr.cutsDict[prefix+"VR1"]         = "(met>250 && met<300 && mt>100)"  + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict[prefix+"VR2"]         = "(met>250 && met<300 && mt>100 && met/(meffInc25_JVF25pt50)>0.3)" + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3"]         = "(met>250 && met<300 && mt>100 && met/(meffInc25_JVF25pt50)>0.3)" + JetSelection + BVetoSelection + LeptonSelection
    configMgr.cutsDict[prefix+"VSR"]         = "(met>250 && met/(meffInc25_JVF25pt50)>0.3 && mt>100)"          + JetSelection + BJetSelection                  +     LeptonSelection
    pass

elif chn==5:                            
    JetSelection                  = "&& (jet1Pt_JVF25pt50>180 && nJet25_JVF25pt50>=2)" 
    BJetSelection                 = "&& jet1IsB_MV1_70p_JVF25pt50==0 && nBJet25_MV1_70p_JVF25pt50>0" 
    BVetoSelection                = "&& (nBJet25_MV1_70p_JVF25pt50==0)"
    
    if not doShapeFit:
        configMgr.cutsDict[prefix+"SR1L1Bc"] = "(met>370            && met/(meffInc25_JVF25pt50)>0.35 && mt>90)"           + JetSelection + BJetSelection                  +     LeptonSelection
    else:
        configMgr.cutsDict[prefix+"SR1L1Bc"] = "(met>370            && met/(meffInc25_JVF25pt50)>0.35 && mt>90)"           + JetSelection + BJetSelection                  +     TriggerSelection + "&& ((lep1Pt>7&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))"

    configMgr.cutsDict[prefix+"CRW1L1Bc"]         = "(met>200 && met<250 && mt>90 && mt < 120)"  + JetSelection + BVetoSelection + HardAndSoftLeptonSelection
    configMgr.cutsDict[prefix+"CRT1L1Bc"]         = "(met>200 && met<250 && mt>90 && mt < 120)"  + JetSelection + BJetSelection  + HardAndSoftLeptonSelection
    configMgr.cutsDict[prefix+"VR1"]         = "(met>250 && met<370 && mt>90)"  + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict[prefix+"VR2"]         = "(met>250 && met<370 && mt>90 && met/(meffInc25_JVF25pt50)>0.35)"  + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict[prefix+"VR3"]         = "(met>250 && met<370 && mt>90 && met/(meffInc25_JVF25pt50)>0.35)"  + JetSelection + BVetoSelection + LeptonSelection
    configMgr.cutsDict[prefix+"VSR"]         = "(met>250            && met/(meffInc25_JVF25pt50)>0.35 && mt>90)"           + JetSelection + BJetSelection                  +     LeptonSelection
    pass





elif chn==6: # soft 2-lepton analysis
    DiLeptonSelection          = TriggerSelection + "&& (lep1Pt>6 && lep2Pt>6 &&  lep3Pt<0) && (lep1Flavor==-1) && (lep2Flavor==-1) && (lep1Charge*lep2Charge<0) && mll>15 && abs(mll-91.2)>10 " ## os dimuon
    DiLeptonSelection          += " && dRminLep2Jet20_JVF25pt50>1.0 && mt>40 " # && jet3Pt<50
## old:   JetSelection               = "&& (jet1Pt_JVF25pt50>100 && jet2Pt_JVF25pt50>25)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)"
    JetSelection               = "&& (jet1Pt_JVF25pt50>80 && jet2Pt_JVF25pt50>25)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)"
    # these B-jet selections are okay
    BVetoSelection             = "&& nB3Jet25_MV1_80p_JVF25pt50==0 " 
    BJetSelection              = "&& nB3Jet25_MV1_80p_JVF25pt50>0  " 



    ##small LR SR
    configMgr.cutsDict[prefix+"SR2La"]  = "(met>180 && mll<60 && lep2Pt>6 && lep1Pt<25 && met/(meffInc25_JVF25pt50)>0.3)" + JetSelection + BVetoSelection + DiLeptonSelection
    #configMgr.cutsDict[prefix+"VSR"]   = "(met>150 && mll<60 && lep2Pt>6 && lep1Pt<25)" + JetSelection + BVetoSelection + DiLeptonSelection    
    configMgr.cutsDict[prefix+"CRW"]   = "(met>180 && mll>60 && lep2Pt>6 && lep1Pt>25)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"CRT"]   = "(met>180 && mll>60 && lep2Pt>6 && lep1Pt>25)" + JetSelection + BJetSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"VR1"]   = "(met>180 && mll<60 && lep2Pt>6 && lep1Pt>25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict[prefix+"VR2"]   = "(met>180 && mll>60 && lep2Pt>6 && lep1Pt>25)" + JetSelection  + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"VR3"]   = "(met>180 && mll>=0 && lep2Pt>6 && lep1Pt<25)" + JetSelection  + BJetSelection + DiLeptonSelection

#####for shape fit
#    configMgr.cutsDict[prefix+"SR2La"]  = "(met>180 && mll>15 && abs(mll-91.2)>10 && mll<60 && lep2Pt>6 && lep1Pt<45 && met/(meffInc25_JVF25pt50)>0.3)" + JetSelection + BVetoSelection + DiLeptonSelection
#    configMgr.cutsDict[prefix+"VSR"]   = "(met>150 && mll>15 && abs(mll-91.2)>10 && mll<60 && lep2Pt>6 && lep1Pt<45)" + JetSelection + BVetoSelection + DiLeptonSelection    
#    configMgr.cutsDict[prefix+"CRW"]   = "(met>180 && mll>15 && abs(mll-91.2)>10 && mll>60 && lep2Pt>6 && lep1Pt>45)" + JetSelection + BVetoSelection + DiLeptonSelection
#    configMgr.cutsDict[prefix+"CRT"]   = "(met>180 && mll>15 && abs(mll-91.2)>10 && mll>60 && lep2Pt>6 && lep1Pt>45)" + JetSelection + BJetSelection + DiLeptonSelection
#    configMgr.cutsDict[prefix+"VR1"]   = "(met>180 && mll>15 && abs(mll-91.2)>10 && mll<60 && lep2Pt>6 && lep1Pt>45)" + JetSelection  + DiLeptonSelection
#    configMgr.cutsDict[prefix+"VR3"]   = "(met>180 && mll>15 && abs(mll-91.2)>10 && mll>60 && lep2Pt>6 && lep1Pt<45)" + JetSelection  + DiLeptonSelection
#    configMgr.cutsDict[prefix+"VR2"]   = "(met>180 && mll>15 && abs(mll-91.2)>10 && mll>60 && lep2Pt>6 && lep1Pt>45)" + JetSelection  + BVetoSelection + DiLeptonSelection



elif chn==8: # soft 2-lepton analysis

##CONF note SR
    DiLeptonSelection          = TriggerSelection + "&& (lep1Pt>6 && lep2Pt>6 &&  lep3Pt<0) && (lep1Flavor==-1) && (lep2Flavor==-1) && (lep1Charge*lep2Charge<0 && mll>15 && abs(mll-91.2)>10)" ## os dimuon
    DiLeptonSelection          += " && dRminLep2Jet20_JVF25pt50>1.0 "
    JetSelection               = "&& (jet1Pt_JVF25pt50>70 && jet2Pt_JVF25pt50>25)" 
    # these B-jet selections are okay
    BVetoSelection             = "&& nB3Jet25_MV1_80p_JVF25pt50==0 " 
    BJetSelection              = "&& nB3Jet25_MV1_80p_JVF25pt50>0  " 
    
    configMgr.cutsDict[prefix+"SR2Lb"]  = "(met>170 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))>80 && lep2Pt>6 && lep1Pt<25)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"VSR"]   = "(met>150 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))<80 && lep2Pt>6 && lep1Pt<25)" + JetSelection + BVetoSelection + DiLeptonSelection    
    configMgr.cutsDict[prefix+"CRW"]   = "(met>170 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))<80 && lep2Pt>6 && lep1Pt>25)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"CRT"]   = "(met>170 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))<80 && lep2Pt>6 && lep1Pt>25)" + JetSelection + BJetSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"VR1"]   = "(met>170 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))>80 && lep2Pt>6 && lep1Pt>25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict[prefix+"VR2"]   = "(met>170 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))<80 && lep2Pt>6 && lep1Pt>25)" + JetSelection  + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict[prefix+"VR3"]   = "(met>170 && sqrt(2*lep2Pt*met*(1-cos(lep2Phi-metPhi)))<80 && lep2Pt>6 && lep1Pt<25)" + JetSelection   + DiLeptonSelection





    pass

#############
## Samples ##
#############

errHigh = 1.3
errLow  = 0.7

## not yet there ...
## to add AlpgenDY and ttbarV
AlpgenDYSample = Sample("AlpgenDY",kOrange-8)
AlpgenDYSample.addSystematic(Systematic(prefix+"err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
AlpgenDYSample.setStatConfig(useStat)
AlpgenDYSample.setFileList(AlpgenDYFiles)
AlpgenDYSample.setNormByTheory()


#5% for WW, 7% for WZ, 5% for ZZ 
if (chn!=6 and chn!=8):
    DibosonsSample = Sample("SherpaDibosonsMassiveBC",kOrange-8)
    dibosonWZsel = "!(DatasetNumber==126892 || DatasetNumber==126894 || DatasetNumber==126895 || DatasetNumber==174834 || DatasetNumber==161962 || DatasetNumber==161963 || DatasetNumber==161966 || DatasetNumber==177997 || DatasetNumber==177999 || DatasetNumber==183586 || DatasetNumber==183588 || DatasetNumber==183590 || DatasetNumber==183734 || DatasetNumber==183736 || DatasetNumber==183738)"
    dibosonWWZZsel = "(DatasetNumber==126892 || DatasetNumber==126894 || DatasetNumber==126895 || DatasetNumber==174834 || DatasetNumber==161962 || DatasetNumber==161963 || DatasetNumber==161966 || DatasetNumber==177997 || DatasetNumber==177999 || DatasetNumber==183586 || DatasetNumber==183588 || DatasetNumber==183590 || DatasetNumber==183734 || DatasetNumber==183736 || DatasetNumber==183738)"

elif (chn==6 or chn==8):
    DibosonsSample = Sample("PowhegDibosons",kOrange-8)
    dibosonWZsel = "!((DatasetNumber>=126928 && DatasetNumber<=126951) || DatasetNumber==178411 || DatasetNumber==178412 || DatasetNumber==178413)"
    dibosonWWZZsel = "((DatasetNumber>=126928 && DatasetNumber<=126951) || DatasetNumber==178411 || DatasetNumber==178412 || DatasetNumber==178413)"

DibosonsSample.addSystematic(Systematic(prefix+"DibosonWWZZXsec", configMgr.weights,configMgr.weights+["( (0.05 * %s) + 1 )" % dibosonWWZZsel],configMgr.weights+["( (-0.05 * %s) + 1 )" % dibosonWWZZsel], "weight","overallSys"))
DibosonsSample.addSystematic(Systematic(prefix+"DibosonWZXsec", configMgr.weights,configMgr.weights+["( (0.07 * %s) + 1 )" % dibosonWZsel],configMgr.weights+["( (-0.07 * %s) + 1 )" % dibosonWZsel], "weight","overallSys"))
#DibosonsSample.addSystematic(Systematic(prefix+"errDB", configMgr.weights, 1.5, 0.5, "user","userOverallSys"))

DibosonsSample.setStatConfig(useStat)
if (chn!=6 and chn!=8):
    DibosonsSample.setFileList(SherpaDibosonsMassiveBCFiles)
elif (chn==6 or chn==8):
    DibosonsSample.setFileList(PowhegDibosonsFiles)
DibosonsSample.setNormByTheory()


ttbarVSample = Sample("ttbarV",kYellow-8)
#Xsec uncertainty: 22% for ttbar+W and ttbar+Z, 50% for ttbar+WW 
ttbarWZsel = "(DatasetNumber==119353 || DatasetNumber==174830 || DatasetNumber==174831 || DatasetNumber==119355 || DatasetNumber==174832 || DatasetNumber==174833)"
ttbarWWsel = "(DatasetNumber==119583)"
ttbarVSample.addSystematic(Systematic(prefix+"ttbarWZXsec", configMgr.weights,configMgr.weights+["( (0.22 * %s) + 1 )" % ttbarWZsel],configMgr.weights+["( (-0.22 * %s) + 1 )" % ttbarWZsel], "weight","overallSys"))
ttbarVSample.addSystematic(Systematic(prefix+"ttbarWWXsec", configMgr.weights,configMgr.weights+["( (0.5 * %s) + 1 )" % ttbarWWsel],configMgr.weights+["( (-0.5 * %s) + 1 )" % ttbarWWsel], "weight","overallSys"))
#ttbarVSample.addSystematic(Systematic(prefix+"errTV", configMgr.weights, errHigh, errLow, "user","userOverallSys"))

ttbarVSample.setStatConfig(useStat)
ttbarVSample.setFileList(ttbarVFiles)
ttbarVSample.setNormByTheory()
#ttbarVSample.addSystematic(pythalp)



SingleTopT = "(DatasetNumber==110101)"
SingleTopS = "(DatasetNumber==110119)"
SingleTopZ = "(DatasetNumber==179991 || DatasetNumber==179992)"
SingleTopW = "0"

if (chn!=6 and chn!=8):
    SingleTopSample = Sample("SingleTop",kGreen-5)
    SingleTopW = "(DatasetNumber==110140)"
    SingleTopSample.addSystematic(Systematic(prefix+"SingleTopTXsec", configMgr.weights, configMgr.weights+["( (0.039 * %s) + 1 )" % SingleTopT],configMgr.weights+["( (-0.022 * %s) + 1 )" % SingleTopT], "weight","overallSys"))
    SingleTopSample.addSystematic(Systematic(prefix+"SingleTopSXsec", configMgr.weights, configMgr.weights+["( (0.039 * %s) + 1 )" % SingleTopS],configMgr.weights+["( (-0.039 * %s) + 1 )" % SingleTopS], "weight","overallSys"))

elif (chn==6 or chn==8):
    SingleTopSample = Sample("SingleTopDiLept",kGreen-5)
    SingleTopW = "(DatasetNumber==110141)"

SingleTopSample.addSystematic(Systematic(prefix+"SingleTopWXsec", configMgr.weights, configMgr.weights+["( (0.068 * %s) + 1 )" % SingleTopW],configMgr.weights+[" ( (-0.068 * %s) + 1 )" % SingleTopW], "weight","overallSys"))
SingleTopSample.addSystematic(Systematic(prefix+"SingleTopZXsec", configMgr.weights, configMgr.weights+["( (0.5 * %s) + 1 )" % SingleTopZ],configMgr.weights+["( (-0.5 * %s) + 1 )" % SingleTopZ], "weight","overallSys"))

#SingleTopSample.addSystematic(Systematic(prefix+"err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setFileList(SingleTopFiles)
SingleTopSample.setNormByTheory()
if chn==6 or chn==8:
    #SingleTopSample.addSampleSpecificWeight("(DatasetNumber==108346)")
    SingleTopSample.addSampleSpecificWeight("(DatasetNumber==110141 || DatasetNumber==179991 || DatasetNumber==179992)")

TTbarSampleName = 'PowhegPythiaTTbar'
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
TTbarSample.setFileList(PowhegPythiaTTbarFiles)
##TTbarSample.addSystematic(pdfIntraSyst)
##TTbarSample.addSystematic(pdfInterSyst)
##TTbarSample.mergeOverallSysSet = [prefix+"pdfIntra",prefix+"pdfInter"] ## post-processing
TTbarSample.addSystematic(tt_pythjim)


if (chn>=0 and chn<=9) and chn!=2 and chn!=3 and chn!=4 and chn!=5 and chn!=6 and chn!=8:
    TTbarSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
elif chn==2:
    TTbarSample.setNormRegions([(prefix+"CRT1L2Ba","cuts")])
elif chn==3:
    TTbarSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
elif chn==4:
    TTbarSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
elif chn==5:
    TTbarSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
elif (chn==6 or chn==8):
    TTbarSample.setNormRegions([(prefix+"CRT","cuts")])
if (chn==6 or chn==8):
    TTbarSample.addSampleSpecificWeight("(DecayIndexTTbar==1)")
    pass

doSherpa = True
if doSherpa and (chn>=0 and chn<=9): # stop
    WSampleName = "SherpaWMassiveBC"
else:
    WSampleName = "AlpgenW"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
##WSample.addSystematic(pdfIntraSyst)
##WSample.addSystematic(pdfInterSyst)
##WSample.mergeOverallSysSet = [prefix+"pdfIntra",prefix+"pdfInter"] ## post-processing
if (chn>=0 and chn<=9) and chn!=2 and chn!=3 and chn!=4 and chn!=5 and chn!=6 and chn!=8:
    WSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
elif chn==2:
    WSample.setNormRegions([(prefix+"CRW1L2Ba","cuts")])
elif chn==3:
    WSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
elif chn==4:
    WSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
elif chn==5:
    WSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
if doSherpa and (chn>=0 and chn<=9): 
    WSample.setFileList(SherpaWMassiveBCFiles) 
    #WSample.addSystematic(sherpgen)  ## turned off for now
else:
    WSample.setFileList(AlpgenWFiles) 


if doSherpa and (chn>=0 and chn<=9): # stop
    ZSampleName = "SherpaZMassiveBC"
else:
    ZSampleName = "AlpgenZ"
ZSample = Sample(ZSampleName,kBlue+3)
#ZSample.setNormFactor("mu_WZ",1.,0.,5.)
ZSample.setStatConfig(useStat)
#ZSample.addSystematic(Systematic(prefix+"err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
if (chn>=0 and chn<=9)  and chn!=2 and chn!=3 and chn!=4 and chn!=5 and chn!=6 and chn!=8:
    ZSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
elif chn==2:
    ZSample.setNormRegions([(prefix+"CRW1L2Ba","cuts")])
elif chn==3:
    ZSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
elif chn==4:
    ZSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
elif chn==5:
    ZSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
if doSherpa and (chn>=0 and chn<=9): # stop
    ZSample.setFileList(SherpaZMassiveBCFiles)
else:
    ZSample.setFileList(AlpgenZFiles) ###(SherpaZMassiveBCFiles)


QCDSample = Sample("QCD",kGray+1)
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
QCDSample.setFileList(qcdFiles)
#QCDSample.addSampleSpecificWeight("abs(qcdWeight)<10")

DataSample = Sample("Data",kBlack)
DataSample.setData()
DataSample.setFileList(dataFiles)


if (chn>=0 and chn<=9) and chn!=2 and chn!=3 and chn!=4 and chn!=5 and chn!=6 and chn!=8:
    AlpgenDYSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
    DibosonsSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
    SingleTopSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
    ttbarVSample.setNormRegions([(prefix+"CRT","cuts"),(prefix+"CRW","cuts")])
elif chn==2:
    AlpgenDYSample.setNormRegions([(prefix+"CRT1L2Ba","cuts"),(prefix+"CRW1L2Ba","cuts")])
    DibosonsSample.setNormRegions([(prefix+"CRT1L2Ba","cuts"),(prefix+"CRW1L2Ba","cuts")])
    SingleTopSample.setNormRegions([(prefix+"CRT1L2Ba","cuts"),(prefix+"CRW1L2Ba","cuts")])
    ttbarVSample.setNormRegions([(prefix+"CRT1L2Ba","cuts"),(prefix+"CRW1L2Ba","cuts")])
elif chn==3:
    AlpgenDYSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
    DibosonsSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
    SingleTopSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
    ttbarVSample.setNormRegions([(prefix+"CRT1L2Bc","cuts"),(prefix+"CRW1L2Bc","cuts")])
elif chn==4:
    AlpgenDYSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
    DibosonsSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
    SingleTopSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
    ttbarVSample.setNormRegions([(prefix+"CRT1L1Ba","cuts"),(prefix+"CRW1L1Ba","cuts")])
elif chn==5:
    AlpgenDYSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
    DibosonsSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
    SingleTopSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
    ttbarVSample.setNormRegions([(prefix+"CRT1L1Bc","cuts"),(prefix+"CRW1L1Bc","cuts")])
elif (chn==6 or chn==8):
    AlpgenDYSample.setNormRegions([(prefix+"CRT","cuts")])
    DibosonsSample.setNormRegions([(prefix+"CRT","cuts")])
    SingleTopSample.setNormRegions([(prefix+"CRT","cuts")])
    ttbarVSample.setNormRegions([(prefix+"CRT","cuts")])
    ZSample.setNormRegions([(prefix+"CRT","cuts")])


#######################
## Systematics (1/2) ##
#######################

bkgMCSamples = [DibosonsSample,SingleTopSample,TTbarSample,ZSample,ttbarVSample,AlpgenDYSample]
if chn!=6 and chn!=8: bkgMCSamples += [WSample] # absorbed in QCD

if includeSyst:
    SetupSamples( bkgMCSamples, SystList )
    #SetupSamples( [TTbarSample], [qfacT,ktfacT] )
    SetupSamples( [WSample], [qfacW,ktfacW] )
    SetupSamples( [ZSample], [qfacZ,ktfacZ] )

## more systematics below

##################
# The fit setup  #
##################

# First define HistFactory attributes
configMgr.analysisName   = "SoftLeptonPaper2013_"+anaName+"_"+grid+gridspec+allpoints[0] # Name to give the analysis
configMgr.outputFileName = "results/"+configMgr.analysisName+".root"
configMgr.histCacheFile  = "data/"+configMgr.analysisName+".root"

for point in allpoints:
    if point=="": continue

    # Fit config instance
    name="Fit_"+anaName+"_"+grid+gridspec+point
    myFitConfig = configMgr.addFitConfig(name)
    if useStat:
        myFitConfig.statErrThreshold=0.05
    else:
        myFitConfig.statErrThreshold=None

    #Add Measurement
    #meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.036)
    meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.028)
##    meas.addParamSetting("alpha_"+prefix+"pdfInter",True,0)
    
    if myFitType==FitType.Background: 
        meas.addPOI("mu_SIG")
        meas.addParamSetting("Lumi",True,1)
        #meas.addParamSetting("gamma_stat_CRW_cuts_bin_0",True,1)

    #meas.addParamSetting("mu_Diboson",True,1) # fix diboson to MC prediction

    if chn==2:
        #meas.addParamSetting("alpha_QCDNorm_CRW",True,0)
        pass
    #meas.addParamSetting("alpha_QCDNorm_WR",True,0)
    #meas.addParamSetting("alpha_QCDNorm_TR",True,0)
    #if chn==1:
    #    meas.addParamSetting("alpha_QCDNorm_WR5",True,0)
    #    meas.addParamSetting("alpha_QCDNorm_TR5",True,0)
    #meas.addParamSetting("alpha_QCDNorm_SS",True,0)
    if chn==6 or chn==8:
        meas.addParamSetting("mu_W",True,1) # fix Z to MC prediction

    # x-section uncertainties stop and ued
    if chn>=2 and chn<=5:
        xsecSig = Systematic(prefix+"SigXSec", configMgr.weights, 1.16, 0.84, "user", "userOverallSys")
        pass
    if chn==6 or chn==8:
        xsecSig = Systematic(prefix+"SigXSec", configMgr.weights, 1.25, 0.75, "user", "userOverallSys")
        pass

    # ISR uncertainty (SS and GG grids)
    if myFitType==FitType.Exclusion:
        if (chn>=0 and chn<=5) or chn==7 or chn==9:
            massSet = point.split('_')
            if len(massSet)==3:
                if not "pMSSM" in grid:
                    DeltaM = float(massSet[0]) - float(massSet[2])
                else:
                    DeltaM = float(massSet[1]) - float(massSet[2])
                DeltaM = float(massSet[0]) - float(massSet[2])
            elif len(massSet)==2:
                DeltaM = float(massSet[0]) - float(massSet[1])
            else:
                log.fatal("Invalid grid point: %s" % point)
            if DeltaM<=0: 
                log.fatal("Invalid value of DeltaM : %f" % DeltaM)
            #
            eisr3 = 0.00
            eisr5 = 0.00
            if grid=="SM_GG1step": 
                eisr3 = exp(-1.4-0.013*DeltaM)
                eisr5 = exp(-1.2-0.005*DeltaM)
                if eisr3<0.06: eisr3=0.06
                if eisr5<0.06: eisr5=0.06
                pass
            elif (grid=="SM_SS1step"): 
                eisr3 = 0.06+exp(0.8-0.1*DeltaM)
                eisr5 = 0.06+exp(-1.5-0.005*DeltaM)
                pass
            elif (grid=="StopBCharDeg") and (chn>=4 and chn<=5):
                eisr3 = exp(-0.0073*DeltaM - 1.93)
                eisr5 = exp(-0.0073*DeltaM - 1.93)
                pass
            isr3j = Systematic(prefix+"isr", configMgr.weights, 1.00+eisr3, 1.00-eisr3, "user", "userOverallSys")
            isr5j = Systematic(prefix+"isr", configMgr.weights, 1.00+eisr5, 1.00-eisr5, "user", "userOverallSys")
            pass
        elif chn==6 or chn==8:
            print (point)
            print (grid)
            massSet = point.split('_')
            print (massSet)
            total = 0.0
            if grid=="mUED2Lfilter":
                if len(massSet)!=2: 
                    log.fatal("Invalid grid point: %s" % point)
                else: 
                    Rinv = float(massSet[0])
                    LR   = float(massSet[1])
                    #total = 0.0
                    if Rinv==700 and (LR==2): total = 0.233
                    if Rinv==700 and (LR==3): total = 0.0764
                    if Rinv==700 and (LR==5): total = 0.1037
                    if Rinv==700 and (LR==10): total = 0.0812
                    if Rinv==700 and (LR==20): total = 0.0549
                    if Rinv==700 and (LR==40): total = 0.0233
                    if Rinv==800 and (LR==2): total = 0.2013
                    if Rinv==800 and (LR==3): total = 0.0671
                    if Rinv==800 and (LR==5): total = 0.1092
                    if Rinv==800 and (LR==10): total = 0.0678
                    if Rinv==800 and (LR==20): total = 0.0299
                    if Rinv==800 and (LR==40): total = 0.0501
                    if Rinv==900 and (LR==2): total = 0.1487
                    if Rinv==900 and (LR==3): total = 0.0895
                    if Rinv==900 and (LR==5): total = 0.0861
                    if Rinv==900 and (LR==10): total = 0.0412
                    if Rinv==900 and (LR==20): total = 0.0071
                    if Rinv==900 and (LR==40): total = 0.0456
                    if Rinv==1000 and (LR==2): total = 0.0764
                    if Rinv==1000 and (LR==3): total = 0.0864
                    if Rinv==1000 and (LR==5): total = 0.0627
                    if Rinv==1000 and (LR==10): total = 0.038
                    if Rinv==1000 and (LR==20): total = 0.0276
                    if Rinv==1000 and (LR==40): total = 0.0203
                    '''
                    ###CONF note uncert
                    if Rinv==700 and (LR==3 or LR==5 or LR==2): total = 0.179
                    if Rinv==700 and (LR==10 or LR==20): total = 0.130
                    if Rinv==700 and LR==40: total = 0.032
                    if Rinv==800 and (LR==3 or LR==5 or LR==2): total = 0.108
                    if Rinv==800 and (LR==10 or LR==20): total = 0.0917
                    if Rinv==800 and LR==40: total = 0.035
                    if Rinv==900 and (LR==3 or LR==5 or LR==2): total = 0.135
                    if Rinv==900 and (LR==10 or LR==20): total = 0.0804
                    if Rinv==900 and LR==40: total = 0.050
                    if Rinv==1000 and (LR==3 or LR==5 or LR==2): total = 0.141
                    if Rinv==1000 and (LR==10 or LR==20): total = 0.0265
                    if Rinv==1000 and LR==40: total = 0.014
                    print "UED ISR="
                    print (total)
                    print "R=%d LR=%d  total=%d" %(Rinv, LR, total)
                    '''
                print "UED ISR="
                print (total)
            isrmm = Systematic(prefix+"isr", configMgr.weights, 1.00+total, 1.00-total, "user", "userOverallSys")

    
    #-------------------------------------------------
    # First add the (grid point specific) signal sample
    #-------------------------------------------------
    gridName = grid.split("_")[0]
    if "StopBCharDeg" in gridName or "Stop3body" in gridName or "Stop4body" in gridName or "NatpMSSM" in gridName or "DMpMSSM" in gridName:
        sigSampleName=gridName+"_"+point
    else:
        sigSampleName=grid+"_"+point
     
    if myFitType==FitType.Exclusion:
        sigSample = Sample(sigSampleName,kRed)
        sigSample.setFileList(sigFiles)
        #sigSample.setTreeName(grid+"_"+point+suffix)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1,0.,100.)
        SetupSamples( [sigSample], SystList+[xsecSig] ) ## systematics
        #sigSample.addSystematic(xsecSig)      ## systematic not working?
        sigSample.setStatConfig(useStat)
        sigSample.mergeOverallSysSet = [prefix+"SigXSec",prefix+"isr"] ## post-processing
        myFitConfig.addSamples(sigSample)
        myFitConfig.setSignalSample(sigSample)
        meas.addPOI("mu_SIG")
    
    ## TR sofar defined for every channel
    if chn==2:
        TR = myFitConfig.addChannel("cuts",[prefix+"CRT1L2Ba"],1,0.5,1.5)
    elif chn==3:
        TR = myFitConfig.addChannel("cuts",[prefix+"CRT1L2Bc"],1,0.5,1.5)
    elif chn==4:
        TR = myFitConfig.addChannel("cuts",[prefix+"CRT1L1Ba"],1,0.5,1.5)
    elif chn==5:
        TR = myFitConfig.addChannel("cuts",[prefix+"CRT1L1Bc"],1,0.5,1.5)  
    else:
        TR = myFitConfig.addChannel("cuts",[prefix+"CRT"],1,0.5,1.5)
    TR.hasBQCD = True # b-tag applied
    myFitConfig.setBkgConstrainChannels(TR)
    ###if chn!=6:
    BRset = [TR] 
    ## WR = second control region
    ##########if chn!=6: # di-lepton region only has control region
    if (chn>=0 and chn<=1) or chn==7 or chn==9:
        WR = myFitConfig.addChannel("cuts",[prefix+"CRW"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]
    elif chn==2:
        WR = myFitConfig.addChannel("cuts",[prefix+"CRW1L2Ba"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]
    elif chn==3:
        WR = myFitConfig.addChannel("cuts",[prefix+"CRW1L2Bc"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]
    elif chn==4:
        WR = myFitConfig.addChannel("cuts",[prefix+"CRW1L1Ba"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]
    elif chn==5:
        WR = myFitConfig.addChannel("cuts",[prefix+"CRW1L1Bc"],1,0.5,1.5)  
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]

    if chn>=2 and chn<=3:
        if (myFitType==FitType.Background) and doValidation:
            BRset = [TR,WR]

    if chn>=4 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            WRbb = myFitConfig.addChannel("cuts",[prefix+"VR3"],1,0.5,1.5)
            WRbb.hasBQCD = True # b-tag applied
            BRset = [TR,WR,WRbb]
            myFitConfig.setValidationChannels(WRbb)

    ## FitType
    SRset = []
    VSRset = []
    if chn==0:
        if not doShapeFit:
            SR3j = myFitConfig.addChannel("cuts",[prefix+"SR1L3j"],1,0.5,1.5)
        else:
            SR3j = myFitConfig.addChannel("met/meffInc25_JVF25pt50",[prefix+"SR1L3j"],4,0.1,0.5)
            SR3j.useOverflowBin=True
            #SR3j.useUnderflowBin=True
    

        if doELMU:
            ELSR3j = myFitConfig.addChannel("cuts",[prefix+"ELSR1L3j"],1,0.5,1.5)
            MUSR3j = myFitConfig.addChannel("cuts",[prefix+"MUSR1L3j"],1,0.5,1.5)
            SRset = [SR3j,ELSR3j,MUSR3j]
        else:
            SRset = [SR3j]
            pass
        
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSRset = [VSR]
            #SR3j.remapSystChanName = 'cuts_VSR'
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
        pass

    if chn==9:
        if not doShapeFit:
            SR3j = myFitConfig.addChannel("cuts",[prefix+"SR1L3j"],1,0.5,1.5)
        else:
            ##SR3j = myFitConfig.addChannel("lep1Pt",[prefix+"SR1L3j"],3,6,25)
            SR3j = myFitConfig.addChannel("met/meffInc25_JVF25pt50",[prefix+"SR1L3j"],4,0.1,0.5)  
            SR3j.useOverflowBin=True
            #SR3j.useUnderflowBin=True

        if doELMU:
            ELSR3j = myFitConfig.addChannel("cuts",[prefix+"ELSR1L3j"],1,0.5,1.5)
            MUSR3j = myFitConfig.addChannel("cuts",[prefix+"MUSR1L3j"],1,0.5,1.5)
            SRset = [SR3j,ELSR3j,MUSR3j]
        else:
            SRset = [SR3j]
            pass

        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSRset = [VSR]
            #SR3j.remapSystChanName = 'cuts_VSR'
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
        pass

    elif chn==1:
        if not doShapeFit:
            SR3j = myFitConfig.addChannel("cuts",[prefix+"SR1L3j"],1,0.5,1.5)
            SR5j = myFitConfig.addChannel("cuts",[prefix+"SR1L5j"],1,0.5,1.5)
        else:
            SR3j = myFitConfig.addChannel("met/meffInc25_JVF25pt50",[prefix+"SR1L3j"],4,0.1,0.5)
            SR5j = myFitConfig.addChannel("met/meffInc25_JVF25pt50",[prefix+"SR1L5j"],4,0.1,0.5)
            SR3j.useOverflowBin=True
            #SR3j.useUnderflowBin=True
            SR5j.useOverflowBin=True
            #SR5j.useUnderflowBin=True


        if doELMU:
            ELSR3j = myFitConfig.addChannel("cuts",[prefix+"ELSR1L3j"],1,0.5,1.5)
            ELSR5j = myFitConfig.addChannel("cuts",[prefix+"ELSR1L5j"],1,0.5,1.5)
            MUSR3j = myFitConfig.addChannel("cuts",[prefix+"MUSR1L3j"],1,0.5,1.5)
            MUSR5j = myFitConfig.addChannel("cuts",[prefix+"MUSR1L5j"],1,0.5,1.5)
            SRset = [SR3j,SR5j,ELSR3j,MUSR3j,ELSR5j,MUSR5j]
        else:
            SRset = [SR3j,SR5j]
            pass

        WR5j = myFitConfig.addChannel("cuts",[prefix+"CRW5j"],1,0.5,1.5)
        TR5j = myFitConfig.addChannel("cuts",[prefix+"CRT5j"],1,0.5,1.5)
        TR5j.hasBQCD = True
        BRset += [WR5j,TR5j]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSR5j = myFitConfig.addChannel("cuts",[prefix+"VSR5j"],1,0.5,1.5)
            VSRset = [VSR,VSR5j]
        myFitConfig.setBkgConstrainChannels(WR5j)
        myFitConfig.setBkgConstrainChannels(TR5j)
        if doSystRemap:
            #SR3j.remapSystChanName = 'cuts_VSR'
            SR5j.remapSystChanName = 'cuts_VSR5j'
        else:
            #     <OverallSys Name="tt_pythjim" High="0.66706" Low="1.33318" />
            tt_pythjimSR5j = Systematic(prefix+"tt_pythjim", configMgr.weights, 0.667, 1.333, "user", "userOverallSys")
            SR5j.addSample(TTbarSample)
            SR5j.getSample('PowhegPythiaTTbar').removeSystematic(prefix+"tt_pythjim")
            SR5j.getSample('PowhegPythiaTTbar').addSystematic( tt_pythjimSR5j )
            pass
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass

    elif chn==7:
        if not doShapeFit:
            SR5j = myFitConfig.addChannel("cuts",[prefix+"SR1L5j"],1,0.5,1.5)
        else:
            SR5j = myFitConfig.addChannel("met/meffInc25_JVF25pt50",[prefix+"SR1L5j"],4,0.1,0.5)
            SR5j.useOverflowBin=True
            #SR5j.useUnderflowBin=True
        if doELMU:
            ELSR5j = myFitConfig.addChannel("cuts",[prefix+"ELSR1L5j"],1,0.5,1.5)
            MUSR5j = myFitConfig.addChannel("cuts",[prefix+"MUSR1L5j"],1,0.5,1.5)
            SRset = [SR5j,ELSR5j,MUSR5j]
        else:
            SRset = [SR5j]
            pass 
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSRset = [VSR]
            SR5j.remapSystChanName = 'cuts_VSR'
        else:
            #     <OverallSys Name="tt_pythjim" High="0.66706" Low="1.33318" />
            tt_pythjimSR5j = Systematic(prefix+"tt_pythjim", configMgr.weights, 0.667, 1.33, "user", "userOverallSys")
            SR5j.addSample(TTbarSample)
            SR5j.getSample('PowhegPythiaTTbar').removeSystematic(prefix+"tt_pythjim")
            SR5j.getSample('PowhegPythiaTTbar').addSystematic( tt_pythjimSR5j )
            pass
        if myFitType==FitType.Exclusion:
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass
    elif chn==2:
        if not doShapeFit:
            SR1a = myFitConfig.addChannel("cuts",[prefix+"SR1L2Ba"],1,0.5,1.5)
        else:
            SR1a = myFitConfig.addChannel("aMT2_BM_JVF25pt50",[prefix+"SR1L2Ba"],6,0,500)
            SR1a.useOverflowBin = True
        if myFitType==FitType.Background and useSRasCR:
            myFitConfig.setBkgConstrainChannels(SR1a)
        SR1a.hasBQCD = True
        SRset = [SR1a]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSRset = [VSR]
            VSR.hasBQCD = True
            #SR1a.remapSystChanName = 'cuts_VSR'
        # MB: Remove since uncertainty isr3j is empty for chn==2 
        #if myFitType==FitType.Exclusion:
        #    SR1a.getSample(sigSampleName).addSystematic(isr3j)
        #pass
    elif chn==3:
        if not doShapeFit:
            SR1b = myFitConfig.addChannel("cuts",[prefix+"SR1L2Bc"],1,0.5,1.5)
        else:
            SR1b = myFitConfig.addChannel("aMT2_BM_JVF25pt50",[prefix+"SR1L2Bc"],6,0,500)
            SR1b.useOverflowBin = True
        if myFitType==FitType.Background and useSRasCR:
            myFitConfig.setBkgConstrainChannels(SR1b)
        SR1b.hasBQCD = True
        SRset = [SR1b]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSR.hasBQCD = True
            VSRset = [VSR]
            #SR1b.remapSystChanName = 'cuts_VSR'
        # MB: Remove since uncertainty isr3j is empty for chn==3 
        #if myFitType==FitType.Exclusion:
        #    SR1b.getSample(sigSampleName).addSystematic(isr3j)
        #pass
    elif chn==4:
        if not doShapeFit:
            SR2a = myFitConfig.addChannel("cuts",[prefix+"SR1L1Ba"],1,0.5,1.5)
        else:
            SR2a = myFitConfig.addChannel("lep1Pt",[prefix+"SR1L1Ba"],4,6.,50.)
            SR2a.useOverflowBin = False
        if myFitType==FitType.Background and useSRasCR:
            myFitConfig.setBkgConstrainChannels(SR2a)
        SR2a.hasBQCD = True
        SRset = [SR2a]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSR.hasBQCD = True
            VSRset = [VSR]
            #SR2a.remapSystChanName = 'cuts_VSR'
        if myFitType==FitType.Exclusion:
            SR2a.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==5:
        if not doShapeFit:
            SR2b = myFitConfig.addChannel("cuts",[prefix+"SR1L1Bc"],1,0.5,1.5)
        else:
            SR2b = myFitConfig.addChannel("lep1Pt",[prefix+"SR1L1Bc"],4,6.,50.)
            SR2b.useOverflowBin = False
        if myFitType==FitType.Background and useSRasCR:
            myFitConfig.setBkgConstrainChannels(SR2b)
        SR2b.hasBQCD = True
        SRset = [SR2b]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSR.hasBQCD = True
            VSRset = [VSR]
            SR2b.remapSystChanName = 'cuts_VSR'
        else:
            #     <OverallSys Name="tt_pythjim" High="1.0936" Low="0.894762" />
            tt_pythjimSR2b = Systematic(prefix+"tt_pythjim", configMgr.weights, 1.0936, 0.894762, "user", "userOverallSys")
            SR2b.addSample(TTbarSample)
            SR2b.getSample('PowhegPythiaTTbar').removeSystematic(prefix+"tt_pythjim")            
            SR2b.getSample('PowhegPythiaTTbar').addSystematic( tt_pythjimSR2b )
            pass
        if myFitType==FitType.Exclusion:
            SR2b.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==6:
        if not doShapeFit:
            SR2la = myFitConfig.addChannel("cuts",[prefix+"SR2La"],1,0.5,1.5)
        else:
            SR2la = myFitConfig.addChannel("lep1Pt",[prefix+"SR2La"],3,6.,45.)
            SR2la.useOverflowBin = False
        #SR2la.hasBQCD = True  use weights w/o b since SR LF dominated
        SRset = [SR2la]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSR.hasBQCD = True
            VSRset = [VSR]
            SR2la.remapSystChanName = 'cuts_VSR'
        else:
            tt_pythjimSR2L = Systematic(prefix+"tt_pythjim", configMgr.weights, 0.85, 1.15, "user", "userOverallSys")
            #tt_pythjimSR2L = Systematic(prefix+"tt_pythjim", configMgr.weights, 0.40176, 1.75343, "user", "userOverallSys")
            SR2la.addSample(TTbarSample)
            SR2la.getSample('PowhegPythiaTTbar').removeSystematic(prefix+"tt_pythjim")            
            SR2la.getSample('PowhegPythiaTTbar').addSystematic( tt_pythjimSR2L )
            pass
        if myFitType==FitType.Exclusion:
            SR2la.getSample(sigSampleName).addSystematic(isrmm)           
        pass


    elif chn==8:
        if not doShapeFit:
            SR2lb = myFitConfig.addChannel("cuts",[prefix+"SR2Lb"],1,0.5,1.5)
        else:
            SR2lb = myFitConfig.addChannel("lep1Pt",[prefix+"SR2Lb"],2,6.,45.)
            SR2lb.useOverflowBin = False
        SR2lb.hasBQCD = True
        SRset = [SR2lb]
        if doSystRemap:
            VSR = myFitConfig.addChannel("cuts",[prefix+"VSR"],1,0.5,1.5)
            VSR.hasBQCD = True
            VSRset = [VSR]
            SR2lb.remapSystChanName = 'cuts_VSR'
        else:
            tt_pythjimSR2L = Systematic(prefix+"tt_pythjim", configMgr.weights, 0.85, 1.15, "user", "userOverallSys")
            SR2lb.addSample(TTbarSample)
            SR2lb.getSample('PowhegPythiaTTbar').removeSystematic(prefix+"tt_pythjim")            
            SR2lb.getSample('PowhegPythiaTTbar').addSystematic( tt_pythjimSR2L )
            pass
        if myFitType==FitType.Exclusion:
            SR2lb.getSample(sigSampleName).addSystematic(isrmm)           
        pass
    

    ## here we propagate the systematics 
    myFitConfig.addSamples(bkgMCSamples+[QCDSample,DataSample]) # DibosonsSample

    ## systematics (2/2)
    if includeSyst and dobtag:
        if chn==0 or chn==7 or chn==9:
            SetupChannels([WR,TR], bTagSyst9)
            pass
        if chn==1:
            SetupChannels([WR,TR], bTagSyst9)
            SetupChannels([WR5j,TR5j], bTagSyst9)
            pass
        elif chn==2:
            SetupChannels(BRset+SRset+VSRset, bTagSyst8) # VR
            pass
        elif chn==3:
            SetupChannels(BRset+SRset+VSRset, bTagSyst8) # VR
            pass
        elif chn==4:
            SetupChannels(BRset+SRset+VSRset, bTagSyst99) # VR
            pass
        elif chn==5:
            SetupChannels(BRset+SRset+VSRset, bTagSyst9) # VR
            pass
        elif chn==6 or chn==8:
            SetupChannels(BRset+SRset+VSRset, bTagSyst11) # VR1,VR2
            pass

    #if chn>=2 and chn<=5:
    if chn>=4 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            WRbb.getSample(WSampleName).addSystematic(wbb)
            pass
        pass
    if chn>=2 and chn<=5:
    #if chn>=4 and chn<=5:
        TR.getSample(WSampleName).addSystematic(wbb)
        for sr in SRset: 
            sr.getSample(WSampleName).addSystematic(wbb)

    ## W and top theory uncertainties in SRs 
    #for sr in SRset:
    #    if chn!=6:
    #        sr.getSample(WSampleName).addSystematic( Systematic(prefix+"TheoW", configMgr.weights, 1.25, 0.75, "user", "userOverallSys") )
    #for sr in SRset:
    #    sr.getSample(TTbarSampleName).addSystematic( Systematic(prefix+"TheoT", configMgr.weights, 1.25, 0.75, "user", "userOverallSys") )

    ## Fit type specifics
    if myFitType!=FitType.Background:
        myFitConfig.setSignalChannels(SRset)
    else:
        for sr in SRset:
            sr.doBlindingOverwrite = doBlinding
        myFitConfig.setValidationChannels(SRset)
    if myFitType==FitType.Discovery:
        #meas.addParamSetting("Lumi",True,1)
        for SR in SRset:
            SR.addDiscoverySamples([SR.name],[1.],[0.],[100.],[kMagenta])
            meas.addPOI("mu_%s" % SR.name)

    # dedicated validation regions for just the signal regions! To be used for systematics swapping
    if doSystRemap:
        myFitConfig.setValidationChannels(VSRset) # note: this appends the channels

    vSet = []
    if doValidation:
        if chn==0:
            #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",[prefix+"VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",[prefix+"VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",[prefix+"VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",[prefix+"VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",[prefix+"VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",[prefix+"VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",[prefix+"VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",[prefix+"VRW3j3"],1,0.5,1.5)
            vSet = [v1,v2] + [v4,v5] + [v7,v8]

            if doELMU:#for discovery check
                v1e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j1"],1,0.5,1.5) ; v1e.hasBQCD = True
                v2e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j1"],1,0.5,1.5)
                v4e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j2"],1,0.5,1.5) ; v4e.hasBQCD = True
                v5e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j2"],1,0.5,1.5)
                v7e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j3"],1,0.5,1.5) ; v7e.hasBQCD = True
                v8e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j3"],1,0.5,1.5)

                v1m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j1"],1,0.5,1.5) ; v1m.hasBQCD = True
                v2m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j1"],1,0.5,1.5)
                v4m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j2"],1,0.5,1.5) ; v4m.hasBQCD = True
                v5m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j2"],1,0.5,1.5)
                v7m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j3"],1,0.5,1.5) ; v7m.hasBQCD = True
                v8m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j3"],1,0.5,1.5)

                vSet +=   [v1e,v2e] + [v4e,v5e] + [v7e,v8e] +  [v1m,v2m] + [v4m,v5m] + [v7m,v8m]
                pass
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass

        if chn==9:
            #v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",[prefix+"VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",[prefix+"VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",[prefix+"VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",[prefix+"VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",[prefix+"VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",[prefix+"VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",[prefix+"VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",[prefix+"VRW3j3"],1,0.5,1.5)
            vSet = [v1,v2] + [v4,v5] + [v7,v8]

            if doELMU:#for discovery check
                v1e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j1"],1,0.5,1.5) ; v1e.hasBQCD = True
                v2e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j1"],1,0.5,1.5)
                v4e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j2"],1,0.5,1.5) ; v4e.hasBQCD = True
                v5e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j2"],1,0.5,1.5)
                v7e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j3"],1,0.5,1.5) ; v7e.hasBQCD = True
                v8e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j3"],1,0.5,1.5)
                
                v1m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j1"],1,0.5,1.5) ; v1m.hasBQCD = True
                v2m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j1"],1,0.5,1.5)
                v4m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j2"],1,0.5,1.5) ; v4m.hasBQCD = True
                v5m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j2"],1,0.5,1.5)
                v7m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j3"],1,0.5,1.5) ; v7m.hasBQCD = True
                v8m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j3"],1,0.5,1.5)

                vSet +=   [v1e,v2e] + [v4e,v5e] + [v7e,v8e] +  [v1m,v2m] + [v4m,v5m] + [v7m,v8m]
                pass
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass

        if chn==7:
            #v9  = myFitConfig.addChannel("cuts",[prefix+"VR5j1"],1,0.5,1.5)
            v10 = myFitConfig.addChannel("cuts",[prefix+"VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True
            v11 = myFitConfig.addChannel("cuts",[prefix+"VRW5j1"],1,0.5,1.5)
            #v12 = myFitConfig.addChannel("cuts",[prefix+"VR5j2"],1,0.5,1.5)
            v13 = myFitConfig.addChannel("cuts",[prefix+"VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            v14 = myFitConfig.addChannel("cuts",[prefix+"VRW5j2"],1,0.5,1.5)
            #v15 = myFitConfig.addChannel("cuts",[prefix+"VR5j3"],1,0.5,1.5)
            v16 = myFitConfig.addChannel("cuts",[prefix+"VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            v17 = myFitConfig.addChannel("cuts",[prefix+"VRW5j3"],1,0.5,1.5)
            vSet = [v10,v13,v16] + [v11,v14,v17] #+ [v9,v12,v15]

            if doELMU:#for discovery check
                v1e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT5j1"],1,0.5,1.5) ; v1e.hasBQCD = True
                v2e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW5j1"],1,0.5,1.5)
                v4e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT5j2"],1,0.5,1.5) ; v4e.hasBQCD = True
                v5e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW5j2"],1,0.5,1.5)
                v7e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT5j3"],1,0.5,1.5) ; v7e.hasBQCD = True
                v8e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW5j3"],1,0.5,1.5)
                
                v1m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT5j1"],1,0.5,1.5) ; v1m.hasBQCD = True
                v2m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW5j1"],1,0.5,1.5)
                v4m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT5j2"],1,0.5,1.5) ; v4m.hasBQCD = True
                v5m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW5j2"],1,0.5,1.5)
                v7m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT5j3"],1,0.5,1.5) ; v7m.hasBQCD = True
                v8m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW5j3"],1,0.5,1.5)

                vSet +=   [v1e,v2e] + [v4e,v5e] + [v7e,v8e] +  [v1m,v2m] + [v4m,v5m] + [v7m,v8m]
                pass

            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==1:
            #v0  = myFitConfig.addChannel("cuts",[prefix+"VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",[prefix+"VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",[prefix+"VRW3j1"],1,0.5,1.5)
            #v3  = myFitConfig.addChannel("cuts",[prefix+"VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",[prefix+"VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",[prefix+"VRW3j2"],1,0.5,1.5)
            #v6  = myFitConfig.addChannel("cuts",[prefix+"VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",[prefix+"VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",[prefix+"VRW3j3"],1,0.5,1.5)
            #v9  = myFitConfig.addChannel("cuts",[prefix+"VR5j1"],1,0.5,1.5)
            v10 = myFitConfig.addChannel("cuts",[prefix+"VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True 
            v11 = myFitConfig.addChannel("cuts",[prefix+"VRW5j1"],1,0.5,1.5)
            #v12 = myFitConfig.addChannel("cuts",[prefix+"VR5j2"],1,0.5,1.5)
            v13 = myFitConfig.addChannel("cuts",[prefix+"VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            v14 = myFitConfig.addChannel("cuts",[prefix+"VRW5j2"],1,0.5,1.5)
            #v15 = myFitConfig.addChannel("cuts",[prefix+"VR5j3"],1,0.5,1.5)
            v16 = myFitConfig.addChannel("cuts",[prefix+"VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            v17 = myFitConfig.addChannel("cuts",[prefix+"VRW5j3"],1,0.5,1.5)
            vSet = [v1,v4,v7,v10,v13,v16] + [v2,v5,v8,v11,v14,v17] #+ [v0,v3,v6,v9,v12,v15]

            if doELMU:#for discovery check
                v1e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j1"],1,0.5,1.5) ; v1e.hasBQCD = True
                v2e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j1"],1,0.5,1.5)
                v4e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j2"],1,0.5,1.5) ; v4e.hasBQCD = True
                v5e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j2"],1,0.5,1.5)
                v7e  = myFitConfig.addChannel("cuts",[prefix+"ELVRT3j3"],1,0.5,1.5) ; v7e.hasBQCD = True
                v8e  = myFitConfig.addChannel("cuts",[prefix+"ELVRW3j3"],1,0.5,1.5)
                
                v1m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j1"],1,0.5,1.5) ; v1m.hasBQCD = True
                v2m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j1"],1,0.5,1.5)
                v4m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j2"],1,0.5,1.5) ; v4m.hasBQCD = True
                v5m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j2"],1,0.5,1.5)
                v7m  = myFitConfig.addChannel("cuts",[prefix+"MUVRT3j3"],1,0.5,1.5) ; v7m.hasBQCD = True
                v8m  = myFitConfig.addChannel("cuts",[prefix+"MUVRW3j3"],1,0.5,1.5)
                #
                v1e5  = myFitConfig.addChannel("cuts",[prefix+"ELVRT5j1"],1,0.5,1.5) ; v1e5.hasBQCD = True
                v2e5  = myFitConfig.addChannel("cuts",[prefix+"ELVRW5j1"],1,0.5,1.5)
                v4e5  = myFitConfig.addChannel("cuts",[prefix+"ELVRT5j2"],1,0.5,1.5) ; v4e5.hasBQCD = True
                v5e5  = myFitConfig.addChannel("cuts",[prefix+"ELVRW5j2"],1,0.5,1.5)
                v7e5  = myFitConfig.addChannel("cuts",[prefix+"ELVRT5j3"],1,0.5,1.5) ; v7e5.hasBQCD = True
                v8e5  = myFitConfig.addChannel("cuts",[prefix+"ELVRW5j3"],1,0.5,1.5)
                
                v1m5  = myFitConfig.addChannel("cuts",[prefix+"MUVRT5j1"],1,0.5,1.5) ; v1m5.hasBQCD = True
                v2m5  = myFitConfig.addChannel("cuts",[prefix+"MUVRW5j1"],1,0.5,1.5)
                v4m5  = myFitConfig.addChannel("cuts",[prefix+"MUVRT5j2"],1,0.5,1.5) ; v4m5.hasBQCD = True
                v5m5  = myFitConfig.addChannel("cuts",[prefix+"MUVRW5j2"],1,0.5,1.5)
                v7m5  = myFitConfig.addChannel("cuts",[prefix+"MUVRT5j3"],1,0.5,1.5) ; v7m5.hasBQCD = True
                v8m5  = myFitConfig.addChannel("cuts",[prefix+"MUVRW5j3"],1,0.5,1.5)

                vSet +=   [v1e,v2e] + [v4e,v5e] + [v7e,v8e] +  [v1m,v2m] + [v4m,v5m] + [v7m,v8m] + [v1e5,v2e5] + [v4e5,v5e5] + [v7e5,v8e5] +  [v1m5,v2m5] + [v4m5,v5m5] + [v7m5,v8m5]
                pass
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==2:
            v0  = myFitConfig.addChannel("cuts",[prefix+"VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            #v1  = myFitConfig.addChannel("cuts",[prefix+"VR2"],1,0.5,1.5)
            #v1.hasBQCD = True
            vSet = [v0]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==3:
            v0  = myFitConfig.addChannel("cuts",[prefix+"VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            #v1  = myFitConfig.addChannel("cuts",[prefix+"VR2"],1,0.5,1.5)
            #v1.hasBQCD = True
            vSet = [v0]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==4:
            v0  = myFitConfig.addChannel("cuts",[prefix+"VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",[prefix+"VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==5:
            v0  = myFitConfig.addChannel("cuts",[prefix+"VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",[prefix+"VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==6 or chn==8:
            v0  = myFitConfig.addChannel("cuts",[prefix+"VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",[prefix+"VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",[prefix+"VR3"],1,0.5,1.5)
            v2.hasBQCD = True
            vSet = [v0,v1,v2]
            myFitConfig.setValidationChannels(vSet)
            pass

    if doValidation and includeSyst and dobtag:
        if chn==0 or chn==7 or chn==9:
            SetupChannels(vSet, bTagSyst9)
            pass
        if chn==1:
            SetupChannels(vSet, bTagSyst9)
            pass
        elif chn==2:
            SetupChannels(vSet, bTagSyst8) # VR
            pass
        elif chn==3:
            SetupChannels(vSet, bTagSyst8) # VR
            pass
        elif chn==4:
            SetupChannels(vSet, bTagSyst99) # VR
            pass
        elif chn==5:
            SetupChannels(vSet, bTagSyst9) # VR
            pass
        elif chn==6 or chn==8:
            SetupChannels(vSet, bTagSyst11) # VR1,VR2
            pass

    # Generator Systematics for each sample,channel
    log.info("** Generator Systematics **")
    AllChannels = BRset+SRset+vSet
    for tgt,syst in generatorSyst:
        tgtsample = tgt[0]
        tgtchan = tgt[1]
        for chan in AllChannels:
            # if tgtchan=="All" or tgtchan==chan.name:
            if 'cuts_'+tgtchan == chan.name  or   'metmeffInc25_JVF25pt50_'+tgtchan == chan.name or   'aMT2_BM_JVF25pt50_'+tgtchan == chan.name or   'lep1Pt_'+tgtchan == chan.name :
                chan.getSample(tgtsample).addSystematic(syst)
                log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))


