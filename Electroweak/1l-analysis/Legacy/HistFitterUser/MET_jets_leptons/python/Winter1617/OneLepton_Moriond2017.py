################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch


from logger import Logger
log = Logger('OneLepton')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onWestGrid='westgrid' in commands.getstatusoutput("hostname")[1] or fnmatch.fnmatch(commands.getstatusoutput("hostname")[1], 'u*')
onUBCAtlasServ='atlasserv' in commands.getstatusoutput("hostname")[1]
onMOGON="mogon" in commands.getstatusoutput("hostname")[1]
onDOL= False
if onDOL:    onLxplus=False  ## previous status does not detect the batch system in lxplus

debug = False #!!!

if onDOL:  debug = True  ## for test

if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]

SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
#SystList.append("JVT")      # JVT scale factors
SystList.append("pileup")   # pileup (common)
SystList.append("BTag")     # b-tagging uncertainties
SystList.append("Dibosons") # scale variation for renormalization, factorization, resummation and generator comparison
SystList.append("Wjets")    # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
SystList.append("Ttbar")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
SystList.append("ttbarPDF") # 
SystList.append("wjetsPDF") # 
SystList.append("Zjets")    # scale variation for renormalization, factorization, resummation,matching

if debug:
    SystList=[]


# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=False

useNJetNormFac=True
#if onDOL:  useNJetNormFac=False
# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["2J","4Jhighx","4Jlowx","6J"] #this is the default - modify from command line


# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 2J,4Jhighx,4Jlowx,6J  option)"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["2J"] = False
ValidRegList["4Jhighx"] = False
ValidRegList["4Jlowx"] = False
ValidRegList["6J"] = False



# for tables (turn doTableInputs to True)
doTableInputs = True #This effectively means no validation plots but only validation tables (but is 100x faster)
for cr in CRregions:
    if "2J" in cr and doTableInputs:
        ValidRegList["2J"] = True
    if "4Jhighx" in cr and doTableInputs:
        ValidRegList["4Jhighx"] = True	
    if "4Jlowx" in cr and doTableInputs:
        ValidRegList["4Jlowx"] = True
    if "6J" in cr and doTableInputs:
        ValidRegList["6J"] = True    	

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=False


# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    
    #sigSamples=["GG_oneStep_945_785_625"]
    #sigSamples=["GG_oneStep_637_626_615"]    
   
    #sigSamples=["GG_oneStep_1105_865_625"]
    sigSamples=["GG_oneStep_1385_705_25"]
    
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
    
if myFitType==FitType.Exclusion:
    if onDOL:
        analysissuffix += '_'+ sigSamples[0]
    if 'GG_oneStep' in sigSamples[0]  and not onDOL:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]  and not onDOL:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]

mylumi= 36.46161#14.78446 #30 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  

mysamples = ["Zjets","ttv","singletop","diboson","Wjets","ttbar","data"]
doOnlySignal=False

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "OneLepton"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
useBackupCache = False
if (onMOGON or onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "OneLepton"+analysissuffix_BackupCache+analysisextension
    if not onLxplus: histBackupCacheFileName = "/project/etp3/jlorenz/shape_fit/HistFitter_git/HistFitterUser/MET_jets_leptons/data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    else: histBackupCacheFileName = "data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    if onDOL:    histBackupCacheFileName = "/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/HISTFITTER/HistFitterUser/MET_jets_leptons/data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

configMgr.fixSigXSec=True
useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = False

#blinding of various regions
configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)
doBlindSRinBGfit  = True # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = ["SS_oneStep_725_375_25",
              "SS_oneStep_925_475_25",
              "SS_oneStep_800_790_60",
              "SS_oneStep_900_700_60",
              "SS_oneStep_900_800_60",
              "SS_oneStep_1200_1190_60",
              "GG_oneStep_1385_705_25",
              "GG_oneStep_1545_785_25",
              "GG_oneStep_1000_990_60",
              "GG_oneStep_1400_1060_60",
              "GG_oneStep_1400_1260_60",
              "GG_oneStep_1400_1390_60",
              "GG_oneStep_1600_1590_60",
              "GG_oneStep_1800_1790_60"]

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

#inputDir="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
#inputDirSig="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
#inputDirData="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"

inputDir="/afs/cern.ch/work/v/vtudorac/public/eos/atlas/user/v/vtudorac/Tag0605/"
inputDirData="/afs/cern.ch/work/v/vtudorac/public/eos/atlas/user/v/vtudorac/Tag0605/"
inputDirSig="/afs/cern.ch/work/v/vtudorac/public/eos/atlas/user/v/vtudorac/Tag0605/"


if not onLxplus:
    inputDir="/project/etp5/SUSYInclusive/trees/T_06_05/"
    inputDirSig="/project/etp5/SUSYInclusive/trees/T_06_05/"
    inputDirData="/project/etp5/SUSYInclusive/trees/T_06_05/"

if onWestGrid :
    inputDir="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirSig="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirData="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"

if onUBCAtlasServ :
    inputDir="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirSig="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirData="/mnt/xrootdd/mgignac/MC15C/T_06_03/"

if onMOGON:
    inputDir="../InputTrees/"
    inputDirSig="../InputTrees/"
    inputDirData="../InputTrees/"

if onDOL:
    inputDir="/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/1l/1l_TREE/JAMB2016/"
    inputDirSig="/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/1l/1l_TREE/JAMB2016/"
    inputDirData="/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/1l/1l_TREE/JAMB2016/"
    
#background files, separately for electron and muon channel:
bgdFiles_e = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]
bgdFiles_m = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]
bgdFiles_em = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]


#signal files
if myFitType==FitType.Exclusion or doOnlySignal:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allTrees_T_06_05_skimslim_met200_oneStep.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allTrees_T_06_05_skimslim_met200_oneStep.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allTrees_T_06_05_skimslim_met200_oneStep.root"]
        
#data files
#dataFiles = [inputDirData+"allTrees_T_06_03_dataOnly.root"] # 949.592 /pb
#dataFiles = [inputDirData+"allDS2_T_06_03.root"]    # DS2: 13277.66 /pb (remember to check lumiErr: should be 2.9%)
#dataFiles = [inputDirData+"allDS2v01_T_06_03.root"] # DS2.1: 14784.46 /pb (remember to check lumiErr: should be 3.0%)
dataFiles = [inputDirData+"allTrees_T_06_05_data.root"] # DS3: 36461.61 /pb (remember to check lumiErr: should be 4.1%)

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","jvtWeight","SherpaVjetsNjetsWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights

#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")

muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")

#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#### electron related uncertainties acting on weights ####
#ID 
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Rec
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Iso
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")


# JVT weight systematics
JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyUp")
JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyDown")


# pileup weight systematics
pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

    eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
    eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}


SingleTopComm = {}
ZjetsComm = {}
WjetsComm={}
TtbarComm={}
DBComm={}



for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 35% syst and an overallSys
   
     
    #basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.35,0.65,"user","userOverallSys"))
   
    	
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_NoSys","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_NoSys","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_NoSys","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_NoSys","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_NoSys","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_NoSys","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6restTerm","_NoSys","_JET_EffectiveNP_6restTerm__1up","_JET_EffectiveNP_6restTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_NoSys","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_NoSys","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_NoSys","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_NoSys","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_NoSys","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_NoSys","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_NoSys","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_NoSys","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_NoSys","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_NoSys","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys"))

    	        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_ID","_NoSys","_MUONS_ID__1up","_MUONS_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUONS_MS","_NoSys","_MUONS_MS__1up","_MUONS_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUONS_SCALE","_NoSys","_MUONS_SCALE__1up","_MUONS_SCALE__1down","tree","overallNormHistoSys"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Eff",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys")) 

    if "JVT" in SystList :
        basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))
    if "BTag" in SystList:
        bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")
        eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,eTagFromCHighWeights,eTagFromCLowWeights,"weight","overallNormHistoSys")



    if 'singletop' in mysamples:
        SingleTopComm[region] = Systematic("h1L_SingleTopComm", configMgr.weights,configMgr.weights+["( 1+0.8 )"],configMgr.weights+["( 1-0.8 )"], "weight","overallSys")
    if 'Zjets' in mysamples:
        ZjetsComm[region] = Systematic("h1L_ZjetsComm", configMgr.weights,configMgr.weights+["( 1+0.5 )"],configMgr.weights+["( 1-0.5 )"], "weight","overallSys")
    if not '6J' in region:
    	if 'Wjets' in mysamples:
	      WjetsComm[region] = Systematic("h1L_WjetsComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
    	if 'ttbar' in mysamples:     
	      TtbarComm[region] = Systematic("h1L_TtbarComm", configMgr.weights,configMgr.weights+["( 1+0.3 )"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")
    	if 'diboson' in mysamples:     
	      DBComm[region] = Systematic("h1L_DBComm", configMgr.weights,configMgr.weights+["( 1+0.6 )"],configMgr.weights+["( 1-0.6 )"], "weight","overallSys")
    	else:
    	     print "other samples"
	   
    else:
        print "6J region"	   
    
    #signal uncertainties                                                                                       
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

    #Generator Systematics
    generatorSyst = []  	

    if "Dibosons" in SystList and 'diboson' in mysamples:
        import Winter1617.theoryUncertainties_OneLepton_Dibosons
        Winter1617.theoryUncertainties_OneLepton_Dibosons.TheorUnc(generatorSyst)
    if "Wjets" in SystList and 'Wjets' in mysamples:
        if useNJetNormFac:
            import Winter1617.theoryUncertainties_OneLepton_Wjets
            Winter1617.theoryUncertainties_OneLepton_Wjets.TheorUnc(generatorSyst)
        else:
            import Winter1617.theoryUncertainties_OneLepton_Wjets_1NF
            Winter1617.theoryUncertainties_OneLepton_Wjets_1NF.TheorUnc(generatorSyst)
    if "Ttbar" in SystList and 'ttbar' in mysamples:
        if useNJetNormFac:
            import Winter1617.theoryUncertainties_OneLepton_Ttbar
            Winter1617.theoryUncertainties_OneLepton_Ttbar.TheorUnc(generatorSyst)
        else:
            import Winter1617.theoryUncertainties_OneLepton_Ttbar_1NF
            Winter1617.theoryUncertainties_OneLepton_Ttbar_1NF.TheorUnc(generatorSyst)
    if "ttbarPDF" in SystList and 'ttbar' in mysamples:
        import Winter1617.theoryUncertainties_OneLepton_ttbarPDF
        Winter1617.theoryUncertainties_OneLepton_ttbarPDF.TheorUnc(generatorSyst)
    if "wjetsPDF" in SystList and 'Wjets' in mysamples:
        import Winter1617.theoryUncertainties_OneLepton_wjetsPDF
        Winter1617.theoryUncertainties_OneLepton_wjetsPDF.TheorUnc(generatorSyst)		
    if "Zjets" in SystList and 'Zjets' in mysamples:
        import Winter1617.theoryUncertainties_OneLepton_Zjets
        Winter1617.theoryUncertainties_OneLepton_Zjets.TheorUnc(generatorSyst)
       
	
    

# ********************************************************************* #
#                              Background samples

# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets_Sherpa221"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)

#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)

#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()

#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setNormFactor("mu_Top",1.,0.,5.)
SingleTopSample.setStatConfig(useStat)
#SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets_Sherpa22"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()

#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()

if doOnlySignal: 
    sigSample = Sample(sigSamples[0],kPink)    
    sigSample.setStatConfig(useStat)
    sigSample.setNormByTheory()
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)
    sigSample.setFileList(sigFiles_em[sigSamples[0]])

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&  nLep_base==1&&nLep_signal==1 && combinedTrigResultMET"## TSTcleaning here
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>7) "
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>6)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
TwoLepSelection = "&&  nLep_base>=2 && nLep_signal>=2 && combinedTrigResultMET"

# ------- 2J region --------------------------------------------------------------------------- #
SR2JSelection ="lep1Pt<35 && nJet30>=2 && met>430. && mt>100. && (met/meffInc30) > 0.35 && (nJet30/lep1Pt)>0.2"
configMgr.cutsDict["SR2J"]=  SR2JSelection + CommonSelection

configMgr.cutsDict["SR2JBV_bin1"]=SR2JSelection +"&& meffInc30>700 && meffInc30<1233 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR2JBV_bin2"]=SR2JSelection +"&& meffInc30>1233 && meffInc30<1766 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR2JBV_bin3"]=SR2JSelection +"&& meffInc30>1766 && meffInc30<2300 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR2JBV_bin4"]=SR2JSelection +"&& meffInc30>2300 && nBJet30_MV2c10==0" + CommonSelection

configMgr.cutsDict["SR2JBT_bin1"]=SR2JSelection +"&& meffInc30>700 && meffInc30<1233 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR2JBT_bin2"]=SR2JSelection +"&& meffInc30>1233 && meffInc30<1766 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR2JBT_bin3"]=SR2JSelection +"&& meffInc30>1766 && meffInc30<2300 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR2JBT_bin4"]=SR2JSelection +"&& meffInc30>2300 && nBJet30_MV2c10>0" + CommonSelection

CR2JSelection = "lep1Pt<35 && nJet30>=2 && met>250 && met<430. && mt>50 && mt<100."#old remove met/meff  cut
configMgr.cutsDict["TR2J"]=CR2JSelection +" && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["TR2J_bin1"]=CR2JSelection +" && meffInc30>700 &&meffInc30<1233 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR2J_bin2"]=CR2JSelection +" && meffInc30>1233 &&meffInc30<1766 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR2J_bin3"]=CR2JSelection +" && meffInc30>1766 &&meffInc30<2300 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR2J_bin4"]=CR2JSelection +" && meffInc30>2300 && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["WR2J"]=CR2JSelection +" && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR2J_bin1"]=CR2JSelection +" && meffInc30>700 &&meffInc30<1233 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR2J_bin2"]=CR2JSelection +" && meffInc30>1233 &&meffInc30<1766 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR2J_bin3"]=CR2JSelection +" && meffInc30>1766 &&meffInc30<2300 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR2J_bin4"]=CR2JSelection +" && meffInc30>2300 && nBJet30_MV2c10==0" + CommonSelection

VR2JaplSelection="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>460. && mt>40. && mt<100. "
VR2JmtSelection="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>250. && met<460. && mt>80. "
VR2JnoaplSelection="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>250. && mt>40.&& mt<80 "
VR2JnomtSelection="lep1Pt<35 && nJet30>=2 && jet1Pt>200. && met>250. && met<460. && mt>40. "

configMgr.cutsDict["VR2Japl_bin1"]=VR2JaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR2Japl_bin2"]=VR2JaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR2Japl_bin3"]=VR2JaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR2Japl_bin4"]=VR2JaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR2Jmt_bin1"]=VR2JmtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR2Jmt_bin2"]=VR2JmtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR2Jmt_bin3"]=VR2JmtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR2Jmt_bin4"]=VR2JmtSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR2Jnoapl_bin1"]=VR2JnoaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR2Jnoapl_bin2"]=VR2JnoaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR2Jnoapl_bin3"]=VR2JnoaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR2Jnoapl_bin4"]=VR2JnoaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR2Jnomt_bin1"]=VR2JnomtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR2Jnomt_bin2"]=VR2JnomtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR2Jnomt_bin3"]=VR2JnomtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR2Jnomt_bin4"]=VR2JnomtSelection+"&& meffInc30>2300" + CommonSelection

# ------- 4J regions for gluino gridx high x--------------------------------------------------------------------------- #
SR4JhighxSelection ="lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && mt>450 && LepAplanarity>0.01 && met/meffInc30>0.20"
configMgr.cutsDict["SR4Jhighx"]=  SR4JhighxSelection + CommonSelection

configMgr.cutsDict["SR4JhighxBV_bin1"]=SR4JhighxSelection +"&& meffInc30>700 && meffInc30<1233 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JhighxBV_bin2"]=SR4JhighxSelection +"&& meffInc30>1233 && meffInc30<1766 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR4JhighxBV_bin3"]=SR4JhighxSelection +"&& meffInc30>1766 && meffInc30<2300 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR4JhighxBV_bin4"]=SR4JhighxSelection +"&& meffInc30>2300 && nBJet30_MV2c10==0" + CommonSelection

configMgr.cutsDict["SR4JhighxBT_bin1"]=SR4JhighxSelection +"&& meffInc30>700 && meffInc30<1233 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR4JhighxBT_bin2"]=SR4JhighxSelection +"&& meffInc30>1233 && meffInc30<1766 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR4JhighxBT_bin3"]=SR4JhighxSelection +"&& meffInc30>1766 && meffInc30<2300 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR4JhighxBT_bin4"]=SR4JhighxSelection +"&& meffInc30>2300 && nBJet30_MV2c10>0" + CommonSelection

CR4JhighxSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && mt>50 &&mt<150 && LepAplanarity<0.01 && met/meffInc30>0.20"#old
configMgr.cutsDict["TR4Jhighx"]=CR4JhighxSelection +" && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["TR4Jhighx_bin1"]=CR4JhighxSelection +" && meffInc30>700 &&meffInc30<1233 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR4Jhighx_bin2"]=CR4JhighxSelection +" && meffInc30>1233 &&meffInc30<1766 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR4Jhighx_bin3"]=CR4JhighxSelection +" && meffInc30>1766 &&meffInc30<2300 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR4Jhighx_bin4"]=CR4JhighxSelection +" && meffInc30>2300 && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["WR4Jhighx"]=CR4JhighxSelection +" && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jhighx_bin1"]=CR4JhighxSelection +" && meffInc30>700 &&meffInc30<1233 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jhighx_bin2"]=CR4JhighxSelection +" && meffInc30>1233 &&meffInc30<1766 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jhighx_bin3"]=CR4JhighxSelection +" && meffInc30>1766 &&meffInc30<2300 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jhighx_bin4"]=CR4JhighxSelection +" && meffInc30>2300 && nBJet30_MV2c10==0" + CommonSelection

VR4JhighxaplSelection="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100  && met>250 && mt>100 && jet6Pt<30"
VR4JhighxmtSelection="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100  && met>250 && mt<100 "
VR4JhighxnoaplSelection="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && met>250 && jet6Pt<30"
VR4JhighxnomtSelection="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100  && met>250 && mt<100 "

configMgr.cutsDict["VR4Jhighxapl_bin1"]=VR4JhighxaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jhighxapl_bin2"]=VR4JhighxaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jhighxapl_bin3"]=VR4JhighxaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jhighxapl_bin4"]=VR4JhighxaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR4Jhighxmt_bin1"]=VR4JhighxmtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jhighxmt_bin2"]=VR4JhighxmtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jhighxmt_bin3"]=VR4JhighxmtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jhighxmt_bin4"]=VR4JhighxmtSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR4Jhighxnoapl_bin1"]=VR4JhighxnoaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jhighxnoapl_bin2"]=VR4JhighxnoaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jhighxnoapl_bin3"]=VR4JhighxnoaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jhighxnoapl_bin4"]=VR4JhighxnoaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR4Jhighxnomt_bin1"]=VR4JhighxnomtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jhighxnomt_bin2"]=VR4JhighxnomtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jhighxnomt_bin3"]=VR4JhighxnomtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jhighxnomt_bin4"]=VR4JhighxnomtSelection+"&& meffInc30>2300" + CommonSelection

# ------- 4J regions for gluino gridx low x--------------------------------------------------------------------------- #
SR4JlowxSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6  && met>250 && mt>150 && mt<450 && LepAplanarity>0.02"
configMgr.cutsDict["SR4Jlowx"]=  SR4JlowxSelection + CommonSelection

configMgr.cutsDict["SR4JlowxBV_bin1"]=SR4JlowxSelection +"&& meffInc30>400 && meffInc30<933 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JlowxBV_bin2"]=SR4JlowxSelection +"&& meffInc30>933 && meffInc30<1466 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR4JlowxBV_bin3"]=SR4JlowxSelection +"&& meffInc30>1466 && meffInc30<2000 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR4JlowxBV_bin4"]=SR4JlowxSelection +"&& meffInc30>2000 && nBJet30_MV2c10==0" + CommonSelection

configMgr.cutsDict["SR4JlowxBT_bin1"]=SR4JlowxSelection +"&& meffInc30>400 && meffInc30<933 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR4JlowxBT_bin2"]=SR4JlowxSelection +"&& meffInc30>933 && meffInc30<1466 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR4JlowxBT_bin3"]=SR4JlowxSelection +"&& meffInc30>1466 && meffInc30<2300 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR4JlowxBT_bin4"]=SR4JlowxSelection +"&& meffInc30>2000 && nBJet30_MV2c10>0" + CommonSelection

CR4JlowxSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>50 && mt<150&& LepAplanarity>0.01 && LepAplanarity<0.02"#old
configMgr.cutsDict["TR4Jlowx"]=CR4JlowxSelection +" && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["TR4Jlowx_bin1"]=CR4JlowxSelection +" && meffInc30>400 &&meffInc30<933 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR4Jlowx_bin2"]=CR4JlowxSelection +" && meffInc30>933 &&meffInc30<1466 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR4Jlowx_bin3"]=CR4JlowxSelection +" && meffInc30>1466 &&meffInc30<2000 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR4Jlowx_bin4"]=CR4JlowxSelection +" && meffInc30>2000 && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["WR4Jlowx"]=CR4JlowxSelection +" && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jlowx_bin1"]=CR4JlowxSelection +" && meffInc30>400 &&meffInc30<933 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jlowx_bin2"]=CR4JlowxSelection +" && meffInc30>933 &&meffInc30<1466 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jlowx_bin3"]=CR4JlowxSelection +" && meffInc30>1466 &&meffInc30<2000 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR4Jlowx_bin4"]=CR4JlowxSelection +" && meffInc30>2000 && nBJet30_MV2c10==0" + CommonSelection

VR4JlowxaplSelection="nJet30>=4 && jet4Pt>100 && met>250 && mt<100 && JetAplanarity>0.03"
VR4JlowxmtSelection="nJet30>=4 && jet4Pt>100 && met>250 && mt>100 && JetAplanarity<0.03 && met/meffInc30<0.35"
VR4JlowxnoaplSelection="nJet30>=4 && jet4Pt>100 && met>250 && mt<100 "
VR4JlowxnomtSelection="nJet30>=4 && jet4Pt>100 && met>250 && JetAplanarity<0.03 && met/meffInc30<0.35"

configMgr.cutsDict["VR4Jlowxapl_bin1"]=VR4JlowxaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jlowxapl_bin2"]=VR4JlowxaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jlowxapl_bin3"]=VR4JlowxaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jlowxapl_bin4"]=VR4JlowxaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR4Jlowxmt_bin1"]=VR4JlowxmtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jlowxmt_bin2"]=VR4JlowxmtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jlowxmt_bin3"]=VR4JlowxmtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jlowxmt_bin4"]=VR4JlowxmtSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR4Jlowxnoapl_bin1"]=VR4JlowxnoaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jlowxnoapl_bin2"]=VR4JlowxnoaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jlowxnoapl_bin3"]=VR4JlowxnoaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jlowxnoapl_bin4"]=VR4JlowxnoaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR4Jlowxnomt_bin1"]=VR4JlowxnomtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR4Jlowxnomt_bin2"]=VR4JlowxnomtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR4Jlowxnomt_bin3"]=VR4JlowxnomtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR4Jlowxnomt_bin4"]=VR4JlowxnomtSelection+"&& meffInc30>2300" + CommonSelection

# ------- 6J regions --------------------------------------------------------------------------- #
SR6JSelection = "lep1Pt>35 && nJet30>=6 && met>350. && mt>175. && LepAplanarity>0.06" 
configMgr.cutsDict["SR6J"]= SR6JSelection + CommonSelection

configMgr.cutsDict["SR6JBV_bin1"]=SR6JSelection +"&& meffInc30>700 && meffInc30<1233 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR6JBV_bin2"]=SR6JSelection +"&& meffInc30>1233 && meffInc30<1766 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR6JBV_bin3"]=SR6JSelection +"&& meffInc30>1766 && meffInc30<2300 && nBJet30_MV2c10==0"+ CommonSelection
configMgr.cutsDict["SR6JBV_bin4"]=SR6JSelection +"&& meffInc30>2300 && nBJet30_MV2c10==0" + CommonSelection

configMgr.cutsDict["SR6JBT_bin1"]=SR6JSelection +"&& meffInc30>700 && meffInc30<1233 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR6JBT_bin2"]=SR6JSelection +"&& meffInc30>1233 && meffInc30<1766 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR6JBT_bin3"]=SR6JSelection +"&& meffInc30>1766 && meffInc30<2300 && nBJet30_MV2c10>0"+ CommonSelection
configMgr.cutsDict["SR6JBT_bin4"]=SR6JSelection +"&& meffInc30>2300 && nBJet30_MV2c10>0" + CommonSelection

CR6JSelection = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. && mt<175 && LepAplanarity<0.06"
configMgr.cutsDict["TR6J"]=CR6JSelection +" && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["TR6J_bin1"]=CR6JSelection +" && meffInc30>700 &&meffInc30<1233 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR6J_bin2"]=CR6JSelection +" && meffInc30>1233 &&meffInc30<1766 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR6J_bin3"]=CR6JSelection +" && meffInc30>1766 &&meffInc30<2300 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["TR6J_bin4"]=CR6JSelection +" && meffInc30>2300 && nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["WR6J"]=CR6JSelection +" && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR6J_bin1"]=CR6JSelection +" && meffInc30>700 &&meffInc30<1233 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR6J_bin2"]=CR6JSelection +" && meffInc30>1233 &&meffInc30<1766 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR6J_bin3"]=CR6JSelection +" && meffInc30>1766 &&meffInc30<2300 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["WR6J_bin4"]=CR6JSelection +" && meffInc30>2300 && nBJet30_MV2c10==0" + CommonSelection

VR6JaplSelection = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. && mt<175 && LepAplanarity>0.06 "
VR6JmtSelection = "lep1Pt>35 && nJet30>=6 && met>350.  && mt<350 && LepAplanarity<0.06"
VR6JnoaplSelection = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. && mt<175"
VR6JnomtSelection = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. &&  LepAplanarity<0.06"

configMgr.cutsDict["VR6Japl_bin1"]=VR6JaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR6Japl_bin2"]=VR6JaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR6Japl_bin3"]=VR6JaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR6Japl_bin4"]=VR6JaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR6Jmt_bin1"]=VR6JmtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR6Jmt_bin2"]=VR6JmtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR6Jmt_bin3"]=VR6JmtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR6Jmt_bin4"]=VR6JmtSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR6Jnoapl_bin1"]=VR6JnoaplSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR6Jnoapl_bin2"]=VR6JnoaplSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR6Jnoapl_bin3"]=VR6JnoaplSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR6Jnoapl_bin4"]=VR6JnoaplSelection+"&& meffInc30>2300" + CommonSelection

configMgr.cutsDict["VR6Jnomt_bin1"]=VR6JnomtSelection+"&& meffInc30>700 &&meffInc30<1233" + CommonSelection
configMgr.cutsDict["VR6Jnomt_bin2"]=VR6JnomtSelection+"&& meffInc30>1233 &&meffInc30<1766" + CommonSelection
configMgr.cutsDict["VR6Jnomt_bin3"]=VR6JnomtSelection+"&& meffInc30>1766 &&meffInc30<2300" + CommonSelection
configMgr.cutsDict["VR6Jnomt_bin4"]=VR6JnomtSelection+"&& meffInc30>2300" + CommonSelection


d=configMgr.cutsDict
#second part: splitting into electron and muon channel for validation purposes	d=configMgr.cutsDict
defined_regions = []
if '2J' in CRregions: 
    defined_regions+=['SR2J','SR2JBV_bin1','SR2JBV_bin2','SR2JBV_bin3','SR2JBV_bin4','SR2JBT_bin1','SR2JBT_bin2','SR2JBT_bin3','SR2JBT_bin4','WR2J','WR2J_bin1','WR2J_bin2','WR2J_bin3','WR2J_bin4','TR2J','TR2J_bin1','TR2J_bin2','TR2J_bin3','TR2J_bin4',"VR2Japl_bin1","VR2Japl_bin2","VR2Japl_bin3","VR2Japl_bin4","VR2Jnoapl_bin1","VR2Jnoapl_bin2","VR2Jnoapl_bin3","VR2Jnoapl_bin4","VR2Jmt_bin1","VR2Jmt_bin2","VR2Jmt_bin3","VR2Jmt_bin4","VR2Jnomt_bin1","VR2Jnomt_bin2","VR2Jnomt_bin3","VR2Jnomt_bin4"]

if '4Jhighx' in CRregions: 
    defined_regions+=['SR4Jhighx','SR4JhighxBV_bin1','SR4JhighxBV_bin2','SR4JhighxBV_bin3','SR4JhighxBV_bin4','SR4JhighxBT_bin1','SR4JhighxBT_bin2','SR4JhighxBT_bin3','SR4JhighxBT_bin4','WR4Jhighx','WR4Jhighx_bin1','WR4Jhighx_bin2','WR4Jhighx_bin3','WR4Jhighx_bin4','TR4Jhighx','TR4Jhighx_bin1','TR4Jhighx_bin2','TR4Jhighx_bin3','TR4Jhighx_bin4',"VR4Jhighxapl_bin1","VR4Jhighxapl_bin2","VR4Jhighxapl_bin3","VR4Jhighxapl_bin4","VR4Jhighxnoapl_bin1","VR4Jhighxnoapl_bin2","VR4Jhighxnoapl_bin3","VR4Jhighxnoapl_bin4","VR4Jhighxmt_bin1","VR4Jhighxmt_bin2","VR4Jhighxmt_bin3","VR4Jhighxmt_bin4","VR4Jhighxnomt_bin1","VR4Jhighxnomt_bin2","VR4Jhighxnomt_bin3","VR4Jhighxnomt_bin4"]

if '4Jlowx' in CRregions: 
    defined_regions+=['SR4Jlowx','SR4JlowxBV_bin1','SR4JlowxBV_bin2','SR4JlowxBV_bin3','SR4JlowxBV_bin4','SR4JlowxBT_bin1','SR4JlowxBT_bin2','SR4JlowxBT_bin3','SR4JlowxBT_bin4','WR4Jlowx','WR4Jlowx_bin1','WR4Jlowx_bin2','WR4Jlowx_bin3','WR4Jlowx_bin4','TR4Jlowx','TR4Jlowx_bin1','TR4Jlowx_bin2','TR4Jlowx_bin3','TR4Jlowx_bin4',"VR4Jlowxapl_bin1","VR4Jlowxapl_bin2","VR4Jlowxapl_bin3","VR4Jlowxapl_bin4","VR4Jlowxnoapl_bin1","VR4Jlowxnoapl_bin2","VR4Jlowxnoapl_bin3","VR4Jlowxnoapl_bin4","VR4Jlowxmt_bin1","VR4Jlowxmt_bin2","VR4Jlowxmt_bin3","VR4Jlowxmt_bin4","VR4Jlowxnomt_bin1","VR4Jlowxnomt_bin2","VR4Jlowxnomt_bin3","VR4Jlowxnomt_bin4"]

if '6J' in CRregions: 
    defined_regions+=['SR6J','SR6JBV_bin1','SR6JBV_bin2','SR6JBV_bin3','SR6JBV_bin4','SR6JBT_bin1','SR6JBT_bin2','SR6JBT_bin3','SR6JBT_bin4','WR6J','WR6J_bin1','WR6J_bin2','WR6J_bin3','WR6J_bin4','TR6J','TR6J_bin1','TR6J_bin2','TR6J_bin3','TR6J_bin4',"VR6Japl_bin1","VR6Japl_bin2","VR6Japl_bin3","VR6Japl_bin4","VR6Jnoapl_bin1","VR6Jnoapl_bin2","VR6Jnoapl_bin3","VR6Jnoapl_bin4","VR6Jmt_bin1","VR6Jmt_bin2","VR6Jmt_bin3","VR6Jmt_bin4","VR6Jnomt_bin1","VR6Jnomt_bin2","VR6Jnomt_bin3","VR6Jnomt_bin4"]



for pre_region in defined_regions:
            configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection
            configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection
            configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection  


# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")

#creating now list of samples
samplelist=[]
if 'Zjets' in mysamples: samplelist.append(ZSample)
if 'ttv' in mysamples: samplelist.append(ttbarVSample)
if 'singletop' in mysamples: samplelist.append(SingleTopSample)
if 'diboson' in mysamples: samplelist.append(DibosonsSample)
if 'Wjets' in mysamples: samplelist.append(WSample)
if 'ttbar' in mysamples: samplelist.append(TTbarSample)
if 'data' in mysamples: samplelist.append(DataSample)

if doOnlySignal: samplelist = [sigSample,DataSample]

#bkgOnly.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
bkgOnly.addSamples(samplelist)
    
if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.041) ## DS3
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in ['2J','4Jhighx','4Jlowx','6J']:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []


######################################################
# Add channels to Bkg-only configuration             #
######################################################


if "2J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR2J_bin1EM"],1,0.5,1.5),[elmuChans["2J"],bVetoChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR2J_bin2EM"],1,0.5,1.5),[elmuChans["2J"],bVetoChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR2J_bin3EM"],1,0.5,1.5),[elmuChans["2J"],bVetoChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR2J_bin4EM"],1,0.5,1.5),[elmuChans["2J"],bVetoChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR2J_bin1EM"],1,0.5,1.5),[elmuChans["2J"],bReqChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR2J_bin2EM"],1,0.5,1.5),[elmuChans["2J"],bReqChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR2J_bin3EM"],1,0.5,1.5),[elmuChans["2J"],bReqChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR2J_bin4EM"],1,0.5,1.5),[elmuChans["2J"],bReqChans["2J"]])
    bkgOnly.addBkgConstrainChannels(tmp)

if "4Jhighx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jhighx_bin1EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bVetoChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jhighx_bin2EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bVetoChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jhighx_bin3EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bVetoChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jhighx_bin4EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bVetoChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jhighx_bin1EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bReqChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jhighx_bin2EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bReqChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jhighx_bin3EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bReqChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jhighx_bin4EM"],1,0.5,1.5),[elmuChans["4Jhighx"],bReqChans["4Jhighx"]])
    bkgOnly.addBkgConstrainChannels(tmp)

if "4Jlowx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jlowx_bin1EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bVetoChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jlowx_bin2EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bVetoChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jlowx_bin3EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bVetoChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR4Jlowx_bin4EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bVetoChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jlowx_bin1EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bReqChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jlowx_bin2EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bReqChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jlowx_bin3EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bReqChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR4Jlowx_bin4EM"],1,0.5,1.5),[elmuChans["4Jlowx"],bReqChans["4Jlowx"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    
if "6J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6J_bin1EM"],1,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6J_bin2EM"],1,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6J_bin3EM"],1,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["WR6J_bin4EM"],1,0.5,1.5),[elmuChans["6J"],bVetoChans["6J"]])
    bkgOnly.addBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6J_bin1EM"],1,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])     
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6J_bin2EM"],1,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])     
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6J_bin3EM"],1,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])     
    bkgOnly.addBkgConstrainChannels(tmp)
    tmp = appendTo(bkgOnly.addChannel("cuts",["TR6J_bin4EM"],1,0.5,1.5),[elmuChans["6J"],bReqChans["6J"]])     
    bkgOnly.addBkgConstrainChannels(tmp)
          
                  
            

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]=False
for region in CRregions:
    ValidRegList["OneLep"]  += ValidRegList[region]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"]:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
       for region in CRregions:
           appendIfMatchName(c,bReqChans[region])
           appendIfMatchName(c,bVetoChans[region])
           appendIfMatchName(c,bAgnosticChans[region])
           appendIfMatchName(c,elChans[region])
           appendIfMatchName(c,muChans[region])      
           appendIfMatchName(c,elmuChans[region])
	   
   
    if doTableInputs:
        if "2J" in CRregions:
            #appendTo(validation.addValidationChannel("cuts",["SR2JEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JEM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
    
            #appendTo(validation.addValidationChannel("cuts",["SR2JBTEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JBTMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JBTEM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
            #appendTo(validation.addValidationChannel("cuts",["SR2JBVEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JBVMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JBVEM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])

            appendTo(validation.addValidationChannel("cuts",["SR2JBV_bin1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBT_bin1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBV_bin2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBT_bin2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBV_bin3EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBT_bin3EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBV_bin4EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["SR2JBT_bin4EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR2JBV_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBT_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBV_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBT_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBV_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBT_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBV_bin4EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR2JBT_bin4EM"]).doBlindingOverwrite = True

            appendTo(validation.addValidationChannel("cuts",["VR2Jmt_bin1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jmt_bin2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jmt_bin3EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jmt_bin4EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR2Japl_bin1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Japl_bin2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Japl_bin3EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Japl_bin4EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR2Jnomt_bin1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jnomt_bin2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jnomt_bin3EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jnomt_bin4EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR2Jnoapl_bin1EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jnoapl_bin2EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jnoapl_bin3EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("cuts",["VR2Jnoapl_bin4EM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
            
        if "4Jhighx" in CRregions:
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
    
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBVEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBVMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBVEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBTEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBTMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBTEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])

            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBV_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBT_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBV_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBT_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBV_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBT_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBV_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JhighxBT_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR4JhighxBV_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBT_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBV_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBT_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBV_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBT_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBV_bin4EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JhighxBT_bin4EM"]).doBlindingOverwrite = True

            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxmt_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxmt_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxmt_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxmt_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxapl_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxapl_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxapl_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxapl_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnomt_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnomt_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnomt_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnomt_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnoapl_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnoapl_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnoapl_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jhighxnoapl_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])

        if "4Jlowx" in CRregions:
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBVEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBVMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBVEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBTEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBTMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBTEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBV_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBT_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBV_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBT_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBV_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBT_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBV_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["SR4JlowxBT_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR4JlowxBV_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBT_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBV_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBT_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBV_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBT_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBV_bin4EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR4JlowxBT_bin4EM"]).doBlindingOverwrite = True

            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxmt_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxmt_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxmt_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxmt_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxapl_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxapl_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxapl_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxapl_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnomt_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnomt_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnomt_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnomt_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnoapl_bin1EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnoapl_bin2EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnoapl_bin3EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("cuts",["VR4Jlowxnoapl_bin4EM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

    
        if "6J" in CRregions:

            #appendTo(validation.addValidationChannel("cuts",["SR6JEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR6JMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR6JEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
    
            appendTo(validation.addValidationChannel("cuts",["SR6JBV_bin1EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBT_bin1EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBV_bin2EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBT_bin2EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBV_bin3EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBT_bin3EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBV_bin4EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["SR6JBT_bin4EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
	    
            if doBlindSRinBGfit:
                validation.getChannel("cuts",["SR6JBV_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBT_bin1EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBV_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBT_bin2EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBV_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBT_bin3EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBV_bin4EM"]).doBlindingOverwrite = True
                validation.getChannel("cuts",["SR6JBT_bin4EM"]).doBlindingOverwrite = True

	    #appendTo(validation.addValidationChannel("cuts",["SR6JBTEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR6JBTMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR6JBTEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
	    
            appendTo(validation.addValidationChannel("cuts",["VR6Jmt_bin1EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jmt_bin2EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jmt_bin3EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jmt_bin4EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
	    
            appendTo(validation.addValidationChannel("cuts",["VR6Japl_bin1EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Japl_bin2EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Japl_bin3EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Japl_bin4EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
	    
            appendTo(validation.addValidationChannel("cuts",["VR6Jnomt_bin1EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jnomt_bin2EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jnomt_bin3EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jnomt_bin4EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])

            appendTo(validation.addValidationChannel("cuts",["VR6Jnoapl_bin1EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jnoapl_bin2EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jnoapl_bin3EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("cuts",["VR6Jnoapl_bin4EM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
	    
	    

        # All validation regions should use over-flow bins!
        for v in validation.channels : 
            if not 'cuts' in v.name: v.useOverflowBin=True

    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs       
    

# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}
    SRs=[]

    SRs+=["SR2JBV_bin1EM","SR2JBV_bin2EM","SR2JBV_bin3EM","SR2JBV_bin4EM","SR2JBT_bin1EM","SR2JBT_bin2EM","SR2JBT_bin3EM","SR2JBT_bin4EM","SR4JhighxBV_bin1EM","SR4JhighxBV_bin2EM","SR4JhighxBV_bin3EM","SR4JhighxBV_bin4EM","SR4JhighxBT_bin1EM","SR4JhighxBT_bin2EM","SR4JhighxBT_bin3EM","SR4JhighxBT_bin4EM","SR4JlowxBV_bin1EM","SR4JlowxBV_bin2EM","SR4JlowxBV_bin3EM","SR4JlowxBV_bin4EM","SR4JlowxBT_bin1EM","SR4JlowxBT_bin2EM","SR4JlowxBT_bin3EM","SR4JlowxBT_bin4EM","SR6JBV_bin1EM","SR6JBV_bin2EM","SR6JBV_bin3EM","SR6JBV_bin4EM","SR6JBT_bin1EM","SR6JBT_bin2EM","SR6JBT_bin3EM","SR6JBT_bin4EM"]
    
    
    #SRs+=["SR2JBV_bin1EM","SR2JBV_bin2EM","SR2JBV_bin3EM","SR2JBV_bin4EM","SR2JBT_bin1EM","SR2JBT_bin2EM","SR2JBT_bin3EM","SR2JBT_bin4EM"]
    #SRs+=["SR4JhighxBV_bin1EM","SR4JhighxBV_bin2EM","SR4JhighxBV_bin3EM","SR4JhighxBV_bin4EM","SR4JhighxBT_bin1EM","SR4JhighxBT_bin2EM","SR4JhighxBT_bin3EM","SR4JhighxBT_bin4EM"]
    #SRs+=["SR4JlowxBV_bin1EM","SR4JlowxBV_bin2EM","SR4JlowxBV_bin3EM","SR4JlowxBV_bin4EM","SR4JlowxBT_bin1EM","SR4JlowxBT_bin2EM","SR4JlowxBT_bin3EM","SR4JlowxBT_bin4EM"]
    #SRs+=["SR6JBV_bin1EM","SR6JBV_bin2EM","SR6JBV_bin3EM","SR6JBV_bin4EM","SR6JBT_bin1EM","SR6JBT_bin2EM","SR6JBT_bin3EM","SR6JBT_bin4EM"]
    

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
                
        #signal-specific uncertainties  
        sigSample.setStatConfig(useStat)
        #sigSample.addSystematic(xsecSig)
        ######### Signal uncertainties #####################                                               
        errsig = 0.001 ## still need inputs for other regions
        if '2J' in CRregions: 
            if 'GG' in sigSamples[0]: errsig = 0.12
            #if 'SS' in sigSamples[0]: errsig = 0.15

        if '6J' in CRregions: errsig = 0.10
        if '4Jlowx'  in CRregions:  errsig = 0.10
        if '4Jhighx' in CRregions: errsig = 0.15
        #print "sig=",sig,"  region=",ERRregion,"errsig=",errsig
        #systSig = Systematic("SigSyst",configMgr.weights,1.00+errsig,1.00-errsig,"user","userOverallSys")
        #################################################                                               

        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if not doShapeFit:
                ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
            else:
                if sr=='SR2JEM' or sr=='SR2JBTEM'  or sr=='SR2JBVEM' :
                    ch = myTopLvl.addChannel("meffInc30",[sr],5,600,2200)
                    ch.useOverflowBin=True     
                elif sr=='SR4JhighxEM' or sr=='SR4JhighxBTEM' or sr=='SR4JhighxBVEM':
                    ch = myTopLvl.addChannel("meffInc30",[sr],5,600,2200)
                    ch.useOverflowBin=True
                elif sr=='"SR4JlowxEM"' or sr=='SR4JlowxBTEM' or sr=='SR4JlowxBVEM':
                    ch = myTopLvl.addChannel("meffInc30",[sr],5,600,2200)
                    ch.useOverflowBin=True
                elif sr=='SR6JEM' or sr=='SR6JBVEM' or sr=='SR6JBTEM':
                    ch = myTopLvl.addChannel("meffInc30",[sr],5,1000,2400)   
                    ch.useOverflowBin=True
                else: 
                    raise RuntimeError("This region is not yet implemented in a shape fit mode: %s"%sr)

            for region in CRregions:
                if region in sr:
                    if 'El' in sr:                
                        elChans[region].append(ch) 
                    elif 'Mu' in sr:
                        muChans[region].append(ch)
                    elif 'EM' in sr:
                        elmuChans[region].append(ch)
                    else: raise RuntimeError("Unexpected signal region %s"%sr)
                    bAgnosticChans[region].append(ch)
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            #ch.getSample(sig).addSystematic(systSig) 
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)
        

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]
    

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]
    tgtchan = tgt[1]
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
        SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
        SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
    
            #if "SR2J" in chan.name:	    
                #theSample.removeWeight("pileupWeight")
                #theSample.removeSystematic("pileup")

             
            sys_region = ""
            if "2J" in chan.name: sys_region = "2J"
            elif "4Jhighx" in chan.name: sys_region = "4Jhighx"
            elif "4Jlowx" in chan.name: sys_region = "4Jlowx"
            elif "6J" in chan.name: sys_region = "6J"
                    
            else: 
                print "Unknown region! - Take systematics from 6J regions."
                sys_region = "6J"

            # replacement of JES PunchThrough systematic for AF2 signal samples (not for FullSim signal samples)
            if not debug and not (sig in FullSimSig):
                print "This is an AFII signal sample -> removing JES_PunchThrough_MC15, adding JES_PunchThrough_AFII and JES_RelativeNonClosure_AFII systematics"
                theSample.removeSystematic("JES_PunchThrough_MC15")
                theSample.addSystematic(Systematic("JES_PunchThrough_AFII","_NoSys","_JET_PunchThrough_AFII__1up","_JET_PunchThrough_AFII__1down","tree","overallNormHistoSys"))
                theSample.addSystematic(Systematic("JES_RelativeNonClosure_AFII","_NoSys","_JET_RelativeNonClosure_AFII__1up","_JET_RelativeNonClosure_AFII__1down","tree","overallNormHistoSys"))            

            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans_all:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all:
                theSigFiles = sigFiles_em[sig]	                  
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels
for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")
       
    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            if 'Zjets' in mysamples: chan.getSample("zjets_Sherpa22").addSystematic(ZjetsComm[region])
            if 'singletop' in mysamples: chan.getSample("singletop").addSystematic(SingleTopComm[region])
            if not '6J' in region:
               if  'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").addSystematic(WjetsComm[region])
               if  'diboson' in mysamples: chan.getSample("diboson").addSystematic(DBComm[region])
               if  'ttbar' in mysamples: chan.getSample("ttbar").addSystematic(TtbarComm[region])
            else:
               print "6J"
	    
	    
            #print chan.name
            if useNJetNormFac:
                if 'bin1' in chan.name:		    
                    if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"_bin1EM","cuts"),("TR"+region+"_bin1EM","cuts")])
                    if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WR"+region+"_bin1EM","cuts"),("TR"+region+"_bin1EM","cuts")])
                    if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WR"+region+"_bin1EM","cuts"),("TR"+region+"_bin1EM","cuts")])
		    		    
                elif 'bin2' in chan.name:
                    if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"_bin2EM","cuts"),("TR"+region+"_bin2EM","cuts")])
                    if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WR"+region+"_bin2EM","cuts"),("TR"+region+"_bin2EM","cuts")])
                    if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WR"+region+"_bin2EM","cuts"),("TR"+region+"_bin2EM","cuts")])
		         
                elif 'bin3' in chan.name:
                    if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"_bin3EM","cuts"),("TR"+region+"_bin3EM","cuts")])
                    if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WR"+region+"_bin3EM","cuts"),("TR"+region+"_bin3EM","cuts")])
                    if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WR"+region+"_bin3EM","cuts"),("TR"+region+"_bin3EM","cuts")])    
                elif 'bin4' in chan.name:
                    if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"_bin4EM","cuts"),("TR"+region+"_bin4EM","cuts")])
                    if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WR"+region+"_bin4EM","cuts"),("TR"+region+"_bin4EM","cuts")])
                    if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WR"+region+"_bin4EM","cuts"),("TR"+region+"_bin4EM","cuts")])    
                else:
                    print chan.name 
                    print 'ERROR: unknown bin'
            else:
                if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"_bin1EM","cuts"),("TR"+region+"_bin1EM","cuts"),("WR"+region+"_bin2EM","cuts"),("TR"+region+"_bin2EM","cuts"),("WR"+region+"_bin3EM","cuts"),("TR"+region+"_bin3EM","cuts"),("WR"+region+"_bin4EM","cuts"),("TR"+region+"_bin4EM","cuts")])
                if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WR"+region+"_bin1EM","cuts"),("TR"+region+"_bin1EM","cuts"),("WR"+region+"_bin2EM","cuts"),("TR"+region+"_bin2EM","cuts"),("WR"+region+"_bin3EM","cuts"),("TR"+region+"_bin3EM","cuts"),("WR"+region+"_bin4EM","cuts"),("TR"+region+"_bin4EM","cuts")])
                if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WR"+region+"_bin1EM","cuts"),("TR"+region+"_bin1EM","cuts"),("WR"+region+"_bin2EM","cuts"),("TR"+region+"_bin2EM","cuts"),("WR"+region+"_bin3EM","cuts"),("TR"+region+"_bin3EM","cuts"),("WR"+region+"_bin4EM","cuts"),("TR"+region+"_bin4EM","cuts")])
    
#####################################################
	# Add separate Normalization Factors for ttbar and W+jets for each CR                               #
######################################################
if useNJetNormFac:
   
    for chan in AllChannels_all:
        mu_W_Xj = "mu_W_XJ"
        mu_Top_Xj = "mu_Top_XJ"
        ### doldol
        if "2J" in chan.name:
            if "bin1" in chan.name:
                mu_W_Xj = "mu_W_2J_bin1"
                mu_Top_Xj = "mu_Top_2J_bin1"
            elif "bin2" in chan.name:
                mu_W_Xj = "mu_W_2J_bin2"
                mu_Top_Xj = "mu_Top_2J_bin2"
            elif "bin3" in chan.name:
                mu_W_Xj = "mu_W_2J_bin3"
                mu_Top_Xj = "mu_Top_2J_bin3"
            elif "bin4" in chan.name:
                mu_W_Xj = "mu_W_2J_bin4"
                mu_Top_Xj = "mu_Top_2J_bin4"
		

        elif "4Jhighx" in chan.name:
            if "bin1" in chan.name:
                mu_W_Xj = "mu_W_4Jhighx_bin1"
                mu_Top_Xj = "mu_Top_4Jhighx_bin1"
            elif "bin2" in chan.name:
                mu_W_Xj = "mu_W_4Jhighx_bin2"
                mu_Top_Xj = "mu_Top_4Jhighx_bin2"
            elif "bin3" in chan.name:
                mu_W_Xj = "mu_W_4Jhighx_bin3"
                mu_Top_Xj = "mu_Top_4Jhighx_bin3"
            elif "bin4" in chan.name:
                mu_W_Xj = "mu_W_4Jhighx_bin4"
                mu_Top_Xj = "mu_Top_4Jhighx_bin4"

        elif "4Jlowx" in chan.name:
            if "bin1" in chan.name:
                mu_W_Xj = "mu_W_4Jlowx_bin1"
                mu_Top_Xj = "mu_Top_4Jlowx_bin1"
            elif "bin2" in chan.name:
                mu_W_Xj = "mu_W_4Jlowx_bin2"
                mu_Top_Xj = "mu_Top_4Jlowx_bin2"
            elif "bin3" in chan.name:
                mu_W_Xj = "mu_W_4Jlowx_bin3"
                mu_Top_Xj = "mu_Top_4Jlowx_bin3"
            elif "bin4" in chan.name:
                mu_W_Xj = "mu_W_4Jlowx_bin4"
                mu_Top_Xj = "mu_Top_4Jlowx_bin4"

        elif "6J" in chan.name:
            if "bin1" in chan.name:
                mu_W_Xj = "mu_W_6J_bin1"
                mu_Top_Xj = "mu_Top_6J_bin1"
            elif "bin2" in chan.name:
                mu_W_Xj = "mu_W_6J_bin2"
                mu_Top_Xj = "mu_Top_6J_bin2"
            elif "bin3" in chan.name:
                mu_W_Xj = "mu_W_6J_bin3"
                mu_Top_Xj = "mu_Top_6J_bin3"
            elif "bin4" in chan.name:
                mu_W_Xj = "mu_W_6J_bin4"
                mu_Top_Xj = "mu_Top_6J_bin4"
   
    
        else:
            log.warning("Channel %s gets no separated normalization factor" % chan.name)
            
        if 'Wjets' in mysamples: chan.getSample(WSampleName).addNormFactor(mu_W_Xj,1.,4.,0.)
        if 'ttbar' in mysamples: chan.getSample(TTbarSampleName).addNormFactor(mu_Top_Xj,1.,4.,0.)
        if 'singletop' in mysamples: chan.getSample(SingleTopSampleName).addNormFactor(mu_Top_Xj,1.,4.,0.1)
        log.info("Adding additional normalization factors (%s, %s) to channel (%s)" %(mu_W_Xj, mu_Top_Xj, chan.name))

    meas.addParamSetting("mu_W","const",1.0)
    meas.addParamSetting("mu_Top","const",1.0)
    
    if validation:
        meas_valid  = validation.getMeasurement("BasicMeasurement")
        meas_valid.addParamSetting("mu_W","const",1.0)
        meas_valid.addParamSetting("mu_Top","const",1.0)
        #meas.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)

       
    if myFitType==FitType.Exclusion:
        for sig in sigSamples:
            meas_excl=configMgr.getFitConfig("Sig_%s"%sig).getMeasurement("BasicMeasurement")
            meas_excl.addParamSetting("mu_W","const",1.0)
            meas_excl.addParamSetting("mu_Top","const",1.0)
     
        


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()
MeffBins = [ '1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    #if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    
    if "TR6J_bin1" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 400.
        chan.titleX="cuts_TR6J_bin1EM"
    elif "TR6J_bin2" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 400.
        chan.titleX="cuts_TR6J_bin2EM"
    elif "TR6J_bin3" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 400.
        chan.titleX="cuts_TR6J_bin3EM"
    elif "TR6J_bin4" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 400.
        chan.titleX="cuts_TR6J_bin4EM"	
  	
    elif "WR6J_bin1" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 500.
        chan.titleX="cuts_TR6J_bin1EM"
    elif "WR6J_bin2" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 500.
        chan.titleX="cuts_TR6J_bin2EM"
    elif "WR6J_bin3" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 500.
        chan.titleX="cuts_TR6J_bin3EM"
    elif "WR6J_bin4" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 500.
        chan.titleX="cuts_TR6J_bin4EM"
	
	
    elif "VR6Jmt_bin1" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 100.
        chan.titleX="cuts_VR6Jmt_bin1EM"
    elif "VR6Jmt_bin2" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 100.
        chan.titleX="cuts_VR6Jmt_bin2EM"
    elif "VR6Jmt_bin3" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 100.
        chan.titleX="cuts_VR6Jmt_bin3EM"
    elif "VR6Jmt_bin4" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 100.
        chan.titleX="cuts_VR6Jmt_bin4EM"
	
    elif "VR6Japl_bin1" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 150.
        chan.titleX="cuts_VR6Japl_bin1EM"
    elif "VR6Japl_bin2" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 150.
        chan.titleX="cuts_VR6Japl_bin2EM"
    elif "VR6Japl_bin3" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 150.
        chan.titleX="cuts_VR6Japl_bin3EM"
    elif "VR6Japl_bin4" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 150.
        chan.titleX="cuts_VR6Japl_bin4EM"
	
    elif "VR6Jnomt_bin1" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Jmt_bin1EM"
    elif "VR6Jnomt_bin2" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Jmt_bin2EM"
    elif "VR6Jnomt_bin3" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Jmt_bin3EM"
    elif "VR6Jnomt_bin4" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Jmt_bin4EM"
	
    elif "VR6Jnoapl_bin1" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Japl_bin1EM"
    elif "VR6Jnoapl_bin2" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Japl_bin2EM"
    elif "VR6Jnoapl_bin3" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Japl_bin3EM"
    elif "VR6Jnoapl_bin4" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 1000.
        chan.titleX="cuts_VR6Japl_bin4EM"
		
	
	
	
	
    elif "SR6J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 18.5
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 25.5
        chan.titleY = "Events / 100 GeV"
        chan.titleX = "p_{T} [GeV]"      
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
