################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch

import Winter1617.SignalAccUncertainties

from logger import Logger
log = Logger('OneLepton')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onWestGrid='westgrid' in commands.getstatusoutput("hostname")[1] or fnmatch.fnmatch(commands.getstatusoutput("hostname")[1], 'u*')
onUBCAtlasServ='atlasserv' in commands.getstatusoutput("hostname")[1]
onMOGON="mogon" in commands.getstatusoutput("hostname")[1]
onDOL= False
if onDOL:    onLxplus=False  ## previous status does not detect the batch system in lxplus

debug = False #!!!

if onDOL:  debug = True  ## for test

if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]

SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
SystList.append("JVT")      # JVT scale factors
SystList.append("pileup")   # pileup (common)
SystList.append("BTag")     # b-tagging uncertainties
SystList.append("Dibosons") # scale variation for renormalization, factorization, resummation and generator comparison
SystList.append("Wjets")    # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
SystList.append("Ttbar")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
SystList.append("SingleTop")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
SystList.append("ttbarPDF") # 
SystList.append("wjetsPDF") # 
SystList.append("Zjets")    # scale variation for renormalization, factorization, resummation,matching

if debug:
    SystList=[]
    
#for creating the backup chache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False
if onDOL: doHistoBuilding = False

# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=False

useNJetNormFac=False
# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["2J","4Jhighx","4Jlowx","6J"] #this is the default - modify from command line


# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 2J,4Jhighx,4Jlowx,6J  option)"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["2J"] = False
ValidRegList["4Jhighx"] = False
ValidRegList["4Jlowx"] = False
ValidRegList["6J"] = False



# for tables (turn doTableInputs to True)
doTableInputs = True #This effectively means no validation plots but only validation tables (but is 100x faster)
if onDOL:  doTableInputs = False
for cr in CRregions:
    if "2J" in cr and doTableInputs:
        ValidRegList["2J"] = True
    if "4Jhighx" in cr and doTableInputs:
        ValidRegList["4Jhighx"] = True	
    if "4Jlowx" in cr and doTableInputs:
        ValidRegList["4Jlowx"] = True
    if "6J" in cr and doTableInputs:
        ValidRegList["6J"] = True    	

#N-1 plots in SRs        
VRplots=False

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=True


# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    
    #sigSamples=["GG_oneStep_945_785_625"]
    #sigSamples=["GG_oneStep_637_626_615"]    
   
    #sigSamples=["GG_oneStep_1105_865_625"]
    sigSamples=["GG_oneStep_1385_705_25"]
    
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
'''    
if myFitType==FitType.Exclusion:
    if onDOL:
        analysissuffix += '_'+ sigSamples[0]
    if 'GG_oneStep' in sigSamples[0]  and not onDOL:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]  and not onDOL:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]
'''	
	

mylumi= 36.06596#36.46161#14.78446 #30 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  

mysamples = ["Zjets","ttv","singletop","diboson","Wjets","ttbar","data"]
doOnlySignal=False

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
	
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "OneLepton"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
useBackupCache = True
if (onMOGON or onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "OneLepton"+analysissuffix_BackupCache+analysisextension
    if not onLxplus: histBackupCacheFileName = "data/backup.root"
    #if not onLxplus: histBackupCacheFileName = "data/OneLepton_6J_GG_oneStep_samples_Wjets.root"
    else: histBackupCacheFileName = "data/OneLepton_2J_4Jhighx_4Jlowx_6J_GG_oneStep.root"
    if onDOL:    histBackupCacheFileName = "/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/HISTFITTER/HistFitterUser/MET_jets_leptons/data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

if onDOL:
    configMgr.fixSigXSec=False
else:
    configMgr.fixSigXSec=True

useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)
doBlindSRinBGfit  = False # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = []

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

#inputDir="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
#inputDirSig="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
#inputDirData="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"

inputDir="root://eosatlas.cern.ch//eos/atlas/user/v/vtudorac/TreesTag0610/"
inputDirData="root://eosatlas.cern.ch//eos/atlas/user/v/vtudorac/TreesTag0610/"
inputDirSig="root://eosatlas.cern.ch//eos/atlas/user/v/vtudorac/TreesTag0610/"

if not onLxplus:
    inputDir="/project/etp5/SUSYInclusive/trees/T_06_10/"
    inputDirSig="/project/etp5/SUSYInclusive/trees/T_06_10/"
    inputDirData="/project/etp5/SUSYInclusive/trees/T_06_10/"

if onWestGrid :
    inputDir="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirSig="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirData="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"

if onUBCAtlasServ :
    inputDir="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirSig="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirData="/mnt/xrootdd/mgignac/MC15C/T_06_03/"

if onMOGON:
    inputDir="../InputTrees/"
    inputDirSig="../InputTrees/"
    inputDirData="../InputTrees/"

if onDOL:
    inputDir="/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/1l/1l_TREE/MORIOND2017/"
    inputDirSig="/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/1l/1l_TREE/MORIOND2017/"
    inputDirData="/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/1l/1l_TREE/MORIOND2017/"
    
#background files, separately for electron and muon channel:
#bgdFiles_e = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]
#bgdFiles_m = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]
#bgdFiles_em = [inputDir+"allTrees_T_06_05_skimslim_met200_bkg.root"]

bgdFiles_e = [inputDir+"allTrees_T_06_10_bkg.root"]
bgdFiles_m = [inputDir+"allTrees_T_06_10_bkg.root"]
bgdFiles_em = [inputDir+"allTrees_T_06_10_bkg.root"]
#bgdFiles_e = [inputDir+"allTrees_T_06_09_skimslim_bkg.root"]
#bgdFiles_m = [inputDir+"allTrees_T_06_09_skimslim_bkg.root"]
#bgdFiles_em = [inputDir+"allTrees_T_06_09_skimslim_bkg.root"]

#signal files
if myFitType==FitType.Exclusion or doOnlySignal:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allTrees_T_06_10_signal.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allTrees_T_06_10_signal.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allTrees_T_06_10_signal.root"]
        
#data files
#dataFiles = [inputDirData+"allTrees_T_06_03_dataOnly.root"] # 949.592 /pb
#dataFiles = [inputDirData+"allDS2_T_06_03.root"]    # DS2: 13277.66 /pb (remember to check lumiErr: should be 2.9%)
#dataFiles = [inputDirData+"allDS2v01_T_06_03.root"] # DS2.1: 14784.46 /pb (remember to check lumiErr: should be 3.0%)
#dataFiles = [inputDirData+"allTrees_T_06_05_data.root"] # DS3: 36461.61 /pb (remember to check lumiErr: should be 4.1%)
dataFiles = [inputDirData+"allTrees_T_06_10_data.root"] # (lumiErr=0.032)
# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","jvtWeight","SherpaVjetsNjetsWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights

#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")

muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")

muonHighTTVAWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1up")
muonLowTTVAWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1down")
muonHighTTVAWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1up")
muonLowTTVAWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1down")

#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#### electron related uncertainties acting on weights ####
#ID 
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Rec
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Iso
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")


# JVT weight systematics
#JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyUp")
#JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyDown")

JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1up")
JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1down")


# pileup weight systematics
pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

    eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
    eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}



#ZjetsComm = {}
WjetsCommbin1={}
WjetsCommbin2={}
WjetsCommbin3={}
WjetsCommbin4={}
#TtbarComm={}
#DBComm={}



for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 35% syst and an overallSys
   
     
    #basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.35,0.65,"user","userOverallSys"))
   
    	
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_NoSys","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_NoSys","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_NoSys","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_NoSys","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_NoSys","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_NoSys","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6","_NoSys","_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_7","_NoSys","_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_8restTerm","_NoSys","_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree","overallNormHistoSys"))
	     
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_NoSys","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_NoSys","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_NoSys","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_NoSys","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_NoSys","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_NoSys","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_NoSys","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_NoSys","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_NoSys","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_NoSys","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys"))

    	        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID","_NoSys","_MUON_ID__1up","_MUON_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS","_NoSys","_MUON_MS__1up","_MUON_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SCALE","_NoSys","_MUON_SCALE__1up","_MUON_SCALE__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_stat",configMgr.weights,muonHighTTVAWeights_stat,muonLowTTVAWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_sys",configMgr.weights,muonHighTTVAWeights_sys,muonLowTTVAWeights_sys,"weight","overallNormHistoSys"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Eff",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys")) 

    if "JVT" in SystList :
        #basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))
    if "BTag" in SystList:
        bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")
        eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,eTagFromCHighWeights,eTagFromCLowWeights,"weight","overallNormHistoSys")

    
    #if 'Zjets' in mysamples:
        #ZjetsComm[region] = Systematic("h1L_ZjetsComm", configMgr.weights,configMgr.weights+["( 1+0.5 )"],configMgr.weights+["( 1-0.5 )"], "weight","overallSys")
    #if 'diboson' in mysamples:     
        #DBComm[region] = Systematic("h1L_DBComm", configMgr.weights,configMgr.weights+["( 1+0.6 )"],configMgr.weights+["( 1-0.6 )"], "weight","overallSys")
 
    if 'Wjets' in mysamples:
        WjetsCommbin1[region] = Systematic("h1L_WjetsComm_"+region+"_bin1", configMgr.weights,configMgr.weights+["( 1+0.3)"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys") 
        WjetsCommbin2[region] = Systematic("h1L_WjetsComm_"+region+"_bin2", configMgr.weights,configMgr.weights+["( 1+0.3)"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys") 
        WjetsCommbin3[region] = Systematic("h1L_WjetsComm_"+region+"_bin3", configMgr.weights,configMgr.weights+["( 1+0.3)"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys")       
        WjetsCommbin4[region] = Systematic("h1L_WjetsComm_"+region+"_bin4", configMgr.weights,configMgr.weights+["( 1+0.3)"],configMgr.weights+["( 1-0.3 )"], "weight","overallSys") 
        
    #signal uncertainties                                                                                       
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")

    #Generator Systematics
    generatorSyst = []  	
    
    if "Dibosons" in SystList and 'diboson' in mysamples:
       import Winter1617.theoryUncertainties_OneLepton_Dibosons
       Winter1617.theoryUncertainties_OneLepton_Dibosons.TheorUnc(generatorSyst)
       
    if "Wjets" in SystList and 'Wjets' in mysamples:        
       import Winter1617.theoryUncertainties_OneLepton_Wjets
       Winter1617.theoryUncertainties_OneLepton_Wjets.TheorUnc(generatorSyst)
            
    if "Ttbar" in SystList and 'ttbar' in mysamples:      
        import Winter1617.theoryUncertainties_OneLepton_Ttbar
        Winter1617.theoryUncertainties_OneLepton_Ttbar.TheorUnc(generatorSyst)
    if "SingleTop" in SystList and 'singletop' in mysamples:
        import Winter1617.theoryUncertainties_OneLepton_SingleTop
        Winter1617.theoryUncertainties_OneLepton_SingleTop.TheorUnc(generatorSyst)
        
#    if "ttbarPDF" in SystList and 'ttbar' in mysamples:
#        import Winter1617.theoryUncertainties_OneLepton_ttbarPDF
#        Winter1617.theoryUncertainties_OneLepton_ttbarPDF.TheorUnc(generatorSyst)
#    if "wjetsPDF" in SystList and 'Wjets' in mysamples:
#        import Winter1617.theoryUncertainties_OneLepton_wjetsPDF
#        Winter1617.theoryUncertainties_OneLepton_wjetsPDF.TheorUnc(generatorSyst)		
    if "Zjets" in SystList and 'Zjets' in mysamples:
        import Winter1617.theoryUncertainties_OneLepton_Zjets
        Winter1617.theoryUncertainties_OneLepton_Zjets.TheorUnc(generatorSyst)
       
	
    

# ********************************************************************* #
#                              Background samples

# ********************************************************************* #

configMgr.nomName = "_NoSys"

##we need to split the top and the W+jets samples into slices of meff to give them individual normalization factors (be careful that the samples may then only be added to the correspodning regions and not to the fit config!)

meff_binning = {}
meff_binning["2J"] = (700.,1100.,1500.,1900.,2300.)
meff_binning["4Jlowx"] = (1300.,1650.,2000.,2350.)
meff_binning["4Jhighx"] = (1000.,1500.,2000.,2500.)
meff_binning["6J"] = (700.,1233.25,1766.5,2299.75,2833.)

WSample = {}
TTbarSample={}
SingleTopSample={}
for region in CRregions:
    WSample[region] = {}
    TTbarSample[region] = {}
    SingleTopSample[region]= {}
    
    for v,i in enumerate(["bin1","bin2","bin3","bin4"]):
        
        final_bin = 3
        
        # need to add an exception for the 4-jet regions as these have only 3 bins
        if region == '4Jlowx' or region == '4Jhighx':
            final_bin = 2
        
        WSample[region][i] = Sample("Wjets_"+region+"_"+i,kAzure-4)
        WSample[region][i].setStatConfig(useStat)
        WSample[region][i].setNormFactor("mu_W_"+region+"_"+i,1.,0.,5.)
        WSample[region][i].setPrefixTreeName("wjets_Sherpa221")
	
        TTbarSample[region][i] = Sample("ttbar_"+region+"_"+i,kGreen-9)
        TTbarSample[region][i].setStatConfig(useStat)
        TTbarSample[region][i].setNormFactor("mu_Top_"+region+"_"+i,1.,0.,5.)
        TTbarSample[region][i].setPrefixTreeName("ttbar")
	
        SingleTopSample[region][i] = Sample("singletop_"+region+"_"+i,kGreen-5)
        SingleTopSample[region][i].setStatConfig(useStat)
        SingleTopSample[region][i].setNormFactor("mu_Top_"+region+"_"+i,1.,0.,5.)
        SingleTopSample[region][i].setPrefixTreeName("singletop")
	
	
        if v < final_bin:
            WSample[region][i].addSampleSpecificWeight("(meffInc30 >= "+str(meff_binning[region][v])+" && meffInc30 < "+str(meff_binning[region][v+1])+")")
            print "Wsample for region "+ region + " in bin "+ i + "active for meff between " + str(meff_binning[region][v]) + " and " + str(meff_binning[region][v+1])
            TTbarSample[region][i].addSampleSpecificWeight("(meffInc30 >= "+str(meff_binning[region][v])+" && meffInc30 < "+str(meff_binning[region][v+1])+")")
            print "Ttbarsample for region "+ region + " in bin "+ i + "active for meff between " + str(meff_binning[region][v]) + " and " + str(meff_binning[region][v+1])
            SingleTopSample[region][i].addSampleSpecificWeight("(meffInc30 >= "+str(meff_binning[region][v])+" && meffInc30 < "+str(meff_binning[region][v+1])+")")
            print "SingleTopsample for region "+ region + " in bin "+ i + "active for meff between " + str(meff_binning[region][v]) + " and " + str(meff_binning[region][v+1])
	     
	    	    
        else:
            WSample[region][i].addSampleSpecificWeight("(meffInc30 >= "+str(meff_binning[region][v])+")")
            print "Wsample for region "+ region + " in bin "+ i + "active for meff above " + str(meff_binning[region][v]) 
            TTbarSample[region][i].addSampleSpecificWeight("(meffInc30 >= "+str(meff_binning[region][v])+")")
            print "Ttbarsample for region "+ region + " in bin "+ i + "active for meff above " + str(meff_binning[region][v])
            SingleTopSample[region][i].addSampleSpecificWeight("(meffInc30 >= "+str(meff_binning[region][v])+")")
            print "SingleTopsample for region "+ region + " in bin "+ i + "active for meff above " + str(meff_binning[region][v]) 
	    
	    

#WSampleName = "wjets_Sherpa221"
#WSample = Sample(WSampleName,kAzure-4)
#WSample.setNormFactor("mu_W",1.,0.,5.)
#WSample.setStatConfig(useStat)

#
#TTbarSampleName = "ttbar"
#TTbarSample = Sample(TTbarSampleName,kGreen-9)
#TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
#TTbarSample.setStatConfig(useStat)

#
DibosonsSampleName = "diboson_Sherpa221"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()

#
#SingleTopSampleName = "singletop"
#SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
#SingleTopSample.setNormFactor("mu_Top",1.,0.,5.)
#SingleTopSample.setStatConfig(useStat)
#SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets_Sherpa221"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()

#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()

if doOnlySignal: 
    sigSample = Sample(sigSamples[0],kPink)    
    sigSample.setStatConfig(useStat)
    sigSample.setNormByTheory()
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)
    sigSample.setFileList(sigFiles_em[sigSamples[0]])

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&  nLep_base==1&&nLep_signal==1 && trigMatch_metTrig"## TSTcleaning here
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>7) "
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>6)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
TwoLepSelection = "&&  nLep_base>=2 && nLep_signal>=2 && trigMatch_metTrig"

# ------- 2J region --------------------------------------------------------------------------- #
SR2JSelection        = "lep1Pt<35 && nJet30>=2 && met>430. && mt>100. && (met/meffInc30) > 0.25 && (nJet30/lep1Pt)>0.2"
configMgr.cutsDict["SR2J"]=  SR2JSelection + CommonSelection
configMgr.cutsDict["SR2JBV"]=SR2JSelection +"&& nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR2JBT"]=SR2JSelection +"&& nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["SR2JBTnomet"]="meffInc30>700. && lep1Pt<35 && nJet30>=2 && mt>100. && (met/meffInc30) > 0.25 && (nJet30/lep1Pt)>0.2 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR2JBTnomt"]="meffInc30>700. && lep1Pt<35 && nJet30>=2 && met>430. && (met/meffInc30) > 0.25 && (nJet30/lep1Pt)>0.2 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR2JBVnomet"]="meffInc30>700. && lep1Pt<35 && nJet30>=2 && mt>100. && (met/meffInc30) > 0.25 && (nJet30/lep1Pt)>0.2 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR2JBVnomt"]="meffInc30>700. && lep1Pt<35 && nJet30>=2 && met>430. && (met/meffInc30) > 0.25 && (nJet30/lep1Pt)>0.2 && nBJet30_MV2c10==0" + CommonSelection


CR2JSelection        = "lep1Pt<35 && nJet30>=2 && met>300 &&met<430 && mt>40 &&mt<100 && (met/meffInc30)>0.15 && (nJet30/lep1Pt)>0.2"
configMgr.cutsDict["TR2J"]=CR2JSelection +" && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["WR2J"]=CR2JSelection +" && nBJet30_MV2c10==0" + CommonSelection

VR2JmtSelection      = "lep1Pt<35 && nJet30>=2 && met>300 && met<430  && mt>100 && (met/meffInc30)>0.10 && (nJet30/lep1Pt)>0.2 "
VR2JmetSelection      = "lep1Pt<35 && nJet30>=2 && met>430  && mt>40 &&mt<100 && (met/meffInc30)>0.25 && (nJet30/lep1Pt)>0.2"
VR2JnometSelection="lep1Pt<35 && nJet30>=2 && mt>40 &&mt<100 && (met/meffInc30)>0.25 && (nJet30/lep1Pt)>0.2" 
VR2JnomtSelection= "lep1Pt<35 && nJet30>=2 && met>300 && met<430 && (met/meffInc30)>0.10 && (nJet30/lep1Pt)>0.2"
configMgr.cutsDict["VR2Jmet"]=VR2JmetSelection + CommonSelection
configMgr.cutsDict["VR2Jmt"]=VR2JmtSelection + CommonSelection
configMgr.cutsDict["VR2Jnomet"]=VR2JnometSelection+ CommonSelection
configMgr.cutsDict["VR2Jnomt"]=VR2JnomtSelection + CommonSelection


# ------- 4J regions for gluino gridx high x--------------------------------------------------------------------------- #
SR4JhighxSelection   = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && mt>450 && LepAplanarity>0.01 && met/meffInc30>0.25"
configMgr.cutsDict["SR4Jhighx"]=  SR4JhighxSelection + CommonSelection
configMgr.cutsDict["SR4JhighxBV"]=SR4JhighxSelection +"&& nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JhighxBT"]=SR4JhighxSelection +"&& nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["SR4JhighxBTnomet"]="meffInc30>1000. && lep1Pt>35 && nJet30>=4 && nJet30<6 && mt>450 && LepAplanarity>0.01 && met/meffInc30>0.25&& nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR4JhighxBTnomt"]="meffInc30>1000. && lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && LepAplanarity>0.01 && met/meffInc30>0.25&& nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR4JhighxBVnomet"]="meffInc30>1000. && lep1Pt>35 && nJet30>=4 && nJet30<6 && mt>450 && LepAplanarity>0.01 && met/meffInc30>0.25&& nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JhighxBVnomt"]="meffInc30>1000. && lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && LepAplanarity>0.01 && met/meffInc30>0.25&& nBJet30_MV2c10==0" + CommonSelection


CR4JhighxSelection   = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && mt>50 && mt<200 && LepAplanarity<0.01 && met/meffInc30>0.25"
configMgr.cutsDict["TR4Jhighx"]=CR4JhighxSelection +" && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["WR4Jhighx"]=CR4JhighxSelection +" && nBJet30_MV2c10==0" + CommonSelection

#VR proposal preliminary
VR4JhighxaplSelection ="lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>50  && mt<150 && LepAplanarity>0.05 && met/meffInc30>0.25"
VR4JhighxmtSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>200          && LepAplanarity<0.01 && met/meffInc30>0.25"
VR4JhighxhybridSelection  = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>150 && mt<450 && LepAplanarity>0.01 && LepAplanarity<0.05" ## VR4Jhybrid
VR4JhighxnoaplSelection ="lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>50  && mt<150 && met/meffInc30>0.25"
VR4JhighxnomtSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && LepAplanarity<0.01 && met/meffInc30>0.25"
configMgr.cutsDict["VR4Jhighxapl"]=VR4JhighxaplSelection + CommonSelection
configMgr.cutsDict["VR4Jhighxmt"]=VR4JhighxmtSelection + CommonSelection
configMgr.cutsDict["VR4Jhighxhybrid"]=VR4JhighxhybridSelection + CommonSelection ## VR4Jhybrid
configMgr.cutsDict["VR4Jhighxnoapl"]=VR4JhighxnoaplSelection+ CommonSelection
configMgr.cutsDict["VR4Jhighxnomt"]=VR4JhighxnomtSelection+ CommonSelection


# ------- 4J regions for gluino gridx low x--------------------------------------------------------------------------- #
SR4JlowxSelection    = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>150 && mt<450 && LepAplanarity>0.05"
configMgr.cutsDict["SR4Jlowx"]=  SR4JlowxSelection + CommonSelection
configMgr.cutsDict["SR4JlowxBV"]=SR4JlowxSelection +"&& nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JlowxBT"]=SR4JlowxSelection +"&& nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["SR4JlowxBVnomet"]="meffInc30>1300. && lep1Pt>35 && nJet30>=4 && nJet30<6 && mt>150 && mt<450 && LepAplanarity>0.05 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JlowxBVnomt"]="meffInc30>1300. && lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && LepAplanarity>0.05 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR4JlowxBTnomet"]="meffInc30>1300. && lep1Pt>35 && nJet30>=4 && nJet30<6 && mt>150 && mt<450 && LepAplanarity>0.05 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR4JlowxBTnomt"]="meffInc30>1300. && lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && LepAplanarity>0.05 && nBJet30_MV2c10>0" + CommonSelection


CR4JlowxSelection    = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>50  && mt<150 && LepAplanarity>0.01 && LepAplanarity<0.05"
configMgr.cutsDict["TR4Jlowx"]=CR4JlowxSelection +" && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["WR4Jlowx"]=CR4JlowxSelection +" && nBJet30_MV2c10==0" + CommonSelection

#VR proposal preliminary
VR4JlowxaplSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>50  && mt<150 && LepAplanarity>0.05"
VR4JlowxhybridSelection  = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>150 && mt<450 && LepAplanarity>0.01 && LepAplanarity<0.05" ## VR4Jhybrid
VR4JlowxnoaplSelection = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>50  && mt<150"
VR4JlowxnomtSelection  = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && LepAplanarity>0.01 && LepAplanarity<0.05"
configMgr.cutsDict["VR4Jlowxapl"]=VR4JlowxaplSelection+ CommonSelection
configMgr.cutsDict["VR4Jlowxhybrid"]=VR4JlowxhybridSelection+ CommonSelection  ## VR4Jhybrid
configMgr.cutsDict["VR4Jlowxnoapl"]=VR4JlowxnoaplSelection+ CommonSelection
configMgr.cutsDict["VR4Jlowxnomt"]=VR4JlowxnomtSelection+ CommonSelection


# ------- 6J regions --------------------------------------------------------------------------- #
SR6JSelection        = "lep1Pt>35 && nJet30>=6 && met>350. && mt>175.&& LepAplanarity>0.06"
configMgr.cutsDict["SR6J"]= SR6JSelection + CommonSelection
configMgr.cutsDict["SR6JBV"]=SR6JSelection +"&& nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR6JBT"]=SR6JSelection +"&& nBJet30_MV2c10>0" + CommonSelection

configMgr.cutsDict["SR6JBTnomet"]="meffInc30>700. && lep1Pt>35 && nJet30>=6 && mt>175.&& LepAplanarity>0.06 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR6JBTnomt"]="meffInc30>700. && lep1Pt>35 && nJet30>=6 && met>350. && LepAplanarity>0.06 && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["SR6JBVnomet"]="meffInc30>700. && lep1Pt>35 && nJet30>=6 && mt>175.&& LepAplanarity>0.06 && nBJet30_MV2c10==0" + CommonSelection
configMgr.cutsDict["SR6JBVnomt"]="meffInc30>700. && lep1Pt>35 && nJet30>=6 && met>350. && LepAplanarity>0.06 && nBJet30_MV2c10==0" + CommonSelection

CR6JSelection        = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. && mt<175 && LepAplanarity<0.06"
configMgr.cutsDict["TR6J"]=CR6JSelection +" && nBJet30_MV2c10>0" + CommonSelection
configMgr.cutsDict["WR6J"]=CR6JSelection +" && nBJet30_MV2c10==0" + CommonSelection

VR6JaplSelection     = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. && mt<175 && LepAplanarity>0.06"
VR6JmtSelection      = "lep1Pt>35 && nJet30>=6 && met>250. && mt>175. && mt<400 && LepAplanarity<0.06"
VR6JnoaplSelection     = "lep1Pt>35 && nJet30>=6 && met>350. && mt>50. && mt<175"
VR6JnomtSelection      = "lep1Pt>35 && nJet30>=6 && met>250. && LepAplanarity<0.06"
configMgr.cutsDict["VR6Japl"]=VR6JaplSelection+ CommonSelection
configMgr.cutsDict["VR6Jmt"]=VR6JmtSelection+ CommonSelection
configMgr.cutsDict["VR6Jnoapl"]=VR6JnoaplSelection+CommonSelection
configMgr.cutsDict["VR6Jnomt"]=VR6JnomtSelection+ CommonSelection

d=configMgr.cutsDict
#second part: splitting into electron and muon channel for validation purposes	d=configMgr.cutsDict
defined_regions = []
if '2J' in CRregions: 
    defined_regions+=['SR2J','SR2JBV','SR2JBT','SR2JBVnomet','SR2JBVnomt','SR2JBTnomt','SR2JBTnomet','WR2J','TR2J',"VR2Jmet","VR2Jnomet","VR2Jmt","VR2Jnomt"]

if '4Jhighx' in CRregions: 
    defined_regions+=['SR4Jhighx','SR4JhighxBV','SR4JhighxBT','SR4JhighxBVnomet','SR4JhighxBVnomt','SR4JhighxBTnomt','SR4JhighxBTnomet','WR4Jhighx','TR4Jhighx',"VR4Jhighxapl","VR4Jhighxnoapl","VR4Jhighxmt","VR4Jhighxnomt","VR4Jhighxhybrid"]

if '4Jlowx' in CRregions: 
    defined_regions+=['SR4Jlowx','SR4JlowxBV','SR4JlowxBT','SR4JlowxBVnomet','SR4JlowxBVnomt','SR4JlowxBTnomt','SR4JlowxBTnomet','WR4Jlowx','TR4Jlowx',"VR4Jlowxapl","VR4Jlowxnoapl","VR4Jlowxhybrid","VR4Jlowxnomt"]

if '6J' in CRregions: 
    defined_regions+=['SR6J','SR6JBV','SR6JBT','SR6JBVnomet','SR6JBVnomt','SR6JBTnomt','SR6JBTnomet','WR6J','TR6J',"VR6Japl","VR6Jnoapl","VR6Jmt","VR6Jnomt"]



for pre_region in defined_regions:
            configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection
            configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection
            configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection  


# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")

#creating now list of samples
samplelist=[]
if 'Zjets' in mysamples: samplelist.append(ZSample)
if 'ttv' in mysamples: samplelist.append(ttbarVSample)
#if 'singletop' in mysamples: samplelist.append(SingleTopSample)
if 'diboson' in mysamples: samplelist.append(DibosonsSample)
#if 'Wjets' in mysamples: samplelist.append(WSample)
#if 'ttbar' in mysamples: samplelist.append(TTbarSample)
if 'data' in mysamples: samplelist.append(DataSample)

if doOnlySignal: samplelist = [sigSample,DataSample]

#bkgOnly.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
bkgOnly.addSamples(samplelist)
    
if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.032) ## new recommendations for Moriond 2017 from February 2017
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in ['2J','4Jhighx','4Jlowx','6J']:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################


if "2J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["WR2JEM"],4,700,2300),[elmuChans["2J"],bVetoChans["2J"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)
    
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["TR2JEM"],4,700,2300),[elmuChans["2J"],bReqChans["2J"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)	
    

if "4Jhighx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["WR4JhighxEM"],3,1000.,2500.),[elmuChans["4Jhighx"],bVetoChans["4Jhighx"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)	  
      
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["TR4JhighxEM"],3,1000.,2500.),[elmuChans["4Jhighx"],bReqChans["4Jhighx"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)   
    

if "4Jlowx" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["WR4JlowxEM"],3,1300.,2350.),[elmuChans["4Jlowx"],bVetoChans["4Jlowx"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)       
    
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["TR4JlowxEM"],3,1300.,2350.),[elmuChans["4Jlowx"],bReqChans["4Jlowx"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)
    
if "6J" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("meffInc30",["WR6JEM"],4,700,2833),[elmuChans["6J"],bVetoChans["6J"]])
    tmp.useOverflowBin=True
    bkgOnly.addBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("meffInc30",["TR6JEM"],4,700,2833),[elmuChans["6J"],bReqChans["6J"]])
    tmp.useOverflowBin=True    
    bkgOnly.addBkgConstrainChannels(tmp)  

          
            

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]=False
for region in CRregions:
    ValidRegList["OneLep"]  += ValidRegList[region]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"] or VRplots:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
       for region in CRregions:
           appendIfMatchName(c,bReqChans[region])
           appendIfMatchName(c,bVetoChans[region])
           appendIfMatchName(c,bAgnosticChans[region])
           appendIfMatchName(c,elChans[region])
           appendIfMatchName(c,muChans[region])      
           appendIfMatchName(c,elmuChans[region])
	   
   
    if doTableInputs:
        if "2J" in CRregions:
            #appendTo(validation.addValidationChannel("cuts",["SR2JEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JEM"],1,0.5,1.5),[bAgnosticChans["2J"],elmuChans["2J"]])
    
            #appendTo(validation.addValidationChannel("cuts",["SR2JBTEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JBTMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR2JBTEM"],4,700.,2300.),[bReqChans["2J"],elmuChans["2J"]])
            
            #appendTo(validation.addValidationChannel("cuts",["SR2JBVEl"],1,0.5,1.5),[bAgnosticChans["2J"],elChans["2J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR2JBVMu"],1,0.5,1.5),[bAgnosticChans["2J"],muChans["2J"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR2JBVEM"],4,700.,2300.),[bVetoChans["2J"],elmuChans["2J"]])

            if doBlindSRinBGfit:
                validation.getChannel("meffInc30",["SR2JBVEM"]).doBlindingOverwrite = True
                validation.getChannel("meffInc30",["SR2JBTEM"]).doBlindingOverwrite = True
                

            appendTo(validation.addValidationChannel("meffInc30",["VR2JmtEM"],4,700.,2300.),[bAgnosticChans["2J"],elmuChans["2J"]])           
            appendTo(validation.addValidationChannel("meffInc30",["VR2JmetEM"],4,700.,2300.),[bAgnosticChans["2J"],elmuChans["2J"]])

            #appendTo(validation.addValidationChannel("meffInc30",["VR2JnomtEM"],4,700.,2300.),[bAgnosticChans["2J"],elmuChans["2J"]])           
            #appendTo(validation.addValidationChannel("meffInc30",["VR2JnometEM"],4,700.,2300.),[bAgnosticChans["2J"],elmuChans["2J"]])
            
            
        if "4Jhighx" in CRregions:
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxEM"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
    
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBVEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBVMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR4JhighxBVEM"],3,1000.,2500.),[bVetoChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBTEl"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JhighxBTMu"],1,0.5,1.5),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR4JhighxBTEM"],3,1000.,2500.),[bReqChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            
            #tmp = appendTo(validation.addValidationChannel("meffInc30",["WR4JhighxEl"],3,1000.,2500.),[elChans["4Jhighx"],bVetoChans["4Jhighx"]])
            #tmp.useOverflowBin=True
            
            #tmp = appendTo(validation.addValidationChannel("meffInc30",["WR4JhighxMu"],3,1000.,2500.),[muChans["4Jhighx"],bVetoChans["4Jhighx"]])
            #tmp.useOverflowBin=True   
            
            #tmp = appendTo(validation.addValidationChannel("meffInc30",["TR4JhighxEl"],3,1000.,2500.),[elChans["4Jhighx"],bReqChans["4Jhighx"]])
            #tmp.useOverflowBin=True
            
            #tmp = appendTo(validation.addValidationChannel("meffInc30",["TR4JhighxMu"],3,1000.,2500.),[muChans["4Jhighx"],bReqChans["4Jhighx"]])
            #tmp.useOverflowBin=True               

            

            if doBlindSRinBGfit:
                validation.getChannel("meffInc30",["SR4JhighxBVEM"]).doBlindingOverwrite = True
                validation.getChannel("meffInc30",["SR4JhighxBTEM"]).doBlindingOverwrite = True

            appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxmtEM"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])                       
            appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxaplEM"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
            
            #appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxaplEl"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],elChans["4Jhighx"]])
            #appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxaplMu"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],muChans["4Jhighx"]])
            
            appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxhybridEM"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]]) ## VR4Jhybrid                  

            #appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxnomtEM"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])            
            #appendTo(validation.addValidationChannel("meffInc30",["VR4JhighxnoaplEM"],3,1000.,2500.),[bAgnosticChans["4Jhighx"],elmuChans["4Jhighx"]])
      

        if "4Jlowx" in CRregions:
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxEM"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])

            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBVEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBVMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR4JlowxBVEM"],3,1300.,2350.),[bVetoChans["4Jlowx"],elmuChans["4Jlowx"]])

            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBTEl"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],elChans["4Jlowx"]])
            #appendTo(validation.addValidationChannel("cuts",["SR4JlowxBTMu"],1,0.5,1.5),[bAgnosticChans["4Jlowx"],muChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR4JlowxBTEM"],3,1300.,2350.),[bReqChans["4Jlowx"],elmuChans["4Jlowx"]])

            
            if doBlindSRinBGfit:
                validation.getChannel("meffInc30",["SR4JlowxBVEM"]).doBlindingOverwrite = True                
                validation.getChannel("meffInc30",["SR4JlowxBTEM"]).doBlindingOverwrite = True

            appendTo(validation.addValidationChannel("meffInc30",["VR4JlowxhybridEM"],3,1300.,2350.),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])  ## VR4Jhybrid
            appendTo(validation.addValidationChannel("meffInc30",["VR4JlowxaplEM"],3,1300.,2350.),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])            
            
            #appendTo(validation.addValidationChannel("meffInc30",["VR4JlowxnomtEM"],3,1300.,2350.),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])           
            #appendTo(validation.addValidationChannel("meffInc30",["VR4JlowxnoaplEM"],3,1300.,2350.),[bAgnosticChans["4Jlowx"],elmuChans["4Jlowx"]])
            

    
        if "6J" in CRregions:

            #appendTo(validation.addValidationChannel("cuts",["SR6JEl"],1,0.5,1.5),[bAgnosticChans["6J"],elChans["6J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR6JMu"],1,0.5,1.5),[bAgnosticChans["6J"],muChans["6J"]])
            #appendTo(validation.addValidationChannel("cuts",["SR6JEM"],1,0.5,1.5),[bAgnosticChans["6J"],elmuChans["6J"]])
    
            appendTo(validation.addValidationChannel("meffInc30",["SR6JBVEM"],4,700.,2833.),[bVetoChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("meffInc30",["SR6JBTEM"],4,700.,2833.),[bReqChans["6J"],elmuChans["6J"]])
	    
            if doBlindSRinBGfit:
                validation.getChannel("meffInc30",["SR6JBVEM"]).doBlindingOverwrite = True               
                validation.getChannel("meffInc30",["SR6JBTEM"]).doBlindingOverwrite = True

	   
	    
            appendTo(validation.addValidationChannel("meffInc30",["VR6JmtEM"],4,700.,2833.),[bAgnosticChans["6J"],elmuChans["6J"]])    
            appendTo(validation.addValidationChannel("meffInc30",["VR6JaplEM"],4,700.,2833.),[bAgnosticChans["6J"],elmuChans["6J"]])
            
	    
            #appendTo(validation.addValidationChannel("meffInc30",["VR6JnomtEM"],4,700.,2833.),[bAgnosticChans["6J"],elmuChans["6J"]])
            #appendTo(validation.addValidationChannel("meffInc30",["VR6JnoaplEM"],4,700.,2833.),[bAgnosticChans["6J"],elmuChans["6J"]])
            
	    
	    

        # All validation regions should use over-flow bins!
        for v in validation.channels : 
            if not 'cuts' in v.name: v.useOverflowBin=True

    #at this point we can add further validation regions, e.g. to plot various distributions in the VRs       
    if VRplots:
        if "2J" in CRregions:
            appendTo(validation.addValidationChannel("met",["SR2JBTnometEM"],10,350.,700.),[bReqChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("met",["SR2JBVnometEM"],10,350.,700.),[bVetoChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("mt",["SR2JBTnomtEM"],13,75.,205.),[bReqChans["2J"],elmuChans["2J"]])
            appendTo(validation.addValidationChannel("mt",["SR2JBVnomtEM"],13,75.,205.),[bVetoChans["2J"],elmuChans["2J"]])
        if "4Jhighx" in CRregions:
            appendTo(validation.addValidationChannel("met",["SR4JhighxBTnometEM"],11,240.,570.),[bReqChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("met",["SR4JhighxBVnometEM"],11,240.,570.),[bVetoChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("mt",["SR4JhighxBTnomtEM"],10,400.,600.),[bReqChans["4Jhighx"],elmuChans["4Jhighx"]])
            appendTo(validation.addValidationChannel("mt",["SR4JhighxBVnomtEM"],10,400.,600.),[bVetoChans["4Jhighx"],elmuChans["4Jhighx"]])            
        if "4Jlowx" in CRregions:
            appendTo(validation.addValidationChannel("met",["SR4JlowxBTnometEM"],11,250.,500.),[bReqChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("met",["SR4JlowxBVnometEM"],11,250.,500.),[bVetoChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("mt",["SR4JlowxBTnomtEM"],10,50.,550.),[bReqChans["4Jlowx"],elmuChans["4Jlowx"]])
            appendTo(validation.addValidationChannel("mt",["SR4JlowxBVnomtEM"],10,50.,550.),[bVetoChans["4Jlowx"],elmuChans["4Jlowx"]])       
        if "6J" in CRregions:
            appendTo(validation.addValidationChannel("met",["SR6JBTnometEM"],10,300.,600.),[bReqChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("met",["SR6JBVnometEM"],10,300.,600.),[bVetoChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("mt",["SR6JBTnomtEM"],11,120.,450.),[bReqChans["6J"],elmuChans["6J"]])
            appendTo(validation.addValidationChannel("mt",["SR6JBVnomtEM"],11,120.,450.),[bVetoChans["6J"],elmuChans["6J"]])       
            
        # All validation regions should use over-flow bins!
        for v in validation.channels : 
            if not 'cuts' in v.name: v.useOverflowBin=True

# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}
    SRs=[]
    if '2J' in CRregions:
        SRs+=["SR2JBVEM","SR2JBTEM"]  
    if '4Jlowx' in CRregions:
        SRs+=["SR4JlowxBVEM","SR4JlowxBTEM"]  
    if '4Jhighx' in CRregions:
        SRs+=["SR4JhighxBVEM","SR4JhighxBTEM"]  
    if '6J' in CRregions:
        SRs+=["SR6JBVEM","SR6JBTEM"]          

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
                
        #signal-specific uncertainties  
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(xsecSig)


        if not doHistoBuilding:
            myTopLvl.addSamples(sigSample)
            myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if not doShapeFit:
                ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
            else:
                if sr=='SR2JEM' or sr=='SR2JBTEM'  or sr=='SR2JBVEM' :
                    ch = myTopLvl.addChannel("meffInc30",[sr],4,700,2300)
                    ch.useOverflowBin=True     
                    ERRregion='SR2J'
                elif sr=='SR4JhighxEM' or sr=='SR4JhighxBTEM' or sr=='SR4JhighxBVEM':
                    ch = myTopLvl.addChannel("meffInc30",[sr],3,1000.,2500.)
                    ch.useOverflowBin=True
                    ERRregion='SR4Jhx'
                elif sr=='"SR4JlowxEM"' or sr=='SR4JlowxBTEM' or sr=='SR4JlowxBVEM':
                    ch = myTopLvl.addChannel("meffInc30",[sr],3,1300.,2350.)
                    ch.useOverflowBin=True
                    ERRregion='SR4Jlx'
                elif sr=='SR6JEM' or sr=='SR6JBVEM' or sr=='SR6JBTEM':
                    ch = myTopLvl.addChannel("meffInc30",[sr],4,700,2833)   
                    ch.useOverflowBin=True
                    ERRregion='SR6J'
                else: 
                    raise RuntimeError("This region is not yet implemented in a shape fit mode: %s"%sr)
                
            for region in CRregions:
                if region in sr:
                    if 'El' in sr:                
                        elChans[region].append(ch) 
                    elif 'Mu' in sr:
                        muChans[region].append(ch)
                    elif 'EM' in sr:
                        elmuChans[region].append(ch)
                    else: raise RuntimeError("Unexpected signal region %s"%sr)
                    if 'BV' in sr:
                        bVetoChans[region].append(ch)
                    elif 'BT' in sr:
                        bReqChans[region].append(ch)
                    else:
                        bAgnosticChans[region].append(ch)
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            ######### Signal theory uncertainties ###########                                               
            if not doHistoBuilding and 'oneStep' in sig:                                               
                 errsig = Winter1617.SignalAccUncertainties.GetUncertaintyFromName(ERRregion,sig)
                 print "sig=",sig,"  region=",ERRregion,"errsig=",errsig
                 theoSig = Systematic("SigTheo",configMgr.weights,1.00+errsig,1.00-errsig,"user","userOverallSys")
                 ch.getSample(sig).addSystematic(theoSig) 
            #################################################                                               
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)
        

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]
    
    
#adding W+jets, ttbar and single top samples for the correct channels
for region in CRregions:
    for chan in AllChannels[region]:
        if 'Wjets' in mysamples:
            chan.addSample(WSample[region]["bin1"])
            chan.addSample(WSample[region]["bin2"])
            chan.addSample(WSample[region]["bin3"])
            if region == '2J' or region == '6J': chan.addSample(WSample[region]["bin4"])
        if 'ttbar' in mysamples:
            chan.addSample(TTbarSample[region]["bin1"])
            chan.addSample(TTbarSample[region]["bin2"])
            chan.addSample(TTbarSample[region]["bin3"])
            if region == '2J' or region == '6J': chan.addSample(TTbarSample[region]["bin4"])
        if 'singletop' in mysamples:
            chan.addSample(SingleTopSample[region]["bin1"])
            chan.addSample(SingleTopSample[region]["bin2"])
            chan.addSample(SingleTopSample[region]["bin3"])
            if region == '2J' or region == '6J': chan.addSample(SingleTopSample[region]["bin4"])

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan
        
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
	#print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
	    #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
        SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
        SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion and not doHistoBuilding:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
    
            #if "SR2J" in chan.name:	    
                #theSample.removeWeight("pileupWeight")
                #theSample.removeSystematic("pileup")

             
            sys_region = ""
            if "2J" in chan.name: sys_region = "2J"
            elif "4Jhighx" in chan.name: sys_region = "4Jhighx"
            elif "4Jlowx" in chan.name: sys_region = "4Jlowx"
            elif "6J" in chan.name: sys_region = "6J"
                    
            else: 
                print "Unknown region! - Take systematics from 6J regions."
                sys_region = "6J"

            # replacement of JES PunchThrough systematic for AF2 signal samples (not for FullSim signal samples)
            if not debug and not (sig in FullSimSig):
                print "This is an AFII signal sample -> removing JES_PunchThrough_MC15, adding JES_PunchThrough_AFII and JES_RelativeNonClosure_AFII systematics"
                theSample.removeSystematic("JES_PunchThrough_MC15")
                theSample.addSystematic(Systematic("JES_PunchThrough_AFII","_NoSys","_JET_PunchThrough_AFII__1up","_JET_PunchThrough_AFII__1down","tree","overallNormHistoSys"))
                theSample.addSystematic(Systematic("JES_RelativeNonClosure_AFII","_NoSys","_JET_RelativeNonClosure_AFII__1up","_JET_RelativeNonClosure_AFII__1down","tree","overallNormHistoSys"))            

            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans_all:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all:
                theSigFiles = sigFiles_em[sig]	                  
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels


for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])	    
        if 'Wjets' in mysamples:	       
            chan.getSample("Wjets_"+region+"_bin1").addSystematic(WjetsCommbin1[region])
            chan.getSample("Wjets_"+region+"_bin2").addSystematic(WjetsCommbin2[region])
            chan.getSample("Wjets_"+region+"_bin3").addSystematic(WjetsCommbin3[region])
            if region == '2J' or region == '6J':chan.getSample("Wjets_"+region+"_bin4").addSystematic(WjetsCommbin4[region])

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")
       
    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            #if 'diboson' in mysamples: chan.getSample("diboson_Sherpa221").addSystematic(DBComm[region])
            #if 'Zjets' in mysamples: chan.getSample("zjets_Sherpa221").addSystematic(ZjetsComm[region])           
           
            if useNJetNormFac:
                		    
                    if 'ttbar' in mysamples: chan.getSample("ttbar").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
		
            else:
                print 'one NF'
                if 'Wjets' in mysamples: 
                    #print chan.name                    
                    chan.getSample("Wjets_"+region+"_bin1").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    chan.getSample("Wjets_"+region+"_bin2").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    chan.getSample("Wjets_"+region+"_bin3").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    if region == '2J' or region == '6J': chan.getSample("Wjets_"+region+"_bin4").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                if 'ttbar' in mysamples:                   
                    chan.getSample("ttbar_"+region+"_bin1").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    chan.getSample("ttbar_"+region+"_bin2").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    chan.getSample("ttbar_"+region+"_bin3").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    if region == '2J' or region == '6J': chan.getSample("ttbar_"+region+"_bin4").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])		    
                if 'singletop' in mysamples:                  
                    chan.getSample("singletop_"+region+"_bin1").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    chan.getSample("singletop_"+region+"_bin2").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    chan.getSample("singletop_"+region+"_bin3").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])
                    if region == '2J' or region == '6J': chan.getSample("singletop_"+region+"_bin4").setNormRegions([("WR"+region+"EM","meffInc30"),("TR"+region+"EM","meffInc30")])     
                    
                    
    #adding extra uncertainty for aplanarity mismodelling
    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            if ('SR' in chan.name or 'apl' in chan.name or 'hybrid' in chan.name) and not '2J' in chan.name:
                variation='0.'
                if 'SR6J' in chan.name or 'VR6Japl' in chan.name: variation='0.11'
                elif 'SR4Jhighx' in chan.name or ('VR4J' in chan.name and 'hybrid' in chan.name): variation='0.04'
                elif ('VR4J' in chan.name and 'apl' in chan.name) or 'SR4Jlowx' in chan.name: variation='0.1'
                if 'Wjets' in mysamples:	       
                    chan.getSample("Wjets_"+region+"_bin1").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    chan.getSample("Wjets_"+region+"_bin2").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    chan.getSample("Wjets_"+region+"_bin3").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    if region == '6J':chan.getSample("Wjets_"+region+"_bin4").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                if 'ttbar' in mysamples:	       
                    chan.getSample("ttbar_"+region+"_bin1").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    chan.getSample("ttbar_"+region+"_bin2").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    chan.getSample("ttbar_"+region+"_bin3").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    if region == '6J':chan.getSample("ttbar_"+region+"_bin4").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))   
                if 'singletop' in mysamples:	       
                    chan.getSample("singletop_"+region+"_bin1").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    chan.getSample("singletop_"+region+"_bin2").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    chan.getSample("singletop_"+region+"_bin3").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))
                    if region == '6J':chan.getSample("singletop_"+region+"_bin4").addSystematic(Systematic("aplmis"+region, configMgr.weights,configMgr.weights+["( 1+"+variation+")"],configMgr.weights+["( 1-"+variation+")"], "weight","overallSys"))                       
                                
                
#####################################################
	# Add separate Normalization Factors for ttbar and W+jets for each CR                               #
######################################################
if useNJetNormFac:
   
    for chan in AllChannels_all:
        mu_W_Xj = "mu_W_XJ"
        mu_Top_Xj = "mu_Top_XJ"
        ### doldol
        if "2J" in chan.name:            
                mu_W_Xj = "mu_W_2J"
                mu_Top_Xj = "mu_Top_2J"
        elif "4Jhighx" in chan.name:            
                mu_W_Xj = "mu_W_4Jhighx"
                mu_Top_Xj = "mu_Top_4Jhighx"
        elif "4Jlowx" in chan.name:            
                mu_W_Xj = "mu_W_4Jlowx"
                mu_Top_Xj = "mu_Top_4Jlowx"
        elif "6J" in chan.name:            
                mu_W_Xj = "mu_W_6J"
                mu_Top_Xj = "mu_Top_6J"
        
        else:
            log.warning("Channel %s gets no separated normalization factor" % chan.name)
            
        if 'Wjets' in mysamples: chan.getSample(WSampleName).addNormFactor(mu_W_Xj,1.,4.,0.)
        if 'ttbar' in mysamples: chan.getSample(TTbarSampleName).addNormFactor(mu_Top_Xj,1.,4.,0.)
        if 'singletop' in mysamples: chan.getSample(SingleTopSampleName).addNormFactor(mu_Top_Xj,1.,4.,0.1)
        log.info("Adding additional normalization factors (%s, %s) to channel (%s)" %(mu_W_Xj, mu_Top_Xj, chan.name))

    meas.addParamSetting("mu_W","const",1.0)
    meas.addParamSetting("mu_Top","const",1.0)
    
    if validation:
        meas_valid  = validation.getMeasurement("BasicMeasurement")
        meas_valid.addParamSetting("mu_W","const",1.0)
        meas_valid.addParamSetting("mu_Top","const",1.0)
        #meas.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)

       
    if myFitType==FitType.Exclusion:
        for sig in sigSamples:
            meas_excl=configMgr.getFitConfig("Sig_%s"%sig).getMeasurement("BasicMeasurement")
            meas_excl.addParamSetting("mu_W","const",1.0)
            meas_excl.addParamSetting("mu_Top","const",1.0)
     
        


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(kGreen-9)
entry.SetFillColor(kGreen-9)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(kAzure-4)
entry.SetFillColor(kAzure-4)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(kGreen-5)
entry.SetFillColor(kGreen-5)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()
MeffBins = [ '1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    chan.titleX="m^{incl}_{eff} [GeV]"
    
    if "TR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0.2 
        chan.maxY = 40000.

    elif "WR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0.2
        chan.maxY = 40000.       
    elif "TR" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0.2 
        chan.maxY = 3000.

    elif "WR" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0.2
        chan.maxY = 3000.	
    elif "VR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0.2
        chan.maxY = 5000.	
    elif "VR" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0.2
        chan.maxY = 1000.

    elif "SR6J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 18.5
    elif "SR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 25.5

    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True

configMgr.fitConfigs.remove(bkgOnly)
            
print 'End of config file'
