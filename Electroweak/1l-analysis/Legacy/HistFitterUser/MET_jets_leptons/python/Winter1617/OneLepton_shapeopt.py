################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch


from logger import Logger
log = Logger('OneLepton')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,bgdFiles,systList):
    for chan in channels:
        chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onWestGrid='westgrid' in commands.getstatusoutput("hostname")[1] or fnmatch.fnmatch(commands.getstatusoutput("hostname")[1], 'u*')
onUBCAtlasServ='atlasserv' in commands.getstatusoutput("hostname")[1]
onMOGON="mogon" in commands.getstatusoutput("hostname")[1]
onDOL=False
if onDOL: onLxplus=True ## previous status does not detect the batch system in lxplus

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]
#SystList.append("JER")      # Jet Energy Resolution (common)
#SystList.append("JES")      # Jet Energy Scale (common)
#SystList.append("MET")      # MET (common)
#SystList.append("LEP")      # lepton uncertainties (common)
#SystList.append("LepEff")   # lepton scale factors (common)
#SystList.append("JVT")      # JVT scale factors
#SystList.append("pileup")   # pileup (common)
#SystList.append("BTag")     # b-tagging uncertainties
#SystList.append("Dibosons") # scale variation for renormalization, factorization, resummation and generator comparison
#SystList.append("Wjets")    # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
#SystList.append("Ttbar")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
#SystList.append("ttbarPDF") # 
#SystList.append("wjetsPDF") # 
#SystList.append("Zjets")    # scale variation for renormalization, factorization, resummation,matching


CRregions = ["2J"] #this is the default - modify from command line


# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r 2J,4J,6J option)"
            
# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    sigSamples=["SS_oneStep_825_425_25"]
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
    
if myFitType==FitType.Exclusion:
    if 'GG_oneStep' in sigSamples[0]:
        if onDOL: analysissuffix += '_'+ sigSamples[0]
        else:   analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]

mylumi= 14.78446 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "OneLepton"+analysissuffix#+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
useBackupCache = False
if (onMOGON or onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = True #!!!
if onDOL:
    #useBackupCache = False ##  creating cache 
    useBackupCache = True  ##  running w/ cache
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "OneLepton"+analysissuffix_BackupCache#+analysisextension
    histBackupCacheFileName = "data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

configMgr.fixSigXSec=True
useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = False 

#blinding of various regions
configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = True # Blind the CRs (default is False)
configMgr.blindVR = True # Blind the VRs (default is False)
doBlindSRinBGfit  = True # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #
'''
#directory with background files:
inputDir="/data/vtudorac/Tag0602/"
inputDirData="/data/vtudorac/Tag0602/"
#signal files
inputDirSig="/data/vtudorac/Tag0602/"
'''

inputDir="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
inputDirSig="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"
inputDirData="/afs/cern.ch/work/m/mgignac/public/StrongProduction/MC15C/T_06_03/merged/"


if not onLxplus:
    inputDir="/project/etp2/jlorenz/HistFitterTrees/06_03/"
    inputDirSig="/project/etp2/jlorenz/HistFitterTrees/06_03/"
    inputDirData="/project/etp2/jlorenz/HistFitterTrees/06_03/"

if onWestGrid :
    inputDir="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirSig="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"
    inputDirData="/home/mgignac/data/downloads/MC15C/T_06_03/merged/"

if onUBCAtlasServ :
    inputDir="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirSig="/mnt/xrootdd/mgignac/MC15C/T_06_03/"
    inputDirData="/mnt/xrootdd/mgignac/MC15C/T_06_03/"

if onMOGON:
    inputDir="../InputTrees/"
    inputDirSig="../InputTrees/"
    inputDirData="../InputTrees/"

#background files, separately for electron and muon channel:
bgdFiles_e = [inputDir+"allBkgs_T_06_03.root"]
bgdFiles_m = [inputDir+"allBkgs_T_06_03.root"]
bgdFiles_em = [inputDir+"allBkgs_T_06_03.root"]


#signal files
if myFitType==FitType.Exclusion:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"allSignal_T_06_03.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"allSignal_T_06_03.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"allSignal_T_06_03.root"]
        
#data files
#dataFiles = [inputDirData+"allTrees_T_06_03_dataOnly.root"] # 949.592 /pb
#dataFiles = [inputDirData+"allDS2_T_06_03.root"]    # DS2: 13277.66 /pb (remember to check lumiErr: should be 2.9%)
#dataFiles = [inputDirData+"allDS2v01_T_06_03.root"] # DS2.1: 14784.46 /pb (remember to check lumiErr: should be 3.0%)

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","jvtWeight","SherpaVjetsNjetsWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights

#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}


for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 20% syst and an overallSys
    basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.35,0.65,"user","userOverallSys"))
    

# ********************************************************************* #
#                              Background samples

# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets_Sherpa22"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)

#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)

#
DibosonsSampleName = "diboson"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
#DibosonsSample.setNormByTheory()

#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setNormByTheory()
#
ZSampleName = "zjets_Sherpa22"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()

#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)

#ttbarVSample.setNormByTheory()
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
#DataSample.setFileList(dataFiles)
DataSample.setData()


# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&  nLep_base==1&&nLep_signal==1&&combinedTrigResult  "## TSTcleaning here
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>7) "
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>6)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
TwoLepSelection = "&&  nLep_base>=2 && nLep_signal>=2 && combinedTrigResult"

# ------- 2J region
configMgr.cutsDict["SR2J"]="lep1Pt<35 && nJet30>=2 && jet1Pt>200.  && met>460. && mt>100. && (met/meffInc30) > 0.35" + CommonSelection

# ------- 4J region
configMgr.cutsDict["SR4J"]="nJet30>=4 && lep1Pt>35 && jet1Pt>400 && jet4Pt>30 && jet4Pt<100 && meffInc30>1600 && met>250 && mt>475 && met/meffInc30>0.3"+ CommonSelection 

# ------- 6J region
configMgr.cutsDict["SR6J"]="lep1Pt>35 && nJet30>=6 && jet1Pt>125. && jet6Pt>30.  && met>250. && mt>225. && meffInc30>1000. && JetAplanarity>0.04 && (met/meffInc30) > 0.2" + CommonSelection

d=configMgr.cutsDict
#second part: splitting into electron and muon channel for validation purposes	d=configMgr.cutsDict
defined_regions = []
if '2J' in CRregions: 
    defined_regions+=['SR2J']
if '4J' in CRregions: 
    defined_regions+=['SR4J']
if '6J' in CRregions: 
    defined_regions+=['SR6J']

for pre_region in defined_regions:
            configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection
            configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection
            configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection  


#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in ['2J','4J','6J']:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #


if myFitType==FitType.Exclusion:     
    SR_channels = {}           
    SRs=[]
    if '2J' in CRregions:
        SRs+=["SR2JEM"]
    if '4J' in CRregions:
        SRs+=["SR4JEM"]
    elif '6J' in CRregions:
        SRs+=["SR6JEM"] 
    

    for sig in sigSamples:
        SR_channels[sig] = []

        myTopLvl = configMgr.addFitConfig("Sig_%s"%sig)
        myTopLvl.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
    
        if useStat:
            myTopLvl.statErrThreshold=0.001
        else:
            myTopLvl.statErrThreshold=None

        #Add Measurement
        meas=myTopLvl.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
        meas.addPOI("mu_SIG")
        meas.addParamSetting("Lumi","const",1.0)

        #myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
                
        #signal-specific uncertainties  
        sigSample.setStatConfig(useStat)                                     

        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        #ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
        for sr in SRs:
            if sr=="SR4JEM":
                ch = myTopLvl.addChannel("mt",[sr],6,125,425)
                ch.useOverflowBin=True      
            elif sr=="SR6JEM":
                ch = myTopLvl.addChannel("meffInc30",[sr],4,1200,2200)
                ch.useOverflowBin=True  
            elif 'SR2J' in sr:
                ch = myTopLvl.addChannel("lep1Pt",[sr],4,7.,107.)
                ch.useOverflowBin=True  
            else: 
                 raise RuntimeError("This region is not yet implemented in a shape fit mode: %s"%sr)

            for region in CRregions:
                if region in sr:
                    if 'El' in sr:                
                        elChans[region].append(ch) 
                    elif 'Mu' in sr:
                        muChans[region].append(ch)
                    elif 'EM' in sr:
                        elmuChans[region].append(ch)
                    else: raise RuntimeError("Unexpected signal region %s"%sr)
                    bAgnosticChans[region].append(ch)
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            #ch.useOverflowBin=True 
            #bAgnosticChans.append(ch)
            SR_channels[sig].append(ch)
        

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]
   
for region in CRregions:
    SetupChannels(elChans[region],bgdFiles_e, basicChanSyst[region])
    SetupChannels(muChans[region],bgdFiles_m, basicChanSyst[region])
    SetupChannels(elmuChans[region],bgdFiles_em, basicChanSyst[region])
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
	    
            #if "SR2J" in chan.name:	    
                #theSample.removeWeight("pileupWeight")
                #theSample.removeSystematic("pileup")
             
            sys_region = ""
            if "2J" in chan.name: sys_region = "2J"
            elif "4J" in chan.name: sys_region = "4J"
            elif "6J" in chan.name: sys_region = "6J"
         
            else: 
                print "Unknown region! - Take systematics from 6J regions."
                sys_region = "6J"
                
            theSigFiles=[]
            if chan in elChans_all:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all:
                theSigFiles = sigFiles_em[sig]	                  
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)

# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(1)#bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(TTbarSample.color)
entry.SetFillColor(TTbarSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(WSample.color)
entry.SetFillColor(WSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(SingleTopSample.color)
entry.SetFillColor(SingleTopSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
#bkgOnly.tLegend = leg
#if validation :
#    validation.totalPdfColor = kBlack
#    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
#    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()

# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    
    if "SR3J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 70.
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR6JGGx12HM" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 18.5
        chan.titleY = "Events / 200 GeV"
        chan.titleX = "m^{incl}_{eff} [GeV]"
    elif "SR2J" in chan.name:
        chan.ATLASLabelX = 0.2
        chan.ATLASLabelY = 0.87
        chan.minY = 0. 
        chan.maxY = 25.5
        chan.titleY = "Events / 100 GeV"
        chan.titleX = "p_{T} [GeV]"      
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True
