## Access signal acceptance uncertainties
## as a function of gluino, chargino and LSP mass
## The numbers are the result of a sim in quadrature of various uncertainty components summarised at


import ROOT
import math
import os

infile = ROOT.TFile("/project/etp3/jlorenz/shape_fit/HistFitter_January2017/HistFitterUser/MET_jets_leptons/python/Winter1617/SignalAcc_unc_2017.root")
#infile = ROOT.TFile.Open(os.path.expandvars("$HFU/MET_jets_leptons/python/Winter1617/SignalAcc_unc_2017.root"))

knownregions=['SR2J','SR4Jhx','SR4Jlx','SR6J','SR9J']



def GetSusyParsFromName(name):
    '''
    in case of gtt the second parameter corresponds to mstop
    '''
    tokens = name.split('_')
    if 'GG' in name: mparticle='GG'
    if 'SS' in name: mparticle='SS'

    if 'oneStep' in name:
        mgluino=float(tokens[2])
        mchargino=float(tokens[3])
        mlsp=float(tokens[4])
    elif '2step' in name:
        mgluino=float(tokens[2])
        mchargino=-1
        mlsp=float(tokens[5])
    else:
        print "unknown signalsample name"
    x = (mchargino-mlsp)/(mgluino-mlsp)

    return (mparticle,mgluino,mchargino,mlsp,x)


def distance( p0, p1 ):
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

def GetUncertaintyFromName(signalregion, massstring):
    mpar,mgl,mch,mlsp,x = GetSusyParsFromName(massstring)
    return GetUncertainty(signalregion, mpar, mgl, mch, mlsp  )

def GetUncertainty(signalregion, mparticle, mgluino, mchargino, mlsp):
    grid = 'x12'
    x = float((mchargino-mlsp))/(mgluino-mlsp)
    if mlsp==60 and ((mchargino-mlsp)/(mgluino-mlsp) != 0.5):
        grid = 'gridx'
    if mchargino < 0:
        # sorry for this hack
        grid = '2stepWZ'

    if signalregion not in knownregions:
        print "{0} not known. Please choose among:".format(signalregion)
        print knownregions
        return 0.

    if grid == 'x12' or grid == 'gridx' or grid == '2stepWZ': #interpolation_SR2J_GGx12
        interpolation = infile.Get("interpolation_{0}_{1}{2}".format(signalregion,mparticle,grid))
        if grid == 'x12' or grid == '2stepWZ':
            binnr = interpolation.FindBin(mgluino, mlsp)
        elif grid == 'gridx':
            binnr = interpolation.FindBin(mgluino, x)

        var = interpolation.GetBinContent(binnr)

        if var!= 0:
            print "Returning unc. for grid: {0}".format(var)
            return var

        ## Requested variation outside the interpolation area.
        ## Find the closest non 0 bin
        if grid == 'x12':
            print "WARNING: Requested point {0}:{1}:{2} lies outside the evaluated phase-space area. Returning closest point inside".format(mparticle, mgluino, mlsp)
        elif grid == 'gridx':
            print "WARNING: Requested point {0}:{1}:{2} lies outside the evaluated phase-space area. Returning closest point inside".format(mparticle, mgluino, x)

        minds=999999.
        for bx in range(1, interpolation.GetNbinsX()+1):
            for by in range(1, interpolation.GetNbinsY()+1):

                x = interpolation.GetXaxis().GetBinCenter(bx)
                y = interpolation.GetYaxis().GetBinCenter(by)
                binnr = interpolation.FindBin(x, y)
                tmpvar = interpolation.GetBinContent(binnr)
                if tmpvar==0: continue
                
                if grid == 'x12' or grid == '2stepWZ':
                    tmpd = distance( (x,y), (mgluino,mlsp) )
                elif grid == 'gridx':
                    tmpd = distance( (x,y), (mgluino, x) )

                if tmpd< minds:
                    minds = tmpd
                    var = tmpvar
        print "Closest point is {0} away in mgl-mlsp space and found variation is {1}".format(minds, var)
        return var
