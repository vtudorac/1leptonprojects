import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
'''
uncert in TF
'''


DibosonSystematics={}


Regions = [ 'BVEM', 'BTEM' ]

DibosonSystematics={}

DibosonSystematics['DibosonsGenTheo_WR2JEM']= Systematic("DibosonsGenTheo_R2JEM", configMgr.weights,[(1+0.24),(1+0.47),(1+0.69),(1+0.91)],[(1-0.24),(1-0.47),(1-0.69),(1-0.91)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_WR2JEM']=  Systematic("DibosonsResummationTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.07),(1+0.09),(1+0.09)],[(1-0.01),(1-0.07),(1-0.09),(1-0.09)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_WR2JEM'] = Systematic("DibosonsRenormTheo_R2JEM",configMgr.weights,[(1+0.21),(1+0.07),(1+0.32),(1+0.03)],[(1-0.21),(1-0.07),(1-0.32),(1-0.03)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_WR2JEM'] = Systematic("DibosonsFacTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.07),(1+0.01),(1+0.47)],[(1-0.01),(1-0.07),(1-0.01),(1-0.47)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_TR2JEM'] = Systematic("DibosonsGenTheo_R2JEM", configMgr.weights,[(1+0.34),(1+0.60),(1+1.08),(1+0.66)],[(1-0.34),(1-0.60),(1-1.08),(1-0.66)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_TR2JEM'] = Systematic("DibosonsResummationTheo_R2JEM",configMgr.weights,[(1+0.04),(1+0.14),(1+0.37),(1+0.34)],[(1-0.04),(1-0.14),(1-0.37),(1-0.34)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_TR2JEM'] = Systematic("DibosonsRenormTheo_R2JEM",configMgr.weights,[(1+0.51),(1+0.43),(1+0.10),(1+0.87)],[(1-0.51),(1-0.43),(1-0.10),(1-0.87)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_TR2JEM'] = Systematic("DibosonsFacTheo_R2JEM",configMgr.weights,[(1+0.03),(1+0.01),(1+0.38),(1+0.60)],[(1-0.03),(1-0.01),(1-0.38),(1-0.60)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_VR2JmtEM']= Systematic("DibosonsGenTheo_R2JEM", configMgr.weights,[(1+0.16),(1+0.52),(1+0.71),(1+0.77)],[(1-0.16),(1-0.52),(1-0.71),(1-0.77)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR2JmtEM']=  Systematic("DibosonsResummationTheo_R2JEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.11),(1+0.14)],[(1-0.03),(1-0.05),(1-0.11),(1-0.14)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR2JmtEM'] = Systematic("DibosonsRenormTheo_R2JEM",configMgr.weights,[(1+0.43),(1+0.39),(1+0.11),(1+0.20)],[(1-0.43),(1-0.39),(1-0.11),(1-0.20)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR2JmtEM'] = Systematic("DibosonsFacTheo_R2JEM",configMgr.weights,[(1+0.04),(1+0.02),(1+0.10),(1+0.20)],[(1-0.04),(1-0.02),(1-0.10),(1-0.20)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_VR2JmetEM'] = Systematic("DibosonsGenTheo_R2JEM", configMgr.weights,[(1+0.18),(1+0.19),(1+1.27),(1+0.44)],[(1-0.18),(1-0.19),(1-1.27),(1-0.44)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR2JmetEM'] = Systematic("DibosonsResummationTheo_R2JEM",configMgr.weights,[(1+0.14),(1+0.05),(1+0.11),(1+0.04)],[(1-0.14),(1-0.05),(1-0.11),(1-0.04)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR2JmetEM'] = Systematic("DibosonsRenormTheo_R2JEM",configMgr.weights,[(1+0.38),(1+0.20),(1+0.37),(1+0.29)],[(1-0.38),(1-0.20),(1-0.37),(1-0.29)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR2JmetEM'] = Systematic("DibosonsFacTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.12),(1+0.02),(1+0.03)],[(1-0.01),(1-0.12),(1-0.02),(1-0.03)], "user","userHistoSys")


DibosonSystematics['DibosonsGenTheo_WR4JlowxEM']= Systematic("DibosonsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.64),(1+0.22),(1+0.21)],[(1-0.64),(1-0.22),(1-0.21)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_WR4JlowxEM']=  Systematic("DibosonsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.09)],[(1-0.03),(1-0.05),(1-0.09)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_WR4JlowxEM'] = Systematic("DibosonsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.26),(1+0.20),(1+0.04)],[(1-0.26),(1-0.20),(1-0.04)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_WR4JlowxEM'] = Systematic("DibosonsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.60),(1+0.01),(1+0.14)],[(1-0.60),(1-0.01),(1-0.14)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_TR4JlowxEM'] = Systematic("DibosonsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.30),(1+0.85),(1+1.06)],[(1-0.30),(1-0.85),(1-1.06)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_TR4JlowxEM'] = Systematic("DibosonsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.06),(1+0.38),(1+0.01)],[(1-0.06),(1-0.38),(1-0.01)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_TR4JlowxEM'] = Systematic("DibosonsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.19),(1+0.18),(1+0.55)],[(1-0.19),(1-0.18),(1-0.55)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_TR4JlowxEM'] = Systematic("DibosonsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.09),(1+0.04),(1+0.35)],[(1-0.09),(1-0.04),(1-0.35)], "user","userHistoSys")


DibosonSystematics['DibosonsGenTheo_VR4JlowxaplEM'] = Systematic("DibosonsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.28),(1+0.36),(1+0.37)],[(1-0.28),(1-0.36),(1-0.37)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR4JlowxaplEM'] = Systematic("DibosonsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.21),(1+0.01)],[(1-0.03),(1-0.21),(1-0.01)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR4JlowxaplEM'] = Systematic("DibosonsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.29),(1+0.45),(1+0.75)],[(1-0.29),(1-0.45),(1-0.75)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR4JlowxaplEM'] = Systematic("DibosonsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.01),(1+0.22),(1+0.06)],[(1-0.01),(1-0.22),(1-0.06)], "user","userHistoSys")


DibosonSystematics['DibosonsGenTheo_VR4JlowxhybridEM']= Systematic("DibosonsGenTheo_R4JlowxEM",configMgr.weights,[(1+0.19),(1+0.45),(1+0.59)],[(1-0.19),(1-0.45),(1-0.59)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR4JlowxaplEM'] = Systematic("DibosonsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.21),(1+0.01)],[(1-0.03),(1-0.21),(1-0.01)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR4JlowxaplEM'] = Systematic("DibosonsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.29),(1+0.45),(1+0.75)],[(1-0.29),(1-0.45),(1-0.75)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR4JlowxaplEM'] = Systematic("DibosonsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.01),(1+0.22),(1+0.06)],[(1-0.01),(1-0.22),(1-0.06)], "user","userHistoSys")



DibosonSystematics['DibosonsGenTheo_WR4JhighxEM']= Systematic("DibosonsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.07),(1+0.14),(1+0.21)],[(1-0.07),(1-0.14),(1-0.21)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_WR4JhighxEM']=  Systematic("DibosonsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.10),(1+0.08),(1+0.05)],[(1-0.10),(1-0.08),(1-0.05)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_WR4JhighxEM'] = Systematic("DibosonsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.25),(1+0.23),(1+0.36)],[(1-0.25),(1-0.23),(1-0.36)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_WR4JhighxEM'] = Systematic("DibosonsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.06),(1+0.01),(1+0.03)],[(1-0.06),(1-0.01),(1-0.03)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_TR4JhighxEM'] = Systematic("DibosonsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.27),(1+1.09),(1+0.77)],[(1-0.27),(1-1.09),(1-0.77)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_TR4JhighxEM'] = Systematic("DibosonsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.17),(1+0.04),(1+0.01)],[(1-0.17),(1-0.04),(1-0.01)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_TR4JhighxEM'] = Systematic("DibosonsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.39),(1+0.59),(1+0.93)],[(1-0.39),(1-0.59),(1-0.93)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_TR4JhighxEM'] = Systematic("DibosonsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.09),(1+0.13),(1+0.01)],[(1-0.09),(1-0.13),(1-0.01)], "user","userHistoSys")


DibosonSystematics['DibosonsGenTheo_VR4JhighxaplEM'] = Systematic("DibosonsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.16),(1+0.17),(1+0.84)],[(1-0.16),(1-0.17),(1-0.84)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR4JhighxaplEM'] = Systematic("DibosonsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.11),(1+0.02)],[(1-0.01),(1-0.11),(1-0.02)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR4JhighxaplEM'] = Systematic("DibosonsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.11),(1+0.53),(1+0.71)],[(1-0.11),(1-0.53),(1-0.71)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR4JhighxaplEM'] = Systematic("DibosonsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.03),(1+0.06),(1+0.25)],[(1-0.03),(1-0.06),(1-0.25)], "user","userHistoSys")


DibosonSystematics['DibosonsGenTheo_VR4JhighxhybridEM']= Systematic("DibosonsGenTheo_R4JhighxEM",configMgr.weights,[(1+0.11),(1+0.36),(1+0.67)],[(1-0.11),(1-0.36),(1-0.67)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR4JhighxhybridEM'] = Systematic("DibosonsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.05),(1+0.20),(1+0.28)],[(1-0.05),(1-0.20),(1-0.28)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR4JhighxhybridEM'] = Systematic("DibosonsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.42),(1+0.40),(1+0.26)],[(1-0.42),(1-0.40),(1-0.26)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR4JhighxhybridEM'] = Systematic("DibosonsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.09),(1+0.01),(1+0.06)],[(1-0.09),(1-0.01),(1-0.06)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_VR4JhighxmtEM'] = Systematic("DibosonsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.25),(1+0.40),(1+0.45)],[(1-0.25),(1-0.40),(1-0.45)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR4JhighxmtEM'] = Systematic("DibosonsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.04),(1+0.45)],[(1-0.01),(1-0.04),(1-0.45)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR4JhighxmtEM'] = Systematic("DibosonsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.43),(1+0.41),(1+0.58)],[(1-0.43),(1-0.41),(1-0.58)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR4JhighxmtEM'] = Systematic("DibosonsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.09),(1+0.19),(1+0.24)],[(1-0.09),(1-0.19),(1-0.24)], "user","userHistoSys")




DibosonSystematics['DibosonsGenTheo_WR6JEM']= Systematic("DibosonsGenTheo_R6JEM", configMgr.weights,[(1+0.60),(1+0.72),(1+0.70),(1+0.81)],[(1-0.60),(1-0.72),(1-0.70),(1-0.81)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_WR6JEM']=  Systematic("DibosonsResummationTheo_R6JEM",configMgr.weights,[(1+0.03),(1+0.01),(1+0.07),(1+0.13)],[(1-0.03),(1-0.01),(1-0.07),(1-0.13)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_WR6JEM'] = Systematic("DibosonsRenormTheo_R6JEM",configMgr.weights,[(1+0.34),(1+0.26),(1+0.22),(1+0.41)],[(1-0.34),(1-0.26),(1-0.22),(1-0.41)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_WR6JEM'] = Systematic("DibosonsFacTheo_R6JEM",configMgr.weights,[(1+0.02),(1+0.00),(1+0.10),(1+0.19)],[(1-0.02),(1-0.00),(1-0.10),(1-0.19)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_TR6JEM'] = Systematic("DibosonsGenTheo_R6JEM", configMgr.weights,[(1+0.73),(1+0.73),(1+0.55),(1+0.50)],[(1-0.73),(1-0.73),(1-0.55),(1-0.50)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_TR6JEM'] = Systematic("DibosonsResummationTheo_R6JEM",configMgr.weights,[(1+0.10),(1+0.08),(1+0.07),(1+0.16)],[(1-0.10),(1-0.08),(1-0.07),(1-0.16)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_TR6JEM'] = Systematic("DibosonsRenormTheo_R6JEM",configMgr.weights,[(1+0.51),(1+0.36),(1+0.36),(1+0.61)],[(1-0.51),(1-0.36),(1-0.36),(1-0.61)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_TR6JEM'] = Systematic("DibosonsFacTheo_R6JEM",configMgr.weights,[(1+0.17),(1+0.22),(1+0.32),(1+0.68)],[(1-0.17),(1-0.22),(1-0.32),(1-0.68)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_VR6JmtEM']= Systematic("DibosonsGenTheo_R6JEM", configMgr.weights,[(1+0.58),(1+0.75),(1+0.24),(1+0.41)],[(1-0.58),(1-0.75),(1-0.24),(1-0.41)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR6JmtEM']=  Systematic("DibosonsResummationTheo_R6JEM",configMgr.weights,[(1+0.18),(1+0.04),(1+0.25),(1+0.14)],[(1-0.18),(1-0.04),(1-0.25),(1-0.14)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR6JmtEM'] = Systematic("DibosonsRenormTheo_R6JEM",configMgr.weights,[(1+0.40),(1+0.35),(1+0.20),(1+0.25)],[(1-0.40),(1-0.35),(1-0.20),(1-0.25)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR6JmtEM'] = Systematic("DibosonsFacTheo_R6JEM",configMgr.weights,[(1+0.15),(1+0.04),(1+0.03),(1+0.38)],[(1-0.15),(1-0.04),(1-0.03),(1-0.38)], "user","userHistoSys")

DibosonSystematics['DibosonsGenTheo_VR6JaplEM'] = Systematic("DibosonsGenTheo_R6JEM", configMgr.weights,[(1+0.62),(1+0.74),(1+0.73),(1+0.36)],[(1-0.62),(1-0.74),(1-0.73),(1-0.36)], "user","userHistoSys")
DibosonSystematics['DibosonsResummationTheo_VR6JaplEM'] = Systematic("DibosonsResummationTheo_R6JEM",configMgr.weights,[(1+0.05),(1+0.09),(1+0.21),(1+0.34)],[(1-0.05),(1-0.09),(1-0.21),(1-0.34)], "user","userHistoSys")
DibosonSystematics['DibosonsRenormTheo_VR6JaplEM'] = Systematic("DibosonsRenormTheo_R6JEM",configMgr.weights,[(1+0.33),(1+0.25),(1+0.47),(1+0.20)],[(1-0.33),(1-0.25),(1-0.47),(1-0.20)], "user","userHistoSys")
DibosonSystematics['DibosonsFacTheo_VR6JaplEM'] = Systematic("DibosonsFacTheo_R6JEM",configMgr.weights,[(1+0.07),(1+0.03),(1+0.10),(1+0.10)],[(1-0.07),(1-0.03),(1-0.10),(1-0.10)], "user","userHistoSys")



for reg in Regions:
               DibosonSystematics['DibosonsGenTheo_SR2J'+reg] = Systematic("DibosonsGenTheo_R2JEM", configMgr.weights,[(1+0.25),(1+0.53),(1+0.43),(1+0.54)],[(1-0.25),(1-0.53),(1-0.43),(1-0.54)], "user","userHistoSys")
               DibosonSystematics['DibosonsResummationTheo_SR2J'+reg]  = Systematic("DibosonsResummationTheo_R2JEM", configMgr.weights,[(1+0.14),(1+0.03),(1+0.20),(1+0.23)],[(1-0.14),(1-0.03),(1-0.20),(1-0.23)], "user","userHistoSys")
               DibosonSystematics['DibosonsRenormTheo_SR2J'+reg] = Systematic("DibosonsRenormTheo_R2JEM", configMgr.weights,[(1+0.57),(1+0.75),(1+0.33),(1+0.55)],[(1-0.57),(1-0.75),(1-0.33),(1-0.55)], "user","userHistoSys")
               DibosonSystematics['DibosonsFacTheo_SR2J'+reg]  = Systematic("DibosonsFacTheo_R2JEM", configMgr.weights,[(1+0.14),(1+0.03),(1+0.20),(1+0.23)],[(1-0.14),(1-0.03),(1-0.20),(1-0.23)], "user","userHistoSys")
	       
	       
               DibosonSystematics['DibosonsGenTheo_SR4Jlowx'+reg] = Systematic("DibosonsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.55),(1+0.45),(1+1.01)],[(1-0.55),(1-0.45),(1-1.01)], "user","userHistoSys")
               DibosonSystematics['DibosonsResummationTheo_SR4Jlowx'+reg]  = Systematic("DibosonsResummationTheo_R4JlowxEM", configMgr.weights,[(1+0.15),(1+0.08),(1+0.09)],[(1-0.15),(1-0.08),(1-0.09)], "user","userHistoSys")
               DibosonSystematics['DibosonsRenormTheo_SR4Jlowx'+reg] = Systematic("DibosonsRenormTheo_R4JlowxEM", configMgr.weights,[(1+0.09),(1+0.46),(1+0.62)],[(1-0.09),(1-0.46),(1-0.62)], "user","userHistoSys")
               DibosonSystematics['DibosonsFacTheo_SR4Jlowx'+reg]  = Systematic("DibosonsFacTheo_R4JlowxEM", configMgr.weights,[(1+0.01),(1+0.01),(1+0.08)],[(1-0.01),(1-0.01),(1-0.08)], "user","userHistoSys")
	       
               DibosonSystematics['DibosonsGenTheo_SR4Jhighx'+reg] = Systematic("DibosonsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.17),(1+0.73),(1+0.84)],[(1-0.17),(1-0.73),(1-0.84)], "user","userHistoSys")
               DibosonSystematics['DibosonsResummationTheo_SR4Jhighx'+reg]  = Systematic("DibosonsResummationTheo_R4JhighxEM", configMgr.weights,[(1+0.12),(1+0.29),(1+0.33)],[(1-0.12),(1-0.29),(1-0.33)], "user","userHistoSys")
               DibosonSystematics['DibosonsRenormTheo_SR4Jhighx'+reg] = Systematic("DibosonsRenormTheo_R4JhighxEM", configMgr.weights,[(1+0.45),(1+0.23),(1+0.14)],[(1-0.45),(1-0.23),(1-0.14)], "user","userHistoSys")
               DibosonSystematics['DibosonsFacTheo_SR4Jhighx'+reg]  = Systematic("DibosonsFacTheo_R4JhighxEM", configMgr.weights,[(1+0.06),(1+0.19),(1+0.05)],[(1-0.06),(1-0.19),(1-0.05)], "user","userHistoSys")
	       
	       
	       
               DibosonSystematics['DibosonsGenTheo_SR6J'+reg] = Systematic("DibosonsGenTheo_R6JEM", configMgr.weights,[(1+0.86),(1+0.46),(1+0.99),(1+0.89)],[(1-0.86),(1-0.46),(1-0.99),(1-0.89)], "user","userHistoSys")
               DibosonSystematics['DibosonsResummationTheo_SR6J'+reg]  = Systematic("DibosonsResummationTheo_R6JEM", configMgr.weights,[(1+0.02),(1+0.10),(1+0.43),(1+0.36)],[(1-0.02),(1-0.10),(1-0.43),(1-0.36)], "user","userHistoSys")
               DibosonSystematics['DibosonsRenormTheo_SR6J'+reg] = Systematic("DibosonsRenormTheo_R6JEM", configMgr.weights,[(1+0.42),(1+0.25),(1+0.29),(1+0.22)],[(1-0.42),(1-0.25),(1-0.29),(1-0.22)], "user","userHistoSys")
               DibosonSystematics['DibosonsFacTheo_SR6J'+reg]  = Systematic("DibosonsFacTheo_R6JEM", configMgr.weights,[(1+0.01),(1+0.29),(1+0.06),(1+0.38)],[(1-0.01),(1-0.29),(1-0.06),(1-0.38)], "user","userHistoSys")
	       
	       

def TheorUnc(generatorSyst):
           
    for key in DibosonSystematics: 
        name=key.split('_') 
	  	
        generatorSyst.append((("diboson_Sherpa221",name[1]), DibosonSystematics[key]))
	
    return generatorSyst
