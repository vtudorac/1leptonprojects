import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

Regions = [ 'BVEM', 'BTEM' ]
MeffBins = [ '_bin1', '_bin2', '_bin3', '_bin4']
SingleTopSystematics={}

for  bin in MeffBins :
   if 'bin1' in bin:
                
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.17,1.-0.17 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.10,1.-0.10 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.22,1.-0.22 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.19,1.-0.19 , "user","userOverallSys")
		
         	
		
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.28,1.-0.28 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.31,1.-0.31 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmetEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.40,1.-0.40 , "user","userOverallSys")		
         	
		
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JlowxhybridEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.26,1.-0.26 , "user","userOverallSys")
		
		
		
         	
		
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.21,1.-0.21 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JlowxaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.19,1.-0.19 , "user","userOverallSys")      	
		
		
		
		
		
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxhybridEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
		
         	
		
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.07,1.-0.07 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.08,1.-0.08 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.14,1.-0.14 , "user","userOverallSys")
         	
				
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.45,1.-0.45 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.32,1.-0.32 , "user","userOverallSys") 	

		
		
		  
   
   	        
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.07,1.-0.07 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
         	
		
         	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
         	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
         	
				
   elif 'bin2' in bin:
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.37,1.-0.37 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.05,1.-0.10 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.56,1.-0.56 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.37,1.-0.37 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmetEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.09,1.-0.09 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.26,1.-0.26 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.68,1.-0.68 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JlowxhybridEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.08,1.-0.08 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.20,1.-0.20 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.93,1.-0.93 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.08,1.-0.08 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JlowxaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.14,1.-0.14 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	
		
				
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.34,1.-0.34 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.42,1.-0.42 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.41,1.-0.41 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
        	
		
			
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxhybridEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.22,1.-0.22 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.22,1.-0.22 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.29,1.-0.29 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")     	
	
     
	
	
   	        
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.44,1.-0.44 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.16,1.-0.16 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.07,1.-0.07 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
        	
		
		
   elif 'bin3' in bin:
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.28,1.-0.28 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.09,1.-0.09 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.08,1.-0.08 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.25,1.-0.25 , "user","userOverallSys")
        			
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.82,1.-0.82 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmetEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.21,1.-0.21, "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.32,1.-0.32 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JlowxhybridEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JlowxhybridEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.27,1.-0.27 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JlowxaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JlowxaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.44,1.-0.44 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.47,1.-0.47 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxhybridEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxhybridEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.58,1.-0.58 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.42,1.-0.42 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
        	
	        
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.85,1.-0.85 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR4JhighxmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.53,1.-0.53 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR4JhighxmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.70,1.-0.70 , "user","userOverallSys")
        	
   
   
   	        
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.25,1.-0.25 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
        	
		
   elif 'bin4' in bin:
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.76,1.-0.76 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.33,1.-0.33 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.56,1.-0.56 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+1.64,1.-1.64 , "user","userOverallSys")
        	
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.54,1.-0.54 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.19,1.-0.19 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR2JmetEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.26,1.-0.26 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR2JmetEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.33,1.-0.33 , "user","userOverallSys")
        	
   
  
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.45,1.-0.45 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JmtEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.25,1.-0.25 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JmtEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.32,1.-0.32 , "user","userOverallSys")
        	
        	SingleTopSystematics['SingleTopSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.53,1.-0.53 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_VR6JaplEM'+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.10,1.-0.10 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_VR6JaplEM'+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
        	
				
   else:
              print "No known bin found"		
				
				
		
   for	reg in Regions:
   
   
   
        if 'bin1' in bin:
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.31,1.-0.31 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR2J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.37,1.-0.37 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.66,1.-0.66 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR4Jlowx'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.39,1.-0.39 , "user","userOverallSys")
        	
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.29,1.-0.29 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.08,1.-0.08 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR4Jhighx'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.31,1.-0.31 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.29,1.-0.29 , "user","userOverallSys")       			
		
			
		        
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.41,1.-0.41 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR6J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.45,1.-0.45 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	
        	
        elif 'bin2' in bin:
	
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.30,1.-0.30, "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR2J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.23,1.-0.23 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.84,1.-0.84, "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR4Jlowx'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.42,1.-0.42 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.25,1.-0.25 , "user","userOverallSys")
        	
				
				
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.32,1.-0.32, "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.09,1.-0.09 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR4Jhighx'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.14,1.-0.14 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.17,1.-0.17 , "user","userOverallSys")
        	
		
		
			
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.51,1.-0.51, "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.24,1.-0.24 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR6J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.06,1.-0.06 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.27,1.-0.27 , "user","userOverallSys")
        	
		
        elif 'bin3' in bin:
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.35,1.-0.35 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.28,1.-0.28 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR2J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.36,1.-0.36 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.27,1.-0.27 , "user","userOverallSys")
        	
		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.45,1.-0.45 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.33,1.-0.33 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR4Jlowx'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.26,1.-0.26 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR4Jlowx'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.34,1.-0.34 , "user","userOverallSys")
        	
		
		
				
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.38,1.-0.38 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.48,1.-0.48 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR4Jhighx'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.07,1.-0.07 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR4Jhighx'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.39,1.-0.39 , "user","userOverallSys")
        			
	
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.45,1.-0.45 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.34,1.-0.34 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR6J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")
        	
        	
        elif 'bin4' in bin:
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.50,1.-0.50 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R2JEM"+bin, configMgr.weights,1.+0.35,1.-0.35 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR2J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R2JEM"+bin, configMgr.weights,1.+0.33,1.-0.33 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR2J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R2JEM"+bin, configMgr.weights,1.+0.52,1.-0.52 , "user","userOverallSys")
        	
        		
        	SingleTopSystematics['SingleTopSherpa22GenTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.43,1.-0.43 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22InterferenceTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22InterferenceTheo_R6JEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22QCDScalesRadiationTheo_SR6J'+reg+bin]= Systematic("SingleTopSherpa22QCDScalesRadiationTheo_R6JEM"+bin, configMgr.weights,1.+0.22,1.-0.22 , "user","userOverallSys")
        	SingleTopSystematics['SingleTopSherpa22HadFragTheo_SR6J'+reg+bin] = Systematic("SingleTopSherpa22HadFragTheo_R6JEM"+bin, configMgr.weights,1.+0.21,1.-0.21 , "user","userOverallSys")
        	
        	
        else:
              print "No known bin found"			
         
def TheorUnc(generatorSyst):
           
    for key in SingleTopSystematics: 
           name=key.split('_')
	   #print key
           name1=name[1]+"_"+name[2]
           #print name1
	 
	     
	     
           if "SR2JBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin1","SR2JBVEM"), SingleTopSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_2J_bin2","SR2JBVEM"), SingleTopSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_2J_bin3","SR2JBVEM"), SingleTopSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("singletop_2J_bin4","SR2JBVEM"), SingleTopSystematics[key]))
	      	   	  
           if "SR2JBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin1","SR2JBTEM"), SingleTopSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin2","SR2JBTEM"), SingleTopSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin3","SR2JBTEM"), SingleTopSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin4" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin4","SR2JBTEM"), SingleTopSystematics[key]))
	      
	      
           if "VR2JmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin1","VR2JmtEM"), SingleTopSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_2J_bin2","VR2JmtEM"), SingleTopSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_2J_bin3","VR2JmtEM"), SingleTopSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("singletop_2J_bin4","VR2JmtEM"), SingleTopSystematics[key]))
	 
           if "VR2JmetEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_2J_bin1","VR2JmetEM"), SingleTopSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_2J_bin2","VR2JmetEM"), SingleTopSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("singletop_2J_bin3","VR2JmetEM"), SingleTopSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin4" in name1:
             generatorSyst.append((("singletop_2J_bin4","VR2JmetEM"), SingleTopSystematics[key]))
	     
	     
           if "SR4JlowxBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jlowx_bin1","SR4JlowxBVEM"), SingleTopSystematics[key]))
           if  "SR4JlowxBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jlowx_bin2","SR4JlowxBVEM"), SingleTopSystematics[key]))
           if  "SR4JlowxBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_4Jlowx_bin3","SR4JlowxBVEM"), SingleTopSystematics[key]))
          
	      	   	  
           if "SR4JlowxBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jlowx_bin1","SR4JlowxBTEM"), SingleTopSystematics[key]))
           if "SR4JlowxBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("singletop_4Jlowx_bin2","SR4JlowxBTEM"), SingleTopSystematics[key]))
           if "SR4JlowxBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("singletop_4Jlowx_bin3","SR4JlowxBTEM"), SingleTopSystematics[key]))
          
	      
	      
           if "VR4JlowxhybridEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jlowx_bin1","VR4JlowxhybridEM"), SingleTopSystematics[key]))
           if  "VR4JlowxhybridEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jlowx_bin2","VR4JlowxhybridEM"), SingleTopSystematics[key]))
           if  "VR4JlowxhybridEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_4Jlowx_bin3","VR4JlowxhybridEM"), SingleTopSystematics[key]))
          
           if "VR4JlowxaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jlowx_bin1","VR4JlowxaplEM"), SingleTopSystematics[key]))
           if  "VR4JlowxaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jlowx_bin2","VR4JlowxaplEM"), SingleTopSystematics[key]))
           if  "VR4JlowxaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("singletop_4Jlowx_bin3","VR4JlowxaplEM"), SingleTopSystematics[key]))
             
	     
	     
	     
	       
           if "SR4JhighxBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin1","SR4JhighxBVEM"), SingleTopSystematics[key]))
           if  "SR4JhighxBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin2","SR4JhighxBVEM"), SingleTopSystematics[key]))
           if  "SR4JhighxBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin3","SR4JhighxBVEM"), SingleTopSystematics[key]))
           
	      	   	  
           if "SR4JhighxBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin1","SR4JhighxBTEM"), SingleTopSystematics[key]))
           if "SR4JhighxBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin2","SR4JhighxBTEM"), SingleTopSystematics[key]))
           if "SR4JhighxBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin3","SR4JhighxBTEM"), SingleTopSystematics[key]))
          
	      
	      
           if "VR4JhighxhybridEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin1","VR4JhighxhybridEM"), SingleTopSystematics[key]))
           if  "VR4JhighxhybridEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin2","VR4JhighxhybridEM"), SingleTopSystematics[key]))
           if  "VR4JhighxhybridEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin3","VR4JhighxhybridEM"), SingleTopSystematics[key]))
          
	 
           if "VR4JhighxaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin1","VR4JhighxaplEM"), SingleTopSystematics[key]))
           if  "VR4JhighxaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin2","VR4JhighxaplEM"), SingleTopSystematics[key]))
           if  "VR4JhighxaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("singletop_4Jhighx_bin3","VR4JhighxaplEM"), SingleTopSystematics[key]))
          
	       
	        
           if "VR4JhighxmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_4Jhighx_bin1","VR4JhighxmtEM"), SingleTopSystematics[key]))
           if  "VR4JhighxmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin2","VR4JhighxmtEM"), SingleTopSystematics[key]))
           if  "VR4JhighxmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_4Jhighx_bin3","VR4JhighxmtEM"), SingleTopSystematics[key]))
           
	  
	     
           if "SR6JBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin1","SR6JBVEM"), SingleTopSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_6J_bin2","SR6JBVEM"), SingleTopSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_6J_bin3","SR6JBVEM"), SingleTopSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("singletop_6J_bin4","SR6JBVEM"), SingleTopSystematics[key]))
	      	   	  
           if "SR6JBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin1","SR6JBTEM"), SingleTopSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin2","SR6JBTEM"), SingleTopSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin3","SR6JBTEM"), SingleTopSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin4" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin4","SR6JBTEM"), SingleTopSystematics[key]))
	      
	      
           if "VR6JmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin1","VR6JmtEM"), SingleTopSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_6J_bin2","VR6JmtEM"), SingleTopSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("singletop_6J_bin3","VR6JmtEM"), SingleTopSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("singletop_6J_bin4","VR6JmtEM"), SingleTopSystematics[key]))
	 
           if "VR6JaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("singletop_6J_bin1","VR6JaplEM"), SingleTopSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("singletop_6J_bin2","VR6JaplEM"), SingleTopSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("singletop_6J_bin3","VR6JaplEM"), SingleTopSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin4" in name1:
             generatorSyst.append((("singletop_6J_bin4","VR6JaplEM"), SingleTopSystematics[key])) 
	      	   	      
	      	   	     
	  
	      	   	      
	      
           
    return generatorSyst
