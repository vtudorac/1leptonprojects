import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


Regions = [ 'BVEM', 'BTEM' ]
MeffBins = [ '_bin1', '_bin2', '_bin3', '_bin4']
TtbarSystematics={}

for  bin in MeffBins :
   if 'bin1' in bin:
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmtEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmetEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmetEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmetEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
		
		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
		
		
		
				
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")		
			
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")	
                		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.27, 1.-0.27 , "user","userOverallSys")
		
		
     
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JmtEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JaplEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
   elif 'bin2' in bin:
   
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmtEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmetEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmetEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmetEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
		
		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.28, 1.-0.28 , "user","userOverallSys")		
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
		
				
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.18, 1.-0.18 , "user","userOverallSys")		
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.18, 1.-0.18 , "user","userOverallSys")
		                
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.27, 1.-0.27 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.61, 1.-0.61 , "user","userOverallSys")
   
 		
		
			
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JmtEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JaplEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
		
		
   elif 'bin3' in bin:
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmtEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.22, 1.-0.22 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmetEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmetEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmetEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
		
		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JlowxhybridEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.48, 1.-0.48 , "user","userOverallSys")		
                
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.14, 1.-0.14 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JlowxaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.32, 1.-0.32 , "user","userOverallSys")
		
		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxhybridEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.36, 1.-0.36 , "user","userOverallSys")		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.35, 1.-0.35 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.50, 1.-0.50 , "user","userOverallSys")		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.17, 1.-0.17 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarHadFragTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.22, 1.-0.22 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR4JhighxmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R4JhybridxEM"+bin, configMgr.weights, 1.+0.46, 1.-0.46 , "user","userOverallSys")				
    
		
		
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JmtEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.23, 1.-0.23 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JaplEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.24, 1.-0.24 , "user","userOverallSys")
   elif 'bin4' in bin:
   
    
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmtEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.42, 1.-0.42 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR2JmetEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR2JmetEM'+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR2JmetEM'+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.27, 1.-0.27 , "user","userOverallSys")
		
		
   
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JmtEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JmtEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JmtEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.19, 1.-0.19 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_VR6JaplEM'+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_VR6JaplEM'+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_VR6JaplEM'+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.24, 1.-0.24 , "user","userOverallSys")
		
   else:
              print "No known bin found"
	      
   for	reg in Regions:
        if 'bin1' in bin:
	
                TtbarSystematics['TtbarRadQCDScalesTheo_SR2J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR2J'+reg+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR2J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.24, 1.-0.24 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.25 , 1.-0.25 , "user","userOverallSys")
				
                TtbarSystematics['TtbarRadQCDScalesTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarHadFragTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarHardScatGenTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
		
		
	
                TtbarSystematics['TtbarRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.16, 1.-0.16 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
		
        elif 'bin2' in bin:
                TtbarSystematics['TtbarRadQCDScalesTheo_SR2J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR2J'+reg+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR2J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
		
                TtbarSystematics['TtbarRadQCDScalesTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.14, 1.-0.14 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.21, 1.-0.21 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.13, 1.-0.13 , "user","userOverallSys")
		
			
                TtbarSystematics['TtbarRadQCDScalesTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarHadFragTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarHardScatGenTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.16, 1.-0.16 , "user","userOverallSys")
	
                TtbarSystematics['TtbarRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.20, 1.-0.20 , "user","userOverallSys")
              
        elif 'bin3' in bin:
                TtbarSystematics['TtbarRadQCDScalesTheo_SR2J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR2J'+reg+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR2J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
		
		
                TtbarSystematics['TtbarRadQCDScalesTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.15, 1.-0.15 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarHadFragTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR4Jlowx'+reg+bin] = Systematic("TtbarHardScatGenTheo_R4JlowxEM"+bin, configMgr.weights, 1.+0.30, 1.-0.30 , "user","userOverallSys")
               
	        
                TtbarSystematics['TtbarRadQCDScalesTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarHadFragTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.35, 1.-0.35 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR4Jhighx'+reg+bin] = Systematic("TtbarHardScatGenTheo_R4JhighxEM"+bin, configMgr.weights, 1.+0.30, 1.-0.30 , "user","userOverallSys")
               
	
                TtbarSystematics['TtbarRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.19, 1.-0.19 , "user","userOverallSys")
               
        elif 'bin4' in bin:
	
                TtbarSystematics['TtbarRadQCDScalesTheo_SR2J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R2JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR2J'+reg+bin] = Systematic("TtbarHadFragTheo_R2JEM"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR2J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R2JEM"+bin, configMgr.weights, 1.+0.17, 1.-0.17 , "user","userOverallSys")
             
	
                TtbarSystematics['TtbarRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_R6JEM"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarHadFragTheo_R6JEM"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
                TtbarSystematics['TtbarHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarHardScatGenTheo_R6JEM"+bin, configMgr.weights, 1.+0.21, 1.-0.21 , "user","userOverallSys")
             
		
        else:
              print "No known bin found"	

         
def TheorUnc(generatorSyst):
           
    for key in TtbarSystematics: 
           name=key.split('_')
	   #print key
           name1=name[1]+"_"+name[2]
           #print name1
	   
	   
	   
           if "SR2JBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin1","SR2JBVEM"), TtbarSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_2J_bin2","SR2JBVEM"), TtbarSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_2J_bin3","SR2JBVEM"), TtbarSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("ttbar_2J_bin4","SR2JBVEM"), TtbarSystematics[key]))
	      	   	  
           if "SR2JBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin1","SR2JBTEM"), TtbarSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin2","SR2JBTEM"), TtbarSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin3","SR2JBTEM"), TtbarSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin4" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin4","SR2JBTEM"), TtbarSystematics[key]))
	      
	      
           if "VR2JmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin1","VR2JmtEM"), TtbarSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_2J_bin2","VR2JmtEM"), TtbarSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_2J_bin3","VR2JmtEM"), TtbarSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("ttbar_2J_bin4","VR2JmtEM"), TtbarSystematics[key]))
	 
           if "VR2JmetEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_2J_bin1","VR2JmetEM"), TtbarSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_2J_bin2","VR2JmetEM"), TtbarSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("ttbar_2J_bin3","VR2JmetEM"), TtbarSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin4" in name1:
             generatorSyst.append((("ttbar_2J_bin4","VR2JmetEM"), TtbarSystematics[key]))  
	     
	     
           if "SR4JlowxBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jlowx_bin1","SR4JlowxBVEM"), TtbarSystematics[key]))
           if  "SR4JlowxBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jlowx_bin2","SR4JlowxBVEM"), TtbarSystematics[key]))
           if  "SR4JlowxBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_4Jlowx_bin3","SR4JlowxBVEM"), TtbarSystematics[key]))
          
	      	   	  
           if "SR4JlowxBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jlowx_bin1","SR4JlowxBTEM"), TtbarSystematics[key]))
           if "SR4JlowxBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("ttbar_4Jlowx_bin2","SR4JlowxBTEM"), TtbarSystematics[key]))
           if "SR4JlowxBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("ttbar_4Jlowx_bin3","SR4JlowxBTEM"), TtbarSystematics[key]))
          
	      
	      
           if "VR4JlowxhybridEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jlowx_bin1","VR4JlowxhybridEM"), TtbarSystematics[key]))
           if  "VR4JlowxhybridEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jlowx_bin2","VR4JlowxhybridEM"), TtbarSystematics[key]))
           if  "VR4JlowxhybridEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_4Jlowx_bin3","VR4JlowxhybridEM"), TtbarSystematics[key]))
          
	 
           if "VR4JlowxaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jlowx_bin1","VR4JlowxaplEM"), TtbarSystematics[key]))
           if  "VR4JlowxaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jlowx_bin2","VR4JlowxaplEM"), TtbarSystematics[key]))
           if  "VR4JlowxaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("ttbar_4Jlowx_bin3","VR4JlowxaplEM"), TtbarSystematics[key])) 
	     
	     
           if "SR4JhighxBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin1","SR4JhighxBVEM"), TtbarSystematics[key]))
           if  "SR4JhighxBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin2","SR4JhighxBVEM"), TtbarSystematics[key]))
           if  "SR4JhighxBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin3","SR4JhighxBVEM"), TtbarSystematics[key]))
          
	      	   	  
           if "SR4JhighxBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin1","SR4JhighxBTEM"), TtbarSystematics[key]))
           if "SR4JhighxBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin2","SR4JhighxBTEM"), TtbarSystematics[key]))
           if "SR4JhighxBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin3","SR4JhighxBTEM"), TtbarSystematics[key]))
          
	      
	      
           if "VR4JhighxhybridEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin1","VR4JhighxhybridEM"), TtbarSystematics[key]))
           if  "VR4JhighxhybridEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin2","VR4JhighxhybridEM"), TtbarSystematics[key]))
           if  "VR4JhighxhybridEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin3","VR4JhighxhybridEM"), TtbarSystematics[key]))
          
	 
           if "VR4JhighxaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin1","VR4JhighxaplEM"), TtbarSystematics[key]))
           if  "VR4JhighxaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin2","VR4JhighxaplEM"), TtbarSystematics[key]))
           if  "VR4JhighxaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("ttbar_4Jhighx_bin3","VR4JhighxaplEM"), TtbarSystematics[key]))     
	     
           if "VR4JhighxmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_4Jhighx_bin1","VR4JhighxmtEM"), TtbarSystematics[key]))
           if  "VR4JhighxmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin2","VR4JhighxmtEM"), TtbarSystematics[key]))
           if  "VR4JhighxmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_4Jhighx_bin3","VR4JhighxmtEM"), TtbarSystematics[key]))
	      	   	     
	  
	
           if "SR6JBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin1","SR6JBVEM"), TtbarSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_6J_bin2","SR6JBVEM"), TtbarSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_6J_bin3","SR6JBVEM"), TtbarSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("ttbar_6J_bin4","SR6JBVEM"), TtbarSystematics[key]))
	      	   	  
           if "SR6JBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin1","SR6JBTEM"), TtbarSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin2","SR6JBTEM"), TtbarSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin3","SR6JBTEM"), TtbarSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin4" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin4","SR6JBTEM"), TtbarSystematics[key]))
	      
	      
           if "VR6JmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin1","VR6JmtEM"), TtbarSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_6J_bin2","VR6JmtEM"), TtbarSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("ttbar_6J_bin3","VR6JmtEM"), TtbarSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("ttbar_6J_bin4","VR6JmtEM"), TtbarSystematics[key]))
	 
           if "VR6JaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("ttbar_6J_bin1","VR6JaplEM"), TtbarSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("ttbar_6J_bin2","VR6JaplEM"), TtbarSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("ttbar_6J_bin3","VR6JaplEM"), TtbarSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin4" in name1:
             generatorSyst.append((("ttbar_6J_bin4","VR6JaplEM"), TtbarSystematics[key]))  
	      	   	     
	  
	      	   	      
	      
           
    return generatorSyst
