import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


Regions = [ 'BV', 'BT' ]
CRs = [ 'WR', 'TR' ]
MeffBins = [ '_bin1EM', '_bin2EM', '_bin3EM', '_bin4EM']
TtbarSystematics={}

for  bin in MeffBins :
   ## TF ##6,8,9,2,1,3
   TtbarSystematics['TtbarRadQCDScalesTheo_VR6Jmt'+bin] = Systematic("TtbarRadQCDScalesTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
   TtbarSystematics['TtbarHadFragTheo_VR6Jmt'+bin] = Systematic("TtbarHadFragTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.08, 1.+0.08 , "user","userOverallSys")
   TtbarSystematics['TtbarHardScatGenTheo_VR6Jmt'+bin] = Systematic("TtbarHardScatGenTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
   TtbarSystematics['TtbarRadQCDScalesTheo_VR6Japl'+bin] = Systematic("TtbarRadQCDScalesTheo_VR6Japl"+bin, configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
   TtbarSystematics['TtbarHadFragTheo_VR6Japl'+bin] = Systematic("TtbarHadFragTheo_VR6Japl"+bin, configMgr.weights, 1.-0.01, 1.+0.01 , "user","userOverallSys")
   TtbarSystematics['TtbarHardScatGenTheo_VR6Japl'+bin] = Systematic("TtbarHardScatGenTheo_VR6Japl"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
   for	reg in Regions:#8,6,18
      TtbarSystematics['TtbarRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarRadQCDScalesTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
      TtbarSystematics['TtbarHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarHadFragTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
      TtbarSystematics['TtbarHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarHardScatGenTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.18, 1.-0.18 , "user","userOverallSys")


   ## meff shape ##
   if 'bin1EM' in bin:#5,-3,12,7,-8,11
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Jmt'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.03, 1.+0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
		
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Japl'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Japl"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Japl'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Japl"+bin, configMgr.weights, 1.-0.08, 1.+0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Japl'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Japl"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
   elif 'bin2EM' in bin:#1,3,-11,-2,6,-20
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Jmt'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.11, 1.+0.11 , "user","userOverallSys")
   
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Japl'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Japl"+bin, configMgr.weights, 1.-0.02, 1.+0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Japl'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Japl"+bin, configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Japl'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Japl"+bin, configMgr.weights, 1.-0.20, 1.+0.20 , "user","userOverallSys")
   elif 'bin3EM' in bin:#-10,-8,-16,-13,7,7
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Jmt'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.10, 1.+0.10 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.08, 1.+0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.16, 1.+0.16 , "user","userOverallSys")
		
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Japl'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Japl"+bin, configMgr.weights, 1.-0.13, 1.+0.13 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Japl'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Japl"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Japl'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Japl"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
   elif 'bin4EM' in bin:#-13,16,12,-21,7,11
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Jmt'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Jmt"+bin, configMgr.weights, 1.-0.13, 1.+0.13 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.16, 1.-0.16 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Jmt'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Jmt"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
		
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_VR6Japl'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_VR6Japl"+bin, configMgr.weights, 1.-0.21, 1.+0.21 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_VR6Japl'+bin] = Systematic("TtbarShapeHadFragTheo_VR6Japl"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_VR6Japl'+bin] = Systematic("TtbarShapeHardScatGenTheo_VR6Japl"+bin, configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
		
   else:
              print "No known bin found"
	      
   for	reg in Regions:
        if 'bin1EM' in bin:#8,8,8
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarShapeRadQCDScalesTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHadFragTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHardScatGenTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
        elif 'bin2EM' in bin:#-3,-8,-6
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarShapeRadQCDScalesTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.03, 1.+0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHadFragTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.08, 1.+0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHardScatGenTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.06, 1.+0.06 , "user","userOverallSys")
        elif 'bin3EM' in bin:#-8,9,-40
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarShapeRadQCDScalesTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.08, 1.+0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHadFragTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHardScatGenTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.40, 1.+0.40 , "user","userOverallSys")
        elif 'bin4EM' in bin:#-9,-11,25
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_SR6J'+reg+bin] = Systematic("TtbarShapeRadQCDScalesTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.09, 1.+0.09 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHadFragTheo_SR6J"+reg+bin, configMgr.weights, 1.-0.11, 1.+0.11 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_SR6J'+reg+bin] = Systematic("TtbarShapeHardScatGenTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.25, 1.-0.25 , "user","userOverallSys")
		
        else:
              print "No known bin found"	


   for	cr in CRs:
        if 'bin1EM' in bin:#8,-3,-3
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_"+cr+"6J"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHadFragTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.03, 1.+0.03 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHardScatGenTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.03, 1.+0.03 , "user","userOverallSys")
        elif 'bin2EM' in bin:#-2,-0,-3
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.02, 1.+0.02 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHadFragTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.00, 1.+0.00 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHardScatGenTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.03, 1.+0.03 , "user","userOverallSys")
        elif 'bin3EM' in bin:#8,7,12
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.08, 1.+0.08 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHadFragTheo_"+cr+"6J"+bin, configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHardScatGenTheo_"+cr+"6J"+bin, configMgr.weights, 1.+0.12, 1.-0.12 , "user","userOverallSys")
        elif 'bin4EM' in bin:#-9,-0,8
                TtbarSystematics['TtbarShapeRadQCDScalesTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeRadQCDScalesTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.09, 1.+0.09 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHadFragTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHadFragTheo_"+cr+"6J"+bin, configMgr.weights, 1.-0.00, 1.+0.00 , "user","userOverallSys")
                TtbarSystematics['TtbarShapeHardScatGenTheo_'+cr+'6J'+bin] = Systematic("TtbarShapeHardScatGenTheo_"+cr+"6J"+bin, configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
		
        else:
              print "No known bin found"	

def TheorUnc(generatorSyst):
           
    for key in TtbarSystematics: 
        name=key.split('_')
        name1=name[1]+"_"+name[2]   	
        generatorSyst.append((("ttbar",name1), TtbarSystematics[key]))

	
    return generatorSyst
