import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

Regions = [ 'BVEM', 'BTEM' ]
MeffBins = [ '_bin1', '_bin2', '_bin3', '_bin4']
WjetsSystematics={}

for  bin in MeffBins :
   if 'bin1' in bin:
                
         	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.22,1.-0.22 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.00,1.-0.00 , "user","userOverallSys")
		
         	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmetEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
		
         	WjetsSystematics['WjetsSherpa22GenTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JlowxhybridEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
         	WjetsSystematics['WjetsSherpa22GenTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.09,1.-0.09 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JlowxaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys") 
		
		
		
		
		
		
         	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxhybridEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
		
         	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys") 
				
         	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.02,1.-0.02, "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

		
		
		  
   
   	        
         	WjetsSystematics['WjetsSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.39,1.-0.39 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
		
         	WjetsSystematics['WjetsSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.14,1.-0.14 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22FacTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
         	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
				
   elif 'bin2' in bin:
        	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.19,1.-0.19 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.09,1.-0.09 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmetEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
		
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.50,1.-0.50 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JlowxhybridEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JlowxaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
		
		
		
				
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")	
		
			
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.39,1.-0.39 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxhybridEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
	
     
	
	
   	        
        	WjetsSystematics['WjetsSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.29,1.-0.29 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
   elif 'bin3' in bin:
        	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.30,1.-0.30 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.14,1.-0.14 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmetEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.03,1.-0.3 , "user","userOverallSys")
		
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.50,1.-0.50 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JlowxhybridEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JlowxhybridEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.20,1.-0.20 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JlowxaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JlowxaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.32,1.-0.32 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxhybridEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxhybridEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.21,1.-0.21 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
	        
        	WjetsSystematics['WjetsSherpa22GenTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.28,1.-0.28 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR4JhighxmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR4JhighxmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

   
   
   	        
        	WjetsSystematics['WjetsSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.32,1.-0.32 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.27,1.-0.27 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
   elif 'bin4' in bin:
        	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.37,1.-0.37, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.25,1.-0.25 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR2JmetEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR2JmetEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
   
  
        	WjetsSystematics['WjetsSherpa22GenTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.36,1.-0.36 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JmtEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JmtEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.33,1.-0.33 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_VR6JaplEM'+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_VR6JaplEM'+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
				
   else:
              print "No known bin found"		
				
				
		
   for	reg in Regions:
   
   
   
        if 'bin1' in bin:
        	WjetsSystematics['WjetsSherpa22GenTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.74,1.-0.74 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR2J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.45,1.-0.45 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR4Jlowx'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.12,1.-0.12 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR4Jhighx'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")		
		
			
		        
        	WjetsSystematics['WjetsSherpa22GenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.47,1.-0.47 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	
        elif 'bin2' in bin:
	
        	WjetsSystematics['WjetsSherpa22GenTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.23,1.-0.23, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR2J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22GenTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.37,1.-0.37, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR4Jlowx'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
				
				
        	WjetsSystematics['WjetsSherpa22GenTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.20,1.-0.20, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR4Jhighx'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
			
        	WjetsSystematics['WjetsSherpa22GenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.14,1.-0.14, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        elif 'bin3' in bin:
        	WjetsSystematics['WjetsSherpa22GenTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.42,1.-0.42 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR2J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")	
		
        	WjetsSystematics['WjetsSherpa22GenTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.31,1.-0.31 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR4Jlowx'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR4Jlowx'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JlowxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
				
        	WjetsSystematics['WjetsSherpa22GenTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.17,1.-0.17, "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR4Jhighx'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.05,1.-0.05 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR4Jhighx'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R4JhighxEM"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")		
	
        	WjetsSystematics['WjetsSherpa22GenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.44,1.-0.44 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	
        elif 'bin4' in bin:
        	WjetsSystematics['WjetsSherpa22GenTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R2JEM"+bin, configMgr.weights,1.+0.40,1.-0.40 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R2JEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR2J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R2JEM"+bin, configMgr.weights,1.+0.04,1.-0.04 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R2JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR2J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R2JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        		
        	WjetsSystematics['WjetsSherpa22GenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_R6JEM"+bin, configMgr.weights,1.+0.5,1.-0.35 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22RenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22FacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_R6JEM"+bin, configMgr.weights,1.+0.02,1.-0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22CKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_R6JEM"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	
        else:
              print "No known bin found"			
         
def TheorUnc(generatorSyst):
           
    for key in WjetsSystematics: 
           name=key.split('_')
	   #print key
           name1=name[1]+"_"+name[2]
           #print name1
	 
	     
	     
           if "SR2JBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin1","SR2JBVEM"), WjetsSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_2J_bin2","SR2JBVEM"), WjetsSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_2J_bin3","SR2JBVEM"), WjetsSystematics[key]))
           if  "SR2JBVEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("Wjets_2J_bin4","SR2JBVEM"), WjetsSystematics[key]))
	      	   	  
           if "SR2JBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin1","SR2JBTEM"), WjetsSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin2","SR2JBTEM"), WjetsSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin3","SR2JBTEM"), WjetsSystematics[key]))
           if "SR2JBTEM" in name1 and "_bin4" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin4","SR2JBTEM"), WjetsSystematics[key]))
	      
	      
           if "VR2JmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin1","VR2JmtEM"), WjetsSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_2J_bin2","VR2JmtEM"), WjetsSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_2J_bin3","VR2JmtEM"), WjetsSystematics[key]))
           if  "VR2JmtEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("Wjets_2J_bin4","VR2JmtEM"), WjetsSystematics[key]))
	 
           if "VR2JmetEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_2J_bin1","VR2JmetEM"), WjetsSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_2J_bin2","VR2JmetEM"), WjetsSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("Wjets_2J_bin3","VR2JmetEM"), WjetsSystematics[key]))
           if  "VR2JmetEM" in name1 and "_bin4" in name1:
             generatorSyst.append((("Wjets_2J_bin4","VR2JmetEM"), WjetsSystematics[key]))
	     
	     
           if "SR4JlowxBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jlowx_bin1","SR4JlowxBVEM"), WjetsSystematics[key]))
           if  "SR4JlowxBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jlowx_bin2","SR4JlowxBVEM"), WjetsSystematics[key]))
           if  "SR4JlowxBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_4Jlowx_bin3","SR4JlowxBVEM"), WjetsSystematics[key]))
          
	      	   	  
           if "SR4JlowxBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jlowx_bin1","SR4JlowxBTEM"), WjetsSystematics[key]))
           if "SR4JlowxBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("Wjets_4Jlowx_bin2","SR4JlowxBTEM"), WjetsSystematics[key]))
           if "SR4JlowxBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("Wjets_4Jlowx_bin3","SR4JlowxBTEM"), WjetsSystematics[key]))
          
	      
	      
           if "VR4JlowxhybridEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jlowx_bin1","VR4JlowxhybridEM"), WjetsSystematics[key]))
           if  "VR4JlowxhybridEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jlowx_bin2","VR4JlowxhybridEM"), WjetsSystematics[key]))
           if  "VR4JlowxhybridEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_4Jlowx_bin3","VR4JlowxhybridEM"), WjetsSystematics[key]))
          
           if "VR4JlowxaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jlowx_bin1","VR4JlowxaplEM"), WjetsSystematics[key]))
           if  "VR4JlowxaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jlowx_bin2","VR4JlowxaplEM"), WjetsSystematics[key]))
           if  "VR4JlowxaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("Wjets_4Jlowx_bin3","VR4JlowxaplEM"), WjetsSystematics[key]))
             
	     
	     
	     
	       
           if "SR4JhighxBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin1","SR4JhighxBVEM"), WjetsSystematics[key]))
           if  "SR4JhighxBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin2","SR4JhighxBVEM"), WjetsSystematics[key]))
           if  "SR4JhighxBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin3","SR4JhighxBVEM"), WjetsSystematics[key]))
           
	      	   	  
           if "SR4JhighxBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin1","SR4JhighxBTEM"), WjetsSystematics[key]))
           if "SR4JhighxBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin2","SR4JhighxBTEM"), WjetsSystematics[key]))
           if "SR4JhighxBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin3","SR4JhighxBTEM"), WjetsSystematics[key]))
          
	      
	      
           if "VR4JhighxhybridEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin1","VR4JhighxhybridEM"), WjetsSystematics[key]))
           if  "VR4JhighxhybridEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin2","VR4JhighxhybridEM"), WjetsSystematics[key]))
           if  "VR4JhighxhybridEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin3","VR4JhighxhybridEM"), WjetsSystematics[key]))
          
	 
           if "VR4JhighxaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin1","VR4JhighxaplEM"), WjetsSystematics[key]))
           if  "VR4JhighxaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin2","VR4JhighxaplEM"), WjetsSystematics[key]))
           if  "VR4JhighxaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("Wjets_4Jhighx_bin3","VR4JhighxaplEM"), WjetsSystematics[key]))
          
	       
	        
           if "VR4JhighxmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_4Jhighx_bin1","VR4JhighxmtEM"), WjetsSystematics[key]))
           if  "VR4JhighxmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin2","VR4JhighxmtEM"), WjetsSystematics[key]))
           if  "VR4JhighxmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_4Jhighx_bin3","VR4JhighxmtEM"), WjetsSystematics[key]))
           
	  
	     
           if "SR6JBVEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin1","SR6JBVEM"), WjetsSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_6J_bin2","SR6JBVEM"), WjetsSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_6J_bin3","SR6JBVEM"), WjetsSystematics[key]))
           if  "SR6JBVEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("Wjets_6J_bin4","SR6JBVEM"), WjetsSystematics[key]))
	      	   	  
           if "SR6JBTEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin1","SR6JBTEM"), WjetsSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin2" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin2","SR6JBTEM"), WjetsSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin3" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin3","SR6JBTEM"), WjetsSystematics[key]))
           if "SR6JBTEM" in name1 and "_bin4" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin4","SR6JBTEM"), WjetsSystematics[key]))
	      
	      
           if "VR6JmtEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin1","VR6JmtEM"), WjetsSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_6J_bin2","VR6JmtEM"), WjetsSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin3" in name1:
              generatorSyst.append((("Wjets_6J_bin3","VR6JmtEM"), WjetsSystematics[key]))
           if  "VR6JmtEM" in name1 and "_bin4" in name1:
              generatorSyst.append((("Wjets_6J_bin4","VR6JmtEM"), WjetsSystematics[key]))
	 
           if "VR6JaplEM" in name1 and "_bin1" in name1:	     	      
              generatorSyst.append((("Wjets_6J_bin1","VR6JaplEM"), WjetsSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin2" in name1:
              generatorSyst.append((("Wjets_6J_bin2","VR6JaplEM"), WjetsSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin3" in name1:
             generatorSyst.append((("Wjets_6J_bin3","VR6JaplEM"), WjetsSystematics[key]))
           if  "VR6JaplEM" in name1 and "_bin4" in name1:
             generatorSyst.append((("Wjets_6J_bin4","VR6JaplEM"), WjetsSystematics[key])) 
	      	   	      
	      	   	     
	  
	      	   	      
	      
           
    return generatorSyst
