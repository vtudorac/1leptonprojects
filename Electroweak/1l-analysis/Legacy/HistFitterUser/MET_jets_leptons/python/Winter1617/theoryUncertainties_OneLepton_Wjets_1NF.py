import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

Regions = [ 'BV', 'BT' ]
CRs = [ 'WR', 'TR' ]
MeffBins = [ '_bin1EM', '_bin2EM', '_bin3EM', '_bin4EM']
WjetsSystematics={}

for  bin in MeffBins :
   ## TF ##11,-1,-1,-1,-1,  ,13,1,-1,1,1
   WjetsSystematics['WjetsSherpa22GenTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22GenTheo_VR6Jmt"+bin, configMgr.weights,1.+0.11,1.-0.11 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22ResummationTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ResummationTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22ResummationTheo_SR6Jmt'+bin]= Systematic("WjetsSherpa22RenormTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22FacTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22FacTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22CKKWTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22CKKWTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22GenTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22GenTheo_VR6Japl"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22ResummationTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ResummationTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22RenormTheo_SR6Japl'+bin]= Systematic("WjetsSherpa22RenormTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22FacTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22FacTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   WjetsSystematics['WjetsSherpa22CKKWTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22CKKWTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
   for  reg in Regions:#21,-1,-1,-1,1 
      WjetsSystematics['WjetsSherpa22GenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22GenTheo_SR6J"+reg+bin, configMgr.weights,1.+0.21,1.-0.21 , "user","userOverallSys")
      WjetsSystematics['WjetsSherpa22ResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ResummationTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
      WjetsSystematics['WjetsSherpa22RenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22RenormTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
      WjetsSystematics['WjetsSherpa22FacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22FacTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
      WjetsSystematics['WjetsSherpa22CKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22CKKWTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")

   ## meff shape ## 
   if 'bin1EM' in bin:#42,-1,-3,1,3,   ,13,-1,1,1,1  	        
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Jmt"+bin, configMgr.weights,1.+0.42,1.-0.42 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Jmt"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Jmt'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Jmt"+bin, configMgr.weights,1.-0.03,1.+0.03 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Jmt"+bin, configMgr.weights,1.+0.03,1.-0.03 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Japl"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Japl'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
				
   elif 'bin2EM' in bin:#-39,1,1,-1,1,  ,10,-1,-1,1,1 
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Jmt"+bin, configMgr.weights,1.-0.39,1.+0.39 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Jmt'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Jmt"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Japl"+bin, configMgr.weights,1.+0.10,1.-0.10 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Japl'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
		
   elif 'bin3EM' in bin:#18,-1,1,1,-4, ,-17,1,-1,-1,-1	        
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Jmt"+bin, configMgr.weights,1.+0.18,1.-0.18 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Jmt"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Jmt'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Jmt"+bin, configMgr.weights,1.-0.04,1.+0.04 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Japl"+bin, configMgr.weights,1.-0.17,1.+0.17 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Japl'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
		
   elif 'bin4EM' in bin:#-22,1,1,-1,1, ,-24,1,1,-2,-1 	        
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Jmt"+bin, configMgr.weights,1.-0.22,1.+0.22 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Jmt'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Jmt"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Jmt'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Jmt"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
		
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_VR6Japl"+bin, configMgr.weights,1.-0.24,1.+0.24 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6Japl'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_VR6Japl"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_VR6Japl"+bin, configMgr.weights,1.-0.02,1.+0.02 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_VR6Japl'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_VR6Japl"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
				
   else:
              print "No known bin found"		
				
		
   for	reg in Regions:
   
        if 'bin1EM' in bin:#47,-1,-1,1,-1	        
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeGenTheo_SR6J"+reg+bin, configMgr.weights,1.+0.47,1.-0.47 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeFacTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        elif 'bin2EM' in bin:#-56,1,1,1,1
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeGenTheo_SR6J"+reg+bin, configMgr.weights,1.-0.56,1.+0.56 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeFacTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")	
        elif 'bin3EM' in bin:#40,1,-1,-1,-1
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeGenTheo_SR6J"+reg+bin, configMgr.weights,1.+0.40,1.-0.40 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeFacTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        elif 'bin4EM' in bin:#34,1,1,-1,-1
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeGenTheo_SR6J"+reg+bin, configMgr.weights,1.+0.34,1.-0.34 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_SR6J'+reg+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_SR6J"+reg+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeFacTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_SR6J'+reg+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_SR6J"+reg+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")	
        else:
              print "No known bin found"			



   for	cr in CRs:
   
        if 'bin1EM' in bin:#13,-1,1,1,1	        
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.13,1.-0.13 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_'+cr+'6J'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        elif 'bin2EM' in bin:#-7,1,-1,-1,-1
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.07,1.+0.07 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_'+cr+'6J'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")	
        elif 'bin3EM' in bin:#-27,1,-1,-1,-1
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.27,1.+0.27 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_'+cr+'6J'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")
        elif 'bin4EM' in bin:#15,1,1,-4,-1
        	WjetsSystematics['WjetsSherpa22ShapeGenTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeGenTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.15,1.-0.15 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeResummationTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeResummationTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeRenormTheo_'+cr+'6J'+bin]= Systematic("WjetsSherpa22ShapeRenormTheo_"+cr+"6J"+bin, configMgr.weights,1.+0.01,1.-0.01 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeFacTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeFacTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.04,1.+0.04 , "user","userOverallSys")
        	WjetsSystematics['WjetsSherpa22ShapeCKKWTheo_'+cr+'6J'+bin] = Systematic("WjetsSherpa22ShapeCKKWTheo_"+cr+"6J"+bin, configMgr.weights,1.-0.01,1.+0.01 , "user","userOverallSys")	
        else:
              print "No known bin found"			
         
def TheorUnc(generatorSyst):
           
    for key in WjetsSystematics: 
        name=key.split('_')
        name1=name[1]+"_"+name[2]
        
        generatorSyst.append((("wjets_Sherpa221",name1), WjetsSystematics[key]))

    return generatorSyst
