import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

Regions = [ 'BVEM', 'BTEM' ]

ZjetsSystematics={}

ZjetsSystematics['zjetsGenTheo_WR2JEM']= Systematic("zjetsGenTheo_R2JEM", configMgr.weights,[(1+0.11),(1+0.31),(1+0.25),(1+0.49)],[(1-0.11),(1-0.31),(1-0.25),(1-0.49)], "user","userHistoSys")
ZjetsSystematics['zjetsGenTheo_TR2JEM']= Systematic("zjetsGenTheo_R2JEM", configMgr.weights,[(1+0.11),(1+0.31),(1+0.25),(1+0.49)],[(1-0.11),(1-0.31),(1-0.25),(1-0.49)], "user","userHistoSys")

ZjetsSystematics['zjetsFacTheo_WR2JEM'] = Systematic("zjetsFacTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_TR2JEM'] = Systematic("zjetsFacTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")

ZjetsSystematics['zjetsRenormTheo_WR2JEM'] = Systematic("zjetsRenormTheo_R2JEM",configMgr.weights,[(1+0.04),(1+0.05),(1+0.08),(1+0.01)],[(1-0.04),(1-0.05),(1-0.08),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_TR2JEM'] = Systematic("zjetsRenormTheo_R2JEM",configMgr.weights,[(1+0.04),(1+0.05),(1+0.08),(1+0.01)],[(1-0.04),(1-0.05),(1-0.08),(1-0.01)], "user","userHistoSys")

ZjetsSystematics['zjetsResummationTheo_WR2JEM'] = Systematic("zjetsResummationTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.02),(1+0.04),(1+0.03)],[(1-0.01),(1-0.02),(1-0.03),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_TR2JEM'] = Systematic("zjetsResummationTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.02),(1+0.04),(1+0.03)],[(1-0.01),(1-0.02),(1-0.03),(1-0.03)], "user","userHistoSys")

ZjetsSystematics['zjetsCKKWMatchTheo_WR2JEM'] = Systematic("zjetsCKKWMatchTheo_R2JEM",configMgr.weights,[(1+0.02),(1+0.02),(1+0.08),(1+0.06)],[(1-0.02),(1-0.02),(1-0.08),(1-0.06)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_TR2JEM'] = Systematic("zjetsCKKWMatchTheo_R2JEM",configMgr.weights,[(1+0.02),(1+0.02),(1+0.08),(1+0.06)],[(1-0.02),(1-0.02),(1-0.08),(1-0.06)], "user","userHistoSys")



ZjetsSystematics['zjetsGenTheo_VR2JmtEM']= Systematic("zjetsGenTheo_R2JEM", configMgr.weights,[(1+0.11),(1+0.31),(1+0.25),(1+0.49)],[(1-0.11),(1-0.31),(1-0.25),(1-0.49)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR2JmtEM'] = Systematic("zjetsFacTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR2JmtEM'] = Systematic("zjetsRenormTheo_R2JEM",configMgr.weights,[(1+0.04),(1+0.05),(1+0.08),(1+0.01)],[(1-0.04),(1-0.05),(1-0.08),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR2JmtEM']=  Systematic("zjetsResummationTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.02),(1+0.04),(1+0.03)],[(1-0.01),(1-0.02),(1-0.03),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR2JmtEM']=  Systematic("zjetsCKKWMatchTheo_R2JEM",configMgr.weights,[(1+0.02),(1+0.02),(1+0.08),(1+0.06)],[(1-0.02),(1-0.02),(1-0.08),(1-0.06)], "user","userHistoSys")


ZjetsSystematics['zjetsGenTheo_VR2JmetEM']= Systematic("zjetsGenTheo_R2JEM", configMgr.weights,[(1+0.11),(1+0.31),(1+0.25),(1+0.49)],[(1-0.11),(1-0.31),(1-0.25),(1-0.49)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR2JmetEM'] = Systematic("zjetsFacTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR2JmetEM'] = Systematic("zjetsRenormTheo_R2JEM",configMgr.weights,[(1+0.04),(1+0.05),(1+0.08),(1+0.01)],[(1-0.04),(1-0.05),(1-0.08),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR2JmetEM']=  Systematic("zjetsResummationTheo_R2JEM",configMgr.weights,[(1+0.01),(1+0.02),(1+0.04),(1+0.03)],[(1-0.01),(1-0.02),(1-0.03),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR2JmetEM']=  Systematic("zjetsCKKWMatchTheo_R2JEM",configMgr.weights,[(1+0.02),(1+0.02),(1+0.08),(1+0.06)],[(1-0.02),(1-0.02),(1-0.08),(1-0.06)], "user","userHistoSys")


ZjetsSystematics['zjetsGenTheo_WR4JlowxEM']= Systematic("zjetsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.16),(1+0.57),(1+0.38)],[(1-0.16),(1-0.57),(1-0.38)], "user","userHistoSys")
ZjetsSystematics['zjetsGenTheo_TR4JlowxEM']= Systematic("zjetsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.16),(1+0.57),(1+0.38)],[(1-0.16),(1-0.57),(1-0.38)], "user","userHistoSys")

ZjetsSystematics['zjetsFacTheo_WR4JlowxEM'] = Systematic("zjetsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_TR4JlowxEM'] = Systematic("zjetsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")

ZjetsSystematics['zjetsRenormTheo_WR4JlowxEM'] = Systematic("zjetsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.08),(1+0.09),(1+0.09)],[(1-0.08),(1-0.09),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_TR4JlowxEM'] = Systematic("zjetsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.08),(1+0.09),(1+0.09)],[(1-0.08),(1-0.09),(1-0.09)], "user","userHistoSys")

ZjetsSystematics['zjetsResummationTheo_WR4JlowxEM'] = Systematic("zjetsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.04),(1+0.04)],[(1-0.03),(1-0.04),(1-0.04)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_TR4JlowxEM'] = Systematic("zjetsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.04),(1+0.04)],[(1-0.03),(1-0.04),(1-0.04)], "user","userHistoSys")

ZjetsSystematics['zjetsCKKWMatchTheo_WR4JlowxEM'] = Systematic("zjetsCKKWMatchTheo_R4JlowxEM",configMgr.weights,[(1+0.06),(1+0.09),(1+0.10)],[(1-0.06),(1-0.09),(1-0.10)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_TR4JlowxEM'] = Systematic("zjetsCKKWMatchTheo_R4JlowxEM",configMgr.weights,[(1+0.06),(1+0.09),(1+0.10)],[(1-0.06),(1-0.09),(1-0.10)], "user","userHistoSys")



ZjetsSystematics['zjetsGenTheo_VR4JlowxhybridEM']= Systematic("zjetsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.16),(1+0.57),(1+0.38)],[(1-0.16),(1-0.57),(1-0.38)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR4JlowxhybridEM'] = Systematic("zjetsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR4JlowxhybridEM'] = Systematic("zjetsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.08),(1+0.09),(1+0.09)],[(1-0.08),(1-0.09),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR4JlowxhybridEM']=  Systematic("zjetsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.04),(1+0.04)],[(1-0.03),(1-0.04),(1-0.04)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR4JlowxhybridEM']=  Systematic("zjetsCKKWMatchTheo_R4JlowxEM",configMgr.weights,[(1+0.06),(1+0.09),(1+0.10)],[(1-0.06),(1-0.09),(1-0.10)], "user","userHistoSys")


ZjetsSystematics['zjetsGenTheo_VR4JlowxaplEM']= Systematic("zjetsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.16),(1+0.57),(1+0.38)],[(1-0.16),(1-0.57),(1-0.38)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR4JlowxaplEM'] = Systematic("zjetsFacTheo_R4JlowxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR4JlowxaplEM'] = Systematic("zjetsRenormTheo_R4JlowxEM",configMgr.weights,[(1+0.08),(1+0.09),(1+0.09)],[(1-0.08),(1-0.09),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR4JlowxaplEM']=  Systematic("zjetsResummationTheo_R4JlowxEM",configMgr.weights,[(1+0.03),(1+0.04),(1+0.04)],[(1-0.03),(1-0.04),(1-0.04)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR4JlowxaplEM']=  Systematic("zjetsCKKWMatchTheo_R4JlowxEM",configMgr.weights,[(1+0.06),(1+0.09),(1+0.10)],[(1-0.06),(1-0.09),(1-0.10)], "user","userHistoSys")





ZjetsSystematics['zjetsGenTheo_WR4JhighxEM']= Systematic("zjetsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.47),(1+0.51),(1+0.75)],[(1-0.47),(1-0.51),(1-0.75)], "user","userHistoSys")
ZjetsSystematics['zjetsGenTheo_TR4JhighxEM']= Systematic("zjetsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.47),(1+0.51),(1+0.75)],[(1-0.47),(1-0.51),(1-0.75)], "user","userHistoSys")

ZjetsSystematics['zjetsFacTheo_WR4JhighxEM'] = Systematic("zjetsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_TR4JhighxEM'] = Systematic("zjetsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")

ZjetsSystematics['zjetsRenormTheo_WR4JhighxEM'] = Systematic("zjetsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.10),(1+0.10),(1+0.09)],[(1-0.10),(1-0.10),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_TR4JhighxEM'] = Systematic("zjetsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.10),(1+0.10),(1+0.09)],[(1-0.10),(1-0.10),(1-0.09)], "user","userHistoSys")

ZjetsSystematics['zjetsResummationTheo_WR4JhighxEM'] = Systematic("zjetsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.03)],[(1-0.03),(1-0.05),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_TR4JhighxEM'] = Systematic("zjetsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.03)],[(1-0.03),(1-0.05),(1-0.03)], "user","userHistoSys")

ZjetsSystematics['zjetsCKKWMatchTheo_WR4JhighxEM'] = Systematic("zjetsCKKWMatchTheo_R4JhighxEM",configMgr.weights,[(1+0.05),(1+0.09),(1+0.09)],[(1-0.05),(1-0.09),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_TR4JhighxEM'] = Systematic("zjetsCKKWMatchTheo_R4JhighxEM",configMgr.weights,[(1+0.05),(1+0.09),(1+0.09)],[(1-0.05),(1-0.09),(1-0.09)], "user","userHistoSys")



ZjetsSystematics['zjetsGenTheo_VR4JhighxhybridEM']= Systematic("zjetsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.47),(1+0.51),(1+0.75)],[(1-0.47),(1-0.51),(1-0.75)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR4JhighxhybridEM'] = Systematic("zjetsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR4JhighxhybridEM'] = Systematic("zjetsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.10),(1+0.10),(1+0.09)],[(1-0.10),(1-0.10),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR4JhighxhybridEM']=  Systematic("zjetsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.03)],[(1-0.03),(1-0.05),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR4JhighxhybridEM']=  Systematic("zjetsCKKWMatchTheo_R4JhighxEM",configMgr.weights,[(1+0.05),(1+0.09),(1+0.09)],[(1-0.05),(1-0.09),(1-0.09)], "user","userHistoSys")


ZjetsSystematics['zjetsGenTheo_VR4JhighxaplEM']= Systematic("zjetsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.47),(1+0.51),(1+0.75)],[(1-0.47),(1-0.51),(1-0.75)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR4JhighxaplEM'] = Systematic("zjetsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR4JhighxaplEM'] = Systematic("zjetsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.10),(1+0.10),(1+0.09)],[(1-0.10),(1-0.10),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR4JhighxaplEM']=  Systematic("zjetsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.03)],[(1-0.03),(1-0.05),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR4JhighxaplEM']=  Systematic("zjetsCKKWMatchTheo_R4JhighxEM",configMgr.weights,[(1+0.05),(1+0.09),(1+0.09)],[(1-0.05),(1-0.09),(1-0.09)], "user","userHistoSys")


ZjetsSystematics['zjetsGenTheo_VR4JhighxmetEM']= Systematic("zjetsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.47),(1+0.51),(1+0.75)],[(1-0.47),(1-0.51),(1-0.75)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR4JhighxmetEM'] = Systematic("zjetsFacTheo_R4JhighxEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR4JhighxmetEM'] = Systematic("zjetsRenormTheo_R4JhighxEM",configMgr.weights,[(1+0.10),(1+0.10),(1+0.09)],[(1-0.10),(1-0.10),(1-0.09)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR4JhighxmetEM']=  Systematic("zjetsResummationTheo_R4JhighxEM",configMgr.weights,[(1+0.03),(1+0.05),(1+0.03)],[(1-0.03),(1-0.05),(1-0.03)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR4JhighxmetEM']=  Systematic("zjetsCKKWMatchTheo_R4JhighxEM",configMgr.weights,[(1+0.05),(1+0.09),(1+0.09)],[(1-0.05),(1-0.09),(1-0.09)], "user","userHistoSys")






ZjetsSystematics['zjetsGenTheo_WR6JEM']= Systematic("zjetsGenTheo_R6JEM", configMgr.weights,[(1+0.47),(1+0.31),(1+0.48),(1+0.38)],[(1-0.47),(1-0.31),(1-0.48),(1-0.38)], "user","userHistoSys")
ZjetsSystematics['zjetsGenTheo_TR6JEM']= Systematic("zjetsGenTheo_R6JEM", configMgr.weights,[(1+0.47),(1+0.31),(1+0.48),(1+0.38)],[(1-0.47),(1-0.31),(1-0.48),(1-0.38)], "user","userHistoSys")

ZjetsSystematics['zjetsFacTheo_WR6JEM'] = Systematic("zjetsFacTheo_R6JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_TR6JEM'] = Systematic("zjetsFacTheo_R6JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")

ZjetsSystematics['zjetsRenormTheo_WR6JEM'] = Systematic("zjetsRenormTheo_R6JEM",configMgr.weights,[(1+0.11),(1+0.10),(1+0.11),(1+0.10)],[(1-0.11),(1-0.10),(1-0.11),(1-0.10)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_TR6JEM'] = Systematic("zjetsRenormTheo_R6JEM",configMgr.weights,[(1+0.11),(1+0.10),(1+0.11),(1+0.10)],[(1-0.11),(1-0.10),(1-0.11),(1-0.10)], "user","userHistoSys")

ZjetsSystematics['zjetsResummationTheo_WR6JEM'] = Systematic("zjetsResummationTheo_R6JEM",configMgr.weights,[(1+0.04),(1+0.04),(1+0.04),(1+0.04)],[(1-0.04),(1-0.04),(1-0.04),(1-0.04)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_TR6JEM'] = Systematic("zjetsResummationTheo_R6JEM",configMgr.weights,[(1+0.04),(1+0.04),(1+0.04),(1+0.04)],[(1-0.04),(1-0.04),(1-0.04),(1-0.04)], "user","userHistoSys")

ZjetsSystematics['zjetsCKKWMatchTheo_WR6JEM'] = Systematic("zjetsCKKWMatchTheo_R6JEM",configMgr.weights,[(1+0.13),(1+0.14),(1+0.13),(1+0.13)],[(1-0.13),(1-0.14),(1-0.13),(1-0.13)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_TR6JEM'] = Systematic("zjetsCKKWMatchTheo_R6JEM",configMgr.weights,[(1+0.13),(1+0.14),(1+0.13),(1+0.13)],[(1-0.13),(1-0.14),(1-0.13),(1-0.13)], "user","userHistoSys")



ZjetsSystematics['zjetsGenTheo_VR6JmtEM']= Systematic("zjetsGenTheo_R6JEM", configMgr.weights,[(1+0.47),(1+0.31),(1+0.48),(1+0.38)],[(1-0.47),(1-0.31),(1-0.48),(1-0.38)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR6JmtEM'] = Systematic("zjetsFacTheo_R6JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR6JmtEM'] = Systematic("zjetsRenormTheo_R6JEM",configMgr.weights,[(1+0.11),(1+0.10),(1+0.11),(1+0.10)],[(1-0.11),(1-0.10),(1-0.11),(1-0.10)],"user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR6JmtEM']=  Systematic("zjetsResummationTheo_R6JEM",configMgr.weights,[(1+0.04),(1+0.04),(1+0.04),(1+0.04)],[(1-0.04),(1-0.04),(1-0.04),(1-0.04)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR6JmtEM']=  Systematic("zjetsCKKWMatchTheo_R6JEM",configMgr.weights,[(1+0.13),(1+0.14),(1+0.13),(1+0.13)],[(1-0.13),(1-0.14),(1-0.13),(1-0.13)], "user","userHistoSys")


ZjetsSystematics['zjetsGenTheo_VR6JaplEM']= Systematic("zjetsGenTheo_R6JEM", configMgr.weights,[(1+0.47),(1+0.31),(1+0.48),(1+0.38)],[(1-0.47),(1-0.31),(1-0.48),(1-0.38)], "user","userHistoSys")
ZjetsSystematics['zjetsFacTheo_VR6JaplEM'] = Systematic("zjetsFacTheo_R6JEM",configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
ZjetsSystematics['zjetsRenormTheo_VR6JaplEM'] = Systematic("zjetsRenormTheo_R6JEM",configMgr.weights,[(1+0.11),(1+0.10),(1+0.11),(1+0.10)],[(1-0.11),(1-0.10),(1-0.11),(1-0.10)], "user","userHistoSys")
ZjetsSystematics['zjetsResummationTheo_VR6JaplEM']=  Systematic("zjetsResummationTheo_R6JEM",configMgr.weights,[(1+0.04),(1+0.04),(1+0.04),(1+0.04)],[(1-0.04),(1-0.04),(1-0.04),(1-0.04)], "user","userHistoSys")
ZjetsSystematics['zjetsCKKWMatchTheo_VR6JaplEM']=  Systematic("zjetsCKKWMatchTheo_R6JEM",configMgr.weights,[(1+0.13),(1+0.14),(1+0.13),(1+0.13)],[(1-0.13),(1-0.14),(1-0.13),(1-0.13)], "user","userHistoSys")






 
for reg in Regions:

        ZjetsSystematics['zjetsGenTheo_SR2J'+reg] = Systematic("zjetsGenTheo_R2JEM", configMgr.weights,[(1+0.11),(1+0.31),(1+0.25),(1+0.49)],[(1-0.11),(1-0.31),(1-0.25),(1-0.49)], "user","userHistoSys")
        ZjetsSystematics['zjetsFacTheo_SR2J'+reg] = Systematic("zjetsFacTheo_R2JEM", configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
        ZjetsSystematics['zjetsRenormTheo_SR2J'+reg] = Systematic("zjetsRenormTheo_R2JEM", configMgr.weights,[(1+0.04),(1+0.05),(1+0.08),(1+0.01)],[(1-0.04),(1-0.05),(1-0.08),(1-0.01)], "user","userHistoSys")
        ZjetsSystematics['zjetsCKKWMatchTheo_SR2J'+reg] = Systematic("zjetsCKKWMatchTheo_R2JEM", configMgr.weights,[(1+0.02),(1+0.02),(1+0.08),(1+0.06)],[(1-0.02),(1-0.02),(1-0.08),(1-0.06)],"user","userHistoSys")
        ZjetsSystematics['zjetsResummationTheo_SR2J'+reg] = Systematic("zjetsResummationTheo_R2JEM", configMgr.weights,[(1+0.01),(1+0.02),(1+0.04),(1+0.03)],[(1-0.01),(1-0.02),(1-0.03),(1-0.03)], "user","userHistoSys")
	
        ZjetsSystematics['zjetsGenTheo_SR4Jlowx'+reg] = Systematic("zjetsGenTheo_R4JlowxEM", configMgr.weights,[(1+0.16),(1+0.57),(1+0.38)],[(1-0.16),(1-0.57),(1-0.38)], "user","userHistoSys")
        ZjetsSystematics['zjetsFacTheo_SR4Jlowx'+reg] = Systematic("zjetsFacTheo_R4JlowxEM", configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
        ZjetsSystematics['zjetsRenormTheo_SR4Jlowx'+reg] = Systematic("zjetsRenormTheo_R4JlowxEM", configMgr.weights,[(1+0.08),(1+0.09),(1+0.09)],[(1-0.08),(1-0.09),(1-0.09)], "user","userHistoSys")
        ZjetsSystematics['zjetsCKKWMatchTheo_SR4Jlowx'+reg] = Systematic("zjetsCKKWMatchTheo_R4JlowxEM", configMgr.weights,[(1+0.06),(1+0.09),(1+0.10)],[(1-0.06),(1-0.09),(1-0.10)], "user","userHistoSys")
        ZjetsSystematics['zjetsResummationTheo_SR4Jlowx'+reg] = Systematic("zjetsResummationTheo_R4JlowxEM", configMgr.weights,[(1+0.03),(1+0.04),(1+0.04)],[(1-0.03),(1-0.04),(1-0.04)], "user","userHistoSys")
	
        ZjetsSystematics['zjetsGenTheo_SR4Jhighx'+reg] = Systematic("zjetsGenTheo_R4JhighxEM", configMgr.weights,[(1+0.47),(1+0.51),(1+0.75)],[(1-0.47),(1-0.51),(1-0.75)], "user","userHistoSys")
        ZjetsSystematics['zjetsFacTheo_SR4Jhighx'+reg] = Systematic("zjetsFacTheo_R4JhighxEM", configMgr.weights,[(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
        ZjetsSystematics['zjetsRenormTheo_SR4Jhighx'+reg] = Systematic("zjetsRenormTheo_R4JhighxEM", configMgr.weights,[(1+0.10),(1+0.10),(1+0.09)],[(1-0.10),(1-0.10),(1-0.09)], "user","userHistoSys")
        ZjetsSystematics['zjetsCKKWMatchTheo_SR4Jhighx'+reg] = Systematic("zjetsCKKWMatchTheo_R4JhighxEM", configMgr.weights,[(1+0.05),(1+0.09),(1+0.09)],[(1-0.05),(1-0.09),(1-0.09)],"user","userHistoSys")
        ZjetsSystematics['zjetsResummationTheo_SR4Jhighx'+reg] = Systematic("zjetsResummationTheo_R4JhighxEM", configMgr.weights,[(1+0.03),(1+0.05),(1+0.03)],[(1-0.03),(1-0.05),(1-0.03)], "user","userHistoSys")
	
	
	
        ZjetsSystematics['zjetsGenTheo_SR6J'+reg] = Systematic("zjetsGenTheo_R6JEM", configMgr.weights,[(1+0.47),(1+0.31),(1+0.48),(1+0.38)],[(1-0.47),(1-0.31),(1-0.48),(1-0.38)], "user","userHistoSys")
        ZjetsSystematics['zjetsFacTheo_SR6J'+reg] = Systematic("zjetsFacTheo_R6JEM", configMgr.weights,[(1+0.01),(1+0.01),(1+0.01),(1+0.01)],[(1-0.01),(1-0.01),(1-0.01),(1-0.01)], "user","userHistoSys")
        ZjetsSystematics['zjetsRenormTheo_SR6J'+reg] = Systematic("zjetsRenormTheo_R6JEM", configMgr.weights,[(1+0.11),(1+0.10),(1+0.11),(1+0.10)],[(1-0.11),(1-0.10),(1-0.11),(1-0.10)], "user","userHistoSys")
        ZjetsSystematics['zjetsCKKWMatchTheo_SR6J'+reg] = Systematic("zjetsCKKWMatchTheo_R6JEM", configMgr.weights,[(1+0.13),(1+0.14),(1+0.13),(1+0.13)],[(1-0.13),(1-0.14),(1-0.13),(1-0.13)],"user","userHistoSys")
        ZjetsSystematics['zjetsResummationTheo_SR6J'+reg] = Systematic("zjetsResummationTheo_R6JEM", configMgr.weights,[(1+0.04),(1+0.04),(1+0.04),(1+0.04)],[(1-0.04),(1-0.04),(1-0.04),(1-0.04)], "user","userHistoSys")





def TheorUnc(generatorSyst):
           
    for key in ZjetsSystematics: 
        name=key.split('_') 
	  	
        generatorSyst.append((("zjets_Sherpa221",name[1]), ZjetsSystematics[key]))
	
    return generatorSyst	
	
	
 
