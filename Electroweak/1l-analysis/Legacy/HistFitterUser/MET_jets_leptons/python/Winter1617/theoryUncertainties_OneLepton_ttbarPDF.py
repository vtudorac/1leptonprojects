import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
    

Regions = [ 'BV', 'BT' ]
MeffBins = [ '_bin1EM', '_bin2EM', '_bin3EM', '_bin4EM']
TtbarPdfSystematics={}

for  bin in MeffBins :
   for	reg in Regions:
        TtbarPdfSystematics['TtbarPdfTheo_SR6J'+reg+bin] = Systematic("TtbarPdfTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.11, 1.-0.09 , "user","userOverallSys")
        #TtbarPdfSystematics['TtbarPdfTheo_SR2J'+reg+bin] = Systematic("TtbarPdfTheo_SR2J"+reg+bin, configMgr.weights, 1.+0.09, 1.-0.08 , "user","userOverallSys")
	
def TheorUnc(generatorSyst):
           
    for key in TtbarPdfSystematics: 
        name=key.split('_')
        name1=name[1]+"_"+name[2]	   	
        generatorSyst.append((("ttbar",name1), TtbarPdfSystematics[key]))
	
    return generatorSyst
