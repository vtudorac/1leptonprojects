import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr



Regions = [ 'BV', 'BT' ]
MeffBins = [ '_bin1EM', '_bin2EM', '_bin3EM', '_bin4EM']
WjetsPdfSystematics={}

for  bin in MeffBins :
   for	reg in Regions:
        WjetsPdfSystematics['WjetsPdfTheo_SR6J'+reg+bin] = Systematic("WjetsPdfTheo_SR6J"+reg+bin, configMgr.weights, 1.+0.14, 1.-0.07 , "user","userOverallSys")
        #WjetsPdfSystematics['WjetsPdfTheo_SR2J'+reg+bin] = Systematic("WjetsPdfTheo_SR2J"+reg+bin, configMgr.weights, 1.+0.09, 1.-0.02 , "user","userOverallSys")
	
def TheorUnc(generatorSyst):
           
    for key in WjetsPdfSystematics: 
        name=key.split('_')
        name1=name[1]+"_"+name[2]   	
        generatorSyst.append((("wjets_Sherpa221",name1), WjetsPdfSystematics[key]))

    return generatorSyst
