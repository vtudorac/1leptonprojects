"""
This config file implements the ABCD-like configuration for the
1-lepton multijet SR.

Usage:

* Run bkg only config

Histfitter.py [-t -w] -f -F bkg OneLepton_multijet.py

* Run bkg+signal model config and extract cls (use -z instead of -p for discovery p0)

Histfitter.py [-t -w] -p -f -F excl -g <sigpoint> OneLepton_multijet.py

* Run (background) histogram creating for one (or a list of )
  systematic(s) only (useful for parallelizing histogram creation)

Histfitter.py -c "filterSys='<systematic(s)>' -t -F bkg OneLepton_multijet.py

** Possible systematics:

JER, MET_SoftTrk, MET_SoftTrk_ResoPara, MET_SoftTrk_ResoPerp,
EG_RESOLUTION_ALL, EG_SCALE_ALL, MUONS_ID, MUONS_MS, MUONS_SCALE,
MUON_Eff_stat, MUON_Eff_sys, MUON_Eff_Iso_stat, MUON_Eff_Iso_sys,
MUON_Eff_stat_lowpt, MUON_Eff_sys_lowpt, EL_Eff_ID, EL_Eff_Reco,
EL_Eff_Iso, pileup, btag_BT, btag_CT, btag_LightT, tag_Extrapolation,
jvt, NOMINAL

and

JES_Group1, JES_Group2, JES_Group3

for the reduced JES set or

JES_BJES_Response, JES_EffectiveNP_1, JES_EffectiveNP_2,
JES_EffectiveNP_3, JES_EffectiveNP_4, JES_EffectiveNP_5,
JES__EffectiveNP_6restTerm, JES_EtaIntercalibration_Modelling,
JES_EtaIntercalibration_NonClosure, JES_EtaIntercalibration_TotalStat,
JES_Flavor_Composition, JES_Flavor_Response, JES_Pileup_OffsetMu,
JES_Pileup_OffsetNPV, JES_Pileup_PtTerm, JES_Pileup_RhoTopology,
JES_PunchThrough_MC15, JES_SingleParticle_HighPt,

for the full JES set

** Note: The histogram cache will always contain the NOMINAL
   histograms which need to be removed before merging all systematics

** For further options see the section about user defined arguments
"""

from math import sqrt
from copy import deepcopy
import json
import pprint
import os
import socket
import shutil
import re

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
import logging

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
import ROOT

configMgr.analysisName = "OneLepton_multijet"

# use custom logger for this config file
logger = logging.getLogger(configMgr.analysisName)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
formatter = logging.Formatter('\033[1;33m<%(levelname)s> %(name)s: %(message)s\033[0m')
ch.setFormatter(formatter)
logger.addHandler(ch)

# --------------------------------------------------
# user defined arguments per command line "-c"
# {{{ ----------------------------------------------

# this is for parallelizing histogram creation
# only run this systematics
if not "filterSys" in dir():
    filterSys = False
else:
    if not issubclass(type(filterSys), list):
        filterSys = [filterSys]

# Add validation region(s) to config - right now this will also add the VR CRs for the ABCD configuration
# Set by "-V"
if not "doValidation" in dir():
    doValidation = False

# Bin SRs in meff?
if not "doBinInMEFF" in dir():
    doBinInMEFF = True

# Bin-by-bin NFs for meff shape?
if not "doBinByBin" in dir():
    doBinByBin = False

# Use MistFitter to create backup cache file
# (ask me if you want to try this (nihartma@cern.ch)
if not "doMistFitter" in dir():
    doMistFitter = False

# Filter on tree names containing this string when using MistFitter
if not "grepTree" in dir():
    grepTree = None

# Require tree names to match this regex when using MistFitter
if not "matchTree" in dir():
    matchTree = None

# Remove bkgOnly config from fitconfig in the end (useful to save time
# if only running exclusion config)
if not "killBkgOnly" in dir():
    killBkgOnly = False

# Use cwd as PYTHONPATH (useful for copying module files like for theory systematics to HistFitter run dirs)
if not "useCWDPythonPath" in dir():
    useCWDPythonPath = True

if not "doOnlySignal" in dir():
    doOnlySignal = True

if useCWDPythonPath:
    sys.path = [os.getcwd()]+sys.path

# hard coded (overflow is used and added last bin)
meffBins = [2, 1000., 2000.] # means bins starting at 1000, 1500

# regions that are binned in meff (for regions containing "CR" - "WR" and "TR" will also be added)
# All those regions will carry the bin by bin NFs if requested
meffBinnedRegions = ["SR", "VR_tail_herself"]

# If doBinByBin is set this region will determine the bin-by-bin NFs
# (has to be in meffBinnedRegions)
meffConstrainRegions = ["CR_highjet_lowmt"]

logger.info("filterSys = {}".format(filterSys))
logger.info("doValidation = {}".format(doValidation))
logger.info("doBinInMEFF = {}".format(doBinInMEFF))
logger.info("doBinByBin = {}".format(doBinByBin))
logger.info("doMistFitter = {}".format(doMistFitter))
logger.info("grepTree = {}".format(grepTree))
logger.info("matchTree = {}".format(matchTree))
logger.info("killBkgOnly = {}".format(killBkgOnly))

# }}} ----------------------------------------------
# custom options for this config file
# {{{ ----------------------------------------------

# using of statistical uncertainties?
useStat=True

# use skimmed bkg trees? -> for T_06_10 there are only skimmed trees ;)
useSkimmedTrees = True

# use HT filtered ttbar samples?
useHTFilteredTTBAR = True
#useHTFilteredTTBAR = False

treePathScratch = None
HOST = socket.gethostname()
logger.info("Host: {}".format(HOST))
if "lxe31" in HOST or os.getenv("BATCHSYSTEM") == "LRZ":
    treePath = os.path.expandvars("$PTMP/trees")
elif "gar-ws" in HOST or os.getenv("BATCHSYSTEM") == "ETP":
    #treePathScratch = "/scratch-local/nhartmann/trees"
    treePath = "/project/etp5/SUSYInclusive/trees/T_06_10"

# signal sample input trees
sigtrees = [os.path.join(treePath, "allTrees_T_06_10_signal.root")]

# signal point given with "-g"
sigpoint = None
if "sigSamples" in dir():
    sigpoint = sigSamples[0]

# background sample input trees -> fill afterwards
bkgtrees = {
    "diboson_Sherpa221" : [],
    "singletop" : [],
    "ttbar" : [],
    "ttv" : [],
    "wjets_Sherpa221" : [],
    "zjets_Sherpa221" : [],
}

if useSkimmedTrees:
    for treeName in bkgtrees:
        if treePathScratch:
            bkgtrees[treeName] = [os.path.join(treePathScratch, "allTrees_T_06_10_bkg.root")]
        else:
            bkgtrees[treeName] = [os.path.join(treePath, "allTrees_T_06_10_bkg.root")]

# Not nescessary anymore - are included in the merged ones for 06_10
# if useHTFilteredTTBAR:
#     #pass
#     bkgtrees["ttbar"] = [os.path.join(treePath, "allTrees_T_06_09_ttbarht.root")]

datatrees = [os.path.join(treePath, "allTrees_T_06_10_bkg.root")]

# activate/deactivate certain types of systematics
doTreeSystematics = True # tree based detector sys
doWeightSystamatics = True # weight based detector sys
doBTagSystematics = True # b-tagging systematics - applied in b-tag and veto regions

# TODO: -> not nescessary anymore! HistFitter can do this better now
# with the "prefix" option
wjets = "wjets_Sherpa221"
zjets = "zjets_Sherpa221"
diboson = "diboson_Sherpa221"

# use full jes set?
useFullJESSet = True

logger.info("useStat = {}".format(useStat))
logger.info("sigtrees = {}".format(sigtrees))
logger.info("sigpoint = {}".format(sigpoint))
logger.info("bkgtrees = {}".format(bkgtrees))
logger.info("doTreeSystematics = {}".format(doTreeSystematics))
logger.info("doWeightSystamatics = {}".format(doWeightSystamatics))
logger.info("doBTagSystematics = {}".format(doBTagSystematics))

def checkRootFileExists(path):
    f = ROOT.TFile.Open(path)
    if f == None:
        logger.error(path+" doesn't exists")
        raise IOError
    f.Close()

#for path in sigtrees+[x[0] for x in bkgtrees.values()]:
for path in [x[0] for x in bkgtrees.values()]:
    checkRootFileExists(path)

# }}} ----------------------------------------------
# Parameters for hypothesis test and configMgr options
# {{{ ----------------------------------------------

#configMgr.doHypoTest=False
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=20
#configMgr.scanRange = (0., 2.)

# toy configuration
# configMgr.nTOYs=1000
# configMgr.calculatorType=0

# smaller correlation matrix
configMgr.ReduceCorrMatrix = True

# for running +/-1 sigma observed
configMgr.fixSigXSec = True

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 # Luminosity of input TTree after weighting
#configMgr.outputLumi = 3.2 # Luminosity required for output histograms
configMgr.outputLumi = 3.21296+32.8616-0.0086 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")

configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

# backup cache
if os.path.exists("data/"+configMgr.analysisName+"_backup.root"):
    logger.info("Found backup cache file data/"+configMgr.analysisName+"_backup.root")
    configMgr.useCacheToTreeFallback = True # enable the fallback to trees
    configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
    configMgr.histBackupCacheFile = "data/"+configMgr.analysisName+"_backup.root"
else:
    logger.info("Didn't find a backup cache file - won't use any")

# writing xml files for debugging purposes
configMgr.writeXML = True

# copy over HistFactorySchema if not existing
if configMgr.writeXML and not os.path.exists("config/HistFactorySchema.dtd"):
    logger.info("Copying HistFactorySchema.dtd")
    if not os.getenv("HF"):
        raise Exception("Environment variable 'HF' not set - don't know where to copy HistFactorySchema from ...")
    if not os.path.exists("config"):
        os.mkdir("config")
    shutil.copyfile(os.path.join(os.getenv("HF"), "config/HistFactorySchema.dtd"), "config/HistFactorySchema.dtd")

# blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)

# don't blind if we want to do fake data
if "buildFakeData" in dir():
    configMgr.blindSR = False
    configMgr.blindCR = True
    configMgr.blindVR = False


# convenient for debugging:
# use signal in blinded data for discovery p0, but not for exclusion cls
if doDiscoveryHypoTests: # set by "-z"
    configMgr.useSignalInBlindedData = True
else:
    configMgr.useSignalInBlindedData = False

# }}} ----------------------------------------------
# Cuts and region definitions
# {{{ ----------------------------------------------

preselection = "&&trigMatch_metTrig&&nLep_signal==1&&nLep_base==1&&lep1Pt>35&&(met/sqrt(Ht30))>8&&meffInc30>1000&&met>200"

# Should work with 06-03
pileupWeight = "pileupWeight"
pileupWeightUp = "pileupWeightUp"
pileupWeightDown = "pileupWeightDown"

# Region B (SR)
configMgr.cutsDict["SR"] = "mt>175&&LepAplanarity>0.07&&nJet30>=9" + preselection

# C
configMgr.cutsDict["CR_lowjet_lowmt"] = "mt<100&&nJet30>=5&&nJet30<=6" + preselection
# A
configMgr.cutsDict["CR_lowjet_highmt"] = "mt>175&&nJet30>=5&&nJet30<=6" + preselection
# D
configMgr.cutsDict["CR_highjet_lowmt"] = "mt<100&&LepAplanarity>0.07&&nJet30>=9" + preselection
# Additional region like D, but with Aplanarity inverted
configMgr.cutsDict["CR_highjet_lowapl"] = "mt<100&&LepAplanarity<0.07&&nJet30>=9" + preselection

bReq = "&&nBJet30_MV2c10>0"
configMgr.cutsDict["TR_lowjet_lowmt"] = configMgr.cutsDict["CR_lowjet_lowmt"] + bReq
configMgr.cutsDict["TR_lowjet_highmt"] = configMgr.cutsDict["CR_lowjet_highmt"] + bReq
configMgr.cutsDict["TR_highjet_lowmt"] = configMgr.cutsDict["CR_highjet_lowmt"] + bReq
configMgr.cutsDict["TR_highjet_lowapl"] = configMgr.cutsDict["CR_highjet_lowapl"] + bReq

bVeto = "&&nBJet30_MV2c10==0"
configMgr.cutsDict["WR_lowjet_lowmt"] = configMgr.cutsDict["CR_lowjet_lowmt"] + bVeto
configMgr.cutsDict["WR_lowjet_highmt"] = configMgr.cutsDict["CR_lowjet_highmt"] + bVeto
configMgr.cutsDict["WR_highjet_lowmt"] = configMgr.cutsDict["CR_highjet_lowmt"] + bVeto
configMgr.cutsDict["WR_highjet_lowapl"] = configMgr.cutsDict["CR_highjet_lowapl"] + bVeto

# VRs (and VR CRs) Note: the "_herself" is important to have region names that are not contained in other region names (for the YieldsTable scripts)
configMgr.cutsDict["VR_tail_herself"] = "mt>175&&LepAplanarity<0.05&&nJet30>=7&&nJet30<=8" + preselection
configMgr.cutsDict["VR_tail_CR"] = "mt<100&&LepAplanarity<0.05&&nJet30>=7&&nJet30<=8" + preselection
configMgr.cutsDict["VR_tail_TR"] = configMgr.cutsDict["VR_tail_CR"] + bReq
configMgr.cutsDict["VR_tail_WR"] = configMgr.cutsDict["VR_tail_CR"] + bVeto
configMgr.cutsDict["VR_jet_herself"] = "mt>100&&mt<175&&LepAplanarity<0.05&&nJet30>=9" + preselection
configMgr.cutsDict["VR_jet_CR"] = "mt>100&&mt<175&&nJet30>=5&&nJet30<=6" + preselection
configMgr.cutsDict["VR_jet_TR"] = configMgr.cutsDict["VR_jet_CR"] + bReq
configMgr.cutsDict["VR_jet_WR"] = configMgr.cutsDict["VR_jet_CR"] + bVeto
configMgr.cutsDict["VR_waldo"] = "mt>100&&mt<175&&LepAplanarity<0.05&&nJet30>=7&&nJet30<=8" + preselection

# for meff validation in high Aplanarity (same as 9J CR)
configMgr.cutsDict["VR_highjet_highapl"] = configMgr.cutsDict["CR_highjet_lowmt"]

# for validation of Aplanarity shape in 9J CR
configMgr.cutsDict["VR_highjet_incl"] = "mt<100&&nJet30>=9" + preselection

for region in list(meffBinnedRegions):
    if "CR" in region:
        meffBinnedRegions.append(region.replace("CR", "WR"))
        meffBinnedRegions.append(region.replace("CR", "TR"))
for region in list(meffConstrainRegions):
    meffConstrainRegions.append(region.replace("CR", "WR"))
    meffConstrainRegions.append(region.replace("CR", "TR"))

logger.debug(meffBinnedRegions)

# add the meff bins in case bin-by-bin is requested
if doBinByBin:
    binBoundaries = [i for i in range(int(meffBins[1]), int(meffBins[2]),
                                      int(float(meffBins[2] - meffBins[1]) / float(meffBins[0])))]
    logger.debug("Bin boundaries: {}".format(binBoundaries))
    for region in meffBinnedRegions:
        for i, binBoundary in enumerate(binBoundaries):
            # include overflow to last bin
            if len(binBoundaries) > i+1:
                upperCut = "&&meffInc30<={}".format(binBoundaries[i+1])
            else:
                upperCut = ""
            lowerCut = "&&meffInc30>{}".format(binBoundary)
            configMgr.cutsDict[region+"_meff{}".format(i)] = configMgr.cutsDict[region].replace("&&meffInc30>1000","") + lowerCut + upperCut

logger.debug("Final cuts dict: {}".format(pprint.pformat(configMgr.cutsDict)))

# For now everything that dosen't have a "TR" or "WR" in its name will go to b-agnostic channels
bAgnosticChannels = set()
for region in configMgr.cutsDict:
    if not ("TR" in region or "WR" in region):
        bAgnosticChannels.add(region)

# Note: the bTagWeight will be removed from b-agnostic channels
configMgr.weights = ["genWeight","eventWeight","leptonWeight",pileupWeight,"bTagWeight","SherpaVjetsNjetsWeight","jvtWeight"]

# }}} --------------------------------------------------
# Normalisation factor configuration
# {{{ --------------------------------------------------

# configure which NFs are applied where - this defines also which regions participate in the fit

# CRs (will be copied to TR/WR)
nfSignaturesCR = {
    "CR_lowjet_lowmt" : {
        wjets : ["mu_56j_w"],
        "ttbar" : ["mu_56j_top"],
        "singletop" : ["mu_56j_top"],
    },
    "CR_lowjet_highmt" : {
        wjets : ["mu_56j_w", "mu_r_tail_w"],
        "ttbar" : ["mu_56j_top", "mu_r_tail_top"],
        "singletop" : ["mu_56j_top", "mu_r_tail_top"],
    },
    "CR_highjet_lowmt" : {
        wjets : ["mu_9j_w"],
        "ttbar" : ["mu_9j_top"],
        "singletop" : ["mu_9j_top"],
    },
}
if doBinByBin:
    # will be filled later
    nfSignaturesCR[meffConstrainRegions[0]] =  {
        wjets : [],
        "ttbar" : [],
        "singletop" : [],
    }

nfSignaturesSR = {
    "SR" : {
         wjets : ["mu_9j_w", "mu_r_tail_w"],
        #wjets : ["mu_r_tail_w"],
        "ttbar" : ["mu_9j_top", "mu_r_tail_top"],
        "singletop" : ["mu_9j_top", "mu_r_tail_top"],
    },
}

nfSignaturesVR = {
    "VR_tail_herself" : {
        wjets : ["mu_78j_w", "mu_r_tail_w"],
        "ttbar" : ["mu_78j_top", "mu_r_tail_top"],
        "singletop" : ["mu_78j_top", "mu_r_tail_top"],
    },
    "VR_jet_herself" : {
        wjets : ["mu_9j_w", "mu_r_tail_vr_w"],
        #wjets : ["mu_r_tail_vr_w"],
        "ttbar" : ["mu_9j_top", "mu_r_tail_vr_top"],
        "singletop" : ["mu_9j_top", "mu_r_tail_vr_top"],
    },
    "VR_waldo" : {
        wjets : ["mu_78j_w", "mu_r_tail_vr_w"],
        "ttbar" : ["mu_78j_top", "mu_r_tail_vr_top"],
        "singletop" : ["mu_78j_top", "mu_r_tail_vr_top"],
    },
}

# VR CRs (will be copied to TR/WR)
nfSignaturesVRCR = {
    "VR_tail_CR" : { # this region is meant to determine the nf for vr_tail
        wjets : ["mu_78j_w"],
        "ttbar" : ["mu_78j_top"],
        "singletop" : ["mu_78j_top"],
    },
    "VR_jet_CR" : { # this region is meant to determine r_tail for vr_jet
        wjets : ["mu_56j_w", "mu_r_tail_vr_w"],
        "ttbar" : ["mu_56j_top", "mu_r_tail_vr_top"],
        "singletop" : ["mu_56j_top", "mu_r_tail_vr_top"],
    },
}

# kill VR dicts if not doing validation
if not doValidation:
    nfSignaturesVRCR = {}
    nfSignaturesVR = {}

# fill the dictionary that will be used finally (copy CR configs to TR/WR and create bin-by-bin NFs if requested)
nfSignaturesWRTR = {}
for CRtype in ["TR", "WR"]:
    for CRname, nfSignature in nfSignaturesCR.items()+nfSignaturesVRCR.items():
        nfSignaturesWRTR[CRname.replace("CR", CRtype)] = nfSignature


# this is the final dictionary
nfSignaturesRegions = {}
for regionName, nfSignature in nfSignaturesWRTR.items()+nfSignaturesSR.items()+nfSignaturesVR.items():
    if doBinByBin and regionName in meffBinnedRegions:
        for i in range(meffBins[0]):
            nfSignatureBin = deepcopy(nfSignature)
            nfSignatureBin[wjets].append("mu_meff{}_w".format(i))
            nfSignatureBin["ttbar"].append("mu_meff{}_top".format(i))
            nfSignatureBin["singletop"].append("mu_meff{}_top".format(i))
            if regionName in meffConstrainRegions:
                nfSignatureBin[wjets].append("mu_9j_w")
                nfSignatureBin["ttbar"].append("mu_9j_top")
                nfSignatureBin["singletop"].append("mu_9j_top")
            logger.debug("Adding NF signature to region {}: {}".format(regionName+"_meff{}".format(i), nfSignatureBin))
            nfSignaturesRegions[regionName+"_meff{}".format(i)] = nfSignatureBin
    else:
        nfSignaturesRegions[regionName] = nfSignature

if doOnlySignal:
    nfSignaturesRegions = {"SR" : {}}

# }}} ----------------------------------------------------------
# Detector Systematics
# {{{ ----------------------------------------------

# name of nominal histogram for systematics
configMgr.nomName = "_NoSys"

# systematic weight definitions
def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList
weights = configMgr.weights
# muons
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")
muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")
muonTTVAHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1up")
muonTTVALowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1down")
muonTTVAHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1up")
muonTTVALowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1down")
# electrons
# electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TotalCorrUncertainty__1up")
# electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TotalCorrUncertainty__1down")
# electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TotalCorrUncertainty__1up")
# electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TotalCorrUncertainty__1down")
# electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TotalCorrUncertainty__1up")
# electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TotalCorrUncertainty__1down")
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")
# pileup
pileupup = replaceWeight(weights, pileupWeight, pileupWeightUp)
pileupdown = replaceWeight(weights, pileupWeight, pileupWeightDown)
# btagging
bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")
cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")
mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")
# jvt
JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1up")
JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1down")

# tree based detector systematics
treeSystematicNames = {
    "JER" : ("_JET_JER_SINGLE_NP__1up", "_NoSys", "overallNormHistoSysOneSide"),
    "MET_SoftTrk" : ("_MET_SoftTrk_ScaleUp", "_MET_SoftTrk_ScaleDown", "overallNormHistoSys"),
    "MET_SoftTrk_ResoPara" : ("_MET_SoftTrk_ResoPara", "_NoSys", "overallNormHistoSysOneSide"),
    "MET_SoftTrk_ResoPerp" : ("_MET_SoftTrk_ResoPerp", "_NoSys", "overallNormHistoSysOneSide"),
    "EG_RESOLUTION_ALL" : ("_EG_RESOLUTION_ALL__1up", "_EG_RESOLUTION_ALL__1down", "overallNormHistoSys"),
    "EG_SCALE_ALL" : ("_EG_SCALE_ALL__1up", "_EG_SCALE_ALL__1down", "overallNormHistoSys"),
    "MUON_ID" : ("_MUON_ID__1up", "_MUON_ID__1down", "overallNormHistoSys"),
    "MUON_MS" : ("_MUON_MS__1up", "_MUON_MS__1down", "overallNormHistoSys"),
    "MUON_SCALE" : ("_MUON_SCALE__1up", "_MUON_SCALE__1down", "overallNormHistoSys"),
}
jesReducedSystematicNames = {
    "JES_Group1" : ("_JET_GroupedNP_1__1up", "_JET_GroupedNP_1__1down", "overallNormHistoSys"),
    "JES_Group2" : ("_JET_GroupedNP_2__1up", "_JET_GroupedNP_2__1down", "overallNormHistoSys"),
    "JES_Group3" : ("_JET_GroupedNP_3__1up", "_JET_GroupedNP_3__1down", "overallNormHistoSys"),
}
jesFullSystematicNames = {
    "JES_BJES_Response" : ("_JET_BJES_Response__1up", "_JET_BJES_Response__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_1" : ("_JET_EffectiveNP_1__1up", "_JET_EffectiveNP_1__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_2" : ("_JET_EffectiveNP_2__1up", "_JET_EffectiveNP_2__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_3" : ("_JET_EffectiveNP_3__1up", "_JET_EffectiveNP_3__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_4" : ("_JET_EffectiveNP_4__1up", "_JET_EffectiveNP_4__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_5" : ("_JET_EffectiveNP_5__1up", "_JET_EffectiveNP_5__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_6" : ("_JET_EffectiveNP_6__1up", "_JET_EffectiveNP_6__1down", "overallNormHistoSys"),
    "JES_EffectiveNP_7" : ("_JET_EffectiveNP_7__1up", "_JET_EffectiveNP_7__1down", "overallNormHistoSys"),
    "JES__EffectiveNP_8restTerm" : ("_JET_EffectiveNP_8restTerm__1up", "_JET_EffectiveNP_8restTerm__1down", "overallNormHistoSys"),
    "JES_EtaIntercalibration_Modelling" : ("_JET_EtaIntercalibration_Modelling__1up", "_JET_EtaIntercalibration_Modelling__1down", "overallNormHistoSys"),
    "JES_EtaIntercalibration_NonClosure" : ("_JET_EtaIntercalibration_NonClosure__1up", "_JET_EtaIntercalibration_NonClosure__1down", "overallNormHistoSys"),
    "JES_EtaIntercalibration_TotalStat" : ("_JET_EtaIntercalibration_TotalStat__1up", "_JET_EtaIntercalibration_TotalStat__1down", "overallNormHistoSys"),
    "JES_Flavor_Composition" : ("_JET_Flavor_Composition__1up", "_JET_Flavor_Composition__1down", "overallNormHistoSys"),
    "JES_Flavor_Response" : ("_JET_Flavor_Response__1up", "_JET_Flavor_Response__1down", "overallNormHistoSys"),
    "JES_Pileup_OffsetMu" : ("_JET_Pileup_OffsetMu__1up", "_JET_Pileup_OffsetMu__1down", "overallNormHistoSys"),
    "JES_Pileup_OffsetNPV" : ("_JET_Pileup_OffsetNPV__1up", "_JET_Pileup_OffsetNPV__1down", "overallNormHistoSys"),
    "JES_Pileup_PtTerm" : ("_JET_Pileup_PtTerm__1up", "_JET_Pileup_PtTerm__1down", "overallNormHistoSys"),
    "JES_Pileup_RhoTopology" : ("_JET_Pileup_RhoTopology__1up", "_JET_Pileup_RhoTopology__1down", "overallNormHistoSys"),
    "JES_PunchThrough_MC15" : ("_JET_PunchThrough_MC15__1up", "_JET_PunchThrough_MC15__1down", "overallNormHistoSys"),
    "JES_SingleParticle_HighPt" : ("_JET_SingleParticle_HighPt__1up", "_JET_SingleParticle_HighPt__1down", "overallNormHistoSys"),
}
if useFullJESSet:
    treeSystematicNames.update(jesFullSystematicNames)
else:
    treeSystematicNames.update(jesReducedSystematicNames)
# will be used for AFII samples (currently just all signal samples)
AFIISystematicNames = {
    "JES_PunchThrough_AFII" : ("_JET_PunchThrough_AFII__1up", "_JET_PunchThrough_AFII__1down", "overallNormHistoSys"),
    "JES_RelativeNonClosure_AFII" : ("_JET_RelativeNonClosure_AFII__1up", "_JET_RelativeNonClosure_AFII__1down", "overallNormHistoSys"),
}
# will be removed from AFII samples (currently just all signal samples)
AFIIIgnoreSystematics = [
    "JES_PunchThrough_MC15"
]

# none for testing
if not doTreeSystematics or doOnlySignal:
    treeSystematicNames = {}
    AFIISystematicNames = {}
    AFIIIgnoreSystematics = []

treeSystematics = {}
treeSystematicsAFII = {}
for theTreeSystematicNames, theTreeSystematics in [(treeSystematicNames, treeSystematics),
                                                   (AFIISystematicNames, treeSystematicsAFII)]:
    for sysname, (up, down, systype) in theTreeSystematicNames.items():
        theTreeSystematics[sysname] = Systematic(sysname, "_NoSys", up, down, "tree", systype)


def fixAFIISystematics(fitConfig, sampleName):
    for chan in fitConfig.channels:
        sigSample = chan.getSample(sampleName)
        for systematicName in AFIIIgnoreSystematics:
            if filterSys and not systematicName in filterSys:
                continue
            logger.warn("Removing systematic {} for sample {}!".format(systematicName, sampleName))
            sigSample.removeSystematic(systematicName)
        for systematic in treeSystematicsAFII.values():
            if filterSys and not systematic in filterSys:
                continue
            logger.warn("Adding systematic {} for {}".format(systematic.name, sampleName))
            sigSample.addSystematic(systematic)


# weight based detector systematics
weightSystematics = [
    Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_TTVA_stat",configMgr.weights,muonTTVAHighWeights_stat,muonTTVALowWeights_stat,"weight","overallNormHistoSys"),
    Systematic("MUON_Eff_TTVA_sys",configMgr.weights,muonTTVAHighWeights_sys,muonTTVALowWeights_sys,"weight","overallNormHistoSys"),
    Systematic("EL_Eff_ID",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys"),
    Systematic("EL_Eff_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys"),
    Systematic("EL_Eff_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys"),
    Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"),
    Systematic("jvt",configMgr.weights,JVTHighWeights,JVTLowWeights,"weight","overallNormHistoSys"),
]
# none for testing
if not doWeightSystamatics or doOnlySignal:
    weightSystematics = []

# b-tagging systematics
btagSystematics = [
    Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys"),
    Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys"),
    Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys"),
    Systematic("tag_Extrapolation",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys"),
]
# none for testing
if not doBTagSystematics or doOnlySignal:
    btagSystematics = []

# only use the given systematics if set
if filterSys:
    for sysname, systematic in treeSystematics.items():
        if not sysname in filterSys:
            treeSystematics.pop(sysname)

    _weightSystematics = list(weightSystematics)
    weightSystematics = []
    for systematic in _weightSystematics:
        if systematic.name in filterSys:
            weightSystematics.append(systematic)

    _btagSystematics = list(btagSystematics)
    btagSystematics = []
    for systematic in _btagSystematics:
        if systematic.name in filterSys:
            btagSystematics.append(systematic)


# }}} --------------------------------------------
# Theory uncertainties
# {{{ ----------------------------------------------

# this will contain all theory uncertainties
# regionName -> sampleName -> systematic
theorySys = {}

def dummysys(sysname, relUnc):
    return Systematic(sysname, configMgr.weights, 1+relUnc, 1-relUnc, "user", "userOverallSys")

from theoryUncertainties_OneLepton_multijet_Ttbar import TtbarSystematicDict
from theoryUncertainties_OneLepton_multijet_Diboson import DibosonSystematicDict

for regionName, nfSignaturesSamples in nfSignaturesRegions.items():

    theorySys[regionName] = {}

    # Samples that carry NFs get uncertainties on closure/TF (only applied in SR/VR)
    for sampleName in nfSignaturesSamples.keys():

        # ttbar has already configured values
        if sampleName == "ttbar":
            if regionName in TtbarSystematicDict:
                theorySys[regionName]["ttbar"] = TtbarSystematicDict[regionName].values()
        else:
            # Other samples only uncertainties in SR/VRs (singletop, wjets)
            if "CR" in regionName or "TR" in regionName or "WR" in regionName:
                continue
            if not ("SR" in regionName or "VR" in regionName):
                continue
            theorySys[regionName][sampleName] = syslist = []
            if sampleName == "singletop":
                syslist.append(dummysys("dummy_{}".format(sampleName), 0.8))
                logger.warn("Using flat 80% uncertainty for {} in region {}".format(sampleName, regionName))
            else:
                syslist.append(dummysys("dummy_{}".format(sampleName), 0.5))
                logger.warn("Using flat 50% uncertainty for {} in region {}".format(sampleName, regionName))

    # The rest go to all regions

    # Diboson exists
    if regionName in DibosonSystematicDict:
        theorySys[regionName][diboson] = DibosonSystematicDict[regionName].values()

    # The rest has flat 50%
    for sampleName in ["ttv", zjets]:
        theorySys[regionName][sampleName] = syslist = []
        syslist.append(dummysys("dummy_{}".format(sampleName), 0.5))
        logger.warn("Using flat 50% uncertainty for {} in region {}".format(sampleName, regionName))

# hack for printing the user sys
from systematic import UserSystematic
UserSystematic.__repr__ = lambda sys : "{}: low {} high {}".format(sys.name, sys.low, sys.high)
logger.debug("Theory systematics configuration:\n{}".format(pprint.pformat(theorySys)))

if doOnlySignal:
    theorySys = {}

# }}} --------------------------------------------
# Samples
# {{{ ----------------------------------------------

#
DibosonsSampleName = diboson
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
#
ZSampleName = zjets
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()

# ttbar, wjets and singletop for simple CR config
WSampleName = wjets
WSample = Sample(WSampleName,kAzure-4)
WSample.setStatConfig(useStat)
#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
TTbarSample.setStatConfig(useStat)
if useHTFilteredTTBAR:
    TTbarSample.setPrefixTreeName("ttbarHt")
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setStatConfig(useStat)


for sample in [DibosonsSample, ttbarVSample, ZSample, WSample, TTbarSample, SingleTopSample]:
    sample.setFileList(bkgtrees[sample.name])

dataSample = Sample("data",kBlack)
dataSample.setData()
dataSample.setFileList(datatrees)
#logger.warn("Data file list commented to avoid accidentially unblinding")

if doOnlySignal:
    sigSample = Sample(sigpoint, kPink)

    sigSample.setNormByTheory()

    sigSample.setNormFactor("mu_sig",1.,0.,5.)
    sigSample.setFileList(sigtrees)
    sigSample.setStatConfig(useStat)

# }}} ----------------------------------------------
# bkgonly fit config without signal sample
# {{{ ----------------------------------------------

logger.info("Creating bkgonly config for ABCD method")

bkgonly = configMgr.addFitConfig("bkgonly")
if doOnlySignal:
    bkgonly.addSamples([dataSample, sigSample])
else:
    bkgonly.addSamples([dataSample, WSample, TTbarSample, SingleTopSample, DibosonsSample, ZSample, ttbarVSample])

# TODO: lumi error?
meas = bkgonly.addMeasurement(name="NormalMeasurement", lumi=1.0, lumiErr=0.032)
meas.addPOI("mu_sig")
meas.addParamSetting("Lumi", True, 1)

# create the control region samples
regions = []
for region, sampleNFDict in nfSignaturesRegions.items():
    logger.debug("Adding region {}".format(region))
    if (doBinInMEFF) and (region in meffBinnedRegions) and (not doBinByBin):
        r = bkgonly.addChannel("meffInc30", [region], *meffBins)
        r.useOverflowBin=True
    else:
        r = bkgonly.addChannel("cuts", [region], 1., 0.5, 1.5)
    for sampleName, NFList in sampleNFDict.items():
        for NFName in NFList:
            logger.debug("adding NF {} for {} in region {}".format(NFName, sampleName, r.name))
            r.getSample(sampleName).addNormFactor(NFName, 1., 5., 0.)
    if "WR" in region or "TR" in region or "CR" in region:
        logger.debug("Setting region {} as a background constrain channel".format(region))
        bkgonly.setBkgConstrainChannels(r)
    elif "SR" in region:
        if not myFitType == FitType.Exclusion:
            logger.debug("Setting region {} as a validation region".format(region))
            bkgonly.setValidationChannels(r)
        else:
            logger.debug("Setting region {} as a signal region".format(region))
            bkgonly.setSignalChannels(r)
    elif "VR" in region:
        logger.debug("Setting region {} as a validation region".format(region))
        bkgonly.setValidationChannels(r)
    else:
        raise ValueError("can't figure out what to do with region {}".format(region))
    if region == "SR":
        sr = r # will be used later
    regions.append(r)
logger.debug("Created the Regions: {}".format([_r.name for _r in regions]))

# ensure the SR is blinded when blindSR is set (even if it is a VR)
if configMgr.blindSR:
    sr.blind = True

for region in regions:
    for sys in treeSystematics.values():
        region.addSystematic(sys)
    for sys in weightSystematics:
        region.addSystematic(sys)
    if not region.regions[0] in bAgnosticChannels:
        logger.debug("region {} not in bAgnosticChannels - adding b-tagging systematics".format(region.name))
        for sys in btagSystematics:
            region.addSystematic(sys)
    else:
        logger.debug("region {} in bAgnosticChannels - removing b-tagging weights".format(region.name))
        region.removeWeight("bTagWeight")

# add theory systematics
for regionName, sampleSystematics in theorySys.items():
    for sampleName, systematicList in sampleSystematics.items():
        for systematic in systematicList:
            logger.debug("Adding theory systematic {} for sample {} to region {}".format(systematic.name, sampleName, regionName))
            if (doBinInMEFF) and (regionName in meffBinnedRegions) and (not doBinByBin):
                region = bkgonly.getChannelByName("meffInc30_"+regionName)
            else:
                region = bkgonly.getChannelByName("cuts_"+regionName)
            region.getSample(sampleName).addSystematic(systematic)

# }}} ----------------------------------------------
# "exclusion" fit config with signal sample
# {{{ ----------------------------------------------

if myFitType == FitType.Exclusion:

    logger.info("Creating \"exclusion\" config with signal sample {} for ABCD method".format(sigpoint))

    exclusion = configMgr.addFitConfigClone(bkgonly, "Sig_{}".format(sigpoint))

    sigSample = Sample(sigpoint, kPink)

    sigSample.setNormByTheory()

    sigSample.setNormFactor("mu_sig",1.,0.,5.)
    sigSample.setFileList(sigtrees)
    sigSample.setStatConfig(useStat)

    # Signal xsec uncertainty for +/-1 sigma band obs
    xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
    xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
    sigSample.addSystematic(xsecSig)

    exclusion.addSamples([sigSample])
    exclusion.setSignalSample(sigSample)

    if "2step" in sigpoint:
        # signal uncertainty for 2step grid
        import Winter1617.SignalAccUncertainties
        errsig = Winter1617.SignalAccUncertainties.GetUncertaintyFromName("SR9J", "Sig_{}".format(sigpoint))
        logger.info("Signal uncertainty: {}".format(errsig))
        theoSig = Systematic("SigTheo", configMgr.weights, 1.00+errsig, 1.00-errsig, "user", "userOverallSys")
        if doBinInMEFF:
            sr = exclusion.getChannel("meffInc30", ["SR"])
        else:
            sr = exclusion.getChannel("cuts", ["SR"])
        sr.getSample(sigpoint).addSystematic(theoSig)

    # Note: systematics should be "trickled down" for signal sample

    # AFII special treatment (right now i assume all signal samples are AFII)
    fixAFIISystematics(exclusion, sigpoint)

# }}} ----------------------------------------------
# Afterburners - Ask me (nihartma@cern.ch) if you want to use these modules
# {{{ --------------------------------------------------

if "buildFakeData" in dir():
    try:
        import fakeData
    except:
        logger.warning("Module fakeData not found - can't use it if needed")

try:
    if buildFakeData == "MTSlope":
        fakeData.buildFakeHistos(dataSample, configMgr, bkgonly, "(mt*0.5/(175.)+1)")
    if buildFakeData == "MTNjetSlope":
        fakeData.buildFakeHistos(dataSample, configMgr, bkgonly, "((nJet30*0.25)*mt*0.5/(175.)+1)")
    if buildFakeData == "MEFFSlope":
        fakeData.buildFakeHistos(dataSample, configMgr, bkgonly, "(1-meffInc30*0.5/2000.)")
    if buildFakeData == "METSlope":
        fakeData.buildFakeHistos(dataSample, configMgr, bkgonly, "(met*0.5/1000.+1)")
except NameError:
    pass

if doMistFitter:
    from mistFitter import MistFitter
    mf = MistFitter(configMgr)
    mf.saveHists(os.path.join("data", configMgr.analysisName+"_backup.root"), grepTree=grepTree, matchTree=matchTree)
    configMgr.useCacheToTreeFallback = True # enable the fallback to trees
    configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
    configMgr.histBackupCacheFile = "data/"+configMgr.analysisName+"_backup.root"

# }}} --------------------------------------------------
# Dump some commands and info to make it more comfortable to produce tables/debugplots etc afterwards
# {{{ --------------------------------------------------

# retrieve some information
channelList = []
if myFitType == FitType.Exclusion:
    channelList = [_channel.name.replace("cuts_","").replace("meffInc30_", "") for _channel in exclusion.channels]
    workspacename = "results/"+configMgr.analysisName+"/Sig_"+sigpoint+"_combined_NormalMeasurement_model_afterFit.root"
    samplestring = "{wjets},ttbar,singletop,ttv,{diboson},{zjets},".format(diboson=diboson, wjets=wjets, zjets=zjets)+sigpoint
else:
    channelList = [_channel.name.replace("cuts_","").replace("meffInc30_", "") for _channel in bkgonly.channels]
    workspacename = "results/"+configMgr.analysisName+"/bkgonly_combined_NormalMeasurement_model_afterFit.root"
    samplestring = "{wjets},ttbar,singletop,ttv,{diboson},{zjets}".format(diboson=diboson, wjets=wjets, zjets=zjets)

options = {
    "channelstring" : ",".join(channelList),
    "workspacename" : workspacename,
    "sigpoint" : sigpoint,
    "samplestring" : samplestring,
}

with open("commands.sh", "w") as of:
    of.write(r"YieldsTable.py -c {channelstring} -s {samplestring} -w {workspacename} -o YieldsTable.tex".format(**options)+"\n")
    of.write(r"SysTable.py -c {channelstring} -s {samplestring} -w {workspacename} -o SysTable.tex".format(**options)+"\n")
    of.write(r"python $HF/scripts/pull_maker.py -i log.txt -o pull"+"\n")

# dump the cutsDict to a json file
json.dump(configMgr.cutsDict, open("cutsDict.json", "w"))

# }}} --------------------------------------------------
# Post processing
# {{{ --------------------------------------------------

if killBkgOnly:
    configMgr.fitConfigs.remove(bkgonly)

# }}}
