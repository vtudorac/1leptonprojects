import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

"""
* MC stat error added quadratically to variation
* symmetrised (up-down)/(up+down)
* (var-nom)/nom for generator comparison
* sign taken into account
* All cuts except hard 1L, nJet dropped for evaluation
"""
DibosonSystematicValueDict = {
   'CR_highjet_lowmt' : {
      'DibosonsFacTheo'         : ([1+0.10], [1-0.10]),
      'DibosonsGenTheo'         : ([1-0.87], [1+0.87]),
      'DibosonsRenormTheo'      : ([1-0.45], [1+0.45]),
      'DibosonsResummationTheo' : ([1+0.11], [1-0.11]),
   },
   'CR_lowjet_highmt' : {
      'DibosonsFacTheo'         : ([1+0.01], [1-0.01]),
      'DibosonsGenTheo'         : ([1-0.45], [1+0.45]),
      'DibosonsRenormTheo'      : ([1-0.33], [1+0.33]),
      'DibosonsResummationTheo' : ([1+0.02], [1-0.02]),
   },
   'CR_lowjet_lowmt' : {
      'DibosonsFacTheo'         : ([1+0.01], [1-0.01]),
      'DibosonsGenTheo'         : ([1-0.45], [1+0.45]),
      'DibosonsRenormTheo'      : ([1-0.33], [1+0.33]),
      'DibosonsResummationTheo' : ([1+0.02], [1-0.02]),
   },
   'SR' : {
      'DibosonsFacTheo'         : ([1+0.10, 1+0.10], [1-0.10, 1-0.10]),
      'DibosonsGenTheo'         : ([1-0.87, 1-0.87], [1+0.87, 1+0.87]),
      'DibosonsRenormTheo'      : ([1-0.45, 1-0.45], [1+0.45, 1+0.45]),
      'DibosonsResummationTheo' : ([1+0.11, 1+0.11], [1-0.11, 1-0.11]),
   },
   'VR_tail_CR' : {
      'DibosonsFacTheo'         : ([1+0.05], [1-0.05]),
      'DibosonsGenTheo'         : ([1-0.76], [1+0.76]),
      'DibosonsRenormTheo'      : ([1-0.26], [1+0.26]),
      'DibosonsResummationTheo' : ([1+0.05], [1-0.05]),
   },
   'VR_tail_herself' : {
      'DibosonsFacTheo'         : ([1+0.05, 1+0.05], [1-0.05, 1-0.05]),
      'DibosonsGenTheo'         : ([1-0.76, 1-0.76], [1+0.76, 1+0.76]),
      'DibosonsRenormTheo'      : ([1-0.26, 1-0.26], [1+0.26, 1+0.26]),
      'DibosonsResummationTheo' : ([1+0.05, 1+0.05], [1-0.05, 1-0.05]),
   },
   'VR_jet_herself' : {
      'DibosonsFacTheo'         : ([1+0.10], [1-0.10]),
      'DibosonsGenTheo'         : ([1-0.87], [1+0.87]),
      'DibosonsRenormTheo'      : ([1-0.45], [1+0.45]),
      'DibosonsResummationTheo' : ([1+0.11], [1-0.11]),
   },
   'VR_jet_CR' : {
      'DibosonsFacTheo'         : ([1+0.01], [1-0.01]),
      'DibosonsGenTheo'         : ([1-0.45], [1+0.45]),
      'DibosonsRenormTheo'      : ([1-0.33], [1+0.33]),
      'DibosonsResummationTheo' : ([1+0.02], [1-0.02]),
   },
}

# Copy CR -> TR/WR and remove CR
for region in DibosonSystematicValueDict.copy():
   if "CR" in region:
      valueDict = DibosonSystematicValueDict.pop(region)
      DibosonSystematicValueDict[region.replace("CR", "WR")] = valueDict
      DibosonSystematicValueDict[region.replace("CR", "TR")] = valueDict

DibosonSystematicDict = {}
for region, valueDict in DibosonSystematicValueDict.items():
   for sysName, (binsDown, binsUp) in valueDict.items():
      if not region in DibosonSystematicDict:
         DibosonSystematicDict[region] = {}
      DibosonSystematicDict[region][sysName] = Systematic(sysName, configMgr.weights,
                                                          binsDown, binsUp,
                                                          "user", "userHistoSys")
