import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

"""
* MC stat error added quadratically to variation
* symmetrised
* sign taken into account
* evaluated on f_C_i = B_i/(A/C*D)
"""
TtbarSystematicValueDict = {
   "SR" : {
      "TtbarHardScatGenTheo"  : ([1-0.52, 1-0.60], [1+0.52, 1+0.60]),
      "TtbarHadFragTheo"      : ([1-0.30, 1+0.14], [1+0.30, 1-0.14]),
      "TtbarRadQCDScalesTheo" : ([1+0.22, 1+0.22], [1-0.22, 1-0.22]),
   },
   "VR_tail_herself"          : {
      "TtbarHardScatGenTheo"  : ([1-0.06, 1+0.16], [1+0.06, 1-0.16]),
      "TtbarHadFragTheo"      : ([1+0.19, 1-0.05], [1-0.19, 1+0.05]),
      "TtbarRadQCDScalesTheo" : ([1-0.06, 1+0.13], [1+0.06, 1-0.13]),
   },
   "VR_jet_herself"           : {
      "TtbarHardScatGenTheo"  : ([1-0.43], [1+0.43]),
      "TtbarHadFragTheo"      : ([1-0.14], [1+0.14]),
      "TtbarRadQCDScalesTheo" : ([1-0.3],  [1+0.3]),
   },
}

TtbarSystematicDict = {}
for region, valueDict in TtbarSystematicValueDict.items():
   for sysName, (binsDown, binsUp) in valueDict.items():
      if not region in TtbarSystematicDict:
         TtbarSystematicDict[region] = {}
      TtbarSystematicDict[region][sysName] = Systematic(sysName, configMgr.weights,
                                                        binsDown, binsUp,
                                                        "user", "userHistoSys")
