
# One lepton simplified fit configuration

This folder contains the simplified fit configuration used in truth and pMSSM studies by the one lepton analysis team. This README will give you some instructions on how to use the simplified fit configuration.

## Simplified fit

The fit uses the background yields from the strong 1-lepton paper, stored in yaml or pickle files. Both formats can be found in folders residing in this directory. It is preferred to use the SRYields_sr_plus_cr_fit_pickle files since the yields in those files result from a background-only fit where both the CR and the SR were used, which slightly improves the fit result in the 2J region where a slight excess has been observed in the paper. The other pickle and yaml files are CR-only. The fit is simplified because it assumes the systematic uncertainties to be fully correlated over all the bins.

The configuration further uses all the SRs defined in the 1-lepton paper and does a simultaneous fit in all the SRs except for the 9J region which is the only region not orthogonal to the others.

## How to run

The fit can be run with a single command. The options in the OneLeptonFitSimplified.py config file can be given to Histfitter with the --userArg argument:

```
HistFitter.py -wtp OneLeptonFitSimplified.py --userArg="--cr-plus-sr --use-data --signal-trees <path-to-signal-trees> --sigpoint-file <path-to-signal-points-file>"
```

By adding --use-data, the fit computes not only expected CLs but also observed CLs values. The --cr-plus-sr option indicates that the fit runs with the yields from the CR+SR background fit. Input trees and output names can of course be specified with further arguments. The signal points file should be a text file with the names of the signal trees (that live in the signal ROOT file), so that the fit knows which signal points to run over.

The output of the fit is in the usual Histfitter format, from where you can further process it.
