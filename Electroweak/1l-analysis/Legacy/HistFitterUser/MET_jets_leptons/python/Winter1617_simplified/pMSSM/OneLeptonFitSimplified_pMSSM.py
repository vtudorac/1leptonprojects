import collections
import yaml
import sys
import re
import pickle
import shlex

import ROOT

from configManager import configMgr
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic

"""
Options to be passed via --userArg="--additional --options 'bla'"
Note: You have to use "--userArg=" or "-u ' --additional --option'"
(with the extra space before the first argument)
"""
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--sigpoint-file", default="OneLepton_models_pMSSM.txt")
parser.add_argument("--add-9J", action='store_true')
parser.add_argument("--only-9J", action='store_true')
parser.add_argument("--use-data", action='store_true')
parser.add_argument("--pickle-histos", action='store_true')
parser.add_argument("--cr-plus-sr", action='store_true', help="use pickle files containing information from bkgonly fit in CR+SR")
parser.add_argument("--yaml-histos", action='store_true')
parser.add_argument("--use-expected", action='store_true')
parser.add_argument("--signal-dir", help='directory where the text files with the signal yields per signal region and per bin are', default='pMSSM_yields')
args = parser.parse_args(args=shlex.split(configMgr.userArg))

if args.pickle_histos:
    args.yaml_histos = False

if args.yaml_histos:
    args.pickle_histos = False

if args.cr_plus_sr:
    args.pickle_histos = True

if args.use_expected and args.yaml_histos:
    raise Exception("Expected only works for the pickle files")

sigDirs = []
with open(args.sigpoint_file) as f:
    for l in f:
        sigDirs.append(l.strip())
        
configMgr.nomName = "_NoSys"

configMgr.writeXML = True

if HistFitterArgs.num_toys < 0:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

configMgr.analysisName = "OneLeptonFitSimplified"
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

configMgr.blindSR = not args.use_data
configMgr.blindCR = True
configMgr.blindVR = True

# to get usable observed p-values in blinded case (needed for
# AsymptoticCalculator to give expected values in all cases):
# use signal in blinded data for discovery p0, but not for exclusion cls
if doDiscoveryHypoTests: # set by "-z"
    configMgr.useSignalInBlindedData = True
else:
    configMgr.useSignalInBlindedData = False

configMgr.weights = ["eventWeight", "genWeight"]

configMgr.inputLumi = 0.001
configMgr.outputLumi =  36.06596
configMgr.setLumiUnits("fb-1")

# constructing the different signal regions
btag = "&&nBJet30_MV2c10>0"
bveto = "&&nBJet30_MV2c10==0"

configMgr.cutsDict["SR_2J_btag"] = "lep1Pt<35 && nJet30>=2 && met>430. && mt>100. && (met/meffInc30) > 0.25 && (nJet30/lep1Pt)>0.2&&  nLep_base==1&&nLep_signal==1 && ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
configMgr.cutsDict["SR_2J_bveto"] = configMgr.cutsDict["SR_2J_btag"]
configMgr.cutsDict["SR_2J_btag"] += btag
configMgr.cutsDict["SR_2J_bveto"] += bveto

configMgr.cutsDict["SR_4Jhighx_btag"] = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>300 && mt>450 && LepAplanarity>0.01 && met/meffInc30>0.25&&  nLep_base==1&&nLep_signal==1 && ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
configMgr.cutsDict["SR_4Jhighx_bveto"] = configMgr.cutsDict["SR_4Jhighx_btag"]
configMgr.cutsDict["SR_4Jhighx_bveto"] += bveto
configMgr.cutsDict["SR_4Jhighx_btag"] += btag

configMgr.cutsDict["SR_4Jlowx_btag"] = "lep1Pt>35 && nJet30>=4 && nJet30<6 && met>250 && mt>150 && mt<450 && LepAplanarity>0.05&&  nLep_base==1&&nLep_signal==1 && ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"
configMgr.cutsDict["SR_4Jlowx_bveto"] = configMgr.cutsDict["SR_4Jlowx_btag"]
configMgr.cutsDict["SR_4Jlowx_bveto"] += bveto
configMgr.cutsDict["SR_4Jlowx_btag"] += btag

configMgr.cutsDict["SR_6J_btag"] = "lep1Pt>35 && nJet30>=6 && met>350. && mt>175.&& LepAplanarity>0.06&&  nLep_base==1&&nLep_signal==1 && ( (AnalysisType==1 && lep1Pt>7) || (AnalysisType==2 && lep1Pt>6))"

# orthogonalise with 9J SR
#configMgr.cutsDict["SR_6J_btag"] += "&&nJet30<=8"

configMgr.cutsDict["SR_6J_bveto"] = configMgr.cutsDict["SR_6J_btag"]
configMgr.cutsDict["SR_6J_bveto"] += bveto
configMgr.cutsDict["SR_6J_btag"] += btag

# 9J SR
configMgr.cutsDict["SR_9J"] = "mt>175&&LepAplanarity>0.07&&nJet30>=9&&nLep_signal==1&&nLep_base==1&&lep1Pt>35&&(met/sqrt(Ht30))>8&&meffInc30>1000&&met>200"

# orthogonalise to 6J SR
configMgr.cutsDict["SR_9J_lowmet"] = configMgr.cutsDict["SR_9J"] + "&&met<350"

bkgSample = Sample("Bkg",ROOT.kGreen-9)
bkgSample.setStatConfig(False)
#bkgSample.setNormFactor("mu_bkg",1., 0., 5., True)

dataSample = Sample("Data",ROOT.kBlack)
dataSample.setData()

# Define fit config for all signals
for filename in sigDirs:
    #if not filename.endswith(".csv"):
        #print('Found a non csv file in directory, going to ignore this file, but you may want to fix this.')
        #continue

    ana = configMgr.addFitConfig("Sig_{}".format(filename))

    sigSample = Sample("Sig_{}".format(filename), ROOT.kPink)
    sigSample.setNormFactor("mu_Sig",1.,0.,100.)
    sigSample.setStatConfig(True)
    sigSample.setNormByTheory()

    ana.addSamples([bkgSample, dataSample, sigSample])
    ana.setSignalSample(sigSample)

    # Define measurement
    meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.032)
    meas.addPOI("mu_Sig")
    meas.addParamSetting("Lumi","const",1)

    # Add the channels
    binnings = collections.OrderedDict()
    binnings["SR_2J_btag"]= ("meffInc30",(4,700,2300))
    binnings["SR_2J_bveto"]= ("meffInc30",(4,700,2300))
    binnings["SR_4Jhighx_btag"]= ("meffInc30",(3,1000.,2500.))
    binnings["SR_4Jhighx_bveto"]= ("meffInc30",(3,1000.,2500.))
    binnings["SR_4Jlowx_btag"]= ("meffInc30",(3,1300.,2350.))
    binnings["SR_4Jlowx_bveto"]= ("meffInc30",(3,1300.,2350.))
    binnings["SR_6J_btag"]= ("meffInc30",(4,700.,2833.))
    binnings["SR_6J_bveto"]= ("meffInc30",(4,700.,2833.))
    
    # Add the translation to the pMSSM text file channels
    pMSSM_binnings = collections.OrderedDict()
    pMSSM_binnings["SR_2J_btag"] = ("SRs2j1bT","SRs2j2bT","SRs2j3bT","SRs2j4bT")
    pMSSM_binnings["SR_2J_bveto"] = ("SRs2j1bV","SRs2j2bV","SRs2j3bV","SRs2j4bV")
    pMSSM_binnings["SR_4Jhighx_btag"] = ("SRh4jhx1bT","SRh4jhx2bT","SRh4jhx3bT","SRh4jhx4bT")
    pMSSM_binnings["SR_4Jhighx_bveto"] = ("SRh4jhx1bV","SRh4jhx2bV","SRh4jhx3bV","SRh4jhx4bV")
    pMSSM_binnings["SR_4Jlowx_btag"] = ("SRh4jlx1bT","SRh4jlx2bT","SRh4jlx3bT","SRh4jlx4bT")
    pMSSM_binnings["SR_4Jlowx_bveto"] = ("SRh4jlx1bV","SRh4jlx2bV","SRh4jlx3bV","SRh4jlx4bV")
    pMSSM_binnings["SR_6J_btag"] = ("SRh6j1bT","SRh6j2bT","SRh6j3bT","SRh6j4bT")
    pMSSM_binnings["SR_6J_bveto"] = ("SRh6j1bV","SRh6j2bV","SRh6j3bV","SRh6j4bV")

    # SR 9J (orthogonal to 6J version)
    if args.add_9J:
        binnings["SR_9J_lowmet"]= ("meffInc30",(2,1000.,2000.))
        pMSSM_binnings["SR_9J_lowmet"] = ("SRh9j1","SRh9j2")
    if args.only_9J:
        binnings = {"SR_9J" : ("meffInc30",(2,1000.,2000.))}
        pMSSM_binnings = {"SR_9J" : ("SRh9j1","SRh9j2")}

    channels = collections.OrderedDict()
    for chanName, binning in binnings.items():
        varName, binningNumbers = binning
        channels[chanName] = ana.addChannel(varName, [chanName], *binningNumbers)
        channels[chanName].useOverflowBin = True

    ana.addSignalChannels([c for c in channels.values()])

    filedir = args.signal_dir+"/"+filename
    filedir = filedir.replace("//","/")
    with open(filedir) as f:
        for chanName, binning in pMSSM_binnings.items():
            values = []
            f.seek(0)
            for line in f:
                for bin in binning:
                    oneLepBin = "OneLepton2016__"+bin
                    if bin in line:
                        line_elements = line.split(",")
                        value = line_elements[1]
                        values.append(float(value))
            nbins, xmin, xmax = binnings[chanName][1]
            width = float(xmax-xmin)/float(nbins)
            sigSample.buildHisto(values, chanName, binnings[chanName][0], xmin, width)

    if args.yaml_histos:
        # build histos from yaml files
        def getDict(parentDict, key, chanName):
            for childDict in parentDict:
                for qualifier in childDict["qualifiers"]:
                    if qualifier["value"] == key:
                        return childDict
            raise Exception("{} dict not found for {}!".format(key, chanName))
        for chanName, channel in channels.items():
            with open("SRYields_yaml/{}.yaml".format(chanName)) as f:
                yamlDict = yaml.load(f)
            mcDict = getDict(yamlDict["dependent_variables"], "MC", chanName)
            binsDown = []
            binsUp = []
            for vDict in mcDict["values"]:
                if len(vDict["errors"]) > 1:
                    print("Can't deal with more than one error ...")
                errDict = vDict["errors"][0]
                if "symerror" in errDict:
                    binsDown.append(1-(errDict["symerror"]/vDict["value"]))
                    binsUp.append(1+(errDict["symerror"]/vDict["value"]))
                elif "asymerror" in errDict:
                    binsDown.append(1+errDict["asymerror"]["minus"]/vDict["value"])
                    binsUp.append(1+errDict["asymerror"]["plus"]/vDict["value"])
                else:
                    raise Exception("Don't know what to do with {}".format(vDict))
            import pprint
            pprint.pprint(mcDict)
            nbins, xmin, xmax = binnings[chanName][1]
            width = float(xmax-xmin)/float(nbins)
            bkgSample.buildHisto([v["value"] for v in mcDict["values"]],
                                 chanName, binnings[chanName][0], xmin, width)
            totalErr = Systematic("totalError", configMgr.weights,
                                  binsDown, binsUp,
                                  "user", "userHistoSys")
            channel.getSample("Bkg").addSystematic(totalErr)
            if args.use_data:
                dataDict = getDict(yamlDict["dependent_variables"], "Data", chanName)
                dataSample.buildHisto([v["value"] for v in dataDict["values"]],
                                     chanName, binnings[chanName][0], xmin, width)

    elif args.pickle_histos:
        for chanName, channel in channels.items():
            if args.cr_plus_sr:
                pickleName = re.sub(r"SR_([0-9]J)([^_]*)_(.*)", r"MyTable1LepSR\3_\1\2_bkgonly_with_sr.pickle", chanName).replace("btag", "BT").replace("bveto", "BV")
                pickleDir = "SRYields_sr_plus_cr_fit_pickle"
            else:
                pickleName = re.sub(r"SR_([0-9]J)([^_]*)_(.*)", r"MyTable1LepSR\3_\1\2_bkgonly.pickle", chanName).replace("btag", "BT").replace("bveto", "BV")
                pickleDir = "SRYields_prefit_pickle"
            with open("{}/{}".format(pickleDir, pickleName)) as f:
                pickleDict = pickle.load(f)
            if args.use_expected:
                binVals = pickleDict["TOTAL_MC_EXP_BKG_events"][1:]
                binErrs = pickleDict["TOTAL_MC_EXP_BKG_err"][1:]
            else:
                binVals = pickleDict["TOTAL_FITTED_bkg_events"][1:]
                binErrs = pickleDict["TOTAL_FITTED_bkg_events_err"][1:]
            nbins, xmin, xmax = binnings[chanName][1]
            width = float(xmax-xmin)/float(nbins)
            bkgSample.buildHisto(binVals,
                                 chanName, binnings[chanName][0], xmin, width)
            totalErr = Systematic("totalError", configMgr.weights,
                                  [1+e/v for v,e in zip(binVals, binErrs)],
                                  [1-e/v for v,e in zip(binVals, binErrs)],
                                  "user", "userHistoSys")
            channel.getSample("Bkg").addSystematic(totalErr)
            if args.use_data:
                dataVals = pickleDict["nobs"][1:]
                dataSample.buildHisto(dataVals,
                                     chanName, binnings[chanName][0], xmin, width)

    else:
        raise Exception("Don't know where to get bkg Histograms from")



# These lines are needed (are they?) for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName)
