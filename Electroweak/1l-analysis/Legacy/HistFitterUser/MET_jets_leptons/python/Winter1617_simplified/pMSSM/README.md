This folder contains the following files:

1) OneLeptonFitSimplified_pMSSM.py : the Histfitter config file for running on pMSSM points
2) maketextfile_pMSSM.py : a short script used to convert the Histfitter output into a comma separated text file
3) OneLepton_models_pMSSM.txt : a sample text file used to define which pMSSM files the fit should run over
4) OneLepton_output_pMSSM.txt : a sample of a final output file containing comma separated CLs values for each point the fit has successfully run over

The fit uses the background yields from the paper, stored in yaml or pickle files. Both formats can be found in the parent directory. It is preferred to use the SRYields_sr_plus_cr_fit_pickle files since in that background-only fit, both the CR and the SR were used, which slightly improves the fit result in the 2J region where a slight excess has been observed in the paper. The other pickle and yaml files are CR-only.

Steps needed to run the fit:
1) Create a text file, where you specify the name of each pMSSM file the fit should run over. This file is given to the fit with the --sigpoint-file option.

2) Run the fit (here with the CR+SR background-only fit from the auxiliary material of the paper, you can also use the CR-only pickle/yaml files):
`HistFitter.py -wtp OneLeptonFitSimplified_pMSSM.py --userArg="--cr-plus-sr --use-data --sigpoint-file <textfile-with-signalfile-names> --signal-dir <dir-to-signalfiles>"`
The result of the fit is stored in a 'results' directory as standard Histfitter output.

3) Convert the output to csv with:
`maketextfile_pMSSM.py`
If you didn't specify a custom name for the output of the fit, this script should find it by default. Otherwise you can always indicate where to look for the output file with the available options.

