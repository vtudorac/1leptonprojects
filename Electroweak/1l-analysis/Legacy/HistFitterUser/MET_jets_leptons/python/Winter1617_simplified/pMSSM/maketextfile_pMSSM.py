#!/usr/bin/env python

import sys, argparse, os, json, csv

import ROOT

parser = argparse.ArgumentParser(description=("Create text files with p0/cls values from histfitter output"))
parser.add_argument('-i', help='root file containing Hypotest results', default='results/OneLeptonFitSimplified_Output_hypotest.root')
parser.add_argument('-o', help='Output file name', default='OneLepton_output_pMSSM.txt')

args = parser.parse_args()

ROOT.gSystem.Load('libSusyFitter.so')

f = ROOT.TFile.Open(args.i)

csv_out = csv.writer(open(args.o, "wb+"))
csv_out.writerow(["point", "CLsexp", "CLsexpu1S", "CLsexpd1S", "CLsobs"])

result = None
counter = 0
for k in f.GetListOfKeys():
    result = f.Get(k.GetName())
    if result.ClassName() == "RooStats::HypoTestInverterResult":
        if "discovery" in result.GetName():
            continue

        counter += 1
        point = result.GetName().replace("hypo_Sig_","")
        print("Printing point: {}".format(point))

        limitres = ROOT.RooStats.get_Pvalue(result)

        # expected CLs
        print("Expected CLs: {} ({} sigma)".format(limitres.GetCLsexp(), ROOT.RooStats.PValueToSignificance(limitres.GetCLsexp())))

        # error band - not sure if those are the right ones
        print("Expected CLs (+1): {} ({} sigma)".format(limitres.GetCLsu1S(), ROOT.RooStats.PValueToSignificance(limitres.GetCLsu1S())))
        print("Expected CLs (-1): {} ({} sigma)".format(limitres.GetCLsd1S(), ROOT.RooStats.PValueToSignificance(limitres.GetCLsd1S())))
        print("Expected CLs (+2): {} ({} sigma)".format(limitres.GetCLsu2S(), ROOT.RooStats.PValueToSignificance(limitres.GetCLsu2S())))
        print("Expected CLs (-2): {} ({} sigma)".format(limitres.GetCLsd2S(), ROOT.RooStats.PValueToSignificance(limitres.GetCLsd2S())))

        # observed CLs
        print("Observed CLs: {} ({} sigma)".format(limitres.GetCLs(), ROOT.RooStats.PValueToSignificance(limitres.GetCLs())))

        #help(limitres)
        csv_out.writerow([point,limitres.GetCLsexp(),limitres.GetCLsu1S(),limitres.GetCLsd1S(),limitres.GetCLs()])

print("Finished writing {} CLs values to text file: {}".format(counter,args.o))
