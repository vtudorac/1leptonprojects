################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
import ROOT
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import sqrt
import pickle
from os import sys
from math import exp

from logger import Logger
log = Logger('SoftLepton')

# ********************************************************************* #
# some useful helper functions
# ********************************************************************* #

def myreplace(l1,l2,element):
    idx=l1.index(element)
    if idx>=0:
        return l1[:idx] + l2 + l1[idx+1:]
    else:
        print "WARNING idx negative: %d" % idx
        return l1

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupSamples(samples,systList):
    for sample in samples:
        for syst in systList:
            sample.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #

#print sys.argv

# ********************************************************************* #
#                              Analysis parameters
# ********************************************************************* #

if not 'useStat' in dir():
    useStat=True
if not 'chn' in dir():
    chn=1                   # analysis channel 0=A,1=B,..
if not 'grid' in dir():
    grid="" # "SU"               # only grid implemented up to now
if not 'gridspec' in dir():
    gridspec="_" # "SU"               # only grid implemented up to now
if not 'suffix' in dir():
    suffix="_NoSys"
if not 'sigSamples' in dir():
    sigSamples=None #["1000_250"]
if not 'anaName' in dir():
    anaName = 'SRs1L'
if not 'includeSyst' in dir():
    includeSyst = True
if not 'dobtag' in dir():
    dobtag = True
if not 'doBlinding' in dir():
    doBlinding = True

configMgr.fixSigXSec=True

# ********************************************************************* #
#                              Options
# ********************************************************************* #

# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    if pickedSRs[0]=="SR3jExcl":
        chn=0
    elif pickedSRs[0]=="SRs1L":
        chn=1
    elif pickedSRs[0]=="SR1a":
        chn=2
    elif pickedSRs[0]=="SR1b":
        chn=3
    elif pickedSRs[0]=="SR2a":
        chn=4
    elif pickedSRs[0]=="SR2b":
        chn=5
    elif pickedSRs[0]=="SRs2L":
        chn=6
    else:
        print "WARNING: analysis not defined"
        sys.exit()
    anaName=pickedSRs[0]
    # set the grid
    if len(pickedSRs) >= 2: 
        grid = pickedSRs[1] 
        pass
    if len(pickedSRs) >= 3: 
        gridspec = '_'+pickedSRs[2]+'_'
        pass

if chn>6 or chn<0:
    print "ERROR analysis not defined!!!"
    print chn
    print pickedSRs
    sys.exit()

allpoints=['500_400_60']

if chn==0 or chn==1:
    # traditional soft 1-lepton analysis
    if grid=="SM_GG1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
    elif grid=="SM_SS1step":
        allpoints=['1000_110_60', '1000_160_60', '1000_260_60', '1000_360_60', '1000_460_60', '1000_560_60', '1000_660_60', '1000_70_60', '1000_760_60', '1000_85_60', '1000_860_60', '1000_900_60', '1000_950_60', '1000_960_60', '1000_975_60', '1000_990_60', '1025_545_65', '1025_625_225', '1025_705_385', '1025_785_545', '1025_865_705', '1025_945_865', '1037_1025_1012', '1050_1025_1000', '1065_1025_985', '1065_545_25', '1065_625_185', '1065_705_345', '1065_785_505', '1065_865_665', '1065_945_825', '1100_1000_60', '1100_1050_60', '1100_1060_60', '1100_1075_60', '1100_1090_60', '1100_110_60', '1100_160_60', '1100_260_60', '1100_360_60', '1100_460_60', '1100_560_60', '1100_660_60', '1100_70_60', '1100_760_60', '1100_85_60', '1100_860_60', '1100_960_60', '1105_1025_945', '1105_625_145', '1105_705_305', '1105_785_465', '1105_865_625', '1105_945_785', '1117_1105_1092', '1130_1105_1080', '1145_1025_905', '1145_1105_1065', '1145_625_105', '1145_705_265', '1145_785_425', '1145_865_585', '1145_945_745', '1185_1025_865', '1185_1105_1025', '1185_625_65', '1185_705_225', '1185_785_385', '1185_865_545', '1185_945_705', '1197_1185_1172', '1200_1060_60', '1200_1100_60', '1200_110_60', '1200_1150_60', '1200_1160_60', '1200_1175_60', '1200_1190_60', '1200_160_60', '1200_260_60', '1200_360_60', '1200_460_60', '1200_560_60', '1200_660_60', '1200_70_60', '1200_760_60', '1200_85_60', '1200_860_60', '1200_960_60', '200_100_60', '200_110_60', '200_150_60', '200_160_60', '200_175_60', '200_190_60', '200_70_60', '200_85_60', '205_125_45', '210_185_160', '225_185_145', '227_215_202', '235_155_75', '240_215_190', '245_125_5', '255_215_175', '257_245_232', '265_185_105', '270_245_220', '275_155_35', '285_245_205', '287_275_262', '295_215_135', '300_110_60', '300_160_60', '300_200_60', '300_250_60', '300_260_60', '300_275_250', '300_275_60', '300_290_60', '300_70_60', '300_85_60', '305_185_65', '315_275_235', '317_305_292', '325_245_165', '330_305_280', '335_215_95', '345_185_25', '345_305_265', '355_275_195', '365_245_125', '375_215_55', '385_305_225', '395_275_155', '397_385_372', '400_110_60', '400_160_60', '400_260_60', '400_300_60', '400_350_60', '400_360_60', '400_375_60', '400_390_60', '400_70_60', '400_85_60', '405_245_85', '410_385_360', '415_215_15', '425_305_185', '425_385_345', '435_275_115', '445_245_45', '465_305_145', '465_385_305', '475_275_75', '477_465_452', '485_245_5', '490_465_440', '500_110_60', '500_160_60', '500_260_60', '500_360_60', '500_400_60', '500_450_60', '500_460_60', '500_475_60', '500_490_60', '500_70_60', '500_85_60', '505_305_105', '505_385_265', '505_465_425', '515_275_35', '545_305_65', '545_385_225', '545_465_385', '557_545_532', '570_545_520', '585_305_25', '585_385_185', '585_465_345', '585_545_505', '600_110_60', '600_160_60', '600_260_60', '600_360_60', '600_460_60', '600_500_60', '600_550_60', '600_560_60', '600_575_60', '600_590_60', '600_70_60', '600_85_60', '625_385_145', '625_465_305', '625_545_465', '637_625_612', '650_625_600', '665_385_105', '665_465_265', '665_545_425', '665_625_585', '700_110_60', '700_160_60', '700_260_60', '700_360_60', '700_460_60', '700_560_60', '700_600_60', '700_650_60', '700_660_60', '700_675_60', '700_690_60', '700_70_60', '700_85_60', '705_385_65', '705_465_225', '705_545_385', '705_625_545', '717_705_692', '730_705_680', '745_385_25', '745_465_185', '745_545_345', '745_625_505', '745_705_665', '785_465_145', '785_545_305', '785_625_465', '785_705_625', '797_785_772', '800_110_60', '800_160_60', '800_260_60', '800_360_60', '800_460_60', '800_560_60', '800_660_60', '800_700_60', '800_70_60', '800_750_60', '800_760_60', '800_775_60', '800_790_60', '800_85_60', '810_785_760', '825_465_105', '825_545_265', '825_625_425', '825_705_585', '825_785_745', '865_465_65', '865_545_225', '865_625_385', '865_705_545', '865_785_705', '877_865_852', '890_865_840', '900_110_60', '900_160_60', '900_260_60', '900_360_60', '900_460_60', '900_560_60', '900_660_60', '900_70_60', '900_760_60', '900_800_60', '900_850_60', '900_85_60', '900_860_60', '900_875_60', '900_890_60', '905_465_25', '905_545_185', '905_625_345', '905_705_505', '905_785_665', '905_865_825', '945_545_145', '945_625_305', '945_705_465', '945_785_625', '945_865_785', '957_945_932', '970_945_920', '985_545_105', '985_625_265', '985_705_425', '985_785_585', '985_865_745', '985_945_905']
        pass
elif chn>=2 and chn<=5:
    # stop soft 1-lepton analysis
    if grid=="StopBCharDeg" and gridspec=="_5gev_":
        allpoints=['150_105_100', '150_140_135', '200_105_100', '200_155_150', '200_190_185', '250_105_100', '250_155_150', '250_205_200', '250_240_235', '300_105_100', '300_155_150', '300_205_200', '300_255_250', '300_290_285', '350_155_150', '350_205_200', '350_255_250', '350_305_300', '350_340_335', '400_105_100', '400_205_200', '400_255_250', '400_305_300', '400_355_350', '400_390_385', '450_155_150', '450_255_250', '450_305_300', '450_355_350', '450_405_400', '450_440_435', '500_105_100', '500_205_200', '500_255_250', '500_305_300', '500_355_350', '500_405_400', '500_455_450', '500_490_485', '550_105_100', '550_155_150', '550_205_200', '550_255_250', '550_305_300', '550_355_350', '550_405_400', '550_455_450', '600_105_100', '600_155_150', '600_205_200', '600_255_250', '600_305_300', '600_355_350', '600_405_400', '600_455_450', '650_105_100', '650_155_150', '650_205_200', '650_255_250', '650_305_300', '650_355_350', '650_405_400', '650_455_450', '700_105_100', '700_155_150', '700_205_200', '700_255_250', '700_305_300', '700_355_350', '700_405_400', '700_455_450', '750_105_100', '750_155_150', '750_205_200', '750_255_250', '750_305_300', '750_355_350', '750_405_400', '750_455_450', '800_105_100', '800_155_150', '800_205_200', '800_255_250', '800_305_300', '800_355_350', '800_405_400', '800_455_450']
    elif grid=="StopBCharDeg" and gridspec=="_20gev_":
        allpoints=['150_140_120', '150_95_75', '200_120_100', '200_170_150', '200_190_170', '200_95_75', '250_120_100', '250_170_150', '250_220_200', '250_240_220', '250_95_75', '300_120_100', '300_170_150', '300_220_200', '300_270_250', '300_290_270', '350_170_150', '350_220_200', '350_270_250', '350_320_300', '350_340_320', '350_95_75', '400_120_100', '400_220_200', '400_270_250', '400_320_300', '400_370_350', '400_390_370', '450_170_150', '450_220_200', '450_270_250', '450_320_300', '450_370_350', '450_420_400', '450_440_420', '450_95_75', '500_120_100', '500_220_200', '500_270_250', '500_320_300', '500_370_350', '500_420_400', '500_470_450', '500_490_470', '550_120_100', '550_170_150', '550_220_200', '550_270_250', '550_320_300', '550_370_350', '550_420_400', '550_470_450', '550_95_75', '600_120_100', '600_170_150', '600_220_200', '600_270_250', '600_320_300', '600_370_350', '600_420_400', '600_470_450', '600_95_75', '650_120_100', '650_170_150', '650_220_200', '650_270_250', '650_320_300', '650_370_350', '650_420_400', '650_470_450', '650_95_75', '700_120_100', '700_170_150', '700_220_200', '700_270_250', '700_320_300', '700_370_350', '700_420_400', '700_470_450', '700_95_75', '750_120_100', '750_170_150', '750_220_200', '750_270_250', '750_320_300', '750_370_350', '750_420_400', '750_470_450', '750_95_75', '800_120_100', '800_170_150', '800_220_200', '800_270_250', '800_320_300', '800_370_350', '800_420_400', '800_470_450', '800_95_75']
    pass
elif chn==6:
    # ued soft 2-lepton analysis
    if grid=="mUED2Lfilter":
        allpoints=['1000_10', '1000_3', '1000_40', '1100_10', '1100_3', '1100_40', '1200_10', '1200_3', '1200_40', '1300_10', '1300_3', '1300_40', '700_10', '700_3', '700_40', '800_10', '800_3', '800_40', '900_10', '900_3', '900_40']
    pass

# No input signal for discovery and bkg fit
if myFitType==FitType.Discovery:
    allpoints=["Discovery"]
    grid=""
    pass
elif myFitType==FitType.Background:
    allpoints=["Background"]
    grid=""
    pass

# sigSamples is set by the "-g" HistFitter option. Overwrites allpoints, used below.
try:
    sigSamples
except NameError:
    sigSamples = None
    
if sigSamples!=None:
    allpoints=sigSamples

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

#configMgr.doHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

# ********************************************************************* #
# Main part - now we start to build the data model
# ********************************************************************* #

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 14.3 #21.0
configMgr.setLumiUnits("fb-1")


inputDirSlim="root://eosatlas//eos/atlas/user/n/nkanaya/trees/Moriond/v4_4_x_2_slim/"
inputDirVars="root://eosatlas//eos/atlas/user/t/tnobe/v4_4_x_2/"  
inputDirSig="root://eosatlas//eos/atlas/user/t/tnobe/v4_4_x_2/"  


#Split bdgFiles per channel
sigFiles = []
if myFitType==FitType.Exclusion:
    if chn>=0 and chn<=5: # soft 1-lepton
        sigFiles += [inputDirSig+"sigtree_Soft1Lep_"+grid+".root"]
    elif chn==6:          # soft 2-lepton
        sigFiles += [inputDirSig+"sigtree_Soft2Lep_"+grid+".root"]
    if len(sigFiles)==0:
        print "WARNING: signal grid files not defined"
        sys.exit()
    pass

# Set the files to read from
if chn>=0 and chn<=5: # soft 1-lepton
    if True:
        dataFiles              = [inputDirVars+"Soft1Lep_Data.root"]
        qcdFiles               = [inputDirVars+"Soft1Lep_QCD.root"]
        AlpgenDYFiles          = [inputDirSlim+"Soft1Lep_AlpgenDY.root"]
        AlpgenWFiles           = [inputDirSlim+"Soft1Lep_AlpgenW.root"]
        AlpgenZFiles           = [inputDirSlim+"Soft1Lep_AlpgenZ.root"]
        SherpaDibosonsFiles    = [inputDirSlim+"Soft1Lep_SherpaDibosons.root"]
        DibosonsFiles          = [inputDirSlim+"Soft1Lep_Dibosons.root"]
        PowhegPythiaTTbarFiles = [inputDirSlim+"Soft1Lep_PowhegPythiaTTbar.root"]
        SherpaWMassiveBFiles   = [inputDirSlim+"Soft1Lep_SherpaWMassiveB.root"]
        SherpaZMassiveBFiles   = [inputDirSlim+"Soft1Lep_SherpaZMassiveB.root"]
        SingleTopFiles         = [inputDirSlim+"Soft1Lep_SingleTop.root"]
        ttbarVFiles            = [inputDirSlim+"Soft1Lep_ttbarV.root"]
        pass
    elif False:
        dataFiles              = [inputDirVars+"Soft1Lep_Data.root"]
        qcdFiles               = [inputDirVars+"Soft1Lep_QCD.root"]
        AlpgenDYFiles          = [inputDirVars+"Soft1Lep_AlpgenDY.root"]
        AlpgenWFiles           = [inputDirVars+"Soft1Lep_AlpgenWZ.root"]
        AlpgenZFiles           = [inputDirVars+"Soft1Lep_AlpgenWZ.root"]
        SherpaDibosonsFiles    = [inputDirVars+"Soft1Lep_SherpaDibosons.root"]
        #
        DibosonsFiles          = [inputDirVars+"bkgtree_Soft1Lep.root"]
        PowhegPythiaTTbarFiles = [inputDirVars+"bkgtree_Soft1Lep.root"]
        SherpaWMassiveBFiles   = [inputDirVars+"bkgtree_Soft1Lep.root"]
        SherpaZMassiveBFiles   = [inputDirVars+"bkgtree_Soft1Lep.root"]
        SingleTopFiles         = [inputDirVars+"bkgtree_Soft1Lep.root"]
        ttbarVFiles            = [inputDirVars+"bkgtree_Soft1Lep.root"]
        bkgFiles               = [inputDirVars+"bkgtree_Soft1Lep.root"]
        pass   
elif chn==6:          # soft 2-lepton
    dataFiles              = [inputDirVars+"Soft2Lep_Data.root"]
    qcdFiles               = [inputDirVars+"Soft2Lep_QCD.root"]
    AlpgenDYFiles          = [inputDirVars+"Soft2Lep_AlpgenDY.root"]
    AlpgenWFiles           = [inputDirVars+"Soft2Lep_AlpgenWZ.root"]
    AlpgenZFiles           = [inputDirVars+"Soft2Lep_AlpgenWZ.root"]
    SherpaDibosonsFiles    = [inputDirVars+"Soft2Lep_SherpaDibosons.root"]
    #
    DibosonsFiles          = [inputDirVars+"bkgtree_Soft2Lep.root"]
    PowhegPythiaTTbarFiles = [inputDirVars+"bkgtree_Soft2Lep.root"]
    SherpaWMassiveBFiles   = [inputDirVars+"bkgtree_Soft2Lep.root"]
    SherpaZMassiveBFiles   = [inputDirVars+"bkgtree_Soft2Lep.root"]
    SingleTopFiles         = [inputDirVars+"bkgtree_Soft2Lep.root"]
    ttbarVFiles            = [inputDirVars+"bkgtree_Soft2Lep.root"]
    bkgFiles               = [inputDirVars+"bkgtree_Soft2Lep.root"]


    
########################################
# Analysis description
########################################

## Lists of weights 
weights = ["genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight"]

toreplace = ""
if chn==0 and dobtag:
    #weights += ["bTagWeight[9]"]
    bweights = weights + ["bTagWeight[9]"]
    toreplace = "bTagWeight[9]"
    pass
elif chn==1 and dobtag:
    #weights += ["bTagWeight[9]"]
    bweights = weights + ["bTagWeight[9]"]
    toreplace = "bTagWeight[9]"
    pass
elif chn==2 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==3 and dobtag:
    #weights += ["bTagWeight[8]"]
    bweights = weights + ["bTagWeight[8]"]
    toreplace = "bTagWeight[8]"
    pass
elif chn==4 and dobtag:
    #weights += ["bTagWeight[9]"]
    bweights = weights + ["bTagWeight[9]"]
    toreplace = "bTagWeight[9]"
    pass
elif chn==5 and dobtag:
    #weights += ["bTagWeight[9]"]
    bweights = weights + ["bTagWeight[9]"]
    toreplace = "bTagWeight[9]"
    pass
elif chn==6 and dobtag:
    #weights += ["bTagWeight[11]"]
    bweights = weights + ["bTagWeight[11]"]
    toreplace = "bTagWeight[11]"
    pass

configMgr.weights = weights # bweights
configMgr.weightsQCD = "qcdWeight"
configMgr.weightsQCDWithB = "qcdBWeight"

xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
lepLowWeights = replaceWeight(weights, "leptonWeight","leptonWeightDown")

sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")

if dobtag:
    bTagHighWeights9 = replaceWeight(bweights,toreplace,"bTagWeightBUp[9]")
    bTagLowWeights9  = replaceWeight(bweights,toreplace,"bTagWeightBDown[9]")
    bTagHighWeights8 = replaceWeight(bweights,toreplace,"bTagWeightBUp[8]")
    bTagLowWeights8  = replaceWeight(bweights,toreplace,"bTagWeightBDown[8]")
    bTagHighWeights11 = replaceWeight(bweights,toreplace,"bTagWeightBUp[11]")
    bTagLowWeights11  = replaceWeight(bweights,toreplace,"bTagWeightBDown[11]")

#-------------------------------------------------------------------------
# Blinding of SR
#-------------------------------------------------------------------------

configMgr.blindSR = doBlinding
configMgr.blindVR = doBlinding

#--------------------------------------------------------------------------
# List of systematics
#--------------------------------------------------------------------------

configMgr.nomName = "_NoSys"

## JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig",   "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallHistoSys")
jes       = Systematic("JES",    "_NoSys", "_JESup"    ,"_JESdown"    ,"tree","overallNormHistoSys") # JES uncertainty - for low pt jets
jer       = Systematic("JER",    "_NoSys", "_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym")

jlow      = Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys") # JES uncertainty - for low pt jets
jmed      = Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys") # JES uncertainty - for medium pt jets
jhigh     = Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys") # JES uncertainty - for high pt jets

## MET uncertainty
scalest   = Systematic("SCALEST","_NoSys", "_SCALESTup","_SCALESTdown","tree","overallNormHistoSys")
resost    = Systematic( "RESOST","_NoSys", "_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym")

## pile-up
pileup = Systematic("pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys")

## b-tagging
if dobtag:
    bTagSyst9  = Systematic("BT",bweights,bTagHighWeights9,bTagLowWeights9,"weight","overallNormHistoSys")
    bTagSyst8  = Systematic("BT",bweights,bTagHighWeights8,bTagLowWeights8,"weight","overallNormHistoSys")
    bTagSyst11 = Systematic("BT",bweights,bTagHighWeights11,bTagLowWeights11,"weight","overallNormHistoSys")

## Trigger efficiency --> should be taken from data/mc trigger study
#trEl = Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#trMu = Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#####trEff= Systematic("TE",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallNormHistoSys")
trEff = Systematic("TE",configMgr.weights,1.05,0.95,"user","userOverallSys")

## MC theoretical uncertainties
## Signal XSec uncertainty as overallSys (pure yeild affect) 
xsecSig = Systematic("SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys" )

## generator level uncertainties
#SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")

qfacT  = Systematic("qfacT",configMgr.weights,configMgr.weights+["qfacUpWeightTTbar"],configMgr.weights+["qfacDownWeightTTbar"],"weight","overallNormHistoSys")
qfacW  = Systematic("qfacW",configMgr.weights,configMgr.weights+["qfacUpWeightW"],configMgr.weights+["qfacDownWeightW"],"weight","overallNormHistoSys")

ktfacT = Systematic("ktfacT",configMgr.weights,configMgr.weights+["ktfacUpWeightTTbar"],configMgr.weights+["ktfacDownWeightTTbar"],"weight","overallNormHistoSys")
ktfacW = Systematic("ktfacW",configMgr.weights,configMgr.weights+["ktfacUpWeightW"],configMgr.weights+["ktfacDownWeightW"],"weight","overallNormHistoSys")

iqoptW = Systematic("iqoptW",configMgr.weights,configMgr.weights+["iqopt2WeightW"],configMgr.weights+["iqopt3WeightW"],"weight","overallNormHistoSys")

wbb    = Systematic("wbb", configMgr.weights,1.24 ,0.76, "user","userOverallSys")

## qcd stat/syst weights automatically picked up.

## Lepton weight uncertainty
#elEff = Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys") 
#muEff = Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")
lepEff= Systematic("LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallNormHistoSys")

## Electron energy scale uncertainty
#ees = Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")
egzee = Systematic("egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys")
egmat = Systematic("egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallSys")
egps  = Systematic("egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys")
eglow = Systematic("eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys")
egres = Systematic("egres","_NoSys","_EGRESup","_EGRESdown","tree","overallSys")

## Muon energy resolutions
merm = Systematic("LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallSys")
meri = Systematic("LRImu","_NoSys","_MIDup","_MIDdown","tree","overallSys")

SystList = []
if True:
    SystList.append(jes)
    SystList.append(jer)
    SystList.append(lepEff)
    SystList.append(scalest)
    SystList.append(resost)
    SystList.append(pileup)
    SystList.append(trEff)
    SystList.append(egzee)
    SystList.append(egmat)
    SystList.append(egps)
    SystList.append(eglow)
    SystList.append(egres)
    SystList.append(merm)
    SystList.append(meri)

## do these per sample/channel
#SystList.append(bTagSyst)
#SystList.append(qfacT)
#SystList.append(qfacW)
#SystList.append(ktfacT)
#SystList.append(ktfacW)
#SystList.append(iqoptW)

#--------------------------------------------------------------------------
# List of channel selections
#--------------------------------------------------------------------------

TriggerSelection = "&& (AnalysisType==5) "
LeptonSelection  = TriggerSelection + "&& (lep1Pt<25) && ((lep1Pt>7&&lep1Flavor==1&&((abs(lep1Eta)<1.37 || 1.52<abs(lep1Eta))))||(lep1Pt>6&&lep1Flavor==-1))"
HardLeptonSelection = "&& (lep1Pt>25) && (lep2Pt<10)"
    
if chn==0:
    Jet3Selection                    = "&& jet1Pt_jvf25>180 && nJet25_jvf25>=3 && jet5Pt_jvf25<25" # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    CommonSelection                  = "&& ( mt>100 )" 
    configMgr.cutsDict["SR1L3j"]   = "( met>400 && mt>100 && (met/meffInc25_jvf25)>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["CRT"]         = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_980_jvf25>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]         = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_980_jvf25==0 )"  + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_980_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_980_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25==0 )" + Jet3Selection + LeptonSelection
    pass
    #
elif chn==1:
    Jet3Selection                    = "&& jet1Pt_jvf25>180 && nJet25_jvf25>=3 && jet5Pt_jvf25<25" # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    Jet5Selection                    = "&& (jet1Pt_jvf25>180 && jet5Pt_jvf25>25)"   # && (jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25 && jetJVF[3]>0.5 && jetJVF[4]>0.25)"   

    CommonSelection                  = "&& ( mt>100 )" 
    configMgr.cutsDict["SR1L3j"]   = "( met>400 && mt>100 && (met/meffInc25_jvf25)>0.3 )"             + Jet3Selection + LeptonSelection
    configMgr.cutsDict["SR1L5j"]   = "( met>300 && mt>100 && (met/meffInc25_jvf25)>0.3 )"             + Jet5Selection + LeptonSelection
    configMgr.cutsDict["CRT"]         = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_980_jvf25>0 )"   + Jet3Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW"]         = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_980_jvf25==0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["CRT5j"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_980_jvf25>0 )"   + Jet5Selection + LeptonSelection # use nB3JEt here?
    configMgr.cutsDict["CRW5j"]        = "( met>180 && met<250 && mt>40 && mt<80 && nBJet25_MV1_980_jvf25==0 )"  + Jet5Selection + LeptonSelection
    #
    configMgr.cutsDict["VR3j1"]      = "( met>180 && met<250 && mt>80)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_980_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_980_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j2"]     = "( met>250 && met<350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25==0 )" + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VR3j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRT3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25>0 )"  + Jet3Selection + LeptonSelection
    configMgr.cutsDict["VRW3j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25==0 )" + Jet3Selection + LeptonSelection
    #
    configMgr.cutsDict["VR5j1"]      = "( met>180 && met<250 && mt>80)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_980_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j1"]     = "( met>180 && met<250 && mt>80 && nBJet25_MV1_980_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j2"]      = "( met>250 && met<350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_980_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j2"]     = "( met>250 && met<350 && mt>40 && mt<100  && nBJet25_MV1_980_jvf25==0 )" + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VR5j3"]      = "( met>350 && mt>40 && mt<100)"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRT5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25>0 )"  + Jet5Selection + LeptonSelection
    configMgr.cutsDict["VRW5j3"]     = "( met>350 && mt>40 && mt<100 && nBJet25_MV1_980_jvf25==0 )" + Jet5Selection + LeptonSelection
    pass
    #
elif chn==2:
    # (lep1Pt<25. && met>200. && nB2Jet60==2 && nJet50_jvf25_wo2jets==0 && mctcorr>150. && ht_wo2jets<50. && dphimin>0.4);
    JetSelection               = "&& (nJet50_jvf25_wo2jets==0 && dphimin>0.4)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)" 
    BJetSelection              = "&& nB2Jet60==2" # "&& (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    BVetoSelection             = "&& nB2Jet60==0" # "&& (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    configMgr.cutsDict["SR1L2Ba"] = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict["CRWbb"] = "(met>150 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>200 && ht_wo2jets<50 && mctcorr>150)" + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && ht_wo2jets<50 && mctcorr<150)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met<200 && met>150 && ht_wo2jets<50 && mctcorr>150)" + JetSelection + BJetSelection + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>200 && ht_wo2jets<50 && mctcorr>150)"            + JetSelection + BJetSelection + HardLeptonSelection
    pass
elif chn==3:
    JetSelection               = "&& (nJet50_jvf25_wo2jets==0 && dphimin>0.4)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)" 
    BJetSelection              = "&& nB2Jet60==2" # "&&  (jetMV1[0]>0.98 && jetMV1[1]>0.98)"
    BVetoSelection             = "&& nB2Jet60==0"
    configMgr.cutsDict["SR1L2Bc"] = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + LeptonSelection 
    configMgr.cutsDict["CRWbb"] = "(met>150 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(jet1Pt>60 && jet2Pt>60 && nJet50_jvf25_wo2jets==0 && dphimin>0.4) && (met>300 && ht_wo2jets<50 && mctcorr>200)" + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && mctcorr<200)"            + JetSelection + BJetSelection + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<300 && mctcorr>200)" + JetSelection + BJetSelection + LeptonSelection 
    configMgr.cutsDict["VR2"]  = "(met>300 && mctcorr>200)"            + JetSelection + BJetSelection + HardLeptonSelection
    pass
elif chn==4:
    # (lep1Pt<25. && met>300. && mt>100. && jet1Pt_jvf25>180. && nJet25_jvf25>=3 && jet1IsB_MV1_772_jvf25 && nBJet25_MV1_772_jvf25>0 && met/meffInc25_jvf25>0.36 );
    JetSelection               = "&& (jet1Pt_jvf25>180 && nJet40_jvf25>=3)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)" 
    BJetSelection              = "&& jet1IsB_MV1_772_jvf25==0 && nBJet40_MV1_772_jvf25>0" # (jetMV1[1]>0.772 || jetMV1[2]>0.772))" # nBJet40>0
    BVetoSelection             = "&& (nBJet40_MV1_772_jvf25==0)"
    #BJetSelection = "&& (jetMV1[0]<0.772 && nBJet40>0)" # (jetMV1[1]>0.772 || jetMV1[2]>0.772))" # nBJet40>0
    configMgr.cutsDict["SR1L1Ba"] = "(met>250 && met/meffInc40_jvf25>0.32 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["CRWbb"] = "(met>150 && met/meffInc40_jvf25>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>250 && met/meffInc40_jvf25>0.32 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met/meffInc40_jvf25>0.32 && mt>100)"            + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<250 && met/meffInc40_jvf25>0.32 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met/meffInc40_jvf25>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    pass
elif chn==5:
    JetSelection               = "&& (jet1Pt_jvf25>180 && nJet25_jvf25>=3)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25 && jetJVF[2]>0.25)"
    BJetSelection              = "&& jet1IsB_MV1_772_jvf25==0 && nBJet25_MV1_772_jvf25>0" # "&& (jetMV1[0]>0.772 || jetMV1[1]>0.772 || jetMV1[2]>0.772)" # nBJet40>0
    BVetoSelection             = "&& (nBJet25_MV1_772_jvf25==0)"
    configMgr.cutsDict["SR1L1Bc"] = "(met>300 && met/meffInc25_jvf25>0.32 && mt>100)"            + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["CRWbb"] = "(met>150 && met/meffInc25_jvf25>0.32 && mt>40 && mt<80)"    + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>300 && met/meffInc25_jvf25>0.32 && mt>40 && mt<80)"    + JetSelection + BVetoSelection + HardLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met/meffInc25_jvf25>0.32 && mt>100)"            + JetSelection + BJetSelection  + HardLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<300 && met/meffInc25_jvf25>0.32 && mt>100)" + JetSelection + BJetSelection  + LeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met/meffInc25_jvf25>0.32 && mt>80 && mt<100)"   + JetSelection + BJetSelection  + HardLeptonSelection
    pass
elif chn==6: # soft 2-lepton analysis
    DiLeptonSelection          = TriggerSelection + "&& (lep1Pt>6 && lep2Pt>6) && (lep1Flavor==-1) && (lep2Flavor==-1) && (lep1Charge*lep2Charge<0)" ## os dimuon
    JetSelection               = "&& (jet1Pt_jvf25>100 && jet2Pt_jvf25>25)" # && jetJVF[0]>0.25 && jetJVF[1]>0.25)"
    # these B-jet selections are okay
    BVetoSelection             = "&& nB3Jet25_MV1_122_jvf25==0 " ## !(jetMV1[0]>0.595 || jetMV1[1]>0.595 || (jet3Pt>40 && jetJVF[2]>0.25 && jetMV1[2]>0.595))" # 75%
    BJetSelection              = "&& nB3Jet25_MV1_122_jvf25>0  " ## (jetMV1[0]>0.595 || jetMV1[1]>0.595 || (jet3Pt>40 && jetJVF[2]>0.25 && jetMV1[2]>0.595))"
    configMgr.cutsDict["SR2L"] = "(met>200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6 && lep1Pt<25 && met/meffInc25_jvf25>0.3)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["CRW"]   = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep2Pt>25)" + JetSelection + BVetoSelection + DiLeptonSelection
    configMgr.cutsDict["CRT"]   = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep2Pt>25)" + JetSelection + DiLeptonSelection
    configMgr.cutsDict["VR1"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>25  && lep2Pt<25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict["VR2"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>6   && lep1Pt<25)" + JetSelection  + DiLeptonSelection
    configMgr.cutsDict["VR3"]  = "(met>150 && met<200 && mll>15 && abs(mll-91.2)>10 && lep1Pt>25  && lep2Pt<25)" + JetSelection  + BVetoSelection + DiLeptonSelection
    pass

#############
## Samples ##
#############

## not yet there ...
## to add AlpgenDY and ttbarV
AlpgenDYSample = Sample("AlpgenDY",kOrange-8)
AlpgenDYSample.addSystematic(Systematic("errDY", configMgr.weights, 1.3, 0.7, "user","userOverallSys"))
AlpgenDYSample.setStatConfig(useStat)
AlpgenDYSample.setFileList(AlpgenDYFiles)
AlpgenDYSample.setNormByTheory()

DibosonsSample = Sample("SherpaDibosons",kOrange-8)
DibosonsSample.addSystematic(Systematic("errDB", configMgr.weights, 2.0, 0.01, "user","userOverallSys"))
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setFileList(SherpaDibosonsFiles)
DibosonsSample.setNormByTheory()

ttbarVSample = Sample("ttbarV",kYellow-8)
ttbarVSample.addSystematic(Systematic("errTV", configMgr.weights, 1.3, 0.7, "user","userOverallSys"))
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setFileList(ttbarVFiles)
ttbarVSample.setNormByTheory()

SingleTopSample = Sample("SingleTop",kGreen-5)
SingleTopSample.addSystematic(Systematic("errST", configMgr.weights, 1.3, 0.7, "user","userOverallSys"))
SingleTopSample.setStatConfig(useStat)
SingleTopSample.setFileList(SingleTopFiles)
SingleTopSample.setNormByTheory()
if chn==6:
    SingleTopSample.addSampleSpecificWeight("(DatasetNumber==108346)")

TTbarSample = Sample("PowhegPythiaTTbar",kGreen-9)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
TTbarSample.setFileList(PowhegPythiaTTbarFiles)
if (chn>=0 and chn<=5) and chn!=2:
    TTbarSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
elif chn==2:
    TTbarSample.setNormRegions([("CRT","cuts")])
elif chn==6:
    TTbarSample.setNormRegions([("CRT","cuts")])
if chn==6:
    TTbarSample.addSampleSpecificWeight("(DecayIndexTTbar==1)")
    pass


if chn>=2 and chn<=5: # stop
    WSampleName = "SherpaWMassiveB"
else:
    WSampleName = "AlpgenW"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_WZ",1.,0.,5.)
WSample.setStatConfig(useStat)
if (chn>=0 and chn<=5) and chn!=2:
    WSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
elif chn==2:
    WSample.setNormRegions([("CRW","cuts")])
if chn>=2 and chn<=5: # stop
    WSample.setFileList(SherpaWMassiveBFiles) 
else:
    WSample.setFileList(AlpgenWFiles) 

if chn>=2 and chn<=5: # stop
    ZSampleName = "SherpaZMassiveB"
else:
    ZSampleName = "AlpgenZ"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setNormFactor("mu_WZ",1.,0.,5.)
ZSample.setStatConfig(useStat)
ZSample.addSystematic(Systematic("errZ", configMgr.weights, 2.0, 0.01, "user","userOverallSys"))
if (chn>=0 and chn<=5) and chn!=2:
    ZSample.setNormRegions([("CRT","cuts"),("CRW","cuts")])
elif chn==2:
    ZSample.setNormRegions([("CRW","cuts")])
if chn>=2 and chn<=5: # stop
    ZSample.setFileList(SherpaZMassiveBFiles)
else:
    ZSample.setFileList(AlpgenZFiles) ###(SherpaZMassiveBFiles)

QCDSample = Sample("QCD",kGray+1)
QCDSample.setQCD(True,"histoSys")
QCDSample.setStatConfig(False)
QCDSample.setFileList(qcdFiles)

DataSample = Sample("Data",kBlack)
DataSample.setData()
DataSample.setFileList(dataFiles)


#######################
## Systematics (1/2) ##
#######################

bkgMCSamples = [DibosonsSample,SingleTopSample,TTbarSample,ZSample,ttbarVSample,AlpgenDYSample]
if chn!=6: bkgMCSamples += [WSample] # absorbed in QCD

if includeSyst:
    SetupSamples( bkgMCSamples, SystList )
    #SetupSamples( [TTbarSample], [qfacT,ktfacT] )
    if (chn>=0 and chn<=1) or chn==6: SetupSamples( [WSample], [qfacW,ktfacW,iqoptW] )

## more systematics below

##################
# The fit setup  #
##################

# First define HistFactory attributes
configMgr.analysisName   = "SoftLeptonMoriond2013_"+anaName+"_"+grid+gridspec+allpoints[0] # Name to give the analysis
configMgr.outputFileName = "results/"+configMgr.analysisName+".root"
configMgr.histCacheFile  = "data/"+configMgr.analysisName+".root"

for point in allpoints:
    if point=="": continue

    # Fit config instance
    name="Fit_"+anaName+"_"+grid+gridspec+point
    myFitConfig = configMgr.addFitConfig(name)
    if useStat:
        myFitConfig.statErrThreshold=0.05
    else:
        myFitConfig.statErrThreshold=None

    #Add Measurement
    meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.037)
    if myFitType==FitType.Background: meas.addPOI("mu_SIG")
    #meas.addPOI("mu_SIG")
    #meas.addParamSetting("mu_Diboson",True,1) # fix diboson to MC prediction
    #meas.addParamSetting("alpha_QCDNorm_WR",True,0)
    #meas.addParamSetting("alpha_QCDNorm_TR",True,0)
    #if chn==1:
    #    meas.addParamSetting("alpha_QCDNorm_WR5",True,0)
    #    meas.addParamSetting("alpha_QCDNorm_TR5",True,0)
    #meas.addParamSetting("alpha_QCDNorm_SS",True,0)
    if chn==6:
        meas.addParamSetting("mu_WZ",True,1) # fix diboson to MC prediction

    # x-section uncertainties stop and ued
    if chn>=2 and chn<=5:
        xsecSig = Systematic("SigXSec", configMgr.weights, 1.16, 0.84, "user", "userOverallSys")
        pass
    if chn==6:
        xsecSig = Systematic("SigXSec", configMgr.weights, 1.25, 0.75, "user", "userOverallSys")
        pass
    
    # ISR uncertainty (SS and GG grids)
    if myFitType==FitType.Exclusion:
      if (chn>=0 and chn<=1):
        massSet = point.split('_')
        if len(massSet)!=3: 
            log.fatal("Invalid grid point: %s" % point)
        DeltaM = float(massSet[0]) - float(massSet[2])
        if DeltaM<=0: 
            log.fatal("Invalid value of DeltaM : %f" % DeltaM)
        #
        if grid=="SM_GG1step": 
            eisr3 = exp(-1.4-0.013*DeltaM)
            eisr5 = exp(-1.2-0.005*DeltaM)
            if eisr3<0.06: eisr3=0.06
            if eisr5<0.06: eisr5=0.06
            pass
        elif grid=="SM_SS1step": 
            eisr3 = 0.06+exp(0.8-0.1*DeltaM)
            eisr5 = 0.06+exp(-1.5-0.005*DeltaM)
            pass
        isr3j = Systematic("isr", configMgr.weights, 1.+eisr3, 1.00-eisr3, "user", "userOverallSys")
        isr5j = Systematic("isr", configMgr.weights, 1.+eisr5, 1.00-eisr5, "user", "userOverallSys")
        pass
    
    #-------------------------------------------------
    # First add the (grid point specific) signal sample
    #-------------------------------------------------
    sigSampleName=grid+"_"+point
    if myFitType==FitType.Exclusion:
        sigSample = Sample(sigSampleName,kRed)
        sigSample.setFileList(sigFiles)
        #sigSample.setTreeName(grid+"_"+point+suffix)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1,0.,100.)
        SetupSamples( [sigSample], SystList+[xsecSig] ) ## systematics
        #sigSample.addSystematic(xsecSig)      ## systematic not working?
        sigSample.setStatConfig(useStat)
        myFitConfig.addSamples(sigSample)
        myFitConfig.setSignalSample(sigSample)
        meas.addPOI("mu_SIG")

    myFitConfig.addSamples(bkgMCSamples+[QCDSample,DataSample]) # DibosonsSample

    #MyConfigExample.py:nJetWS.hasBQCD = False
    #MyConfigExample.py:nJetTS.hasBQCD = True

    ## TR sofar defined for every channel
    TR = myFitConfig.addChannel("cuts",["CRT"],1,0.5,1.5)
    myFitConfig.setBkgConstrainChannels(TR)
    ###if chn!=6:
    TR.hasBQCD = True # b-tag applied
    BRset = [TR] 
    ## WR = second control region
    ##########if chn!=6: # di-lepton region only has control region
    if chn>=0 and chn<=5:
        WR = myFitConfig.addChannel("cuts",["CRW"],1,0.5,1.5)
        myFitConfig.setBkgConstrainChannels(WR)
        BRset = [TR,WR]
    if chn>=2 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            WRbb = myFitConfig.addChannel("cuts",["CRWbb"],1,0.5,1.5)
            WRbb.hasBQCD = True # b-tag applied
            BRset = [TR,WR,WRbb]
            myFitConfig.setValidationChannels(WRbb)


    ## FitType
    SRset = []
    if chn==0:
        SR3j = myFitConfig.addChannel("cuts",["SR1L3j"],1,0.5,1.5)
        SRset = [SR3j]
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
        pass
    elif chn==1:
        SR3j = myFitConfig.addChannel("cuts",["SR1L3j"],1,0.5,1.5)
        SR5j = myFitConfig.addChannel("cuts",["SR1L5j"],1,0.5,1.5)
        WR5j = myFitConfig.addChannel("cuts",["CRW5j"],1,0.5,1.5)
        TR5j = myFitConfig.addChannel("cuts",["CRT5j"],1,0.5,1.5)
        TR5j.hasBQCD = True
        BRset += [WR5j,TR5j]
        myFitConfig.setBkgConstrainChannels(WR5j)
        myFitConfig.setBkgConstrainChannels(TR5j)
        SRset = [SR3j,SR5j]
        if myFitType==FitType.Exclusion:
            SR3j.getSample(sigSampleName).addSystematic(isr3j)
            SR5j.getSample(sigSampleName).addSystematic(isr5j)
        pass
    elif chn==2:
        SR1a = myFitConfig.addChannel("cuts",["SR1L2Ba"],1,0.5,1.5)
        SR1a.hasBQCD = True
        SRset = [SR1a]
        pass
    elif chn==3:
        SR1b = myFitConfig.addChannel("cuts",["SR1L2Bc"],1,0.5,1.5)
        SR1b.hasBQCD = True
        SRset = [SR1b]
        pass
    elif chn==4:
        SR2a = myFitConfig.addChannel("cuts",["SR1L1Ba"],1,0.5,1.5)
        SR2a.hasBQCD = True
        SRset = [SR2a]
        pass
    elif chn==5:
        SR2b = myFitConfig.addChannel("cuts",["SR1L1Bc"],1,0.5,1.5)
        SR2b.hasBQCD = True
        SRset = [SR2b]
        pass
    elif chn==6:
        SR2l = myFitConfig.addChannel("cuts",["SR2L"],1,0.5,1.5)
        SR2l.hasBQCD = True
        SRset = [SR2l]
        pass

    ## systematics (2/2)
    if includeSyst and dobtag:
        if chn==0:
            SetupChannels([WR,TR], [bTagSyst9])
            pass
        if chn==1:
            SetupChannels([WR,TR], [bTagSyst9])
            SetupChannels([WR5j,TR5j], [bTagSyst9])
            pass
        elif chn==2:
            SetupChannels(BRset+SRset, [bTagSyst8]) # VR
            pass
        elif chn==3:
            SetupChannels(BRset+SRset, [bTagSyst8]) # VR
            pass
        elif chn==4:
            SetupChannels(BRset+SRset, [bTagSyst9]) # VR
            pass
        elif chn==5:
            SetupChannels(BRset+SRset, [bTagSyst9]) # VR
            pass
        elif chn==6:
            SetupChannels(BRset+SRset, [bTagSyst11]) # VR1,VR2
            pass

    if chn>=2 and chn<=5:
        if (myFitType==FitType.Background) and doValidation:
            WRbb.getSample(WSampleName).addSystematic(wbb)
    if chn>=2 and chn<=5:
        TR.getSample(WSampleName).addSystematic(wbb)
        for sr in SRset: sr.getSample(WSampleName).addSystematic(wbb)

    if myFitType!=FitType.Background:
        myFitConfig.setSignalChannels(SRset)
    else:
        myFitConfig.setValidationChannels(SRset)

    if myFitType==FitType.Discovery:
        #meas.addParamSetting("Lumi",True,1)
        for SR in SRset:
            SR.addDiscoverySamples([SR.name],[1.],[0.],[100.],[kMagenta])
            meas.addPOI("mu_%s" % SR.name)


    vSet = []
    if doValidation:
        if chn==0:
            v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
            v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
            v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
            vSet = [v0,v3,v6] + [v1,v4,v7] + [v2,v5,v8]
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==1:
            v0  = myFitConfig.addChannel("cuts",["VR3j1"],1,0.5,1.5)
            v1  = myFitConfig.addChannel("cuts",["VRT3j1"],1,0.5,1.5) ; v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VRW3j1"],1,0.5,1.5)
            v3  = myFitConfig.addChannel("cuts",["VR3j2"],1,0.5,1.5)
            v4  = myFitConfig.addChannel("cuts",["VRT3j2"],1,0.5,1.5) ; v4.hasBQCD = True
            v5  = myFitConfig.addChannel("cuts",["VRW3j2"],1,0.5,1.5)
            v6  = myFitConfig.addChannel("cuts",["VR3j3"],1,0.5,1.5)
            v7  = myFitConfig.addChannel("cuts",["VRT3j3"],1,0.5,1.5) ; v7.hasBQCD = True
            v8  = myFitConfig.addChannel("cuts",["VRW3j3"],1,0.5,1.5)
            v9  = myFitConfig.addChannel("cuts",["VR5j1"],1,0.5,1.5)
            v10 = myFitConfig.addChannel("cuts",["VRT5j1"],1,0.5,1.5) ; v10.hasBQCD = True 
            v11 = myFitConfig.addChannel("cuts",["VRW5j1"],1,0.5,1.5)
            v12 = myFitConfig.addChannel("cuts",["VR5j2"],1,0.5,1.5)
            v13 = myFitConfig.addChannel("cuts",["VRT5j2"],1,0.5,1.5) ; v13.hasBQCD = True
            v14 = myFitConfig.addChannel("cuts",["VRW5j2"],1,0.5,1.5)
            v15 = myFitConfig.addChannel("cuts",["VR5j3"],1,0.5,1.5)
            v16 = myFitConfig.addChannel("cuts",["VRT5j3"],1,0.5,1.5) ; v16.hasBQCD = True
            v17 = myFitConfig.addChannel("cuts",["VRW5j3"],1,0.5,1.5)
            vSet = [v0,v3,v6,v9,v12,v15] + [v1,v4,v7,v10,v13,v16] + [v2,v5,v8,v11,v14,v17]
            #SetupChannels(vSet, [jes,jer,pileup,trEff,eglow])
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==2:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==3:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==4:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==5:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            vSet = [v0,v1]
            myFitConfig.setValidationChannels(vSet)
            pass
        if chn==6:
            v0  = myFitConfig.addChannel("cuts",["VR1"],1,0.5,1.5)
            v0.hasBQCD = True
            v1  = myFitConfig.addChannel("cuts",["VR2"],1,0.5,1.5)
            v1.hasBQCD = True
            v2  = myFitConfig.addChannel("cuts",["VR3"],1,0.5,1.5)
            v2.hasBQCD = True
            vSet = [v0,v1,v2]
            myFitConfig.setValidationChannels(vSet)
            pass

    if doValidation and includeSyst and dobtag:
        if chn==0:
            SetupChannels(vSet, [bTagSyst9])
            pass
        if chn==1:
            SetupChannels(vSet, [bTagSyst9])
            pass
        elif chn==2:
            SetupChannels(vSet, [bTagSyst8]) # VR
            pass
        elif chn==3:
            SetupChannels(vSet, [bTagSyst8]) # VR
            pass
        elif chn==4:
            SetupChannels(vSet, [bTagSyst9]) # VR
            pass
        elif chn==5:
            SetupChannels(vSet, [bTagSyst9]) # VR
            pass
        elif chn==6:
            SetupChannels(vSet, [bTagSyst11]) # VR1,VR2
            pass
