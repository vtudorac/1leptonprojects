################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
import ROOT
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import sqrt
import pickle
from os import sys
from math import exp

from logger import Logger
log = Logger('Zmet')

# ********************************************************************* #
# some useful helper functions
# ********************************************************************* #

def myreplace(l1,l2,element):
    idx=l1.index(element)
    if idx>=0:
        return l1[:idx] + l2 + l1[idx+1:]
    else:
        print "WARNING idx negative: %d" % idx
        return l1

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        for syst in systList:
            chan.addSystematic(syst)
    return

def SetupSamples(samples,systList):
    for sample in samples:
        for syst in systList:
            sample.addSystematic(syst)
    return

# ********************************************************************* #
#                              Debug
# ********************************************************************* #

#print sys.argv

# ********************************************************************* #
#                              Analysis parameters
# ********************************************************************* #

if not 'useStat' in dir():
    useStat=False
if not 'chn' in dir():
    chn=0                   # analysis channel 0=A,1=B,..
if not 'grid' in dir():
    grid="" # "SU"               # only grid implemented up to now
if not 'gridspec' in dir():
    gridspec="_" # "SU"               # only grid implemented up to now
if not 'suffix' in dir():
    suffix="_NoSys"
if not 'sigSamples' in dir():
    sigSamples=None #["1000_250"]
if not 'anaName' in dir():
    anaName = 'SRs1L'
if not 'includeSyst' in dir():
    includeSyst = False
if not 'dobtag' in dir():
    dobtag = False
if not 'doBlinding' in dir():
    doBlinding = False
if not 'doSystRemap' in dir():
    doSystRemap = False

doBlinding = True
configMgr.fixSigXSec=True

# ********************************************************************* #
#                              Options
# ********************************************************************* #

    
###FlavourSymSample.setFileList(PowhegPythiaFlavourSymFiles)
    
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    if pickedSRs[0]=="SR3ee":
        chn=0
        nJetSmearSR = [0.56594] 
        ucb_js1 = Systematic("ucb_js1", configMgr.weights,1.3548,0.6452,"user","userOverallSys") # response function
        ucb_js2 = Systematic("ucb_js2", configMgr.weights,1.004,0.996,"user","userOverallSys") # stats. in the seed region
        ucb_js3 = Systematic("ucb_js3", configMgr.weights,1.0299,0.97435,"user","userOverallSys") # significance cut
                
    elif pickedSRs[0]=="SR3mm":
        chn=1
        nJetSmearSR = [0.56594] 
        ucb_js1 = Systematic("ucb_js1", configMgr.weights,1.3548,0.6452,"user","userOverallSys") # response function
        ucb_js2 = Systematic("ucb_js2", configMgr.weights,1.004,0.996,"user","userOverallSys") # stats. in the seed region
        ucb_js3 = Systematic("ucb_js3", configMgr.weights,1.0299,0.97435,"user","userOverallSys") # significance cut
        
    else:
        print "WARNING: analysis not defined"
        sys.exit()
    anaName=pickedSRs[0]
    # set the grid
    if len(pickedSRs) >= 2: 
        grid = pickedSRs[1] 
        pass
    if len(pickedSRs) >= 3: 
        gridspec = '_'+pickedSRs[2]+'_'
        pass

if chn>5 or chn<0:
    print "ERROR analysis not defined!!!"
    print chn
    print pickedSRs
    sys.exit()

if chn>-1:
    # traditional soft 1-lepton analysis
    if grid=="bRPV":
        allpoints=['400_200_30','400_250_30','400_300_30','400_350_30','400_400_30','400_450_30','400_500_30','400_550_30','400_600_30','400_650_30','400_700_30','400_750_30','400_800_30','600_200_30','600_250_30','600_300_30','600_350_30','600_400_30','600_450_30','600_500_30','600_550_30','600_600_30','600_650_30','600_700_30','600_750_30','600_800_30','800_200_30','800_250_30','800_300_30','800_350_30','800_400_30','800_450_30','800_500_30','800_550_30','800_600_30','800_650_30','800_700_30','800_750_30','800_800_30','1000_200_30','1000_250_30','1000_300_30','1000_350_30','1000_400_30','1000_450_30','1000_500_30','1000_550_30','1000_600_30','1000_650_30','1000_700_30','1000_750_30','1000_800_30','1200_200_30','1200_250_30','1200_300_30','1200_350_30','1200_400_30','1200_450_30','1200_500_30','1200_550_30','1200_600_30','1200_650_30','1200_700_30','1200_750_30','1200_800_30','1400_200_30','1400_250_30','1400_300_30','1400_350_30','1400_400_30','1400_450_30','1400_500_30','1400_550_30','1400_600_30','1400_650_30','1400_700_30','1400_750_30','1400_800_30','1600_200_30','1600_250_30','1600_300_30','1600_350_30','1600_400_30','1600_450_30','1600_500_30','1600_550_30','1600_600_30','1600_650_30','1600_700_30','1600_750_30','1600_800_30','1800_200_30','1800_250_30','1800_300_30','1800_350_30','1800_400_30','1800_450_30','1800_500_30','1800_550_30','1800_600_30','1800_650_30','1800_700_30','1800_750_30','1800_800_30','2000_250_30','2000_300_30','2000_350_30','2000_400_30','2000_450_30','2000_500_30','2000_550_30','2000_600_30','2000_650_30','2000_700_30','2000_750_30','2000_800_30','2200_250_30','2200_300_30','2200_350_30','2200_400_30','2200_450_30','2200_500_30','2200_550_30','2200_600_30','2200_650_30']
        pass
    elif grid=="GGM":
        allpoints=['600_120_0','600_150_0','600_200_0','600_300_0','600_400_0','600_500_0','600_590_0','700_120_0','700_150_0','700_200_0','700_300_0','700_400_0','700_500_0','700_600_0','700_690_0','800_120_0','800_150_0','800_200_0','800_300_0','800_400_0','800_500_0','800_600_0','800_700_0','800_790_0','900_120_0','900_150_0','900_200_0','900_300_0','900_400_0','900_500_0','900_600_0','900_700_0','900_800_0','900_890_0','1000_120_0','1000_150_0','1000_200_0','1000_300_0','1000_400_0','1000_500_0','1000_600_0','1000_700_0','1000_800_0','1000_900_0','1000_990_0']
        #allpoints=['400_120_30','400_150_30','400_200_30','400_300_30','400_390_30','500_120_30','500_150_30','500_200_30','500_300_30','500_400_30','500_490_30','600_120_30','600_150_30','600_200_30','600_300_30','600_400_30','600_500_30','600_590_30','700_120_30','700_150_30','700_200_30','700_300_30','700_400_30','700_500_30','700_600_30','700_690_30','800_120_30','800_150_30','800_200_30','800_300_30','800_400_30','800_500_30','800_600_30','800_700_30','800_790_30','900_120_30','900_150_30','900_200_30','900_300_30','900_400_30','900_500_30','900_600_30','900_700_30','900_800_30','900_890_30','1000_120_30','1000_150_30','1000_200_30','1000_300_30','1000_400_30','1000_500_30','1000_600_30','1000_700_30','1000_800_30','1000_900_30','1000_990_30']
        pass
    
# No input signal for discovery and bkg fit
if myFitType==FitType.Discovery:
    allpoints=["Discovery"]
    grid=""
    pass
elif myFitType==FitType.Background:
    allpoints=["Background"]
    grid=""
    pass

# sigSamples is set by the "-g" HistFitter option. Overwrites allpoints, used below.
try:
    sigSamples
except NameError:
    sigSamples = None
    
if sigSamples!=None:
    allpoints=sigSamples

#-------------------------------
# Parameters for hypothesis test
#-------------------------------

#configMgr.doHypoTest=True
#configMgr.nTOYs=-1
#configMgr.calculatorType=0 #toys
configMgr.fixSigXSec=True
configMgr.calculatorType=2 #asimov
configMgr.testStaType=3
configMgr.nPoints=20

# ********************************************************************* #
# Main part - now we start to build the data model
# ********************************************************************* #

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001
configMgr.outputLumi = 20.3   #####20.490 # 14.3 #21.0
configMgr.setLumiUnits("fb-1")

inputDirData_p1328 = "root://eosatlas//eos/atlas/user/e/eromeroa/trees/v8_3_2/"
inputDirBkg_p1328 = "root://eosatlas//eos/atlas/user/e/eromeroa/trees/v8_3_2/"
inputDirSig_p1328  = "root://eosatlas//eos/atlas/user/e/eromeroa/trees/v8_3_2/"


#Split bdgFiles per channel
sigFiles = []
if myFitType==FitType.Exclusion:
    sigFiles += [inputDirSig_p1328+grid+"_mt2lMuo_n8_3_withSys.root"]
    sigFiles += [inputDirSig_p1328+grid+"_mt2lEle_n8_3_withSys.root"]
    if len(sigFiles)==0:
        print "WARNING: signal grid files not defined"
        sys.exit()
    pass

# Set the files to read from
if chn>-1: # soft 1-lepton
    if True:
        dataFiles              =  [inputDirData_p1328+"datatree_mt2lMuo.root"] #"Soft1Lep_Data.root"]
        dataFiles              += [inputDirData_p1328+"datatree_mt2lEle.root"] #"Soft1Lep_Data.root"]
        AlpgenWFiles           =  [inputDirBkg_p1328+"bkgtree_mt2lMuo.root"]
        AlpgenWFiles           += [inputDirBkg_p1328+"bkgtree_mt2lEle.root"]
        AlpgenZFiles           =  [inputDirBkg_p1328+"bkgtree_mt2lMuo.root"]
        AlpgenZFiles           += [inputDirBkg_p1328+"bkgtree_mt2lEle.root"]
        DibosonsFiles          =  [inputDirBkg_p1328+"bkgtree_mt2lMuo.root"]
        DibosonsFiles          += [inputDirBkg_p1328+"bkgtree_mt2lEle.root"]
        PowhegPythiaTTbarFiles =  [inputDirBkg_p1328+"bkgtree_mt2lMuo.root"]
        PowhegPythiaTTbarFiles += [inputDirBkg_p1328+"bkgtree_mt2lEle.root"]
        SingleTopFiles         =  [inputDirBkg_p1328+"bkgtree_mt2lMuo.root"]
        SingleTopFiles         += [inputDirBkg_p1328+"bkgtree_mt2lEle.root"]
        ttbarVFiles            =  [inputDirBkg_p1328+"bkgtree_mt2lMuo.root"]
        ttbarVFiles            += [inputDirBkg_p1328+"bkgtree_mt2lEle.root"]
        pass


    
########################################
# Analysis description
########################################

## Lists of weights 
#configMgr.weights = ("genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight")    ######## turn off April-24 ,"SherpaWweight"]
weights = ["genWeight","leptonWeight","triggerWeight","pileupWeight"]    ######## turn off April-24 ,"SherpaWweight"]
configMgr.weights = weights

#xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
#xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#lepHighWeights = replaceWeight(weights,"leptonWeight","leptonWeightUp")
#lepLowWeights = replaceWeight(weights, "leptonWeight","leptonWeightDown")

#sysWeight_pileupUp   = replaceWeight(weights, "pileupWeight", "pileupWeightUp")
#sysWeight_pileupDown = replaceWeight(weights, "pileupWeight", "pileupWeightDown")


#-------------------------------------------------------------------------
# Blinding of SR
#-------------------------------------------------------------------------

configMgr.blindSR = True  #doBlinding
configMgr.blindVR = True # doBlinding

#--------------------------------------------------------------------------
# List of systematics
#--------------------------------------------------------------------------

configMgr.nomName = "_NoSys"

## JES uncertainty as shapeSys - one systematic per region (combine WR and TR), merge samples
jesSignal = Systematic("JSig",   "_NoSys", "_JESNoCloseByup"    ,"_JESNoCloseBydown"    ,"tree","overallHistoSys")
jes       = Systematic("JES",    "_NoSys", "_JESNoCloseByup"    ,"_JESNoCloseBydown"    ,"tree","overallNormHistoSys") # JES uncertainty - for low pt jets
jer       = Systematic("JER",    "_NoSys", "_JER"      ,"_JER"        ,"tree","overallNormHistoSysOneSideSym")

jlow      = Systematic("JLow","_NoSys","_JESLowup","_JESLowdown","tree","histoSys") # JES uncertainty - for low pt jets
jmed      = Systematic("JMedium","_NoSys","_JESMediumup","_JESMediumdown","tree","histoSys") # JES uncertainty - for medium pt jets
jhigh     = Systematic("JHigh","_NoSys","_JESHighup","_JESHighdown","tree","histoSys") # JES uncertainty - for high pt jets

## MET uncertainty
scalest   = Systematic("SCALEST","_NoSys", "_SCALESTup","_SCALESTdown","tree","overallNormHistoSys")
resost    = Systematic( "RESOST","_NoSys", "_RESOST" ,"_RESOST" ,"tree","overallNormHistoSysOneSideSym")

## pile-up
#pileup = Systematic("pileup", configMgr.weights, sysWeight_pileupUp, sysWeight_pileupDown, "weight", "overallNormHistoSys")
#pileup.allowRemapOfSyst = doSystRemap


## Trigger efficiency --> should be taken from data/mc trigger study
#trEl = Systematic("TEel",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#trMu = Systematic("TEmu",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallSys")
#####trEff= Systematic("TE",configMgr.weights,trigHighWeights,trigLowWeights,"weight","overallNormHistoSys")
#trEff = Systematic("TE",configMgr.weights,1.05,0.95,"user","userOverallSys")

## MC theoretical uncertainties
## Signal XSec uncertainty as overallSys (pure yeild affect) 
#xsecSig = Systematic("SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys" )

## generator level uncertainties
#SystGenW = Systematic("GenW",configMgr.weights,1.20,0.80,"user","userOverallSys")
#SystGenTTbar = Systematic("GenTTbar",configMgr.weights,1.15,0.85,"user","userOverallSys")

# qfacUpWeightW:qfacDownWeightW:ktfacUpWeightW:ktfacDownWeightW

## qcd stat/syst weights automatically picked up.

## Lepton weight uncertainty
#elEff = Systematic("LEel",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys") 
#muEff = Systematic("LEmu",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallSys")
#lepEff= Systematic("LE",configMgr.weights,lepHighWeights,lepLowWeights,"weight","overallHistoSys")
#lepEff.allowRemapOfSyst = doSystRemap

## Electron energy scale uncertainty
#ees = Systematic("LESel","_NoSys","_LESup","_LESdown","tree","overallSys")
#egzee = Systematic("egzee","_NoSys","_EGZEEup","_EGZEEdown","tree","overallSys")
#egmat = Systematic("egmat","_NoSys","_EGMATup","_EGMATdown","tree","overallSys")
#egps  = Systematic("egps", "_NoSys","_EGPSup", "_EGPSdown", "tree","overallSys")
#eglow = Systematic("eglow","_NoSys","_EGLOWup","_EGLOWdown","tree","overallSys")
#egres = Systematic("egres","_NoSys","_EGRESup","_EGRESdown","tree","overallSys")

## Muon energy resolutions
#merm = Systematic("LRMmu","_NoSys","_MMSup","_MMSdown","tree","overallSys")
#meri = Systematic("LRImu","_NoSys","_MIDup","_MIDdown","tree","overallSys")

## muon energy scale systematics
#mes = Systematic("mes","_NoSys","_MSCALEup","_MSCALEdown","tree","overallSys")


generatorSyst = []
generatorSystW = []





SystList = []
if False:
    SystList.append(jes)
    SystList.append(jer)
    SystList.append(lepEff)
    SystList.append(scalest)
    SystList.append(resost)
    SystList.append(pileup)
    SystList.append(trEff)
    SystList.append(egzee)
    SystList.append(egmat)
    SystList.append(egps)
    SystList.append(eglow)
    SystList.append(egres)
    SystList.append(merm)
    SystList.append(meri)
    SystList.append(mes)

## do these per sample/channel
#SystList.append(bTagSyst)
#SystList.append(qfacT)
#SystList.append(qfacW)
#SystList.append(ktfacT)
#SystList.append(ktfacW)
#SystList.append(iqoptW)

#--------------------------------------------------------------------------
# List of channel selections
#--------------------------------------------------------------------------


ObjSelection  = " && lep1Pt>25 && lep2Pt>10 && nJet35>0 && (lep1Charge * lep2Charge < 0) "

ee_channel   = ObjSelection + " && lep1Flavor==1 && lep2Flavor==1 "
mumu_channel = ObjSelection + " && lep1Flavor==-1 && lep2Flavor==-1 "
emu_channel  = ObjSelection + " && (lep1Flavor*lep2Flavor<0) "

Z_mll = " && mll>81 && mll<101 "
sideBand_mll = " && (mll<81 || mll > 101) "


if chn==0:
    configMgr.cutsDict["SR3_ee"]      = "( met>100 && (meffInc35-met)>900 && nJet35>4 )" + ee_channel   + Z_mll
    pass

if chn==1:
    configMgr.cutsDict["SR3_mumu"]    = "( met>100 && (meffInc35-met)>900 && nJet35>4 )" + mumu_channel + Z_mll
    pass


    configMgr.cutsDict["CR3T"]        = "( met<100 && (meffInc35-met)>400 && nJet35>4 )" + emu_channel  + Z_mll


#############
## Samples ##
#############

errHigh = 1.3
errLow  = 0.7

## not yet there ...

DibosonsSample = Sample("Dibosons",kOrange-8)
#DibosonsSample.addSystematic(Systematic("errDB", configMgr.weights, 1.5, 0.5, "user","userOverallSys"))
#DibosonsSample.addSystematic( sherheg )
#DibosonsSample.setStatConfig(useStat)
DibosonsSample.setFileList(DibosonsFiles)
DibosonsSample.addSampleSpecificWeight("eventWeight")
#DibosonsSample.setNormByTheory()


ttbarVSample = Sample("ttbarV",kYellow-8)
#ttbarVSample.setNormFactor("mu_ttV",1.,0.,5.)
#ttbarVSample.addSystematic(Systematic("errTV", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
#ttbarVSample.setStatConfig(useStat)
ttbarVSample.setFileList(ttbarVFiles)
ttbarVSample.addSampleSpecificWeight("eventWeight")
#ttbarVSample.setNormByTheory()
#ttbarVSample.addSystematic(pythalp)

SingleTopSample = Sample("SingleTop",kGreen-5)
#SingleTopSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))
#SingleTopSample.setStatConfig(useStat)
SingleTopSample.setFileList(SingleTopFiles)
SingleTopSample.addSampleSpecificWeight("eventWeight")
#SingleTopSample.setNormByTheory()


TTbarSampleName = 'PowhegPythiaTTbar'
TTbarSample = Sample(TTbarSampleName,kGreen-9)
#TTbarSample.addSystematic(ucb)
TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
#TTbarSample.setStatConfig(useStat)
TTbarSample.setFileList(PowhegPythiaTTbarFiles)
TTbarSample.addSampleSpecificWeight("eventWeight")
#TTbarSample.addSystematic(pdfIntraSyst)
#TTbarSample.addSystematic(pdfInterSyst)
#TTbarSample.mergeOverallSysSet = ['pdfIntra','pdfInter'] ## post-processing
#TTbarSample.addSystematic(pythwig) 
#TTbarSample.addSystematic(pythgen) ## MB: Turned off for now ...

TTbarSample.setNormRegions([("CR3T","cuts")])


WSampleName = "AlpgenJimmyW"
WSample = Sample(WSampleName,kAzure-4)
WSample.setFileList(AlpgenWFiles)
WSample.addSampleSpecificWeight("eventWeight")
#WSample.setNormFactor("mu_WZ",1.,0.,5.)
#WSample.setStatConfig(useStat)
#WSample.addSystematic(pdfIntraSyst)
#WSample.addSystematic(pdfInterSyst)
#WSample.mergeOverallSysSet = ['pdfIntra','pdfInter'] ## post-processing

ZSampleName = "AlpgenJimmyZ"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setFileList(AlpgenZFiles)
ZSample.addSampleSpecificWeight("eventWeight")
ZSample.addSampleSpecificWeight("((DatasetNumber>107669 && DatasetNumber<107676) || (DatasetNumber>117705 && DatasetNumber<117710) || (DatasetNumber>109309 && DatasetNumber<109314))") # only z->tautau; Z->ee and Z->mumu included in jet smearing estimate
#ZSample.setNormFactor("mu_WZ",1.,0.,5.)
#ZSample.setStatConfig(useStat)
#ZSample.addSystematic(Systematic("err", configMgr.weights, errHigh, errLow, "user","userOverallSys"))


#QCDSample = Sample("QCD",kGray+1)
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#QCDSample.setFileList(qcdFiles)
#QCDSample.addSampleSpecificWeight("abs(qcdWeight)<10")

JetSmearingSampleName = 'JetSmearing'
JetSmearingSample = Sample(JetSmearingSampleName,kGreen-9)
if chn==0:
    JetSmearingSample.buildHisto(nJetSmearSR,"SR3_ee","cuts")
if chn==1:
    JetSmearingSample.buildHisto(nJetSmearSR,"SR3_mumu","cuts")
JetSmearingSample.addSystematic(ucb_js1)
JetSmearingSample.addSystematic(ucb_js2)
JetSmearingSample.addSystematic(ucb_js3)


###FlavourSymSample.setFileList(PowhegPythiaFlavourSymFiles)

DataSample = Sample("Data",kBlack)
DataSample.setData()
DataSample.setFileList(dataFiles)


#######################
## Systematics (1/2) ##
#######################

###bkgMCSamples = [DibosonsSample,SingleTopSample,TTbarSample,ZSample,WSample,ttbarVSample]
#bkgMCSamples = [FlavourSymSample,JetSmearingSample]
bkgSamples = [JetSmearingSample,DibosonsSample,WSample,ZSample,ttbarVSample,TTbarSample]
bkgMCSamples = [DibosonsSample,WSample,ZSample,ttbarVSample,TTbarSample]

if includeSyst:
    SetupSamples( bkgMCSamples, SystList )
    #SetupSamples( [TTbarSample], [qfacT,ktfacT] )
    #SetupSamples( [WSample], [qfacW,ktfacW] )

## more systematics below

##################
# The fit setup  #
##################

# First define HistFactory attributes
configMgr.analysisName   = "Zmet_SR3_NormMethod__"+anaName+"_"+grid+gridspec+allpoints[0] # Name to give the analysis
configMgr.outputFileName = "results/"+configMgr.analysisName+".root"
configMgr.histCacheFile  = "data/"+configMgr.analysisName+".root"

for point in allpoints:
    if point=="": continue

    # Fit config instance
    name="Fit_"+anaName+"_"+grid+gridspec+point
    myFitConfig = configMgr.addFitConfig(name)
    #if useStat:
    #    myFitConfig.statErrThreshold=0.05
    #else:
    #    myFitConfig.statErrThreshold=None

    #Add Measurement
    #meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.036)
    meas=myFitConfig.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.028)
    #meas.addParamSetting("alpha_pdfInter",True,0)
    
    if myFitType==FitType.Background: 
        meas.addPOI("mu_SIG")
        meas.addParamSetting("Lumi",True,1)
        #meas.addParamSetting("gamma_stat_CRW_cuts_bin_0",True,1)

    #meas.addParamSetting("mu_Diboson",True,1) # fix diboson to MC prediction

    
    #-------------------------------------------------
    # First add the (grid point specific) signal sample
    #-------------------------------------------------
    sigSampleName=grid+"_"+point
    if myFitType==FitType.Exclusion:
        sigSample = Sample(sigSampleName,kRed)
        sigSample.setFileList(sigFiles)
        #sigSample.setTreeName(grid+"_"+point+suffix)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1,0.,100.)
        #SetupSamples( [sigSample], SystList+[xsecSig] ) ## systematics
        #sigSample.addSystematic(xsecSig)      ## systematic not working?
        sigSample.setStatConfig(useStat)
        #sigSample.mergeOverallSysSet = ['SigXSec','isr'] ## post-processing
        myFitConfig.addSamples(sigSample)
        myFitConfig.setSignalSample(sigSample)
        meas.addPOI("mu_SIG")
    
    ## TR so far defined for every channel
        TR = myFitConfig.addChannel("cuts",["CR3T"],1,0.5,1.5)
        
    #TR.hasBQCD = True # b-tag applied
    ###myFitConfig.setBkgConstrainChannels(TR)
    ###if chn!=6:
    ###BRset = [TR] 

    ## FitType
    SRset = []
    VSRset = []
    if chn==0:
        SR3ee = myFitConfig.addChannel("cuts",["SR3_ee"],1,0.5,1.5)
        SRset = [SR3ee]
    if chn==1:
        SR3mm = myFitConfig.addChannel("cuts",["SR3_mm"],1,0.5,1.5)
        SRset = [SR3mm]
   
        #if myFitType==FitType.Exclusion:
        #    SR1ee.getSample(sigSampleName).addSystematic(isr3j)
        #    SR1mm.getSample(sigSampleName).addSystematic(isr3j)
        #pass

    ## here we add the other pythwig systematics
    #TTbarSample.addSystematic(pythwig)
    ## here we propagate the systematics 
    myFitConfig.addSamples(bkgSamples+[DataSample]) # DibosonsSample


    ## Fit type specifics
    if myFitType!=FitType.Background:
        myFitConfig.setSignalChannels(SRset)
    else:
        for sr in SRset:
            sr.doBlindingOverwrite = doBlinding
        myFitConfig.setValidationChannels(SRset)
    if myFitType==FitType.Discovery:
        #meas.addParamSetting("Lumi",True,1)
        for SR in SRset:
            SR.addDiscoverySamples([SR.name],[1.],[0.],[100.],[kMagenta])
            meas.addPOI("mu_%s" % SR.name)

