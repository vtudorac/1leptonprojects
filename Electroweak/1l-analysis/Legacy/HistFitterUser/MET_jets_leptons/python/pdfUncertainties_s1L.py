import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr


# estimated with v12_1_5 samples


prefixTheo = "s1L_"
#prefixTheo = ""

PDF_TTBAR_CRT1L1Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08451, 0.923263, "user", "userNormHistoSys")  # inter=+/-0.0300751 intra(up)=+0.0789741 intra(down)=-0.0705982
PDF_TTBAR_CRW1L1Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08777, 0.921426, "user", "userNormHistoSys")  # inter=+/-0.0223204 intra(up)=+0.0848891 intra(down)=-0.0753366
PDF_TTBAR_SR1L1Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08323, 0.922571, "user", "userNormHistoSys")  # inter=+/-0.031649 intra(up)=+0.0769751 intra(down)=-0.0706656
PDF_TTBAR_SR1L1Badis=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0805, 0.925667, "user", "userNormHistoSys")  # inter=+/-0.0295597 intra(up)=+0.0748727 intra(down)=-0.0682024
PDF_TTBAR_VR1L1Ba1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08613, 0.921027, "user", "userNormHistoSys")  # inter=+/-0.0283282 intra(up)=+0.0813354 intra(down)=-0.0737176
PDF_TTBAR_VR1L1Ba2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0836, 0.925482, "user", "userNormHistoSys")  # inter=+/-0.0270614 intra(up)=+0.0791003 intra(down)=-0.0694304
PDF_TTBAR_CRT1L1Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08657, 0.920715, "user", "userNormHistoSys")  # inter=+/-0.0287399 intra(up)=+0.0816635 intra(down)=-0.0738929
PDF_TTBAR_CRW1L1Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09264, 0.918017, "user", "userNormHistoSys")  # inter=+/-0.021664 intra(up)=+0.0900662 intra(down)=-0.0790692
PDF_TTBAR_SR1L1Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08572, 0.917445, "user", "userNormHistoSys")  # inter=+/-0.0350723 intra(up)=+0.0782114 intra(down)=-0.0747347
PDF_TTBAR_SR1L1Bcdis=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0819, 0.921464, "user", "userNormHistoSys")  # inter=+/-0.0320879 intra(up)=+0.0753497 intra(down)=-0.0716815
PDF_TTBAR_VR1L1Bc1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09426, 0.914974, "user", "userNormHistoSys")  # inter=+/-0.0258983 intra(up)=+0.0906344 intra(down)=-0.0809857
PDF_TTBAR_VR1L1Bc2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08925, 0.92137, "user", "userNormHistoSys")  # inter=+/-0.0223487 intra(up)=+0.0864074 intra(down)=-0.0753866
PDF_TTBAR_CRT1L2Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09315, 0.918057, "user", "userNormHistoSys")  # inter=+/-0.024141 intra(up)=+0.0899689 intra(down)=-0.0783064
PDF_TTBAR_CRW1L2Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09638, 0.917067, "user", "userNormHistoSys")  # inter=+/-0.0191752 intra(up)=+0.0944537 intra(down)=-0.0806856
PDF_TTBAR_SR1L2Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08774, 0.920689, "user", "userNormHistoSys")  # inter=+/-0.0250381 intra(up)=+0.0840933 intra(down)=-0.0752549
PDF_TTBAR_SR1L2Badis=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0844, 0.923526, "user", "userNormHistoSys")  # inter=+/-0.026281 intra(up)=+0.0802001 intra(down)=-0.0718165
PDF_TTBAR_VR1L2Ba=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0715, 0.932978, "user", "userNormHistoSys")  # inter=+/-0.0294266 intra(up)=+0.0651642 intra(down)=-0.0602166
PDF_TTBAR_CRT1L2Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0982, 0.911356, "user", "userNormHistoSys")  # inter=+/-0.0261213 intra(up)=+0.0946649 intra(down)=-0.0847077
PDF_TTBAR_CRW1L2Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11567, 0.900137, "user", "userNormHistoSys")  # inter=+/-0.0192153 intra(up)=+0.114062 intra(down)=-0.0979973
PDF_TTBAR_SR1L2Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11394, 0.89735, "user", "userNormHistoSys")  # inter=+/-0.0230646 intra(up)=+0.111581 intra(down)=-0.100025
PDF_TTBAR_SR1L2Bcdis=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11645, 0.902603, "user", "userNormHistoSys")  # inter=+/-0.020824 intra(up)=+0.114571 intra(down)=-0.095145
PDF_TTBAR_VR1L2Bc=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10994, 0.900554, "user", "userNormHistoSys")  # inter=+/-0.0231526 intra(up)=+0.107477 intra(down)=-0.096713
PDF_TTBAR_CRT1L3J=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09315, 0.914753, "user", "userNormHistoSys")  # inter=+/-0.0269321 intra(up)=+0.0891695 intra(down)=-0.0808811
PDF_TTBAR_CRW1L3J=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10285, 0.911587, "user", "userNormHistoSys")  # inter=+/-0.0184914 intra(up)=+0.101171 intra(down)=-0.0864572
PDF_TTBAR_SR1L3J=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08711, 0.922378, "user", "userNormHistoSys")  # inter=+/-0.0178495 intra(up)=+0.0852667 intra(down)=-0.0755417
PDF_TTBAR_SR1L3Jdis=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08375, 0.925119, "user", "userNormHistoSys")  # inter=+/-0.0173695 intra(up)=+0.0819277 intra(down)=-0.0728387
PDF_TTBAR_VR1L3J1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09029, 0.918615, "user", "userNormHistoSys")  # inter=+/-0.025313 intra(up)=+0.0866712 intra(down)=-0.0773485
PDF_TTBAR_VR1L3J2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10346, 0.907589, "user", "userNormHistoSys")  # inter=+/-0.0232189 intra(up)=+0.100819 intra(down)=-0.0894468
PDF_TTBAR_VR1L3J3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11948, 0.898391, "user", "userNormHistoSys")  # inter=+/-0.0200213 intra(up)=+0.11779 intra(down)=-0.0996169
PDF_TTBAR_VRW1L3J1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09648, 0.917675, "user", "userNormHistoSys")  # inter=+/-0.021142 intra(up)=+0.0941371 intra(down)=-0.0795636
PDF_TTBAR_VRW1L3J2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10829, 0.905027, "user", "userNormHistoSys")  # inter=+/-0.0199362 intra(up)=+0.106436 intra(down)=-0.0928568
PDF_TTBAR_VRW1L3J3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11646, 0.899958, "user", "userNormHistoSys")  # inter=+/-0.0249042 intra(up)=+0.113762 intra(down)=-0.0968922
PDF_TTBAR_VRT1L3J1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08852, 0.918797, "user", "userNormHistoSys")  # inter=+/-0.0267393 intra(up)=+0.0843825 intra(down)=-0.0766738
PDF_TTBAR_VRT1L3J2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10156, 0.908548, "user", "userNormHistoSys")  # inter=+/-0.0245939 intra(up)=+0.0985334 intra(down)=-0.0880831
PDF_TTBAR_VRT1L3J3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.12142, 0.897334, "user", "userNormHistoSys")  # inter=+/-0.0177042 intra(up)=+0.12012 intra(down)=-0.101128
PDF_TTBAR_CRT1L5J=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09129, 0.91554, "user", "userNormHistoSys")  # inter=+/-0.0295861 intra(up)=+0.0863636 intra(down)=-0.0791083
PDF_TTBAR_CRW1L5J=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09069, 0.917453, "user", "userNormHistoSys")  # inter=+/-0.0266586 intra(up)=+0.0866796 intra(down)=-0.0781233
PDF_TTBAR_SR1L5J=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08833, 0.920692, "user", "userNormHistoSys")  # inter=+/-0.0305372 intra(up)=+0.0828859 intra(down)=-0.0731935
PDF_TTBAR_SR1L5Jdis=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08673, 0.919062, "user", "userNormHistoSys")  # inter=+/-0.0325826 intra(up)=+0.0803773 intra(down)=-0.0740897
PDF_TTBAR_VR1L5J1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0854, 0.92121, "user", "userNormHistoSys")  # inter=+/-0.0304506 intra(up)=+0.0797829 intra(down)=-0.0726675
PDF_TTBAR_VR1L5J2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.1011, 0.908196, "user", "userNormHistoSys")  # inter=+/-0.0262524 intra(up)=+0.0976351 intra(down)=-0.0879703
PDF_TTBAR_VR1L5J3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.12245, 0.897542, "user", "userNormHistoSys")  # inter=+/-0.0213328 intra(up)=+0.120579 intra(down)=-0.100212
PDF_TTBAR_VRT1L5J1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08481, 0.921329, "user", "userNormHistoSys")  # inter=+/-0.0315594 intra(up)=+0.078723 intra(down)=-0.0720638
PDF_TTBAR_VRT1L5J2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10031, 0.908218, "user", "userNormHistoSys")  # inter=+/-0.0274813 intra(up)=+0.0964762 intra(down)=-0.0875711
PDF_TTBAR_VRT1L5J3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11787, 0.89825, "user", "userNormHistoSys")  # inter=+/-0.0211313 intra(up)=+0.115957 intra(down)=-0.0995314
PDF_TTBAR_VRW1L5J1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08872, 0.92018, "user", "userNormHistoSys")  # inter=+/-0.0258655 intra(up)=+0.0848697 intra(down)=-0.0755128
PDF_TTBAR_VRW1L5J2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10452, 0.907807, "user", "userNormHistoSys")  # inter=+/-0.0217662 intra(up)=+0.102225 intra(down)=-0.0895863
PDF_TTBAR_VRW1L5J3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.14145, 0.894485, "user", "userNormHistoSys")  # inter=+/-0.02206 intra(up)=+0.139718 intra(down)=-0.103183
PDF_TTBAR_CRT1L3JIncl=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09492, 0.912714, "user", "userNormHistoSys")  # inter=+/-0.0275665 intra(up)=+0.0908248 intra(down)=-0.0828189
PDF_TTBAR_CRW1L3JIncl=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09896, 0.913035, "user", "userNormHistoSys")  # inter=+/-0.0218174 intra(up)=+0.0965282 intra(down)=-0.0841842
PDF_TTBAR_SR1L3JIncl=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08481, 0.925536, "user", "userNormHistoSys")  # inter=+/-0.0251384 intra(up)=+0.0809974 intra(down)=-0.0700923
PDF_TTBAR_VR1L3JIncl1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0893, 0.917635, "user", "userNormHistoSys")  # inter=+/-0.0285497 intra(up)=+0.0846148 intra(down)=-0.0772589
PDF_TTBAR_VR1L3JIncl2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10078, 0.909799, "user", "userNormHistoSys")  # inter=+/-0.0237851 intra(up)=+0.0979311 intra(down)=-0.0870086
PDF_TTBAR_VR1L3JIncl3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10931, 0.902929, "user", "userNormHistoSys")  # inter=+/-0.0227841 intra(up)=+0.106912 intra(down)=-0.0943593
PDF_TTBAR_VRW1L3JIncl1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09347, 0.917159, "user", "userNormHistoSys")  # inter=+/-0.0248054 intra(up)=+0.090123 intra(down)=-0.0790397
PDF_TTBAR_VRW1L3JIncl2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.10643, 0.908653, "user", "userNormHistoSys")  # inter=+/-0.0163281 intra(up)=+0.105173 intra(down)=-0.0898759
PDF_TTBAR_VRW1L3JIncl3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.11083, 0.903914, "user", "userNormHistoSys")  # inter=+/-0.0200349 intra(up)=+0.109001 intra(down)=-0.0939737
PDF_TTBAR_VRT1L3JIncl1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08843, 0.917674, "user", "userNormHistoSys")  # inter=+/-0.0294988 intra(up)=+0.0833694 intra(down)=-0.0768594
PDF_TTBAR_VRT1L3JIncl2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09952, 0.909911, "user", "userNormHistoSys")  # inter=+/-0.0258905 intra(up)=+0.0960901 intra(down)=-0.086289
PDF_TTBAR_VRT1L3JIncl3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.1089, 0.902594, "user", "userNormHistoSys")  # inter=+/-0.0236355 intra(up)=+0.106306 intra(down)=-0.094495
PDF_WJETS_CRT1L1Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11596, 0.888973, "user", "userNormHistoSys")  # inter=+/-0.095987 intra(up)=+0.0650559 intra(down)=-0.0557978
PDF_WJETS_CRW1L1Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10364, 0.899462, "user", "userNormHistoSys")  # inter=+/-0.0911683 intra(up)=+0.0492809 intra(down)=-0.0423819
PDF_WJETS_SR1L1Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10613, 0.900738, "user", "userNormHistoSys")  # inter=+/-0.0754804 intra(up)=+0.0746052 intra(down)=-0.0644641
PDF_WJETS_SR1L1Badis=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11818, 0.889255, "user", "userNormHistoSys")  # inter=+/-0.0797972 intra(up)=+0.0871744 intra(down)=-0.0767914
PDF_WJETS_VR1L1Ba1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11754, 0.89663, "user", "userNormHistoSys")  # inter=+/-0.0854619 intra(up)=+0.080703 intra(down)=-0.0581519
PDF_WJETS_VR1L1Ba2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10155, 0.917573, "user", "userNormHistoSys")  # inter=+/-0.0611655 intra(up)=+0.0810671 intra(down)=-0.0552546
PDF_WJETS_CRT1L1Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11518, 0.88898, "user", "userNormHistoSys")  # inter=+/-0.0990454 intra(up)=+0.0588 intra(down)=-0.0501549
PDF_WJETS_CRW1L1Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10075, 0.900837, "user", "userNormHistoSys")  # inter=+/-0.0925115 intra(up)=+0.0398932 intra(down)=-0.0357057
PDF_WJETS_SR1L1Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11289, 0.897995, "user", "userNormHistoSys")  # inter=+/-0.0693954 intra(up)=+0.0890456 intra(down)=-0.0747621
PDF_WJETS_SR1L1Bcdis=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10174, 0.914103, "user", "userNormHistoSys")  # inter=+/-0.0217238 intra(up)=+0.0993951 intra(down)=-0.0831041
PDF_WJETS_VR1L1Bc1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10988, 0.899222, "user", "userNormHistoSys")  # inter=+/-0.0848405 intra(up)=+0.0698217 intra(down)=-0.054391
PDF_WJETS_VR1L1Bc2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10194, 0.90506, "user", "userNormHistoSys")  # inter=+/-0.083024 intra(up)=+0.0591483 intra(down)=-0.0460505
PDF_WJETS_CRT1L2Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.07266, 0.926435, "user", "userNormHistoSys")  # inter=+/-0.0576701 intra(up)=+0.0442012 intra(down)=-0.0456728
PDF_WJETS_CRW1L2Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.0976, 0.903098, "user", "userNormHistoSys")  # inter=+/-0.0913074 intra(up)=+0.0344712 intra(down)=-0.0324479
PDF_WJETS_SR1L2Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.07655, 0.922422, "user", "userNormHistoSys")  # inter=+/-0.0666502 intra(up)=+0.0376456 intra(down)=-0.039701
PDF_WJETS_SR1L2Badis=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.07594, 0.922906, "user", "userNormHistoSys")  # inter=+/-0.0666956 intra(up)=+0.0363119 intra(down)=-0.0386673
PDF_WJETS_VR1L2Ba=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.07989, 0.915808, "user", "userNormHistoSys")  # inter=+/-0.0739264 intra(up)=+0.0302965 intra(down)=-0.0402882
PDF_WJETS_CRT1L2Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.08103, 0.921086, "user", "userNormHistoSys")  # inter=+/-0.0654307 intra(up)=+0.04779 intra(down)=-0.0441156
PDF_WJETS_CRW1L2Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1019, 0.903031, "user", "userNormHistoSys")  # inter=+/-0.0842359 intra(up)=+0.0573451 intra(down)=-0.0480341
PDF_WJETS_SR1L2Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.07715, 0.923572, "user", "userNormHistoSys")  # inter=+/-0.0445213 intra(up)=+0.0630115 intra(down)=-0.0621221
PDF_WJETS_SR1L2Bcdis=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.07655, 0.924448, "user", "userNormHistoSys")  # inter=+/-0.0434684 intra(up)=+0.063007 intra(down)=-0.0617944
PDF_WJETS_VR1L2Bc=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.06596, 0.929901, "user", "userNormHistoSys")  # inter=+/-0.0551695 intra(up)=+0.0361456 intra(down)=-0.0432456
PDF_WJETS_CRT1L3J=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.09981, 0.903555, "user", "userNormHistoSys")  # inter=+/-0.086297 intra(up)=+0.0501432 intra(down)=-0.0430629
PDF_WJETS_CRW1L3J=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10305, 0.898202, "user", "userNormHistoSys")  # inter=+/-0.0943713 intra(up)=+0.0413807 intra(down)=-0.0381685
PDF_WJETS_SR1L3J=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11047, 0.895601, "user", "userNormHistoSys")  # inter=+/-0.0728362 intra(up)=+0.0830541 intra(down)=-0.0747932
PDF_WJETS_SR1L3Jdis=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11047, 0.895601, "user", "userNormHistoSys")  # inter=+/-0.0728362 intra(up)=+0.0830541 intra(down)=-0.0747932
PDF_WJETS_VR1L3J1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10449, 0.897159, "user", "userNormHistoSys")  # inter=+/-0.0959377 intra(up)=+0.0414003 intra(down)=-0.0370422
PDF_WJETS_VR1L3J2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10337, 0.899095, "user", "userNormHistoSys")  # inter=+/-0.091761 intra(up)=+0.0475955 intra(down)=-0.0419723
PDF_WJETS_VR1L3J3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10329, 0.902417, "user", "userNormHistoSys")  # inter=+/-0.0832048 intra(up)=+0.0612055 intra(down)=-0.050985
PDF_WJETS_VRW1L3J1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10452, 0.896988, "user", "userNormHistoSys")  # inter=+/-0.0963911 intra(up)=+0.040414 intra(down)=-0.0363344
PDF_WJETS_VRW1L3J2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10364, 0.898749, "user", "userNormHistoSys")  # inter=+/-0.092282 intra(up)=+0.0471835 intra(down)=-0.0416621
PDF_WJETS_VRW1L3J3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10306, 0.901984, "user", "userNormHistoSys")  # inter=+/-0.0839839 intra(up)=+0.0597415 intra(down)=-0.0505351
PDF_WJETS_VRT1L3J1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1056, 0.898154, "user", "userNormHistoSys")  # inter=+/-0.0895659 intra(up)=+0.0559361 intra(down)=-0.0484817
PDF_WJETS_VRT1L3J2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1, 0.903365, "user", "userNormHistoSys")  # inter=+/-0.0841157 intra(up)=+0.0540709 intra(down)=-0.0475691
PDF_WJETS_VRT1L3J3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11142, 0.90735, "user", "userNormHistoSys")  # inter=+/-0.0726178 intra(up)=+0.0845102 intra(down)=-0.057539
PDF_WJETS_CRT1L5J=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10479, 0.899735, "user", "userNormHistoSys")  # inter=+/-0.0841062 intra(up)=+0.0625107 intra(down)=-0.0545824
PDF_WJETS_CRW1L5J=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10852, 0.895676, "user", "userNormHistoSys")  # inter=+/-0.0909659 intra(up)=+0.0591717 intra(down)=-0.0510748
PDF_WJETS_SR1L5J=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.13684, 0.878409, "user", "userNormHistoSys")  # inter=+/-0.0950173 intra(up)=+0.0984683 intra(down)=-0.0758695
PDF_WJETS_SR1L5Jdis=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10814, 0.893886, "user", "userNormHistoSys")  # inter=+/-0.0916784 intra(up)=+0.0573621 intra(down)=-0.053435
PDF_WJETS_VR1L5J1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11191, 0.893675, "user", "userNormHistoSys")  # inter=+/-0.0949053 intra(up)=+0.0593028 intra(down)=-0.047937
PDF_WJETS_VR1L5J2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11573, 0.88969, "user", "userNormHistoSys")  # inter=+/-0.0960025 intra(up)=+0.0646242 intra(down)=-0.0543302
PDF_WJETS_VR1L5J3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.12194, 0.889053, "user", "userNormHistoSys")  # inter=+/-0.0876349 intra(up)=+0.084791 intra(down)=-0.0680397
PDF_WJETS_VRT1L5J1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10445, 0.901558, "user", "userNormHistoSys")  # inter=+/-0.0753176 intra(up)=+0.0723689 intra(down)=-0.063389
PDF_WJETS_VRT1L5J2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1107, 0.897287, "user", "userNormHistoSys")  # inter=+/-0.0857615 intra(up)=+0.0700021 intra(down)=-0.056523
PDF_WJETS_VRT1L5J3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1239, 0.894215, "user", "userNormHistoSys")  # inter=+/-0.0779373 intra(up)=+0.096318 intra(down)=-0.0715275
PDF_WJETS_VRW1L5J1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11306, 0.892469, "user", "userNormHistoSys")  # inter=+/-0.0969557 intra(up)=+0.0581532 intra(down)=-0.0465019
PDF_WJETS_VRW1L5J2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11644, 0.888691, "user", "userNormHistoSys")  # inter=+/-0.0972511 intra(up)=+0.0640262 intra(down)=-0.0541473
PDF_WJETS_VRW1L5J3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.12191, 0.888506, "user", "userNormHistoSys")  # inter=+/-0.0885031 intra(up)=+0.0838458 intra(down)=-0.0678092
PDF_WJETS_CRT1L3JIncl=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10327, 0.901661, "user", "userNormHistoSys")  # inter=+/-0.0835885 intra(up)=+0.0606417 intra(down)=-0.0518023
PDF_WJETS_CRW1L3JIncl=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10591, 0.896613, "user", "userNormHistoSys")  # inter=+/-0.0940035 intra(up)=+0.048791 intra(down)=-0.0430363
PDF_WJETS_SR1L3JIncl=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10467, 0.903652, "user", "userNormHistoSys")  # inter=+/-0.0764578 intra(up)=+0.0714866 intra(down)=-0.0586277
PDF_WJETS_VR1L3JIncl1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10474, 0.898381, "user", "userNormHistoSys")  # inter=+/-0.0923754 intra(up)=+0.04937 intra(down)=-0.0423466
PDF_WJETS_VR1L3JIncl2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11007, 0.895196, "user", "userNormHistoSys")  # inter=+/-0.0920703 intra(up)=+0.060324 intra(down)=-0.0500689
PDF_WJETS_VR1L3JIncl3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1071, 0.897817, "user", "userNormHistoSys")  # inter=+/-0.0897494 intra(up)=+0.0584387 intra(down)=-0.0488515
PDF_WJETS_VRW1L3JIncl1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10512, 0.897768, "user", "userNormHistoSys")  # inter=+/-0.0935963 intra(up)=+0.0478625 intra(down)=-0.0411242
PDF_WJETS_VRW1L3JIncl2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11016, 0.894977, "user", "userNormHistoSys")  # inter=+/-0.0928211 intra(up)=+0.059323 intra(down)=-0.0491333
PDF_WJETS_VRW1L3JIncl3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1073, 0.897428, "user", "userNormHistoSys")  # inter=+/-0.0904969 intra(up)=+0.0576531 intra(down)=-0.0482839
PDF_WJETS_VRT1L3JIncl1=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.10336, 0.902865, "user", "userNormHistoSys")  # inter=+/-0.07864 intra(up)=+0.0670703 intra(down)=-0.057017
PDF_WJETS_VRT1L3JIncl2=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.11042, 0.896385, "user", "userNormHistoSys")  # inter=+/-0.0832894 intra(up)=+0.0724918 intra(down)=-0.0616355
PDF_WJETS_VRT1L3JIncl3=Systematic(prefixTheo+"PDFWjets", configMgr.weights, 1.1058, 0.901373, "user", "userNormHistoSys")  # inter=+/-0.0813196 intra(up)=+0.0676802 intra(down)=-0.0558075
PDF_ZJETS_CRT1L1Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10465, 0.908455, "user", "userHistoSys")  # inter=+/-0.0133352 intra(up)=+0.103801 intra(down)=-0.090569
PDF_ZJETS_CRW1L1Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08522, 0.928157, "user", "userHistoSys")  # inter=+/-0.010408 intra(up)=+0.0845774 intra(down)=-0.0710847
PDF_ZJETS_SR1L1Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09475, 0.908291, "user", "userHistoSys")  # inter=+/-0.0857185 intra(up)=+0.0403785 intra(down)=-0.0326009
PDF_ZJETS_SR1L1Badis=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.17673, 0.86252, "user", "userHistoSys")  # inter=+/-0.00416712 intra(up)=+0.176678 intra(down)=-0.137416
PDF_ZJETS_VR1L1Ba1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.11788, 0.880796, "user", "userHistoSys")  # inter=+/-0.0743878 intra(up)=+0.0914453 intra(down)=-0.093145
PDF_ZJETS_VR1L1Ba2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.16566, 0.834222, "user", "userHistoSys")  # inter=+/-0.119696 intra(up)=+0.114529 intra(down)=-0.114696
PDF_ZJETS_CRT1L1Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.11312, 0.896112, "user", "userHistoSys")  # inter=+/-0.0613901 intra(up)=+0.0950158 intra(down)=-0.0838095
PDF_ZJETS_CRW1L1Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08032, 0.932884, "user", "userHistoSys")  # inter=+/-0.0341603 intra(up)=+0.0726982 intra(down)=-0.0577717
PDF_ZJETS_SR1L1Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08869, 0.914525, "user", "userHistoSys")  # inter=+/-0.00636254 intra(up)=+0.0884615 intra(down)=-0.0852382
PDF_ZJETS_SR1L1Bcdis=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09864, 0.906177, "user", "userHistoSys")  # inter=+/-0.00688643 intra(up)=+0.0983957 intra(down)=-0.0935702
PDF_ZJETS_VR1L1Bc1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10624, 0.908605, "user", "userHistoSys")  # inter=+/-0.0102285 intra(up)=+0.105751 intra(down)=-0.0908211
PDF_ZJETS_VR1L1Bc2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.16933, 0.865152, "user", "userHistoSys")  # inter=+/-0.0212282 intra(up)=+0.167989 intra(down)=-0.133167
PDF_ZJETS_CRT1L2Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.1157, 0.904231, "user", "userHistoSys")  # inter=+/-0.0493433 intra(up)=+0.104652 intra(down)=-0.082079
PDF_ZJETS_CRW1L2Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08431, 0.937899, "user", "userHistoSys")  # inter=+/-0.0329737 intra(up)=+0.0775918 intra(down)=-0.0526239
PDF_ZJETS_SR1L2Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.14541, 0.8719, "user", "userHistoSys")  # inter=+/-0.0871778 intra(up)=+0.116376 intra(down)=-0.0938599
PDF_ZJETS_SR1L2Badis=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.18761, 0.845501, "user", "userHistoSys")  # inter=+/-0.0895442 intra(up)=+0.164862 intra(down)=-0.125904
PDF_ZJETS_VR1L2Ba=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.05049, 0.945734, "user", "userHistoSys")  # inter=+/-0.0245151 intra(up)=+0.0441374 intra(down)=-0.0484129
PDF_ZJETS_CRT1L2Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.12858, 0.889824, "user", "userHistoSys")  # inter=+/-0.0546621 intra(up)=+0.116387 intra(down)=-0.0956602
PDF_ZJETS_CRW1L2Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10465, 0.913945, "user", "userHistoSys")  # inter=+/-0.0453909 intra(up)=+0.0942961 intra(down)=-0.0731105
PDF_ZJETS_SR1L2Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.19927, 0.823231, "user", "userHistoSys")  # inter=+/-0.0341121 intra(up)=+0.196324 intra(down)=-0.173446
PDF_ZJETS_SR1L2Bcdis=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.19927, 0.823231, "user", "userHistoSys")  # inter=+/-0.0341121 intra(up)=+0.196324 intra(down)=-0.173446
PDF_ZJETS_VR1L2Bc=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.19077, 0.822377, "user", "userHistoSys")  # inter=+/-0.137955 intra(up)=+0.131765 intra(down)=-0.111884
PDF_ZJETS_CRT1L3J=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.07932, 0.92943, "user", "userHistoSys")  # inter=+/-0.00180963 intra(up)=+0.0793026 intra(down)=-0.070547
PDF_ZJETS_CRW1L3J=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09601, 0.920247, "user", "userHistoSys")  # inter=+/-0.0353684 intra(up)=+0.0892619 intra(down)=-0.071482
PDF_ZJETS_SR1L3J=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.13529, 0.892476, "user", "userHistoSys")  # inter=+/-0.0481002 intra(up)=+0.126446 intra(down)=-0.096165
PDF_ZJETS_SR1L3Jdis=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.14307, 0.886536, "user", "userHistoSys")  # inter=+/-0.0423912 intra(up)=+0.136643 intra(down)=-0.105248
PDF_ZJETS_VR1L3J1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08716, 0.92293, "user", "userHistoSys")  # inter=+/-0.0463455 intra(up)=+0.0738133 intra(down)=-0.0615778
PDF_ZJETS_VR1L3J2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.12172, 0.914976, "user", "userHistoSys")  # inter=+/-0.0407354 intra(up)=+0.114702 intra(down)=-0.0746307
PDF_ZJETS_VR1L3J3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.13799, 0.90527, "user", "userHistoSys")  # inter=+/-0.0121227 intra(up)=+0.137459 intra(down)=-0.0939513
PDF_ZJETS_VRW1L3J1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08263, 0.926977, "user", "userHistoSys")  # inter=+/-0.0453769 intra(up)=+0.0690594 intra(down)=-0.0572123
PDF_ZJETS_VRW1L3J2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.12043, 0.917621, "user", "userHistoSys")  # inter=+/-0.0393447 intra(up)=+0.113819 intra(down)=-0.0723756
PDF_ZJETS_VRW1L3J3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.13926, 0.904941, "user", "userHistoSys")  # inter=+/-0.00772201 intra(up)=+0.139044 intra(down)=-0.0947453
PDF_ZJETS_VRT1L3J1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.13638, 0.875275, "user", "userHistoSys")  # inter=+/-0.0555917 intra(up)=+0.124533 intra(down)=-0.11165
PDF_ZJETS_VRT1L3J2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.15106, 0.873479, "user", "userHistoSys")  # inter=+/-0.0569516 intra(up)=+0.139909 intra(down)=-0.112979
PDF_ZJETS_VRT1L3J3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.16006, 0.867992, "user", "userHistoSys")  # inter=+/-0.0909153 intra(up)=+0.131738 intra(down)=-0.0957102
PDF_ZJETS_CRT1L5J=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.25228, 0.827934, "user", "userHistoSys")  # inter=+/-0.104001 intra(up)=+0.229846 intra(down)=-0.137079
PDF_ZJETS_CRW1L5J=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.08436, 0.92362, "user", "userHistoSys")  # inter=+/-0.0235734 intra(up)=+0.0810009 intra(down)=-0.0726516
PDF_ZJETS_SR1L5J=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.13465, 0.890753, "user", "userHistoSys")  # inter=+/-0.0396557 intra(up)=+0.128674 intra(down)=-0.101795
PDF_ZJETS_SR1L5Jdis=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.0913, 0.918065, "user", "userHistoSys")  # inter=+/-0.0345149 intra(up)=+0.0845238 intra(down)=-0.0743103
PDF_ZJETS_VR1L5J1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10998, 0.905021, "user", "userHistoSys")  # inter=+/-0.0289983 intra(up)=+0.106093 intra(down)=-0.0904436
PDF_ZJETS_VR1L5J2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09031, 0.921461, "user", "userHistoSys")  # inter=+/-0.0361332 intra(up)=+0.0827653 intra(down)=-0.0697336
PDF_ZJETS_VR1L5J3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10293, 0.907094, "user", "userHistoSys")  # inter=+/-0.0519189 intra(up)=+0.088876 intra(down)=-0.0770449
PDF_ZJETS_VRT1L5J1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.13102, 0.881416, "user", "userHistoSys")  # inter=+/-0.0224893 intra(up)=+0.12908 intra(down)=-0.116432
PDF_ZJETS_VRT1L5J2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.17975, 0.840992, "user", "userHistoSys")  # inter=+/-0.102265 intra(up)=+0.147827 intra(down)=-0.121759
PDF_ZJETS_VRT1L5J3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.25208, 0.80468, "user", "userHistoSys")  # inter=+/-0.0863193 intra(up)=+0.236837 intra(down)=-0.175211
PDF_ZJETS_VRW1L5J1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10622, 0.908873, "user", "userHistoSys")  # inter=+/-0.0312843 intra(up)=+0.101509 intra(down)=-0.0855887
PDF_ZJETS_VRW1L5J2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.07303, 0.936272, "user", "userHistoSys")  # inter=+/-0.0204737 intra(up)=+0.0701028 intra(down)=-0.0603498
PDF_ZJETS_VRW1L5J3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10264, 0.906867, "user", "userHistoSys")  # inter=+/-0.0536465 intra(up)=+0.0875089 intra(down)=-0.0761301
PDF_ZJETS_CRT1L3JIncl=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.19121, 0.857017, "user", "userHistoSys")  # inter=+/-0.0897766 intra(up)=+0.16882 intra(down)=-0.111285
PDF_ZJETS_CRW1L3JIncl=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09299, 0.922189, "user", "userHistoSys")  # inter=+/-0.0180992 intra(up)=+0.0912155 intra(down)=-0.0756765
PDF_ZJETS_SR1L3JIncl=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.20248, 0.848805, "user", "userHistoSys")  # inter=+/-0.0498953 intra(up)=+0.19624 intra(down)=-0.142724
PDF_ZJETS_VR1L3JIncl1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.0836, 0.924053, "user", "userHistoSys")  # inter=+/-0.0101724 intra(up)=+0.0829797 intra(down)=-0.0752632
PDF_ZJETS_VR1L3JIncl2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10316, 0.912636, "user", "userHistoSys")  # inter=+/-0.0139341 intra(up)=+0.10221 intra(down)=-0.0862455
PDF_ZJETS_VR1L3JIncl3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.12275, 0.905929, "user", "userHistoSys")  # inter=+/-0.0427776 intra(up)=+0.115059 intra(down)=-0.0837823
PDF_ZJETS_VRW1L3JIncl1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.07903, 0.927836, "user", "userHistoSys")  # inter=+/-0.00558061 intra(up)=+0.0788354 intra(down)=-0.0719481
PDF_ZJETS_VRW1L3JIncl2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.10109, 0.913634, "user", "userHistoSys")  # inter=+/-0.0168286 intra(up)=+0.0996772 intra(down)=-0.0847108
PDF_ZJETS_VRW1L3JIncl3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.11732, 0.910113, "user", "userHistoSys")  # inter=+/-0.0375786 intra(up)=+0.111136 intra(down)=-0.0816546
PDF_ZJETS_VRT1L3JIncl1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.12729, 0.884544, "user", "userHistoSys")  # inter=+/-0.0418419 intra(up)=+0.120218 intra(down)=-0.107607
PDF_ZJETS_VRT1L3JIncl2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.17766, 0.849011, "user", "userHistoSys")  # inter=+/-0.047 intra(up)=+0.171327 intra(down)=-0.143488
PDF_ZJETS_VRT1L3JIncl3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.21038, 0.835034, "user", "userHistoSys")  # inter=+/-0.0994877 intra(up)=+0.185367 intra(down)=-0.13159
PDF_DIBOSONS_CRT1L1Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07312, 0.938258, "user", "userHistoSys")  # inter=+/-0.0160919 intra(up)=+0.0713297 intra(down)=-0.0596078
PDF_DIBOSONS_CRW1L1Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.09589, 0.921505, "user", "userHistoSys")  # inter=+/-0.0419098 intra(up)=+0.0862521 intra(down)=-0.0663707
PDF_DIBOSONS_SR1L1Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.14211, 0.910382, "user", "userHistoSys")  # inter=+/-0.0428915 intra(up)=+0.135481 intra(down)=-0.0786869
PDF_DIBOSONS_SR1L1Badis=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.14554, 0.871211, "user", "userHistoSys")  # inter=+/-0.0837064 intra(up)=+0.119063 intra(down)=-0.0978763
PDF_DIBOSONS_VR1L1Ba1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10562, 0.902422, "user", "userHistoSys")  # inter=+/-0.0755457 intra(up)=+0.0738116 intra(down)=-0.0617602
PDF_DIBOSONS_VR1L1Ba2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10915, 0.896129, "user", "userHistoSys")  # inter=+/-0.0833287 intra(up)=+0.0705012 intra(down)=-0.0620119
PDF_DIBOSONS_CRT1L1Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.24662, 0.798176, "user", "userHistoSys")  # inter=+/-0.0880587 intra(up)=+0.23036 intra(down)=-0.181601
PDF_DIBOSONS_CRW1L1Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06446, 0.947628, "user", "userHistoSys")  # inter=+/-0.00260536 intra(up)=+0.0644114 intra(down)=-0.0523071
PDF_DIBOSONS_SR1L1Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10623, 0.915961, "user", "userHistoSys")  # inter=+/-0.0474378 intra(up)=+0.0950496 intra(down)=-0.0693696
PDF_DIBOSONS_SR1L1Bcdis=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.15177, 0.882603, "user", "userHistoSys")  # inter=+/-0.0672849 intra(up)=+0.136043 intra(down)=-0.0962024
PDF_DIBOSONS_VR1L1Bc1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07579, 0.935617, "user", "userHistoSys")  # inter=+/-0.0173884 intra(up)=+0.0737724 intra(down)=-0.0619901
PDF_DIBOSONS_VR1L1Bc2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.08633, 0.923742, "user", "userHistoSys")  # inter=+/-0.0502882 intra(up)=+0.0701701 intra(down)=-0.0573264
PDF_DIBOSONS_CRT1L2Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07227, 0.925207, "user", "userHistoSys")  # inter=+/-0.061066 intra(up)=+0.038656 intra(down)=-0.0431844
PDF_DIBOSONS_CRW1L2Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.0481, 0.958301, "user", "userHistoSys")  # inter=+/-0.00814693 intra(up)=+0.0474052 intra(down)=-0.0408952
PDF_DIBOSONS_SR1L2Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.04934, 0.93585, "user", "userHistoSys")  # inter=+/-0.0244427 intra(up)=+0.042858 intra(down)=-0.0593112
PDF_DIBOSONS_SR1L2Badis=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10603, 0.886032, "user", "userHistoSys")  # inter=+/-0.0919482 intra(up)=+0.0528058 intra(down)=-0.067337
PDF_DIBOSONS_VR1L2Ba=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.03674, 0.957205, "user", "userHistoSys")  # inter=+/-0.0242658 intra(up)=+0.0275905 intra(down)=-0.0352507
PDF_DIBOSONS_CRT1L2Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.05701, 0.940834, "user", "userHistoSys")  # inter=+/-0.0396418 intra(up)=+0.0409708 intra(down)=-0.0439217
PDF_DIBOSONS_CRW1L2Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.0835, 0.947158, "user", "userHistoSys")  # inter=+/-0.000653264 intra(up)=+0.083502 intra(down)=-0.0528379
PDF_DIBOSONS_SR1L2Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06096, 0.927859, "user", "userHistoSys")  # inter=+/-0.0440797 intra(up)=+0.0421146 intra(down)=-0.0571083
PDF_DIBOSONS_SR1L2Bcdis=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06096, 0.927859, "user", "userHistoSys")  # inter=+/-0.0440797 intra(up)=+0.0421146 intra(down)=-0.0571083
PDF_DIBOSONS_VR1L2Bc=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.13929, 0.849097, "user", "userHistoSys")  # inter=+/-0.111615 intra(up)=+0.0833271 intra(down)=-0.101557
PDF_DIBOSONS_CRT1L3J=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.14891, 0.867438, "user", "userHistoSys")  # inter=+/-0.0839772 intra(up)=+0.122967 intra(down)=-0.10257
PDF_DIBOSONS_CRW1L3J=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06669, 0.943241, "user", "userHistoSys")  # inter=+/-0.0137202 intra(up)=+0.0652617 intra(down)=-0.0550753
PDF_DIBOSONS_SR1L3J=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.12891, 0.894081, "user", "userHistoSys")  # inter=+/-0.0500807 intra(up)=+0.118783 intra(down)=-0.0933316
PDF_DIBOSONS_SR1L3Jdis=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.13144, 0.891955, "user", "userHistoSys")  # inter=+/-0.0517423 intra(up)=+0.120831 intra(down)=-0.0948494
PDF_DIBOSONS_VR1L3J1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06174, 0.946211, "user", "userHistoSys")  # inter=+/-0.00325627 intra(up)=+0.0616562 intra(down)=-0.0536898
PDF_DIBOSONS_VR1L3J2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06507, 0.944771, "user", "userHistoSys")  # inter=+/-0.0252549 intra(up)=+0.0599639 intra(down)=-0.0491162
PDF_DIBOSONS_VR1L3J3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07305, 0.936021, "user", "userHistoSys")  # inter=+/-0.00133147 intra(up)=+0.0730413 intra(down)=-0.0639649
PDF_DIBOSONS_VRW1L3J1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.0561, 0.951362, "user", "userHistoSys")  # inter=+/-0.00430353 intra(up)=+0.0559311 intra(down)=-0.0484473
PDF_DIBOSONS_VRW1L3J2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.0663, 0.944074, "user", "userHistoSys")  # inter=+/-0.0253897 intra(up)=+0.0612501 intra(down)=-0.0498301
PDF_DIBOSONS_VRW1L3J3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07824, 0.931914, "user", "userHistoSys")  # inter=+/-0.00825597 intra(up)=+0.0777984 intra(down)=-0.0675836
PDF_DIBOSONS_VRT1L3J1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.12746, 0.881563, "user", "userHistoSys")  # inter=+/-0.0615754 intra(up)=+0.111597 intra(down)=-0.101172
PDF_DIBOSONS_VRT1L3J2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.05609, 0.947623, "user", "userHistoSys")  # inter=+/-0.0236643 intra(up)=+0.0508506 intra(down)=-0.0467265
PDF_DIBOSONS_VRT1L3J3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.09016, 0.912463, "user", "userHistoSys")  # inter=+/-0.0692955 intra(up)=+0.0576828 intra(down)=-0.0534877
PDF_DIBOSONS_CRT1L5J=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07122, 0.937577, "user", "userHistoSys")  # inter=+/-0.0146667 intra(up)=+0.0696979 intra(down)=-0.0606754
PDF_DIBOSONS_CRW1L5J=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06299, 0.950019, "user", "userHistoSys")  # inter=+/-0.00983146 intra(up)=+0.0622215 intra(down)=-0.0490048
PDF_DIBOSONS_SR1L5J=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.14555, 0.883618, "user", "userHistoSys")  # inter=+/-0.0362376 intra(up)=+0.140966 intra(down)=-0.110596
PDF_DIBOSONS_SR1L5Jdis=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10167, 0.911169, "user", "userHistoSys")  # inter=+/-0.00802967 intra(up)=+0.101348 intra(down)=-0.0884677
PDF_DIBOSONS_VR1L5J1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06373, 0.940689, "user", "userHistoSys")  # inter=+/-0.0103992 intra(up)=+0.0628726 intra(down)=-0.0583923
PDF_DIBOSONS_VR1L5J2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10291, 0.914266, "user", "userHistoSys")  # inter=+/-0.0303875 intra(up)=+0.098322 intra(down)=-0.0801684
PDF_DIBOSONS_VR1L5J3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.1124, 0.885714, "user", "userHistoSys")  # inter=+/-0.0569586 intra(up)=+0.0969011 intra(down)=-0.0990807
PDF_DIBOSONS_VRT1L5J1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07474, 0.939601, "user", "userHistoSys")  # inter=+/-0.0101298 intra(up)=+0.074049 intra(down)=-0.0595431
PDF_DIBOSONS_VRT1L5J2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.13955, 0.884504, "user", "userHistoSys")  # inter=+/-0.0426293 intra(up)=+0.132883 intra(down)=-0.107341
PDF_DIBOSONS_VRT1L5J3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.19079, 0.860793, "user", "userHistoSys")  # inter=+/-0.0452637 intra(up)=+0.185343 intra(down)=-0.131642
PDF_DIBOSONS_VRW1L5J1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06518, 0.935826, "user", "userHistoSys")  # inter=+/-0.0104393 intra(up)=+0.0643339 intra(down)=-0.0633188
PDF_DIBOSONS_VRW1L5J2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.09698, 0.918967, "user", "userHistoSys")  # inter=+/-0.0282603 intra(up)=+0.0927712 intra(down)=-0.0759449
PDF_DIBOSONS_VRW1L5J3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.12116, 0.870897, "user", "userHistoSys")  # inter=+/-0.072501 intra(up)=+0.097073 intra(down)=-0.106824
PDF_DIBOSONS_CRT1L3JIncl=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.15524, 0.857052, "user", "userHistoSys")  # inter=+/-0.114162 intra(up)=+0.105203 intra(down)=-0.0860303
PDF_DIBOSONS_CRW1L3JIncl=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06199, 0.950784, "user", "userHistoSys")  # inter=+/-0.0014308 intra(up)=+0.0619735 intra(down)=-0.0491956
PDF_DIBOSONS_SR1L3JIncl=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06566, 0.947278, "user", "userHistoSys")  # inter=+/-0.00347551 intra(up)=+0.0655667 intra(down)=-0.0526076
PDF_DIBOSONS_VR1L3JIncl1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06465, 0.942647, "user", "userHistoSys")  # inter=+/-0.0291137 intra(up)=+0.0577269 intra(down)=-0.0494137
PDF_DIBOSONS_VR1L3JIncl2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06985, 0.945661, "user", "userHistoSys")  # inter=+/-0.00569036 intra(up)=+0.0696153 intra(down)=-0.0540398
PDF_DIBOSONS_VR1L3JIncl3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06659, 0.943264, "user", "userHistoSys")  # inter=+/-0.0166911 intra(up)=+0.0644615 intra(down)=-0.0542255
PDF_DIBOSONS_VRW1L3JIncl1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06397, 0.942327, "user", "userHistoSys")  # inter=+/-0.0365615 intra(up)=+0.0524917 intra(down)=-0.0446034
PDF_DIBOSONS_VRW1L3JIncl2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.06656, 0.947664, "user", "userHistoSys")  # inter=+/-0.00556186 intra(up)=+0.0663306 intra(down)=-0.05204
PDF_DIBOSONS_VRW1L3JIncl3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07348, 0.936993, "user", "userHistoSys")  # inter=+/-0.0241313 intra(up)=+0.0694042 intra(down)=-0.0582031
PDF_DIBOSONS_VRT1L3JIncl1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.12357, 0.889132, "user", "userHistoSys")  # inter=+/-0.0394945 intra(up)=+0.11709 intra(down)=-0.103595
PDF_DIBOSONS_VRT1L3JIncl2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.10457, 0.922842, "user", "userHistoSys")  # inter=+/-0.00672647 intra(up)=+0.104349 intra(down)=-0.0768638
PDF_DIBOSONS_VRT1L3JIncl3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.05419, 0.951175, "user", "userHistoSys")  # inter=+/-0.024749 intra(up)=+0.0482095 intra(down)=-0.0420871
PDF_TTBARV_CRT1L1Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.21053, 0.804308, "user", "userHistoSys")  # inter=+/-0.187192 intra(up)=+0.0963535 intra(down)=-0.0570487
PDF_TTBARV_CRW1L1Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.16271, 0.838039, "user", "userHistoSys")  # inter=+/-0.160644 intra(up)=+0.0258371 intra(down)=-0.020613
PDF_TTBARV_SR1L1Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.25339, 0.757834, "user", "userHistoSys")  # inter=+/-0.231529 intra(up)=+0.102962 intra(down)=-0.0709837
PDF_TTBARV_SR1L1Badis=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.26281, 0.742654, "user", "userHistoSys")  # inter=+/-0.244865 intra(up)=+0.0954579 intra(down)=-0.079172
PDF_TTBARV_VR1L1Ba1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.19895, 0.804794, "user", "userHistoSys")  # inter=+/-0.189943 intra(up)=+0.0591758 intra(down)=-0.0450215
PDF_TTBARV_VR1L1Ba2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.198, 0.801271, "user", "userHistoSys")  # inter=+/-0.19709 intra(up)=+0.0189155 intra(down)=-0.0254705
PDF_TTBARV_CRT1L1Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18669, 0.821402, "user", "userHistoSys")  # inter=+/-0.172 intra(up)=+0.0725993 intra(down)=-0.0480963
PDF_TTBARV_CRW1L1Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.20721, 0.801688, "user", "userHistoSys")  # inter=+/-0.190562 intra(up)=+0.0813767 intra(down)=-0.0548962
PDF_TTBARV_SR1L1Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.25263, 0.752265, "user", "userHistoSys")  # inter=+/-0.242055 intra(up)=+0.072343 intra(down)=-0.0527453
PDF_TTBARV_SR1L1Bcdis=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.27959, 0.727994, "user", "userHistoSys")  # inter=+/-0.258446 intra(up)=+0.106668 intra(down)=-0.0848107
PDF_TTBARV_VR1L1Bc1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18803, 0.816997, "user", "userHistoSys")  # inter=+/-0.177213 intra(up)=+0.0628531 intra(down)=-0.045667
PDF_TTBARV_VR1L1Bc2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18501, 0.816164, "user", "userHistoSys")  # inter=+/-0.180805 intra(up)=+0.0392422 intra(down)=-0.0332474
PDF_TTBARV_CRT1L2Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.13862, 0.862126, "user", "userHistoSys")  # inter=+/-0.137153 intra(up)=+0.0200936 intra(down)=-0.0140854
PDF_TTBARV_CRW1L2Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.12098, 0.879898, "user", "userHistoSys")  # inter=+/-0.118925 intra(up)=+0.0221994 intra(down)=-0.0167718
PDF_TTBARV_SR1L2Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.13978, 0.859738, "user", "userHistoSys")  # inter=+/-0.139441 intra(up)=+0.0097415 intra(down)=-0.0151535
PDF_TTBARV_SR1L2Badis=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18057, 0.819165, "user", "userHistoSys")  # inter=+/-0.178186 intra(up)=+0.029219 intra(down)=-0.0308392
PDF_TTBARV_VR1L2Ba=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.28403, 0.788538, "user", "userHistoSys")  # inter=+/-0.168081 intra(up)=+0.228958 intra(down)=-0.128316
PDF_TTBARV_CRT1L2Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.17255, 0.83162, "user", "userHistoSys")  # inter=+/-0.166239 intra(up)=+0.0462566 intra(down)=-0.0267693
PDF_TTBARV_CRW1L2Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18367, 0.824408, "user", "userHistoSys")  # inter=+/-0.166773 intra(up)=+0.0769486 intra(down)=-0.0549473
PDF_TTBARV_SR1L2Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.21346, 0.786478, "user", "userHistoSys")  # inter=+/-0.208544 intra(up)=+0.0455421 intra(down)=-0.0458397
PDF_TTBARV_SR1L2Bcdis=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.2126, 0.787686, "user", "userHistoSys")  # inter=+/-0.207466 intra(up)=+0.0464218 intra(down)=-0.0451107
PDF_TTBARV_VR1L2Bc=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.1909, 0.81018, "user", "userHistoSys")  # inter=+/-0.188354 intra(up)=+0.0310437 intra(down)=-0.0235451
PDF_TTBARV_CRT1L3J=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.22354, 0.805522, "user", "userHistoSys")  # inter=+/-0.169917 intra(up)=+0.145252 intra(down)=-0.0946042
PDF_TTBARV_CRW1L3J=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.13513, 0.874284, "user", "userHistoSys")  # inter=+/-0.121071 intra(up)=+0.060013 intra(down)=-0.0338608
PDF_TTBARV_SR1L3J=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.25735, 0.760941, "user", "userHistoSys")  # inter=+/-0.212173 intra(up)=+0.145634 intra(down)=-0.110143
PDF_TTBARV_SR1L3Jdis=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.27366, 0.750577, "user", "userHistoSys")  # inter=+/-0.21615 intra(up)=+0.167834 intra(down)=-0.124462
PDF_TTBARV_VR1L3J1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.15291, 0.849347, "user", "userHistoSys")  # inter=+/-0.148968 intra(up)=+0.0345053 intra(down)=-0.0224704
PDF_TTBARV_VR1L3J2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.14572, 0.861048, "user", "userHistoSys")  # inter=+/-0.132328 intra(up)=+0.0610106 intra(down)=-0.0423896
PDF_TTBARV_VR1L3J3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.16204, 0.844782, "user", "userHistoSys")  # inter=+/-0.142285 intra(up)=+0.0775434 intra(down)=-0.0620292
PDF_TTBARV_VRW1L3J1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18222, 0.815674, "user", "userHistoSys")  # inter=+/-0.176342 intra(up)=+0.045917 intra(down)=-0.053662
PDF_TTBARV_VRW1L3J2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18866, 0.832358, "user", "userHistoSys")  # inter=+/-0.146792 intra(up)=+0.118512 intra(down)=-0.0809699
PDF_TTBARV_VRW1L3J3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.11681, 0.881835, "user", "userHistoSys")  # inter=+/-0.110906 intra(up)=+0.0366513 intra(down)=-0.0407761
PDF_TTBARV_VRT1L3J1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.14384, 0.858986, "user", "userHistoSys")  # inter=+/-0.138842 intra(up)=+0.0375922 intra(down)=-0.0246573
PDF_TTBARV_VRT1L3J2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.13195, 0.870865, "user", "userHistoSys")  # inter=+/-0.125949 intra(up)=+0.0393541 intra(down)=-0.0285079
PDF_TTBARV_VRT1L3J3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18836, 0.822871, "user", "userHistoSys")  # inter=+/-0.157505 intra(up)=+0.103302 intra(down)=-0.0810368
PDF_TTBARV_CRT1L5J=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.19059, 0.812314, "user", "userHistoSys")  # inter=+/-0.183578 intra(up)=+0.0512269 intra(down)=-0.0390514
PDF_TTBARV_CRW1L5J=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.16465, 0.835984, "user", "userHistoSys")  # inter=+/-0.163525 intra(up)=+0.0192462 intra(down)=-0.0126819
PDF_TTBARV_SR1L5J=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.23672, 0.764627, "user", "userHistoSys")  # inter=+/-0.229367 intra(up)=+0.0585206 intra(down)=-0.0528298
PDF_TTBARV_SR1L5Jdis=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.23142, 0.770045, "user", "userHistoSys")  # inter=+/-0.227706 intra(up)=+0.0412754 intra(down)=-0.032082
PDF_TTBARV_VR1L5J1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.22579, 0.78658, "user", "userHistoSys")  # inter=+/-0.201957 intra(up)=+0.100973 intra(down)=-0.0690013
PDF_TTBARV_VR1L5J2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18747, 0.814521, "user", "userHistoSys")  # inter=+/-0.181758 intra(up)=+0.0459048 intra(down)=-0.0369622
PDF_TTBARV_VR1L5J3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.19104, 0.810721, "user", "userHistoSys")  # inter=+/-0.186374 intra(up)=+0.0419423 intra(down)=-0.0330356
PDF_TTBARV_VRT1L5J1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.23994, 0.776643, "user", "userHistoSys")  # inter=+/-0.207726 intra(up)=+0.120094 intra(down)=-0.0820885
PDF_TTBARV_VRT1L5J2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18165, 0.820125, "user", "userHistoSys")  # inter=+/-0.176354 intra(up)=+0.043536 intra(down)=-0.0354145
PDF_TTBARV_VRT1L5J3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.19291, 0.809168, "user", "userHistoSys")  # inter=+/-0.187904 intra(up)=+0.0436772 intra(down)=-0.0333016
PDF_TTBARV_VRW1L5J1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.17859, 0.821981, "user", "userHistoSys")  # inter=+/-0.17728 intra(up)=+0.0216264 intra(down)=-0.0162085
PDF_TTBARV_VRW1L5J2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.21192, 0.791049, "user", "userHistoSys")  # inter=+/-0.204268 intra(up)=+0.056448 intra(down)=-0.0439898
PDF_TTBARV_VRW1L5J3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18392, 0.816593, "user", "userHistoSys")  # inter=+/-0.178847 intra(up)=+0.0428839 intra(down)=-0.0406415
PDF_TTBARV_CRT1L3JIncl=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.19331, 0.81135, "user", "userHistoSys")  # inter=+/-0.182417 intra(up)=+0.0639617 intra(down)=-0.048093
PDF_TTBARV_CRW1L3JIncl=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.17026, 0.832689, "user", "userHistoSys")  # inter=+/-0.163812 intra(up)=+0.0464115 intra(down)=-0.0340391
PDF_TTBARV_SR1L3JIncl=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.16792, 0.831927, "user", "userHistoSys")  # inter=+/-0.166166 intra(up)=+0.0242306 intra(down)=-0.0252509
PDF_TTBARV_VR1L3JIncl1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.19177, 0.813909, "user", "userHistoSys")  # inter=+/-0.18262 intra(up)=+0.0585378 intra(down)=-0.0357761
PDF_TTBARV_VR1L3JIncl2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18802, 0.813915, "user", "userHistoSys")  # inter=+/-0.181296 intra(up)=+0.0498168 intra(down)=-0.0419442
PDF_TTBARV_VR1L3JIncl3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18466, 0.818426, "user", "userHistoSys")  # inter=+/-0.17684 intra(up)=+0.0531589 intra(down)=-0.0411922
PDF_TTBARV_VRW1L3JIncl1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.17533, 0.824636, "user", "userHistoSys")  # inter=+/-0.17477 intra(up)=+0.0140314 intra(down)=-0.0144295
PDF_TTBARV_VRW1L3JIncl2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18263, 0.817691, "user", "userHistoSys")  # inter=+/-0.177436 intra(up)=+0.0432396 intra(down)=-0.0418689
PDF_TTBARV_VRW1L3JIncl3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18959, 0.815322, "user", "userHistoSys")  # inter=+/-0.177468 intra(up)=+0.0666904 intra(down)=-0.0510979
PDF_TTBARV_VRT1L3JIncl1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.1971, 0.810962, "user", "userHistoSys")  # inter=+/-0.184391 intra(up)=+0.0696328 intra(down)=-0.0416574
PDF_TTBARV_VRT1L3JIncl2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18982, 0.812742, "user", "userHistoSys")  # inter=+/-0.1824 intra(up)=+0.0525695 intra(down)=-0.0423752
PDF_TTBARV_VRT1L3JIncl3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18357, 0.819097, "user", "userHistoSys")  # inter=+/-0.176667 intra(up)=+0.0498679 intra(down)=-0.0389192
PDF_ALPGENDY_CRT1L1Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L1Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L1Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L1Badis=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L1Ba1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L1Ba2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRT1L1Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L1Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L1Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L1Bcdis=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L1Bc1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L1Bc2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRT1L2Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L2Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.09745, 0.899206, "user", "userHistoSys")  # inter=+/-0.0885095 intra(up)=+0.0407763 intra(down)=-0.048224
PDF_ALPGENDY_SR1L2Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L2Badis=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L2Ba=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRT1L2Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L2Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.27661, 0.776612, "user", "userHistoSys")  # inter=+/-0.169005 intra(up)=+0.21897 intra(down)=-0.14608
PDF_ALPGENDY_SR1L2Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L2Bcdis=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L2Bc=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRT1L3J=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L3J=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.65402, 0.389823, "user", "userHistoSys")  # inter=+/-0.374793 intra(up)=+0.535981 intra(down)=-0.481505
PDF_ALPGENDY_SR1L3J=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L3Jdis=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L3J1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L3J2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L3J3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L3J1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L3J2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L3J3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L3J1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L3J2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L3J3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRT1L5J=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L5J=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.06291, 0.923918, "user", "userHistoSys")  # inter=+/-0.0437358 intra(up)=+0.0452167 intra(down)=-0.0622546
PDF_ALPGENDY_SR1L5J=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR1L5Jdis=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L5J1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L5J2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L5J3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L5J1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L5J2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L5J3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L5J1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L5J2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L5J3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRT1L3JIncl=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_CRW1L3JIncl=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.65393, 0.389906, "user", "userHistoSys")  # inter=+/-0.374738 intra(up)=+0.535912 intra(down)=-0.481442
PDF_ALPGENDY_SR1L3JIncl=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L3JIncl1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L3JIncl2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR1L3JIncl3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L3JIncl1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L3JIncl2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRW1L3JIncl3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L3JIncl1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L3JIncl2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VRT1L3JIncl3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_SINGLETOP_CRT1L1Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11201, 0.893855, "user", "userHistoSys")  # inter=+/-0.0891793 intra(up)=+0.0677702 intra(down)=-0.0575665
PDF_SINGLETOP_CRW1L1Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11238, 0.893257, "user", "userHistoSys")  # inter=+/-0.0915932 intra(up)=+0.0651139 intra(down)=-0.0548154
PDF_SINGLETOP_SR1L1Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11785, 0.869313, "user", "userHistoSys")  # inter=+/-0.0630649 intra(up)=+0.0995516 intra(down)=-0.114464
PDF_SINGLETOP_SR1L1Badis=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14711, 0.866839, "user", "userHistoSys")  # inter=+/-0.101437 intra(up)=+0.106542 intra(down)=-0.0862691
PDF_SINGLETOP_VR1L1Ba1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.09831, 0.910354, "user", "userHistoSys")  # inter=+/-0.0720365 intra(up)=+0.066906 intra(down)=-0.0533587
PDF_SINGLETOP_VR1L1Ba2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.09618, 0.901377, "user", "userHistoSys")  # inter=+/-0.0913228 intra(up)=+0.0301893 intra(down)=-0.0372368
PDF_SINGLETOP_CRT1L1Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.13871, 0.868738, "user", "userHistoSys")  # inter=+/-0.114207 intra(up)=+0.0787149 intra(down)=-0.0647023
PDF_SINGLETOP_CRW1L1Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.19549, 0.865763, "user", "userHistoSys")  # inter=+/-0.113294 intra(up)=+0.15931 intra(down)=-0.0720011
PDF_SINGLETOP_SR1L1Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.15857, 0.839477, "user", "userHistoSys")  # inter=+/-0.12926 intra(up)=+0.0918562 intra(down)=-0.0951817
PDF_SINGLETOP_SR1L1Bcdis=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.19978, 0.796765, "user", "userHistoSys")  # inter=+/-0.177635 intra(up)=+0.091413 intra(down)=-0.0987444
PDF_SINGLETOP_VR1L1Bc1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11746, 0.88969, "user", "userHistoSys")  # inter=+/-0.0883754 intra(up)=+0.0773703 intra(down)=-0.0660158
PDF_SINGLETOP_VR1L1Bc2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14421, 0.868716, "user", "userHistoSys")  # inter=+/-0.10981 intra(up)=+0.0934734 intra(down)=-0.0719542
PDF_SINGLETOP_CRT1L2Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.07907, 0.923764, "user", "userHistoSys")  # inter=+/-0.0692807 intra(up)=+0.0381101 intra(down)=-0.0318131
PDF_SINGLETOP_CRW1L2Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11173, 0.901488, "user", "userHistoSys")  # inter=+/-0.0772522 intra(up)=+0.0807262 intra(down)=-0.0611285
PDF_SINGLETOP_SR1L2Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.107, 0.894127, "user", "userHistoSys")  # inter=+/-0.103072 intra(up)=+0.0287304 intra(down)=-0.0241902
PDF_SINGLETOP_SR1L2Badis=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11742, 0.882242, "user", "userHistoSys")  # inter=+/-0.108456 intra(up)=+0.0449946 intra(down)=-0.0458718
PDF_SINGLETOP_VR1L2Ba=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.04939, 0.950318, "user", "userHistoSys")  # inter=+/-0.0410602 intra(up)=+0.0274447 intra(down)=-0.0279714
PDF_SINGLETOP_CRT1L2Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.08221, 0.924649, "user", "userHistoSys")  # inter=+/-0.0581282 intra(up)=+0.0581285 intra(down)=-0.0479474
PDF_SINGLETOP_CRW1L2Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.22384, 0.832769, "user", "userHistoSys")  # inter=+/-0.0984742 intra(up)=+0.201018 intra(down)=-0.135163
PDF_SINGLETOP_SR1L2Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.09682, 0.908262, "user", "userHistoSys")  # inter=+/-0.0758485 intra(up)=+0.0601833 intra(down)=-0.0516032
PDF_SINGLETOP_SR1L2Bcdis=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.08243, 0.925739, "user", "userHistoSys")  # inter=+/-0.0484299 intra(up)=+0.0667014 intra(down)=-0.0562961
PDF_SINGLETOP_VR1L2Bc=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.06907, 0.927415, "user", "userHistoSys")  # inter=+/-0.00851565 intra(up)=+0.0685421 intra(down)=-0.072084
PDF_SINGLETOP_CRT1L3J=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11099, 0.890043, "user", "userHistoSys")  # inter=+/-0.105655 intra(up)=+0.0339935 intra(down)=-0.0304561
PDF_SINGLETOP_CRW1L3J=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.10845, 0.895339, "user", "userHistoSys")  # inter=+/-0.0970354 intra(up)=+0.0484203 intra(down)=-0.0392181
PDF_SINGLETOP_SR1L3J=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.27736, 0.733249, "user", "userHistoSys")  # inter=+/-0.186902 intra(up)=+0.204925 intra(down)=-0.190325
PDF_SINGLETOP_SR1L3Jdis=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.27736, 0.733249, "user", "userHistoSys")  # inter=+/-0.186902 intra(up)=+0.204925 intra(down)=-0.190325
PDF_SINGLETOP_VR1L3J1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14439, 0.869735, "user", "userHistoSys")  # inter=+/-0.117065 intra(up)=+0.0845163 intra(down)=-0.0571371
PDF_SINGLETOP_VR1L3J2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.18052, 0.835321, "user", "userHistoSys")  # inter=+/-0.134708 intra(up)=+0.120172 intra(down)=-0.0947259
PDF_SINGLETOP_VR1L3J3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.21714, 0.829657, "user", "userHistoSys")  # inter=+/-0.122572 intra(up)=+0.179232 intra(down)=-0.118291
PDF_SINGLETOP_VRW1L3J1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.19973, 0.858556, "user", "userHistoSys")  # inter=+/-0.11599 intra(up)=+0.162598 intra(down)=-0.080948
PDF_SINGLETOP_VRW1L3J2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.20672, 0.816787, "user", "userHistoSys")  # inter=+/-0.136451 intra(up)=+0.155285 intra(down)=-0.122263
PDF_SINGLETOP_VRW1L3J3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.2922, 0.725966, "user", "userHistoSys")  # inter=+/-0.203265 intra(up)=+0.209914 intra(down)=-0.183788
PDF_SINGLETOP_VRT1L3J1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.12635, 0.874493, "user", "userHistoSys")  # inter=+/-0.117737 intra(up)=+0.0458407 intra(down)=-0.0434736
PDF_SINGLETOP_VRT1L3J2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.1666, 0.845122, "user", "userHistoSys")  # inter=+/-0.133625 intra(up)=+0.0995038 intra(down)=-0.0783046
PDF_SINGLETOP_VRT1L3J3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.21461, 0.857841, "user", "userHistoSys")  # inter=+/-0.0921853 intra(up)=+0.193801 intra(down)=-0.108218
PDF_SINGLETOP_CRT1L5J=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.10047, 0.901761, "user", "userHistoSys")  # inter=+/-0.0884052 intra(up)=+0.0477404 intra(down)=-0.0428426
PDF_SINGLETOP_CRW1L5J=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.09012, 0.909348, "user", "userHistoSys")  # inter=+/-0.0855847 intra(up)=+0.0282135 intra(down)=-0.0298832
PDF_SINGLETOP_SR1L5J=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.32065, 0.765712, "user", "userHistoSys")  # inter=+/-0.11178 intra(up)=+0.300533 intra(down)=-0.205904
PDF_SINGLETOP_SR1L5Jdis=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.32458, 0.761231, "user", "userHistoSys")  # inter=+/-0.120632 intra(up)=+0.301326 intra(down)=-0.206055
PDF_SINGLETOP_VR1L5J1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.07879, 0.929451, "user", "userHistoSys")  # inter=+/-0.0544437 intra(up)=+0.0569569 intra(down)=-0.0448663
PDF_SINGLETOP_VR1L5J2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.1121, 0.90606, "user", "userHistoSys")  # inter=+/-0.0680441 intra(up)=+0.0890834 intra(down)=-0.064767
PDF_SINGLETOP_VR1L5J3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.19253, 0.847804, "user", "userHistoSys")  # inter=+/-0.0903823 intra(up)=+0.17 intra(down)=-0.122452
PDF_SINGLETOP_VRT1L5J1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.08097, 0.926848, "user", "userHistoSys")  # inter=+/-0.0584417 intra(up)=+0.0560406 intra(down)=-0.0439977
PDF_SINGLETOP_VRT1L5J2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.08692, 0.919175, "user", "userHistoSys")  # inter=+/-0.0652324 intra(up)=+0.0574441 intra(down)=-0.0477218
PDF_SINGLETOP_VRT1L5J3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14204, 0.890593, "user", "userHistoSys")  # inter=+/-0.0527317 intra(up)=+0.131887 intra(down)=-0.0958605
PDF_SINGLETOP_VRW1L5J1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.07566, 0.933495, "user", "userHistoSys")  # inter=+/-0.0438205 intra(up)=+0.0616809 intra(down)=-0.0500274
PDF_SINGLETOP_VRW1L5J2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.2325, 0.840682, "user", "userHistoSys")  # inter=+/-0.0772802 intra(up)=+0.219281 intra(down)=-0.13932
PDF_SINGLETOP_VRW1L5J3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.35539, 0.706577, "user", "userHistoSys")  # inter=+/-0.190762 intra(up)=+0.299856 intra(down)=-0.222951
PDF_SINGLETOP_CRT1L3JIncl=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.10228, 0.901298, "user", "userHistoSys")  # inter=+/-0.0907387 intra(up)=+0.0471937 intra(down)=-0.0388413
PDF_SINGLETOP_CRW1L3JIncl=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.1192, 0.887749, "user", "userHistoSys")  # inter=+/-0.0960887 intra(up)=+0.0705315 intra(down)=-0.0580273
PDF_SINGLETOP_SR1L3JIncl=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.10531, 0.912054, "user", "userHistoSys")  # inter=+/-0.0468667 intra(up)=+0.0943041 intra(down)=-0.0744178
PDF_SINGLETOP_VR1L3JIncl1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.12143, 0.897914, "user", "userHistoSys")  # inter=+/-0.0895115 intra(up)=+0.0820523 intra(down)=-0.0490843
PDF_SINGLETOP_VR1L3JIncl2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14258, 0.867035, "user", "userHistoSys")  # inter=+/-0.108663 intra(up)=+0.0923097 intra(down)=-0.0766287
PDF_SINGLETOP_VR1L3JIncl3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.13579, 0.881464, "user", "userHistoSys")  # inter=+/-0.0899387 intra(up)=+0.101733 intra(down)=-0.0772129
PDF_SINGLETOP_VRW1L3JIncl1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.22378, 0.876641, "user", "userHistoSys")  # inter=+/-0.0927736 intra(up)=+0.203642 intra(down)=-0.0813047
PDF_SINGLETOP_VRW1L3JIncl2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.16336, 0.855277, "user", "userHistoSys")  # inter=+/-0.107155 intra(up)=+0.123311 intra(down)=-0.0972764
PDF_SINGLETOP_VRW1L3JIncl3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.19379, 0.841823, "user", "userHistoSys")  # inter=+/-0.102523 intra(up)=+0.164446 intra(down)=-0.120454
PDF_SINGLETOP_VRT1L3JIncl1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.09689, 0.904399, "user", "userHistoSys")  # inter=+/-0.088347 intra(up)=+0.0397858 intra(down)=-0.0365291
PDF_SINGLETOP_VRT1L3JIncl2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.13562, 0.870601, "user", "userHistoSys")  # inter=+/-0.109712 intra(up)=+0.0797219 intra(down)=-0.0686111
PDF_SINGLETOP_VRT1L3JIncl3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.1133, 0.897475, "user", "userHistoSys")  # inter=+/-0.0836173 intra(up)=+0.0764522 intra(down)=-0.0593257
PDF_TTBAR_CRT2MU=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09263, 0.914748, "user", "userNormHistoSys")  # inter=+/-0.0295951 intra(up)=+0.08777 intra(down)=-0.0799499
PDF_TTBAR_VR2MU1=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.07748, 0.929016, "user", "userNormHistoSys")  # inter=+/-0.0288832 intra(up)=+0.0718988 intra(down)=-0.0648419
PDF_TTBAR_VR2MU2=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.0756, 0.928385, "user", "userNormHistoSys")  # inter=+/-0.0344649 intra(up)=+0.0672863 intra(down)=-0.062776
PDF_TTBAR_VR2MU3=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.09238, 0.917726, "user", "userNormHistoSys")  # inter=+/-0.02524 intra(up)=+0.0888631 intra(down)=-0.078307
PDF_TTBAR_SR2MU=Systematic(prefixTheo+"PDFttbar", configMgr.weights, 1.08293, 0.924779, "user", "userNormHistoSys")  # inter=+/-0.0290362 intra(up)=+0.0776771 intra(down)=-0.0693904
PDF_ZJETS_CRT2MU=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09611, 0.92207, "user", "userHistoSys")  # inter=+/-0.00584184 intra(up)=+0.0959296 intra(down)=-0.0777103
PDF_ZJETS_VR2MU1=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.07546, 0.932811, "user", "userHistoSys")  # inter=+/-0.00629542 intra(up)=+0.0751981 intra(down)=-0.0668929
PDF_ZJETS_VR2MU2=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.1263, 0.873356, "user", "userHistoSys")  # inter=+/-0.119609 intra(up)=+0.0405569 intra(down)=-0.0416225
PDF_ZJETS_VR2MU3=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.09855, 0.920727, "user", "userHistoSys")  # inter=+/-0.000655635 intra(up)=+0.0985445 intra(down)=-0.0792701
PDF_ZJETS_SR2MU=Systematic(prefixTheo+"PDFZjets", configMgr.weights, 1.07036, 0.93149, "user", "userHistoSys")  # inter=+/-0.0427904 intra(up)=+0.0558505 intra(down)=-0.0535033
PDF_TTBARV_CRT2MU=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.14394, 0.858301, "user", "userHistoSys")  # inter=+/-0.139577 intra(up)=+0.0351622 intra(down)=-0.0244254
PDF_TTBARV_VR2MU1=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.1347, 0.86539, "user", "userHistoSys")  # inter=+/-0.134183 intra(up)=+0.0117332 intra(down)=-0.0107044
PDF_TTBARV_VR2MU2=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.18187, 0.815895, "user", "userHistoSys")  # inter=+/-0.179927 intra(up)=+0.0264864 intra(down)=-0.0389965
PDF_TTBARV_VR2MU3=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.16847, 0.833525, "user", "userHistoSys")  # inter=+/-0.161799 intra(up)=+0.0469391 intra(down)=-0.0391814
PDF_TTBARV_SR2MU=Systematic(prefixTheo+"PDFttbarV", configMgr.weights, 1.06362, 0.91266, "user", "userHistoSys")  # inter=+/-0.00122164 intra(up)=+0.0636117 intra(down)=-0.0873311
PDF_ALPGENDY_CRT2MU=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR2MU1=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.06386, 0.920277, "user", "userHistoSys")  # inter=+/-0.0134108 intra(up)=+0.0624325 intra(down)=-0.0785867
PDF_ALPGENDY_VR2MU2=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_VR2MU3=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_ALPGENDY_SR2MU=Systematic(prefixTheo+"PDFDY", configMgr.weights, 1.0 , 1.0, "user", "userHistoSys")
PDF_SINGLETOP_CRT2MU=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14298, 0.882736, "user", "userHistoSys")  # inter=+/-0.0359693 intra(up)=+0.138377 intra(down)=-0.111611
PDF_SINGLETOP_VR2MU1=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.11092, 0.896251, "user", "userHistoSys")  # inter=+/-0.0573174 intra(up)=+0.0949601 intra(down)=-0.0864789
PDF_SINGLETOP_VR2MU2=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.1211, 0.880652, "user", "userHistoSys")  # inter=+/-0.0884957 intra(up)=+0.0826619 intra(down)=-0.0800781
PDF_SINGLETOP_VR2MU3=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.16391, 0.856784, "user", "userHistoSys")  # inter=+/-0.047966 intra(up)=+0.156736 intra(down)=-0.134945
PDF_SINGLETOP_SR2MU=Systematic(prefixTheo+"PDFsingletop", configMgr.weights, 1.14321, 0.870309, "user", "userHistoSys")  # inter=+/-0.0596327 intra(up)=+0.130203 intra(down)=-0.115168
PDF_DIBOSONS_CRT2MU=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.04753, 0.95389, "user", "userHistoSys")  # inter=+/-0.00586238 intra(up)=+0.0471718 intra(down)=-0.0457362
PDF_DIBOSONS_VR2MU1=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.02681, 0.975328, "user", "userHistoSys")  # inter=+/-0.00351473 intra(up)=+0.0265822 intra(down)=-0.0244208
PDF_DIBOSONS_VR2MU2=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.07063, 0.944268, "user", "userHistoSys")  # inter=+/-0.037494 intra(up)=+0.0598608 intra(down)=-0.0412342
PDF_DIBOSONS_VR2MU3=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.03903, 0.960283, "user", "userHistoSys")  # inter=+/-0.0162839 intra(up)=+0.0354724 intra(down)=-0.0362251
PDF_DIBOSONS_SR2MU=Systematic(prefixTheo+"PDFdibosons", configMgr.weights, 1.04517, 0.962821, "user", "userHistoSys")  # inter=+/-0.0266177 intra(up)=+0.0364948 intra(down)=-0.0259576
