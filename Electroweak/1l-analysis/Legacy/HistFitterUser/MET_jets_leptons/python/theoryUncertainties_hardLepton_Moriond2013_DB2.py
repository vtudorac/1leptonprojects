import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
'''
# Envelope uncertainties of 18/12/2013 
dbTheoSR3J = Systematic("h1L_dbTheo",configMgr.weights,1.47 ,0.88 ,"user","userOverallSys")
dbTheoWR3J = Systematic("h1L_dbTheo",configMgr.weights,1.56 ,0.82 ,"user","userOverallSys")
dbTheoTR3J = Systematic("h1L_dbTheo",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
dbTheoVR3JhighMET = Systematic("h1L_dbTheo",configMgr.weights,1.1 ,0.84 ,"user","userOverallSys")
dbTheoVR3JhighMT = Systematic("h1L_dbTheo",configMgr.weights,1.61 ,0.88 ,"user","userOverallSys")
dbTheoSR5J = Systematic("h1L_dbTheo",configMgr.weights,1.58 ,0.62 ,"user","userOverallSys")
dbTheoWR5J = Systematic("h1L_dbTheo",configMgr.weights,1.12 ,0.88 ,"user","userOverallSys")
dbTheoTR5J = Systematic("h1L_dbTheo",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
dbTheoVR5JhighMET = Systematic("h1L_dbTheo",configMgr.weights,1.16 ,0.72 ,"user","userOverallSys")
dbTheoVR5JhighMT = Systematic("h1L_dbTheo",configMgr.weights,1.26 ,0.79 ,"user","userOverallSys")
dbTheoSR6J = Systematic("h1L_dbTheo",configMgr.weights,2.07 ,0.57 ,"user","userOverallSys")
dbTheoWR6J = Systematic("h1L_dbTheo",configMgr.weights,1.38 ,0.74 ,"user","userOverallSys")
dbTheoTR6J = Systematic("h1L_dbTheo",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
dbTheoVR6JhighMET = Systematic("h1L_dbTheo",configMgr.weights,1.27 ,0.79 ,"user","userOverallSys")
dbTheoVR6JhighMT = Systematic("h1L_dbTheo",configMgr.weights,1.39 ,0.63 ,"user","userOverallSys")

#Envelope uncertainties 18/03/2014

dbTheoSR3J = Systematic("dbTheo",configMgr.weights,1.3 ,0.81 ,"user","userOverallSys")
dbTheoWR3J = Systematic("dbTheo",configMgr.weights,1.39 ,0.8 ,"user","userOverallSys")
dbTheoTR3J = Systematic("dbTheo",configMgr.weights,1.14 ,0.84 ,"user","userOverallSys")
dbTheoVR3JhighMET = Systematic("dbTheo",configMgr.weights,1.1 ,0.77 ,"user","userOverallSys")
dbTheoVR3JhighMT = Systematic("dbTheo",configMgr.weights,1.27 ,0.84 ,"user","userOverallSys")
dbTheoSR5J = Systematic("dbTheo",configMgr.weights,1.57 ,0.46 ,"user","userOverallSys")
dbTheoWR5J = Systematic("dbTheo",configMgr.weights,1.25 ,0.75 ,"user","userOverallSys")
dbTheoTR5J = Systematic("dbTheo",configMgr.weights,1.21 ,0.8 ,"user","userOverallSys")
dbTheoVR5JhighMET = Systematic("dbTheo",configMgr.weights,1.3 ,0.7 ,"user","userOverallSys")
dbTheoVR5JhighMT = Systematic("dbTheo",configMgr.weights,1.36 ,0.64 ,"user","userOverallSys")
dbTheoSR6J = Systematic("dbTheo",configMgr.weights,1.8 ,0.36 ,"user","userOverallSys")
dbTheoWR6J = Systematic("dbTheo",configMgr.weights,1.41 ,0.59 ,"user","userOverallSys")
dbTheoTR6J = Systematic("dbTheo",configMgr.weights,1.44 ,0.56 ,"user","userOverallSys")
dbTheoVR6JhighMET = Systematic("dbTheo",configMgr.weights,1.48 ,0.52 ,"user","userOverallSys")
dbTheoVR6JhighMT = Systematic("dbTheo",configMgr.weights,1.36 ,0.62 ,"user","userOverallSys")


#Envelope uncertainties 18/03/2014 norm to W+jets
dbTheoSR3J = Systematic("dbTheo",configMgr.weights,1.3 ,0.81 ,"user","userOverallSys")
dbTheoWR3J = Systematic("dbTheo",configMgr.weights,1.39 ,0.8 ,"user","userOverallSys")
dbTheoTR3J = Systematic("dbTheo",configMgr.weights,1.14 ,0.84 ,"user","userOverallSys")
dbTheoVR3JhighMET = Systematic("dbTheo",configMgr.weights,1.1 ,0.77 ,"user","userOverallSys")
dbTheoVR3JhighMT = Systematic("dbTheo",configMgr.weights,1.27 ,0.84 ,"user","userOverallSys")
dbTheoSR5J = Systematic("dbTheo",configMgr.weights,1.57 ,0.46 ,"user","userOverallSys")
dbTheoWR5J = Systematic("dbTheo",configMgr.weights,1.25 ,0.75 ,"user","userOverallSys")
dbTheoTR5J = Systematic("dbTheo",configMgr.weights,1.21 ,0.8 ,"user","userOverallSys")
dbTheoVR5JhighMET = Systematic("dbTheo",configMgr.weights,1.3 ,0.7 ,"user","userOverallSys")
dbTheoVR5JhighMT = Systematic("dbTheo",configMgr.weights,1.36 ,0.64 ,"user","userOverallSys")
dbTheoSR6J = Systematic("dbTheo",configMgr.weights,1.8 ,0.36 ,"user","userOverallSys")
dbTheoWR6J = Systematic("dbTheo",configMgr.weights,1.41 ,0.59 ,"user","userOverallSys")
dbTheoTR6J = Systematic("dbTheo",configMgr.weights,1.44 ,0.56 ,"user","userOverallSys")
dbTheoVR6JhighMET = Systematic("dbTheo",configMgr.weights,1.48 ,0.52 ,"user","userOverallSys")
dbTheoVR6JhighMT = Systematic("dbTheo",configMgr.weights,1.36 ,0.62 ,"user","userOverallSys")

#Envelope uncertainties 9/04/2014 TF norm to Normalization Region:1L, lepPt>25GeV, |eta|<2.5

dbTheoSR3J = Systematic("dbTheo",configMgr.weights,1.29 ,0.9 ,"user","userOverallSys")
dbTheoWR3J = Systematic("dbTheo",configMgr.weights,1.38 ,0.85 ,"user","userOverallSys")
dbTheoTR3J = Systematic("dbTheo",configMgr.weights,1.14 ,0.84 ,"user","userOverallSys")
dbTheoVR3JhighMET = Systematic("dbTheo",configMgr.weights,1.1 ,0.78 ,"user","userOverallSys")
dbTheoVR3JhighMT = Systematic("dbTheo",configMgr.weights,1.27 ,0.93 ,"user","userOverallSys")
dbTheoSR5J = Systematic("dbTheo",configMgr.weights,1.55 ,0.54 ,"user","userOverallSys")
dbTheoWR5J = Systematic("dbTheo",configMgr.weights,1.13 ,0.82 ,"user","userOverallSys")
dbTheoTR5J = Systematic("dbTheo",configMgr.weights,1.2 ,0.92 ,"user","userOverallSys")
dbTheoVR5JhighMET = Systematic("dbTheo",configMgr.weights,1.19 ,0.81 ,"user","userOverallSys")
dbTheoVR5JhighMT = Systematic("dbTheo",configMgr.weights,1.26 ,0.74 ,"user","userOverallSys")
dbTheoSR6J = Systematic("dbTheo",configMgr.weights,1.79 ,0.42 ,"user","userOverallSys")
dbTheoWR6J = Systematic("dbTheo",configMgr.weights,1.31 ,0.68 ,"user","userOverallSys")
dbTheoTR6J = Systematic("dbTheo",configMgr.weights,1.34 ,0.66 ,"user","userOverallSys")
dbTheoVR6JhighMET = Systematic("dbTheo",configMgr.weights,1.4 ,0.6 ,"user","userOverallSys")
dbTheoVR6JhighMT = Systematic("dbTheo",configMgr.weights,1.31 ,0.62 ,"user","userOverallSys")
'''
#Envelope uncert 12.05.2014

dbTheoSR3J = Systematic("dbTheo",configMgr.weights,1.29 ,0.79 ,"user","userOverallSys")
dbTheoWR3J = Systematic("dbTheo",configMgr.weights,1.38 ,0.78 ,"user","userOverallSys")
dbTheoTR3J = Systematic("dbTheo",configMgr.weights,1.16 ,0.84 ,"user","userOverallSys")
dbTheoVR3JhighMET = Systematic("dbTheo",configMgr.weights,1.12 ,0.78 ,"user","userOverallSys")
dbTheoVR3JhighMT = Systematic("dbTheo",configMgr.weights,1.27 ,0.8 ,"user","userOverallSys")
dbTheoSR5J = Systematic("dbTheo",configMgr.weights,1.55 ,0.45 ,"user","userOverallSys")
dbTheoWR5J = Systematic("dbTheo",configMgr.weights,1.26 ,0.74 ,"user","userOverallSys")
dbTheoTR5J = Systematic("dbTheo",configMgr.weights,1.23 ,0.77 ,"user","userOverallSys")
dbTheoVR5JhighMET = Systematic("dbTheo",configMgr.weights,1.32 ,0.68 ,"user","userOverallSys")
dbTheoVR5JhighMT = Systematic("dbTheo",configMgr.weights,1.38 ,0.62 ,"user","userOverallSys")
dbTheoSR6J = Systematic("dbTheo",configMgr.weights,1.45 ,0.55 ,"user","userOverallSys")
dbTheoWR6J = Systematic("dbTheo",configMgr.weights,1.42 ,0.58 ,"user","userOverallSys")
dbTheoTR6J = Systematic("dbTheo",configMgr.weights,1.45 ,0.55 ,"user","userOverallSys")
dbTheoVR6JhighMET = Systematic("dbTheo",configMgr.weights,1.51 ,0.49 ,"user","userOverallSys")
dbTheoVR6JhighMT = Systematic("dbTheo",configMgr.weights,1.38 ,0.62 ,"user","userOverallSys")
dbTheoSR7J = Systematic("dbTheo",configMgr.weights,1.51 ,0.49 ,"user","userOverallSys")
dbTheoWR7J = Systematic("dbTheo",configMgr.weights,1.57 ,0.43 ,"user","userOverallSys")
dbTheoTR7J = Systematic("dbTheo",configMgr.weights,1.62 ,0.49 ,"user","userOverallSys")
dbTheoVR7JhighMET = Systematic("dbTheo",configMgr.weights,1.51 ,0.49 ,"user","userOverallSys")
dbTheoVR7JhighMT = Systematic("dbTheo",configMgr.weights,1.81 ,0.62 ,"user","userOverallSys")


def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JEl"), dbTheoWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JMu"), dbTheoWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JEl"), dbTheoTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JMu"), dbTheoTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JEl"), dbTheoWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JMu"), dbTheoWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JEl"), dbTheoTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JMu"), dbTheoTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JEl"), dbTheoWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JMu"), dbTheoWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JEl"), dbTheoTR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JMu"), dbTheoTR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR7JEl"), dbTheoWR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR7JMu"), dbTheoWR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR7JEl"), dbTheoTR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR7JMu"), dbTheoTR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JEM"), dbTheoWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JEM"), dbTheoTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JEM"), dbTheoWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JEM"), dbTheoTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JEM"), dbTheoWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JEM"), dbTheoTR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR7JEM"), dbTheoWR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR7JEM"), dbTheoTR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JEl"), dbTheoSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JMu"), dbTheoSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JEl"), dbTheoSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JMu"), dbTheoSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JEl"), dbTheoSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JMu"), dbTheoSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryEl"), dbTheoSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryMu"), dbTheoSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryEl"), dbTheoSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryMu"), dbTheoSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryEl"), dbTheoSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryMu"), dbTheoSR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR7JEl"), dbTheoSR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR7JMu"), dbTheoSR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JEM"), dbTheoSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JEM"), dbTheoSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JEM"), dbTheoSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryEM"), dbTheoSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryEM"), dbTheoSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryEM"), dbTheoSR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR7JEM"), dbTheoSR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETEl"), dbTheoVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETMu"), dbTheoVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTEl"), dbTheoVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTMu"), dbTheoVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETEl"), dbTheoVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETMu"), dbTheoVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTEl"), dbTheoVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTMu"), dbTheoVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETEl"), dbTheoVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETMu"), dbTheoVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTEl"), dbTheoVR6JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTMu"), dbTheoVR6JhighMT))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMETEl"), dbTheoVR7JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMETMu"), dbTheoVR7JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMTEl"), dbTheoVR7JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMTMu"), dbTheoVR7JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMETEM"), dbTheoVR7JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMTEM"), dbTheoVR7JhighMT))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETEM"), dbTheoVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTEM"), dbTheoVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETEM"), dbTheoVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTEM"), dbTheoVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETEM"), dbTheoVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTEM"), dbTheoVR6JhighMT))

    return generatorSyst
