import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

 
DYTheoSR3J = Systematic("h1L_ZTheoNpart_3J",configMgr.weights,0.25 ,1.75 ,"user","userOverallSys")
DYTheoWR3J = Systematic("h1L_ZTheoNpart_3J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
DYTheoTR3J = Systematic("h1L_ZTheoNpart_3J",configMgr.weights,0.52 ,1.48 ,"user","userOverallSys")
DYTheoVR3JhighMET = Systematic("h1L_ZTheoNpart_3J",configMgr.weights,0.21 ,1.79 ,"user","userOverallSys")
DYTheoVR3JhighMT = Systematic("h1L_ZTheoNpart_3J",configMgr.weights,0.66 ,1.34 ,"user","userOverallSys")
DYTheoSR5J = Systematic("h1L_ZTheoNpart_5J",configMgr.weights,1.77 ,0.23 ,"user","userOverallSys")
DYTheoWR5J = Systematic("h1L_ZTheoNpart_5J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
DYTheoTR5J = Systematic("h1L_ZTheoNpart_5J",configMgr.weights,0.34 ,1.66 ,"user","userOverallSys")
DYTheoVR5JhighMET = Systematic("h1L_ZTheoNpart_5J",configMgr.weights,1.77 ,0.23 ,"user","userOverallSys")
DYTheoVR5JhighMT = Systematic("h1L_ZTheoNpart_5J",configMgr.weights,0.37 ,1.63 ,"user","userOverallSys")
DYTheoSR6J = Systematic("h1L_ZTheoNpart_6J",configMgr.weights,0.6 ,1.4 ,"user","userOverallSys")
DYTheoWR6J = Systematic("h1L_ZTheoNpart_6J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
DYTheoTR6J = Systematic("h1L_ZTheoNpart_6J",configMgr.weights,0.4 ,1.6 ,"user","userOverallSys")
DYTheoVR6JhighMET = Systematic("h1L_ZTheoNpart_6J",configMgr.weights,0.56 ,1.44 ,"user","userOverallSys")
DYTheoVR6JhighMT = Systematic("h1L_ZTheoNpart_6J",configMgr.weights,0.56 ,1.44 ,"user","userOverallSys")

def TheorUnc(generatorSyst):
    # Finite number of partons
    generatorSyst.append((("SherpaDY","h1L_WR3JEl"), DYTheoWR3J))
    generatorSyst.append((("SherpaDY","h1L_WR3JMu"), DYTheoWR3J))
    generatorSyst.append((("SherpaDY","h1L_TR3JEl"), DYTheoTR3J))
    generatorSyst.append((("SherpaDY","h1L_TR3JMu"), DYTheoTR3J))
    generatorSyst.append((("SherpaDY","h1L_WR5JEl"), DYTheoWR5J))
    generatorSyst.append((("SherpaDY","h1L_WR5JMu"), DYTheoWR5J))
    generatorSyst.append((("SherpaDY","h1L_TR5JEl"), DYTheoTR5J))
    generatorSyst.append((("SherpaDY","h1L_TR5JMu"), DYTheoTR5J))
    generatorSyst.append((("SherpaDY","h1L_WR6JEl"), DYTheoWR6J))
    generatorSyst.append((("SherpaDY","h1L_WR6JMu"), DYTheoWR6J))
    generatorSyst.append((("SherpaDY","h1L_TR6JEl"), DYTheoTR6J))
    generatorSyst.append((("SherpaDY","h1L_TR6JMu"), DYTheoTR6J))

    generatorSyst.append((("SherpaDY","h1L_WR3JEM"), DYTheoWR3J))
    generatorSyst.append((("SherpaDY","h1L_TR3JEM"), DYTheoTR3J))
    generatorSyst.append((("SherpaDY","h1L_WR5JEM"), DYTheoWR5J))
    generatorSyst.append((("SherpaDY","h1L_TR5JEM"), DYTheoTR5J))
    generatorSyst.append((("SherpaDY","h1L_WR6JEM"), DYTheoWR6J))
    generatorSyst.append((("SherpaDY","h1L_TR6JEM"), DYTheoTR6J))

    generatorSyst.append((("SherpaDY","h1L_SR5JEl"), DYTheoSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR5JMu"), DYTheoSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JEl"), DYTheoSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR3JMu"), DYTheoSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JEl"), DYTheoSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR6JMu"), DYTheoSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR5JdiscoveryEl"), DYTheoSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR5JdiscoveryMu"), DYTheoSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JdiscoveryEl"), DYTheoSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR3JdiscoveryMu"), DYTheoSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JdiscoveryEl"), DYTheoSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR6JdiscoveryMu"), DYTheoSR6J))

    generatorSyst.append((("SherpaDY","h1L_SR5JEM"), DYTheoSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JEM"), DYTheoSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JEM"), DYTheoSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR5JdiscoveryEM"), DYTheoSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JdiscoveryEM"), DYTheoSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JdiscoveryEM"), DYTheoSR6J))

    generatorSyst.append((("SherpaDY","h1L_VR3JhighMETEl"), DYTheoVR3JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMETMu"), DYTheoVR3JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMTEl"),  DYTheoVR3JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMTMu"),  DYTheoVR3JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMETEl"), DYTheoVR5JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMETMu"), DYTheoVR5JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMTEl"),  DYTheoVR5JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMTMu"),  DYTheoVR5JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMETEl"), DYTheoVR6JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMETMu"), DYTheoVR6JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMTEl"),  DYTheoVR6JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMTMu"),  DYTheoVR6JhighMT))

    generatorSyst.append((("SherpaDY","h1L_VR3JhighMETEM"), DYTheoVR3JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMTEM"),  DYTheoVR3JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMETEM"), DYTheoVR5JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMTEM"),  DYTheoVR5JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMETEM"), DYTheoVR6JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMTEM"),  DYTheoVR6JhighMT))

    return generatorSyst
