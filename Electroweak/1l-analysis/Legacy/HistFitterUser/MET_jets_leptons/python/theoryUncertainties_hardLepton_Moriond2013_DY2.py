import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

''' 
DYTheoSR3J = Systematic("h1L_ZTheoNpart",configMgr.weights,0.25 ,1.75 ,"user","userOverallSys")
DYTheoWR3J = Systematic("h1L_ZTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
DYTheoTR3J = Systematic("h1L_ZTheoNpart",configMgr.weights,0.52 ,1.48 ,"user","userOverallSys")
DYTheoVR3JhighMET = Systematic("h1L_ZTheoNpart",configMgr.weights,0.21 ,1.79 ,"user","userOverallSys")
DYTheoVR3JhighMT = Systematic("h1L_ZTheoNpart",configMgr.weights,0.66 ,1.34 ,"user","userOverallSys")
DYTheoSR5J = Systematic("h1L_ZTheoNpart",configMgr.weights,1.77 ,0.23 ,"user","userOverallSys")
DYTheoWR5J = Systematic("h1L_ZTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
DYTheoTR5J = Systematic("h1L_ZTheoNpart",configMgr.weights,0.34 ,1.66 ,"user","userOverallSys")
DYTheoVR5JhighMET = Systematic("h1L_ZTheoNpart",configMgr.weights,1.77 ,0.23 ,"user","userOverallSys")
DYTheoVR5JhighMT = Systematic("h1L_ZTheoNpart",configMgr.weights,0.37 ,1.63 ,"user","userOverallSys")
DYTheoSR6J = Systematic("h1L_ZTheoNpart",configMgr.weights,0.6 ,1.4 ,"user","userOverallSys")
DYTheoWR6J = Systematic("h1L_ZTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
DYTheoTR6J = Systematic("h1L_ZTheoNpart",configMgr.weights,0.4 ,1.6 ,"user","userOverallSys")
DYTheoVR6JhighMET = Systematic("h1L_ZTheoNpart",configMgr.weights,0.56 ,1.44 ,"user","userOverallSys")
DYTheoVR6JhighMT = Systematic("h1L_ZTheoNpart",configMgr.weights,0.56 ,1.44 ,"user","userOverallSys")
'''
#Envelope uncertainties 9/04/2014 TF norm to Normalization Region:1L, lepPt>25GeV, |eta|<2.5

# AK: no DY systematics samples, so we just copy from Z+jets
ZTheoNpartSR3J = Systematic("ZTheoNpart",configMgr.weights,0.33 ,1.67 ,"user","userOverallSys")
ZTheoNpartWR3J = Systematic("ZTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
ZTheoNpartTR3J = Systematic("ZTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
ZTheoNpartVR3JhighMET = Systematic("ZTheoNpart",configMgr.weights,0.25 ,1.75 ,"user","userOverallSys")
ZTheoNpartVR3JhighMT = Systematic("ZTheoNpart",configMgr.weights,0.76 ,1.24 ,"user","userOverallSys")
ZTheoNpartSR5J = Systematic("ZTheoNpart",configMgr.weights,2.0 ,0.0042 ,"user","userOverallSys")
ZTheoNpartWR5J = Systematic("ZTheoNpart",configMgr.weights,1.91 ,0.086 ,"user","userOverallSys")
ZTheoNpartTR5J = Systematic("ZTheoNpart",configMgr.weights,0.67 ,1.33 ,"user","userOverallSys")
ZTheoNpartVR5JhighMET = Systematic("ZTheoNpart",configMgr.weights,2. ,0.0025 ,"user","userOverallSys")
ZTheoNpartVR5JhighMT = Systematic("ZTheoNpart",configMgr.weights,0.75 ,1.25 ,"user","userOverallSys")
ZTheoNpartSR6J = Systematic("ZTheoNpart",configMgr.weights,1.31 ,0.69 ,"user","userOverallSys")
ZTheoNpartWR6J = Systematic("ZTheoNpart",configMgr.weights,1.6 ,0.4 ,"user","userOverallSys")
ZTheoNpartTR6J = Systematic("ZTheoNpart",configMgr.weights,0.68 ,1.32 ,"user","userOverallSys")
ZTheoNpartVR6JhighMET = Systematic("ZTheoNpart",configMgr.weights,1.43 ,0.57 ,"user","userOverallSys")
ZTheoNpartVR6JhighMT = Systematic("ZTheoNpart",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")

def TheorUnc(generatorSyst):
    # Finite number of partons
    generatorSyst.append((("SherpaDY","h1L_WR3JEl"), ZTheoNpartWR3J))
    generatorSyst.append((("SherpaDY","h1L_WR3JMu"), ZTheoNpartWR3J))
    generatorSyst.append((("SherpaDY","h1L_TR3JEl"), ZTheoNpartTR3J))
    generatorSyst.append((("SherpaDY","h1L_TR3JMu"), ZTheoNpartTR3J))
    generatorSyst.append((("SherpaDY","h1L_WR5JEl"), ZTheoNpartWR5J))
    generatorSyst.append((("SherpaDY","h1L_WR5JMu"), ZTheoNpartWR5J))
    generatorSyst.append((("SherpaDY","h1L_TR5JEl"), ZTheoNpartTR5J))
    generatorSyst.append((("SherpaDY","h1L_TR5JMu"), ZTheoNpartTR5J))
    generatorSyst.append((("SherpaDY","h1L_WR6JEl"), ZTheoNpartWR6J))
    generatorSyst.append((("SherpaDY","h1L_WR6JMu"), ZTheoNpartWR6J))
    generatorSyst.append((("SherpaDY","h1L_TR6JEl"), ZTheoNpartTR6J))
    generatorSyst.append((("SherpaDY","h1L_TR6JMu"), ZTheoNpartTR6J))

    generatorSyst.append((("SherpaDY","h1L_WR7JEl"), ZTheoNpartWR6J))
    generatorSyst.append((("SherpaDY","h1L_WR7JMu"), ZTheoNpartWR6J))
    generatorSyst.append((("SherpaDY","h1L_TR7JEl"), ZTheoNpartTR6J))
    generatorSyst.append((("SherpaDY","h1L_TR7JMu"), ZTheoNpartTR6J))
    generatorSyst.append((("SherpaDY","h1L_WR7JEM"), ZTheoNpartWR6J))
    generatorSyst.append((("SherpaDY","h1L_TR7JEM"), ZTheoNpartTR6J))

    generatorSyst.append((("SherpaDY","h1L_WR3JEM"), ZTheoNpartWR3J))
    generatorSyst.append((("SherpaDY","h1L_TR3JEM"), ZTheoNpartTR3J))
    generatorSyst.append((("SherpaDY","h1L_WR5JEM"), ZTheoNpartWR5J))
    generatorSyst.append((("SherpaDY","h1L_TR5JEM"), ZTheoNpartTR5J))
    generatorSyst.append((("SherpaDY","h1L_WR6JEM"), ZTheoNpartWR6J))
    generatorSyst.append((("SherpaDY","h1L_TR6JEM"), ZTheoNpartTR6J))

    generatorSyst.append((("SherpaDY","h1L_SR5JEl"), ZTheoNpartSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR5JMu"), ZTheoNpartSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JEl"), ZTheoNpartSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR3JMu"), ZTheoNpartSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JEl"), ZTheoNpartSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR6JMu"), ZTheoNpartSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR5JdiscoveryEl"), ZTheoNpartSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR5JdiscoveryMu"), ZTheoNpartSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JdiscoveryEl"), ZTheoNpartSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR3JdiscoveryMu"), ZTheoNpartSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JdiscoveryEl"), ZTheoNpartSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR6JdiscoveryMu"), ZTheoNpartSR6J))

    generatorSyst.append((("SherpaDY","h1L_SR7JEl"), ZTheoNpartSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR7JMu"), ZTheoNpartSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR7JEM"), ZTheoNpartSR6J))

    generatorSyst.append((("SherpaDY","h1L_SR5JEM"), ZTheoNpartSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JEM"), ZTheoNpartSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JEM"), ZTheoNpartSR6J))
    generatorSyst.append((("SherpaDY","h1L_SR5JdiscoveryEM"), ZTheoNpartSR5J))
    generatorSyst.append((("SherpaDY","h1L_SR3JdiscoveryEM"), ZTheoNpartSR3J))
    generatorSyst.append((("SherpaDY","h1L_SR6JdiscoveryEM"), ZTheoNpartSR6J))

    generatorSyst.append((("SherpaDY","h1L_VR3JhighMETEl"), ZTheoNpartVR3JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMETMu"), ZTheoNpartVR3JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMTEl"),  ZTheoNpartVR3JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMTMu"),  ZTheoNpartVR3JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMETEl"), ZTheoNpartVR5JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMETMu"), ZTheoNpartVR5JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMTEl"),  ZTheoNpartVR5JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMTMu"),  ZTheoNpartVR5JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMETEl"), ZTheoNpartVR6JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMETMu"), ZTheoNpartVR6JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMTEl"),  ZTheoNpartVR6JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMTMu"),  ZTheoNpartVR6JhighMT))

    generatorSyst.append((("SherpaDY","h1L_VR3JhighMETEM"), ZTheoNpartVR3JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR3JhighMTEM"),  ZTheoNpartVR3JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMETEM"), ZTheoNpartVR5JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR5JhighMTEM"),  ZTheoNpartVR5JhighMT))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMETEM"), ZTheoNpartVR6JhighMET))
    generatorSyst.append((("SherpaDY","h1L_VR6JhighMTEM"),  ZTheoNpartVR6JhighMT))

    return generatorSyst
