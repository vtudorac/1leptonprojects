import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11 
# Raw stat in the discovery SRs: SR3: 13+4; SR5: 7+2; SR6: 1+0 (ele+muo)
dbTheoPDFWR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.10609,0.969659,"user","userNormHistoSys") #inter=+/-0.0303406 intraUP=0.101664 intraDN=0
dbTheoPDFTR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.27012,0.925368,"user","userNormHistoSys") #inter=+/-0.0746319 intraUP=0.259603 intraDN=0
dbTheoPDFSR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.16666,0.945395,"user","userNormHistoSys") #inter=+/-0.0546054 intraUP=0.157458 intraDN=0
dbTheoPDFVR3JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.22807,0.923312,"user","userNormHistoSys") #inter=+/-0.0766883 intraUP=0.214793 intraDN=0
dbTheoPDFVR3JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.12363,0.960138,"user","userNormHistoSys") #inter=+/-0.0398625 intraUP=0.117027 intraDN=0
dbTheoPDFSRdisc3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.01979,0.979447,"user","userNormHistoSys") #inter=+/-0.00255999 intraUP=0.0196283 intraDN=-0.0203925

dbTheoPDFWR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.01835,0.990039,"user","userNormHistoSys") #inter=+/-0.00203508 intraUP=0.0182399 intraDN=-0.00975132
dbTheoPDFTR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.17313,0.940315,"user","userNormHistoSys") #inter=+/-0.0596851 intraUP=0.162517 intraDN=0
dbTheoPDFSR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.06293,0.978111,"user","userNormHistoSys") #inter=+/-0.0218888 intraUP=0.0589966 intraDN=0
dbTheoPDFVR5JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.044,0.988137,"user","userNormHistoSys") #inter=+/-0.0118627 intraUP=0.0423722 intraDN=0
dbTheoPDFVR5JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.15016,0.952462,"user","userNormHistoSys") #inter=+/-0.0475383 intraUP=0.142433 intraDN=0
dbTheoPDFSRdisc5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.12635,0.953422,"user","userNormHistoSys") #inter=+/-0.0465779 intraUP=0.117449 intraDN=0

dbTheoPDFWR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.01057,0.94974,"user","userNormHistoSys") #inter=+/-0.0105745 intraUP=0 intraDN=-0.0491346
dbTheoPDFTR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.1771,0.941975,"user","userNormHistoSys") #inter=+/-0.0580255 intraUP=0.167323 intraDN=0
dbTheoPDFSR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.13391,0.95203,"user","userNormHistoSys") #inter=+/-0.0479697 intraUP=0.125019 intraDN=0
dbTheoPDFVR6JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.02427,0.890146,"user","userNormHistoSys") #inter=+/-0.0242679 intraUP=0 intraDN=-0.10714
dbTheoPDFVR6JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.0345,0.89231,"user","userNormHistoSys") #inter=+/-0.025538 intraUP=0.023203 intraDN=-0.104618
dbTheoPDFSRdisc6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.4024,0.847566,"user","userNormHistoSys") #inter=+/-0.152434 intraUP=0.372406 intraDN=0

def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JEl"), dbTheoPDFWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JMu"), dbTheoPDFWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JEl"), dbTheoPDFTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JMu"), dbTheoPDFTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JEl"), dbTheoPDFWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JMu"), dbTheoPDFWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JEl"), dbTheoPDFTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JMu"), dbTheoPDFTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JEl"), dbTheoPDFWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JMu"), dbTheoPDFWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JEl"), dbTheoPDFTR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JMu"), dbTheoPDFTR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JEM"), dbTheoPDFWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JEM"), dbTheoPDFTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JEM"), dbTheoPDFWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JEM"), dbTheoPDFTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JEM"), dbTheoPDFWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JEM"), dbTheoPDFTR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JEl"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JMu"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JEl"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JMu"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JEl"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JMu"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryEl"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryMu"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryEl"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryMu"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryEl"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryMu"), dbTheoPDFSR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JEM"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JEM"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JEM"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryEM"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryEM"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryEM"), dbTheoPDFSR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETEl"), dbTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETMu"), dbTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTEl"), dbTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTMu"), dbTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETEl"), dbTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETMu"), dbTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTEl"), dbTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTMu"), dbTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETEl"), dbTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETMu"), dbTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTEl"), dbTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTMu"), dbTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETEM"), dbTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTEM"), dbTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETEM"), dbTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTEM"), dbTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETEM"), dbTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTEM"), dbTheoPDFVR6JhighMT))

    return generatorSyst
