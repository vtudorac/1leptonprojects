import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11 
# Raw stat in the discovery SRs: SR3: 13+4; SR5: 7+2; SR6: 1+0 (ele+muo)
'''
dbTheoPDFWR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.10609,0.969659,"user","userOverallSys") #inter=+/-0.0303406 intraUP=0.101664 intraDN=0
dbTheoPDFTR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.27012,0.925368,"user","userOverallSys") #inter=+/-0.0746319 intraUP=0.259603 intraDN=0
dbTheoPDFSR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.16666,0.945395,"user","userOverallSys") #inter=+/-0.0546054 intraUP=0.157458 intraDN=0
dbTheoPDFVR3JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.22807,0.923312,"user","userOverallSys") #inter=+/-0.0766883 intraUP=0.214793 intraDN=0
dbTheoPDFVR3JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.12363,0.960138,"user","userOverallSys") #inter=+/-0.0398625 intraUP=0.117027 intraDN=0
dbTheoPDFSRdisc3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.01979,0.979447,"user","userOverallSys") #inter=+/-0.00255999 intraUP=0.0196283 intraDN=-0.0203925

dbTheoPDFWR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.01835,0.990039,"user","userOverallSys") #inter=+/-0.00203508 intraUP=0.0182399 intraDN=-0.00975132
dbTheoPDFTR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.17313,0.940315,"user","userOverallSys") #inter=+/-0.0596851 intraUP=0.162517 intraDN=0
dbTheoPDFSR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.06293,0.978111,"user","userOverallSys") #inter=+/-0.0218888 intraUP=0.0589966 intraDN=0
dbTheoPDFVR5JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.044,0.988137,"user","userOverallSys") #inter=+/-0.0118627 intraUP=0.0423722 intraDN=0
dbTheoPDFVR5JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.15016,0.952462,"user","userOverallSys") #inter=+/-0.0475383 intraUP=0.142433 intraDN=0
dbTheoPDFSRdisc5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.12635,0.953422,"user","userOverallSys") #inter=+/-0.0465779 intraUP=0.117449 intraDN=0

dbTheoPDFWR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.01057,0.94974,"user","userOverallSys") #inter=+/-0.0105745 intraUP=0 intraDN=-0.0491346
dbTheoPDFTR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.1771,0.941975,"user","userOverallSys") #inter=+/-0.0580255 intraUP=0.167323 intraDN=0
dbTheoPDFSR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.13391,0.95203,"user","userOverallSys") #inter=+/-0.0479697 intraUP=0.125019 intraDN=0
dbTheoPDFVR6JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.02427,0.890146,"user","userOverallSys") #inter=+/-0.0242679 intraUP=0 intraDN=-0.10714
dbTheoPDFVR6JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.0345,0.89231,"user","userOverallSys") #inter=+/-0.025538 intraUP=0.023203 intraDN=-0.104618
dbTheoPDFSRdisc6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.4024,0.847566,"user","userOverallSys") #inter=+/-0.152434 intraUP=0.372406 intraDN=0
'''
#3J
dbTheoPDFWR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.10042,0.909844,"user","userOverallSys") #inter=+/-0.0803987 intraUP=0.060165 intraDN=-0.040794
dbTheoPDFTR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.16636,0.856693,"user","userOverallSys") #inter=+/-0.00828861 intraUP=0.16615 intraDN=-0.143067
dbTheoPDFSR3J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.15879,0.909406,"user","userOverallSys") #inter=+/-0.0714457 intraUP=0.141809 intraDN=-0.0557022
dbTheoPDFVR3JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.09337,0.908558,"user","userOverallSys") #inter=+/-0.080615 intraUP=0.0471053 intraDN=-0.0431609
dbTheoPDFVR3JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.11336,0.91743,"user","userOverallSys") #inter=+/-0.0676334 intraUP=0.0909723 intraDN=-0.0473661
dbTheoPDFSR3Jdisc = Systematic("h1L_dbTheoPDF",configMgr.weights,1.24422,0.913591,"user","userOverallSys") #inter=+/-0.0468982 intraUP=0.23967 intraDN=-0.0725745
#5J
dbTheoPDFWR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.10147,0.908456,"user","userOverallSys") #inter=+/-0.079502 intraUP=0.0630594 intraDN=-0.0453835
dbTheoPDFTR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.08667,0.913352,"user","userOverallSys") #inter=+/-0.0363657 intraUP=0.0786707 intraDN=-0.0786471
dbTheoPDFSR5J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.08961,0.924774,"user","userOverallSys") #inter=+/-0.0555724 intraUP=0.0702953 intraDN=-0.0507018
dbTheoPDFVR5JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.10284,0.894908,"user","userOverallSys") #inter=+/-0.0967176 intraUP=0.0349466 intraDN=-0.0411103
dbTheoPDFVR5JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.14066,0.885798,"user","userOverallSys") #inter=+/-0.0999817 intraUP=0.0989459 intraDN=-0.0551887
dbTheoPDFSR5Jdisc = Systematic("h1L_dbTheoPDF",configMgr.weights,1.08227,0.937519,"user","userOverallSys") #inter=+/-0.0349433 intraUP=0.0744852 intraDN=-0.0517963
#6J
dbTheoPDFWR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.13955,0.901223,"user","userOverallSys") #inter=+/-0.0905164 intraUP=0.106218 intraDN=-0.0395432
dbTheoPDFTR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.10104,0.891469,"user","userOverallSys") #inter=+/-0.0958825 intraUP=0.0318708 intraDN=-0.0508479
dbTheoPDFSR6J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.14162,0.881871,"user","userOverallSys") #inter=+/-0.0975128 intraUP=0.102698 intraDN=-0.0666757
dbTheoPDFVR6JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.13814,0.884144,"user","userOverallSys") #inter=+/-0.0945187 intraUP=0.100747 intraDN=-0.066999
dbTheoPDFVR6JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.12131,0.880745,"user","userOverallSys") #inter=+/-0.10784 intraUP=0.055564 intraDN=-0.0509159
dbTheoPDFSR6Jdisc= Systematic("h1L_dbTheoPDF",configMgr.weights,1.07345,0.919776,"user","userOverallSys") #inter=+/-0.0533902 intraUP=0.0504434 intraDN=-0.0598786
#7J
dbTheoPDFWR7J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.14048,0.881966,"user","userOverallSys") #inter=+/-0.110936 intraUP=0.0861893 intraDN=-0.040315
dbTheoPDFTR7J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.14559,0.844963,"user","userOverallSys") #inter=+/-0.144545 intraUP=0.0173845 intraDN=-0.0560623
dbTheoPDFSR7J = Systematic("h1L_dbTheoPDF",configMgr.weights,1.13157,0.890825,"user","userOverallSys") #inter=+/-0.090542 intraUP=0.0954655 intraDN=-0.0610022
dbTheoPDFVR7JhighMET = Systematic("h1L_dbTheoPDF",configMgr.weights,1.11813,0.889873,"user","userOverallSys") #inter=+/-0.0985348 intraUP=0.0651548 intraDN=-0.0491827
dbTheoPDFVR7JhighMT = Systematic("h1L_dbTheoPDF",configMgr.weights,1.15322,0.863179,"user","userOverallSys") #inter=+/-0.129142 intraUP=0.082464 intraDN=-0.0451944
dbTheoPDFSR7Jdisc = Systematic("h1L_dbTheoPDF",configMgr.weights,1.09303,0.917272,"user","userOverallSys") #inter=+/-0.0387286 intraUP=0.0845841 intraDN=-0.0731031

def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JEl"), dbTheoPDFWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JMu"), dbTheoPDFWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JEl"), dbTheoPDFTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JMu"), dbTheoPDFTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JEl"), dbTheoPDFWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JMu"), dbTheoPDFWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JEl"), dbTheoPDFTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JMu"), dbTheoPDFTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JEl"), dbTheoPDFWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JMu"), dbTheoPDFWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JEl"), dbTheoPDFTR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JMu"), dbTheoPDFTR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR7JEl"), dbTheoPDFWR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR7JMu"), dbTheoPDFWR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR7JEl"), dbTheoPDFTR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR7JMu"), dbTheoPDFTR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR3JEM"), dbTheoPDFWR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR3JEM"), dbTheoPDFTR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR5JEM"), dbTheoPDFWR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR5JEM"), dbTheoPDFTR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR6JEM"), dbTheoPDFWR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR6JEM"), dbTheoPDFTR6J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_WR7JEM"), dbTheoPDFWR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_TR7JEM"), dbTheoPDFTR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JEl"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JMu"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JEl"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JMu"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JEl"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JMu"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryEl"), dbTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryMu"), dbTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryEl"), dbTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryMu"), dbTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryEl"), dbTheoPDFSR6Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryMu"), dbTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR7JEl"), dbTheoPDFSR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR7JMu"), dbTheoPDFSR7J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR7JEM"), dbTheoPDFSR7J))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JEM"), dbTheoPDFSR5J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JEM"), dbTheoPDFSR3J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JEM"), dbTheoPDFSR6J))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR5JdiscoveryEM"), dbTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR3JdiscoveryEM"), dbTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_SR6JdiscoveryEM"), dbTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETEl"), dbTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETMu"), dbTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTEl"), dbTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTMu"), dbTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETEl"), dbTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETMu"), dbTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTEl"), dbTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTMu"), dbTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETEl"), dbTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETMu"), dbTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTEl"), dbTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTMu"), dbTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMETEl"), dbTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMETMu"), dbTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMTEl"), dbTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMTMu"), dbTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMETEM"), dbTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR7JhighMTEM"), dbTheoPDFVR7JhighMT))

    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMETEM"), dbTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR3JhighMTEM"), dbTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMETEM"), dbTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR5JhighMTEM"), dbTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMETEM"), dbTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaDibosonsMassiveBC","h1L_VR6JhighMTEM"), dbTheoPDFVR6JhighMT))

    return generatorSyst
