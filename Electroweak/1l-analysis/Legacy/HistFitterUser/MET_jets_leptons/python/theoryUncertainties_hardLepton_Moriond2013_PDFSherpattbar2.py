import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11
# Raw stat in the discovery SRs: SR3: 38+37; SR5: 75+77; SR6: 53+50 (ele+muo)
#3J
topTheoPDFWR3J = Systematic("h1L_topTheoPDF",configMgr.weights,1.08435,0.938548,"user","userNormHistoSys") #inter=+/-0.0224999 intraUP=0.0812941 intraDN=-0.057185
topTheoPDFTR3J = Systematic("h1L_topTheoPDF",configMgr.weights,1.08766,0.937118,"user","userNormHistoSys") #inter=+/-0.0227812 intraUP=0.0846484 intraDN=-0.0586098
topTheoPDFSR3J = Systematic("h1L_topTheoPDF",configMgr.weights,1.10408,0.931015,"user","userNormHistoSys") #inter=+/-0.0219292 intraUP=0.10174 intraDN=-0.0654064
topTheoPDFVR3JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.10937,0.933172,"user","userNormHistoSys") #inter=+/-0.0175923 intraUP=0.107946 intraDN=-0.0644714
topTheoPDFVR3JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.07936,0.935006,"user","userNormHistoSys") #inter=+/-0.0255057 intraUP=0.0751487 intraDN=-0.0597808
topTheoPDFSR3Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.12232,0.927806,"user","userNormHistoSys") #inter=+/-0.0205842 intraUP=0.120574 intraDN=-0.0691977
#5J
topTheoPDFWR5J = Systematic("h1L_topTheoPDF",configMgr.weights,1.08086,0.936003,"user","userNormHistoSys") #inter=+/-0.0243693 intraUP=0.0771027 intraDN=-0.0591754
topTheoPDFTR5J = Systematic("h1L_topTheoPDF",configMgr.weights,1.08591,0.933607,"user","userNormHistoSys") #inter=+/-0.0249925 intraUP=0.0821905 intraDN=-0.0615095
topTheoPDFSR5J = Systematic("h1L_topTheoPDF",configMgr.weights,1.08655,0.927884,"user","userNormHistoSys") #inter=+/-0.0293591 intraUP=0.0814224 intraDN=-0.0658695
topTheoPDFVR5JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.11509,0.927822,"user","userNormHistoSys") #inter=+/-0.0197774 intraUP=0.113382 intraDN=-0.0694153
topTheoPDFVR5JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.08098,0.933595,"user","userNormHistoSys") #inter=+/-0.0247008 intraUP=0.0771178 intraDN=-0.0616397
topTheoPDFSR5Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.09483,0.927832,"user","userNormHistoSys") #inter=+/-0.030418 intraUP=0.0898217 intraDN=-0.0654441
#6J
topTheoPDFWR6J = Systematic("h1L_topTheoPDF",configMgr.weights,1.066,0.936362,"user","userNormHistoSys") #inter=+/-0.0290008 intraUP=0.0592893 intraDN=-0.0566458
topTheoPDFTR6J = Systematic("h1L_topTheoPDF",configMgr.weights,1.07657,0.932144,"user","userNormHistoSys") #inter=+/-0.0268493 intraUP=0.0717105 intraDN=-0.0623184
topTheoPDFSR6J = Systematic("h1L_topTheoPDF",configMgr.weights,1.10243,0.930379,"user","userNormHistoSys") #inter=+/-0.0180523 intraUP=0.10083 intraDN=-0.0672402
topTheoPDFVR6JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.0845,0.93079,"user","userNormHistoSys") #inter=+/-0.0257469 intraUP=0.0804793 intraDN=-0.0642422
topTheoPDFVR6JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.07515,0.932064,"user","userNormHistoSys") #inter=+/-0.0267926 intraUP=0.0702111 intraDN=-0.0624296
topTheoPDFSR6Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.09927,0.929862,"user","userNormHistoSys") #inter=+/-0.0245707 intraUP=0.0961854 intraDN=-0.065693
#7J for the SherpaTTbar as nominal
topTheoPDFWR7J = Systematic("h1L_topTheoPDF",configMgr.weights,1.21289,0.839887,"user","userNormHistoSys") #inter=+/-0.13979 intraUP=0.16057 intraDN=-0.0780704
topTheoPDFTR7J = Systematic("h1L_topTheoPDF",configMgr.weights,1.20981,0.838868,"user","userNormHistoSys") #inter=+/-0.140888 intraUP=0.155475 intraDN=-0.0781922
topTheoPDFSR7J = Systematic("h1L_topTheoPDF",configMgr.weights,1.34074,0.84898,"user","userNormHistoSys") #inter=+/-0.141231 intraUP=0.310092 intraDN=-0.0534878
topTheoPDFVR7JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.24645,0.826347,"user","userNormHistoSys") #inter=+/-0.152698 intraUP=0.193445 intraDN=-0.0826961
topTheoPDFVR7JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.22096,0.835398,"user","userNormHistoSys") #inter=+/-0.144801 intraUP=0.166897 intraDN=-0.0782712
topTheoPDFSR7Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.39393,0.783916,"user","userNormHistoSys") #inter=+/-0.190502 intraUP=0.344799 intraDN=-0.101987

'''
#3J
topTheoPDFWR3J = Systematic("h1L_topTheoPDF",configMgr.weights,1.23059,0.800829,"user","userNormHistoSys") #inter=+/-0.184281 intraUP=0.138603 intraDN=-0.075563
topTheoPDFTR3J = Systematic("h1L_topTheoPDF",configMgr.weights,1.23327,0.800877,"user","userNormHistoSys") #inter=+/-0.183329 intraUP=0.144244 intraDN=-0.077719
topTheoPDFSR3J = Systematic("h1L_topTheoPDF",configMgr.weights,1.26351,0.784636,"user","userNormHistoSys") #inter=+/-0.197879 intraUP=0.174018 intraDN=-0.0850023
topTheoPDFVR3JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.27499,0.782163,"user","userNormHistoSys") #inter=+/-0.19817 intraUP=0.190655 intraDN=-0.0904517
topTheoPDFVR3JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.21869,0.802239,"user","userNormHistoSys") #inter=+/-0.183653 intraUP=0.118724 intraDN=-0.0733552
topTheoPDFSR3Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.3218,0.756032,"user","userNormHistoSys") #inter=+/-0.221154 intraUP=0.23377 intraDN=-0.103011
#5J
topTheoPDFWR5J = Systematic("h1L_topTheoPDF",configMgr.weights,1.22705,0.798227,"user","userNormHistoSys") #inter=+/-0.18765 intraUP=0.12783 intraDN=-0.0741604
topTheoPDFTR5J = Systematic("h1L_topTheoPDF",configMgr.weights,1.23194,0.796907,"user","userNormHistoSys") #inter=+/-0.18793 intraUP=0.135933 intraDN=-0.0769992
topTheoPDFSR5J = Systematic("h1L_topTheoPDF",configMgr.weights,1.22503,0.798626,"user","userNormHistoSys") #inter=+/-0.185736 intraUP=0.127043 intraDN=-0.077805
topTheoPDFVR5JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.28658,0.771312,"user","userNormHistoSys") #inter=+/-0.209879 intraUP=0.195138 intraDN=-0.0908231
topTheoPDFVR5JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.22817,0.794872,"user","userNormHistoSys") #inter=+/-0.190861 intraUP=0.125034 intraDN=-0.0751645
topTheoPDFSR5Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.23633,0.798842,"user","userNormHistoSys") #inter=+/-0.183567 intraUP=0.148848 intraDN=-0.0822656
#6J
topTheoPDFWR6J = Systematic("h1L_topTheoPDF",configMgr.weights,1.19466,0.814553,"user","userNormHistoSys") #inter=+/-0.173584 intraUP=0.0880923 intraDN=-0.0652618
topTheoPDFTR6J = Systematic("h1L_topTheoPDF",configMgr.weights,1.21475,0.801592,"user","userNormHistoSys") #inter=+/-0.18474 intraUP=0.109495 intraDN=-0.0723687
topTheoPDFSR6J = Systematic("h1L_topTheoPDF",configMgr.weights,1.26852,0.773072,"user","userNormHistoSys") #inter=+/-0.209547 intraUP=0.167903 intraDN=-0.0871005
topTheoPDFVR6JhighMET = Systematic("h1L_topTheoPDF",configMgr.weights,1.23094,0.793695,"user","userNormHistoSys") #inter=+/-0.191068 intraUP=0.129723 intraDN=-0.0778111
topTheoPDFVR6JhighMT = Systematic("h1L_topTheoPDF",configMgr.weights,1.2123,0.801791,"user","userNormHistoSys") #inter=+/-0.18487 intraUP=0.104379 intraDN=-0.0714843
topTheoPDFSR6Jdisc = Systematic("h1L_topTheoPDF",configMgr.weights,1.25619,0.784175,"user","userNormHistoSys") #inter=+/-0.19876 intraUP=0.161639 intraDN=-0.0841141
'''


def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaTTbar","h1L_WR3JEl"), topTheoPDFWR3J))
    generatorSyst.append((("SherpaTTbar","h1L_WR3JMu"), topTheoPDFWR3J))
    generatorSyst.append((("SherpaTTbar","h1L_TR3JEl"), topTheoPDFTR3J))
    generatorSyst.append((("SherpaTTbar","h1L_TR3JMu"), topTheoPDFTR3J))
    generatorSyst.append((("SherpaTTbar","h1L_WR5JEl"), topTheoPDFWR5J))
    generatorSyst.append((("SherpaTTbar","h1L_WR5JMu"), topTheoPDFWR5J))
    generatorSyst.append((("SherpaTTbar","h1L_TR5JEl"), topTheoPDFTR5J))
    generatorSyst.append((("SherpaTTbar","h1L_TR5JMu"), topTheoPDFTR5J))
    generatorSyst.append((("SherpaTTbar","h1L_WR6JEl"), topTheoPDFWR6J))
    generatorSyst.append((("SherpaTTbar","h1L_WR6JMu"), topTheoPDFWR6J))
    generatorSyst.append((("SherpaTTbar","h1L_TR6JEl"), topTheoPDFTR6J))
    generatorSyst.append((("SherpaTTbar","h1L_TR6JMu"), topTheoPDFTR6J))

    generatorSyst.append((("SherpaTTbar","h1L_WR7JEl"), topTheoPDFWR7J))
    generatorSyst.append((("SherpaTTbar","h1L_WR7JMu"), topTheoPDFWR7J))
    generatorSyst.append((("SherpaTTbar","h1L_TR7JEl"), topTheoPDFTR7J))
    generatorSyst.append((("SherpaTTbar","h1L_TR7JMu"), topTheoPDFTR7J))
    generatorSyst.append((("SherpaTTbar","h1L_WR7JEM"), topTheoPDFWR7J))
    generatorSyst.append((("SherpaTTbar","h1L_TR7JEM"), topTheoPDFTR7J))

    generatorSyst.append((("SherpaTTbar","h1L_WR3JEM"), topTheoPDFWR3J))
    generatorSyst.append((("SherpaTTbar","h1L_TR3JEM"), topTheoPDFTR3J))
    generatorSyst.append((("SherpaTTbar","h1L_WR5JEM"), topTheoPDFWR5J))
    generatorSyst.append((("SherpaTTbar","h1L_TR5JEM"), topTheoPDFTR5J))
    generatorSyst.append((("SherpaTTbar","h1L_WR6JEM"), topTheoPDFWR6J))
    generatorSyst.append((("SherpaTTbar","h1L_TR6JEM"), topTheoPDFTR6J))

    generatorSyst.append((("SherpaTTbar","h1L_SR5JEl"), topTheoPDFSR5J))
    generatorSyst.append((("SherpaTTbar","h1L_SR5JMu"), topTheoPDFSR5J))
    generatorSyst.append((("SherpaTTbar","h1L_SR3JEl"), topTheoPDFSR3J))
    generatorSyst.append((("SherpaTTbar","h1L_SR3JMu"), topTheoPDFSR3J))
    generatorSyst.append((("SherpaTTbar","h1L_SR6JEl"), topTheoPDFSR6J))
    generatorSyst.append((("SherpaTTbar","h1L_SR6JMu"), topTheoPDFSR6J))
    generatorSyst.append((("SherpaTTbar","h1L_SR5JdiscoveryEl"), topTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR5JdiscoveryMu"), topTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR3JdiscoveryEl"), topTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR3JdiscoveryMu"), topTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR6JdiscoveryEl"), topTheoPDFSR6Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR6JdiscoveryMu"), topTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaTTbar","h1L_SR7JEl"), topTheoPDFSR7J))
    generatorSyst.append((("SherpaTTbar","h1L_SR7JMu"), topTheoPDFSR7J))
    generatorSyst.append((("SherpaTTbar","h1L_SR7JEM"), topTheoPDFSR7J))

    generatorSyst.append((("SherpaTTbar","h1L_SR5JEM"), topTheoPDFSR5J))
    generatorSyst.append((("SherpaTTbar","h1L_SR3JEM"), topTheoPDFSR3J))
    generatorSyst.append((("SherpaTTbar","h1L_SR6JEM"), topTheoPDFSR6J))
    generatorSyst.append((("SherpaTTbar","h1L_SR5JdiscoveryEM"), topTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR3JdiscoveryEM"), topTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaTTbar","h1L_SR6JdiscoveryEM"), topTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaTTbar","h1L_VR3JhighMETEl"), topTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR3JhighMETMu"), topTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR3JhighMTEl"), topTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR3JhighMTMu"), topTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR5JhighMETEl"), topTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR5JhighMETMu"), topTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR5JhighMTEl"), topTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR5JhighMTMu"), topTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR6JhighMETEl"), topTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR6JhighMETMu"), topTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR6JhighMTEl"), topTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR6JhighMTMu"), topTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaTTbar","h1L_VR7JhighMETEl"), topTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR7JhighMETMu"), topTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR7JhighMTEl"), topTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR7JhighMTMu"), topTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR7JhighMETEM"), topTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR7JhighMTEM"), topTheoPDFVR7JhighMT))

    generatorSyst.append((("SherpaTTbar","h1L_VR3JhighMETEM"), topTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR3JhighMTEM"), topTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR5JhighMETEM"), topTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR5JhighMTEM"), topTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaTTbar","h1L_VR6JhighMETEM"), topTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaTTbar","h1L_VR6JhighMTEM"), topTheoPDFVR6JhighMT))

    return generatorSyst
