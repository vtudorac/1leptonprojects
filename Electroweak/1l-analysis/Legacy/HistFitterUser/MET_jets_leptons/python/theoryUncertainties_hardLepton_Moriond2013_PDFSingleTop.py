import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11
# Raw stat in the discovery SRs: SR3: 16+17; SR5: 12+10; SR6: 7+4 (ele+muo)
#3J
SingleTopTheoPDFWR3J = Systematic("h1L_SingleTopTheoPDF_3J",configMgr.weights,1.178,0.898031,"user","userNormHistoSys") #inter=+/-0.0177056 intraUP=0.177121 intraDN=-0.10042
SingleTopTheoPDFTR3J = Systematic("h1L_SingleTopTheoPDF_3J",configMgr.weights,1.15833,0.901288,"user","userNormHistoSys") #inter=+/-0.0183792 intraUP=0.157261 intraDN=-0.0969856
SingleTopTheoPDFSR3J = Systematic("h1L_SingleTopTheoPDF_3J",configMgr.weights,1.12532,0.902402,"user","userNormHistoSys") #inter=+/-0.0390331 intraUP=0.119087 intraDN=-0.0894529
SingleTopTheoPDFVR3JhighMET = Systematic("h1L_SingleTopTheoPDF_3J",configMgr.weights,1.28664,0.877895,"user","userNormHistoSys") #inter=+/-0.0256052 intraUP=0.285491 intraDN=-0.11939
SingleTopTheoPDFVR3JhighMT = Systematic("h1L_SingleTopTheoPDF_3J",configMgr.weights,1.20112,0.899577,"user","userNormHistoSys") #inter=+/-0.0102776 intraUP=0.200857 intraDN=-0.0998955
SingleTopTheoPDFSRdisc3J = Systematic("h1L_SingleTopTheoPDF_3J",configMgr.weights,1.20777,0.859928,"user","userNormHistoSys") #inter=+/-0.0424645 intraUP=0.203384 intraDN=-0.13348
#5J
SingleTopTheoPDFWR5J = Systematic("h1L_SingleTopTheoPDF_5J",configMgr.weights,1.15451,0.892967,"user","userNormHistoSys") #inter=+/-0.0315421 intraUP=0.151261 intraDN=-0.10228
SingleTopTheoPDFTR5J = Systematic("h1L_SingleTopTheoPDF_5J",configMgr.weights,1.15964,0.901155,"user","userNormHistoSys") #inter=+/-0.0157698 intraUP=0.158856 intraDN=-0.0975788
SingleTopTheoPDFSR5J = Systematic("h1L_SingleTopTheoPDF_5J",configMgr.weights,1.24231,0.885878,"user","userNormHistoSys") #inter=+/-0.0360074 intraUP=0.239617 intraDN=-0.108293
SingleTopTheoPDFVR5JhighMET = Systematic("h1L_SingleTopTheoPDF_5J",configMgr.weights,1.44879,0.870744,"user","userNormHistoSys") #inter=+/-0.0198486 intraUP=0.448351 intraDN=-0.127723
SingleTopTheoPDFVR5JhighMT = Systematic("h1L_SingleTopTheoPDF_5J",configMgr.weights,1.3942,0.868109,"user","userNormHistoSys") #inter=+/-0.0570154 intraUP=0.390052 intraDN=-0.118931
SingleTopTheoPDFSRdisc5J = Systematic("h1L_SingleTopTheoPDF_5J",configMgr.weights,1.26372,0.8866,"user","userNormHistoSys") #inter=+/-0.0431495 intraUP=0.260168 intraDN=-0.10487
#6J
SingleTopTheoPDFWR6J = Systematic("h1L_SingleTopTheoPDF_6J",configMgr.weights,1.12627,0.902639,"user","userNormHistoSys") #inter=+/-0.0393478 intraUP=0.119982 intraDN=-0.089056
SingleTopTheoPDFTR6J = Systematic("h1L_SingleTopTheoPDF_6J",configMgr.weights,1.13871,0.903938,"user","userNormHistoSys") #inter=+/-0.0190065 intraUP=0.137401 intraDN=-0.0941629
SingleTopTheoPDFSR6J = Systematic("h1L_SingleTopTheoPDF_6J",configMgr.weights,1.30778,0.867378,"user","userNormHistoSys") #inter=+/-0.0629337 intraUP=0.301274 intraDN=-0.116738
SingleTopTheoPDFVR6JhighMET = Systematic("h1L_SingleTopTheoPDF_6J",configMgr.weights,1.34863,0.855582,"user","userNormHistoSys") #inter=+/-0.0509479 intraUP=0.34489 intraDN=-0.135133
SingleTopTheoPDFVR6JhighMT = Systematic("h1L_SingleTopTheoPDF_6J",configMgr.weights,1.30436,0.865723,"user","userNormHistoSys") #inter=+/-0.0434213 intraUP=0.301246 intraDN=-0.127062
SingleTopTheoPDFSRdisc6J = Systematic("h1L_SingleTopTheoPDF_6J",configMgr.weights,1.31427,0.876157,"user","userNormHistoSys") #inter=+/-0.0637008 intraUP=0.307747 intraDN=-0.106204

def TheorUnc(generatorSyst):
    generatorSyst.append((("SingleTop","h1L_WR3JEl"), SingleTopTheoPDFWR3J))
    generatorSyst.append((("SingleTop","h1L_WR3JMu"), SingleTopTheoPDFWR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JEl"), SingleTopTheoPDFTR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JMu"), SingleTopTheoPDFTR3J))
    generatorSyst.append((("SingleTop","h1L_WR5JEl"), SingleTopTheoPDFWR5J))
    generatorSyst.append((("SingleTop","h1L_WR5JMu"), SingleTopTheoPDFWR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JEl"), SingleTopTheoPDFTR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JMu"), SingleTopTheoPDFTR5J))
    generatorSyst.append((("SingleTop","h1L_WR6JEl"), SingleTopTheoPDFWR6J))
    generatorSyst.append((("SingleTop","h1L_WR6JMu"), SingleTopTheoPDFWR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JEl"), SingleTopTheoPDFTR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JMu"), SingleTopTheoPDFTR6J))

    generatorSyst.append((("SingleTop","h1L_WR3JEM"), SingleTopTheoPDFWR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JEM"), SingleTopTheoPDFTR3J))
    generatorSyst.append((("SingleTop","h1L_WR5JEM"), SingleTopTheoPDFWR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JEM"), SingleTopTheoPDFTR5J))
    generatorSyst.append((("SingleTop","h1L_WR6JEM"), SingleTopTheoPDFWR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JEM"), SingleTopTheoPDFTR6J))

    generatorSyst.append((("SingleTop","h1L_SR5JEl"), SingleTopTheoPDFSR5J))
    generatorSyst.append((("SingleTop","h1L_SR5JMu"), SingleTopTheoPDFSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JEl"), SingleTopTheoPDFSR3J))
    generatorSyst.append((("SingleTop","h1L_SR3JMu"), SingleTopTheoPDFSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JEl"), SingleTopTheoPDFSR6J))
    generatorSyst.append((("SingleTop","h1L_SR6JMu"), SingleTopTheoPDFSR6J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryEl"), SingleTopTheoPDFSRdisc5J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryMu"), SingleTopTheoPDFSRdisc5J))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryEl"), SingleTopTheoPDFSRdisc3J))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryMu"), SingleTopTheoPDFSRdisc3J))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryEl"), SingleTopTheoPDFSRdisc6J))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryMu"), SingleTopTheoPDFSRdisc6J))

    generatorSyst.append((("SingleTop","h1L_SR5JEM"), SingleTopTheoPDFSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JEM"), SingleTopTheoPDFSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JEM"), SingleTopTheoPDFSR6J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryEM"), SingleTopTheoPDFSRdisc5J))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryEM"), SingleTopTheoPDFSRdisc3J))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryEM"), SingleTopTheoPDFSRdisc6J))

    generatorSyst.append((("SingleTop","h1L_VR3JhighMETEl"), SingleTopTheoPDFVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMETMu"), SingleTopTheoPDFVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTEl"), SingleTopTheoPDFVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTMu"), SingleTopTheoPDFVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETEl"), SingleTopTheoPDFVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETMu"), SingleTopTheoPDFVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTEl"), SingleTopTheoPDFVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTMu"), SingleTopTheoPDFVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETEl"), SingleTopTheoPDFVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETMu"), SingleTopTheoPDFVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTEl"), SingleTopTheoPDFVR6JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTMu"), SingleTopTheoPDFVR6JhighMT))

    generatorSyst.append((("SingleTop","h1L_VR3JhighMETEM"), SingleTopTheoPDFVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTEM"), SingleTopTheoPDFVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETEM"), SingleTopTheoPDFVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTEM"), SingleTopTheoPDFVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETEM"), SingleTopTheoPDFVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTEM"), SingleTopTheoPDFVR6JhighMT))

    return generatorSyst
