import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11
# Raw stat in the discovery SRs: SR3: 16+17; SR5: 12+10; SR6: 7+4 (ele+muo)
'''
#3J
SingleTopTheoPDFWR3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.178,0.898031,"user","userOverallSys") #inter=+/-0.0177056 intraUP=0.177121 intraDN=-0.10042
SingleTopTheoPDFTR3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.15833,0.901288,"user","userOverallSys") #inter=+/-0.0183792 intraUP=0.157261 intraDN=-0.0969856
SingleTopTheoPDFSR3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.12532,0.902402,"user","userOverallSys") #inter=+/-0.0390331 intraUP=0.119087 intraDN=-0.0894529
SingleTopTheoPDFVR3JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.28664,0.877895,"user","userOverallSys") #inter=+/-0.0256052 intraUP=0.285491 intraDN=-0.11939
SingleTopTheoPDFVR3JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.20112,0.899577,"user","userOverallSys") #inter=+/-0.0102776 intraUP=0.200857 intraDN=-0.0998955
SingleTopTheoPDFSRdisc3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.20777,0.859928,"user","userOverallSys") #inter=+/-0.0424645 intraUP=0.203384 intraDN=-0.13348
#5J
SingleTopTheoPDFWR5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.15451,0.892967,"user","userOverallSys") #inter=+/-0.0315421 intraUP=0.151261 intraDN=-0.10228
SingleTopTheoPDFTR5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.15964,0.901155,"user","userOverallSys") #inter=+/-0.0157698 intraUP=0.158856 intraDN=-0.0975788
SingleTopTheoPDFSR5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.24231,0.885878,"user","userOverallSys") #inter=+/-0.0360074 intraUP=0.239617 intraDN=-0.108293
SingleTopTheoPDFVR5JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.44879,0.870744,"user","userOverallSys") #inter=+/-0.0198486 intraUP=0.448351 intraDN=-0.127723
SingleTopTheoPDFVR5JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.3942,0.868109,"user","userOverallSys") #inter=+/-0.0570154 intraUP=0.390052 intraDN=-0.118931
SingleTopTheoPDFSRdisc5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.26372,0.8866,"user","userOverallSys") #inter=+/-0.0431495 intraUP=0.260168 intraDN=-0.10487
#6J
SingleTopTheoPDFWR6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.12627,0.902639,"user","userOverallSys") #inter=+/-0.0393478 intraUP=0.119982 intraDN=-0.089056
SingleTopTheoPDFTR6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.13871,0.903938,"user","userOverallSys") #inter=+/-0.0190065 intraUP=0.137401 intraDN=-0.0941629
SingleTopTheoPDFSR6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.30778,0.867378,"user","userOverallSys") #inter=+/-0.0629337 intraUP=0.301274 intraDN=-0.116738
SingleTopTheoPDFVR6JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.34863,0.855582,"user","userOverallSys") #inter=+/-0.0509479 intraUP=0.34489 intraDN=-0.135133
SingleTopTheoPDFVR6JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.30436,0.865723,"user","userOverallSys") #inter=+/-0.0434213 intraUP=0.301246 intraDN=-0.127062
SingleTopTheoPDFSRdisc6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.31427,0.876157,"user","userOverallSys") #inter=+/-0.0637008 intraUP=0.307747 intraDN=-0.106204
'''

#3J
SingleTopTheoPDFWR3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.13422,0.941149,"user","userOverallSys") #inter=+/-0.0177051 intraUP=0.133049 intraDN=-0.0561243
SingleTopTheoPDFTR3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.11454,0.943349,"user","userOverallSys") #inter=+/-0.0183798 intraUP=0.113053 intraDN=-0.0535864
SingleTopTheoPDFSR3J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.08415,0.937919,"user","userOverallSys") #inter=+/-0.039033 intraUP=0.0745493 intraDN=-0.0482749
SingleTopTheoPDFVR3JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.2427,0.919375,"user","userOverallSys") #inter=+/-0.0256058 intraUP=0.241341 intraDN=-0.0764504
SingleTopTheoPDFVR3JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.15814,0.941445,"user","userOverallSys") #inter=+/-0.0102776 intraUP=0.157803 intraDN=-0.0576463
SingleTopTheoPDFSR3Jdisc = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.16228,0.898699,"user","userOverallSys") #inter=+/-0.0424645 intraUP=0.156626 intraDN=-0.0919714
#5J
SingleTopTheoPDFWR5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.10993,0.93404,"user","userOverallSys") #inter=+/-0.0315422 intraUP=0.105304 intraDN=-0.0579295
SingleTopTheoPDFTR5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.11597,0.943646,"user","userOverallSys") #inter=+/-0.0157693 intraUP=0.114894 intraDN=-0.054103
SingleTopTheoPDFSR5J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.19888,0.925926,"user","userOverallSys") #inter=+/-0.0360075 intraUP=0.195592 intraDN=-0.0647339
SingleTopTheoPDFVR5JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.40766,0.912551,"user","userOverallSys") #inter=+/-0.0198488 intraUP=0.407177 intraDN=-0.0851668
SingleTopTheoPDFVR5JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.35277,0.905202,"user","userOverallSys") #inter=+/-0.0570153 intraUP=0.348133 intraDN=-0.0757359
SingleTopTheoPDFSR5Jdisc = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.22268,0.921196,"user","userOverallSys") #inter=+/-0.0431494 intraUP=0.218463 intraDN=-0.0659409
#6J
SingleTopTheoPDFWR6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.08372,0.941442,"user","userOverallSys") #inter=+/-0.0393473 intraUP=0.0738975 intraDN=-0.0433685
SingleTopTheoPDFTR6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.09442,0.947783,"user","userOverallSys") #inter=+/-0.019006 intraUP=0.0924844 intraDN=-0.0486355
SingleTopTheoPDFSR6J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.26629,0.89941,"user","userOverallSys") #inter=+/-0.0629338 intraUP=0.258742 intraDN=-0.0784709
SingleTopTheoPDFVR6JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.30425,0.894213,"user","userOverallSys") #inter=+/-0.0509477 intraUP=0.299957 intraDN=-0.0927103
SingleTopTheoPDFVR6JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.2599,0.905044,"user","userOverallSys") #inter=+/-0.0434215 intraUP=0.256247 intraDN=-0.0844468
SingleTopTheoPDFSR6Jdisc = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.27617,0.895873,"user","userOverallSys") #inter=+/-0.0637008 intraUP=0.268724 intraDN=-0.082369
#7J
SingleTopTheoPDFWR7J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.10168,0.93477,"user","userOverallSys") #inter=+/-0.0338412 intraUP=0.0958794 intraDN=-0.0557652
SingleTopTheoPDFTR7J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.12041,0.947586,"user","userOverallSys") #inter=+/-0.0060583 intraUP=0.120258 intraDN=-0.0520628
SingleTopTheoPDFSR7J = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.21871,0.926428,"user","userOverallSys") #inter=+/-0.0217213 intraUP=0.217631 intraDN=-0.0702919
SingleTopTheoPDFVR7JhighMET = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.17007,0.930494,"user","userOverallSys") #inter=+/-0.00380048 intraUP=0.170028 intraDN=-0.0694017
SingleTopTheoPDFVR7JhighMT = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.10273,0.92505,"user","userOverallSys") #inter=+/-0.0470835 intraUP=0.0913082 intraDN=-0.0583154
SingleTopTheoPDFSR7Jdisc = Systematic("h1L_SingleTopTheoPDF",configMgr.weights,1.27365,0.895432,"user","userOverallSys") #inter=+/-0.0625255 intraUP=0.266413 intraDN=-0.0838156



def TheorUnc(generatorSyst):
    generatorSyst.append((("SingleTop","h1L_WR3JEl"), SingleTopTheoPDFWR3J))
    generatorSyst.append((("SingleTop","h1L_WR3JMu"), SingleTopTheoPDFWR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JEl"), SingleTopTheoPDFTR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JMu"), SingleTopTheoPDFTR3J))
    generatorSyst.append((("SingleTop","h1L_WR5JEl"), SingleTopTheoPDFWR5J))
    generatorSyst.append((("SingleTop","h1L_WR5JMu"), SingleTopTheoPDFWR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JEl"), SingleTopTheoPDFTR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JMu"), SingleTopTheoPDFTR5J))
    generatorSyst.append((("SingleTop","h1L_WR6JEl"), SingleTopTheoPDFWR6J))
    generatorSyst.append((("SingleTop","h1L_WR6JMu"), SingleTopTheoPDFWR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JEl"), SingleTopTheoPDFTR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JMu"), SingleTopTheoPDFTR6J))

    generatorSyst.append((("SingleTop","h1L_WR7JEl"), SingleTopTheoPDFWR7J))
    generatorSyst.append((("SingleTop","h1L_WR7JMu"), SingleTopTheoPDFWR7J))
    generatorSyst.append((("SingleTop","h1L_TR7JEl"), SingleTopTheoPDFTR7J))
    generatorSyst.append((("SingleTop","h1L_TR7JMu"), SingleTopTheoPDFTR7J))
    generatorSyst.append((("SingleTop","h1L_WR7JEM"), SingleTopTheoPDFWR7J))
    generatorSyst.append((("SingleTop","h1L_TR7JEM"), SingleTopTheoPDFTR7J))

    generatorSyst.append((("SingleTop","h1L_WR3JEM"), SingleTopTheoPDFWR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JEM"), SingleTopTheoPDFTR3J))
    generatorSyst.append((("SingleTop","h1L_WR5JEM"), SingleTopTheoPDFWR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JEM"), SingleTopTheoPDFTR5J))
    generatorSyst.append((("SingleTop","h1L_WR6JEM"), SingleTopTheoPDFWR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JEM"), SingleTopTheoPDFTR6J))

    generatorSyst.append((("SingleTop","h1L_SR5JEl"), SingleTopTheoPDFSR5J))
    generatorSyst.append((("SingleTop","h1L_SR5JMu"), SingleTopTheoPDFSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JEl"), SingleTopTheoPDFSR3J))
    generatorSyst.append((("SingleTop","h1L_SR3JMu"), SingleTopTheoPDFSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JEl"), SingleTopTheoPDFSR6J))
    generatorSyst.append((("SingleTop","h1L_SR6JMu"), SingleTopTheoPDFSR6J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryEl"), SingleTopTheoPDFSR5Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryMu"), SingleTopTheoPDFSR5Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryEl"), SingleTopTheoPDFSR3Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryMu"), SingleTopTheoPDFSR3Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryEl"), SingleTopTheoPDFSR6Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryMu"), SingleTopTheoPDFSR6Jdisc))

    generatorSyst.append((("SingleTop","h1L_SR7JEl"), SingleTopTheoPDFSR7J))
    generatorSyst.append((("SingleTop","h1L_SR7JMu"), SingleTopTheoPDFSR7J))
    generatorSyst.append((("SingleTop","h1L_SR7JEM"), SingleTopTheoPDFSR7J))

    generatorSyst.append((("SingleTop","h1L_SR5JEM"), SingleTopTheoPDFSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JEM"), SingleTopTheoPDFSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JEM"), SingleTopTheoPDFSR6J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryEM"), SingleTopTheoPDFSR5Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryEM"), SingleTopTheoPDFSR3Jdisc))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryEM"), SingleTopTheoPDFSR6Jdisc))

    generatorSyst.append((("SingleTop","h1L_VR3JhighMETEl"), SingleTopTheoPDFVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMETMu"), SingleTopTheoPDFVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTEl"), SingleTopTheoPDFVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTMu"), SingleTopTheoPDFVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETEl"), SingleTopTheoPDFVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETMu"), SingleTopTheoPDFVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTEl"), SingleTopTheoPDFVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTMu"), SingleTopTheoPDFVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETEl"), SingleTopTheoPDFVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETMu"), SingleTopTheoPDFVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTEl"), SingleTopTheoPDFVR6JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTMu"), SingleTopTheoPDFVR6JhighMT))

    generatorSyst.append((("SingleTop","h1L_VR7JhighMETEl"), SingleTopTheoPDFVR7JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMETMu"), SingleTopTheoPDFVR7JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMTEl"), SingleTopTheoPDFVR7JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMTMu"), SingleTopTheoPDFVR7JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMETEM"), SingleTopTheoPDFVR7JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMTEM"), SingleTopTheoPDFVR7JhighMT))

    generatorSyst.append((("SingleTop","h1L_VR3JhighMETEM"), SingleTopTheoPDFVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTEM"), SingleTopTheoPDFVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETEM"), SingleTopTheoPDFVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTEM"), SingleTopTheoPDFVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETEM"), SingleTopTheoPDFVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTEM"), SingleTopTheoPDFVR6JhighMT))

    return generatorSyst
