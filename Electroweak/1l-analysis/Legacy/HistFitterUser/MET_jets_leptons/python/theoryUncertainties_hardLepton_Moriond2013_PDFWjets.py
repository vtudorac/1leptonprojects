import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11
# Raw stat in the discovery SRs: SR3: 32+38; SR5: 13+14; SR6: 10+10 (ele+muo) 
WTheoPDFWR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.10979,0.903973,"user","userNormHistoSys") #inter=+/-0.090309 intraUP=0.0624387 intraDN=-0.0326421
WTheoPDFTR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.117,0.901638,"user","userNormHistoSys") #inter=+/-0.0887013 intraUP=0.0762972 intraDN=-0.0425115
WTheoPDFSR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.0876,0.923843,"user","userNormHistoSys") #inter=+/-0.0674005 intraUP=0.0559523 intraDN=-0.0354554
WTheoPDFVR3JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.10712,0.910244,"user","userNormHistoSys") #inter=+/-0.0824464 intraUP=0.0683831 intraDN=-0.0354777
WTheoPDFVR3JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.12065,0.90406,"user","userNormHistoSys") #inter=+/-0.0880096 intraUP=0.0825226 intraDN=-0.0381942
WTheoPDFSRdisc3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.1218,0.924799,"user","userNormHistoSys") #inter=+/-0.0571367 intraUP=0.107571 intraDN=-0.0488929

WTheoPDFWR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.1157,0.901227,"user","userNormHistoSys") #inter=+/-0.0913977 intraUP=0.0709442 intraDN=-0.03745
WTheoPDFTR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.12394,0.898681,"user","userNormHistoSys") #inter=+/-0.0909301 intraUP=0.0842155 intraDN=-0.0446919
WTheoPDFSR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.13755,0.915754,"user","userNormHistoSys") #inter=+/-0.0708179 intraUP=0.117916 intraDN=-0.0456321
WTheoPDFVR5JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12492,0.898907,"user","userNormHistoSys") #inter=+/-0.0926812 intraUP=0.0837629 intraDN=-0.0403731
WTheoPDFVR5JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.16546,0.908421,"user","userNormHistoSys") #inter=+/-0.0772616 intraUP=0.146314 intraDN=-0.0491665
WTheoPDFSRdisc5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.1729,0.927595,"user","userNormHistoSys") #inter=+/-0.048935 intraUP=0.165827 intraDN=-0.0533652

WTheoPDFWR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11747,0.89514,"user","userNormHistoSys") #inter=+/-0.0981238 intraUP=0.0645831 intraDN=-0.0369788
WTheoPDFTR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.1235,0.896149,"user","userNormHistoSys") #inter=+/-0.0925237 intraUP=0.0817955 intraDN=-0.0471647
WTheoPDFSR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.14746,0.893326,"user","userNormHistoSys") #inter=+/-0.0914565 intraUP=0.115675 intraDN=-0.0549099
WTheoPDFVR6JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12559,0.89845,"user","userNormHistoSys") #inter=+/-0.0936021 intraUP=0.0837394 intraDN=-0.0393842
WTheoPDFVR6JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.12482,0.882138,"user","userNormHistoSys") #inter=+/-0.103521 intraUP=0.0697418 intraDN=-0.0563449
WTheoPDFSRdisc6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.12861,0.924375,"user","userNormHistoSys") #inter=+/-0.0586423 intraUP=0.114466 intraDN=-0.0477524

def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR3JEl"), WTheoPDFWR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR3JMu"), WTheoPDFWR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR3JEl"), WTheoPDFTR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR3JMu"), WTheoPDFTR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR5JEl"), WTheoPDFWR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR5JMu"), WTheoPDFWR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR5JEl"), WTheoPDFTR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR5JMu"), WTheoPDFTR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR6JEl"), WTheoPDFWR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR6JMu"), WTheoPDFWR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR6JEl"), WTheoPDFTR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR6JMu"), WTheoPDFTR6J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_WR3JEM"), WTheoPDFWR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR3JEM"), WTheoPDFTR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR5JEM"), WTheoPDFWR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR5JEM"), WTheoPDFTR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR6JEM"), WTheoPDFWR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR6JEM"), WTheoPDFTR6J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JEl"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JMu"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JEl"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JMu"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JEl"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JMu"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JdiscoveryEl"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JdiscoveryMu"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JdiscoveryEl"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JdiscoveryMu"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JdiscoveryEl"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JdiscoveryMu"), WTheoPDFSR6J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JEM"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JEM"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JEM"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JdiscoveryEM"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JdiscoveryEM"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JdiscoveryEM"), WTheoPDFSR6J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMETEl"), WTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMETMu"), WTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMTEl"), WTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMTMu"), WTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMETEl"), WTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMETMu"), WTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMTEl"), WTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMTMu"), WTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMETEl"), WTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMETMu"), WTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMTEl"), WTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMTMu"), WTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMETEM"), WTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMTEM"), WTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMETEM"), WTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMTEM"), WTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMETEM"), WTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMTEM"), WTheoPDFVR6JhighMT))

    return generatorSyst
