import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11
# Raw stat in the discovery SRs: SR3: 32+38; SR5: 13+14; SR6: 10+10 (ele+muo) 
#3J
WTheoPDFWR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.10979,0.903973,"user","userNormHistoSys") #inter=+/-0.090309 intraUP=0.0624387 intraDN=-0.0326421
WTheoPDFTR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.117,0.901638,"user","userNormHistoSys") #inter=+/-0.0887013 intraUP=0.0762972 intraDN=-0.0425115
WTheoPDFSR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.0876,0.923843,"user","userNormHistoSys") #inter=+/-0.0674005 intraUP=0.0559523 intraDN=-0.0354554
WTheoPDFVR3JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.10712,0.910244,"user","userNormHistoSys") #inter=+/-0.0824464 intraUP=0.0683831 intraDN=-0.0354777
WTheoPDFVR3JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.12065,0.90406,"user","userNormHistoSys") #inter=+/-0.0880096 intraUP=0.0825226 intraDN=-0.0381942
WTheoPDFSR3Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.1218,0.924799,"user","userNormHistoSys") #inter=+/-0.0571367 intraUP=0.107571 intraDN=-0.0488929
#5J
WTheoPDFWR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.1157,0.901227,"user","userNormHistoSys") #inter=+/-0.0913977 intraUP=0.0709442 intraDN=-0.03745
WTheoPDFTR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.12394,0.898681,"user","userNormHistoSys") #inter=+/-0.0909301 intraUP=0.0842155 intraDN=-0.0446919
WTheoPDFSR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.13755,0.915754,"user","userNormHistoSys") #inter=+/-0.0708179 intraUP=0.117916 intraDN=-0.0456321
WTheoPDFVR5JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12492,0.898907,"user","userNormHistoSys") #inter=+/-0.0926812 intraUP=0.0837629 intraDN=-0.0403731
WTheoPDFVR5JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.16546,0.908421,"user","userNormHistoSys") #inter=+/-0.0772616 intraUP=0.146314 intraDN=-0.0491665
WTheoPDFSR5Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.1729,0.927595,"user","userNormHistoSys") #inter=+/-0.048935 intraUP=0.165827 intraDN=-0.0533652
#6J
WTheoPDFWR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11747,0.89514,"user","userNormHistoSys") #inter=+/-0.0981238 intraUP=0.0645831 intraDN=-0.0369788
WTheoPDFTR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.1235,0.896149,"user","userNormHistoSys") #inter=+/-0.0925237 intraUP=0.0817955 intraDN=-0.0471647
WTheoPDFSR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.14746,0.893326,"user","userNormHistoSys") #inter=+/-0.0914565 intraUP=0.115675 intraDN=-0.0549099
WTheoPDFVR6JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12559,0.89845,"user","userNormHistoSys") #inter=+/-0.0936021 intraUP=0.0837394 intraDN=-0.0393842
WTheoPDFVR6JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.12482,0.882138,"user","userNormHistoSys") #inter=+/-0.103521 intraUP=0.0697418 intraDN=-0.0563449
WTheoPDFSR6Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.12861,0.924375,"user","userNormHistoSys") #inter=+/-0.0586423 intraUP=0.114466 intraDN=-0.0477524
#7J
WTheoPDFWR7J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11614,0.902331,"user","userOverallSys") #inter=+/-0.0885644 intraUP=0.0751282 intraDN=-0.0411766
WTheoPDFTR7J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11629,0.895944,"user","userOverallSys") #inter=+/-0.0931281 intraUP=0.06964 intraDN=-0.0464205
WTheoPDFSR7J = Systematic("h1L_WTheoPDF",configMgr.weights,1.15138,0.87533,"user","userOverallSys") #inter=+/-0.114367 intraUP=0.0991791 intraDN=-0.0496266
WTheoPDFVR7JhighMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12639,0.896985,"user","userOverallSys") #inter=+/-0.0937886 intraUP=0.0847257 intraDN=-0.0426119
WTheoPDFVR7JhighMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.13038,0.875241,"user","userOverallSys") #inter=+/-0.116008 intraUP=0.0595061 intraDN=-0.0459017
WTheoPDFSR7Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.12527,0.907668,"user","userOverallSys") #inter=+/-0.0788882 intraUP=0.0973096 intraDN=-0.0479771

'''
#ELMU
#3J
WTheoPDFWR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11066,0.903415,"user","userNormHistoSys") #inter=+/-0.0907347 intraUP=0.0633485 intraDN=-0.033104
WTheoPDFTR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11902,0.89932,"user","userNormHistoSys") #inter=+/-0.091015 intraUP=0.0766946 intraDN=-0.0430445
WTheoPDFSR3J = Systematic("h1L_WTheoPDF",configMgr.weights,1.09079,0.921423,"user","userNormHistoSys") #inter=+/-0.0692586 intraUP=0.0587022 intraDN=-0.0371155
WTheoPDFVR3JMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.10724,0.909981,"user","userNormHistoSys") #inter=+/-0.0827571 intraUP=0.0682002 intraDN=-0.0354227
WTheoPDFVR3JMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.12238,0.901285,"user","userNormHistoSys") #inter=+/-0.0914277 intraUP=0.081347 intraDN=-0.037223
WTheoPDFSR3Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.11573,0.929618,"user","userNormHistoSys") #inter=+/-0.049256 intraUP=0.10473 intraDN=-0.050274

#5J
WTheoPDFWR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11663,0.900548,"user","userNormHistoSys") #inter=+/-0.0920398 intraUP=0.0716264 intraDN=-0.0376737
WTheoPDFTR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11656,0.90376,"user","userNormHistoSys") #inter=+/-0.0869527 intraUP=0.0776243 intraDN=-0.0412482
WTheoPDFSR5J = Systematic("h1L_WTheoPDF",configMgr.weights,1.14075,0.917423,"user","userNormHistoSys") #inter=+/-0.0677871 intraUP=0.123352 intraDN=-0.0471582
WTheoPDFVR5JMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12421,0.899425,"user","userNormHistoSys") #inter=+/-0.0922741 intraUP=0.083146 intraDN=-0.0400097
WTheoPDFVR5JMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.1784,0.913466,"user","userNormHistoSys") #inter=+/-0.0674169 intraUP=0.165169 intraDN=-0.0542507
WTheoPDFSR5Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.17314,0.918161,"user","userNormHistoSys") #inter=+/-0.0607071 intraUP=0.162148 intraDN=-0.054884

#6J
WTheoPDFWR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.11613,0.896763,"user","userNormHistoSys") #inter=+/-0.0963101 intraUP=0.0648957 intraDN=-0.0371788
WTheoPDFTR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.12366,0.895553,"user","userNormHistoSys") #inter=+/-0.09284 intraUP=0.0816846 intraDN=-0.047852
WTheoPDFSR6J = Systematic("h1L_WTheoPDF",configMgr.weights,1.15808,0.880471,"user","userNormHistoSys") #inter=+/-0.105247 intraUP=0.117947 intraDN=-0.0566587
WTheoPDFVR6JMET = Systematic("h1L_WTheoPDF",configMgr.weights,1.12372,0.899145,"user","userNormHistoSys") #inter=+/-0.09316 intraUP=0.0814146 intraDN=-0.0386376
WTheoPDFVR6JMT = Systematic("h1L_WTheoPDF",configMgr.weights,1.09355,0.911063,"user","userNormHistoSys") #inter=+/-0.0782261 intraUP=0.0513 intraDN=-0.0423148
WTheoPDFSR6Jdisc = Systematic("h1L_WTheoPDF",configMgr.weights,1.13644,0.910184,"user","userNormHistoSys") #inter=+/-0.074372 intraUP=0.114386 intraDN=-0.050356
'''


def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR3JEl"), WTheoPDFWR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR3JMu"), WTheoPDFWR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR3JEl"), WTheoPDFTR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR3JMu"), WTheoPDFTR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR5JEl"), WTheoPDFWR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR5JMu"), WTheoPDFWR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR5JEl"), WTheoPDFTR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR5JMu"), WTheoPDFTR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR6JEl"), WTheoPDFWR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR6JMu"), WTheoPDFWR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR6JEl"), WTheoPDFTR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR6JMu"), WTheoPDFTR6J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_WR7JEl"), WTheoPDFWR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR7JMu"), WTheoPDFWR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR7JEl"), WTheoPDFTR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR7JMu"), WTheoPDFTR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR7JEM"), WTheoPDFWR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR7JEM"), WTheoPDFTR7J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_WR3JEM"), WTheoPDFWR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR3JEM"), WTheoPDFTR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR5JEM"), WTheoPDFWR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR5JEM"), WTheoPDFTR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_WR6JEM"), WTheoPDFWR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_TR6JEM"), WTheoPDFTR6J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JEl"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JMu"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JEl"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JMu"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JEl"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JMu"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JdiscoveryEl"), WTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JdiscoveryMu"), WTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JdiscoveryEl"), WTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JdiscoveryMu"), WTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JdiscoveryEl"), WTheoPDFSR6Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JdiscoveryMu"), WTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaWMassiveBC","h1L_SR7JEl"), WTheoPDFSR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR7JMu"), WTheoPDFSR7J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR7JEM"), WTheoPDFSR7J))

    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JEM"), WTheoPDFSR5J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JEM"), WTheoPDFSR3J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JEM"), WTheoPDFSR6J))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR5JdiscoveryEM"), WTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR3JdiscoveryEM"), WTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaWMassiveBC","h1L_SR6JdiscoveryEM"), WTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMETEl"), WTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMETMu"), WTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMTEl"), WTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMTMu"), WTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMETEl"), WTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMETMu"), WTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMTEl"), WTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMTMu"), WTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMETEl"), WTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMETMu"), WTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMTEl"), WTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMTMu"), WTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaWMassiveBC","h1L_VR7JhighMETEl"), WTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR7JhighMETMu"), WTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR7JhighMTEl"), WTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR7JhighMTMu"), WTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR7JhighMETEM"), WTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR7JhighMTEM"), WTheoPDFVR7JhighMT))

    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMETEM"), WTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR3JhighMTEM"), WTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMETEM"), WTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR5JhighMTEM"), WTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMETEM"), WTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaWMassiveBC","h1L_VR6JhighMTEM"), WTheoPDFVR6JhighMT))

    return generatorSyst
