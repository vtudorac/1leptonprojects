import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11 
# Raw stat in the discovery SRs: SR3: 3+18; SR5: 1+8; SR6: 0+2 (ele+muo)
ZTheoPDFWR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12516,0.904247,"user","userNormHistoSys") #inter=+/-0.0846448 intraUP=0.0921978 intraDN=-0.0447659
ZTheoPDFTR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.09299,0.917192,"user","userNormHistoSys") #inter=+/-0.048371 intraUP=0.0794209 intraDN=-0.0672116
ZTheoPDFSR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14187,0.87354,"user","userNormHistoSys") #inter=+/-0.109634 intraUP=0.0900356 intraDN=-0.0630277
ZTheoPDFVR3JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.13131,0.899078,"user","userNormHistoSys") #inter=+/-0.088219 intraUP=0.0972621 intraDN=-0.0490179
ZTheoPDFVR3JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.11954,0.884087,"user","userNormHistoSys") #inter=+/-0.11068 intraUP=0.0451505 intraDN=-0.034435
ZTheoPDFSRdisc3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.13185,0.907638,"user","userNormHistoSys") #inter=+/-0.0671541 intraUP=0.113466 intraDN=-0.0634113

ZTheoPDFWR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.11611,0.888747,"user","userNormHistoSys") #inter=+/-0.104258 intraUP=0.0510966 intraDN=-0.0388257
ZTheoPDFTR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.22176,0.824384,"user","userNormHistoSys") #inter=+/-0.153948 intraUP=0.159613 intraDN=-0.0845032
ZTheoPDFSR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.10582,0.917276,"user","userNormHistoSys") #inter=+/-0.0471531 intraUP=0.0947311 intraDN=-0.0679693
ZTheoPDFVR5JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.25344,0.861138,"user","userNormHistoSys") #inter=+/-0.109206 intraUP=0.228706 intraDN=-0.0857701
ZTheoPDFVR5JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14546,0.877895,"user","userNormHistoSys") #inter=+/-0.105389 intraUP=0.100251 intraDN=-0.0616658
ZTheoPDFSRdisc5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.10771,0.91914,"user","userNormHistoSys") #inter=+/-0.0409975 intraUP=0.0995977 intraDN=-0.0696965

ZTheoPDFWR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12262,0.889085,"user","userNormHistoSys") #inter=+/-0.102577 intraUP=0.067175 intraDN=-0.0421916
ZTheoPDFTR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.07135,0.929118,"user","userNormHistoSys") #inter=+/-0.0280558 intraUP=0.0656046 intraDN=-0.0650935
ZTheoPDFSR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.09111,0.922437,"user","userNormHistoSys") #inter=+/-0.0244372 intraUP=0.0877676 intraDN=-0.0736132
ZTheoPDFVR6JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.06645,0.957972,"user","userNormHistoSys") #inter=+/-0.0226747 intraUP=0.0624648 intraDN=-0.0353865
ZTheoPDFVR6JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.28251,0.902189,"user","userNormHistoSys") #inter=+/-0.00930056 intraUP=0.282359 intraDN=-0.0973679
ZTheoPDFSRdisc6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.08424,0.9223,"user","userNormHistoSys") #inter=+/-0.0744844 intraUP=0.039341 intraDN=-0.0221201

def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR3JEl"), ZTheoPDFWR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR3JMu"), ZTheoPDFWR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR3JEl"), ZTheoPDFTR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR3JMu"), ZTheoPDFTR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR5JEl"), ZTheoPDFWR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR5JMu"), ZTheoPDFWR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR5JEl"), ZTheoPDFTR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR5JMu"), ZTheoPDFTR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR6JEl"), ZTheoPDFWR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR6JMu"), ZTheoPDFWR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR6JEl"), ZTheoPDFTR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR6JMu"), ZTheoPDFTR6J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_WR3JEM"), ZTheoPDFWR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR3JEM"), ZTheoPDFTR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR5JEM"), ZTheoPDFWR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR5JEM"), ZTheoPDFTR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR6JEM"), ZTheoPDFWR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR6JEM"), ZTheoPDFTR6J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JEl"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JMu"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JEl"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JMu"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JEl"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JMu"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JdiscoveryEl"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JdiscoveryMu"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JdiscoveryEl"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JdiscoveryMu"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JdiscoveryEl"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JdiscoveryMu"), ZTheoPDFSR6J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JEM"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JEM"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JEM"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JdiscoveryEM"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JdiscoveryEM"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JdiscoveryEM"), ZTheoPDFSR6J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMETEl"), ZTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMETMu"), ZTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMTEl"), ZTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMTMu"), ZTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMETEl"), ZTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMETMu"), ZTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMTEl"), ZTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMTMu"), ZTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMETEl"), ZTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMETMu"), ZTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMTEl"), ZTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMTMu"), ZTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMETEM"), ZTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMTEM"), ZTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMETEM"), ZTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMTEM"), ZTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMETEM"), ZTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMTEM"), ZTheoPDFVR6JhighMT))

    return generatorSyst
