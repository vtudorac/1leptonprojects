import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
'''
# e+mu v11 
# Raw stat in the discovery SRs: SR3: 3+18; SR5: 1+8; SR6: 0+2 (ele+muo)
ZTheoPDFWR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12516,0.904247,"user","userOverallSys") #inter=+/-0.0846448 intraUP=0.0921978 intraDN=-0.0447659
ZTheoPDFTR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.09299,0.917192,"user","userOverallSys") #inter=+/-0.048371 intraUP=0.0794209 intraDN=-0.0672116
ZTheoPDFSR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14187,0.87354,"user","userOverallSys") #inter=+/-0.109634 intraUP=0.0900356 intraDN=-0.0630277
ZTheoPDFVR3JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.13131,0.899078,"user","userOverallSys") #inter=+/-0.088219 intraUP=0.0972621 intraDN=-0.0490179
ZTheoPDFVR3JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.11954,0.884087,"user","userOverallSys") #inter=+/-0.11068 intraUP=0.0451505 intraDN=-0.034435
ZTheoPDFSRdisc3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.13185,0.907638,"user","userOverallSys") #inter=+/-0.0671541 intraUP=0.113466 intraDN=-0.0634113

ZTheoPDFWR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.11611,0.888747,"user","userOverallSys") #inter=+/-0.104258 intraUP=0.0510966 intraDN=-0.0388257
ZTheoPDFTR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.22176,0.824384,"user","userOverallSys") #inter=+/-0.153948 intraUP=0.159613 intraDN=-0.0845032
ZTheoPDFSR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.10582,0.917276,"user","userOverallSys") #inter=+/-0.0471531 intraUP=0.0947311 intraDN=-0.0679693
ZTheoPDFVR5JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.25344,0.861138,"user","userOverallSys") #inter=+/-0.109206 intraUP=0.228706 intraDN=-0.0857701
ZTheoPDFVR5JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14546,0.877895,"user","userOverallSys") #inter=+/-0.105389 intraUP=0.100251 intraDN=-0.0616658
ZTheoPDFSRdisc5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.10771,0.91914,"user","userOverallSys") #inter=+/-0.0409975 intraUP=0.0995977 intraDN=-0.0696965

ZTheoPDFWR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12262,0.889085,"user","userOverallSys") #inter=+/-0.102577 intraUP=0.067175 intraDN=-0.0421916
ZTheoPDFTR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.07135,0.929118,"user","userOverallSys") #inter=+/-0.0280558 intraUP=0.0656046 intraDN=-0.0650935
ZTheoPDFSR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.09111,0.922437,"user","userOverallSys") #inter=+/-0.0244372 intraUP=0.0877676 intraDN=-0.0736132
ZTheoPDFVR6JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.06645,0.957972,"user","userOverallSys") #inter=+/-0.0226747 intraUP=0.0624648 intraDN=-0.0353865
ZTheoPDFVR6JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.28251,0.902189,"user","userOverallSys") #inter=+/-0.00930056 intraUP=0.282359 intraDN=-0.0973679
ZTheoPDFSRdisc6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.08424,0.9223,"user","userOverallSys") #inter=+/-0.0744844 intraUP=0.039341 intraDN=-0.0221201
'''
#3J
ZTheoPDFWR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14484,0.895059,"user","userOverallSys") #inter=+/-0.0853579 intraUP=0.117016 intraDN=-0.0610456
ZTheoPDFTR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.10855,0.914334,"user","userOverallSys") #inter=+/-0.0457494 intraUP=0.0984346 intraDN=-0.0724275
ZTheoPDFSR3J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.15201,0.871717,"user","userOverallSys") #inter=+/-0.0998134 intraUP=0.114651 intraDN=-0.0805849
ZTheoPDFVR3JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14794,0.89181,"user","userOverallSys") #inter=+/-0.0864349 intraUP=0.120067 intraDN=-0.0650691
ZTheoPDFVR3JhighMT= Systematic("h1L_ZTheoPDF",configMgr.weights,1.13058,0.878321,"user","userOverallSys") #inter=+/-0.110852 intraUP=0.0690129 intraDN=-0.0501761
ZTheoPDFSR3Jdisc = Systematic("h1L_ZTheoPDF",configMgr.weights,1.1566,0.89074,"user","userOverallSys") #inter=+/-0.076741 intraUP=0.136504 intraDN=-0.0777723
#5J
ZTheoPDFWR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12747,0.881667,"user","userOverallSys") #inter=+/-0.104268 intraUP=0.0733267 intraDN=-0.0559546
ZTheoPDFTR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.23593,0.823876,"user","userOverallSys") #inter=+/-0.152616 intraUP=0.179919 intraDN=-0.0879086
ZTheoPDFSR5J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12432,0.904436,"user","userOverallSys") #inter=+/-0.052876 intraUP=0.112518 intraDN=-0.0796025
ZTheoPDFVR5JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.2744,0.854026,"user","userOverallSys") #inter=+/-0.10931 intraUP=0.251686 intraDN=-0.096745
ZTheoPDFVR5JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.15229,0.877279,"user","userOverallSys") #inter=+/-0.100954 intraUP=0.114025 intraDN=-0.0697767
ZTheoPDFSR5Jdisc = Systematic("h1L_ZTheoPDF",configMgr.weights,1.09455,0.927851,"user","userOverallSys") #inter=+/-0.00880073 intraUP=0.0941441 intraDN=-0.0716105
#6J
ZTheoPDFWR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.1363,0.882395,"user","userOverallSys") #inter=+/-0.102606 intraUP=0.0897244 intraDN=-0.0574718
ZTheoPDFTR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.08901,0.923081,"user","userOverallSys") #inter=+/-0.0327945 intraUP=0.0827461 intraDN=-0.0695776
ZTheoPDFSR6J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.11102,0.871143,"user","userOverallSys") #inter=+/-0.0807986 intraUP=0.0761411 intraDN=-0.100378
ZTheoPDFVR6JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.08026,0.949556,"user","userOverallSys") #inter=+/-0.0217681 intraUP=0.0772516 intraDN=-0.0455059
ZTheoPDFVR6JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.31396,0.894825,"user","userOverallSys") #inter=+/-0.00864849 intraUP=0.313841 intraDN=-0.104819
ZTheoPDFSR6Jdisc = Systematic("h1L_ZTheoPDF",configMgr.weights,1.15472,0.829543,"user","userOverallSys") #inter=+/-0.138277 intraUP=0.069412 intraDN=-0.0996747
#7J
ZTheoPDFWR7J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.18838,0.840229,"user","userOverallSys") #inter=+/-0.142935 intraUP=0.122711 intraDN=-0.0713892
ZTheoPDFTR7J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.13313,0.879627,"user","userOverallSys") #inter=+/-0.109077 intraUP=0.076335 intraDN=-0.0509089
ZTheoPDFSR7J = Systematic("h1L_ZTheoPDF",configMgr.weights,1.14738,0.876915,"user","userOverallSys") #inter=+/-0.0808111 intraUP=0.123245 intraDN=-0.0928416
ZTheoPDFVR7JhighMET = Systematic("h1L_ZTheoPDF",configMgr.weights,1.12708,0.89367,"user","userOverallSys") #inter=+/-0.0834933 intraUP=0.0958015 intraDN=-0.0658399
ZTheoPDFVR7JhighMT = Systematic("h1L_ZTheoPDF",configMgr.weights,1.22163,0.874805,"user","userOverallSys") #inter=+/-0.108973 intraUP=0.192994 intraDN=-0.0616325
ZTheoPDFSR7Jdisc = Systematic("h1L_ZTheoPDF",configMgr.weights,1.0864,0.91665,"user","userOverallSys") #inter=+/-0.0744844 intraUP=0.0437778 intraDN=-0.0374082


def TheorUnc(generatorSyst):
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR3JEl"), ZTheoPDFWR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR3JMu"), ZTheoPDFWR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR3JEl"), ZTheoPDFTR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR3JMu"), ZTheoPDFTR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR5JEl"), ZTheoPDFWR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR5JMu"), ZTheoPDFWR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR5JEl"), ZTheoPDFTR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR5JMu"), ZTheoPDFTR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR6JEl"), ZTheoPDFWR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR6JMu"), ZTheoPDFWR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR6JEl"), ZTheoPDFTR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR6JMu"), ZTheoPDFTR6J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_WR3JEM"), ZTheoPDFWR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR3JEM"), ZTheoPDFTR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR5JEM"), ZTheoPDFWR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR5JEM"), ZTheoPDFTR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR6JEM"), ZTheoPDFWR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR6JEM"), ZTheoPDFTR6J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_WR7JEl"), ZTheoPDFWR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR7JMu"), ZTheoPDFWR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR7JEl"), ZTheoPDFTR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR7JMu"), ZTheoPDFTR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_WR7JEM"), ZTheoPDFWR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_TR7JEM"), ZTheoPDFTR7J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JEl"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JMu"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JEl"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JMu"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JEl"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JMu"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JdiscoveryEl"), ZTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JdiscoveryMu"), ZTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JdiscoveryEl"), ZTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JdiscoveryMu"), ZTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JdiscoveryEl"), ZTheoPDFSR6Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JdiscoveryMu"), ZTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaZMassiveBC","h1L_SR7JEl"), ZTheoPDFSR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR7JMu"), ZTheoPDFSR7J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR7JEM"), ZTheoPDFSR7J))

    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JEM"), ZTheoPDFSR5J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JEM"), ZTheoPDFSR3J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JEM"), ZTheoPDFSR6J))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR5JdiscoveryEM"), ZTheoPDFSR5Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR3JdiscoveryEM"), ZTheoPDFSR3Jdisc))
    generatorSyst.append((("SherpaZMassiveBC","h1L_SR6JdiscoveryEM"), ZTheoPDFSR6Jdisc))

    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMETEl"), ZTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMETMu"), ZTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMTEl"), ZTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMTMu"), ZTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMETEl"), ZTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMETMu"), ZTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMTEl"), ZTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMTMu"), ZTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMETEl"), ZTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMETMu"), ZTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMTEl"), ZTheoPDFVR6JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMTMu"), ZTheoPDFVR6JhighMT))

    generatorSyst.append((("SherpaZMassiveBC","h1L_VR7JhighMETEl"), ZTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR7JhighMETMu"), ZTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR7JhighMTEl"), ZTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR7JhighMTMu"), ZTheoPDFVR7JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR7JhighMETEM"), ZTheoPDFVR7JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR7JhighMTEM"), ZTheoPDFVR7JhighMT))

    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMETEM"), ZTheoPDFVR3JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR3JhighMTEM"), ZTheoPDFVR3JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMETEM"), ZTheoPDFVR5JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR5JhighMTEM"), ZTheoPDFVR5JhighMT))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMETEM"), ZTheoPDFVR6JhighMET))
    generatorSyst.append((("SherpaZMassiveBC","h1L_VR6JhighMTEM"), ZTheoPDFVR6JhighMT))

    return generatorSyst
