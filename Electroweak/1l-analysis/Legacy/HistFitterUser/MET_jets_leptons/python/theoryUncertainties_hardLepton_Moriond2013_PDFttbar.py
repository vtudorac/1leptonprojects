import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11
# Raw stat in the discovery SRs: SR3: 38+37; SR5: 75+77; SR6: 53+50 (ele+muo)
#3J
topTheoPDFWR3J = Systematic("h1L_topTheoPDF_3J",configMgr.weights,1.08435,0.938548,"user","userNormHistoSys") #inter=+/-0.0224999 intraUP=0.0812941 intraDN=-0.057185
topTheoPDFTR3J = Systematic("h1L_topTheoPDF_3J",configMgr.weights,1.08766,0.937118,"user","userNormHistoSys") #inter=+/-0.0227812 intraUP=0.0846484 intraDN=-0.0586098
topTheoPDFSR3J = Systematic("h1L_topTheoPDF_3J",configMgr.weights,1.10408,0.931015,"user","userNormHistoSys") #inter=+/-0.0219292 intraUP=0.10174 intraDN=-0.0654064
topTheoPDFVR3JhighMET = Systematic("h1L_topTheoPDF_3J",configMgr.weights,1.10937,0.933172,"user","userNormHistoSys") #inter=+/-0.0175923 intraUP=0.107946 intraDN=-0.0644714
topTheoPDFVR3JhighMT = Systematic("h1L_topTheoPDF_3J",configMgr.weights,1.07936,0.935006,"user","userNormHistoSys") #inter=+/-0.0255057 intraUP=0.0751487 intraDN=-0.0597808
topTheoPDFSRdisc3J = Systematic("h1L_topTheoPDF_3J",configMgr.weights,1.12232,0.927806,"user","userNormHistoSys") #inter=+/-0.0205842 intraUP=0.120574 intraDN=-0.0691977
#5J
topTheoPDFWR5J = Systematic("h1L_topTheoPDF_5J",configMgr.weights,1.08086,0.936003,"user","userNormHistoSys") #inter=+/-0.0243693 intraUP=0.0771027 intraDN=-0.0591754
topTheoPDFTR5J = Systematic("h1L_topTheoPDF_5J",configMgr.weights,1.08591,0.933607,"user","userNormHistoSys") #inter=+/-0.0249925 intraUP=0.0821905 intraDN=-0.0615095
topTheoPDFSR5J = Systematic("h1L_topTheoPDF_5J",configMgr.weights,1.08655,0.927884,"user","userNormHistoSys") #inter=+/-0.0293591 intraUP=0.0814224 intraDN=-0.0658695
topTheoPDFVR5JhighMET = Systematic("h1L_topTheoPDF_5J",configMgr.weights,1.11509,0.927822,"user","userNormHistoSys") #inter=+/-0.0197774 intraUP=0.113382 intraDN=-0.0694153
topTheoPDFVR5JhighMT = Systematic("h1L_topTheoPDF_5J",configMgr.weights,1.08098,0.933595,"user","userNormHistoSys") #inter=+/-0.0247008 intraUP=0.0771178 intraDN=-0.0616397
topTheoPDFSRdisc5J = Systematic("h1L_topTheoPDF_5J",configMgr.weights,1.09483,0.927832,"user","userNormHistoSys") #inter=+/-0.030418 intraUP=0.0898217 intraDN=-0.0654441
#6J
topTheoPDFWR6J = Systematic("h1L_topTheoPDF_6J",configMgr.weights,1.066,0.936362,"user","userNormHistoSys") #inter=+/-0.0290008 intraUP=0.0592893 intraDN=-0.0566458
topTheoPDFTR6J = Systematic("h1L_topTheoPDF_6J",configMgr.weights,1.07657,0.932144,"user","userNormHistoSys") #inter=+/-0.0268493 intraUP=0.0717105 intraDN=-0.0623184
topTheoPDFSR6J = Systematic("h1L_topTheoPDF_6J",configMgr.weights,1.10243,0.930379,"user","userNormHistoSys") #inter=+/-0.0180523 intraUP=0.10083 intraDN=-0.0672402
topTheoPDFVR6JhighMET = Systematic("h1L_topTheoPDF_6J",configMgr.weights,1.0845,0.93079,"user","userNormHistoSys") #inter=+/-0.0257469 intraUP=0.0804793 intraDN=-0.0642422
topTheoPDFVR6JhighMT = Systematic("h1L_topTheoPDF_6J",configMgr.weights,1.07515,0.932064,"user","userNormHistoSys") #inter=+/-0.0267926 intraUP=0.0702111 intraDN=-0.0624296
topTheoPDFSRdisc6J = Systematic("h1L_topTheoPDF_6J",configMgr.weights,1.09927,0.929862,"user","userNormHistoSys") #inter=+/-0.0245707 intraUP=0.0961854 intraDN=-0.065693

def TheorUnc(generatorSyst):
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JEl"), topTheoPDFWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JMu"), topTheoPDFWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JEl"), topTheoPDFTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JMu"), topTheoPDFTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JEl"), topTheoPDFWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JMu"), topTheoPDFWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JEl"), topTheoPDFTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JMu"), topTheoPDFTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JEl"), topTheoPDFWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JMu"), topTheoPDFWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JEl"), topTheoPDFTR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JMu"), topTheoPDFTR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JEM"), topTheoPDFWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JEM"), topTheoPDFTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JEM"), topTheoPDFWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JEM"), topTheoPDFTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JEM"), topTheoPDFWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JEM"), topTheoPDFTR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JEl"), topTheoPDFSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JMu"), topTheoPDFSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JEl"), topTheoPDFSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JMu"), topTheoPDFSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JEl"), topTheoPDFSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JMu"), topTheoPDFSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryEl"), topTheoPDFSRdisc5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryMu"), topTheoPDFSRdisc5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryEl"), topTheoPDFSRdisc3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryMu"), topTheoPDFSRdisc3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryEl"), topTheoPDFSRdisc6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryMu"), topTheoPDFSRdisc6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JEM"), topTheoPDFSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JEM"), topTheoPDFSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JEM"), topTheoPDFSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryEM"), topTheoPDFSRdisc5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryEM"), topTheoPDFSRdisc3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryEM"), topTheoPDFSRdisc6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETEl"), topTheoPDFVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETMu"), topTheoPDFVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTEl"), topTheoPDFVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTMu"), topTheoPDFVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETEl"), topTheoPDFVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETMu"), topTheoPDFVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTEl"), topTheoPDFVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTMu"), topTheoPDFVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETEl"), topTheoPDFVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETMu"), topTheoPDFVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTEl"), topTheoPDFVR6JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTMu"), topTheoPDFVR6JhighMT))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETEM"), topTheoPDFVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTEM"), topTheoPDFVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETEM"), topTheoPDFVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTEM"), topTheoPDFVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETEM"), topTheoPDFVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTEM"), topTheoPDFVR6JhighMT))

    return generatorSyst
