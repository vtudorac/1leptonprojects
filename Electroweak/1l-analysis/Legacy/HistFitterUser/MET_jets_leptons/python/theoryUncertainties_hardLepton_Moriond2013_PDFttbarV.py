import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

# e+mu v11 
# Raw stat in the discovery SRs: SR3: 32+30; SR5: 60+41; SR6: 54+43 (ele+muo)
ttbarVTheoPDFWR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.17783,0.945904,"user","userNormHistoSys") #inter=+/-0.0540964 intraUP=0.169401 intraDN=0
ttbarVTheoPDFTR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.09548,0.96831,"user","userNormHistoSys") #inter=+/-0.0316896 intraUP=0.0900655 intraDN=0
ttbarVTheoPDFSR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.19664,0.931926,"user","userNormHistoSys") #inter=+/-0.068074 intraUP=0.184482 intraDN=0
ttbarVTheoPDFVR3JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.112,0.963133,"user","userNormHistoSys") #inter=+/-0.0368671 intraUP=0.105759 intraDN=0
ttbarVTheoPDFVR3JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.13479,0.95705,"user","userNormHistoSys") #inter=+/-0.0429498 intraUP=0.127766 intraDN=0
ttbarVTheoPDFSRdisc3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.50811,0.842972,"user","userNormHistoSys") #inter=+/-0.157028 intraUP=0.483233 intraDN=0

ttbarVTheoPDFWR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.09975,0.969412,"user","userNormHistoSys") #inter=+/-0.0305879 intraUP=0.0949467 intraDN=0
ttbarVTheoPDFTR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.1173,0.960827,"user","userNormHistoSys") #inter=+/-0.0391735 intraUP=0.110563 intraDN=0
ttbarVTheoPDFSR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.25825,0.915711,"user","userNormHistoSys") #inter=+/-0.0842888 intraUP=0.244103 intraDN=0
ttbarVTheoPDFVR5JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.17593,0.943848,"user","userNormHistoSys") #inter=+/-0.0561523 intraUP=0.166726 intraDN=0
ttbarVTheoPDFVR5JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.11559,0.963647,"user","userNormHistoSys") #inter=+/-0.0363535 intraUP=0.109722 intraDN=0
ttbarVTheoPDFSRdisc5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.35548,0.883022,"user","userNormHistoSys") #inter=+/-0.116978 intraUP=0.335687 intraDN=0

ttbarVTheoPDFWR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.14504,0.953715,"user","userNormHistoSys") #inter=+/-0.046285 intraUP=0.137456 intraDN=0
ttbarVTheoPDFTR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.15088,0.951232,"user","userNormHistoSys") #inter=+/-0.0487676 intraUP=0.142785 intraDN=0
ttbarVTheoPDFSR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.18207,0.941252,"user","userNormHistoSys") #inter=+/-0.0587479 intraUP=0.172327 intraDN=0
ttbarVTheoPDFVR6JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.13898,0.957324,"user","userNormHistoSys") #inter=+/-0.0426759 intraUP=0.132267 intraDN=0
ttbarVTheoPDFVR6JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.14998,0.950074,"user","userNormHistoSys") #inter=+/-0.0499261 intraUP=0.141429 intraDN=0
ttbarVTheoPDFSRdisc6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.22893,0.915115,"user","userNormHistoSys") #inter=+/-0.0848848 intraUP=0.212611 intraDN=0

def TheorUnc(generatorSyst):
    generatorSyst.append((("ttbarV","h1L_WR3JEl"), ttbarVTheoPDFWR3J))
    generatorSyst.append((("ttbarV","h1L_WR3JMu"), ttbarVTheoPDFWR3J))
    generatorSyst.append((("ttbarV","h1L_TR3JEl"), ttbarVTheoPDFTR3J))
    generatorSyst.append((("ttbarV","h1L_TR3JMu"), ttbarVTheoPDFTR3J))
    generatorSyst.append((("ttbarV","h1L_WR5JEl"), ttbarVTheoPDFWR5J))
    generatorSyst.append((("ttbarV","h1L_WR5JMu"), ttbarVTheoPDFWR5J))
    generatorSyst.append((("ttbarV","h1L_TR5JEl"), ttbarVTheoPDFTR5J))
    generatorSyst.append((("ttbarV","h1L_TR5JMu"), ttbarVTheoPDFTR5J))
    generatorSyst.append((("ttbarV","h1L_WR6JEl"), ttbarVTheoPDFWR6J))
    generatorSyst.append((("ttbarV","h1L_WR6JMu"), ttbarVTheoPDFWR6J))
    generatorSyst.append((("ttbarV","h1L_TR6JEl"), ttbarVTheoPDFTR6J))
    generatorSyst.append((("ttbarV","h1L_TR6JMu"), ttbarVTheoPDFTR6J))

    generatorSyst.append((("ttbarV","h1L_WR3JEM"), ttbarVTheoPDFWR3J))
    generatorSyst.append((("ttbarV","h1L_TR3JEM"), ttbarVTheoPDFTR3J))
    generatorSyst.append((("ttbarV","h1L_WR5JEM"), ttbarVTheoPDFWR5J))
    generatorSyst.append((("ttbarV","h1L_TR5JEM"), ttbarVTheoPDFTR5J))
    generatorSyst.append((("ttbarV","h1L_WR6JEM"), ttbarVTheoPDFWR6J))
    generatorSyst.append((("ttbarV","h1L_TR6JEM"), ttbarVTheoPDFTR6J))

    generatorSyst.append((("ttbarV","h1L_SR5JEl"), ttbarVTheoPDFSR5J))
    generatorSyst.append((("ttbarV","h1L_SR5JMu"), ttbarVTheoPDFSR5J))
    generatorSyst.append((("ttbarV","h1L_SR3JEl"), ttbarVTheoPDFSR3J))
    generatorSyst.append((("ttbarV","h1L_SR3JMu"), ttbarVTheoPDFSR3J))
    generatorSyst.append((("ttbarV","h1L_SR6JEl"), ttbarVTheoPDFSR6J))
    generatorSyst.append((("ttbarV","h1L_SR6JMu"), ttbarVTheoPDFSR6J))
    generatorSyst.append((("ttbarV","h1L_SR5JdiscoveryEl"), ttbarVTheoPDFSRdisc5J))
    generatorSyst.append((("ttbarV","h1L_SR5JdiscoveryMu"), ttbarVTheoPDFSRdisc5J))
    generatorSyst.append((("ttbarV","h1L_SR3JdiscoveryEl"), ttbarVTheoPDFSRdisc3J))
    generatorSyst.append((("ttbarV","h1L_SR3JdiscoveryMu"), ttbarVTheoPDFSRdisc3J))
    generatorSyst.append((("ttbarV","h1L_SR6JdiscoveryEl"), ttbarVTheoPDFSRdisc6J))
    generatorSyst.append((("ttbarV","h1L_SR6JdiscoveryMu"), ttbarVTheoPDFSRdisc6J))

    generatorSyst.append((("ttbarV","h1L_SR5JEM"), ttbarVTheoPDFSR5J))
    generatorSyst.append((("ttbarV","h1L_SR3JEM"), ttbarVTheoPDFSR3J))
    generatorSyst.append((("ttbarV","h1L_SR6JEM"), ttbarVTheoPDFSR6J))
    generatorSyst.append((("ttbarV","h1L_SR5JdiscoveryEM"), ttbarVTheoPDFSRdisc5J))
    generatorSyst.append((("ttbarV","h1L_SR3JdiscoveryEM"), ttbarVTheoPDFSRdisc3J))
    generatorSyst.append((("ttbarV","h1L_SR6JdiscoveryEM"), ttbarVTheoPDFSRdisc6J))

    generatorSyst.append((("ttbarV","h1L_VR3JhighMETEl"), ttbarVTheoPDFVR3JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMETMu"), ttbarVTheoPDFVR3JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMTEl"), ttbarVTheoPDFVR3JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMTMu"), ttbarVTheoPDFVR3JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMETEl"), ttbarVTheoPDFVR5JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMETMu"), ttbarVTheoPDFVR5JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMTEl"), ttbarVTheoPDFVR5JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMTMu"), ttbarVTheoPDFVR5JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMETEl"), ttbarVTheoPDFVR6JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMETMu"), ttbarVTheoPDFVR6JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMTEl"), ttbarVTheoPDFVR6JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMTMu"), ttbarVTheoPDFVR6JhighMT))

    generatorSyst.append((("ttbarV","h1L_VR3JhighMETEM"), ttbarVTheoPDFVR3JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMTEM"), ttbarVTheoPDFVR3JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMETEM"), ttbarVTheoPDFVR5JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMTEM"), ttbarVTheoPDFVR5JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMETEM"), ttbarVTheoPDFVR6JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMTEM"), ttbarVTheoPDFVR6JhighMT))

    return generatorSyst
