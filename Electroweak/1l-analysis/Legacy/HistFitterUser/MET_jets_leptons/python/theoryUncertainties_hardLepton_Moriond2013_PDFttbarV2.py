import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
'''
# e+mu v11 
# Raw stat in the discovery SRs: SR3: 32+30; SR5: 60+41; SR6: 54+43 (ele+muo)
ttbarVTheoPDFWR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.17783,0.945904,"user","userOverallSys") #inter=+/-0.0540964 intraUP=0.169401 intraDN=0
ttbarVTheoPDFTR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.09548,0.96831,"user","userOverallSys") #inter=+/-0.0316896 intraUP=0.0900655 intraDN=0
ttbarVTheoPDFSR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.19664,0.931926,"user","userOverallSys") #inter=+/-0.068074 intraUP=0.184482 intraDN=0
ttbarVTheoPDFVR3JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.112,0.963133,"user","userOverallSys") #inter=+/-0.0368671 intraUP=0.105759 intraDN=0
ttbarVTheoPDFVR3JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.13479,0.95705,"user","userOverallSys") #inter=+/-0.0429498 intraUP=0.127766 intraDN=0
ttbarVTheoPDFSRdisc3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.50811,0.842972,"user","userOverallSys") #inter=+/-0.157028 intraUP=0.483233 intraDN=0

ttbarVTheoPDFWR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.09975,0.969412,"user","userOverallSys") #inter=+/-0.0305879 intraUP=0.0949467 intraDN=0
ttbarVTheoPDFTR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.1173,0.960827,"user","userOverallSys") #inter=+/-0.0391735 intraUP=0.110563 intraDN=0
ttbarVTheoPDFSR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.25825,0.915711,"user","userOverallSys") #inter=+/-0.0842888 intraUP=0.244103 intraDN=0
ttbarVTheoPDFVR5JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.17593,0.943848,"user","userOverallSys") #inter=+/-0.0561523 intraUP=0.166726 intraDN=0
ttbarVTheoPDFVR5JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.11559,0.963647,"user","userOverallSys") #inter=+/-0.0363535 intraUP=0.109722 intraDN=0
ttbarVTheoPDFSRdisc5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.35548,0.883022,"user","userOverallSys") #inter=+/-0.116978 intraUP=0.335687 intraDN=0

ttbarVTheoPDFWR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.14504,0.953715,"user","userOverallSys") #inter=+/-0.046285 intraUP=0.137456 intraDN=0
ttbarVTheoPDFTR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.15088,0.951232,"user","userOverallSys") #inter=+/-0.0487676 intraUP=0.142785 intraDN=0
ttbarVTheoPDFSR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.18207,0.941252,"user","userOverallSys") #inter=+/-0.0587479 intraUP=0.172327 intraDN=0
ttbarVTheoPDFVR6JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.13898,0.957324,"user","userOverallSys") #inter=+/-0.0426759 intraUP=0.132267 intraDN=0
ttbarVTheoPDFVR6JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.14998,0.950074,"user","userOverallSys") #inter=+/-0.0499261 intraUP=0.141429 intraDN=0
ttbarVTheoPDFSRdisc6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.22893,0.915115,"user","userOverallSys") #inter=+/-0.0848848 intraUP=0.212611 intraDN=0
'''
#3J
ttbarVTheoPDFWR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.09767,0.932639,"user","userOverallSys") #inter=+/-0.0620557 intraUP=0.0754175 intraDN=-0.0262027
ttbarVTheoPDFTR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.05477,0.963253,"user","userOverallSys") #inter=+/-0.0342719 intraUP=0.0427198 intraDN=-0.0132568
ttbarVTheoPDFSR3J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.11814,0.908091,"user","userOverallSys") #inter=+/-0.0878937 intraUP=0.0789461 intraDN=-0.0268698
ttbarVTheoPDFVR3JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.07521,0.958826,"user","userOverallSys") #inter=+/-0.0363266 intraUP=0.065861 intraDN=-0.0193814
ttbarVTheoPDFVR3JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.10649,0.909827,"user","userOverallSys") #inter=+/-0.0883296 intraUP=0.0594748 intraDN=-0.0181386
ttbarVTheoPDFSR3Jdisc = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.37097,0.754074,"user","userOverallSys") #inter=+/-0.222688 intraUP=0.296695 intraDN=-0.104355
#5J
ttbarVTheoPDFWR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.08884,0.924175,"user","userOverallSys") #inter=+/-0.0738883 intraUP=0.0493271 intraDN=-0.0170281
ttbarVTheoPDFTR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.07143,0.94276,"user","userOverallSys") #inter=+/-0.0554555 intraUP=0.0450239 intraDN=-0.0141822
ttbarVTheoPDFSR5J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.22453,0.81183,"user","userOverallSys") #inter=+/-0.181801 intraUP=0.131759 intraDN=-0.0485416
ttbarVTheoPDFVR5JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.15993,0.884032,"user","userOverallSys") #inter=+/-0.11094 intraUP=0.115193 intraDN=-0.0337759
ttbarVTheoPDFVR5JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.10025,0.910781,"user","userOverallSys") #inter=+/-0.0875553 intraUP=0.0488333 intraDN=-0.0171506
ttbarVTheoPDFSR5Jdisc = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.30433,0.773874,"user","userOverallSys") #inter=+/-0.211085 intraUP=0.219225 intraDN=-0.0810947
#6J
ttbarVTheoPDFWR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.12372,0.882953,"user","userOverallSys") #inter=+/-0.115902 intraUP=0.0432767 intraDN=-0.0163314
ttbarVTheoPDFTR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.11149,0.896739,"user","userOverallSys") #inter=+/-0.102169 intraUP=0.044619 intraDN=-0.0149755
ttbarVTheoPDFSR6J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.17741,0.846445,"user","userOverallSys") #inter=+/-0.14961 intraUP=0.0953553 intraDN=-0.0345823
ttbarVTheoPDFVR6JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.19692,0.846687,"user","userOverallSys") #inter=+/-0.148216 intraUP=0.129644 intraDN=-0.0392014
ttbarVTheoPDFVR6JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.1884,0.83852,"user","userOverallSys") #inter=+/-0.157996 intraUP=0.102619 intraDN=-0.0333612
ttbarVTheoPDFSR6Jdisc = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.23455,0.811292,"user","userOverallSys") #inter=+/-0.180099 intraUP=0.15026 intraDN=-0.0563488
#7J
ttbarVTheoPDFWR7J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.15278,0.860264,"user","userOverallSys") #inter=+/-0.137646 intraUP=0.0663077 intraDN=-0.0240815
ttbarVTheoPDFTR7J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.13748,0.87223,"user","userOverallSys") #inter=+/-0.12626 intraUP=0.0544006 intraDN=-0.0195853
ttbarVTheoPDFSR7J = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.21741,0.814544,"user","userOverallSys") #inter=+/-0.180463 intraUP=0.121243 intraDN=-0.0427414
ttbarVTheoPDFVR7JhighMET = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.14937,0.870527,"user","userOverallSys") #inter=+/-0.127085 intraUP=0.0784819 intraDN=-0.0247523
ttbarVTheoPDFVR7JhighMT = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.16528,0.849666,"user","userOverallSys") #inter=+/-0.147867 intraUP=0.0738319 intraDN=-0.0271223
ttbarVTheoPDFSR7Jdisc = Systematic("h1L_ttbarVTheoPDF",configMgr.weights,1.27023,0.789599,"user","userOverallSys") #inter=+/-0.199285 intraUP=0.182506 intraDN=-0.0674827

def TheorUnc(generatorSyst):
    generatorSyst.append((("ttbarV","h1L_WR3JEl"), ttbarVTheoPDFWR3J))
    generatorSyst.append((("ttbarV","h1L_WR3JMu"), ttbarVTheoPDFWR3J))
    generatorSyst.append((("ttbarV","h1L_TR3JEl"), ttbarVTheoPDFTR3J))
    generatorSyst.append((("ttbarV","h1L_TR3JMu"), ttbarVTheoPDFTR3J))
    generatorSyst.append((("ttbarV","h1L_WR5JEl"), ttbarVTheoPDFWR5J))
    generatorSyst.append((("ttbarV","h1L_WR5JMu"), ttbarVTheoPDFWR5J))
    generatorSyst.append((("ttbarV","h1L_TR5JEl"), ttbarVTheoPDFTR5J))
    generatorSyst.append((("ttbarV","h1L_TR5JMu"), ttbarVTheoPDFTR5J))
    generatorSyst.append((("ttbarV","h1L_WR6JEl"), ttbarVTheoPDFWR6J))
    generatorSyst.append((("ttbarV","h1L_WR6JMu"), ttbarVTheoPDFWR6J))
    generatorSyst.append((("ttbarV","h1L_TR6JEl"), ttbarVTheoPDFTR6J))
    generatorSyst.append((("ttbarV","h1L_TR6JMu"), ttbarVTheoPDFTR6J))

    generatorSyst.append((("ttbarV","h1L_WR7JEl"), ttbarVTheoPDFWR7J))
    generatorSyst.append((("ttbarV","h1L_WR7JMu"), ttbarVTheoPDFWR7J))
    generatorSyst.append((("ttbarV","h1L_TR7JEl"), ttbarVTheoPDFTR7J))
    generatorSyst.append((("ttbarV","h1L_TR7JMu"), ttbarVTheoPDFTR7J))
    generatorSyst.append((("ttbarV","h1L_WR7JEM"), ttbarVTheoPDFWR7J))
    generatorSyst.append((("ttbarV","h1L_TR7JEM"), ttbarVTheoPDFTR7J))

    generatorSyst.append((("ttbarV","h1L_WR3JEM"), ttbarVTheoPDFWR3J))
    generatorSyst.append((("ttbarV","h1L_TR3JEM"), ttbarVTheoPDFTR3J))
    generatorSyst.append((("ttbarV","h1L_WR5JEM"), ttbarVTheoPDFWR5J))
    generatorSyst.append((("ttbarV","h1L_TR5JEM"), ttbarVTheoPDFTR5J))
    generatorSyst.append((("ttbarV","h1L_WR6JEM"), ttbarVTheoPDFWR6J))
    generatorSyst.append((("ttbarV","h1L_TR6JEM"), ttbarVTheoPDFTR6J))

    generatorSyst.append((("ttbarV","h1L_SR5JEl"), ttbarVTheoPDFSR5J))
    generatorSyst.append((("ttbarV","h1L_SR5JMu"), ttbarVTheoPDFSR5J))
    generatorSyst.append((("ttbarV","h1L_SR3JEl"), ttbarVTheoPDFSR3J))
    generatorSyst.append((("ttbarV","h1L_SR3JMu"), ttbarVTheoPDFSR3J))
    generatorSyst.append((("ttbarV","h1L_SR6JEl"), ttbarVTheoPDFSR6J))
    generatorSyst.append((("ttbarV","h1L_SR6JMu"), ttbarVTheoPDFSR6J))
    generatorSyst.append((("ttbarV","h1L_SR5JdiscoveryEl"), ttbarVTheoPDFSR5Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR5JdiscoveryMu"), ttbarVTheoPDFSR5Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR3JdiscoveryEl"), ttbarVTheoPDFSR3Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR3JdiscoveryMu"), ttbarVTheoPDFSR3Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR6JdiscoveryEl"), ttbarVTheoPDFSR6Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR6JdiscoveryMu"), ttbarVTheoPDFSR6Jdisc))

    generatorSyst.append((("ttbarV","h1L_SR7JEl"), ttbarVTheoPDFSR7J))
    generatorSyst.append((("ttbarV","h1L_SR7JMu"), ttbarVTheoPDFSR7J))
    generatorSyst.append((("ttbarV","h1L_SR7JEM"), ttbarVTheoPDFSR7J))

    generatorSyst.append((("ttbarV","h1L_SR5JEM"), ttbarVTheoPDFSR5J))
    generatorSyst.append((("ttbarV","h1L_SR3JEM"), ttbarVTheoPDFSR3J))
    generatorSyst.append((("ttbarV","h1L_SR6JEM"), ttbarVTheoPDFSR6J))
    generatorSyst.append((("ttbarV","h1L_SR5JdiscoveryEM"), ttbarVTheoPDFSR5Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR3JdiscoveryEM"), ttbarVTheoPDFSR3Jdisc))
    generatorSyst.append((("ttbarV","h1L_SR6JdiscoveryEM"), ttbarVTheoPDFSR6Jdisc))

    generatorSyst.append((("ttbarV","h1L_VR3JhighMETEl"), ttbarVTheoPDFVR3JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMETMu"), ttbarVTheoPDFVR3JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMTEl"), ttbarVTheoPDFVR3JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMTMu"), ttbarVTheoPDFVR3JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMETEl"), ttbarVTheoPDFVR5JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMETMu"), ttbarVTheoPDFVR5JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMTEl"), ttbarVTheoPDFVR5JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMTMu"), ttbarVTheoPDFVR5JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMETEl"), ttbarVTheoPDFVR6JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMETMu"), ttbarVTheoPDFVR6JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMTEl"), ttbarVTheoPDFVR6JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMTMu"), ttbarVTheoPDFVR6JhighMT))

    generatorSyst.append((("ttbarV","h1L_VR7JhighMETEl"), ttbarVTheoPDFVR7JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR7JhighMETMu"), ttbarVTheoPDFVR7JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR7JhighMTEl"), ttbarVTheoPDFVR7JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR7JhighMTMu"), ttbarVTheoPDFVR7JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR7JhighMETEM"), ttbarVTheoPDFVR7JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR7JhighMTEM"), ttbarVTheoPDFVR7JhighMT))

    generatorSyst.append((("ttbarV","h1L_VR3JhighMETEM"), ttbarVTheoPDFVR3JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR3JhighMTEM"), ttbarVTheoPDFVR3JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMETEM"), ttbarVTheoPDFVR5JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR5JhighMTEM"), ttbarVTheoPDFVR5JhighMT))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMETEM"), ttbarVTheoPDFVR6JhighMET))
    generatorSyst.append((("ttbarV","h1L_VR6JhighMTEM"), ttbarVTheoPDFVR6JhighMT))

    return generatorSyst
