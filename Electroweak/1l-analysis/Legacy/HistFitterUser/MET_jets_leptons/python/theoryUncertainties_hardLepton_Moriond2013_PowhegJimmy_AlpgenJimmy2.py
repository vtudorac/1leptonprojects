import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

AlpgenJimmyTTbarTheoSR3J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.10 ,0.90 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoWR3J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoTR3J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR3JhighMET = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR3JhighMT = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoSR5J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoWR5J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoTR5J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR5JhighMET = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR5JhighMT = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.37 ,0.63 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoSR6J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.30 ,0.70 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoWR6J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoTR6J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR6JhighMET = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.16 ,0.84 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR6JhighMT = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.22 ,0.78 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoSR7J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.47 ,0.53 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoWR7J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoTR7J = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR7JhighMET = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
AlpgenJimmyTTbarTheoVR7JhighMT = Systematic("h1L_AlpgenJimmyTTbar",configMgr.weights,1.45 ,0.55 ,"user","userOverallSys")

def TheorUnc(generatorSyst):
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JEl"), AlpgenJimmyTTbarTheoWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JMu"), AlpgenJimmyTTbarTheoWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JEl"), AlpgenJimmyTTbarTheoTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JMu"), AlpgenJimmyTTbarTheoTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JEl"), AlpgenJimmyTTbarTheoWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JMu"), AlpgenJimmyTTbarTheoWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JEl"), AlpgenJimmyTTbarTheoTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JMu"), AlpgenJimmyTTbarTheoTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JEl"), AlpgenJimmyTTbarTheoWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JMu"), AlpgenJimmyTTbarTheoWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JEl"), AlpgenJimmyTTbarTheoTR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JMu"), AlpgenJimmyTTbarTheoTR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR7JEl"), AlpgenJimmyTTbarTheoWR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR7JMu"), AlpgenJimmyTTbarTheoWR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR7JEl"), AlpgenJimmyTTbarTheoTR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR7JMu"), AlpgenJimmyTTbarTheoTR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR7JEM"), AlpgenJimmyTTbarTheoWR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR7JEM"), AlpgenJimmyTTbarTheoTR7J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JEM"), AlpgenJimmyTTbarTheoWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JEM"), AlpgenJimmyTTbarTheoTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JEM"), AlpgenJimmyTTbarTheoWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JEM"), AlpgenJimmyTTbarTheoTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JEM"), AlpgenJimmyTTbarTheoWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JEM"), AlpgenJimmyTTbarTheoTR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JEl"), AlpgenJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JMu"), AlpgenJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JEl"), AlpgenJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JMu"), AlpgenJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JEl"), AlpgenJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JMu"), AlpgenJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryEl"), AlpgenJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryMu"), AlpgenJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryEl"), AlpgenJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryMu"), AlpgenJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryEl"), AlpgenJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryMu"), AlpgenJimmyTTbarTheoSR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR7JEl"), AlpgenJimmyTTbarTheoSR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR7JMu"), AlpgenJimmyTTbarTheoSR7J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR7JEM"), AlpgenJimmyTTbarTheoSR7J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JEM"), AlpgenJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JEM"), AlpgenJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JEM"), AlpgenJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryEM"), AlpgenJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryEM"), AlpgenJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryEM"), AlpgenJimmyTTbarTheoSR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETEl"), AlpgenJimmyTTbarTheoVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETMu"), AlpgenJimmyTTbarTheoVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTEl"), AlpgenJimmyTTbarTheoVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTMu"), AlpgenJimmyTTbarTheoVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETEl"), AlpgenJimmyTTbarTheoVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETMu"), AlpgenJimmyTTbarTheoVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTEl"), AlpgenJimmyTTbarTheoVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTMu"), AlpgenJimmyTTbarTheoVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETEl"), AlpgenJimmyTTbarTheoVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETMu"), AlpgenJimmyTTbarTheoVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTEl"), AlpgenJimmyTTbarTheoVR6JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTMu"), AlpgenJimmyTTbarTheoVR6JhighMT))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR7JhighMETEl"), AlpgenJimmyTTbarTheoVR7JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR7JhighMETMu"), AlpgenJimmyTTbarTheoVR7JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR7JhighMTEl"), AlpgenJimmyTTbarTheoVR7JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR7JhighMTMu"), AlpgenJimmyTTbarTheoVR7JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR7JhighMETEM"), AlpgenJimmyTTbarTheoVR7JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR7JhighMTEM"), AlpgenJimmyTTbarTheoVR7JhighMT))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETEM"), AlpgenJimmyTTbarTheoVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTEM"), AlpgenJimmyTTbarTheoVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETEM"), AlpgenJimmyTTbarTheoVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTEM"), AlpgenJimmyTTbarTheoVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETEM"), AlpgenJimmyTTbarTheoVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTEM"), AlpgenJimmyTTbarTheoVR6JhighMT))

    return generatorSyst
