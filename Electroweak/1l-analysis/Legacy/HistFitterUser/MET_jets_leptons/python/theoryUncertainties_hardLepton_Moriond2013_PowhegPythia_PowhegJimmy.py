import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

PowhegJimmyTTbarTheoSR3J = Systematic("h1L_PowhegJimmyTTbar_3J",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
PowhegJimmyTTbarTheoWR3J = Systematic("h1L_PowhegJimmyTTbar_3J",configMgr.weights,1.01 ,0.86 ,"user","userOverallSys")
PowhegJimmyTTbarTheoTR3J = Systematic("h1L_PowhegJimmyTTbar_3J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR3JhighMET = Systematic("h1L_PowhegJimmyTTbar_3J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR3JhighMT = Systematic("h1L_PowhegJimmyTTbar_3J",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
PowhegJimmyTTbarTheoSR5J = Systematic("h1L_PowhegJimmyTTbar_5J",configMgr.weights,1.25 ,0.75 ,"user","userOverallSys")
PowhegJimmyTTbarTheoWR5J = Systematic("h1L_PowhegJimmyTTbar_5J",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
PowhegJimmyTTbarTheoTR5J = Systematic("h1L_PowhegJimmyTTbar_5J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR5JhighMET = Systematic("h1L_PowhegJimmyTTbar_5J",configMgr.weights,1.16 ,0.84 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR5JhighMT = Systematic("h1L_PowhegJimmyTTbar_5J",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
PowhegJimmyTTbarTheoSR6J = Systematic("h1L_PowhegJimmyTTbar_6J",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
PowhegJimmyTTbarTheoWR6J = Systematic("h1L_PowhegJimmyTTbar_6J",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
PowhegJimmyTTbarTheoTR6J = Systematic("h1L_PowhegJimmyTTbar_6J",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR6JhighMET = Systematic("h1L_PowhegJimmyTTbar_6J",configMgr.weights,1.02 ,0.98 ,"user","userOverallSys")
PowhegJimmyTTbarTheoVR6JhighMT = Systematic("h1L_PowhegJimmyTTbar_6J",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")



def TheorUnc(generatorSyst):
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JEl"), PowhegJimmyTTbarTheoWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JMu"), PowhegJimmyTTbarTheoWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JEl"), PowhegJimmyTTbarTheoTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JMu"), PowhegJimmyTTbarTheoTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JEl"), PowhegJimmyTTbarTheoWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JMu"), PowhegJimmyTTbarTheoWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JEl"), PowhegJimmyTTbarTheoTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JMu"), PowhegJimmyTTbarTheoTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JEl"), PowhegJimmyTTbarTheoWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JMu"), PowhegJimmyTTbarTheoWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JEl"), PowhegJimmyTTbarTheoTR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JMu"), PowhegJimmyTTbarTheoTR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR3JEM"), PowhegJimmyTTbarTheoWR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR3JEM"), PowhegJimmyTTbarTheoTR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR5JEM"), PowhegJimmyTTbarTheoWR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR5JEM"), PowhegJimmyTTbarTheoTR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_WR6JEM"), PowhegJimmyTTbarTheoWR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_TR6JEM"), PowhegJimmyTTbarTheoTR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JEl"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JMu"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JEl"), PowhegJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JMu"), PowhegJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JEl"), PowhegJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JMu"), PowhegJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryEl"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryMu"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryEl"), PowhegJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryMu"), PowhegJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryEl"), PowhegJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryMu"), PowhegJimmyTTbarTheoSR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JEM"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JEM"), PowhegJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JEM"), PowhegJimmyTTbarTheoSR6J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR5JdiscoveryEM"), PowhegJimmyTTbarTheoSR5J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR3JdiscoveryEM"), PowhegJimmyTTbarTheoSR3J))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_SR6JdiscoveryEM"), PowhegJimmyTTbarTheoSR6J))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETEl"), PowhegJimmyTTbarTheoVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETMu"), PowhegJimmyTTbarTheoVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTEl"), PowhegJimmyTTbarTheoVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTMu"), PowhegJimmyTTbarTheoVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETEl"), PowhegJimmyTTbarTheoVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETMu"), PowhegJimmyTTbarTheoVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTEl"), PowhegJimmyTTbarTheoVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTMu"), PowhegJimmyTTbarTheoVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETEl"), PowhegJimmyTTbarTheoVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETMu"), PowhegJimmyTTbarTheoVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTEl"), PowhegJimmyTTbarTheoVR6JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTMu"), PowhegJimmyTTbarTheoVR6JhighMT))

    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMETEM"), PowhegJimmyTTbarTheoVR3JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR3JhighMTEM"), PowhegJimmyTTbarTheoVR3JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMETEM"), PowhegJimmyTTbarTheoVR5JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR5JhighMTEM"), PowhegJimmyTTbarTheoVR5JhighMT))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMETEM"), PowhegJimmyTTbarTheoVR6JhighMET))
    generatorSyst.append((("PowhegPythiaTTbar","h1L_VR6JhighMTEM"), PowhegJimmyTTbarTheoVR6JhighMT))

    return generatorSyst
