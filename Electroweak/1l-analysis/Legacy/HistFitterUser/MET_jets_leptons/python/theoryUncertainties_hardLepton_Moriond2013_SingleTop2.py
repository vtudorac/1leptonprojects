import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr
'''
#envelope PS/INT/ISR/FSR
SingleTopTheoSR3J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.46 ,0.35 ,"user","userOverallSys")
SingleTopTheoWR3J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")
SingleTopTheoTR3J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
SingleTopTheoVR3JhighMET = Systematic("h1L_SingleTopTheo",configMgr.weights,2.35 ,0.01 ,"user","userOverallSys")
SingleTopTheoVR3JhighMT = Systematic("h1L_SingleTopTheo",configMgr.weights,1.19 ,0.81 ,"user","userOverallSys")
SingleTopTheoSR5J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.79 ,0.83 ,"user","userOverallSys")
SingleTopTheoWR5J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.35 ,0.85 ,"user","userOverallSys")
SingleTopTheoTR5J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
SingleTopTheoVR5JhighMET = Systematic("h1L_SingleTopTheo",configMgr.weights,1.84 ,0.15 ,"user","userOverallSys")
SingleTopTheoVR5JhighMT = Systematic("h1L_SingleTopTheo",configMgr.weights,1.25 ,0.75 ,"user","userOverallSys")
SingleTopTheoSR6J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.62 ,0.17 ,"user","userOverallSys")
SingleTopTheoWR6J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.43 ,0.95 ,"user","userOverallSys")
SingleTopTheoTR6J = Systematic("h1L_SingleTopTheo",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
SingleTopTheoVR6JhighMET = Systematic("h1L_SingleTopTheo",configMgr.weights,1.35 ,0.068 ,"user","userOverallSys")
SingleTopTheoVR6JhighMT = Systematic("h1L_SingleTopTheo",configMgr.weights,1.28 ,0.32 ,"user","userOverallSys")
'''

#envelope PS/INT/ISR/FSR 9/04/2014 TF norm to Normalization Region:1L, lepPt>25GeV, |eta|<2.5

SingleTopTheoSR3J = Systematic("SingleTopTheo",configMgr.weights,1.52 ,0.48 ,"user","userOverallSys")
SingleTopTheoWR3J = Systematic("SingleTopTheo",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")
SingleTopTheoTR3J = Systematic("SingleTopTheo",configMgr.weights,1.11 ,0.89 ,"user","userOverallSys")
SingleTopTheoVR3JhighMET = Systematic("SingleTopTheo",configMgr.weights,1.3 ,0.7 ,"user","userOverallSys")
SingleTopTheoVR3JhighMT = Systematic("SingleTopTheo",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
SingleTopTheoSR5J = Systematic("SingleTopTheo",configMgr.weights,1.21 ,0.79 ,"user","userOverallSys")
SingleTopTheoWR5J = Systematic("SingleTopTheo",configMgr.weights,1.28 ,0.72 ,"user","userOverallSys")
SingleTopTheoTR5J = Systematic("SingleTopTheo",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
SingleTopTheoVR5JhighMET = Systematic("SingleTopTheo",configMgr.weights,1.39 ,0.61 ,"user","userOverallSys")
SingleTopTheoVR5JhighMT = Systematic("SingleTopTheo",configMgr.weights,1.29 ,0.71 ,"user","userOverallSys")
SingleTopTheoSR6J = Systematic("SingleTopTheo",configMgr.weights,1.41 ,0.59 ,"user","userOverallSys")
SingleTopTheoWR6J = Systematic("SingleTopTheo",configMgr.weights,1.23 ,0.77 ,"user","userOverallSys")
SingleTopTheoTR6J = Systematic("SingleTopTheo",configMgr.weights,1.28 ,0.72 ,"user","userOverallSys")
SingleTopTheoVR6JhighMET = Systematic("SingleTopTheo",configMgr.weights,1.14 ,0.86 ,"user","userOverallSys")
SingleTopTheoVR6JhighMT = Systematic("SingleTopTheo",configMgr.weights,1.52 ,0.48 ,"user","userOverallSys")
SingleTopTheoSR7J = Systematic("SingleTopTheo",configMgr.weights,1.89 ,0.66 ,"user","userOverallSys")
SingleTopTheoWR7J = Systematic("SingleTopTheo",configMgr.weights,1.56 ,0.78 ,"user","userOverallSys")
SingleTopTheoTR7J = Systematic("SingleTopTheo",configMgr.weights,1.44 ,0.69 ,"user","userOverallSys")
SingleTopTheoVR7JhighMET = Systematic("SingleTopTheo",configMgr.weights,1.65 ,0.98 ,"user","userOverallSys")
SingleTopTheoVR7JhighMT = Systematic("SingleTopTheo",configMgr.weights,1.59 ,0.41 ,"user","userOverallSys")


def TheorUnc(generatorSyst):
    generatorSyst.append((("SingleTop","h1L_WR3JEl"), SingleTopTheoWR3J))
    generatorSyst.append((("SingleTop","h1L_WR3JMu"), SingleTopTheoWR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JEl"), SingleTopTheoTR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JMu"), SingleTopTheoTR3J))
    generatorSyst.append((("SingleTop","h1L_WR5JEl"), SingleTopTheoWR5J))
    generatorSyst.append((("SingleTop","h1L_WR5JMu"), SingleTopTheoWR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JEl"), SingleTopTheoTR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JMu"), SingleTopTheoTR5J))
    generatorSyst.append((("SingleTop","h1L_WR6JEl"), SingleTopTheoWR6J))
    generatorSyst.append((("SingleTop","h1L_WR6JMu"), SingleTopTheoWR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JEl"), SingleTopTheoTR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JMu"), SingleTopTheoTR6J))

    generatorSyst.append((("SingleTop","h1L_WR7JEl"), SingleTopTheoWR7J))
    generatorSyst.append((("SingleTop","h1L_WR7JMu"), SingleTopTheoWR7J))
    generatorSyst.append((("SingleTop","h1L_TR7JEl"), SingleTopTheoTR7J))
    generatorSyst.append((("SingleTop","h1L_TR7JMu"), SingleTopTheoTR7J))
    generatorSyst.append((("SingleTop","h1L_WR7JEM"), SingleTopTheoWR7J))
    generatorSyst.append((("SingleTop","h1L_TR7JEM"), SingleTopTheoTR7J))

    generatorSyst.append((("SingleTop","h1L_WR3JEM"), SingleTopTheoWR3J))
    generatorSyst.append((("SingleTop","h1L_TR3JEM"), SingleTopTheoTR3J))
    generatorSyst.append((("SingleTop","h1L_WR5JEM"), SingleTopTheoWR5J))
    generatorSyst.append((("SingleTop","h1L_TR5JEM"), SingleTopTheoTR5J))
    generatorSyst.append((("SingleTop","h1L_WR6JEM"), SingleTopTheoWR6J))
    generatorSyst.append((("SingleTop","h1L_TR6JEM"), SingleTopTheoTR6J))

    generatorSyst.append((("SingleTop","h1L_SR5JEl"), SingleTopTheoSR5J))
    generatorSyst.append((("SingleTop","h1L_SR5JMu"), SingleTopTheoSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JEl"), SingleTopTheoSR3J))
    generatorSyst.append((("SingleTop","h1L_SR3JMu"), SingleTopTheoSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JEl"), SingleTopTheoSR6J))
    generatorSyst.append((("SingleTop","h1L_SR6JMu"), SingleTopTheoSR6J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryEl"), SingleTopTheoSR5J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryMu"), SingleTopTheoSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryEl"), SingleTopTheoSR3J))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryMu"), SingleTopTheoSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryEl"), SingleTopTheoSR6J))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryMu"), SingleTopTheoSR6J))

    generatorSyst.append((("SingleTop","h1L_SR7JEl"), SingleTopTheoSR7J))
    generatorSyst.append((("SingleTop","h1L_SR7JMu"), SingleTopTheoSR7J))
    generatorSyst.append((("SingleTop","h1L_SR7JEM"), SingleTopTheoSR7J))

    generatorSyst.append((("SingleTop","h1L_SR5JEM"), SingleTopTheoSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JEM"), SingleTopTheoSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JEM"), SingleTopTheoSR6J))
    generatorSyst.append((("SingleTop","h1L_SR5JdiscoveryEM"), SingleTopTheoSR5J))
    generatorSyst.append((("SingleTop","h1L_SR3JdiscoveryEM"), SingleTopTheoSR3J))
    generatorSyst.append((("SingleTop","h1L_SR6JdiscoveryEM"), SingleTopTheoSR6J))

    generatorSyst.append((("SingleTop","h1L_VR3JhighMETEl"), SingleTopTheoVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMETMu"), SingleTopTheoVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTEl"), SingleTopTheoVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTMu"), SingleTopTheoVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETEl"), SingleTopTheoVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETMu"), SingleTopTheoVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTEl"), SingleTopTheoVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTMu"), SingleTopTheoVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETEl"), SingleTopTheoVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETMu"), SingleTopTheoVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTEl"), SingleTopTheoVR6JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTMu"), SingleTopTheoVR6JhighMT))

    generatorSyst.append((("SingleTop","h1L_VR7JhighMETEl"), SingleTopTheoVR7JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMETMu"), SingleTopTheoVR7JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMTEl"), SingleTopTheoVR7JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMTMu"), SingleTopTheoVR7JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMETEM"), SingleTopTheoVR7JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR7JhighMTEM"), SingleTopTheoVR7JhighMT))

    generatorSyst.append((("SingleTop","h1L_VR3JhighMETEM"), SingleTopTheoVR3JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR3JhighMTEM"), SingleTopTheoVR3JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMETEM"), SingleTopTheoVR5JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR5JhighMTEM"), SingleTopTheoVR5JhighMT))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMETEM"), SingleTopTheoVR6JhighMET))
    generatorSyst.append((("SingleTop","h1L_VR6JhighMTEM"), SingleTopTheoVR6JhighMT))

    return generatorSyst
