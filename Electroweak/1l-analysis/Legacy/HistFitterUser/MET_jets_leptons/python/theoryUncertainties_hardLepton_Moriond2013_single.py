import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

topTheoPSSR3J = Systematic("topTheoPS_3J",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")
topTheoPSWR3J = Systematic("topTheoPS_3J",configMgr.weights,0.77 ,1.23 ,"user","userOverallSys")
topTheoPSTR3J = Systematic("topTheoPS_3J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoPSVR3JhighMET = Systematic("topTheoPS_3J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoPSVR3JhighMT = Systematic("topTheoPS_3J",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
topTheoPSSR5J = Systematic("topTheoPS_5J",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
topTheoPSWR5J = Systematic("topTheoPS_5J",configMgr.weights,0.98 ,1.02 ,"user","userOverallSys")
topTheoPSTR5J = Systematic("topTheoPS_5J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoPSVR5JhighMET = Systematic("topTheoPS_5J",configMgr.weights,0.98 ,1.02 ,"user","userOverallSys")
topTheoPSVR5JhighMT = Systematic("topTheoPS_5J",configMgr.weights,0.94 ,1.06 ,"user","userOverallSys")
topTheoPSSR6J = Systematic("topTheoPS_6J",configMgr.weights,0.85 ,1.15 ,"user","userOverallSys")
topTheoPSWR6J = Systematic("topTheoPS_6J",configMgr.weights,0.97 ,1.03 ,"user","userOverallSys")
topTheoPSTR6J = Systematic("topTheoPS_6J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoPSVR6JhighMET = Systematic("topTheoPS_6J",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
topTheoPSVR6JhighMT = Systematic("topTheoPS_6J",configMgr.weights,0.84 ,1.16 ,"user","userOverallSys")
 
 
 
 
topTheoRenScSR3J = Systematic("topTheoRenSc_3J",configMgr.weights,0.9 ,0.92 ,"user","userOverallSys")
topTheoRenScWR3J = Systematic("topTheoRenSc_3J",configMgr.weights,1.36 ,1.32 ,"user","userOverallSys")
topTheoRenScTR3J = Systematic("topTheoRenSc_3J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoRenScVR3JhighMET = Systematic("topTheoRenSc_3J",configMgr.weights,1.07 ,1.07 ,"user","userOverallSys")
topTheoRenScVR3JhighMT = Systematic("topTheoRenSc_3J",configMgr.weights,0.95 ,1.06 ,"user","userOverallSys")
topTheoRenScSR5J = Systematic("topTheoRenSc_5J",configMgr.weights,1.12 ,1.1 ,"user","userOverallSys")
topTheoRenScWR5J = Systematic("topTheoRenSc_5J",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
topTheoRenScTR5J = Systematic("topTheoRenSc_5J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoRenScVR5JhighMET = Systematic("topTheoRenSc_5J",configMgr.weights,1.24 ,0.92 ,"user","userOverallSys")
topTheoRenScVR5JhighMT = Systematic("topTheoRenSc_5J",configMgr.weights,1.14 ,1.14 ,"user","userOverallSys")
topTheoRenScSR6J = Systematic("topTheoRenSc_6J",configMgr.weights,0.89 ,0.77 ,"user","userOverallSys")
topTheoRenScWR6J = Systematic("topTheoRenSc_6J",configMgr.weights,0.97 ,1.05 ,"user","userOverallSys")
topTheoRenScTR6J = Systematic("topTheoRenSc_6J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoRenScVR6JhighMET = Systematic("topTheoRenSc_6J",configMgr.weights,0.94 ,1.05 ,"user","userOverallSys")
topTheoRenScVR6JhighMT = Systematic("topTheoRenSc_6J",configMgr.weights,0.92 ,1.09 ,"user","userOverallSys")
 
 
 
topTheoFacScSR3J = Systematic("topTheoFacSc_3J",configMgr.weights,0.89 ,1.06 ,"user","userOverallSys")
topTheoFacScWR3J = Systematic("topTheoFacSc_3J",configMgr.weights,1.2 ,0.87 ,"user","userOverallSys")
topTheoFacScTR3J = Systematic("topTheoFacSc_3J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoFacScVR3JhighMET = Systematic("topTheoFacSc_3J",configMgr.weights,1.07 ,1.05 ,"user","userOverallSys")
topTheoFacScVR3JhighMT = Systematic("topTheoFacSc_3J",configMgr.weights,1.05 ,1.09 ,"user","userOverallSys")
topTheoFacScSR5J = Systematic("topTheoFacSc_5J",configMgr.weights,0.82 ,1.13 ,"user","userOverallSys")
topTheoFacScWR5J = Systematic("topTheoFacSc_5J",configMgr.weights,1.06 ,1.06 ,"user","userOverallSys")
topTheoFacScTR5J = Systematic("topTheoFacSc_5J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoFacScVR5JhighMET = Systematic("topTheoFacSc_5J",configMgr.weights,1.09 ,1.08 ,"user","userOverallSys")
topTheoFacScVR5JhighMT = Systematic("topTheoFacSc_5J",configMgr.weights,1.12 ,1.2 ,"user","userOverallSys")
topTheoFacScSR6J = Systematic("topTheoFacSc_6J",configMgr.weights,0.88 ,0.87 ,"user","userOverallSys")
topTheoFacScWR6J = Systematic("topTheoFacSc_6J",configMgr.weights,1.06 ,1.03 ,"user","userOverallSys")
topTheoFacScTR6J = Systematic("topTheoFacSc_6J",configMgr.weights,1. ,1. ,"user","userOverallSys")
topTheoFacScVR6JhighMET = Systematic("topTheoFacSc_6J",configMgr.weights,0.95 ,0.92 ,"user","userOverallSys")
topTheoFacScVR6JhighMT = Systematic("topTheoFacSc_6J",configMgr.weights,1.08 ,0.83 ,"user","userOverallSys")
 
 
 
WTheoPtMinSR3J = Systematic("WTheoPtMin_3J",configMgr.weights,0.74 ,1.26 ,"user","userOverallSys")
WTheoPtMinWR3J = Systematic("WTheoPtMin_3J",configMgr.weights,1. ,1. ,"user","userOverallSys")
WTheoPtMinTR3J = Systematic("WTheoPtMin_3J",configMgr.weights,0.67 ,1.33 ,"user","userOverallSys")
WTheoPtMinVR3JhighMET = Systematic("WTheoPtMin_3J",configMgr.weights,0.81 ,1.19 ,"user","userOverallSys")
WTheoPtMinVR3JhighMT = Systematic("WTheoPtMin_3J",configMgr.weights,0.75 ,1.25 ,"user","userOverallSys")
WTheoPtMinSR5J = Systematic("WTheoPtMin_5J",configMgr.weights,0.62 ,1.38 ,"user","userOverallSys")
WTheoPtMinWR5J = Systematic("WTheoPtMin_5J",configMgr.weights,1. ,1. ,"user","userOverallSys")
WTheoPtMinTR5J = Systematic("WTheoPtMin_5J",configMgr.weights,0.54 ,1.46 ,"user","userOverallSys")
WTheoPtMinVR5JhighMET = Systematic("WTheoPtMin_5J",configMgr.weights,0.82 ,1.18 ,"user","userOverallSys")
WTheoPtMinVR5JhighMT = Systematic("WTheoPtMin_5J",configMgr.weights,0.57 ,1.43 ,"user","userOverallSys")
WTheoPtMinSR6J = Systematic("WTheoPtMin_6J",configMgr.weights,0.41 ,1.59 ,"user","userOverallSys")
WTheoPtMinWR6J = Systematic("WTheoPtMin_6J",configMgr.weights,1. ,1. ,"user","userOverallSys")
WTheoPtMinTR6J = Systematic("WTheoPtMin_6J",configMgr.weights,0.29 ,1.71 ,"user","userOverallSys")
WTheoPtMinVR6JhighMET = Systematic("WTheoPtMin_6J",configMgr.weights,0.8 ,1.2 ,"user","userOverallSys")
WTheoPtMinVR6JhighMT = Systematic("WTheoPtMin_6J",configMgr.weights,0.42 ,1.58 ,"user","userOverallSys")
 
 
 
WTheoNpartSR3J = Systematic("WTheoNpart_3J",configMgr.weights,0.9 ,1.09 ,"user","userOverallSys")
WTheoNpartWR3J = Systematic("WTheoNpart_3J",configMgr.weights,1 ,1 ,"user","userOverallSys")
WTheoNpartTR3J = Systematic("WTheoNpart_3J",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")
WTheoNpartVR3JhighMET = Systematic("WTheoNpart_3J",configMgr.weights,1.06 ,0.94 ,"user","userOverallSys")
WTheoNpartVR3JhighMT = Systematic("WTheoNpart_3J",configMgr.weights,0.9 ,1.09 ,"user","userOverallSys")
WTheoNpartSR5J = Systematic("WTheoNpart_5J",configMgr.weights,0.74 ,1.24 ,"user","userOverallSys")
WTheoNpartWR5J = Systematic("WTheoNpart_5J",configMgr.weights,1 ,1 ,"user","userOverallSys")
WTheoNpartTR5J = Systematic("WTheoNpart_5J",configMgr.weights,0.95 ,1.06 ,"user","userOverallSys")
WTheoNpartVR5JhighMET = Systematic("WTheoNpart_5J",configMgr.weights,1.06 ,0.93 ,"user","userOverallSys")
WTheoNpartVR5JhighMT = Systematic("WTheoNpart_5J",configMgr.weights,0.77 ,1.23 ,"user","userOverallSys")
WTheoNpartSR6J = Systematic("WTheoNpart_6J",configMgr.weights,0.59 ,1.38 ,"user","userOverallSys")
WTheoNpartWR6J = Systematic("WTheoNpart_6J",configMgr.weights,1 ,1 ,"user","userOverallSys")
WTheoNpartTR6J = Systematic("WTheoNpart_6J",configMgr.weights,0.96 ,1.05 ,"user","userOverallSys")
WTheoNpartVR6JhighMET = Systematic("WTheoNpart_6J",configMgr.weights,1.1 ,0.88 ,"user","userOverallSys")
WTheoNpartVR6JhighMT = Systematic("WTheoNpart_6J",configMgr.weights,1.22 ,0.74 ,"user","userOverallSys")
