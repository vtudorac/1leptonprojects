import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr




topTheoPSSRel1L3j = Systematic("topTheoPS",configMgr.weights,0.9 ,1.1 ,"user","userOverallSys")
topTheoPSCRelT3j = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSCRelW3j = Systematic("topTheoPS",configMgr.weights,0.88 ,1.12 ,"user","userOverallSys")
topTheoPSSRel1L5j = Systematic("topTheoPS",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
topTheoPSCRelT5j = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSCRelW5j = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSSRel1L2Ba = Systematic("topTheoPS",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
topTheoPSCRelWbb1L2Ba = Systematic("topTheoPS",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")
topTheoPSCRelW1L2Ba = Systematic("topTheoPS",configMgr.weights,1.02 ,0.98 ,"user","userOverallSys")
topTheoPSCRelT1L2Ba = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSSRel1L2Bc = Systematic("topTheoPS",configMgr.weights,0.94 ,1.06 ,"user","userOverallSys")
topTheoPSCRelWbb1L2Bc = Systematic("topTheoPS",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")
topTheoPSCRelW1L2Bc = Systematic("topTheoPS",configMgr.weights,1.13 ,0.87 ,"user","userOverallSys")
topTheoPSCRelT1L2Bc = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSSRel1L1Ba = Systematic("topTheoPS",configMgr.weights,1.02 ,0.98 ,"user","userOverallSys")
topTheoPSCRelWbb1L1Ba = Systematic("topTheoPS",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
topTheoPSCRelW1L1Ba = Systematic("topTheoPS",configMgr.weights,0.96 ,1.04 ,"user","userOverallSys")
topTheoPSCRelT1L1Ba = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSSRel1L1Bc = Systematic("topTheoPS",configMgr.weights,0.97 ,1.03 ,"user","userOverallSys")
topTheoPSCRelWbb1L1Bc = Systematic("topTheoPS",configMgr.weights,1.01 ,0.99 ,"user","userOverallSys")
topTheoPSCRelW1L1Bc = Systematic("topTheoPS",configMgr.weights,1.21 ,0.79 ,"user","userOverallSys")
topTheoPSCRelT1L1Bc = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSSR2L = Systematic("topTheoPS",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")
topTheoPSCRT2L = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSVRel3j1 = Systematic("topTheoPS",configMgr.weights,0.99 ,1.01 ,"user","userOverallSys")
topTheoPSVRelT3j1 = Systematic("topTheoPS",configMgr.weights,0.99 ,1.01 ,"user","userOverallSys")
topTheoPSVRelW3j1 = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSVRel3j2 = Systematic("topTheoPS",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
topTheoPSVRelT3j2 = Systematic("topTheoPS",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
topTheoPSVRelW3j2 = Systematic("topTheoPS",configMgr.weights,1.12 ,0.88 ,"user","userOverallSys")
topTheoPSVRel3j3 = Systematic("topTheoPS",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
topTheoPSVRelT3j3 = Systematic("topTheoPS",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
topTheoPSVRelW3j3 = Systematic("topTheoPS",configMgr.weights,1.26 ,0.74 ,"user","userOverallSys")
topTheoPSVRel5j1 = Systematic("topTheoPS",configMgr.weights,0.99 ,1.01 ,"user","userOverallSys")
topTheoPSVRelT5j1 = Systematic("topTheoPS",configMgr.weights,0.99 ,1.01 ,"user","userOverallSys")
topTheoPSVRelW5j1 = Systematic("topTheoPS",configMgr.weights,0.91 ,1.09 ,"user","userOverallSys")
topTheoPSVRel5j2 = Systematic("topTheoPS",configMgr.weights,0.98 ,1.02 ,"user","userOverallSys")
topTheoPSVRelT5j2 = Systematic("topTheoPS",configMgr.weights,0.98 ,1.02 ,"user","userOverallSys")
topTheoPSVRelW5j2 = Systematic("topTheoPS",configMgr.weights,0.93 ,1.07 ,"user","userOverallSys")
topTheoPSVRel5j3 = Systematic("topTheoPS",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
topTheoPSVRelT5j3 = Systematic("topTheoPS",configMgr.weights,1.15 ,0.85 ,"user","userOverallSys")
topTheoPSVRelW5j3 = Systematic("topTheoPS",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")
topTheoPSVRel1L2Ba1 = Systematic("topTheoPS",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
topTheoPSVRel1L2Ba2 = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSVRel1L2Bc1 = Systematic("topTheoPS",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
topTheoPSVRel1L2Bc2 = Systematic("topTheoPS",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
topTheoPSVRel1L1Ba1 = Systematic("topTheoPS",configMgr.weights,1.03 ,0.97 ,"user","userOverallSys")
topTheoPSVRel1L1Ba2 = Systematic("topTheoPS",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
topTheoPSVRel1L1Bc1 = Systematic("topTheoPS",configMgr.weights,0.98 ,1.02 ,"user","userOverallSys")
topTheoPSVRel1L1Bc2 = Systematic("topTheoPS",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoPSVRel2L1 = Systematic("topTheoPS",configMgr.weights,1.05 ,0.95 ,"user","userOverallSys")
topTheoPSVRel2L2 = Systematic("topTheoPS",configMgr.weights,1.08 ,0.92 ,"user","userOverallSys")
topTheoPSVRel2L3 = Systematic("topTheoPS",configMgr.weights,1.07 ,0.93 ,"user","userOverallSys")
 
 

 
 
topTheoRenScSRel1L3j = Systematic("topTheoRenSc",configMgr.weights,0.81 ,0.86 ,"user","userOverallSys")
topTheoRenScCRelT3j = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScCRelW3j = Systematic("topTheoRenSc",configMgr.weights,0.87 ,0.68 ,"user","userOverallSys")
topTheoRenScSRel1L5j = Systematic("topTheoRenSc",configMgr.weights,0.88 ,0.87 ,"user","userOverallSys")
topTheoRenScCRelT5j = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScCRelW5j = Systematic("topTheoRenSc",configMgr.weights,1.83 ,1.3 ,"user","userOverallSys")
topTheoRenScSRel1L2Ba = Systematic("topTheoRenSc",configMgr.weights,0.94 ,0.95 ,"user","userOverallSys")
topTheoRenScCRelWbb1L2Ba = Systematic("topTheoRenSc",configMgr.weights,0.92 ,0.94 ,"user","userOverallSys")
topTheoRenScCRelW1L2Ba = Systematic("topTheoRenSc",configMgr.weights,0.94 ,0.94 ,"user","userOverallSys")
topTheoRenScCRelT1L2Ba = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScSRel1L2Bc = Systematic("topTheoRenSc",configMgr.weights,1.17 ,0.88 ,"user","userOverallSys")
topTheoRenScCRelWbb1L2Bc = Systematic("topTheoRenSc",configMgr.weights,0.97 ,1.04 ,"user","userOverallSys")
topTheoRenScCRelW1L2Bc = Systematic("topTheoRenSc",configMgr.weights,1.34 ,1.17 ,"user","userOverallSys")
topTheoRenScCRelT1L2Bc = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScSRel1L1Ba = Systematic("topTheoRenSc",configMgr.weights,0.9 ,1.12 ,"user","userOverallSys")
topTheoRenScCRelWbb1L1Ba = Systematic("topTheoRenSc",configMgr.weights,0.93 ,0.92 ,"user","userOverallSys")
topTheoRenScCRelW1L1Ba = Systematic("topTheoRenSc",configMgr.weights,1.35 ,1.3 ,"user","userOverallSys")
topTheoRenScCRelT1L1Ba = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScSRel1L1Bc = Systematic("topTheoRenSc",configMgr.weights,1.1 ,1.11 ,"user","userOverallSys")
topTheoRenScCRelWbb1L1Bc = Systematic("topTheoRenSc",configMgr.weights,1.06 ,0.91 ,"user","userOverallSys")
topTheoRenScCRelW1L1Bc = Systematic("topTheoRenSc",configMgr.weights,0.72 ,1.33 ,"user","userOverallSys")
topTheoRenScCRelT1L1Bc = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScSR2L = Systematic("topTheoRenSc",configMgr.weights,0.78 ,0.74 ,"user","userOverallSys")
topTheoRenScCRT2L = Systematic("topTheoRenSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoRenScVRel3j1 = Systematic("topTheoRenSc",configMgr.weights,0.96 ,0.96 ,"user","userOverallSys")
topTheoRenScVRelT3j1 = Systematic("topTheoRenSc",configMgr.weights,0.96 ,0.96 ,"user","userOverallSys")
topTheoRenScVRelW3j1 = Systematic("topTheoRenSc",configMgr.weights,0.73 ,0.66 ,"user","userOverallSys")
topTheoRenScVRel3j2 = Systematic("topTheoRenSc",configMgr.weights,0.95 ,1.04 ,"user","userOverallSys")
topTheoRenScVRelT3j2 = Systematic("topTheoRenSc",configMgr.weights,0.95 ,1.05 ,"user","userOverallSys")
topTheoRenScVRelW3j2 = Systematic("topTheoRenSc",configMgr.weights,0.75 ,0.62 ,"user","userOverallSys")
topTheoRenScVRel3j3 = Systematic("topTheoRenSc",configMgr.weights,0.85 ,0.81 ,"user","userOverallSys")
topTheoRenScVRelT3j3 = Systematic("topTheoRenSc",configMgr.weights,0.84 ,0.78 ,"user","userOverallSys")
topTheoRenScVRelW3j3 = Systematic("topTheoRenSc",configMgr.weights,1.52 ,2.16 ,"user","userOverallSys")
topTheoRenScVRel5j1 = Systematic("topTheoRenSc",configMgr.weights,1.06 ,1.09 ,"user","userOverallSys")
topTheoRenScVRelT5j1 = Systematic("topTheoRenSc",configMgr.weights,1.05 ,1.08 ,"user","userOverallSys")
topTheoRenScVRelW5j1 = Systematic("topTheoRenSc",configMgr.weights,3.18 ,1.78 ,"user","userOverallSys")
topTheoRenScVRel5j2 = Systematic("topTheoRenSc",configMgr.weights,0.93 ,0.95 ,"user","userOverallSys")
topTheoRenScVRelT5j2 = Systematic("topTheoRenSc",configMgr.weights,0.93 ,0.95 ,"user","userOverallSys")
topTheoRenScVRelW5j2 = Systematic("topTheoRenSc",configMgr.weights,0.66 ,1.46 ,"user","userOverallSys")
topTheoRenScVRel5j3 = Systematic("topTheoRenSc",configMgr.weights,1.3 ,1.18 ,"user","userOverallSys")
topTheoRenScVRelT5j3 = Systematic("topTheoRenSc",configMgr.weights,1.27 ,1.17 ,"user","userOverallSys")
topTheoRenScVRelW5j3 = Systematic("topTheoRenSc",configMgr.weights,3.75 ,2.06 ,"user","userOverallSys")
topTheoRenScVRel1L2Ba1 = Systematic("topTheoRenSc",configMgr.weights,0.92 ,0.94 ,"user","userOverallSys")
topTheoRenScVRel1L2Ba2 = Systematic("topTheoRenSc",configMgr.weights,0.94 ,0.94 ,"user","userOverallSys")
topTheoRenScVRel1L2Bc1 = Systematic("topTheoRenSc",configMgr.weights,1.05 ,1.08 ,"user","userOverallSys")
topTheoRenScVRel1L2Bc2 = Systematic("topTheoRenSc",configMgr.weights,1.14 ,1.15 ,"user","userOverallSys")
topTheoRenScVRel1L1Ba1 = Systematic("topTheoRenSc",configMgr.weights,1.11 ,1.15 ,"user","userOverallSys")
topTheoRenScVRel1L1Ba2 = Systematic("topTheoRenSc",configMgr.weights,1.11 ,1.2 ,"user","userOverallSys")
topTheoRenScVRel1L1Bc1 = Systematic("topTheoRenSc",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
topTheoRenScVRel1L1Bc2 = Systematic("topTheoRenSc",configMgr.weights,1.11 ,0.91 ,"user","userOverallSys")
topTheoRenScVRel2L1 = Systematic("topTheoRenSc",configMgr.weights,0.88 ,0.89 ,"user","userOverallSys")
topTheoRenScVRel2L2 = Systematic("topTheoRenSc",configMgr.weights,0.86 ,0.87 ,"user","userOverallSys")
topTheoRenScVRel2L3 = Systematic("topTheoRenSc",configMgr.weights,0.93 ,0.91 ,"user","userOverallSys")
 
 
 
topTheoFacScSRel1L3j = Systematic("topTheoFacSc",configMgr.weights,0.87 ,0.84 ,"user","userOverallSys")
topTheoFacScCRelT3j = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScCRelW3j = Systematic("topTheoFacSc",configMgr.weights,0.88 ,0.83 ,"user","userOverallSys")
topTheoFacScSRel1L5j = Systematic("topTheoFacSc",configMgr.weights,1.09 ,1.09 ,"user","userOverallSys")
topTheoFacScCRelT5j = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScCRelW5j = Systematic("topTheoFacSc",configMgr.weights,0.77 ,1.23 ,"user","userOverallSys")
topTheoFacScSRel1L2Ba = Systematic("topTheoFacSc",configMgr.weights,0.94 ,1.05 ,"user","userOverallSys")
topTheoFacScCRelWbb1L2Ba = Systematic("topTheoFacSc",configMgr.weights,1.04 ,0.96 ,"user","userOverallSys")
topTheoFacScCRelW1L2Ba = Systematic("topTheoFacSc",configMgr.weights,1.1 ,1.11 ,"user","userOverallSys")
topTheoFacScCRelT1L2Ba = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScSRel1L2Bc = Systematic("topTheoFacSc",configMgr.weights,0.77 ,0.89 ,"user","userOverallSys")
topTheoFacScCRelWbb1L2Bc = Systematic("topTheoFacSc",configMgr.weights,0.95 ,1.04 ,"user","userOverallSys")
topTheoFacScCRelW1L2Bc = Systematic("topTheoFacSc",configMgr.weights,1.26 ,1.32 ,"user","userOverallSys")
topTheoFacScCRelT1L2Bc = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScSRel1L1Ba = Systematic("topTheoFacSc",configMgr.weights,1.18 ,0.9 ,"user","userOverallSys")
topTheoFacScCRelWbb1L1Ba = Systematic("topTheoFacSc",configMgr.weights,1.14 ,1.14 ,"user","userOverallSys")
topTheoFacScCRelW1L1Ba = Systematic("topTheoFacSc",configMgr.weights,1.63 ,1.34 ,"user","userOverallSys")
topTheoFacScCRelT1L1Ba = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScSRel1L1Bc = Systematic("topTheoFacSc",configMgr.weights,1.2 ,0.9 ,"user","userOverallSys")
topTheoFacScCRelWbb1L1Bc = Systematic("topTheoFacSc",configMgr.weights,1.07 ,1.07 ,"user","userOverallSys")
topTheoFacScCRelW1L1Bc = Systematic("topTheoFacSc",configMgr.weights,0.63 ,1.24 ,"user","userOverallSys")
topTheoFacScCRelT1L1Bc = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScSR2L = Systematic("topTheoFacSc",configMgr.weights,0.87 ,0.88 ,"user","userOverallSys")
topTheoFacScCRT2L = Systematic("topTheoFacSc",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
topTheoFacScVRel3j1 = Systematic("topTheoFacSc",configMgr.weights,0.96 ,1.05 ,"user","userOverallSys")
topTheoFacScVRelT3j1 = Systematic("topTheoFacSc",configMgr.weights,1.04 ,1.05 ,"user","userOverallSys")
topTheoFacScVRelW3j1 = Systematic("topTheoFacSc",configMgr.weights,0.71 ,0.75 ,"user","userOverallSys")
topTheoFacScVRel3j2 = Systematic("topTheoFacSc",configMgr.weights,0.9 ,1.05 ,"user","userOverallSys")
topTheoFacScVRelT3j2 = Systematic("topTheoFacSc",configMgr.weights,0.91 ,1.05 ,"user","userOverallSys")
topTheoFacScVRelW3j2 = Systematic("topTheoFacSc",configMgr.weights,0.49 ,0.83 ,"user","userOverallSys")
topTheoFacScVRel3j3 = Systematic("topTheoFacSc",configMgr.weights,0.78 ,0.75 ,"user","userOverallSys")
topTheoFacScVRelT3j3 = Systematic("topTheoFacSc",configMgr.weights,0.76 ,0.73 ,"user","userOverallSys")
topTheoFacScVRelW3j3 = Systematic("topTheoFacSc",configMgr.weights,1.65 ,1.88 ,"user","userOverallSys")
topTheoFacScVRel5j1 = Systematic("topTheoFacSc",configMgr.weights,0.95 ,1.1 ,"user","userOverallSys")
topTheoFacScVRelT5j1 = Systematic("topTheoFacSc",configMgr.weights,0.95 ,1.09 ,"user","userOverallSys")
topTheoFacScVRelW5j1 = Systematic("topTheoFacSc",configMgr.weights,1.6 ,1.71 ,"user","userOverallSys")
topTheoFacScVRel5j2 = Systematic("topTheoFacSc",configMgr.weights,0.94 ,0.95 ,"user","userOverallSys")
topTheoFacScVRelT5j2 = Systematic("topTheoFacSc",configMgr.weights,0.94 ,0.95 ,"user","userOverallSys")
topTheoFacScVRelW5j2 = Systematic("topTheoFacSc",configMgr.weights,0.6 ,0.33 ,"user","userOverallSys")
topTheoFacScVRel5j3 = Systematic("topTheoFacSc",configMgr.weights,1.11 ,0.89 ,"user","userOverallSys")
topTheoFacScVRelT5j3 = Systematic("topTheoFacSc",configMgr.weights,1.1 ,0.9 ,"user","userOverallSys")
topTheoFacScVRelW5j3 = Systematic("topTheoFacSc",configMgr.weights,2.55 ,0 ,"user","userOverallSys")
topTheoFacScVRel1L2Ba1 = Systematic("topTheoFacSc",configMgr.weights,1.05 ,0.94 ,"user","userOverallSys")
topTheoFacScVRel1L2Ba2 = Systematic("topTheoFacSc",configMgr.weights,1.05 ,0.94 ,"user","userOverallSys")
topTheoFacScVRel1L2Bc1 = Systematic("topTheoFacSc",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
topTheoFacScVRel1L2Bc2 = Systematic("topTheoFacSc",configMgr.weights,1.14 ,1.16 ,"user","userOverallSys")
topTheoFacScVRel1L1Ba1 = Systematic("topTheoFacSc",configMgr.weights,1.31 ,1.23 ,"user","userOverallSys")
topTheoFacScVRel1L1Ba2 = Systematic("topTheoFacSc",configMgr.weights,1.48 ,1.16 ,"user","userOverallSys")
topTheoFacScVRel1L1Bc1 = Systematic("topTheoFacSc",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
topTheoFacScVRel1L1Bc2 = Systematic("topTheoFacSc",configMgr.weights,1.1 ,1.11 ,"user","userOverallSys")
topTheoFacScVRel2L1 = Systematic("topTheoFacSc",configMgr.weights,0.91 ,1.09 ,"user","userOverallSys")
topTheoFacScVRel2L2 = Systematic("topTheoFacSc",configMgr.weights,0.93 ,0.94 ,"user","userOverallSys")
topTheoFacScVRel2L3 = Systematic("topTheoFacSc",configMgr.weights,0.94 ,1.09 ,"user","userOverallSys")
 
 
''' 
WTheoPtMinSRel1L3j = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelT3j = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelW3j = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinSRel1L5j = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelT5j = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelW5j = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinSRel1L2Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelWbb1L2Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelW1L2Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelT1L2Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinSRel1L2Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelWbb1L2Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelW1L2Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelT1L2Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinSRel1L1Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelWbb1L1Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelW1L1Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelT1L1Ba = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinSRel1L1Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelWbb1L1Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelW1L1Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRelT1L1Bc = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinSR2L = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinCRT2L = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel3j1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelT3j1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelW3j1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel3j2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelT3j2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelW3j2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel3j3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelT3j3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelW3j3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel5j1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelT5j1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelW5j1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel5j2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelT5j2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelW5j2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel5j3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelT5j3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRelW5j3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L2Ba1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L2Ba2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L2Bc1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L2Bc2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L1Ba1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L1Ba2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L1Bc1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel1L1Bc2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel2L1 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel2L2 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoPtMinVRel2L3 = Systematic("WTheoPtMin",configMgr.weights,nan ,nan ,"user","userOverallSys")
''' 
 
 
WTheoNpartSRel1L3j = Systematic("WTheoNpart",configMgr.weights,0.77 ,1.23 ,"user","userOverallSys")
WTheoNpartCRelT3j = Systematic("WTheoNpart",configMgr.weights,0.97 ,1.03 ,"user","userOverallSys")
WTheoNpartCRelW3j = Systematic("WTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
WTheoNpartSRel1L5j = Systematic("WTheoNpart",configMgr.weights,0.71 ,1.29 ,"user","userOverallSys")
WTheoNpartCRelT5j = Systematic("WTheoNpart",configMgr.weights,0.85 ,1.15 ,"user","userOverallSys")
WTheoNpartCRelW5j = Systematic("WTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
WTheoNpartSRel1L2Ba = Systematic("WTheoNpart",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
WTheoNpartCRelWbb1L2Ba = Systematic("WTheoNpart",configMgr.weights,0.96 ,1.04 ,"user","userOverallSys")
WTheoNpartCRelW1L2Ba = Systematic("WTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
WTheoNpartCRelT1L2Ba = Systematic("WTheoNpart",configMgr.weights,0.9 ,1.1 ,"user","userOverallSys")
WTheoNpartSRel1L2Bc = Systematic("WTheoNpart",configMgr.weights,0.88 ,1.12 ,"user","userOverallSys")
WTheoNpartCRelWbb1L2Bc = Systematic("WTheoNpart",configMgr.weights,0.94 ,1.06 ,"user","userOverallSys")
WTheoNpartCRelW1L2Bc = Systematic("WTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
WTheoNpartCRelT1L2Bc = Systematic("WTheoNpart",configMgr.weights,0.91 ,1.09 ,"user","userOverallSys")
WTheoNpartSRel1L1Ba = Systematic("WTheoNpart",configMgr.weights,0.81 ,1.19 ,"user","userOverallSys")
WTheoNpartCRelWbb1L1Ba = Systematic("WTheoNpart",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
WTheoNpartCRelW1L1Ba = Systematic("WTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
WTheoNpartCRelT1L1Ba = Systematic("WTheoNpart",configMgr.weights,0.81 ,1.19 ,"user","userOverallSys")
WTheoNpartSRel1L1Bc = Systematic("WTheoNpart",configMgr.weights,0.8 ,1.2 ,"user","userOverallSys")
WTheoNpartCRelWbb1L1Bc = Systematic("WTheoNpart",configMgr.weights,0.96 ,1.04 ,"user","userOverallSys")
WTheoNpartCRelW1L1Bc = Systematic("WTheoNpart",configMgr.weights,1.0 ,1.0 ,"user","userOverallSys")
WTheoNpartCRelT1L1Bc = Systematic("WTheoNpart",configMgr.weights,0.85 ,1.15 ,"user","userOverallSys")
#WTheoNpartSR2L = Systematic("WTheoNpart",configMgr.weights,nan ,nan ,"user","userOverallSys")
#WTheoNpartCRT2L = Systematic("WTheoNpart",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoNpartVRel3j1 = Systematic("WTheoNpart",configMgr.weights,0.93 ,1.07 ,"user","userOverallSys")
WTheoNpartVRelT3j1 = Systematic("WTheoNpart",configMgr.weights,0.91 ,1.09 ,"user","userOverallSys")
WTheoNpartVRelW3j1 = Systematic("WTheoNpart",configMgr.weights,0.94 ,1.06 ,"user","userOverallSys")
WTheoNpartVRel3j2 = Systematic("WTheoNpart",configMgr.weights,0.96 ,1.04 ,"user","userOverallSys")
WTheoNpartVRelT3j2 = Systematic("WTheoNpart",configMgr.weights,0.95 ,1.05 ,"user","userOverallSys")
WTheoNpartVRelW3j2 = Systematic("WTheoNpart",configMgr.weights,0.95 ,1.05 ,"user","userOverallSys")
WTheoNpartVRel3j3 = Systematic("WTheoNpart",configMgr.weights,0.95 ,1.05 ,"user","userOverallSys")
WTheoNpartVRelT3j3 = Systematic("WTheoNpart",configMgr.weights,0.93 ,1.07 ,"user","userOverallSys")
WTheoNpartVRelW3j3 = Systematic("WTheoNpart",configMgr.weights,0.89 ,1.11 ,"user","userOverallSys")
WTheoNpartVRel5j1 = Systematic("WTheoNpart",configMgr.weights,0.88 ,1.12 ,"user","userOverallSys")
WTheoNpartVRelT5j1 = Systematic("WTheoNpart",configMgr.weights,0.88 ,1.12 ,"user","userOverallSys")
WTheoNpartVRelW5j1 = Systematic("WTheoNpart",configMgr.weights,0.81 ,1.19 ,"user","userOverallSys")
WTheoNpartVRel5j2 = Systematic("WTheoNpart",configMgr.weights,0.84 ,1.16 ,"user","userOverallSys")
WTheoNpartVRelT5j2 = Systematic("WTheoNpart",configMgr.weights,0.88 ,1.12 ,"user","userOverallSys")
WTheoNpartVRelW5j2 = Systematic("WTheoNpart",configMgr.weights,0.74 ,1.26 ,"user","userOverallSys")
WTheoNpartVRel5j3 = Systematic("WTheoNpart",configMgr.weights,0.87 ,1.13 ,"user","userOverallSys")
WTheoNpartVRelT5j3 = Systematic("WTheoNpart",configMgr.weights,0.82 ,1.18 ,"user","userOverallSys")
WTheoNpartVRelW5j3 = Systematic("WTheoNpart",configMgr.weights,0.83 ,1.17 ,"user","userOverallSys")
WTheoNpartVRel1L2Ba1 = Systematic("WTheoNpart",configMgr.weights,0.93 ,1.07 ,"user","userOverallSys")
WTheoNpartVRel1L2Ba2 = Systematic("WTheoNpart",configMgr.weights,0.94 ,1.06 ,"user","userOverallSys")
WTheoNpartVRel1L2Bc1 = Systematic("WTheoNpart",configMgr.weights,0.93 ,1.07 ,"user","userOverallSys")
WTheoNpartVRel1L2Bc2 = Systematic("WTheoNpart",configMgr.weights,0.88 ,1.12 ,"user","userOverallSys")
WTheoNpartVRel1L1Ba1 = Systematic("WTheoNpart",configMgr.weights,0.58 ,1.42 ,"user","userOverallSys")
WTheoNpartVRel1L1Ba2 = Systematic("WTheoNpart",configMgr.weights,0.9 ,1.1 ,"user","userOverallSys")
WTheoNpartVRel1L1Bc1 = Systematic("WTheoNpart",configMgr.weights,0.83 ,1.17 ,"user","userOverallSys")
WTheoNpartVRel1L1Bc2 = Systematic("WTheoNpart",configMgr.weights,0.92 ,1.08 ,"user","userOverallSys")
#WTheoNpartVRel2L1 = Systematic("WTheoNpart",configMgr.weights,nan ,nan ,"user","userOverallSys")
#WTheoNpartVRel2L2 = Systematic("WTheoNpart",configMgr.weights,nan ,nan ,"user","userOverallSys")
WTheoNpartVRel2L3 = Systematic("WTheoNpart",configMgr.weights,0.77 ,1.23 ,"user","userOverallSys")
