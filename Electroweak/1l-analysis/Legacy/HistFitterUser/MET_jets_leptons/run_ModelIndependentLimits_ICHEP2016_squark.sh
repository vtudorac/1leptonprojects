lxplusLSFparams="-q 8nh -J HF"
mogonLSFparams="-app Reserve5G -R rusage[atlasio=0] -q atlasshort -W 5:00 -J HF"

if [[ $HOSTNAME == *".cern.ch"* ]]
then
	echo "I am on lxplus.";
	myLSFparams="$lxplusLSFparams"
fi
if [[ $HOSTNAME == *"mogon"* ]]
then
	echo "I am on mogon.";
	myLSFparams="$mogonLSFparams"
fi

HistFitter.py -r SR4JSSx12EM_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR4JSSx12EM_2016.log 2>&1
HistFitter.py -r SR5JSSx12EM_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR5JSSx12EM_2016.log 2>&1
HistFitter.py -r SR4JSSlowxEM_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR4JSSlowxEM_2016.log 2>&1
HistFitter.py -r SR5JSShighxEM_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR5JSShighxEM_2016.log 2>&1

HistFitter.py -r SR4JSSx12El_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR4JSSx12El_2016.log 2>&1
HistFitter.py -r SR5JSSx12El_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR5JSSx12El_2016.log 2>&1
HistFitter.py -r SR4JSSlowxEl_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR4JSSlowxEl_2016.log 2>&1
HistFitter.py -r SR5JSShighxEl_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR5JSShighxEl_2016.log 2>&1

HistFitter.py -r SR4JSSx12Mu_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR4JSSx12Mu_2016.log 2>&1
HistFitter.py -r SR5JSSx12Mu_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR5JSSx12Mu_2016.log 2>&1
HistFitter.py -r SR4JSSlowxMu_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR4JSSlowxMu_2016.log 2>&1
HistFitter.py -r SR5JSShighxMu_2016 -t -w python/MyDiscoveryAnalysis_2016_SR.py >HF_run_SR5JSShighxMu_2016.log 2>&1

bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR4JSSx12EM_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR4JSSx12EM_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 21 >HF_run_upperLimit_SR4JSSx12EM_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR5JSSx12EM_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR5JSSx12EM_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 24 >HF_run_upperLimit_SR5JSSx12EM_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR4JSSlowxEM_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR4JSSlowxEM_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 24 >HF_run_upperLimit_SR4JSSlowxEM_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR5JSShighxEM_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR5JSShighxEM_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 20 >HF_run_upperLimit_SR5JSShighxEM_2016.log 2>&1"

bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR4JSSx12El_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR4JSSx12El_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 >HF_run_upperLimit_SR4JSSx12El_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR5JSSx12El_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR5JSSx12El_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 >HF_run_upperLimit_SR5JSSx12El_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR4JSSlowxEl_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR4JSSlowxEl_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 >HF_run_upperLimit_SR4JSSlowxEl_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR5JSShighxEl_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR5JSShighxEl_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 >HF_run_upperLimit_SR5JSShighxEl_2016.log 2>&1"

bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR4JSSx12Mu_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR4JSSx12Mu_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 19 >HF_run_upperLimit_SR4JSSx12Mu_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR5JSSx12Mu_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR5JSSx12Mu_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 23 >HF_run_upperLimit_SR5JSSx12Mu_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR4JSSlowxMu_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR4JSSlowxMu_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 19 >HF_run_upperLimit_SR4JSSlowxMu_2016.log 2>&1"
bsub $myLSFparams "UpperLimitTable.py -n 10000 -c SR5JSShighxMu_2016 -w results/MyDiscoveryAnalysis_Paper2015_HardLepton_SR5JSShighxMu_2016/SPlusB_combined_BasicMeasurement_model.root -l 6.74299 -R 17 >HF_run_upperLimit_SR5JSShighxMu_2016.log 2>&1"
