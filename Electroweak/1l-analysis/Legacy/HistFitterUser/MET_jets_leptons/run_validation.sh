#!/bin/bash

# Absolute path to this script
script=$(readlink -f "$0")
# Absolute path this script is in
my_path=$(dirname "$script")

in_ds=$1

echo $in_ds

#make sure that tables are activated!

HistFitter.py -w -r $in_ds -f -m "all" -D "corrMatrix" --fitname=Validation ${my_path}/python/ICHEP/OneLepton_ICHEP.py 2>&1 | tee out_${in_ds}.log

mkdir -p tables

## CR
YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c TR${in_ds}EM,WR${in_ds}EM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u TR${in_ds}EM,WR${in_ds}EM -o tables/MyTable1LepCR_${in_ds}.tex
SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c TR${in_ds}EM -o tables/Systematics1LepTR${in_ds}.tex
SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c WR${in_ds}EM -o tables/Systematics1LepWR${in_ds}.tex

## SR
if [ $in_ds != "4JlowxGG" ]
  then
    YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}EM,SR${in_ds}El,SR${in_ds}Mu -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u SR${in_ds}EM,SR${in_ds}El,SR${in_ds}Mu -o tables/MyTable1LepSR_${in_ds}.tex
    YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}EM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u SR${in_ds}EM -o tables/MyTable1LepSR_${in_ds}2.tex 2>&1 | tee -a out_${in_ds}.log
    SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}EM -o tables/Systematics1LepSR${in_ds}.tex
  else
    YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}exclEM,SR${in_ds}exclEl,SR${in_ds}exclMu -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u SR${in_ds}exclEM,SR${in_ds}exclEl,SR${in_ds}exclMu -o tables/MyTable1LepSR_${in_ds}excl.tex
    YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}exclEM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u SR${in_ds}exclEM -o tables/MyTable1LepSR_${in_ds}excl2.tex 2>&1 | tee -a out_${in_ds}.log
    SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}exclEM -o tables/Systematics1LepSR${in_ds}.tex    
    YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}discEM,SR${in_ds}discEl,SR${in_ds}discMu -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u SR${in_ds}discEM,SR${in_ds}discEl,SR${in_ds}discMu -o tables/MyTable1LepSR_${in_ds}disc.tex
    YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}discEM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u SR${in_ds}discEM -o tables/MyTable1LepSR_${in_ds}disc2.tex 2>&1 | tee -a out_${in_ds}.log
    SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c SR${in_ds}discEM -o tables/Systematics1LepSR${in_ds}disc.tex
fi
 
## VR
if [ $in_ds = "2J" ]
  then
  YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_1EM,VR${in_ds}_2EM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u VR${in_ds}_1EM,VR${in_ds}_2EM -o tables/MyTable1LepVR_${in_ds}.tex
  SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_1EM,VR${in_ds}_2EM -o tables/Systematics1LepVR${in_ds}.tex ## VR syst
fi

if [ $in_ds = "4JhighxGG" ]
  then
  YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_mtEM,VR${in_ds}_metovermeffEM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u VR${in_ds}_mtEM,VR${in_ds}_metovermeffEM -o tables/MyTable1LepVR_${in_ds}.tex
  SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_mtEM,VR${in_ds}_metovermeffEM -o tables/Systematics1LepVR${in_ds}.tex ## VR syst
fi

if [ $in_ds = "4JlowxGG" ] || [ $in_ds = "6JGGx12" ] || [ $in_ds = "6JGGx12HM" ] || $in_ds = "4JSSlowx" || [ $in_ds = "4JSSx12" ] || [ $in_ds = "5JSShighx" ]
  then
  YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_mtEM,VR${in_ds}_aplanarityEM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u VR${in_ds}_mtEM,VR${in_ds}_aplanarityEM -o tables/MyTable1LepVR_${in_ds}.tex
  SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_mtEM,VR${in_ds}_aplanarityEM -o tables/Systematics1LepVR${in_ds}.tex ## VR syst
fi

if [ $in_ds = "5JSSx12" ]
  then
  YieldsTable.py -b -y -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_mtEM,VR${in_ds}_metEM -s ttbar,wjets_Sherpa221,zjets_Sherpa22,singletop,diboson,ttv -t $in_ds -u VR${in_ds}_mtEM,VR${in_ds}_metEM  -o tables/MyTable1LepVR_${in_ds}.tex
  SysTable.py -% -w results/OneLepton_${in_ds}/Validation_combined_BasicMeasurement_model_afterFit.root -c VR${in_ds}_mtEM,VR${in_ds}_metEM -o tables/Systematics1LepVR${in_ds}.tex ## VR syst
fi


