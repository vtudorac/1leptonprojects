#!/usr/bin/env python
from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
from ROOT import ConfigMgr,FitConfig #this module comes from gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet,RooAbsData,RooRandom 
from ROOT import Util, TMath
from ROOT import RooFit

import os
import sys
from sys import exit

from SysTableTex import *
import pickle


def getnamemap():

  namemap = {}

  namemap['alpha_BT'] = 'B tagging'
  namemap['alpha_JR'] = 'Jet energy resolution'
  namemap['alpha_JR3T'] = 'Jet energy resolution 3jT'
  namemap['alpha_JR4T'] =  'Jet energy resolution 4jT'
  namemap['alpha_KtScaleTop'] = 'kT scale Alpgen ttbar'
  namemap['alpha_KtScaleWZ'] = 'kT scale Alpgen W+jets'
  namemap['alpha_LE'] = 'Lepton efficiency'
  namemap['alpha_LES'] = 'Lepton energy scale'
  namemap['alpha_LRI'] = 'Lepton energy resolution with inner detector'
  namemap['alpha_LRI3T'] = 'Lepton energy resolution with inner detector 3jT'
  namemap['alpha_LRI4T'] = 'Lepton energy resolution with inner detector 4jT'
  namemap['alpha_LRM'] =  'Lepton energy resolution with muon system'
  namemap['alpha_LRM3T'] =  'Lepton energy resolution with muon system 3jT'
  namemap['alpha_LRM4T'] =  'Lepton energy resolution with muon system 4jT'
  namemap['alpha_MC'] = 'MET cell-out'
  namemap['alpha_MC3T'] = 'MET cell-out 3jT'
  namemap['alpha_MC4T'] = 'MET cell-out 4jT'
  namemap['alpha_MP'] = 'MET pile-up'
  namemap['alpha_MP3T'] = 'MET pile-up 3jT'
  namemap['alpha_MP4T'] = 'MET pile-up 4jT'
  namemap['alpha_PU'] = 'Pile-up'

  namemap['alpha_PtMinTop3T'] = 'pTmin ttbar 3jT'
  namemap['alpha_PtMinTop4T'] = 'pTmin ttbar 4jT'
  namemap['alpha_PtMinTopC'] =  'pTmin ttbar control regions'
  namemap['alpha_PtMinTopS2T'] =  'pTmin ttbar s1l2j'
  namemap['alpha_PtMinWZ3T'] =  'pTmin W+jets 3jT'
  namemap['alpha_PtMinWZ4T'] =  'pTmin W+jets 4jT'
  namemap['alpha_PtMinWZC'] =  'pTmin W+jets control regions'
  namemap['alpha_PtMinWZS2T'] =  'pTmin W+jets s1l2j'

  namemap['alpha_QCDNorm_S3_cuts'] = 'QCD estimate 3j'
  namemap['alpha_QCDNorm_S4_cuts'] = 'QCD estimate 4j'
  namemap['alpha_QCDNorm_TR_nJet'] =  'QCD estimate TRL1'
  namemap['alpha_QCDNorm_WR_nJet'] =  'QCD estimate WRL1'
  namemap['alpha_QCDNorm_SR1s2j_cuts'] = 'QCD estimate s1l2j' 
  namemap['alpha_QCDNorm_SR3jT_cuts'] = 'QCD estimate 3jT'	   
  namemap['alpha_QCDNorm_SR4jT_cuts'] = 'QCD estimate 4jT'   

  namemap['alpha_TE'] = 'Trigger weight'
  namemap['alpha_WP'] = 'W pT reweighting'

  namemap['gamma_J3T_bin_0'] = 'Jet energy scale 3jT'
  namemap['gamma_J4T_bin_0'] = 'Jet energy scale 4jT'
  namemap['gamma_JS2T_bin_0'] = 'Jet energy scale s1l2j'

  namemap['gamma_JC_bin_0'] = 'Jet energy scale control regions bin 0'
  namemap['gamma_JC_bin_1'] = 'Jet energy scale control regions bin 1'
  namemap['gamma_JC_bin_2'] = 'Jet energy scale control regions bin 2'
  namemap['gamma_JC_bin_3'] = 'Jet energy scale control regions bin 3'
  namemap['gamma_JC_bin_4'] = 'Jet energy scale control regions bin 4'
  namemap['gamma_JC_bin_5'] = 'Jet energy scale control regions bin 5'
  namemap['gamma_JC_bin_6'] = 'Jet energy scale control regions bin 6'

  namemap['gamma_stat_S4_cuts_bin_0'] = 'MC statistics 4jT bin 0'
  namemap['gamma_stat_TR_nJet_bin_6'] = 'MC statistics TRL1 bin 6'
  namemap['gamma_stat_WR_nJet_bin_6'] = 'MC statistics WRL1 bin 6'

  namemap['gamma_stat_SR1s2j_cuts_bin_0'] = 'MC statistics s1l2j bin 0'
  namemap['gamma_stat_SR3jT_cuts_bin_0'] = 'MC statistics 3jT bin 0'
  namemap['gamma_stat_SR4jT_cuts_bin_0'] = 'MC statistics 4jT bin 0'

  namemap['mu_S3'] = 'Signal yield 3j'
  namemap['mu_S4'] = 'Signal yield 4j'
  namemap['mu_Top'] =  'ttbar yield'
  namemap['mu_WZ'] = 'W(Z)+jets yield'

  namemap['mu_SR3jT'] = 'Signal yield 3jT'
  namemap['mu_SR4jT'] = 'Signal yield 4jT'

  namemap['mu_SS'] = 'Signal yield soft lepton SR'
  namemap['mu_SR1s2j'] = 'Signal yield s1l2j'

  return namemap

  

def latexfitresults(chanSys,filename,resultname='RooFitResult_obsData_fitRegions_WR_nJet_TR_nJet_SR3jT_cuts', signalregion='3jL' , dataname='obsData',resultnameCR=''):

  ############################################

  namemap = {}
  namemap = getnamemap()

  ############################################

  w = Util.GetWorkspaceFromFile(filename,'w')

  if w==None:
    print "ERROR : Cannot open workspace : ", workspacename
    sys.exit(1) 

  result = w.obj(resultname)
  if result==None:
    print "ERROR : Cannot open fit result : ", resultname
    sys.exit(1)

##   resultCR = None
##   resultCR = w.obj(resultnameCR)
##   if resultCR==None:
##     print "ERROR : Cannot open fit result : ", resultnameCR
    
  # snapshot = resultname.replace('RooFitResult','snapshot_paramsVals')
  snapshot =  'snapshot_paramsVals_' + resultname
  w.loadSnapshot(snapshot)

  data_set = w.data(dataname)
  if data_set==None:
    print "ERROR : Cannot open dataset : ", "data_set"
    sys.exit(1)
      
  regionCat = w.obj("channelCat")
  data_set.table(regionCat).Print("v");

  signalregionFullName = Util.GetFullRegionName(regionCat, signalregion);

  #####################################################

  SRcat = 'channelCat==channelCat::' + signalregionFullName.Data()
  dataSR = data_set.reduce(SRcat)
  
  nobs_SR = 0.
  
  if dataSR:
    nobs_SR = dataSR.sumEntries()
  else:
    print " ERROR : dataset-category dataSR not found"

  chanSys['sqrtnobsa'].append( [TMath.Sqrt(nobs_SR)] )

  ####

  varsInSR = []
  errsInSR = 0.0
  
  bkginSR  = Util.GetComponent(w,"Top,WZ,QCD,BG",signalregion)
  nbkginSR = bkginSR.getVal()
  # bkgerrorcr = Util.GetPropagatedError(bkginSR, resultCR)
  bkgerrororig = Util.GetPropagatedError(bkginSR, result) #, resultCR)
  bkgerrororigP = bkgerrororig
  bkgerrororigM = bkgerrororig
  
  print "GREPME: SR combined Bkg fit: " , result.covQual(), "  ", bkginSR.getVal() ," +- " , bkgerrororig, " - ", bkgerrororigM, " + " , bkgerrororigP,  "\n\n"

  QCDinSR = Util.GetComponent(w,"QCD",signalregion) #w.function("nexp_wbij_QCDSR_SR"+lmtsuffix)
  nQCDinSR = QCDinSR.getVal()
  QCDinSRerr = Util.GetPropagatedError(QCDinSR, result) #, resultCR)
  QCDinSRerrP = QCDinSRerr
  QCDinSRerrM = QCDinSRerr
  chanSys['poisqcderr'].append( [ QCDinSRerrM, QCDinSRerrP, (QCDinSRerrM+QCDinSRerrP)/2., QCDinSRerr ] )

  #  1. FIRST CALCULATE THE ERROR PROPAGATED PURELY FROM THE FIT TO CRS
  fpf = result.floatParsFinal() # in SR
#  fpfCR = resultCR.floatParsFinal() # in SR

  # set all floating parmaeters constant
  for idx in range(fpf.getSize()):
    parname = fpf[idx].GetName()
    par = w.var(parname)
    par.setConstant()
    #    print   " XXX-XXX      ", parname
    pass

  # do not calculate the error on mu_SIG
  mu_SIG_name = 'mu_' + signalregion

  errmap = {}

  # calculate error per parameter on  fitresult
  for idx in range(fpf.getSize()):
    parname = fpf[idx].GetName()
    if parname.find(mu_SIG_name)==0: continue
    par = w.var(parname)
    par.setConstant(False)
    #    par.Print()
    bkgerror  = Util.GetPropagatedError(bkginSR, result)
    bkgerrorP = bkgerror
    bkgerrorM = bkgerror
    bkgerrorA = ( abs(bkgerrorP) + abs(bkgerrorM) )/2.
    errmap[parname] = [ bkgerrorM, bkgerrorP, bkgerrorA, bkgerror ]

    par.setConstant()
    pass

  ## #  2. CALCULATE THE ERROR ON EACH PARAMETER THAT IS ONLY(!) IN SR on CR+SR fit
##   # list of all pars in CR fit
##   parsCR = [ (fpfCR[idxcr].GetName())  for idxcr in range(fpfCR.getSize())]
##   # print parsCR

##   #find the pars that are only in SR
##   fpf = result.floatParsFinal() # in CR+SR
##   parsSR = []
##   for idx in range(fpf.getSize()):
##     foundParSR = False
##     parname = fpf[idx].GetName()
##     par = w.var(parname)
##     par.setConstant()
##     if parname not in parsCR:
##       #     print " found parSR = ", fpf[idx].GetName()
##       parsSR.append(parname)
##     pass
##   # print parsSR

##   # calculate the error as result of each SR parameter for fit result in CR+SR
##   parsSRerror = []
##   for parname in parsSR:
##     if parname.find(mu_SIG_name)==0: continue
##     par = w.var(parname)
##     par.setConstant(False)
##     bkgerror  = Util.GetPropagatedError(bkginSR, result)
##     parsSRerror.append(bkgerror)
##     errmap[parname] = [ bkgerror, bkgerror, bkgerror, bkgerror ]
##     par.setConstant()
##     pass

##   # add all errors in quadrature to the error propagated from CR only fit
##   bkgerrorSR = bkgerrorcr*bkgerrorcr # bkgerrororig*bkgerrororig
##   for parEr in parsSRerror:
##     print " parEr = ", parEr
##     bkgerrorSR += parEr*parEr
    
##   bkgerrorSR = TMath.Sqrt(bkgerrorSR)

##   print " \n orig CR fit error  = ", bkgerrorcr
##   print " \n orig SR fit error  = ", bkgerrororig
##   print "  SR augm.  error  = ", bkgerrorSR

  #print "SR combined Bkg fit: " , bkginSR.getVal() ," +- " , bkgerrororig, " - ", bkgerrororigM ," + ", bkgerrororigP ,"\n\n"
  #  chanSys['totbkgsysa'].append( [bkgerrororigM,bkgerrororigP,(bkgerrororigM+bkgerrororigP)/2.,bkgerrororig] )
  chanSys['totbkgsysa'].append( [bkgerrororigM,bkgerrororigP,bkgerrororig,bkgerrororig] )

  errmap_listofkeys = errmap.keys()
  errmap_listofkeys.sort()

  for name in errmap_listofkeys: #bkgerrmap.keys():
    if namemap.has_key(name):
      nameconv = namemap[name]
      if chanSys.has_key(nameconv):
        chanSys[nameconv].append( errmap[name] )
      else: 
        chanSys[nameconv] = []
        chanSys[nameconv].append(errmap[name])
    else: 
      if chanSys.has_key(name):
         chanSys[name].append( errmap[name] )
      else:
         chanSys[name] = []
         chanSys[name].append( errmap[name] )
    pass


  return 





##################################
##################################
##################################


def latexfitresults_method2(chanSys,filename,resultname='RooFitResult_obsData_fitRegions_WR_nJet_TR_nJet_SR3jT_cuts', signalregion='3jL', fitregions = 'WR,TR,S3,S4,SR3jT,SR4jT', dataname='obsData'):

############################################
##   ConfigMgr* mgr = ConfigMgr::getInstance();
##   FitConfig* fc = mgr->getFitConfig(fcName);
##   Float_t lumi =  fc->getLumi();

  #cppMgr = ConfigMgr.getInstance() #C++ alter ego of this configManager

  namemap = {}
  namemap = getnamemap()

 ############################################

  w = Util.GetWorkspaceFromFile(filename,'w')
  if w==None:
    print "ERROR : Cannot open workspace : "
    sys.exit(1) 

  result = w.obj(resultname)
  if result==None:
    print "ERROR : Cannot open fit result : ", resultname
    sys.exit(1)
    
  snapshot = resultname.replace('RooFitResult','snapshot_paramsVals')
  w.loadSnapshot(snapshot)

  data_set = w.data(dataname)
  if data_set==None:
    print "ERROR : Cannot open dataset : ", "data_set"
    sys.exit(1)
      
  regionCat = w.obj("channelCat")
  data_set.table(regionCat).Print("v");

  signalregionFullName = Util.GetFullRegionName(regionCat, signalregion);

  #####################################################

  SRcat = 'channelCat==channelCat::' + signalregionFullName.Data()
  dataSR = data_set.reduce(SRcat)
  nobs_SR = 0.
  
  if dataSR:
    nobs_SR = dataSR.sumEntries()
  else:
    print " ERROR : dataset-category dataSR not found"

  chanSys['sqrtnobsa'].append( [TMath.Sqrt(nobs_SR)] )

  ####

  # 0) check if the fit result in workspace was done on the same dataset
  lumiConst = True
  defaultresult = result
  if dataname not in resultname:
    print dataname, " not  used in default fit result, gonna redo the fit with this dataset"
    w.loadSnapshot('snapshot_paramsVals_initial')
    defaultresult = Util.FitPdf(w, fitregions, lumiConst, data_set)
        
  # load the snapshot for the defaultresult
  defaultresultname = defaultresult.GetName()
  defaultsnapshot = defaultresultname.replace('RooFitResult','snapshot_paramsVals')
  w.loadSnapshot(defaultsnapshot)
  
  # 1) calculate nbkginSR w/ all pars floating
  bkginSR  = Util.GetComponent(w,"Top,WZ,QCD,BG",signalregion)
  #bkginSR  = Util.GetComponent(w,"Top",signalregion)
  nbkginSR = bkginSR.getVal()
  nbkginSRerr = Util.GetPropagatedError(bkginSR, defaultresult)
  chanSys['totbkgsysa'].append( [nbkginSRerr,nbkginSRerr,(nbkginSRerr+nbkginSRerr)/2.,nbkginSRerr] )

  fpf = result.floatParsFinal()
  mu_SIG_name = 'mu_' + signalregion
  errmap = {}

  # 2) calculate nbkginSR w/ only 1 parameter fixed
  for idx in range(fpf.getSize()):
    # before redoing the fit, set the values of parameters to default
    w.loadSnapshot(defaultsnapshot)

    parname = fpf[idx].GetName()
    print "\n\n parname = ", parname
    if parname.find(mu_SIG_name)==0: continue

    # the parameter that is fixed, needs to have the value of the default fit
    par = w.var(parname)
    parDefVal =  par.getVal()

    # before redoing the fit, set the values of parameters to initial snapshot, otherwise MIGRAD cannot find improvement
    w.loadSnapshot('snapshot_paramsVals_initial')
    par.setVal(parDefVal)
    par.setConstant(True)
    suffix = parname + "Fixed"
    result_1parfixed = Util.FitPdf(w, fitregions, lumiConst, data_set, suffix)

    bkginSR  = Util.GetComponent(w,"Top,WZ,QCD,BG",signalregion)

    nbkginSR_1parfixed = bkginSR.getVal()
    nbkginSRerr_1parfixed = Util.GetPropagatedError(bkginSR, result_1parfixed)

    if nbkginSRerr_1parfixed > nbkginSRerr:
      print "\n\n  WARNING  parameter ", parname," gives a larger error when set constant. Do you expect this?"
      print "        WARNING          nbkginSRerr = ", nbkginSRerr, "    nbkginSRerr_1parfixed = ",  nbkginSRerr_1parfixed

    systerror  =  TMath.Sqrt(abs(nbkginSRerr*nbkginSRerr - nbkginSRerr_1parfixed*nbkginSRerr_1parfixed))
    par.setConstant(False)

    ## only add parameters to the table if the fit converged
#    print " XXX status = ",  result_1parfixed.status(), "   covQual() = ", result_1parfixed.covQual()
#    print " XXX numStatusHistory() = ",  result_1parfixed.numStatusHistory()
#    if result_1parfixed.numStatusHistory() >0:
#      print "   XXX  statusCodeHistory(0) = ",   result_1parfixed.statusCodeHistory(0), "   statusLabelHistory(0) = ",   result_1parfixed.statusLabelHistory(0)
#    if result_1parfixed.numStatusHistory() >1:
#      print "   XXX  statusCodeHistory(1) = ",   result_1parfixed.statusCodeHistory(1), "   statusLabelHistory(1) = ",   result_1parfixed.statusLabelHistory(1)

    if result_1parfixed.status()==0 and result_1parfixed.covQual()==3 and result_1parfixed.numStatusHistory()==2 and  result_1parfixed.statusCodeHistory(0)==0 and  result_1parfixed.statusCodeHistory(1) ==0:
      systerror = systerror
    else:
      systerror = 0.0

    print " \n   XXX  parameter = ", parname,"       systerror = ", systerror, "             nbkginSRerr = ", nbkginSRerr, "    nbkginSRerr_1parfixed = ",  nbkginSRerr_1parfixed

    systerrorP = systerror
    systerrorM = systerror
    systerrorA = ( abs(systerrorP) + abs(systerrorM) )/2.
    errmap[parname] = [ systerrorM, systerrorP, systerrorA, systerror ]

    pass

  QCDinSR = Util.GetComponent(w,"QCD",signalregion) #w.function("nexp_wbij_QCDSR_SR"+lmtsuffix)
  nQCDinSR = QCDinSR.getVal()
  QCDinSRerr = Util.GetPropagatedError(QCDinSR, result)
  QCDinSRerrP = QCDinSRerr
  QCDinSRerrM = QCDinSRerr
  chanSys['poisqcderr'].append( [ QCDinSRerrM, QCDinSRerrP, (QCDinSRerrM+QCDinSRerrP)/2., QCDinSRerr ] )

  for name in errmap.keys(): #bkgerrmap.keys():
    if namemap.has_key(name):
      nameconv = namemap[name]
      if chanSys.has_key(nameconv):
        chanSys[nameconv].append( errmap[name] )
      else: 
        chanSys[nameconv] = []
        chanSys[nameconv].append(errmap[name])
    else: 
      if chanSys.has_key(name):
         chanSys[name].append( errmap[name] )
      else:
         chanSys[name] = []
         chanSys[name].append( errmap[name] )
    pass

  return 




##################################
##################################
##################################

# MAIN

if __name__ == "__main__":
  
  import os, sys
  import getopt
  def usage():
    print "Usage:"
    print "BkgSysTable.py [-o outputFileName] [-c channels] [-w workspace_afterFit] \n"
    print "\nFor example:"
    print "./BkgSysTable.py.py -w results/Combined_KFactorFit_5Channel_bkgonly_combined_BasicMeasurement_model_afterFit.root -c SR7jTEl_meffInc,SR7jTMu_meffInc -o SystematicsMultiJetsSR.tex"
    sys.exit(0)        

  wsFileName='/results/MyOneLeptonKtScaleFit_HardLepR17_BkgOnlyKt_combined_NormalMeasurement_model_afterFit.root'
  try:
    opts, args = getopt.getopt(sys.argv[1:], "o:c:w:")
  except:
    usage()
  if len(opts)<2:
    usage()

  outputFileName="default"
  for opt,arg in opts:
    if opt == '-c':
      chanStr=arg.replace(",","_")
      chanList=arg.split(",")
    elif opt == '-w':
      wsFileName=arg
    elif opt == '-o':
      outputFileName=arg


if outputFileName=="default":
  outputFileName=chanStr+'_BkgSysTable.tex'
  pass

## Tight Signal Regions
chanSys = {}
chanSys['sqrtnobsa'] = []
chanSys['totbkgsysa'] = []
chanSys['poisqcderr'] = []

resultname = 'RooExpandedFitResult_afterFit'

#  Method-1
for sr in chanList:
  latexfitresults(chanSys,wsFileName,resultname,sr,'obsData')
  
skiplist = ['sqrtnobsa', 'totbkgsysa', 'poisqcderr']

line_chanSysTight = tablefragment(chanSys,'Signal',chanList,skiplist,chanStr)

f = open(outputFileName, 'w')
f.write( line_chanSysTight )
f.close()
print "\nwrote results in file: %s"%(outputFileName)

