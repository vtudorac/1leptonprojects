#!/usr/bin/env python

from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet, RooFormulaVar, RooAbsData, RooRandom 
from ROOT import Util, TMath, TMap, RooExpandedFitResult

from ResultsTableTex import *
import os
import sys

# Main function calls are defined below.

#def latexfitresults(filename = os.environ['SUSYFITTER'] +'/afs/physik.uni-mainz.de/home/urrejola/workspace/Fitter/130328_trunk/HistFitterUser/MET_jets_leptons/results/OneLeptonMoriond2013_SingleBin_AlpgenJimmyW/Validation_combined_BasicMeasurement_model_afterFit.root',resultname='RooFitResult_dataFitRegions_fitRegions_WR3JEl_cuts_WR3JMu_cuts_TR3JEl_cuts_TR3JMu_cuts_WR5JEl_cuts_WR5JMu_cuts_TR5JEl_cuts_TR5JMu_cuts_WR6JEl_cuts_WR6JMu_cuts_TR6JEl_cuts_TR6JMu_cuts',regionList='VR3JhighMETEl_cuts,VR3JhighMETMu_cuts,VR5JhighMETEl_cuts,VR5JhighMETMu_cuts,VR6JhighMETEl_cuts,VR6JhighMETMu_cuts,VR3JhighMTEl_cuts,VR3JhighMTMu_cuts,VR5JhighMTEl_cuts,VR5JhighMTMu_cuts,VR6JhighMTEl_cuts,VR6JhighMEMu_cuts', dataname='obsData'):

def latexfitresults(filename = os.environ['SUSYFITTER'] +'/afs/physik.uni-mainz.de/home/urrejola/workspace/Fitter/130328_trunk/HistFitterUser/MET_jets_leptons/results/OneLeptonMoriond2013_SingleBin_AlpgenJimmyW/bkgonly_combined_BasicMeasurement_model_afterFit.root',resultname='RooFitResult_dataFitRegions_fitRegions_WR3JEl_cuts_WR3JMu_cuts_TR3JEl_cuts_TR3JMu_cuts_WR5JEl_cuts_WR5JMu_cuts_TR5JEl_cuts_TR5JMu_cuts_WR6JEl_cuts_WR6JMu_cuts_TR6JEl_cuts_TR6JMu_cuts',regionList='WR3JEl_cuts,WR3JMu_cuts,TR3JEl_cuts,TR3JMu_cuts,WR5JEl_cuts,WR5JMu_cuts,TR5JEl_cuts,TR5JMu_cuts,WR6JEl_cuts,WR6JMu_cuts,TR6JEl_cuts,TR6JMu_cuts', doAsym=True, dataname='obsData'):

  print "Opening file ", filename
  w = Util.GetWorkspaceFromFile(filename,'w')


  if w==None:
    print "ERROR : Cannot open workspace : "#, workspacename
    sys.exit(1) 
    
  result = w.obj(resultname)

  if result==None:
    print "ERROR : Cannot open fit result : ", resultname
    sys.exit(1)

  snapshot =  'snapshot_paramsVals_' + resultname
  w.loadSnapshot(snapshot)
  print snapshot

  if not w.loadSnapshot(snapshot):
    print "ERROR : Cannot load snapshot : ", snapshot
    sys.exit(1)

    
  data_set = w.data(dataname)
  if data_set==None:
    print "ERROR : Cannot open dataset : ", "data_set"+suffix
    sys.exit(1)
      
  regionCat = w.obj("channelCat")
  data_set.table(regionCat).Print("v");

  regionFullNameList = [ Util.GetFullRegionName(regionCat, region) for region in regionList]
  print regionFullNameList

  ###

  tablenumbers = {}
  tablenumbers['names'] = regionList 
 
  regionCatList = [ 'channelCat==channelCat::' +region.Data() for region in regionFullNameList]
  
  regionDatasetList = [data_set.reduce(regioncat) for regioncat in regionCatList]
  for index, data in  enumerate(regionDatasetList):
    data.SetName("data_" + regionList[index])
    data.SetTitle("data_" + regionList[index])
    
  nobs_regionList = [ data.sumEntries() for data in regionDatasetList]
  tablenumbers['nobs'] = nobs_regionList
  print nobs_regionList
 
  ####
  bkginRegionList = [ Util.GetComponent(w,"ttbarV,SherpaDibosonsMassiveBC,QCD,SingleTop,SherpaZMassiveBC,PowhegPythiaTTbar,SherpaWMassiveBC",region) for region in regionList]
  nbkginRegionList = [  bkginRegion.getVal() for bkginRegion in bkginRegionList]
  [region.Print() for region in bkginRegionList]
  print nbkginRegionList

  nbkgerrinRegionList = [ Util.GetPropagatedError(bkginRegion, result, doAsym)  for bkginRegion in bkginRegionList]
  print nbkgerrinRegionList

  tablenumbers['Fitted_bkg_events']    =  nbkginRegionList
  tablenumbers['Fitted_bkg_events_err']    =  nbkgerrinRegionList
  
  return tablenumbers




##################################
##################################
##################################

if __name__ == "__main__":

  runInterpreter = True
  skipStatErr=False
  outputFileName="default"
  useBeforeFit=False
  
  import os, sys
  import getopt
  def usage():
    print "Usage:"
    print "./MakePullPlot_HardLep.py [-i] [-y][-c channels] [-w workspace_afterFit] [-o outputFileName] [-b] \n"
    print "\nFor example:"
    print "./MakePullPlot_HardLep.py -c WR3JEl_cuts,WR3JMu_cuts,TR3JEl_cuts,TR3JMu_cuts,WR5JEl_cuts,WR5JMu_cuts,TR5JEl_cuts,TR5JMu_cuts,WR6JEl_cuts,WR6JMu_cuts,TR6JEl_cuts,TR6JMu_cuts -w /afs/cern.ch/user/[letter]/[user]/[HistFitterUser_path]/HistFitterUser/MET_jets_leptons/results/OneLeptonMoriond2013_SingleBin/bkgonly_combined_BasicMeasurement_model_afterFit.root"
    print "./MakePullPlot_HardLep.py -c VR3JhighMETEl_cuts,VR3JhighMETMu_cuts,VR5JhighMETEl_cuts,VR5JhighMETMu_cuts,VR6JhighMETEl_cuts,VR6JhighMETMu_cuts,VR3JhighMTEl_cuts,VR3JhighMTMu_cuts,VR5JhighMTEl_cuts,VR5JhighMTMu_cuts,VR6JhighMTEl_cuts,VR6JhighMEMu_cuts -w /afs/cern.ch/user/[letter]/[user]/[HistFitterUser_path]/HistFitterUser/MET_jets_leptons/results/OneLeptonMoriond2013_SingleBin/Validation_combined_BasicMeasurement_model_afterFit.root -o Pulls_Valid"
    sys.exit(0)        

  wsFileName='/afs/physik.uni-mainz.de/home/urrejola/workspace/Fitter/130328_trunk/HistFitterUser/MET_jets_leptons/results/OneLeptonMoriond2013_SingleBin_AlpgenJimmyW/bkgonly_combined_BasicMeasurement_model_afterFit.root'
#  wsFileName='/afs/physik.uni-mainz.de/home/urrejola/workspace/Fitter/130328_trunk/HistFitterUser/MET_jets_leptons/results/OneLeptonMoriond2013_SingleBin_AlpgenJimmyW/Validation_combined_BasicMeasurement_model_afterFit.root'

  try:
    opts, args = getopt.getopt(sys.argv[1:], "iysbo:c:w:")
  except:
    usage()
#  if len(opts)<2:
#    usage()

  doAsym=True
  for opt,arg in opts:
    if opt == '-c':
      chanStr=arg.replace(",","_")
      chanList=arg.split(",")
    elif opt == '-w':
      wsFileName=arg
    elif opt == '-o':
      outputFileName=arg
    elif opt == '-i':
      runInterpreter = True
    elif opt == '-s':
      skipStatErr = True
    elif opt == '-y':
      doAsym=True

if outputFileName=="default":
  outputFileName=chanStr+"_AsPull"
  pass

#print "useBeforeFit",useBeforeFit

regionsList_1Digit = chanList
regionsList_2Digits = chanList


import pickle
if wsFileName.endswith(".pickle"):
  f = open(wsFileName, 'r')
  m3 = pickle.load(f)
  f.close()
else:
  # PULL PLOTS AFTER OR BEFORE FIT!!!!!
  if useBeforeFit:
    m3 = latexfitresults(wsFileName,'RooExpandedFitResult_beforeFit',regionsList_1Digit,doAsym)
  else:
    m3 = latexfitresults(wsFileName,'RooExpandedFitResult_afterFit',regionsList_1Digit,doAsym)
  f = open(outputFileName+".pickle", 'w')
  pickle.dump(m3, f)
  f.close()


from ROOT import XtraValues,ValidationUtils
from math import sqrt
from RegionsDic_HardLep import regDic

elVals = XtraValues()
muVals = XtraValues()
EMVals = XtraValues()
nDof=0.0
nDof_el=0.0
nDof_mu=0.0
nDof_em=0.0
chisq=0.0
chisq_el=0.0
chisq_mu=0.0
chisq_em=0.0
#print "regionsList_1Digit",regionsList_1Digit
#print "regDic",regDic
for reg in regionsList_1Digit:
  regToPrint=regDic[reg]
  isEl=(reg.find("El")>-1 or reg.find("ee")>-1)
  isMu=(reg.find("Mu")>-1 or reg.find("mm")>-1)
  isEM=(reg.find("EM")>-1 or reg.find("em")>-1)
  if isEl and isMu:
    print "WARNING ambiguous el/mu flavor for region '%s'. Skip."%reg
    continue
  elif isEl:
    outVals=elVals
  elif isMu:
    outVals=muVals
  elif isEM:
    outVals=EMVals
  else:
    print "ERROR unexpeted region named '%s'. Skip."%reg
    continue

  i=m3['names'].index(reg)
  nObs=m3['nobs'][i]
  nObsErr=0.0
  if nObs>0.0: nObsErr=sqrt(nObs)
  nPred = m3['Fitted_bkg_events'][i]
  nPred_eFit = m3['Fitted_bkg_events_err'][i]
  Delta=nObs-nPred
  if skipStatErr:
    totDeltaErr=nPred_eFit
  else:
    totDeltaErr=sqrt((nPred_eFit*nPred_eFit) + nPred)
  if totDeltaErr!=0.0:
    tmpChisq=(Delta/totDeltaErr)*(Delta/totDeltaErr)
  else:
    tmpChisq=0.0
  nDof+=1.0
  chisq+=tmpChisq

  print "waht ... ", reg, reg.find("EM")

  if isEl:
    elVals.m_nObs.push_back(nObs)
    elVals.m_nObs_eStat.push_back(nObsErr)
    elVals.m_nPred.push_back(nPred)
    elVals.m_nPred_eFit.push_back(nPred_eFit)
    elVals.m_Delta.push_back(Delta)
    elVals.m_Delta_eTot.push_back(totDeltaErr)
    elVals.m_reg_names.push_back(regToPrint)
    nDof_el+=1.0
    chisq_el+=tmpChisq
  elif isMu:
    muVals.m_reg_names.push_back(regToPrint)
    muVals.m_nObs.push_back(nObs)
    muVals.m_nObs_eStat.push_back(nObsErr)
    muVals.m_nPred.push_back(nPred)
    muVals.m_nPred_eFit.push_back(nPred_eFit)
    muVals.m_Delta.push_back(Delta)
    muVals.m_Delta_eTot.push_back(totDeltaErr)
    nDof_mu+=1.0
    chisq_mu+=tmpChisq
  elif isEM:
    EMVals.m_reg_names.push_back(regToPrint)
    EMVals.m_nObs.push_back(nObs)
    EMVals.m_nObs_eStat.push_back(nObsErr)
    EMVals.m_nPred.push_back(nPred)
    EMVals.m_nPred_eFit.push_back(nPred_eFit)
    EMVals.m_Delta.push_back(Delta)
    EMVals.m_Delta_eTot.push_back(totDeltaErr)
    nDof_em+=1.0
    chisq_em+=tmpChisq
  pass

ValidationUtils.PullPlot5(elVals,muVals,EMVals,outputFileName)

print "\n * * * Chi^2 / ndof * * *"
if nDof_el: print "Chi^2/nDof electron: %f / %i = %f"%(chisq_el,nDof_el,chisq_el/nDof_el)
if nDof_mu: print "Chi^2/nDof muon:     %f / %i = %f"%(chisq_mu,nDof_mu,chisq_mu/nDof_mu)
if nDof_em: print "Chi^2/nDof em:       %f / %i = %f"%(chisq_em,nDof_em,chisq_em/nDof_em)
print "Chi^2/nDof all:      %f / %i = %f"%(chisq,   nDof   ,chisq/nDof)
print "\n"

if runInterpreter:
  from code import InteractiveConsole
  cons = InteractiveConsole(locals())
  cons.interact("Continuing interactive session... press Ctrl+d to exit")
