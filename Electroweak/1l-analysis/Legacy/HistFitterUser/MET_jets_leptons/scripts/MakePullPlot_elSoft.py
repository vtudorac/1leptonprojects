#!/usr/bin/env python

from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet, RooFormulaVar, RooAbsData, RooRandom 
from ROOT import Util, TMath, TMap, RooExpandedFitResult

from ResultsTableTex import *
import os
import sys

# Main function calls are defined below.

def latexfitresults(filename = os.environ['SUSYFITTER'] + '/results/Fit_SR1a__Background_combined_BasicMeasurement_model_afterFit.root',resultname='RooFitResult_obsData_fitRegions_WR_nJet_TR_nJet',regionList='VR1', dataname='obsData'):

  w = Util.GetWorkspaceFromFile(filename,'w')

  if w==None:
    print "ERROR : Cannot open workspace : ", workspacename
    sys.exit(1) 
    
  result = w.obj(resultname)

  if result==None:
    print "ERROR : Cannot open fit result : ", resultname
    sys.exit(1)

  snapshot =  'snapshot_paramsVals_' + resultname
  w.loadSnapshot(snapshot)
  
  if not w.loadSnapshot(snapshot):
    print "ERROR : Cannot load snapshot : ", snapshot
    sys.exit(1)

    
  data_set = w.data(dataname)
  if data_set==None:
    print "ERROR : Cannot open dataset : ", "data_set"+suffix
    sys.exit(1)
      
  regionCat = w.obj("channelCat")
  data_set.table(regionCat).Print("v");

  regionFullNameList = [ Util.GetFullRegionName(regionCat, region) for region in regionList]
  print regionFullNameList

  ###

  tablenumbers = {}
  tablenumbers['names'] = regionList 
 
  regionCatList = [ 'channelCat==channelCat::' +region.Data() for region in regionFullNameList]
  
  regionDatasetList = [data_set.reduce(regioncat) for regioncat in regionCatList]
  for index, data in  enumerate(regionDatasetList):
    data.SetName("data_" + regionList[index])
    data.SetTitle("data_" + regionList[index])
    
  nobs_regionList = [ data.sumEntries() for data in regionDatasetList]
  tablenumbers['nobs'] = nobs_regionList
  
  ####
  bkginRegionList = [ Util.GetComponent(w,"SingleTop,PowhegPythiaTTbar,QCD,SherpaW,SherpaZ,AlpgenDY,ttbarV,SherpaDiboson",region) for region in regionList]
  nbkginRegionList = [  bkginRegion.getVal() for bkginRegion in bkginRegionList]
  [region.Print() for region in bkginRegionList]
  print nbkginRegionList

  nbkgerrinRegionList = [ Util.GetPropagatedError(bkginRegion, result)  for bkginRegion in bkginRegionList]
  print nbkgerrinRegionList
  
  tablenumbers['Fitted_bkg_events']    =  nbkginRegionList
  tablenumbers['Fitted_bkg_events_err']    =  nbkgerrinRegionList
  
  return tablenumbers




##################################
##################################
##################################



runInterpreter = False
skipStatErr=False
outputFileName="default"


from ROOT import XtraValues,ValidationUtils
from math import sqrt
from RegionsDic import regDic


#chnList = ['SR3j','SR5j','SR1a','SR1b','SR2a','SR2b','SR2l']
#chnList = ['SR3j','SR5j','SR2l']
chnList = ['SR3j','SR5j']

#whichRegion = 'CR'
whichRegion = 'VR'

if whichRegion=='VR': iffitList = ['after']
if whichRegion=='CR': iffitList = ['before','after']
 
nDof=0.0
chisq=0.0
outVals= XtraValues()

for chn in chnList:
  if chn=='SR3j':
    SRname = 'SR1L3j'
    if whichRegion=='VR': regionsList_Digit = ['VRelW3j1', 'VRelW3j2', 'VRelW3j3','VRelT3j1', 'VRelT3j2', 'VRelT3j3']
    if whichRegion=='CR': regionsList_Digit = ['CRelT', 'CRelW'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SR3j__Background/Fit_SR3j__Background_combined_BasicMeasurement_model_afterFit.root'
    pass

  if chn=='SR5j':
    SRname = 'SR1L5j'
    if whichRegion=='VR': regionsList_Digit = ['VRelW5j1', 'VRelW5j2', 'VRelW5j3','VRelT5j1', 'VRelT5j2', 'VRelT5j3']
    if whichRegion=='CR': regionsList_Digit = ['CRelT','CRelW'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SR5j__Background/Fit_SR5j__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR1a':
    SRname = 'SR1L2Ba'
    if whichRegion=='VR': regionsList_Digit = ['CRelWbb','VRel1', 'VRel2']
    if whichRegion=='CR': regionsList_Digit = ['CRelT', 'CRelW'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SR1a__Background/Fit_SR1a__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR1b':
    SRname = 'SR1L2Bc'
    if whichRegion=='VR': regionsList_Digit = ['CRelWbb','VRel1', 'VRel2']
    if whichRegion=='CR': regionsList_Digit = ['CRelT', 'CRelW'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SR1b__Background/Fit_SR1b__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR2a':
    SRname = 'SR1L1Ba'
    if whichRegion=='VR': regionsList_Digit = ['CRelWbb','VRel1', 'VRel2']
    if whichRegion=='CR': regionsList_Digit = ['CRelT', 'CRelW'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SR2a__Background/Fit_SR2a__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR2b':
    SRname = 'SR1L1Bc'
    if whichRegion=='VR': regionsList_Digit = ['CRelWbb','VRel1', 'VRel2']
    if whichRegion=='CR': regionsList_Digit = ['CRelT', 'CRelW'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SR2b__Background/Fit_SR2b__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR2l':
    SRname = 'SR2L'
    if whichRegion=='VR': regionsList_Digit = ['VR1', 'VR2', 'VR3']
    if whichRegion=='CR': regionsList_Digit = ['CRT'] 
    wsFileName= 'results_em/SoftLeptonMoriond2013_SRs2L__Background/Fit_SRs2L__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  #######################################################################################
  print "chn=",chn
  print "wsFileName=",wsFileName
  print "regionsList_Digit=",regionsList_Digit
  #######################################################################################
  for reg in regionsList_Digit:
    print "reg=",reg
    for  iffit in iffitList:
      
      import pickle
      if wsFileName.endswith(".pickle"):
        f = open(wsFileName, 'r')
        m3 = pickle.load(f)
        f.close()
      else:
        if  iffit=="before" : m3 = latexfitresults(wsFileName,'RooExpandedFitResult_beforeFit',regionsList_Digit)
        if  iffit=="after" : m3 = latexfitresults(wsFileName,'RooExpandedFitResult_afterFit',regionsList_Digit)
        print m3
        f = open(chn+".pickle", 'w')
        pickle.dump(m3, f)
        f.close()
        pass
      
      regToPrint=iffit+':'+SRname+'_'+reg  ##regDic[reg]   ## before/after
      #regToPrint=SRname+'_'+reg  ##regDic[reg]

      i=m3['names'].index(reg)
      nObs=m3['nobs'][i]
      nObsErr=0.0
      if nObs>0.0: nObsErr=sqrt(nObs)
      nPred = m3['Fitted_bkg_events'][i]
      nPred_eFit = m3['Fitted_bkg_events_err'][i]
      Delta=nObs-nPred
      if skipStatErr:
        totDeltaErr=nPred_eFit
      else:
        totDeltaErr=sqrt((nPred_eFit*nPred_eFit) + nPred)
        pass
      if totDeltaErr!=0.0:
        tmpChisq=(Delta/totDeltaErr)*(Delta/totDeltaErr)
      else:
        tmpChisq=0.0
        pass
      nDof+=1.0
      chisq+=tmpChisq
      
      print regToPrint,": nObs=",nObs," nObsErr=",nObsErr," nPred=",nPred," nPred_eFit=",nPred_eFit," Delta=",Delta," totDeltaErr=",totDeltaErr
      outVals.m_nObs.push_back(nObs)
      outVals.m_nObs_eStat.push_back(nObsErr)
      outVals.m_nPred.push_back(nPred)
      outVals.m_nPred_eFit.push_back(nPred_eFit)
      outVals.m_Delta.push_back(Delta)
      outVals.m_Delta_eTot.push_back(totDeltaErr)
      outVals.m_reg_names.push_back(regToPrint)



      
print "Chi^2/nDof all:      %f / %i = %f"%(chisq,   nDof   ,chisq/nDof)
 

#if whichRegion=='VR': outputFileName = 'Stop_VR_combined_before_after'
if whichRegion=='VR': outputFileName = 'ELEC_VR_combined_after'
if whichRegion=='CR': outputFileName = 'ELEC_CR_combined_before_after'


## it will plot color pair only when the naming is with "_before_after" ##

ValidationUtils.PullPlot4(outVals,outputFileName)

#print "\n * * * Chi^2 / ndof * * *"
#if nDof_el: print "Chi^2/nDof electron: %f / %i = %f"%(chisq_el,nDof_el,chisq_el/nDof_el)
#if nDof_mu: print "Chi^2/nDof muon:     %f / %i = %f"%(chisq_mu,nDof_mu,chisq_mu/nDof_mu)
#if nDof_em: print "Chi^2/nDof em:       %f / %i = %f"%(chisq_em,nDof_em,chisq_em/nDof_em)
#print "Chi^2/nDof all:      %f / %i = %f"%(chisq,   nDof   ,chisq/nDof)
#print "\n"

if runInterpreter:
  from code import InteractiveConsole
  cons = InteractiveConsole(locals())
  cons.interact("Continuing interactive session... press Ctrl+d to exit")
