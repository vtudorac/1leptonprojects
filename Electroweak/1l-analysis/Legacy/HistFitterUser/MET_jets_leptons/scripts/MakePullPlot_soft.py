#!/usr/bin/env python

from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet, RooFormulaVar, RooAbsData, RooRandom 
from ROOT import Util, TMath, TMap, RooExpandedFitResult

from ResultsTableTex import *
import os
import sys

# Main function calls are defined below.

def latexfitresults(filename = os.environ['SUSYFITTER'] + '/results/Fit_SR1a__Background_combined_BasicMeasurement_model_afterFit.root',resultname='RooFitResult_obsData_fitRegions_WR_nJet_TR_nJet',regionList='VR1', dataname='obsData'):

  w = Util.GetWorkspaceFromFile(filename,'w')

  if w==None:
    print "ERROR : Cannot open workspace : ", workspacename
    sys.exit(1) 
    
  result = w.obj(resultname)

  if result==None:
    print "ERROR : Cannot open fit result : ", resultname
    sys.exit(1)

  snapshot =  'snapshot_paramsVals_' + resultname
  w.loadSnapshot(snapshot)

  if not w.loadSnapshot(snapshot):
    print "ERROR : Cannot load snapshot : ", snapshot
    sys.exit(1)

    
  data_set = w.data(dataname)
  if data_set==None:
    print "ERROR : Cannot open dataset : ", "data_set"+suffix
    sys.exit(1)
      
  regionCat = w.obj("channelCat")
  data_set.table(regionCat).Print("v");

  regionFullNameList = [ Util.GetFullRegionName(regionCat, region) for region in regionList]
  print regionFullNameList

  ###

  tablenumbers = {}
  tablenumbers['names'] = regionList 
 
  regionCatList = [ 'channelCat==channelCat::' +region.Data() for region in regionFullNameList]
  
  regionDatasetList = [data_set.reduce(regioncat) for regioncat in regionCatList]
  for index, data in  enumerate(regionDatasetList):
    data.SetName("data_" + regionList[index])
    data.SetTitle("data_" + regionList[index])
    
  nobs_regionList = [ data.sumEntries() for data in regionDatasetList]
  tablenumbers['nobs'] = nobs_regionList
 
  ####

  bkginRegionList = [ Util.GetComponent(w,"SingleTop,PowhegPythiaTTbar,QCD,AlpgenW,AlpgenZ,AlpgenDY,ttbarV,SherpaDiboson",region) for region in regionList]
  nbkginRegionList = [  bkginRegion.getVal() for bkginRegion in bkginRegionList]
  [region.Print() for region in bkginRegionList]
  print nbkginRegionList

  nbkgerrinRegionList = [ Util.GetPropagatedError(bkginRegion, result)  for bkginRegion in bkginRegionList]
  print nbkgerrinRegionList

  tablenumbers['Fitted_bkg_events']    =  nbkginRegionList
  tablenumbers['Fitted_bkg_events_err']    =  nbkgerrinRegionList
  
  return tablenumbers




##################################
##################################
##################################

if __name__ == "__main__":

  runInterpreter = False
  skipStatErr=False
  outputFileName="default"
  
  import os, sys
  import getopt
  def usage():
    print "Usage:"
    print "YieldsTable.py [-i] [-s] [-o outputFileName] [-c channels] [-w workspace_afterFit] \n"
    print "\nFor example:"
    print "./YieldsTable.py -c SR3jTEl,SR3jTMu,SR4jTEl,SR4jTMu -w /afs/cern.ch/user/c/cote/susy0/users/cote/HistFitter5/results/Combined_KFactorFit_5Channel_bkgonly_combined_BasicMeasurement_model_afterFit.root -o MyTableSR.tex"
    print "./YieldsTable.py -c SR7jTEl,SR7jTMu -w /afs/cern.ch/user/c/cote/susy0/users/cote/HistFitter5/results/Combined_KFactorFit_5Channel_bkgonly_combined_BasicMeasurement_model_afterFit.root -o MyTableSR7j.tex"
    print "./YieldsTable.py -c S2eeT,S2mmT,S2emT,S4eeT,S4mmT,S4emT -w /afs/cern.ch/user/c/cote/susy0/users/cote/HistFitter5/results/Combined_KFactorFit_5Channel_bkgonly_combined_BasicMeasurement_model_afterFit.root -o MyTableDilep.tex"
    print "\n-i starts interactive session"
    print "-s skips stat error. Use for control regions used by the fit. (Default: %s)"%skipStatErr
    sys.exit(0)        

  wsFileName='results/SoftLeptonMoriond2013_SRs1L__Background/Fit_SRs1L__Background_combined_BasicMeasurement_model_afterFit.root'
  try:
    opts, args = getopt.getopt(sys.argv[1:], "iso:c:w:")
  except:
    usage()
  if len(opts)<2:
    usage()

  for opt,arg in opts:
    if opt == '-c':
      chanStr=arg.replace(",","_")
      chanList=arg.split(",")
    elif opt == '-w':
      wsFileName=arg
    elif opt == '-o':
      outputFileName=arg
    elif opt == '-i':
      runInterpreter = True
    elif opt == '-s':
      skipStatErr = True

if outputFileName=="default":
  outputFileName=chanStr+"_AsPull"
  pass


regionsList_1Digit = chanList
print "chanList=",chanList
print "regionsList_1Digit=",regionsList_1Digit

print "wsFileName=",wsFileName

import pickle
if wsFileName.endswith(".pickle"):
  f = open(wsFileName, 'r')
  m3 = pickle.load(f)
  f.close()
else:
  m3 = latexfitresults(wsFileName,'RooExpandedFitResult_afterFit',regionsList_1Digit)
  print m3
  f = open(outputFileName+".pickle", 'w')
  pickle.dump(m3, f)
  f.close()


from ROOT import XtraValues,ValidationUtils
from math import sqrt
from RegionsDic import regDic

elVals = XtraValues()
#muVals = XtraValues()
nDof=0.0
#nDof_el=0.0
#nDof_mu=0.0
#nDof_em=0.0
chisq=0.0
#chisq_el=0.0
#chisq_mu=0.0
#chisq_em=0.0

for reg in regionsList_1Digit:
  print "reg=",reg
  regToPrint=reg  ##regDic[reg]
  outVals=elVals 

  i=m3['names'].index(reg)
  nObs=m3['nobs'][i]
  nObsErr=0.0
  if nObs>0.0: nObsErr=sqrt(nObs)
  nPred = m3['Fitted_bkg_events'][i]
  nPred_eFit = m3['Fitted_bkg_events_err'][i]
  Delta=nObs-nPred
  if skipStatErr:
    totDeltaErr=nPred_eFit
  else:
    totDeltaErr=sqrt((nPred_eFit*nPred_eFit) + nPred)

  if totDeltaErr!=0.0:
    tmpChisq=(Delta/totDeltaErr)*(Delta/totDeltaErr)
  else:
    tmpChisq=0.0
  nDof+=1.0
  chisq+=tmpChisq

  outVals.m_nObs.push_back(nObs)
  outVals.m_nObs_eStat.push_back(nObsErr)
  outVals.m_nPred.push_back(nPred)
  outVals.m_nPred_eFit.push_back(nPred_eFit)
  outVals.m_Delta.push_back(Delta)
  outVals.m_Delta_eTot.push_back(totDeltaErr)
  outVals.m_reg_names.push_back(regToPrint)
  print "Chi^2/nDof all:      %f / %i = %f"%(chisq,   nDof   ,chisq/nDof)
  pass











ValidationUtils.PullPlot4(outVals,outputFileName)

#print "\n * * * Chi^2 / ndof * * *"
#if nDof_el: print "Chi^2/nDof electron: %f / %i = %f"%(chisq_el,nDof_el,chisq_el/nDof_el)
#if nDof_mu: print "Chi^2/nDof muon:     %f / %i = %f"%(chisq_mu,nDof_mu,chisq_mu/nDof_mu)
#if nDof_em: print "Chi^2/nDof em:       %f / %i = %f"%(chisq_em,nDof_em,chisq_em/nDof_em)
#print "Chi^2/nDof all:      %f / %i = %f"%(chisq,   nDof   ,chisq/nDof)
#print "\n"

if runInterpreter:
  from code import InteractiveConsole
  cons = InteractiveConsole(locals())
  cons.interact("Continuing interactive session... press Ctrl+d to exit")
