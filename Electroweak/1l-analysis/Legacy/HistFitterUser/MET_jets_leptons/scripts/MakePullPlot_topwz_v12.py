#!/usr/bin/env python

from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet, RooFormulaVar, RooAbsData, RooRandom 
from ROOT import Util, TMath, TMap, RooExpandedFitResult

from ResultsTableTex import *
import os
import sys

# Main function calls are defined below.
  

def latexfitresults( filename, resultName="RooExpandedFitResult_afterFit", outName="test.tex" ):

  

  ############################################
  workspacename = 'w'
  w = Util.GetWorkspaceFromFile(filename,workspacename)

  if w==None:
    print "ERROR : Cannot open workspace : ", workspacename
    sys.exit(1) 

  result = w.obj(resultName)
  if result==None:
    print "ERROR : Cannot open fit result ", resultName
    sys.exit(1)

  #####################################################

  regSys = {}

  # calculate error per parameter on  fitresult
  fpf = result.floatParsFinal() 
  fpi = result.floatParsInit()

  '''
  // P r i n t   l a t ex   t a b l e   o f   p a r a m e t e r s   o f   p d f 
  // --------------------------------------------------------------------------


  // Print parameter list in LaTeX for (one column with names, one column with values)
  params->printLatex() ;

  // Print parameter list in LaTeX for (names values|names values)
  params->printLatex(Columns(2)) ;

  // Print two parameter lists side by side (name values initvalues)
  params->printLatex(Sibling(*initParams)) ;

  // Print two parameter lists side by side (name values initvalues|name values initvalues)
  params->printLatex(Sibling(*initParams),Columns(2)) ;

  // Write LaTex table to file
  params->printLatex(Sibling(*initParams),OutputFile("rf407_latextables.tex")) ;
  '''

  ####fpf.printLatex(RooFit.Format("NE",RooFit.AutoPrecision(2),RooFit.VerbatimName()),RooFit.Sibling(fpi),RooFit.OutputFile(outName)) 

  # set all floating parameters constant
  for idx in range(fpf.getSize()):
    parname = fpf[idx].GetName()
    ip = fpi[idx]
    ipv  = ip.getVal()
    ipe  = ip.getError()
    ipel = ip.getErrorLo()
    ipeh = ip.getErrorHi()

    fp = fpf[idx]
    fpv  = fp.getVal()
    fpe  = fp.getError()
    fpel = fp.getErrorLo()
    fpeh = fp.getErrorHi()

    name = parname
    

    ##regSys[name] = (ipv,ipe,ipel,ipeh,fpv,fpe,fpel,fpeh)
    regSys[name] = (fpv,fpe)
    #regSys[name] = (fpv,ipe)

  return regSys



##################################
##################################
##################################



runInterpreter = False
skipStatErr=False
outputFileName="default"


from ROOT import XtraValues,ValidationUtils
from math import sqrt
from RegionsDic import regDic



iffitList = ['after']   ## before fit mu_wz/top is always 1.

#whichName = 'mu_WZ'   ## not for SR2l
#chnList = ['SR3jIncl','SR3j','SR5j','SR1a','SR1b','SR2a','SR2b']

whichName = 'mu_Top'   
chnList = ['SR3jIncl','SR3j','SR5j','SR1a','SR1b','SR2a','SR2b','SRs2La']

#chnList = ['SR3jIncl','SR3j','SR5j']

nDof=0.0
chisq=0.0
outVals= XtraValues()

for chn in chnList:
  if chn=='SR3jIncl':
    SRname = 'SR1L3jIncl'
    wsFileName= 'results/Fit_s1L_SR3jIncl__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR3j':
    SRname = 'SR1L3j'
    wsFileName= 'results/Fit_s1L_SR3j__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR5j':
    SRname = 'SR1L5j'
    wsFileName= 'results/Fit_s1L_SR5j__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR1a':
    SRname = 'SR1L2Ba'
    wsFileName= 'results/Fit_SR1a__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR1b':
    SRname = 'SR1L2Bc'
    wsFileName= 'results/Fit_SR1b__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR2a':
    SRname = 'SR1L1Ba'
    wsFileName= 'results/Fit_SR2a__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SR2b':
    SRname = 'SR1L1Bc'
    wsFileName= 'results/Fit_SR2b__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SRs2La':
    SRname = 'SR2La'
    wsFileName= 'results/Fit_SRs2La__Background_combined_BasicMeasurement_model_afterFit.root'
    pass
  if chn=='SRs2Lb':
    SRname = 'SR2Lb'
    wsFileName= 'results/Fit__SRs2Lb__Background_combined_BasicMeasurement_model_afterFit.root'
    pass

  #######################################################################################
  #print "chn=",chn
  #print "wsFileName=",wsFileName
  #######################################################################################
  for  iffit in iffitList:
    
    import pickle
    if wsFileName.endswith(".pickle"):
      f = open(wsFileName, 'r')
      m3 = pickle.load(f)
      f.close()
    else:
      if  iffit=="before" : m3 = latexfitresults(wsFileName,'RooExpandedFitResult_beforeFit',"default")  ## before fit mu_wz/top is always 1.
      if  iffit=="after" : m3 = latexfitresults(wsFileName,'RooExpandedFitResult_afterFit',"default")  ###
        #print m3
      f = open(chn+".pickle", 'w')
      pickle.dump(m3, f)
      f.close()
      pass
      
      #regToPrint=iffit+':'+chn+'_'+reg  ##regDic[reg]
    regToPrint=SRname #reg  ##regDic[reg]
      #print  "m3:",m3
      #print  'mu_WZ : ',m3['mu_WZ'][0],' err=',m3['mu_WZ'][1]
      #print  'mu_Top : ',m3['mu_Top'][0],' err=',m3['mu_Top'][1]

    
    nObs=m3[whichName][0]   
    totDeltaErr=m3[whichName][1]  
    nPred = 1. 
   
    #print regToPrint,": nObs=",nObs," nPred=",nPred," totDeltaErr=",totDeltaErr
    if whichName == 'mu_WZ': print regToPrint," & W(Z)+jets yield & $ \phantom{-} %6.2f   \pm    %6.2f  $ \\\\" %(nObs,totDeltaErr)
    if whichName == 'mu_Top': print regToPrint," & ttbar yield & $ \phantom{-} %6.2f   \pm    %6.2f  $  \\\\" %(nObs,totDeltaErr)

    #if whichName == 'mu_WZ': print regToPrint,": W(Z)+jets yield: %6.2f   +/-    %6.2f  " %(nObs,totDeltaErr)
    #if whichName == 'mu_Top': print regToPrint,": ttbar yield: %6.2f   +/-    %6.2f  " %(nObs,totDeltaErr)


    outVals.m_nObs.push_back(nObs)
    outVals.m_nPred.push_back(nPred)
    outVals.m_Delta_eTot.push_back(totDeltaErr)
    outVals.m_reg_names.push_back(regToPrint)


if whichName=='mu_WZ': outputFileName = 'pullplot_mu_WZ'
if whichName=='mu_Top': outputFileName = 'pullplot_mu_Top'

ValidationUtils.PullPlot4(outVals,outputFileName)

#print "\n * * * Chi^2 / ndof * * *"
#if nDof_el: print "Chi^2/nDof electron: %f / %i = %f"%(chisq_el,nDof_el,chisq_el/nDof_el)
#if nDof_mu: print "Chi^2/nDof muon:     %f / %i = %f"%(chisq_mu,nDof_mu,chisq_mu/nDof_mu)
#if nDof_em: print "Chi^2/nDof em:       %f / %i = %f"%(chisq_em,nDof_em,chisq_em/nDof_em)
#print "Chi^2/nDof all:      %f / %i = %f"%(chisq,   nDof   ,chisq/nDof)
#print "\n"

if runInterpreter:
  from code import InteractiveConsole
  cons = InteractiveConsole(locals())
  cons.interact("Continuing interactive session... press Ctrl+d to exit")
