#!/bin/env python

import ROOT,sys,os
from ROOT import *
ROOT.SetSignalPolicy( ROOT.kSignalFast )
ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.LoadMacro("/afs/cern.ch/atlas/project/cern/susy2/SPyRoot/susy12a_razor_p1328/HistFitter/macros/AtlasStyle.C")
gROOT.LoadMacro("AtlasStyle.C")
ROOT.SetAtlasStyle()
gROOT.LoadMacro("AtlasLabels.C")

Names={"2Jbin1" : "2J m_{eff}^{bin 1}","2Jbin2" : "2J m_{eff}^{bin 2}","2Jbin3" : "2J m_{eff}^{bin 3}","2Jbin4" : "2J m_{eff}^{bin 4}","4Jlowxbin1" : "4J low-x m_{eff}^{bin 1}","4Jlowxbin2" : "4J low-x m_{eff}^{bin 2}","4Jlowxbin3" : "4J low-x m_{eff}^{bin 3}","4Jhighxbin1" : "4J high-x m_{eff}^{bin 1}","4Jhighxbin2" : "4J high-x m_{eff}^{bin 2}","4Jhighxbin3" : "4J high-x m_{eff}^{bin 3}","6Jbin1" : "6J m_{eff}^{bin 1}","6Jbin2" : "6J m_{eff}^{bin 2}","6Jbin3" : "6J m_{eff}^{bin 3}","6Jbin4" : "6J m_{eff}^{bin 4}"}


if len(sys.argv)>1:
    if sys.argv[1]!='-b': infiles = sys.argv[1]
    elif len(sys.argv)>2: infiles = sys.argv[2]
    else:
        print 'Please specify an input file'
        sys.exit(1)
print 'Using files',infiles


#doSignal = False
#for doSignal in [True,False]:
for doSignal in [False]:
    aset = infiles	      
    muTop = [[],[],[],[]]
    muW = [[],[],[],[]]
    
    #	     log = open( 'logs/test_'+aset+('' if not doSignal else '_signal')+'.log' , 'r' )
    log = open( 'logs/'+aset+('' if not doSignal else '_signal'), 'r' )
    switchOn = False
    muIDX = -1
    for aline in log.readlines():
        if 'FinalValue' in aline:
            switchOn = True
            muTop = [[],[],[],[]]
            muW = [[],[],[],[]]
	    
            continue
        if '------------' in aline: continue
        if switchOn:
	   
            if len(aline.strip())<3:
                switchOn=False
                continue #break
			  


	    #print aline
	    
            if 'mu_Top' in aline:	        
                muTop[0] += [ aline.split()[0].split('_')[2] + aline.split()[0].split('_')[3] ]		
                #print 'mutop', muTop[0]	    
                #print "aline: ", aline		
                brief = (aline.split()[3].split('(')[1].split(')')[0])
               #print brief
		
                muTop[1] += [float(aline.split()[2])]
                muTop[2] += [ abs(float(brief.split(',')[0])) ]
                muTop[3] += [ abs(float(brief.split(',')[1])) ]
                #print muTop[2], muTop[3]
		
            if 'mu_W' in aline:	        
                muW[0] += [ aline.split()[0].split('_')[2] + aline.split()[0].split('_')[3] ]		
                #print 'muW', muW[0]	    
                #print "aline: ", aline		
                brief = (aline.split()[3].split('(')[1].split(')')[0])
                #print brief
                muW[1] += [float(aline.split()[2])]		
                muW[2] += [ abs(float(brief.split(',')[0])) ]
                muW[3] += [ abs(float(brief.split(',')[1])) ]		
                #print muW[2], muW[3]	
		

    if len(muTop[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue
	
    if len(muW[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue	
    
    #print  muTop[1], muTop[2], muTop[3]
    #print muW[1], muW[2], muW[3]
    
    from array import array
    y = array('d',muTop[1])   
    yep = array('d',muTop[2])   
    yem = array('d',muTop[3])     
    x = array('d', [ a for a in xrange( len(muTop[1])) ] )   
    xe = array('d', [ 0.0 for a in xrange( len(muTop[1])) ] )
   
    y1 = array('d',muW[1]) 
    yep1 = array('d',muW[2])
    yem1 = array('d',muW[3])
    x1 = array('d', [ a for a in xrange( len(muW[1])) ] ) 
    xe1 = array('d', [ 0.0 for a in xrange( len(muW[1])) ] )
  
    c = ROOT.TCanvas('NormFactor_'+aset,'',1200,600)
    c.SetBottomMargin(0.42)
    c.SetTopMargin(0.03)
    c.SetRightMargin(0.02)
    c.SetLeftMargin(0.06)
    
    
    
    
    '''
    frame = ROOT.TH2D('frame_'+aset,'',len(muTop[1]),-0.5,len(muTop[1])-0.5,5,0,1.5)
    frame.SetYTitle('mu_Top')
    frame.GetXaxis().SetLabelSize(0.040)
    frame.SetXTitle('')
    frame.Draw()
    frame.GetYaxis().SetTitleSize(0.045)
    frame.GetYaxis().SetTitleOffset(0.6)
    '''
   
    eg = ROOT.TGraphAsymmErrors(len(muTop[1]),x,y,xe,xe,yem,yep)
    #eg.SetFillColor(2)
    #eg.SetFillStyle(3001)
    #eg.Draw('a2')
    #eg.SetMarkerSize(0.3)
    #eg.Draw('sameP')
    #eg.SetMarkerSize(0.6)
    eg.SetLineWidth(1)
    eg.Draw('AP')
    eg.GetYaxis().SetTitle('#mu_{Top}')
    eg.GetYaxis().SetTitleSize(0.045)
    eg.GetYaxis().SetTitleOffset(0.65)
    eg.GetYaxis().SetLabelSize(0.045)
    eg.GetXaxis().SetLabelSize(0.055)
    eg.SetMinimum(0)
    eg.SetMaximum(2.0)
   
    #eg.GetXaxis().SetLimits(0.,len(muTop[1])+1);
    eg.GetXaxis().Set(len(muTop[1]),-0.5,len(muTop[1])-0.5)    
    pone = ROOT.TLine(-0.5,1,len(muTop[1])-0.5,1)    
    pone.SetLineStyle(2)
    pone.SetLineWidth(1)
    pone.SetLineColor (9)
    pone.Draw('')
    eg.Draw('Psame')
      
   
    for abin in xrange(len(muTop[1])):
        eg.GetXaxis().SetBinLabel(abin+1, Names[muTop[0][abin]])
	 
	#print abin+1, muTop[0][abin], muTop[1]
      	
    eg.GetXaxis().LabelsOption('v')
    ATLASLabel(0.11,0.89,"Internal")
   
    l2 = ROOT.TLatex()
    l2.SetNDC()
    l2.SetTextSize(0.035)
    
    l2.DrawLatex(0.11,0.76,"1 e/#mu + jets + E_{T}^{miss}")
    l2.DrawLatex(0.11,0.82,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}")

    
    c.SaveAs('muTopW/muTop_'+(aset.replace('.','_'))+'.eps')
    c.SaveAs('muTopW/muTop_'+(aset.replace('.','_'))+'.png')
    
    
     
    eg1 = ROOT.TGraphAsymmErrors(len(muW[1]),x1,y1,xe1,xe1,yem1,yep1)
    #eg1.SetFillColor(2)
    #eg1.SetFillStyle(3001)
    #eg1.Draw('a2')
    #eg1.SetMarkerSize(0.3)
    eg1.SetLineWidth(1)
    eg1.Draw('AP')
    eg1.GetYaxis().SetTitle('#mu_{W}')
    eg1.GetYaxis().SetTitleSize(0.045)
    eg1.GetYaxis().SetLabelSize(0.045)
    eg1.GetYaxis().SetTitleOffset(0.65)
    eg1.GetXaxis().SetLabelSize(0.055)
    eg1.SetMinimum(0)
    eg1.SetMaximum(2)
    eg1.GetXaxis().Set(len(muW[1]),-0.5,len(muW[1])-0.5)    
    pone1 = ROOT.TLine(-0.5,1,len(muW[1])-0.5,1)    
    pone1.SetLineStyle(2)
    pone1.SetLineWidth(1)
    pone1.SetLineColor (9)
    pone1.Draw('')
    eg1.Draw('Psame')
    
    
    for abin in xrange(len(muW[1])):
        eg1.GetXaxis().SetBinLabel(abin+1, Names[muW[0][abin]])	 
	#print abin+1, muW[0][abin], muW[1]
	
    ATLASLabel(0.11,0.89,"Internal")
   
    l2 = ROOT.TLatex()
    l2.SetNDC()
    l2.SetTextSize(0.035)
    
    l2.DrawLatex(0.11,0.76,"1 e/#mu + jets + E_{T}^{miss}")
    l2.DrawLatex(0.11,0.82,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}")   	
    eg1.GetXaxis().LabelsOption('v')
    
    c.SaveAs('muTopW/muW_'+(aset.replace('.','_'))+'.eps')
    c.SaveAs('muTopW/muW_'+(aset.replace('.','_'))+'.png')
    
    
