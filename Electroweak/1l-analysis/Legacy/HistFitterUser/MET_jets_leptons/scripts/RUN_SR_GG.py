"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils2
from pullPlotUtils2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["SR2JEM"] = "2-jet SR"
    #myRegionDict["SR2JEl"] = "2-jet SR  (e)"
    #myRegionDict["SR2JMu"] = "2-jet SR  (#mu)"
    
    
    myRegionDict["SR6JGGx12EM"] = "GG 6J bulk"
    #myRegionDict["SR6JGGx12El"] = "6-jet SR bulk   (e)"
    #myRegionDict["SR6JGGx12Mu"] = "6-jet SR bulk   (#mu)"
    
    
    
    myRegionDict["SR6JGGx12HMEM"] = "GG 6J high-mass"
    #myRegionDict["SR6JGGx12HMEl"] = "6-jet SR  High Mass (e)"
    #myRegionDict["SR6JGGx12HMMu"] = "6-jet SR  High Mass (#mu)"
    
    myRegionDict["SR4JlowxGGdiscEM"] = "GG 4J low-x"
    myRegionDict["SR4JlowxGGexclEM"] = " GG 4J low-x b-veto"
    
    
        
    
    myRegionDict["SR4JhighxGGEM"] = "GG 4J high-x"
    
    #myRegionDict["SR4JhighxGGEl"] = "4-jet highx (e)"
    #myRegionDict["SR4JhighxGGMu"] = "4-jet highx (#mu)"
    
    
#myRegionDict["VR2J_2El"] = "2-jet VR 2 (e)"                         
#myRegionDict["VR2J_2Mu"] = "2-jet VR 2 (#mu)"                       
    
      
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

   
    regionList += ["SR2JEM"]
    #regionList += ["SR2JEl"]
    #regionList += ["SR2JMu"]
    
    regionList += ["SR4JhighxGGEM"]
    #regionList += ["SR4JhighxGGEl"]
    #regionList += ["SR4JhighxGGMu"]
    
    regionList += ["SR4JlowxGGdiscEM"]
    regionList += ["SR4JlowxGGexclEM"]

    
    regionList += ["SR6JGGx12EM"]
    #regionList += ["SR6JGGx12El"]
    #regionList += ["SR6JGGx12Mu"]
    
    
    regionList += ["SR6JGGx12HMEM"]
    #regionList += ["SR6JGGx12HMEl"]
    #regionList += ["SR6JGGx12HMMu"]
    
    
    
   
    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("4J-jet highx") != -1: return kBlack
    if name.find("4J-jet lowx") != -1: return kBlack 
    if name.find("4J-jet lowx b-veto") != -1: return kBlack 
     
    if name.find("6J-jet SR bulk") != -1: return kBlack 
    if name.find("6J-jet SR high-mass") != -1: return kBlack   
    
    if name.find("2-jet SR") != -1: return kBlack  
            
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "ttbar":         return kGreen - 9
    if sample == "wjets_Sherpa221":       return kAzure + 1
    if sample == "zjets_Sherpa22":     return kOrange
    if sample == "diboson":     return kViolet - 8
    if sample == "singletop":     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils2.getRegionColor = getRegionColor
    pullPlotUtils2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_2J2.pickle",
                       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_2JEl.pickle",
                       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_2JMu.pickle",
    
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_6JGGx122.pickle",
		       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_6JGGx12El.pickle",
		       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_6JGGx12Mu.pickle",
		       
		       
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_6JGGx12HM2.pickle",
		       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_6JGGx12HMEl.pickle",
		       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_6JGGx12HMMu.pickle",
		             
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_4JhighxGG2.pickle",
		       
		       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_4JlowxGGdisc2.pickle",
		       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_4JlowxGGexcl2.pickle",
		       
		       
		       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_4JhighxEl.pickle",
		       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSR_4JhighxMu.pickle",
		       
                     ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "SR_GG"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets_Sherpa22,singletop,diboson,wjets_Sherpa221,ttbar"
    #samples = "ttv,diboson,singletop,ttbar,wjets,zjets"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
