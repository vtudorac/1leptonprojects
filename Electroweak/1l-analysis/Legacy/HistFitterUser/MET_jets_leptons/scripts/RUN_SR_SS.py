
"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils2
from pullPlotUtils2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["SR4JSSlowxEM"] = " SS 4J low-x"
   
 
    myRegionDict["SR4JSSx12EM"] = " SS 4J  x=1/2"
   
    
    myRegionDict["SR5JSShighxEM"] = " SS 5J high-x"
       
    myRegionDict["SR5JSSx12EM"] = "SS 5J x=1/2"
    
    #myRegionDict["SR2JEM"] = "GG/SS 2J"
    
    
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]
#    regionList += ["SR2JEM"]

    regionList += ["SR5JSSx12EM"]
    regionList += ["SR4JSSx12EM"]
    regionList += ["SR5JSShighxEM"]
    regionList += ["SR4JSSlowxEM"]   

    
    
    

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SR4JSSlowx") != -1: return kBlack     
    if name.find("SR4JSSx12") != -1: return kBlack 
    if name.find("SR5JSShighx") != -1: return kBlack        
    if name.find("SR5JSSx12EM") != -1: return kBlack
    if name.find("SR2JEM") != -1: return kBlack 
           
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:     return kGreen - 9
    if "wjets" in sample:     return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson" in sample:   return kViolet - 8
    if "singletop" in sample: return kGreen + 2
    if "ttv" in sample:       return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils2.getRegionColor = getRegionColor
    pullPlotUtils2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 
    '''
    pickleFilename = [#os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/GGquarks/MyTable1LepSR_2J.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/4JSSlowx_SR4JSSlowxEM_YieldTable.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/4JSSx12_SR4JSSx12EM_YieldTable.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/5JSShighx_SR5JSShighxEM_YieldTable.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/5JSSx12_SR5JSSx12EM_YieldTable.pickle",
		      ]
	'''
    pickleFilename = [ #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/GGquarks/MyTable1LepVR_2J.pickle",
                      "tables/MyTable1LepSR_4JSSlowx2.pickle",
                      "tables/MyTable1LepSR_4JSSx122.pickle",
                      "tables/MyTable1LepSR_5JSShighx2.pickle",
                      "tables/MyTable1LepSR_5JSSx122.pickle",
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "SR_squarks"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets_Sherpa22,singletop,diboson,wjets_Sherpa221,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
