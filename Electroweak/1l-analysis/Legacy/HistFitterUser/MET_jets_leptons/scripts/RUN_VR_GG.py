"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils2
from pullPlotUtils2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR2J_1EM"] = "GG 2J VR E_{T}^{miss} "
    myRegionDict["VR2J_2EM"] = "GG 2J VR m_{T} "
    #myRegionDict["VR2J_1EM"] = "2-jet VR 1 "
#myRegionDict["VR2J_1El"] = "2-jet VR 1  (e)"                        
#myRegionDict["VR2J_1Mu"] = "2-jet VR 1 (#mu)"                       
    
    #myRegionDict["VR2J_2EM"] = "2J VR 2 "
#myRegionDict["VR2J_2El"] = "2-jet VR 2 (e)"                         
#myRegionDict["VR2J_2Mu"] = "2-jet VR 2 (#mu)"                       
    
    myRegionDict["VR6JGGx12_mtEM"] = " GG 6J VR bulk m_{T}"
#myRegionDict["VR6JGGx12_mtEl"] = "6-jet bulk VR mt (e)"             
#myRegionDict["VR6JGGx12_mtMu"] = "6-jet bulk VR mt (#mu)"           

    myRegionDict["VR6JGGx12_aplanarityEM"] = "GG 6J  VR bulk  apl"
#myRegionDict["VR6JGGx12_aplanarityEl"] = "6-jet  bulk VR app (e)"   
#myRegionDict["VR6JGGx12_aplanarityMu"] = "6-jet  bulk VR app (#mu)" 
    
    myRegionDict["VR6JGGx12HM_mtEM"] = "6J VR high-mass m_{T}"
#myRegionDict["VR6JGGx12HM_mtEl"] = "6-jet HM VR mt  (e)"            
#myRegionDict["VR6JGGx12HM_mtMu"] = "6-jet HM VR mt  (#mu)"          
    
    myRegionDict["VR6JGGx12HM_aplanarityEM"] = "GG 6J VR high-mass apl"
#myRegionDict["VR6JGGx12HM_aplanarityEl"] = "6-jet  HM VR app (e)"   
#myRegionDict["VR6JGGx12HM_aplanarityMu"] = "6-jet  HM VR app (#mu)" 

    #myRegionDict["VR5J_mtEM"] = "5-jet VR m_{T}"
    #myRegionDict["VR5J_mtEl"] = "5-jet VR m_{T} (e)"    
    #myRegionDict["VR5J_mtMu"] = "5-jet VR m_{T} (#mu)" 
    
    #myRegionDict["VR5J_aplanarityEM"] = "5-jet VR aplan."
    #myRegionDict["VR5J_aplanarityEl"] = "5-jet VR aplan. (e)"    
    #myRegionDict["VR5J_aplanarityMu"] = "5-jet VR aplan. (#mu)"  
    
    #myRegionDict["VR6J_mtEM"] = "6-jet VR m_{T}"
    #myRegionDict["VR6J_mtEl"] = "6-jet VR m_{T} (e)"    
    #myRegionDict["VR6J_mtMu"] = "6-jet VR m_{T} (#mu)" 
    
    #myRegionDict["VR6J_aplanarityEM"] = "6-jet VR aplan."
    #myRegionDict["VR6J_aplanarityEl"] ap= "6-jet VR aplan. (e)"    
    #myRegionDict["VR6J_aplanarityMu"] = "6-jet VR aplan. (#mu)"     
    
    myRegionDict["VR4JlowxGG_mtEM"] = "GG 4J  VR low-x m_{T}"
    #myRegionDict["VR4Jlowx_mtEl"] = "4-jet low-x VR m_{T} (e)"    
    #myRegionDict["VR4Jlowx_mtMu"] = "4-jet low-x VR m_{T} (#mu)" 
    
    myRegionDict["VR4JlowxGG_aplanarityEM"] = "GG 4J VR low-x  apl."
    #myRegionDict["VR4Jlowx_aplanarityEl"] = "4-jet low-x VR aplan. (e)"    
    #myRegionDict["VR4Jlowx_aplanarityMu"] = "4-jet low-x VR aplan. (#mu)"   
    
    myRegionDict["VR4JhighxGG_mtEM"] = "GG 4J VR high-x m_{T}"
    #myRegionDict["VR4Jhighx_mtEl"] = "4-jet high-x VR m_{T} (e)"    
    #myRegionDict["VR4Jhighx_mtMu"] = "4-jet high-x VR m_{T} (#mu)" 
    
    myRegionDict["VR4JhighxGG_metovermeffEM"] = "GG 4J VR high-x E_{T}^{miss}/m_{eff}"
    #myRegionDict["VR4Jhighx_meffEl"] = "4-jet high-x VR E_{T}^{miss}/m_{eff} (e)"    
    #myRegionDict["VR4Jhighx_meffMu"] = "4-jet high-x VR E_{T}^{miss}/m_{eff} (#mu)"  
    
    myRegionDict["VR4JSSx12_mtEM"] = "GG 4J  m_{T}"
    myRegionDict["VR4JSSx12_aplanarityEM"] = "GG 4-jet VR apl."

    myRegionDict["VR5JSSx12_mtEM"] = "GG 5J VR m_{T}"
    myRegionDict["VR5JSSx12_metEM"] = "GG 5J VR E_{T}^{miss}"

    #myRegionDict["VR2J_1EM"] = "2-jet soft-lepton VR (E_{T}^{miss})"       
    #myRegionDict["VR2J_2EM"] = "2-jet soft-lepton VR (m_{T})"    
    
    #myRegionDict["VR5J_1EM"] = "5-jet soft-lepton VR (E_{T}^{miss})"       
    #myRegionDict["VR5J_2EM"] = "5-jet soft-lepton VR (H_{T})"            
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    
    regionList += ["VR2J_1EM"]
    regionList += ["VR2J_2EM"]

    regionList += ["VR6JGGx12_mtEM"]
    regionList += ["VR6JGGx12_aplanarityEM"]

    regionList += ["VR6JGGx12HM_mtEM"]
    regionList += ["VR6JGGx12HM_aplanarityEM"]

    regionList += ["VR4JhighxGG_mtEM"]
    regionList += ["VR4JhighxGG_metovermeffEM"]
      
    regionList += ["VR4JlowxGG_mtEM"]
    regionList += ["VR4JlowxGG_aplanarityEM"] 

    #regionList += ["VR4JSSx12_mtEM_cuts"]
    #regionList += ["VR4JSSx12_aplanarityEM_cuts"]

    #regionList += ["VR5JSSx12_mtEM_cuts"]
    #regionList += ["VR5JSSx12_metEM_cuts"]

    #regionList += ["VR5J_mtEM_cuts"]
    #regionList += ["VR5J_aplanarityEM_cuts"] 
    
    #regionList += ["VR6J_mtEM_cuts"]
    #regionList += ["VR6J_aplanarityEM_cuts"] 
    #regionList += ["SR1sl2j_cuts","SLVR2_nJet","SSloose_metmeff2Jet"]
    #regionList += ["SS_metmeff2Jet"]
    
    #regionList += ["VR2J_1EM"]
    #regionList += ["VR2J_2EM"]   
       
    #regionList += ["VR5J_1EM"]
    #regionList += ["VR5J_2EM"]  

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("5-jet soft-lepton") != -1: return kBlack   
    if name.find("4-jet high-x VR E") != -1: return kBlack  
    if name.find("4-jet high-x VR a") != -1: return kBlack      
    if name.find("4-jet low-x VR m") != -1: return kBlack     
    if name.find("4-jet low-x VR a") != -1: return kBlack  

    if name.find("2-jet VR 1") != -1: return kBlack  
    if name.find("2-jet VR 2") != -1: return kBlack  
    if name.find("6-jet bulk VR mt") != -1: return kBlack  
    if name.find("6-jet  bulk VR apl") != -1: return kBlack  
    if name.find("6-jet HM VR mt") != -1: return kBlack
    if name.find("6-jet  HM VR apl") != -1: return kBlack  
    if name.find("4-jet") != -1: return kBlack
    if name.find("2-jet") != -1: return kBlack         
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "ttbar":         return kGreen - 9
    if sample == "wjets_Sherpa221":       return kAzure + 1
    if sample == "zjets_Sherpa22":     return kOrange
    if sample == "diboson":     return kViolet - 8
    if sample == "singletop":     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils2.getRegionColor = getRegionColor
    pullPlotUtils2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_2J.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_6JGGx12.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_6JGGx12HM.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_4JhighxGG.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_4JlowxGG.pickle",
                       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_4JSSx12.pickle",
                       #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR_5JSSx12.pickle",
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VR_GG"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets_Sherpa22,singletop,diboson,wjets_Sherpa221,ttbar"
    #samples = "zjets,singletop,diboson,wjets,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
