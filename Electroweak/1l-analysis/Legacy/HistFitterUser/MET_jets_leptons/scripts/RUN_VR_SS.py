
"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils2
from pullPlotUtils2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    #myRegionDict["VR2J_1EM"] = "GG/SS 2J VR E_{T}^{miss} "
    #myRegionDict["VR2J_2EM"] = "GG/SS 2J VR m_{T} "
    myRegionDict["VR4JSSlowx_aplanarityEM"] = "SS 4J VR low-x apl"
    myRegionDict["VR4JSSlowx_mtEM"] = "SS 4J VR low-x m_{T}"
    #myRegionDict["VR4JSSlowx_DR"] = "SS 4J VR low-x DR"
    
    #myRegionDict["VR2J_1El"] = "2-jet VR 1  (e)"    
    #myRegionDict["VR2J_1Mu"] = "2-jet VR 1 (#mu)" 
    
    myRegionDict["VR4JSSx12_aplanarityEM"] = " SS 4J VR x=1/2 apl"
    myRegionDict["VR4JSSx12_mtEM"] = " SS 4J VR x=1/2 m_{T}"
    #myRegionDict["VR4JSSx12_DR"] = " SS 4J  VR x=1/2 DR"
    
    #myRegionDict["VR2J_2El"] = "2-jet VR 2 (e)"    
    #myRegionDict["VR2J_2Mu"] = "2-jet VR 2 (#mu)" 
    
  
    
    myRegionDict["VR5JSShighx_aplanarityEM"] = " SS 5J VR high-x apl"
    myRegionDict["VR5JSShighx_mtEM"] = " SS 5J  VR high-x m_{T}"
    #myRegionDict["VR5JSShighx_DR"] = " SS 5J VR high-x DR"
    
    #myRegionDict["VR6JGGx12_mtEl"] = "6-jet bulk VR mt (e)"    
    #myRegionDict["VR6JGGx12_mtMu"] = "6-jet bulk VR mt (#mu)" 
    
    myRegionDict["VR5JSSx12_metEM"] = " SS 5J  VR x=1/2 E_{T}^{miss}"
    myRegionDict["VR5JSSx12_mtEM"] = "SS 5J x=1/2 m_{T}"
    #myRegionDict["VR5JSSx12_DR"] = "SS 5J x=1/2 DR"
    
    #myRegionDict["VR6JGGx12_aplanarityEl"] = "6-jet  bulk VR app (e)"    
    #myRegionDict["VR6JGGx12_aplanarityMu"] = "6-jet  bulk VR app (#mu)"
    
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]
#    regionList += ["VR2J_1EM"]
#    regionList += ["VR2J_2EM"]

    regionList += ["VR5JSSx12_metEM"]
    regionList += ["VR5JSSx12_mtEM"]
    #regionList += ["VR5JSSx12_DR"]
    
    regionList += ["VR4JSSx12_aplanarityEM"]
    regionList += ["VR4JSSx12_mtEM"]
    #regionList += ["VR4JSSx12_DR"]
    
    
    regionList += ["VR5JSShighx_aplanarityEM"]
    regionList += ["VR5JSShighx_mtEM"]
    #regionList += ["VR5JSShighx_DR"]
    
    regionList += ["VR4JSSlowx_aplanarityEM"]
    regionList += ["VR4JSSlowx_mtEM"]
    #regionList += ["VR4JSSlowx_DR"]
    
    
    

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("2-jet VR 1") != -1: return kBlack  
    if name.find("2-jet VR 2") != -1: return kBlack
      
    if name.find("VR4JSSlowx_aplanarityEM") != -1: return kGreen+3 
    if name.find("VR4JSSlowx_mtEM") != -1: return kGreen+3 
    if name.find("VR4JSSlowx_DR") != -1: return kGreen+3 
    
       
    if name.find("VR5JSShighx_aplanarityEM") != -1: return kGreen+3
    if name.find("VR5JSShighx_mtEM") != -1: return kGreen+3
    if name.find("VR5JSShighx_DR") != -1: return kGreen+3
    
    
    if name.find("VR5JSSx12_metEM") != -1: return kGreen+3 
    if name.find("VR5JSSx12_mtEM") != -1: return kGreen+3
    if name.find("VR5JSSx12_DR") != -1: return kGreen+3
     
       
    if name.find("VR4JSSx12_aplanarityEM") != -1: return kGreen+3
    if name.find("VR4JSSx12_mtEM") != -1: return kGreen+3
    if name.find("VR4JSSx12_DR") != -1: return kGreen+3
    
    
         
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:     return kGreen - 9
    if "wjets" in sample:     return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson" in sample:   return kViolet - 8
    if "singletop" in sample: return kGreen + 2
    if "ttv" in sample:       return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils2.getRegionColor = getRegionColor
    pullPlotUtils2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 
    '''
    pickleFilename = [os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/4JSSlowx_VR4JSSlowx_mtEM_VR4JSSlowx_aplanarityEM_YieldTable.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/4JSSx12_VR4JSSx12_mtEM_VR4JSSx12_aplanarityEM_YieldTable.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/5JSShighx_VR5JSShighx_mtEM_VR5JSShighx_aplanarityEM_YieldTable.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/5JSSx12_VR5JSSx12_mtEM_VR5JSSx12_metEM_YieldTable.pickle",
		      ]
    
    pickleFilename = [ #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/GGquarks/MyTable1LepVR_2J.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/4JSSlowx_VR4JSSlowx_mtEM_VR4JSSlowx_aplanarityEM_VR4JSSlowx_DR_YieldTable.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/4JSSx12_VR4JSSx12_mtEM_VR4JSSx12_aplanarityEM_VR4JSSx12_DR_YieldTable.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/5JSShighx_VR5JSShighx_mtEM_VR5JSShighx_aplanarityEM_VR5JSShighx_DR_YieldTable.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/SSquarks/5JSSx12_VR5JSSx12_mtEM_VR5JSSx12_metEM_VR5JSSx12_DR_YieldTable.pickle",
		      ]
    '''
    pickleFilename = [ #os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/scripts/GGquarks/MyTable1LepVR_2J.pickle",
                      "tables/MyTable1LepVR_4JSSlowx.pickle",
                      "tables/MyTable1LepVR_4JSSx12.pickle",
                      "tables/MyTable1LepVR_5JSShighx.pickle",
                      "tables/MyTable1LepVR_5JSSx12.pickle",
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VR_squarks"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets_Sherpa22,singletop,diboson,wjets_Sherpa221,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
