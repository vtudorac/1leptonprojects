#!/usr/bin/env python

regDic = {}

##CRs ['WR3JEl', 'TR3JEl', 'WR3JMu', 'TR3JMu', 'WR5JEl', 'TR5JEl', 'WR5JMu', 'TR5JMu']
valid=False
#if not valid:
regDic["WR3JEl_cuts"]="W CR 3Jet "
regDic["TR3JEl_cuts"]="T CR 3Jet "
regDic["WR5JEl_cuts"]="W CR 5Jet"
regDic["TR5JEl_cuts"]="T CR 5Jet"
regDic["WR6JEl_cuts"]="W CR 6Jet"
regDic["TR6JEl_cuts"]="T CR 6Jet"
regDic["WR3JMu_cuts"]="W CR 3Jet"
regDic["TR3JMu_cuts"]="T CR 3Jet"
regDic["WR5JMu_cuts"]="W CR 5Jet"
regDic["TR5JMu_cuts"]="T CR 5Jet"
regDic["WR6JMu_cuts"]="W CR 6Jet"
regDic["TR6JMu_cuts"]="T CR 6Jet"

#if valid: 
regDic["VR3JhighMETEl_cuts"]="VR3JhighMET"
regDic["VR5JhighMETEl_cuts"]="VR5JhighMET"
regDic["VR6JhighMETEl_cuts"]="VR6JhighMET"
regDic["VR3JhighMTEl_cuts"]="VR3JhighMT"
regDic["VR5JhighMTEl_cuts"]="VR5JhighMT"
regDic["VR6JhighMTEl_cuts"]="VR6JhighMT"
regDic["VR3JhighMETMu_cuts"]="VR3JhighMET"
regDic["VR5JhighMETMu_cuts"]="VR5JhighMET"
regDic["VR6JhighMETMu_cuts"]="VR6JhighMET"
regDic["VR3JhighMTMu_cuts"]="VR3JhighMT"
regDic["VR5JhighMTMu_cuts"]="VR5JhighMT"
regDic["VR6JhighMTMu_cuts"]="VR6JhighMT"

