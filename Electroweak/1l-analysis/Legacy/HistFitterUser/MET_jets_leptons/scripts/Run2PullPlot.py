"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils
from pullPlotUtils import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR5J_mtEM"] = "5-jet VR m_{T} (e+#mu)"
    myRegionDict["VR5J_mtEl"] = "5-jet VR m_{T} (e)"    
    myRegionDict["VR5J_mtMu"] = "5-jet VR m_{T} (#mu)" 
    
    myRegionDict["VR5J_aplanarityEM"] = "5-jet VR aplan. (e+#mu)"
    myRegionDict["VR5J_aplanarityEl"] = "5-jet VR aplan. (e)"    
    myRegionDict["VR5J_aplanarityMu"] = "5-jet VR aplan. (#mu)"  
    
    myRegionDict["VR6J_mtEM"] = "6-jet VR m_{T} (e+#mu)"
    myRegionDict["VR6J_mtEl"] = "6-jet VR m_{T} (e)"    
    myRegionDict["VR6J_mtMu"] = "6-jet VR m_{T} (#mu)" 
    
    myRegionDict["VR6J_aplanarityEM"] = "6-jet VR aplan. (e+#mu)"
    myRegionDict["VR6J_aplanarityEl"] = "6-jet VR aplan. (e)"    
    myRegionDict["VR6J_aplanarityMu"] = "6-jet VR aplan. (#mu)"     
    
    myRegionDict["VR4Jlowx_mtEM"] = "4-jet VR low x m_{T} (e+#mu)"
    myRegionDict["VR4Jlowx_mtEl"] = "4-jet VR low x m_{T} (e)"    
    myRegionDict["VR4Jlowx_mtMu"] = "4-jet VR low x m_{T} (#mu)" 
    
    myRegionDict["VR4Jlowx_aplanarityEM"] = "4-jet VR low x aplan. (e+#mu)"
    myRegionDict["VR4Jlowx_aplanarityEl"] = "4-jet VR low x aplan. (e)"    
    myRegionDict["VR4Jlowx_aplanarityMu"] = "4-jet VR low x aplan. (#mu)"   
    
    myRegionDict["VR4Jhighx_mtEM"] = "4-jet VR high x m_{T} (e+#mu)"
    myRegionDict["VR4Jhighx_mtEl"] = "4-jet VR high x m_{T} (e)"    
    myRegionDict["VR4Jhighx_mtMu"] = "4-jet VR high x m_{T} (#mu)" 
    
    myRegionDict["VR4Jhighx_meffEM"] = "4-jet VR high x E_{T}^{miss}/m_{eff} (e+#mu)"
    myRegionDict["VR4Jhighx_meffEl"] = "4-jet VR high x E_{T}^{miss}/m_{eff} (e)"    
    myRegionDict["VR4Jhighx_meffMu"] = "4-jet VR high x E_{T}^{miss}/m_{eff} (#mu)"       
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["VR4Jhighx_mtEM_cuts","VR4Jhighx_mtEl_cuts","VR4Jhighx_mtMu_cuts"]
    regionList += ["VR4Jhighx_meffEM_cuts","VR4Jhighx_meffEl_cuts","VR4Jhighx_meffMu_cuts"]  
    
    regionList += ["VR4Jlowx_mtEM_cuts","VR4Jlowx_mtEl_cuts","VR4Jlowx_mtMu_cuts"]
    regionList += ["VR4Jlowx_aplanarityEM_cuts","VR4Jlowx_aplanarityEl_cuts","VR4Jlowx_aplanarityMu_cuts"] 
    
    regionList += ["VR5J_mtEM_cuts","VR5J_mtEl_cuts","VR5J_mtMu_cuts"]
    regionList += ["VR5J_aplanarityEM_cuts","VR5J_aplanarityEl_cuts","VR5J_aplanarityMu_cuts"] 
    
    regionList += ["VR6J_mtEM_cuts","VR6J_mtEl_cuts","VR6J_mtMu_cuts"]
    regionList += ["VR6J_aplanarityEM_cuts","VR6J_aplanarityEl_cuts","VR6J_aplanarityMu_cuts"] 
    #regionList += ["SR1sl2j_cuts","SLVR2_nJet","SSloose_metmeff2Jet"]
    #regionList += ["SS_metmeff2Jet"]

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("4-jet VR high x") != -1: return kBlue    
    if name.find("4-jet VR low x") != -1: return kBlue+1
    if name.find("5-jet") != -1: return kBlue+2
    if name.find("6-jet") != -1: return kBlue+3
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "ttbar":         return kGreen - 9
    if sample == "wjets":       return kAzure + 1
    if sample == "zjets":     return kYellow - 3
    if sample == "diboson":     return kGray + 1
    if sample == "singletop":     return kBlue + 1
    if sample == "ttv":     return kBlue + 3
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils.getRegionColor = getRegionColor
    pullPlotUtils.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_4Jhighxmt.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_4Jhighxmeff.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_4Jlowxmt.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_4Jlowxaplanarity.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_5Jmt.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_5Japlanarity.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_6Jmt.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables/MyTable1LepVR_6Japlanarity.pickle",
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VR"

    # Samples to stack on top of eachother in each region
    samples = "ttbar,singletop,ttv,wjets,diboson,zjets"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
