"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsSR2
from pullPlotUtilsSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["SR4JlowxEM"] = "4-jet low-x SR"
    
    myRegionDict["SR4JhighxEM"] = "4-jet high-x SR"    
    
    myRegionDict["SR5JEM"] = "5-jet SR"
    
    myRegionDict["SR6JEM"] = "6-jet SR"
    
    myRegionDict["SR2JEM"] = "2-jet soft-lepton SR"
    
    myRegionDict["SR5JEMsoft"] = "5-jet soft-lepton SR"    
        
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["SR4JlowxEM_cuts"] 
    regionList += ["SR4JhighxEM_cuts"] 
    regionList += ["SR5JEM_cuts"]     
    regionList += ["SR6JEM_cuts"]    
    regionList += ["SR2JEM"]     
    regionList += ["SR5JEMsoft"]        

    #regionList += ["SR1sl2j_cuts","SLVR2_nJet","SSloose_metmeff2Jet"]
    #regionList += ["SS_metmeff2Jet"]

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("5-jet soft-lepton") != -1: return kGreen-2   
    if name.find("4-jet high-x ") != -1: return kBlue    
    if name.find("4-jet low-x ") != -1: return kBlue+1
    if name.find("5-jet") != -1: return kBlue+2
    if name.find("6-jet") != -1: return kBlue+3
    if name.find("2-jet") != -1: return kGreen         
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "ttbar":         return kGreen - 9
    if sample == "wjets":       return kAzure + 1
    if sample == "zjets":     return kOrange
    if sample == "diboson":     return kViolet - 8
    if sample == "singletop":     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtilsSR2.getRegionColor = getRegionColor
    pullPlotUtilsSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepSR_4Jlowx2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepSR_4Jhighx2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepSR_5J2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepSR_6J2.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/softresults_January_150116/MySoftTable1LepSR2JEM.pickle",
		      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/softresults_January_150116/MySoftTable1LepSR_5JEM.pickle",		      
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "SR_comb"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets,singletop,diboson,wjets,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
