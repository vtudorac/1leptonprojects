"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils
from pullPlotUtils import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR2JmtEM_meffInc30"] = "2-jet VR m_{T}"
    myRegionDict["VR2JmtEM_meffInc30_bin0"] = "2-jet VR m_{T} bin 0"
    myRegionDict["VR2JmtEM_meffInc30_bin1"] = "2-jet VR m_{T} bin 1"    
    myRegionDict["VR2JmtEM_meffInc30_bin2"] = "2-jet VR m_{T} bin 2"
    myRegionDict["VR2JmtEM_meffInc30_bin3"] = "2-jet VR m_{T} bin 3" 
    
    myRegionDict["VR2JmetEM_meffInc30"] = "2-jet VR E_{T}^{miss}"
    myRegionDict["VR2JmetEM_meffInc30_bin0"] = "2-jet VR E_{T}^{miss} bin 0"
    myRegionDict["VR2JmetEM_meffInc30_bin1"] = "2-jet VR E_{T}^{miss} bin 1"    
    myRegionDict["VR2JmetEM_meffInc30_bin2"] = "2-jet VR E_{T}^{miss} bin 2"
    myRegionDict["VR2JmetEM_meffInc30_bin3"] = "2-jet VR E_{T}^{miss} bin 3"    
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["VR2JmtEM_meffInc30","VR2JmtEM_meffInc30_bin0","VR2JmtEM_meffInc30_bin1","VR2JmtEM_meffInc30_bin2","VR2JmtEM_meffInc30_bin3","VR2JmetEM_meffInc30","VR2JmetEM_meffInc30_bin0","VR2JmetEM_meffInc30_bin1","VR2JmetEM_meffInc30_bin2","VR2JmetEM_meffInc30_bin3"]


    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("2-jet") != -1: return kBlue    
    if name.find("6-jet") != -1: return kBlue    
    if name.find("4-jet") != -1: return kBlue        
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "Wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kYellow - 3
    if "diboson" in sample:     return kGray + 1
    if "singletop" in sample:     return kBlue + 1
    if sample == "ttv":     return kBlue + 3
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils.getRegionColor = getRegionColor
    pullPlotUtils.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR2Jmt_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR2Jmet_bkgonly.pickle"
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VR2J"

    # Samples to stack on top of eachother in each region
    samples = "group_ttbar_2J_bin1_ttbar_2J_bin2_ttbar_2J_bin3_ttbar_2J_bin4,group_singletop_2J_bin1_singletop_2J_bin2_singletop_2J_bin3_singletop_2J_bin4,ttv,group_Wjets_2J_bin1_Wjets_2J_bin2_Wjets_2J_bin3_Wjets_2J_bin4,diboson_Sherpa221,zjets_Sherpa221"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
