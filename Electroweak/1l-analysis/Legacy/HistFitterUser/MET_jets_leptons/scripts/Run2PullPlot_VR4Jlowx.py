"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils
from pullPlotUtils import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR4JlowxhybridEM_meffInc30"] = "4-jet low-x VR hybrid"
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin0"] = "4-jet low-x VR hybrid bin 0"
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin1"] = "4-jet low-x VR hybrid bin 1"    
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin2"] = "4-jet low-x VR hybrid bin 2"
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin3"] = "4-jet low-x VR hybrid bin 3" 
    
    myRegionDict["VR4JlowxaplEM_meffInc30"] = "4-jet low-x VR aplanarity"
    myRegionDict["VR4JlowxaplEM_meffInc30_bin0"] = "4-jet low-x VR aplanarity bin 0"
    myRegionDict["VR4JlowxaplEM_meffInc30_bin1"] = "4-jet low-x VR aplanarity bin 1"    
    myRegionDict["VR4JlowxaplEM_meffInc30_bin2"] = "4-jet low-x VR aplanarity bin 2"
    myRegionDict["VR4JlowxaplEM_meffInc30_bin3"] = "4-jet low-x VR aplanarity bin 3"    
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["VR4JlowxhybridEM_meffInc30","VR4JlowxhybridEM_meffInc30_bin0","VR4JlowxhybridEM_meffInc30_bin1","VR4JlowxhybridEM_meffInc30_bin2","VR4JlowxaplEM_meffInc30","VR4JlowxaplEM_meffInc30_bin0","VR4JlowxaplEM_meffInc30_bin1","VR4JlowxaplEM_meffInc30_bin2"]


    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("2-jet") != -1: return kBlue    
    if name.find("6-jet") != -1: return kBlue    
    if name.find("4-jet") != -1: return kBlue        
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "Wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kYellow - 3
    if "diboson" in sample:     return kGray + 1
    if "singletop" in sample:     return kBlue + 1
    if sample == "ttv":     return kBlue + 3
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils.getRegionColor = getRegionColor
    pullPlotUtils.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR4Jlowxhybrid_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR4Jlowxapl_bkgonly.pickle"
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VR4Jlowx"

    # Samples to stack on top of eachother in each region
    samples = "group_ttbar_4Jlowx_bin1_ttbar_4Jlowx_bin2_ttbar_4Jlowx_bin3,group_singletop_4Jlowx_bin1_singletop_4Jlowx_bin2_singletop_4Jlowx_bin3,ttv,group_Wjets_4Jlowx_bin1_Wjets_4Jlowx_bin2_Wjets_4Jlowx_bin3,diboson_Sherpa221,zjets_Sherpa221"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
