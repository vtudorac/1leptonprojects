"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtilsSR2J module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsVRSR2
from pullPlotUtilsVRSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR2JmtEM_meffInc30"] = "VR m_{T}"
    myRegionDict["VR2JmtEM_meffInc30_bin0"] = "VR m_{T} m_{eff}^{bin 1}"
    myRegionDict["VR2JmtEM_meffInc30_bin1"] = "VR m_{T} m_{eff}^{bin 2}"    
    myRegionDict["VR2JmtEM_meffInc30_bin2"] = "VR m_{T} m_{eff}^{bin 3}"
    myRegionDict["VR2JmtEM_meffInc30_bin3"] = "VR m_{T} m_{eff}^{bin 4}" 
    
    myRegionDict["VR2JmetEM_meffInc30"] = "VR E_{T}^{miss}"
    myRegionDict["VR2JmetEM_meffInc30_bin0"] = "VR E_{T}^{miss} m_{eff}^{bin 1}"
    myRegionDict["VR2JmetEM_meffInc30_bin1"] = "VR E_{T}^{miss} m_{eff}^{bin 2}"    
    myRegionDict["VR2JmetEM_meffInc30_bin2"] = "VR E_{T}^{miss} m_{eff}^{bin 3}"
    myRegionDict["VR2JmetEM_meffInc30_bin3"] = "VR E_{T}^{miss} m_{eff}^{bin 4}"    
    
    myRegionDict["SR2JBTEM_meffInc30"] = "SR b-tag"
    myRegionDict["SR2JBTEM_meffInc30_bin0"] = "SR b-tag m_{eff}^{bin 1}"
    myRegionDict["SR2JBTEM_meffInc30_bin1"] = "SR b-tag m_{eff}^{bin 2}"    
    myRegionDict["SR2JBTEM_meffInc30_bin2"] = "SR b-tag m_{eff}^{bin 3}"
    myRegionDict["SR2JBTEM_meffInc30_bin3"] = "SR b-tag m_{eff}^{bin 4}" 
    
    myRegionDict["SR2JBVEM_meffInc30"] = "SR b-veto"
    myRegionDict["SR2JBVEM_meffInc30_bin0"] = "SR b-veto m_{eff}^{bin 1}"
    myRegionDict["SR2JBVEM_meffInc30_bin1"] = "SR b-veto m_{eff}^{bin 2}"    
    myRegionDict["SR2JBVEM_meffInc30_bin2"] = "SR b-veto m_{eff}^{bin 3}"
    myRegionDict["SR2JBVEM_meffInc30_bin3"] = "SR b-veto m_{eff}^{bin 4}"    
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["VR2JmtEM_meffInc30","VR2JmtEM_meffInc30_bin0","VR2JmtEM_meffInc30_bin1","VR2JmtEM_meffInc30_bin2","VR2JmtEM_meffInc30_bin3","VR2JmetEM_meffInc30","VR2JmetEM_meffInc30_bin0","VR2JmetEM_meffInc30_bin1","VR2JmetEM_meffInc30_bin2","VR2JmetEM_meffInc30_bin3","SR2JBTEM_meffInc30","SR2JBTEM_meffInc30_bin0","SR2JBTEM_meffInc30_bin1","SR2JBTEM_meffInc30_bin2","SR2JBTEM_meffInc30_bin3","SR2JBVEM_meffInc30","SR2JBVEM_meffInc30_bin0","SR2JBVEM_meffInc30_bin1","SR2JBVEM_meffInc30_bin2","SR2JBVEM_meffInc30_bin3"]


    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("b-veto") != -1: return kBlue    
    if name.find("b-tag") != -1: return kBlue    
    if name.find("VR") != -1: return kBlue        
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "Wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson" in sample:     return kViolet - 8
    if "singletop" in sample:     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtilsSR2' default colours (which are all black)
    pullPlotUtilsVRSR2.getRegionColor = getRegionColor
    pullPlotUtilsVRSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR2Jmt_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR2Jmet_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBT_2J_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBV_2J_bkgonly.pickle"
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VRSR2J"

    # Samples to stack on top of eachother in each region
    samples = "zjets_Sherpa221,ttv,diboson_Sherpa221,group_Wjets_2J_bin1_Wjets_2J_bin2_Wjets_2J_bin3_Wjets_2J_bin4,group_singletop_2J_bin1_singletop_2J_bin2_singletop_2J_bin3_singletop_2J_bin4,group_ttbar_2J_bin1_ttbar_2J_bin2_ttbar_2J_bin3_ttbar_2J_bin4"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
