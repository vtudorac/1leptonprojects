"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtilsSR2 module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsVRSR2
from pullPlotUtilsVRSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR4JlowxhybridEM_meffInc30"] = "VR hybrid"
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin0"] = "VR hybrid m_{eff}^{bin 1}"
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin1"] = "VR hybrid m_{eff}^{bin 2}"    
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin2"] = "VR hybrid m_{eff}^{bin 3}"
    myRegionDict["VR4JlowxhybridEM_meffInc30_bin3"] = "VR hybrid m_{eff}^{bin 4}" 
    
    myRegionDict["VR4JlowxaplEM_meffInc30"] = "VR aplanarity"
    myRegionDict["VR4JlowxaplEM_meffInc30_bin0"] = "VR aplanarity m_{eff}^{bin 1}"
    myRegionDict["VR4JlowxaplEM_meffInc30_bin1"] = "VR aplanarity m_{eff}^{bin 2}"    
    myRegionDict["VR4JlowxaplEM_meffInc30_bin2"] = "VR aplanarity m_{eff}^{bin 3}"
    myRegionDict["VR4JlowxaplEM_meffInc30_bin3"] = "VR aplanarity m_{eff}^{bin 4}"  
    
    myRegionDict["SR4JlowxBTEM_meffInc30"] = "SR b-tag"
    myRegionDict["SR4JlowxBTEM_meffInc30_bin0"] = "SR b-tag m_{eff}^{bin 1}"
    myRegionDict["SR4JlowxBTEM_meffInc30_bin1"] = "SR b-tag m_{eff}^{bin 2}"    
    myRegionDict["SR4JlowxBTEM_meffInc30_bin2"] = "SR b-tag m_{eff}^{bin 3}"
    myRegionDict["SR4JlowxBTEM_meffInc30_bin3"] = "SR b-tag m_{eff}^{bin 4}" 
    
    myRegionDict["SR4JlowxBVEM_meffInc30"] = "SR b-veto"
    myRegionDict["SR4JlowxBVEM_meffInc30_bin0"] = "SR b-veto m_{eff}^{bin 1}"
    myRegionDict["SR4JlowxBVEM_meffInc30_bin1"] = "SR b-veto m_{eff}^{bin 2}"    
    myRegionDict["SR4JlowxBVEM_meffInc30_bin2"] = "SR b-veto m_{eff}^{bin 3}"
    myRegionDict["SR4JlowxBVEM_meffInc30_bin3"] = "SR b-veto m_{eff}^{bin 4}"    
    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["VR4JlowxhybridEM_meffInc30","VR4JlowxhybridEM_meffInc30_bin0","VR4JlowxhybridEM_meffInc30_bin1","VR4JlowxhybridEM_meffInc30_bin2","VR4JlowxaplEM_meffInc30","VR4JlowxaplEM_meffInc30_bin0","VR4JlowxaplEM_meffInc30_bin1","VR4JlowxaplEM_meffInc30_bin2","SR4JlowxBTEM_meffInc30","SR4JlowxBTEM_meffInc30_bin0","SR4JlowxBTEM_meffInc30_bin1","SR4JlowxBTEM_meffInc30_bin2","SR4JlowxBVEM_meffInc30","SR4JlowxBVEM_meffInc30_bin0","SR4JlowxBVEM_meffInc30_bin1","SR4JlowxBVEM_meffInc30_bin2"]


    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("b-tag") != -1: return kBlue    
    if name.find("b-veto") != -1: return kBlue    
    if name.find("VR") != -1: return kBlue        
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "Wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson" in sample:     return kViolet - 8
    if "singletop" in sample:     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtilsSR2' default colours (which are all black)
    pullPlotUtilsVRSR2.getRegionColor = getRegionColor
    pullPlotUtilsVRSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR4Jlowxhybrid_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepVR4Jlowxapl_bkgonly.pickle",
os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBT_4Jlowx_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBV_4Jlowx_bkgonly.pickle"
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VRSR4Jlowx"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets_Sherpa221,diboson_Sherpa221,group_Wjets_4Jlowx_bin1_Wjets_4Jlowx_bin2_Wjets_4Jlowx_bin3,group_singletop_4Jlowx_bin1_singletop_4Jlowx_bin2_singletop_4Jlowx_bin3,group_ttbar_4Jlowx_bin1_ttbar_4Jlowx_bin2_ttbar_4Jlowx_bin3"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
