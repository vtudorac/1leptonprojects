"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsVRSR2
from pullPlotUtilsVRSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["SR2JBTEM_meffInc30"] = "SR 2J b-tag"
    myRegionDict["SR2JBTEM_meffInc30_bin0"] = "SR 2J b-tag m_{eff}^{bin 1}"
    myRegionDict["SR2JBTEM_meffInc30_bin1"] = "SR 2J b-tag m_{eff}^{bin 2}"    
    myRegionDict["SR2JBTEM_meffInc30_bin2"] = "SR 2J b-tag m_{eff}^{bin 3}"
    myRegionDict["SR2JBTEM_meffInc30_bin3"] = "SR 2J b-tag m_{eff}^{bin 4}" 
    
    myRegionDict["SR2JBVEM_meffInc30"] = "SR 2J b-veto"
    myRegionDict["SR2JBVEM_meffInc30_bin0"] = "SR 2J b-veto m_{eff}^{bin 1}"
    myRegionDict["SR2JBVEM_meffInc30_bin1"] = "SR 2J b-veto m_{eff}^{bin 2}"    
    myRegionDict["SR2JBVEM_meffInc30_bin2"] = "SR 2J b-veto m_{eff}^{bin 3}"
    myRegionDict["SR2JBVEM_meffInc30_bin3"] = "SR 2J b-veto m_{eff}^{bin 4}"    
    
    myRegionDict["SR4JhighxBTEM_meffInc30"] = "SR 4J high-x b-tag"
    myRegionDict["SR4JhighxBTEM_meffInc30_bin0"] = "SR 4J high-x b-tag m_{eff}^{bin 1}"
    myRegionDict["SR4JhighxBTEM_meffInc30_bin1"] = "SR 4J high-x b-tag m_{eff}^{bin 2}"    
    myRegionDict["SR4JhighxBTEM_meffInc30_bin2"] = "SR 4J high-x b-tag m_{eff}^{bin 3}"
    
    myRegionDict["SR4JhighxBVEM_meffInc30"] = "SR 4J high-x b-veto"
    myRegionDict["SR4JhighxBVEM_meffInc30_bin0"] = "SR 4J high-x b-veto m_{eff}^{bin 1}"
    myRegionDict["SR4JhighxBVEM_meffInc30_bin1"] = "SR 4J high-x b-veto m_{eff}^{bin 2}"    
    myRegionDict["SR4JhighxBVEM_meffInc30_bin2"] = "SR 4J high-x b-veto m_{eff}^{bin 3}" 
    
    
    myRegionDict["SR4JlowxBTEM_meffInc30"] = "SR 4J low-x b-tag"
    myRegionDict["SR4JlowxBTEM_meffInc30_bin0"] = "SR 4J low-x b-tag m_{eff}^{bin 1}"
    myRegionDict["SR4JlowxBTEM_meffInc30_bin1"] = "SR 4J low-x b-tag m_{eff}^{bin 2}"    
    myRegionDict["SR4JlowxBTEM_meffInc30_bin2"] = "SR 4J low-x b-tag m_{eff}^{bin 3}"
    
    myRegionDict["SR4JlowxBVEM_meffInc30"] = "SR 4J low-x b-veto"
    myRegionDict["SR4JlowxBVEM_meffInc30_bin0"] = "SR 4J low-x b-veto m_{eff}^{bin 1}"
    myRegionDict["SR4JlowxBVEM_meffInc30_bin1"] = "SR 4J low-x b-veto m_{eff}^{bin 2}"    
    myRegionDict["SR4JlowxBVEM_meffInc30_bin2"] = "SR 4J low-x b-veto m_{eff}^{bin 3}"   
    
    myRegionDict["SR6JBTEM_meffInc30"] = "SR 6J b-tag"
    myRegionDict["SR6JBTEM_meffInc30_bin0"] = "SR 6J b-tag m_{eff}^{bin 1}"
    myRegionDict["SR6JBTEM_meffInc30_bin1"] = "SR 6J b-tag m_{eff}^{bin 2}"    
    myRegionDict["SR6JBTEM_meffInc30_bin2"] = "SR 6J b-tag m_{eff}^{bin 3}"
    myRegionDict["SR6JBTEM_meffInc30_bin3"] = "SR 6J b-tag m_{eff}^{bin 4}" 
    
    myRegionDict["SR6JBVEM_meffInc30"] = "SR 6J b-veto"
    myRegionDict["SR6JBVEM_meffInc30_bin0"] = "SR 6J b-veto m_{eff}^{bin 1}"
    myRegionDict["SR6JBVEM_meffInc30_bin1"] = "SR 6J b-veto m_{eff}^{bin 2}"    
    myRegionDict["SR6JBVEM_meffInc30_bin2"] = "SR 6J b-veto m_{eff}^{bin 3}"
    myRegionDict["SR6JBVEM_meffInc30_bin3"] = "SR 6J b-veto m_{eff}^{bin 4}"    
    
    
    myRegionDict["SR"] = "SR 9J"
    myRegionDict["SR_bin0"] = "SR 9J m_{eff}^{bin 1}"
    myRegionDict["SR_bin1"] = "SR 9J m_{eff}^{bin 2}"
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["SR2JBTEM_meffInc30","SR2JBTEM_meffInc30_bin0","SR2JBTEM_meffInc30_bin1","SR2JBTEM_meffInc30_bin2","SR2JBTEM_meffInc30_bin3","SR2JBVEM_meffInc30","SR2JBVEM_meffInc30_bin0","SR2JBVEM_meffInc30_bin1","SR2JBVEM_meffInc30_bin2","SR2JBVEM_meffInc30_bin3","SR4JhighxBTEM_meffInc30","SR4JhighxBTEM_meffInc30_bin0","SR4JhighxBTEM_meffInc30_bin1","SR4JhighxBTEM_meffInc30_bin2","SR4JhighxBVEM_meffInc30","SR4JhighxBVEM_meffInc30_bin0","SR4JhighxBVEM_meffInc30_bin1","SR4JhighxBVEM_meffInc30_bin2","SR4JlowxBTEM_meffInc30","SR4JlowxBTEM_meffInc30_bin0","SR4JlowxBTEM_meffInc30_bin1","SR4JlowxBTEM_meffInc30_bin2","SR4JlowxBVEM_meffInc30","SR4JlowxBVEM_meffInc30_bin0","SR4JlowxBVEM_meffInc30_bin1","SR4JlowxBVEM_meffInc30_bin2","SR6JBTEM_meffInc30","SR6JBTEM_meffInc30_bin0","SR6JBTEM_meffInc30_bin1","SR6JBTEM_meffInc30_bin2","SR6JBTEM_meffInc30_bin3","SR6JBVEM_meffInc30","SR6JBVEM_meffInc30_bin0","SR6JBVEM_meffInc30_bin1","SR6JBVEM_meffInc30_bin2","SR6JBVEM_meffInc30_bin3","SR","SR_bin0","SR_bin1"]


    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("6J") != -1: return kBlue    
    elif name.find("2J") != -1: return kBlue    
    elif name.find("4J") != -1: return kBlue        
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "Wjets" in sample or 'wjets' in sample:       return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson" in sample:     return kViolet - 8
    if "singletop" in sample:     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtilsVRSR2.getRegionColor = getRegionColor
    pullPlotUtilsVRSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBT_2J_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBV_2J_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBT_4Jhighx_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBV_4Jhighx_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBT_4Jlowx_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBV_4Jlowx_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBT_6J_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/MyTable1LepSRBV_6J_bkgonly.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser/MET_jets_leptons/tables/sr_only_multijet_06_10.pickle"
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "allSR"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets_Sherpa221,diboson_Sherpa221,group_Wjets_2J_bin1_Wjets_2J_bin2_Wjets_2J_bin3_Wjets_2J_bin4,group_Wjets_4Jhighx_bin1_Wjets_4Jhighx_bin2_Wjets_4Jhighx_bin3,group_Wjets_4Jlowx_bin1_Wjets_4Jlowx_bin2_Wjets_4Jlowx_bin3,group_Wjets_6J_bin1_Wjets_6J_bin2_Wjets_6J_bin3_Wjets_6J_bin4,wjets_Sherpa221,group_singletop_2J_bin1_singletop_2J_bin2_singletop_2J_bin3_singletop_2J_bin4,group_singletop_4Jhighx_bin1_singletop_4Jhighx_bin2_singletop_4Jhighx_bin3,group_singletop_4Jlowx_bin1_singletop_4Jlowx_bin2_singletop_4Jlowx_bin3,group_singletop_6J_bin1_singletop_6J_bin2_singletop_6J_bin3_singletop_6J_bin4,singletop,group_ttbar_2J_bin1_ttbar_2J_bin2_ttbar_2J_bin3_ttbar_2J_bin4,group_ttbar_4Jhighx_bin1_ttbar_4Jhighx_bin2_ttbar_4Jhighx_bin3,group_ttbar_4Jlowx_bin1_ttbar_4Jlowx_bin2_ttbar_4Jlowx_bin3,group_ttbar_6J_bin1_ttbar_6J_bin2_ttbar_6J_bin3_ttbar_6J_bin4,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
