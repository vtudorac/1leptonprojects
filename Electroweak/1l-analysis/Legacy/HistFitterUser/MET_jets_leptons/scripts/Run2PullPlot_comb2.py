"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils2
from pullPlotUtils2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["VR5J_mtEM"] = "5-jet VR m_{T}"
    #myRegionDict["VR5J_mtEl"] = "5-jet VR m_{T} (e)"    
    #myRegionDict["VR5J_mtMu"] = "5-jet VR m_{T} (#mu)" 
    
    myRegionDict["VR5J_aplanarityEM"] = "5-jet VR aplan."
    #myRegionDict["VR5J_aplanarityEl"] = "5-jet VR aplan. (e)"    
    #myRegionDict["VR5J_aplanarityMu"] = "5-jet VR aplan. (#mu)"  
    
    myRegionDict["VR6J_mtEM"] = "6-jet VR m_{T}"
    #myRegionDict["VR6J_mtEl"] = "6-jet VR m_{T} (e)"    
    #myRegionDict["VR6J_mtMu"] = "6-jet VR m_{T} (#mu)" 
    
    myRegionDict["VR6J_aplanarityEM"] = "6-jet VR aplan."
    #myRegionDict["VR6J_aplanarityEl"] = "6-jet VR aplan. (e)"    
    #myRegionDict["VR6J_aplanarityMu"] = "6-jet VR aplan. (#mu)"     
    
    myRegionDict["VR4Jlowx_mtEM"] = "4-jet low-x VR m_{T}"
    #myRegionDict["VR4Jlowx_mtEl"] = "4-jet low-x VR m_{T} (e)"    
    #myRegionDict["VR4Jlowx_mtMu"] = "4-jet low-x VR m_{T} (#mu)" 
    
    myRegionDict["VR4Jlowx_aplanarityEM"] = "4-jet low-x VR aplan."
    #myRegionDict["VR4Jlowx_aplanarityEl"] = "4-jet low-x VR aplan. (e)"    
    #myRegionDict["VR4Jlowx_aplanarityMu"] = "4-jet low-x VR aplan. (#mu)"   
    
    myRegionDict["VR4Jhighx_mtEM"] = "4-jet high-x VR m_{T}"
    #myRegionDict["VR4Jhighx_mtEl"] = "4-jet high-x VR m_{T} (e)"    
    #myRegionDict["VR4Jhighx_mtMu"] = "4-jet high-x VR m_{T} (#mu)" 
    
    myRegionDict["VR4Jhighx_meffEM"] = "4-jet high-x VR E_{T}^{miss}/m_{eff}"
    #myRegionDict["VR4Jhighx_meffEl"] = "4-jet high-x VR E_{T}^{miss}/m_{eff} (e)"    
    #myRegionDict["VR4Jhighx_meffMu"] = "4-jet high-x VR E_{T}^{miss}/m_{eff} (#mu)"  
    
    myRegionDict["VR2J_1EM"] = "2-jet soft-lepton VR (E_{T}^{miss})"       
    myRegionDict["VR2J_2EM"] = "2-jet soft-lepton VR (m_{T})"    
    
    myRegionDict["VR5J_1EM"] = "5-jet soft-lepton VR (E_{T}^{miss})"       
    myRegionDict["VR5J_2EM"] = "5-jet soft-lepton VR (H_{T})"            
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["VR4Jhighx_mtEM_cuts"]
    regionList += ["VR4Jhighx_meffEM_cuts"]  
    
    regionList += ["VR4Jlowx_mtEM_cuts"]
    regionList += ["VR4Jlowx_aplanarityEM_cuts"] 
    
    regionList += ["VR5J_mtEM_cuts"]
    regionList += ["VR5J_aplanarityEM_cuts"] 
    
    regionList += ["VR6J_mtEM_cuts"]
    regionList += ["VR6J_aplanarityEM_cuts"] 
    #regionList += ["SR1sl2j_cuts","SLVR2_nJet","SSloose_metmeff2Jet"]
    #regionList += ["SS_metmeff2Jet"]
    
    regionList += ["VR2J_1EM"]
    regionList += ["VR2J_2EM"]   
       
    regionList += ["VR5J_1EM"]
    regionList += ["VR5J_2EM"]  

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("5-jet soft-lepton") != -1: return kGreen-2   
    if name.find("4-jet high-x VR ") != -1: return kBlue    
    if name.find("4-jet low-x VR ") != -1: return kBlue+1
    if name.find("5-jet") != -1: return kBlue+2
    if name.find("6-jet") != -1: return kBlue+3
    if name.find("2-jet") != -1: return kGreen         
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "ttbar":         return kGreen - 9
    if sample == "wjets":       return kAzure + 1
    if sample == "zjets":     return kOrange
    if sample == "diboson":     return kViolet - 8
    if sample == "singletop":     return kGreen + 2
    if sample == "ttv":     return kOrange
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils2.getRegionColor = getRegionColor
    pullPlotUtils2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_4Jhighxmt2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_4Jhighxmeff2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_4Jlowxmt2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_4Jlowxaplanarity2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_5Jmt2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_5Japlanarity2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_6Jmt2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/tables_3.2ifb_150116/MyTable1LepVR_6Japlanarity2.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/softresults_January_150116/MySoftTable1LepVR2JEM.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/softresults_January_150116/MySoftTable1LepVR5J_1EM.pickle",
                      os.getenv("HISTFITTER")+"/../HistFitterUser2/MET_jets_leptons/softresults_January_150116/MySoftTable1LepVR5J_2EM.pickle",			      	      
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "VR_comb"

    # Samples to stack on top of eachother in each region
    samples = "ttv,zjets,singletop,diboson,wjets,ttbar"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
