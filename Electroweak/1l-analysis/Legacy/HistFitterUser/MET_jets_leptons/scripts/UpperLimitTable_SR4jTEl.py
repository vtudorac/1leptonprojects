
from sys import exit
import os
import sys
from ROOT import gROOT,gSystem,gDirectory
#gSystem.Load("libCombinationTools")
gSystem.Load("libSusyFitter.so")
from ROOT import ConfigMgr,FitConfig #this module comes from gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet,RooAbsData,RooRandom 
from ROOT import Util, TMath, RooStats
from ROOT import RooFit

from UpperLimitTex import *

import pickle

## MB instructions
# The main functions are called at the bottom of this file.
# This will probably require a bit more work to get to work nicely.

def latexfitresults(upLim,filename,workspacename='w', lumiFB=1.0, lumiRelUncert=0.037, poiname='mu_SIG', dataname='obsData'):

  ############################################
  
##   file = TFile.Open(filename)
##   if file.IsZombie() :
##     print "ERROR : Cannot open file : ", filename
##     sys.exit(1)
  
  w = Util.GetWorkspaceFromFile(filename,workspacename)
  
  if w==None:
    print "ERROR : Cannot open workspace : ", workspacename
    sys.exit(1) 

##   result = w.obj(resultname)
##   if result==None:
##     print "ERROR : Cannot open fit result : ", resultname
##     sys.exit(1)
    
## ##  snapshot = 'discoveryfit'  
## ##   w.loadSnapshot(snapshot)
##   snapshot = resultname.replace('RooFitResult','snapshot_paramsVals')
##   w.loadSnapshot(snapshot)

##   data_set = w.data(dataname)
##   if data_set==None:
##     print "ERROR : Cannot open dataset : ", "data_set"
##     sys.exit(1)
      
##   regionCat = w.obj("channelCat")
##   data_set.table(regionCat).Print("v")

##   signalregionFullName = Util.GetFullRegionName(regionCat, signalregion)

  if len(poiname)==0:
    print " "
  else:
    modelConfig = w.obj("ModelConfig")
    poi = w.var(poiname)
    if poi==None:
      print "ERROR : Cannot find POI with name: ", poiname
      sys.exit(1)
    modelConfig.SetParametersOfInterest(RooArgSet(poi))
    modelConfig.GetNuisanceParameters().remove(poi)


  
  ##
  #  w.Print()
  ntoys = 3000
  calctype = 2    # toys = 0, asymptotic (asimov) = 2
  npoints = 20
#  hti_result = RooStats.MakeUpperLimitPlot(poiname,w,2,3,1000,True,20)  
  hti_result = RooStats.MakeUpperLimitPlot(poiname,w,calctype,3,ntoys,True,npoints)
  outFileName = "./htiResult_poi_" + poiname + "_ntoys_" + str(ntoys) + "_calctype_" + str(calctype) + "_npoints_" + str(npoints) + ".root"
  hti_result.SaveAs(outFileName)
##   RooStats::MakeUpperLimitPlot(const char* fileprefix,
## 			     RooWorkspace* w,
## 			     int calculatorType ,                         # toys = 0, asymptotic (asimov) = 2
## 			     int testStatType , 
## 			     int ntoys,
## 			     bool useCLs ,  
## 			     int npoints )
  hti_result.Print()
  
  uL_nobsinSR = hti_result.UpperLimit()
  uL_visXsec = uL_nobsinSR / lumiFB
  uL_visXsecErrorUp = uL_visXsec - uL_nobsinSR/(lumiFB * (1. + lumiRelUncert))
  uL_visXsecErrorDown = uL_nobsinSR/(lumiFB * (1. - lumiRelUncert)) - uL_visXsec

  uL_nexpinSR = hti_result.GetExpectedUpperLimit(0)
  uL_nexpinSR_P = hti_result.GetExpectedUpperLimit(1) 
  uL_nexpinSR_M = hti_result.GetExpectedUpperLimit(-1)
  if uL_nexpinSR > uL_nexpinSR_P or uL_nexpinSR < uL_nexpinSR_M:
    print " \n something very strange, either the uL_nexpinSR > uL_nexpinSR_P or uL_nexpinSR < uL_nexpinSR_M"
    print "  uL_nexpinSR = ", uL_nexpinSR , " uL_nexpinSR_P = ", uL_nexpinSR_P, " uL_nexpinSR_M = ", uL_nexpinSR_M
  uL_nexpinSRerrP = hti_result.GetExpectedUpperLimit(1) - uL_nexpinSR
  uL_nexpinSRerrM = uL_nexpinSR - hti_result.GetExpectedUpperLimit(-1)

  # find the CLB values at indexes above and below observed CLs p-value
  CLB_P = 0.
  CLB_M = 0.
  mu_P = 0.
  mu_M = 0.
  index_P = 0
  indexFound = False
  for iresult in range(hti_result.ArraySize()):
    xval = hti_result.GetXValue(iresult) 
    # print " GetXValue() = ", xval
    yval = hti_result.GetYValue(iresult)
    #  print " GetYValue() = ", yval
    if xval>uL_nobsinSR and not indexFound:
      index_P = iresult
      CLB_P = hti_result.CLb(iresult)
      mu_P = xval
      if iresult>0:
        CLB_M = hti_result.CLb(iresult-1)
        mu_M = hti_result.GetXValue(iresult-1)
        indexFound = True
        print " \n\n        Found the CLB values to interpolate!"
        print " CLB_M =", CLB_M, " CLB_P =", CLB_P, "  mu_P = ", mu_P, " mu_M = ", mu_M
        
    
  # interpolate the value of CLB to be exactly above upperlimit p-val
  alpha_CLB = (CLB_P - CLB_M) / (mu_P - mu_M)
  beta_CLB = CLB_P - alpha_CLB*mu_P
  # CLB is taken as the point on the CLB curve for the same poi value, as the observed upperlimit
  CLB = alpha_CLB * uL_nobsinSR + beta_CLB
  
  print "\n\n\n\n  ***---  now doing p-value calculation ---*** \n\n\n\n"
  #pval = 0.
  pval = RooStats.get_Presult(w,False,1000,2)
## get_Presult(  RooWorkspace* w,
##           		bool doUL, // = true, // true = exclusion, false = discovery
##             		int ntoys, //=1000,
##             		int calculatorType, // = 0,
##             		int testStatType, // = 3,  
##             		const char * modelSBName, // = "ModelConfig",
##             		const char * modelBName, // = "",
##             		const char * dataName, // = "obsData",
##             		bool useCLs, // = true ,   
##             		bool useNumberCounting, // = false,
##             		const char * nuisPriorName) // = 0 

  
  signalregion = poiname.replace("mu_","")
  uLmap = {}
#  uLmap[signalregion] = [ uL_visXsec, uL_visXsecErrorUp, uL_visXsecErrorDown, uL_nobsinSR, uL_nexpinSR, uL_nexpinSRerrP, uL_nexpinSRerrM, CLB, pval ]
  uLmap[signalregion] = [ uL_visXsec, uL_nobsinSR, uL_nexpinSR, uL_nexpinSRerrP, uL_nexpinSRerrM, CLB, pval ]

  for sr in uLmap.keys():
    upLim[sr] = []
    upLim[sr].append( uLmap[sr] )


  return 





##################################
##################################
##################################

# MAIN

if True:
  upLim = {}

  #  filename = os.environ['SUSYFITTER'] + '/results/discoveryDan_MyOneLeptonKtScaleFitR17_Discovery_combined_NormalMeasurement_model_afterFit.root'
  # filename = os.environ['SUSYFITTER'] + '/results/MyOneLeptonKtScaleFitR17_Discovery_combined_NormalMeasurement_model_afterFit.root'
  #  filename = os.environ['SUSYFITTER'] + '/results/MyOneLeptonKtScaleFit_discoveryUpperLimitR17_Discovery_combined_NormalMeasurement_model_afterFit.root'
  #filename = os.environ['SUSYFITTER'] + '/results/MyOneLeptonKtScaleFit_discoveryUpperLimitR17_Discovery_combined_DiscoveryMeasurement_model.root'
  #  filename = os.environ['SUSYFITTER'] + '/results/SL/MyOneLeptonKtScaleFit_mergerSoftLep_soft_discoveryTight_newR17_Discovery_combined_NormalMeasurement_model.root'
  #  filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_SR3jTEl_combined_BasicMeasurement_model_afterFit.root'
  workspacename = 'combined' # 'combined' # 'w'
  lumiFB = 4.713
  lumiRelUncert = 0.037

  filename = os.environ['HISTFITTER'] + '/results/MyDiscoveryAnalysis_SR4jTEl_SPlusB_combined_NormalMeasurement_model.root'
  poiname = 'mu_SR4jTEl'
  latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
  line_upLim = tablefragment(upLim,'upperLim.1l')
  f = open('UpperLimit_SR4jTEl.tex', 'w')
  f.write( line_upLim )
  f.close()
  print " \n\n Model independent upper limit for POI = ",poiname, " written in file UpperLimit_SR4jTEl.tex"

##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_SR4jTEl_combined_BasicMeasurement_model.root'
##   poiname = 'mu_SR4jTEl'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_SR4jTEl.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}

##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_SR3jTMu_combined_BasicMeasurement_model.root'
##   poiname = 'mu_SR3jTMu'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_SR3jTMu.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}

##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_SR4jTMu_combined_BasicMeasurement_model.root'
##   poiname = 'mu_SR4jTMu'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_SR4jTMu.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  
##    filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_SSTEl_combined_BasicMeasurement_model.root'
##    poiname = 'mu_SSTEl'
##    latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##    line_upLim = tablefragment(upLim,'upperLim.1l')
##    f = open('UpperLimit_SSTEl.tex', 'w')
##    f.write( line_upLim )
##    f.close()
##    upLim ={}

##    filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_SSTMu_combined_BasicMeasurement_model.root'
##    poiname = 'mu_SSTMu'
##    latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##    line_upLim = tablefragment(upLim,'upperLim.1l')
##    f = open('UpperLimit_SSTMu.tex', 'w')
##    f.write( line_upLim )
##    f.close()
##    upLim ={}
  
##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_S2eeT_combined_BasicMeasurement_model.root'
##   poiname = 'mu_S2eeT'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_2lepSR_S2eeT.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  
##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_S2emT_combined_BasicMeasurement_model.root'
##   poiname = 'mu_S2emT'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_2lepSR_S2emT.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  
##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_S2mmT_combined_BasicMeasurement_model.root'
##   poiname = 'mu_S2mmT'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_2lepSR_S2mmT.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  
##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_S4eeT_combined_BasicMeasurement_model.root'
##   poiname = 'mu_S4eeT'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_2lepSR_S4eeT.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  
##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_S4emT_combined_BasicMeasurement_model.root'
##   poiname = 'mu_2lepSR_S4emT'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_S4emT.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  
##   filename = os.environ['HISTFITTER'] + '/results/Combined_KFactorFit_5Channel_Discovery_S4mmT_combined_BasicMeasurement_model.root'
##   poiname = 'mu_2lepSR_S4mmT'
##   latexfitresults(upLim,filename,workspacename,lumiFB,lumiRelUncert,poiname)
##   line_upLim = tablefragment(upLim,'upperLim.1l')
##   f = open('UpperLimit_S4mmT.tex', 'w')
##   f.write( line_upLim )
##   f.close()
##   upLim ={}
  

  #  latexfitresults(upLim,filename,resultname,'S4')
  #  latexfitresults(upLim,filename,resultname,'S3,S4')


