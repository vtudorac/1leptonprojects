#################################################################################################
## This is the script for validation pull plots (inclusive 1-lepton analysis as an example)  ^-^
##  
## 1) please put the path of your input tables: Yields tables from background fit
## 2) please put your region names (each region name corresponding to one yields table)
## 3) python note_pullplot_1L_RUN2.py
## 4) the pull plots will be saved in figures/
#################################################################################################

#!/bin/env python
import ROOT
import math
from math import *
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("/afs/cern.ch/atlas/project/cern/susy2/SPyRoot/susy12a_razor_p1328/HistFitter/macros/AtlasStyle.C")
ROOT.SetAtlasStyle()
from ROOT import kAzure, kBlue, kGreen

### please choose the turn on of 'VR' or 'CR'  ###
region = 'VR' 
#region = 'CR' ## can also check CR. although they are ~ 0 after fit.

### please put your region names below ###
if 'VR' in region:
    sampleregions = ['VR_4Jhighxmeff','VR_4Jhighxmt','VR_4Jlowxaplanarity','VR_4Jlowxmt','VR_5Japlanarity','VR_5Jmt','VR_6Japlanarity','VR_6Jmt']
if 'CR' in region:
    sampleregions = ['CR_4Jhighx','CR_4Jlowx','CR_5J','CR_6J']

VR_obs=[]
VR_fit=[]
VR_err=[]
VR_exp=[]
VR_exp_err=[]
VR_obs_err=[]

for sample in sampleregions:
### please put the path to your input tables here ###
    afile = open( 'tables_1.7ifb/MyTable1Lep'+sample+'.tex','r') ### Input tables

    for aline in afile.readlines():
        if 'Observed events' in aline:
            #print "3: ",ROOT.Double(aline.split()[3][1:-1])
            #print "5: ",ROOT.Double(aline.split()[5][1:-1])
            #print "7: ",ROOT.Double(aline.split()[7][1:-1])
            VR_obs.append( ROOT.Double(aline.split()[3][1:-1]) )
            VR_obs_err.append( sqrt(ROOT.Double(aline.split()[3][1:-1])) )
            VR_obs.append( ROOT.Double(aline.split()[5][1:-1]) )
            VR_obs_err.append( sqrt(ROOT.Double(aline.split()[5][1:-1])) )
            if 'VR' in region:
                VR_obs.append( ROOT.Double(aline.split()[7][1:-1]) )
                VR_obs_err.append( sqrt(ROOT.Double(aline.split()[7][1:-1])) )

            #print sample,":VR_obs: ",VR_obs
            #print sample,":VR_obs_err: ",VR_obs_err

        if 'Fitted bkg events' in aline:
            #print  "4: ", ROOT.Double(aline.split()[4][1:])
            #print  "6: ", ROOT.Double(aline.split()[6][:-1]) 
            #print  "8: ", ROOT.Double(aline.split()[8][1:])
            #print  "10: ", ROOT.Double(aline.split()[10][:-1])
            #print  "12: ", ROOT.Double(aline.split()[12][1:])
            #print  "14: ", ROOT.Double(aline.split()[14][:-1]) 
            VR_fit.append( ROOT.Double(aline.split()[4][1:]) )
            VR_err.append( ROOT.Double(aline.split()[6][:-1]) )
            VR_fit.append( ROOT.Double(aline.split()[8][1:]) )
            VR_err.append( ROOT.Double(aline.split()[10][:-1]) )
            if 'VR' in region:
                VR_fit.append( ROOT.Double(aline.split()[12][1:]) )
                VR_err.append( ROOT.Double(aline.split()[14][:-1]) )
            
            #print sample,":VR_fit: ",VR_fit
            #print sample,":VR_err: ",VR_err

        if 'MC exp. SM events' in aline:
            #print "MC 5: ",ROOT.Double(aline.split()[5][1:-1])
            #print "MC 7: ",ROOT.Double(aline.split()[7][:-1])
            #print "MC 9: ",ROOT.Double(aline.split()[9][1:-1])
            #print "MC 11: ",ROOT.Double(aline.split()[11][:-1])
            #print "MC 13: ",ROOT.Double(aline.split()[13][1:-1])
            #print "MC 15: ",ROOT.Double(aline.split()[15][:-1])
            VR_exp.append( ROOT.Double(aline.split()[5][1:-1]) )
            VR_exp_err.append( ROOT.Double(aline.split()[7][:-1]) )
            VR_exp.append( ROOT.Double(aline.split()[9][1:-1]) )
            VR_exp_err.append( ROOT.Double(aline.split()[11][:-1]) )

            if 'VR' in region:
                VR_exp.append( ROOT.Double(aline.split()[13][1:-1]) )
                VR_exp_err.append( ROOT.Double(aline.split()[15][:-1]) )
            
            #print sample,":VR_exp: ",VR_exp
            #print sample,":VR_exp_err: ",VR_exp_err


if 'CR' in region:
    # control regions
    VRs = ['1L 4-jet highx T-CR', '1L 4-jet highx W-CR',
           '1L 4-jet lowx T-CR', '1L 4-jet lowx W-CR', 
           '1L 5-jet T-CR', '1L 5-jet W-CR', 
           '1L 6-jet T-CR', '1L 6-jet W-CR', 
           ] 
elif 'VR' in region:
    # validation regions
    VRs = ['1L 4-jet highx m_{eff} (e+#mu)','1L 4-jet highx m_{eff} (e)','1L 4-jet highx m_{eff} (#mu)',  
           '1L 4-jet highx m_{T} (e+#mu)','1L 4-jet highx m_{T} (e)','1L 4-jet highx m_{T} (#mu)',  
           '1L 4-jet lowx aplanarity (e+#mu)','1L 4-jet lowx aplanarity (e)','1L 4-jet lowx aplanarity (#mu)',  
           '1L 4-jet lowx m_{T} (e+#mu)','1L 4-jet lowx m_{T} (e)','1L 4-jet lowx m_{T} (#mu)',  
           '1L 5-jet aplanarity (e+#mu)','1L 5-jet aplanarity (e)','1L 5-jet aplanarity (#mu)',  
           '1L 5-jet m_{T} (e+#mu)','1L 5-jet m_{T} (e)','1L 5-jet m_{T} (#mu)',  
           '1L 6-jet aplanarity (e+#mu)','1L 6-jet aplanarity (e)','1L 6-jet aplanarity (#mu)',  
           '1L 6-jet m_{T} (e+#mu)','1L 6-jet m_{T} (e)','1L 6-jet m_{T} (#mu)']


VR_sd_before = []
VR_sd_after = []

for i in xrange(len(VRs)):
    VR_sd_after += [(VR_obs[i] - VR_fit[i]) / (sqrt(VR_obs_err[i]**2 + VR_err[i]**2))]
    VR_sd_before += [(VR_obs[i] - VR_exp[i]) / (sqrt(VR_obs_err[i]**2 + VR_err[i]**2))]

print ":VR_obs: ",VR_obs                                                                                                                 
print ":VR_obs_err: ",VR_obs_err   
print "VR_fit: ",VR_fit
print "VR_err: ",VR_err  
print "VR_exp: ",VR_exp
print "VR_exp_err: ",VR_exp_err
print "VR_sd_after:", VR_sd_after
print "VR_sd_before:", VR_sd_before


print 'what is region?  ',region
c = ROOT.TCanvas('Pulls_hard_1lepton','',1200,600)

#c.SetBottomMargin(0.42)
c.SetBottomMargin(0.55)
c.SetTopMargin(0.03)
c.SetRightMargin(0.02)
c.SetLeftMargin(0.06)

if 'VR' in region:
    frame = ROOT.TH2D('frame_1lepton_'+region,'',len(VRs),-0.5,len(VRs)-0.5,5,-4,4)
elif 'CR' in region:
    frame = ROOT.TH2D('frame_1lepton_'+region,'',len(VRs),-0.5,len(VRs)-0.5,5,-1,1)
frame.SetYTitle('(n_{obs} - n_{pred}) / #sigma_{tot}') #'Deviations per region')
frame.GetYaxis().CenterTitle()
frame.SetXTitle('')

hafterfit = ROOT.TH1D('fit_pulls','',len(VRs),-0.5,len(VRs)-0.5)#frame.Clone()
hafterfit_4Ja = ROOT.TH1D('fit_pulls_VR4Ja','',len(VRs),-0.5,len(VRs)-0.5)#frame.Clone()
hafterfit_4Jb = ROOT.TH1D('fit_pulls_VR4Jb','',len(VRs),-0.5,len(VRs)-0.5)#frame.Clone()
hafterfit_5Ja = ROOT.TH1D('fit_pulls_VR5Ja','',len(VRs),-0.5,len(VRs)-0.5)#frame.Clone()
hafterfit_6Ja = ROOT.TH1D('fit_pulls_VR6Ja','',len(VRs),-0.5,len(VRs)-0.5)#frame.Clone()

for abin in xrange(len(VRs)):
    print "VR = ", abin
    print "VRs[abin]=", VRs[abin]

    if '4-jet high' in VRs[abin]:
        hafterfit_4Ja.SetBinContent(abin+1, VR_sd_after[abin])
        frame.GetXaxis().SetBinLabel( abin+1 , VRs[abin] )
    elif '4-jet low' in VRs[abin]:
        hafterfit_4Jb.SetBinContent(abin+1, VR_sd_after[abin])
        frame.GetXaxis().SetBinLabel( abin+1 , VRs[abin] )
    elif '5-jet' in VRs[abin]:
        hafterfit_5Ja.SetBinContent(abin+1, VR_sd_after[abin])
        frame.GetXaxis().SetBinLabel( abin+1 , VRs[abin] )
    elif '6-jet' in VRs[abin]:
        hafterfit_6Ja.SetBinContent(abin+1, VR_sd_after[abin])
        frame.GetXaxis().SetBinLabel( abin+1 , VRs[abin] )
        
        
frame.GetXaxis().LabelsOption('v')
frame.GetXaxis().SetLabelSize( frame.GetXaxis().GetLabelSize()*1.3 )
frame.Draw()
frame.GetYaxis().SetTitleOffset(0.5)

hafterfit_4Ja.SetFillColor(kBlue)
hafterfit_4Ja.SetLineColor(kBlue)
hafterfit_4Ja.Draw("Bsame")

hafterfit_4Jb.SetFillColor(kBlue-10)
hafterfit_4Jb.SetLineColor(kBlue-10)
hafterfit_4Jb.Draw("Bsame")

hafterfit_5Ja.SetFillColor(kBlue+2)
hafterfit_5Ja.SetLineColor(kBlue+2)
hafterfit_5Ja.Draw("Bsame")

hafterfit_6Ja.SetFillColor(kBlue-6)
hafterfit_6Ja.SetLineColor(kBlue-6)
hafterfit_6Ja.Draw("Bsame")

#sep = ROOT.TLine(len(VRs)*0.8325,-3,len(VRs)*0.8325,3)
#sep.SetLineStyle(3)
#sep.Draw('same')

pone = ROOT.TLine(-0.5,1,len(VRs)-0.5,1)
pone.SetLineStyle(3)
pone.Draw('same')
zero = ROOT.TLine(-0.5,0,len(VRs)-0.5,0)
zero.SetLineStyle(3)
zero.Draw('same')
mone = ROOT.TLine(-0.5,-1,len(VRs)-0.5,-1)
mone.SetLineStyle(3)
mone.Draw('same')


if 'VR' in region: 
    ana = ROOT.TLatex( 7, 1.3, "Hard lepton(s) Validation Regions")#
    lumi = ROOT.TLatex( 7, 1.95, "#sqrt{s}=13 TeV, 1.7 fb^{-1}")
    prel = ROOT.TLatex(7, 2.7,"#it{#bf{ATLAS}} Internal")#Internal")
elif 'CR' in region: 
    ana = ROOT.TLatex( 3, 0.2, "Hard lepton(s) Control Regions")#
    lumi = ROOT.TLatex( 3, 0.5, "#sqrt{s}=13 TeV, 1.7 fb^{-1}")
    prel = ROOT.TLatex(3, 0.8,"#it{#bf{ATLAS}} Internal")#Internal")

prel.SetTextAlign( 11 )
prel.SetTextSize( 0.045 )
prel.SetTextColor( 1 )
prel.SetTextFont( 42 )
prel.Draw()

lumi.SetTextAlign( 11 )
lumi.SetTextSize( 0.045 )
lumi.SetTextColor( 1 )
lumi.SetTextFont( 42 )
lumi.Draw()

ana.SetTextAlign( 11 )
ana.SetTextSize( 0.045 )
ana.SetTextColor( 1 )
ana.SetTextFont( 42 )
ana.Draw()

ROOT.gROOT.SetStyle("Plain")
#ROOT.gStyle.SetLegendBorderSize(0)
#ROOT.gStyle.SetFrameBorderMode(0)
#ROOT.gStyle.SetCanvasBorderMode(0)
#ROOT.gStyle.SetPadBorderMode(0)
#ROOT.gStyle.SetOptStat(0)

c.SaveAs('figures/pull_Hard1L_'+region+'s'+'.eps')
c.SaveAs('figures/pull_Hard1L_'+region+'s'+'.pdf')
