#!/bin/env python

import ROOT,sys,os
from ROOT import *
ROOT.SetSignalPolicy( ROOT.kSignalFast )
ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.LoadMacro("/afs/cern.ch/atlas/project/cern/susy2/SPyRoot/susy12a_razor_p1328/HistFitter/macros/AtlasStyle.C")
gROOT.LoadMacro("AtlasStyle.C")
ROOT.SetAtlasStyle()

if len(sys.argv)>1:
    if sys.argv[1]!='-b': infiles = sys.argv[1]
    elif len(sys.argv)>2: infiles = sys.argv[2]
    else:
        print 'Please specify an input file'
        sys.exit(1)
print 'Using files',infiles


#doSignal = False
#for doSignal in [True,False]:
for doSignal in [False]:
#for aset in [ 'test_Razor_2leptonFit_newBase']:
    aset = infiles
	      
    uncRes_2J = [[],[],[],[]]
    uncRes_4Jlowx = [[],[],[],[]]
    uncRes_4Jhighx = [[],[],[],[]]
    uncRes_6J = [[],[],[],[]]
    
    uncRes= [[],[],[],[]]
    
    #	     log = open( 'logs/test_'+aset+('' if not doSignal else '_signal')+'.log' , 'r' )
    log = open( 'logs/'+aset+('' if not doSignal else '_signal'), 'r' )
    switchOn = False
    muIDX_2J= -1
    muIDX_4Jlowx= -1
    muIDX_4Jhighx= -1
    muIDX_6J= -1
    muIDX= -1
    for aline in log.readlines():
        if 'FinalValue' in aline:
            switchOn = True	    	      
            uncRes_2J = [[],[],[],[]]
            uncRes_4Jlowx = [[],[],[],[]]
            uncRes_4Jhighx = [[],[],[],[]]
            uncRes_6J = [[],[],[],[]]
            uncRes = [[],[],[],[]]
            muIDX_2J= -1
            muIDX_4Jlowx= -1
            muIDX_4Jhighx= -1
            muIDX_6J= -1
            muIDX= -1
            continue
        if '------------' in aline: continue
        if switchOn:	    
            if 'gamma_stat' in aline: continue
            if 'QCDNorm' in aline: continue
            if 'mu_' in aline and muIDX_2J<0:
                muIDX_2J=len(uncRes_2J[0])
            if 'mu_' in aline and muIDX_4Jlowx<0:
                muIDX_4Jlowx=len(uncRes_4Jlowx[0])	
            if 'mu_' in aline and muIDX_4Jhighx<0:
                muIDX_4Jhighx=len(uncRes_4Jhighx[0])	
            if 'mu_' in aline and muIDX_6J<0:
                muIDX_6J=len(uncRes_6J[0])
            if 'mu_' in aline and muIDX<0:
                muIDX=len(uncRes[0])			
            if len(aline.strip())<3:
                switchOn=False
                continue #break
			  
	    #		      alpha_BJes    3.3586e-01 +/-  5.94e-01

	    #print aline
            if '2J' in aline: 
               if 'mu_' in aline:
                    uncRes_2J[0] += [ aline.split()[0]+'-1' ]
                    uncRes_2J[1] += [ float(aline.split()[2-(0 if doSignal else 0)])-1. ]
               if 'alpha_' in aline:
                    uncRes_2J[0] += [ aline.split()[0].replace('alpha_','') ]
                    uncRes_2J[1] += [ float(aline.split()[2-(0 if doSignal else 0)]) ]
               if ' +/- ' in aline and '2J':
                    uncRes_2J[2] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
                    uncRes_2J[3] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
               else:
                    if len(aline.strip())<2: continue
		    #print "aline: ", aline
                    brief = (aline.split()[3-(0 if doSignal else 0)].split('(')[1].split(')')[0]).replace('--','-')
                    uncRes_2J[2] += [ abs(float(brief.split(',')[0])) ]
                    uncRes_2J[3] += [ abs(float(brief.split(',')[1])) ]
                    if uncRes_2J[2][-1]<0.00001: uncRes_2J[2][-1]=uncRes_2J[3][-1]
                    if uncRes_2J[3][-1]<0.00001: uncRes_2J[3][-1]=uncRes_2J[2][-1]
		    
            if'4Jlowx' in aline: 
               if 'mu_' in aline:
                    uncRes_4Jlowx[0] += [ aline.split()[0]+'-1' ]
                    uncRes_4Jlowx[1] += [ float(aline.split()[2-(0 if doSignal else 0)])-1. ]
               if 'alpha_' in aline:
                    uncRes_4Jlowx[0] += [ aline.split()[0].replace('alpha_','') ]
                    uncRes_4Jlowx[1] += [ float(aline.split()[2-(0 if doSignal else 0)]) ]
               if ' +/- ' in aline:
                    uncRes_4Jlowx[2] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
                    uncRes_4Jlowx[3] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
               else:
                    if len(aline.strip())<2: continue
                    #print "aline: ", aline
                    brief = (aline.split()[3-(0 if doSignal else 0)].split('(')[1].split(')')[0]).replace('--','-')
                    uncRes_4Jlowx[2] += [ abs(float(brief.split(',')[0])) ]
                    uncRes_4Jlowx[3] += [ abs(float(brief.split(',')[1])) ]
                    if uncRes_4Jlowx[2][-1]<0.00001: uncRes_4Jlowx[2][-1]=uncRes_4Jlowx[3][-1]
                    if uncRes_4Jlowx[3][-1]<0.00001: uncRes_4Jlowx[3][-1]=uncRes_4Jlowx[2][-1]
		    	    
            if '4Jhighx' in aline: 
               if 'mu_' in aline:
                    uncRes_4Jhighx[0] += [ aline.split()[0]+'-1' ]
                    uncRes_4Jhighx[1] += [ float(aline.split()[2-(0 if doSignal else 0)])-1. ]
               if 'alpha_' in aline:
                    uncRes_4Jhighx[0] += [ aline.split()[0].replace('alpha_','') ]
                    uncRes_4Jhighx[1] += [ float(aline.split()[2-(0 if doSignal else 0)]) ]
               if ' +/- ' in aline:
                    uncRes_4Jhighx[2] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
                    uncRes_4Jhighx[3] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
               else:
                    if len(aline.strip())<2: continue
                    #print "aline: ", aline
                    brief = (aline.split()[3-(0 if doSignal else 0)].split('(')[1].split(')')[0]).replace('--','-')
                    uncRes_4Jhighx[2] += [ abs(float(brief.split(',')[0])) ]
                    uncRes_4Jhighx[3] += [ abs(float(brief.split(',')[1])) ]
                    if uncRes_4Jhighx[2][-1]<0.00001: uncRes_4Jhighx[2][-1]=uncRes_4Jhighx[3][-1]
                    if uncRes_4Jhighx[3][-1]<0.00001: uncRes_4Jhighx[3][-1]=uncRes_4Jhighx[2][-1]
		
		
            if '6J' in aline: 
               if 'mu_' in aline:
                    uncRes_6J[0] += [ aline.split()[0]+'-1' ]
                    uncRes_6J[1] += [ float(aline.split()[2-(0 if doSignal else 0)])-1. ]
               if 'alpha_' in aline:
                    uncRes_6J[0] += [ aline.split()[0].replace('alpha_','') ]
                    uncRes_6J[1] += [ float(aline.split()[2-(0 if doSignal else 0)]) ]
               if ' +/- ' in aline:
                    uncRes_6J[2] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
                    uncRes_6J[3] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
               else:
                    if len(aline.strip())<2: continue
                    #print "aline: ", aline
                    brief = (aline.split()[3-(0 if doSignal else 0)].split('(')[1].split(')')[0]).replace('--','-')
                    uncRes_6J[2] += [ abs(float(brief.split(',')[0])) ]
                    uncRes_6J[3] += [ abs(float(brief.split(',')[1])) ]
                    if uncRes_6J[2][-1]<0.00001: uncRes_6J[2][-1]=uncRes_6J[3][-1]
                    if uncRes_6J[3][-1]<0.00001: uncRes_6J[3][-1]=uncRes_6J[2][-1]
		    
            if not '6J' in aline:
             if not '2J' in aline:
               if not'4J' in aline: 
                 if 'mu_' in aline:
                    uncRes[0] += [ aline.split()[0]+'-1' ]
                    uncRes[1] += [ float(aline.split()[2-(0 if doSignal else 0)])-1. ]
                 if 'alpha_' in aline:
                    uncRes[0] += [ aline.split()[0].replace('alpha_','') ]
                    uncRes[1] += [ float(aline.split()[2-(0 if doSignal else 0)]) ]
                 if ' +/- ' in aline:
                    uncRes[2] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
                    uncRes[3] += [ float(aline.split()[4-(0 if doSignal else 0)]) ]
                 else:
                    if len(aline.strip())<2: continue
                    #print "aline: ", aline
                    brief = (aline.split()[3-(0 if doSignal else 0)].split('(')[1].split(')')[0]).replace('--','-')
                    uncRes[2] += [ abs(float(brief.split(',')[0])) ]
                    uncRes[3] += [ abs(float(brief.split(',')[1])) ]
                    if uncRes[2][-1]<0.00001: uncRes[2][-1]=uncRes[3][-1]
                    if uncRes[3][-1]<0.00001: uncRes[3][-1]=uncRes[2][-1]
	  	    	    	    	
		    

    if len(uncRes_2J[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue
    if len(uncRes_4Jlowx[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue
    if len(uncRes_4Jhighx[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue

    if len(uncRes_6J[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue	
    
    
    if len(uncRes[1])==0:
        print 'Set',aset,'failed - no errors found.  Continuing'
        continue
	
    from array import array
    y_2J = array('d',uncRes_2J[1])
    yep_2J = array('d',uncRes_2J[2])
    yem_2J = array('d',uncRes_2J[3])
    x_2J = array('d', [ a for a in xrange( len(uncRes_2J[1]) ) ] )
    xe_2J = array('d', [ 0 for a in xrange( len(uncRes_2J[1]) ) ] )
    
    y_4Jlowx = array('d',uncRes_4Jlowx[1])
    yep_4Jlowx = array('d',uncRes_4Jlowx[2])
    yem_4Jlowx = array('d',uncRes_4Jlowx[3])
    x_4Jlowx = array('d', [ a for a in xrange( len(uncRes_4Jlowx[1]) ) ] )
    xe_4Jlowx = array('d', [ 0 for a in xrange( len(uncRes_4Jlowx[1]) ) ] )
  
    y_4Jhighx = array('d',uncRes_4Jhighx[1])
    yep_4Jhighx = array('d',uncRes_4Jhighx[2])
    yem_4Jhighx = array('d',uncRes_4Jhighx[3])
    x_4Jhighx = array('d', [ a for a in xrange( len(uncRes_4Jhighx[1]) ) ] )
    xe_4Jhighx = array('d', [ 0 for a in xrange( len(uncRes_4Jhighx[1]) ) ] ) 
    
    y_6J = array('d',uncRes_6J[1])
    yep_6J = array('d',uncRes_6J[2])
    yem_6J = array('d',uncRes_6J[3])
    x_6J = array('d', [ a for a in xrange( len(uncRes_6J[1]) ) ] )
    xe_6J = array('d', [ 0 for a in xrange( len(uncRes_6J[1]) ) ] ) 
    
    
    y= array('d',uncRes[1])
    yep= array('d',uncRes[2])
    yem = array('d',uncRes[3])
    x= array('d', [ a for a in xrange( len(uncRes[1]) ) ] )
    xe = array('d', [ 0 for a in xrange( len(uncRes[1]) ) ] ) 
    
    if uncRes_2J:
    
       c_2J = ROOT.TCanvas('Pulls_'+aset+"2J",'',1200,600)
       c_2J.SetBottomMargin(0.42)
       c_2J.SetTopMargin(0.03)
       c_2J.SetRightMargin(0.02)
       c_2J.SetLeftMargin(0.06)
    
       frame = ROOT.TH2D('frame_'+aset+"2J",'',len(uncRes_2J[1]),-0.5,len(uncRes_2J[1])-0.5,5,-3,3)   
       frame.SetYTitle('Uncertainty After Fit')
       frame.GetXaxis().SetLabelSize(0.025)
       frame.SetXTitle('')
       frame.Draw()
       frame.GetYaxis().SetTitleOffset(0.5)
    
       eg = ROOT.TGraphAsymmErrors(len(uncRes_2J[1]),x_2J,y_2J,xe_2J,xe_2J,yem_2J,yep_2J)
       eg.Draw('sameP')
    
       pone = ROOT.TLine(-0.5,1,len(uncRes_2J[1])-0.5,1)
       pone.SetLineStyle(3)
       pone.Draw('same')
       zero = ROOT.TLine(-0.5,0,len(uncRes_2J[1])-0.5,0)
       zero.SetLineStyle(2)
       zero.Draw('same')
       mone = ROOT.TLine(-0.5,-1,len(uncRes_2J[1])-0.5,-1)
       mone.SetLineStyle(3)
       mone.Draw('same')

       mul = ROOT.TLine(muIDX_2J-0.5,-3,muIDX_2J-0.5,3)
       mul.SetLineStyle(4)
       mul.Draw('same')
    
       for abin in xrange(len(uncRes_2J[1])):
       	     frame.GetXaxis().SetBinLabel( abin+1 , uncRes_2J[0][abin] )
	     #print uncRes[0][abin] 
       frame.GetXaxis().LabelsOption('v')
       #if  uncRes_2J[0][abin]:   
       c_2J.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_2J'+'.eps')
       #c_2J.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_2J'+'.pdf')
	
    if uncRes_4Jlowx:
    
       c_4Jlowx = ROOT.TCanvas('Pulls_'+aset+"4Jlowx",'',1200,600)
       c_4Jlowx.SetBottomMargin(0.42)
       c_4Jlowx.SetTopMargin(0.03)
       c_4Jlowx.SetRightMargin(0.02)
       c_4Jlowx.SetLeftMargin(0.06)
    
       frame = ROOT.TH2D('frame_'+aset+"4Jlowx",'',len(uncRes_4Jlowx[1]),-0.5,len(uncRes_4Jlowx[1])-0.5,5,-3,3)   
       frame.SetYTitle('Uncertainty After Fit')
       frame.GetXaxis().SetLabelSize(0.025)
       frame.SetXTitle('')
       frame.Draw()
       frame.GetYaxis().SetTitleOffset(0.5)
    
       eg = ROOT.TGraphAsymmErrors(len(uncRes_4Jlowx[1]),x_4Jlowx,y_4Jlowx,xe_4Jlowx,xe_4Jlowx,yem_4Jlowx,yep_4Jlowx)
       eg.Draw('sameP')
    
       pone = ROOT.TLine(-0.5,1,len(uncRes_4Jlowx[1])-0.5,1)
       pone.SetLineStyle(3)
       pone.Draw('same')
       zero = ROOT.TLine(-0.5,0,len(uncRes_4Jlowx[1])-0.5,0)
       zero.SetLineStyle(2)
       zero.Draw('same')
       mone = ROOT.TLine(-0.5,-1,len(uncRes_4Jlowx[1])-0.5,-1)
       mone.SetLineStyle(3)
       mone.Draw('same')

       mul = ROOT.TLine(muIDX_4Jlowx-0.5,-3,muIDX_4Jlowx-0.5,3)
       mul.SetLineStyle(4)
       mul.Draw('same')
    
       for abin in xrange(len(uncRes_4Jlowx[1])):
       	     frame.GetXaxis().SetBinLabel( abin+1 , uncRes_4Jlowx[0][abin] )
	     #print uncRes[0][abin] 
       frame.GetXaxis().LabelsOption('v')
       #if  uncRes_4Jlowx[0][abin]:   
       c_4Jlowx.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_4Jlowx'+'.eps')
       #c_4Jlowx.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_4Jlowx'+'.pdf')	

    if uncRes_4Jhighx:
    
       c_4Jhighx = ROOT.TCanvas('Pulls_'+aset+"4Jhighx",'',1200,600)
       c_4Jhighx.SetBottomMargin(0.42)
       c_4Jhighx.SetTopMargin(0.03)
       c_4Jhighx.SetRightMargin(0.02)
       c_4Jhighx.SetLeftMargin(0.06)
    
       frame = ROOT.TH2D('frame_'+aset+"4Jhighx",'',len(uncRes_4Jhighx[1]),-0.5,len(uncRes_4Jhighx[1])-0.5,5,-3,3)   
       frame.SetYTitle('Uncertainty After Fit')
       frame.GetXaxis().SetLabelSize(0.025)
       frame.SetXTitle('')
       frame.Draw()
       frame.GetYaxis().SetTitleOffset(0.5)
    
       eg = ROOT.TGraphAsymmErrors(len(uncRes_4Jhighx[1]),x_4Jhighx,y_4Jhighx,xe_4Jhighx,xe_4Jhighx,yem_4Jhighx,yep_4Jhighx)
       eg.Draw('sameP')
    
       pone = ROOT.TLine(-0.5,1,len(uncRes_4Jhighx[1])-0.5,1)
       pone.SetLineStyle(3)
       pone.Draw('same')
       zero = ROOT.TLine(-0.5,0,len(uncRes_4Jhighx[1])-0.5,0)
       zero.SetLineStyle(2)
       zero.Draw('same')
       mone = ROOT.TLine(-0.5,-1,len(uncRes_4Jhighx[1])-0.5,-1)
       mone.SetLineStyle(3)
       mone.Draw('same')

       mul = ROOT.TLine(muIDX_4Jhighx-0.5,-3,muIDX_4Jhighx-0.5,3)
       mul.SetLineStyle(4)
       mul.Draw('same')
    
       for abin in xrange(len(uncRes_4Jhighx[1])):
       	     frame.GetXaxis().SetBinLabel( abin+1 , uncRes_4Jhighx[0][abin] )
	     #print uncRes[0][abin] 
       frame.GetXaxis().LabelsOption('v')
       #if  uncRes_4Jhighx[0][abin]:   
       c_4Jhighx.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_4Jhighx'+'.eps')
       #c_4Jhighx.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_4Jhighx'+'.pdf')	

    if uncRes_6J:
    
       c_6J = ROOT.TCanvas('Pulls_'+aset+"6J",'',1200,600)
       c_6J.SetBottomMargin(0.42)
       c_6J.SetTopMargin(0.03)
       c_6J.SetRightMargin(0.02)
       c_6J.SetLeftMargin(0.06)
    
       frame = ROOT.TH2D('frame_'+aset+"6J",'',len(uncRes_6J[1]),-0.5,len(uncRes_6J[1])-0.5,5,-3,3)   
       frame.SetYTitle('Uncertainty After Fit')
       frame.GetXaxis().SetLabelSize(0.025)
       frame.SetXTitle('')
       frame.Draw()
       frame.GetYaxis().SetTitleOffset(0.5)
    
       eg = ROOT.TGraphAsymmErrors(len(uncRes_6J[1]),x_6J,y_6J,xe_6J,xe_6J,yem_6J,yep_6J)
       eg.Draw('sameP')
    
       pone = ROOT.TLine(-0.5,1,len(uncRes_6J[1])-0.5,1)
       pone.SetLineStyle(3)
       pone.Draw('same')
       zero = ROOT.TLine(-0.5,0,len(uncRes_6J[1])-0.5,0)
       zero.SetLineStyle(2)
       zero.Draw('same')
       mone = ROOT.TLine(-0.5,-1,len(uncRes_6J[1])-0.5,-1)
       mone.SetLineStyle(3)
       mone.Draw('same')

       mul = ROOT.TLine(muIDX_6J-0.5,-3,muIDX_6J-0.5,3)
       mul.SetLineStyle(4)
       mul.Draw('same')
    
       for abin in xrange(len(uncRes_6J[1])):
       	     frame.GetXaxis().SetBinLabel( abin+1 , uncRes_6J[0][abin] )
	     #print uncRes[0][abin] 
       frame.GetXaxis().LabelsOption('v')
       #if  uncRes_6J[0][abin]:   
       c_6J.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_6J'+'.eps')
       #c_6J.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+'_6J'+'.pdf')	


    if uncRes:
    
       c = ROOT.TCanvas('Pulls_'+aset+"NoJetMult",'',1200,600)
       c.SetBottomMargin(0.42)
       c.SetTopMargin(0.03)
       c.SetRightMargin(0.02)
       c.SetLeftMargin(0.06)
    
       frame = ROOT.TH2D('frame_'+aset+"NoJetMult",'',len(uncRes[1]),-0.5,len(uncRes[1])-0.5,5,-3,3)   
       frame.SetYTitle('Uncertainty After Fit')
       frame.GetXaxis().SetLabelSize(0.025)
       frame.SetXTitle('')
       frame.Draw()
       frame.GetYaxis().SetTitleOffset(0.5)
    
       eg = ROOT.TGraphAsymmErrors(len(uncRes[1]),x,y,xe,xe,yem,yep)
       eg.Draw('sameP')
    
       pone = ROOT.TLine(-0.5,1,len(uncRes[1])-0.5,1)
       pone.SetLineStyle(3)
       pone.Draw('same')
       zero = ROOT.TLine(-0.5,0,len(uncRes[1])-0.5,0)
       zero.SetLineStyle(2)
       zero.Draw('same')
       mone = ROOT.TLine(-0.5,-1,len(uncRes[1])-0.5,-1)
       mone.SetLineStyle(3)
       mone.Draw('same')

       mul = ROOT.TLine(muIDX-0.5,-3,muIDX-0.5,3)
       mul.SetLineStyle(4)
       mul.Draw('same')
    
       for abin in xrange(len(uncRes[1])):
       	     frame.GetXaxis().SetBinLabel( abin+1 , uncRes[0][abin] )
	     #print uncRes[0][abin] 
       frame.GetXaxis().LabelsOption('v')
       #if  uncRes[0][abin]:   
       c.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+''+'.eps')
       #c.SaveAs('pull_plots/pull_afterFit_test'+(aset.replace('.','_'))+('' if not doSignal else '_signal')+''+'.pdf')
