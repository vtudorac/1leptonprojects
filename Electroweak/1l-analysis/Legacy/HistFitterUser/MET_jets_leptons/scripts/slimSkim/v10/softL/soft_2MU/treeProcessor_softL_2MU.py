import ROOT,os,sys,shutil

class treeProcessor:

    def __init__(this):
        print
        this.SkimCut = []
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_1_2"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_4_6"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v5_5_1"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v6_1_2"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_2_3"
        # this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_3_1"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_3_1/NoCloseBy"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v10_5"
        this.inputFolder  = "root://eosatlas//eos/atlas/user/t/tnobe/p1328/v10_1_1_slim"
        this.outputFolder = "trees"
        this.fileList = ["Soft2Lep_%s.root"%(x) for x in ["AlpgenDY","PowhegDibosons","PowhegPythiaTTbar","SherpaZMassiveBC","SingleTopDiLept","ttbarV"]] #,"SherpaDibosons","SherpaWMassiveBC"
        #        this.fileList = ["SherpaWMassiveB_%s.root"%(x) for x in ["HardEle"]]
        #       this.fileList = ["PowhegPythiaTTbar_%s.root"%(x) for x in ["HardEle","HardMuo"]]
        #        this.fileList = ["datatree_%s.root"%(x) for x in ["HardEle","HardMuo"]]
        this.sampleList = []
        this.dividerList = {}
        this.RemoveBranchList = []
        this.KeepBranchList = []

        this.CheckOutFolder()

    def CheckOutFolder(this):
        if os.path.exists(this.outputFolder):
            shutil.rmtree(this.outputFolder)
            os.mkdir(this.outputFolder)
            print "Delete and Created Folder :",this.outputFolder
        else:
            os.mkdir(this.outputFolder)
            print "Created Folder :",this.outputFolder
        print
        return

    def SetSample(this,sam,div={}):
        this.sampleList.append(sam)
        mydiv = {}
        for d in div:
            if type(div[d])==type("") : mydiv[d] = [div[d]]
            else                   : mydiv[d] = div[d]
        this.dividerList[sam] = mydiv
        print "Add Sample :",sam," Divider=",str(mydiv)
        print
        return

    def SetSkim(this,skim):
        this.SkimCut = skim
        print "Skim =",str(skim)
        return

    def CutString(this,Cut):
        if Cut:
            myStr = "("
            for c in Cut:
                myStr += " ( %s ) &&"%c
            myStr = myStr[:-2] + ")"
        else:
            myStr = ""
        return myStr

    def SetRemoveBranches(this,branch):
        if type(branch)==type([]):
            this.RemoveBranchList += branch
        else:
            this.RemoveBranchList += [branch]
        return

    def SetKeepBranches(this,branch):
        if type(branch)==type([]):
            this.KeepBranchList += branch
        else:
            this.KeepBranchList += [branch]
        return

    def ProcessOne(this,inFileName,inTreeName,outFileName,Divider={}):
        inFile = ROOT.TFile.Open(inFileName)
        allkeys = inFile.GetListOfKeys()
        next = ROOT.TIter(allkeys)
        outFile = ROOT.TFile(outFileName,"Update")
        while True:
            obj = next()
            if not obj : break
            inName = obj.GetName()
            if not inName.startswith(inTreeName) : continue
            inTree = inFile.Get(inName)

            # Disable Branch
            for item in this.RemoveBranchList:
                inTree.SetBranchStatus(item,0)

            # Keep Branch
            if len(this.KeepBranchList)>0:

                # Check keepBranches list
                braList = inTree.GetListOfBranches()
                for item in this.KeepBranchList:
                    if item in braList:
                        continue
                    else:
                        print "\n WARNING -->   In tree '",inTreeName,"' requested branch to be kept: '", item, "' is not in Tree"
            
                for bra in inTree.GetListOfBranches():
                    braName =  bra.GetName()
                    #print "branchName = ", braName
                    if braName in this.KeepBranchList:
                        #   print "keeping"
                        continue
                    else:
                        #  print "removing"
                        inTree.SetBranchStatus(braName,0)
                    
                    
            print inName,"(",inFileName,") EvNum =",inTree.GetEntries()
            if Divider:
                for item in Divider:
                    newName = inName
                    newName = newName.split("_")
                    newName = newName[:-1]+[item]+[newName[-1]]
                    newName = "_".join(newName)
                    myCut   = this.SkimCut+Divider[item]
                    print " ->",newName, " : ",myCut,
                    outTree = inTree.CopyTree(this.CutString(myCut))
                    outTree.Write(newName)
                    print " EvNum =",outTree.GetEntries()
            print " ->",inName," : ",this.SkimCut,
            outTree = inTree.CopyTree(this.CutString(this.SkimCut))
            outTree.Write(inName)
            print " EvNum =",outTree.GetEntries()
            print
        inFile.Close()
        outFile.Close()
        return

    def ProcessAll(this):
        for fil in this.fileList:
            for sam in this.sampleList:
                inFileName  = this.inputFolder .rstrip("/")+"/"+fil
                outFileName = this.outputFolder.rstrip("/")+"/"+fil
                inTreeName  = sam
                Divider     = this.dividerList[sam]
                this.ProcessOne(inFileName,inTreeName,outFileName,Divider=Divider)

if __name__=="__main__":
    tp = treeProcessor()
    
    # bool lep2 = lep1Flavor==-1 && lep2Flavor==-1 && jet1Pt_jvf25>80 && nJet25_jvf25>=2; 
    tp.SetSkim(["lep1Flavor==-1","lep2Flavor==-1","jet1Pt_jvf25>80","nJet25_jvf25>=2"])

    # define samples
    tp.SetSample("AlpgenDY")
    #tp.SetSample("SherpaWMassiveBC")
    tp.SetSample("PowhegPythiaTTbar")
    tp.SetSample("ttbarV")
    # tp.SetSample("SherpaDibosons")
    tp.SetSample("PowhegDibosons")
    tp.SetSample("SingleTopDiLept")
    tp.SetSample("SherpaZMassiveBC")

    # define branches to be removed
    #    tp.SetRemoveBranches(["jet1Pt","jet3Pt","jet5Pt","jet6Pt","jet7Pt","jet8Pt"])
    
    # define branches to be kept
    tp.SetKeepBranches(["isQEDFSR","genWeight","pileupWeight","pileupWeightUp","pileupWeightDown","EventNumber","DatasetNumber","met","metPhi","mt","mt2","lep1Charge","lep2Charge","lep1Flavor","lep2Flavor","lep1Origin","lep2Origin","lep1Signal","lep2Signal","lep1Pt","lep2Pt","lep3Pt","lep2Phi","eventWeight","leptonWeight","leptonWeightUp","leptonWeightDown","triggerWeight","triggerWeightUp","triggerWeightDown","bTagWeight","bTagWeightBUp","bTagWeightBDown","bTagWeightCUp","bTagWeightCDown","bTagWeightMUp","bTagWeightMDown","qcdWeight","qcdWeightSyst","qcdWeightStat","qcdBWeight","qcdBWeightSyst","qcdBWeightStat","mll","Truth_mll","AnalysisType","jet1Pt_jvf25","jet2Pt_jvf25","DecayIndexTTbar","EF_xe80_tclcw_loose","EF_xe80T_tclcw_loose","qfacUpWeightTTbar","qfacDownWeightTTbar","ktfacUpWeightTTbar","ktfacDownWeightTTbar","qfacUpWeightW","qfacDownWeightW","ktfacUpWeightW","ktfacDownWeightW","iqopt2WeightW","iqopt3WeightW","nJet25_jvf25","nB3Jet25_MV1_80p_jvf25","meffInc25_jvf25","pdfWeight","pdfWeightVars","dRminLepJet20","dRminLepJet25_jvf25","dRminLep2Jet20","dRminLep2Jet25_jvf25"])
    
    tp.ProcessAll()
