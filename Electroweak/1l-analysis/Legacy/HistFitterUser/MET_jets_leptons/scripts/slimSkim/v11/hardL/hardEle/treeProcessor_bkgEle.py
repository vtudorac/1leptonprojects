import ROOT,os,sys,shutil

class treeProcessor:
    
    def __init__(this):
        print
        this.SkimCut = []
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_1_2"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v4_4_6"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v5_5_1"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v6_1_2"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_2_3"
        # this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_3_1"
        #        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v8_3_1/NoCloseBy"
        this.inputFolder  = "root://eosatlas//eos/atlas/user/y/ysasaki/trees/Moriond/v11_2_3"
        this.outputFolder = "trees"
        this.fileList = ["bkgtree_%s.root"%(x) for x in ["HardEle"]]
        this.fileList.append("HardEle_PowhegJimmyTTbar_NoSys.root")
        #        this.fileList = ["SherpaWMassiveB_%s.root"%(x) for x in ["HardEle"]]
        #       this.fileList = ["PowhegPythiaTTbar_%s.root"%(x) for x in ["HardEle","HardMuo"]]
        #        this.fileList = ["datatree_%s.root"%(x) for x in ["HardEle","HardMuo"]]
        this.sampleList = []
        this.dividerList = {}
        this.RemoveBranchList = []
        this.KeepBranchList = []

        this.CheckOutFolder()

    def CheckOutFolder(this):
        if os.path.exists(this.outputFolder):
            shutil.rmtree(this.outputFolder)
            os.mkdir(this.outputFolder)
            print "Delete and Created Folder :",this.outputFolder
        else:
            os.mkdir(this.outputFolder)
            print "Created Folder :",this.outputFolder
        print
        return

    def SetSample(this,sam,div={}):
        this.sampleList.append(sam)
        mydiv = {}
        for d in div:
            if type(div[d])==type("") : mydiv[d] = [div[d]]
            else                   : mydiv[d] = div[d]
        this.dividerList[sam] = mydiv
        print "Add Sample :",sam," Divider=",str(mydiv)
        print
        return

    def SetSkim(this,skim):
        this.SkimCut = skim
        print "Skim =",str(skim)
        return

    def CutString(this,Cut):
        if Cut:
            myStr = "("
            for c in Cut:
                myStr += " ( %s ) &&"%c
            myStr = myStr[:-2] + ")"
        else:
            myStr = ""
        return myStr

    def SetRemoveBranches(this,branch):
        if type(branch)==type([]):
            this.RemoveBranchList += branch
        else:
            this.RemoveBranchList += [branch]
        return

    def SetKeepBranches(this,branch):
        if type(branch)==type([]):
            this.KeepBranchList += branch
        else:
            this.KeepBranchList += [branch]
        return

    def ProcessOne(this,inFileName,inTreeName,outFileName,Divider={}):
        inFile = ROOT.TFile.Open(inFileName)
        allkeys = inFile.GetListOfKeys()
        next = ROOT.TIter(allkeys)
        outFile = ROOT.TFile(outFileName,"Update")
        while True:
            obj = next()
            if not obj : break
            inName = obj.GetName()
            if not inName.startswith(inTreeName) : continue
            inTree = inFile.Get(inName)

            # Disable Branch
            for item in this.RemoveBranchList:
                inTree.SetBranchStatus(item,0)

            # Keep Branch
            if len(this.KeepBranchList)>0:

                # Check keepBranches list
                braList = inTree.GetListOfBranches()
                for item in this.KeepBranchList:
                    if item in braList:
                        continue
                    else:
                        print "\n WARNING -->   In tree '",inTreeName,"' requested branch to be kept: '", item, "' is not in Tree"
            
                for bra in inTree.GetListOfBranches():
                    braName =  bra.GetName()
                    #print "branchName = ", braName
                    if braName in this.KeepBranchList:
                        #   print "keeping"
                        continue
                    else:
                        #  print "removing"
                        inTree.SetBranchStatus(braName,0)
                    
                    
            print inName,"(",inFileName,") EvNum =",inTree.GetEntries()
            if Divider:
                for item in Divider:
                    newName = inName
                    newName = newName.split("_")
                    newName = newName[:-1]+[item]+[newName[-1]]
                    newName = "_".join(newName)
                    myCut   = this.SkimCut+Divider[item]
                    print " ->",newName, " : ",myCut,
                    outTree = inTree.CopyTree(this.CutString(myCut))
                    outTree.Write(newName)
                    print " EvNum =",outTree.GetEntries()
            print " ->",inName," : ",this.SkimCut,
            outTree = inTree.CopyTree(this.CutString(this.SkimCut))
            outTree.Write(inName)
            print " EvNum =",outTree.GetEntries()
            print
        inFile.Close()
        outFile.Close()
        return

    def ProcessAll(this):
        for fil in this.fileList:
            for sam in this.sampleList:
                inFileName  = this.inputFolder .rstrip("/")+"/"+fil
                outFileName = this.outputFolder.rstrip("/")+"/"+fil
                inTreeName  = sam
                Divider     = this.dividerList[sam]
                this.ProcessOne(inFileName,inTreeName,outFileName,Divider=Divider)

if __name__=="__main__":
    tp = treeProcessor()
    
    # AK: ,"isQEDFSR==0" is for "Can you remove Sherpa W+jet/Z+jet events with isQEDFSR=1 at skim level?
    #                            W+jet+gamma(FSR) is covered by SherpaDiboson and we have to remove it."
    #    tp.SetSkim(["mt>40.","met>100.","jet1Pt>80.", "jet2Pt>50.","jet3Pt>30.", "meffInc30>500.","isQEDFSR==0","lep1Pt>25.","lep2Pt<10."])
    tp.SetSkim(["mt>40.","met>100.","jet1Pt_JVF25pt50>80.", "jet2Pt_JVF25pt50>50.","jet3Pt_JVF25pt50>30.", "meffInc30_JVF25pt50>500.","isQEDFSR==0","lep1Pt>25.","lep2Pt<10."])

    # define samples
    tp.SetSample("SherpaWMassiveBC")
    tp.SetSample("PowhegPythiaTTbar")
    tp.SetSample("ttbarV")
    tp.SetSample("SherpaDibosons")
    tp.SetSample("SingleTop")
    tp.SetSample("SherpaZMassiveBC")

    tp.SetSample("PowhegJimmyTTbar")


    # define branches to be removed
    #    tp.SetRemoveBranches(["jet1Pt","jet3Pt","jet5Pt","jet6Pt","jet7Pt","jet8Pt"])

    # define branches to be kept
    tp.SetKeepBranches(["mt","lep2Pt","lep1Pt","jet1Pt_JVF25pt50","jet2Pt_JVF25pt50","jet3Pt_JVF25pt50","jet4Pt_JVF25pt50","jet5Pt_JVF25pt50","jet6Pt_JVF25pt50","jet1Pt","jet2Pt","jet3Pt","jet4Pt","jet5Pt","jet6Pt","met","meffInc30_JVF25pt50","meffInc30","isQEDFSR","nJet30_JVF25pt50","nBJet30_MV1_60p_JVF25pt50","nBJet30_MV1_70p_JVF25pt50","nBJet30_MV1_80p_JVF25pt50","nJet30","nB3Jet30","AnalysisType","isNoCrackElectron","lep1Eta","EF_e24vh_medium1_EFxe35_tclcw","EF_e60_medium1","EF_mu24_j65_a4tchad_EFxe40_tclcw","qcdWeight","qcdBWeight","genWeight","eventWeight","leptonWeight","triggerWeight","pileupWeight","bTagWeight", "genWeightUp","genWeightDown","pileupWeightUp","pileupWeightDown","leptonWeightUp","leptonWeightDown","bTagWeightBUp","bTagWeightBDown","bTagWeightCUp","bTagWeightCDown","bTagWeightMUp","bTagWeightMDown","triggerWeightUp","triggerWeightDown","qfacUpWeightW","qfacDownWeightW","ktfacUpWeightW","ktfacDownWeightW","iqopt2WeightW","iqopt3WeightW","pdfWeightVars","pdfWeight","nJet40_JVF25pt50","jetEta_JVF25pt50","nJet40","jetEta","lep1Phi","metPhi","Ht30_JVF25pt50","nJet25_JVF25pt50","nBJet30_JVF25pt50","Ht30","nJet25","nBJet30","Wpt","jet7Pt_JVF25pt50","meff3Jet30_JVF25pt50","jet7Pt","meff3Jet30","nB3Jet30_MV1_60p_JVF25pt50","VecSumTTbarPt","DatasetNumber"])
    
    tp.ProcessAll()
