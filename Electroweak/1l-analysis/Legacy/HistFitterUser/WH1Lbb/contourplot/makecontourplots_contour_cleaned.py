#!/usr/bin/env python

import os,sys,ROOT

print 'Running makecontourplots'

doUpperLimit = False
if "-l" in sys.argv:
    doUpperLimit = True
    sys.argv.remove("-l")

if len(sys.argv)>1:
    #if sys.argv[1]!='-b': infiles = sys.argv[1:]
    outputname = sys.argv[1]
    if len(sys.argv)>2: infiles = sys.argv[2:]
else:
    print 'Please specify an input file(s)'
    sys.exit(1)
print 'Using files',infiles

if len(infiles)>1:
    file_up = infiles[1]
else:
    file_up = None
   
if len(infiles)>2:
    file_down = infiles[2]
else:
    file_down = None   

print 'Calling over to macro'


execfile( 'summaryplots/limit_plot_SM_special.py' )

#################################################
## OBS bands depend on w/ or w.o up/down files ##
#################################################
#######################################################################################################################
## for exclusion contours : the last argument by default == -1.  
limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '1 e/#mu + 2 b-jets + E_{T}^{miss}', 36.1, True, True, True) ### OBS and EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '1 e/#mu + jets + E_{T}^{miss}', 6.3, True, True, False ) ### OBS and EXP w.o b
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '1 e/#mu + jets + E_{T}^{miss}', 6.3, False, False, True ) ###  EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '1 e/#mu + jets + E_{T}^{miss}', 6.3, False, False, False ) ### EXP w.o band

#######################################################################################################################
## for discovery significance: make sure you have makelist for hypo_discovery and dodis = 3 or 5 here
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, True, True, True, False, False, True,3) 
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'hard 1-lepton + jets + E_{T}^{miss}', 1.7, True, True, True, False, False, True,5) 

#######################################################################################################################
# Arguments:
# drawFirstObs=False, allObs=False, firstOneSigma=False, upperlimit=False, printCLs=False,write_curves=True, dodis=-1
# dodis = -1 (default) or 3 or 5
#######################################################################################################################



