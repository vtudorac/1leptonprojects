#!/usr/bin/env python

import ROOT

import contourPlotter

ROOT.gROOT.LoadMacro("~/atlasstyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.LoadMacro("~/atlasstyle/AtlasLabels.C")


drawTheorySysts = False

plot = contourPlotter.contourPlotter("contourPlotterWhSS",800,600)

plot.processLabel = "#tilde{#chi}^{0}_{2}#tilde{#chi}^{#pm}_{1} #rightarrow Wh #tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}"
plot.lumiLabel = "#sqrt{s}=13 TeV, 36.1 fb^{-1}, All limits at 95% CL"

## Just open up a root file with TGraphs in it so you can hand them to the functions below!

f = ROOT.TFile("outputGraphs.root")

f.ls()

## Axes

plot.drawAxes( [150,0,350,180] )

## Other limits to draw

# plot.drawShadedRegion( externalGraphs.curve, title="ATLAS 8 TeV, 20.3 fb^{-1} (observed)" )

## Main Result

plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")


#drawing of yellow band
yellow_band = f.Get("SubGraphs/clsd1s_Contour_0_Down")
yellow_band.SetPoint(yellow_band.GetN()+1,0,0)
#yellow_band.SetPoint(i,0,0)


plot.drawOneSigmaBand(yellow_band)
#plot.drawOneSigmaBand(  f.Get("SubGraphs/clsd1s_Contour_0_Down")   )
plot.drawExpected(      f.Get("Exp_0")       )
plot.drawObserved(      f.Get("Obs_0"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")

## Draw Lines

plot.drawLine(  coordinates = [150,25,300,175], label = "m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}) < m(#tilde{#chi}^{0}_{1}) + 125 GeV", style = 7, angle = 40 )

## Axis Labels

plot.setXAxisLabel( "m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}) [GeV]" )
plot.setYAxisLabel( "m(#tilde{#chi}^{0}_{1}) [GeV]"  )

plot.createLegend(shape=(0.22,0.58,0.55,0.77) ).Draw()

if drawTheorySysts:
    plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
    plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
    # coordinate in NDC
    plot.drawTheoryLegendLines( xyCoord=(0.234,0.6625), length=0.057 )


ROOT.ATLASLabel(0.25,0.85,"Internal")	

plot.decorateCanvas( )
plot.writePlot( )




