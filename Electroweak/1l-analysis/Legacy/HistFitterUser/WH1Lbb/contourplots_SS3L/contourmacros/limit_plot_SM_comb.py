#!/bin/env python

import ROOT
ROOT.gROOT.ProcessLine('#include "contourmacros/CombinationGlob.C"')

ROOT.CombinationGlob.Initialize()

import pickle

execfile("summary_harvest_tree_description.py")

def limit_plot_SM_comb( file_nominal, file_up=None, file_down=None , outputfilename='', prefix=None, lumi=21, drawFirstObs=False, allObs=False, firstOneSigma=False, upperlimit=False, printCLs=False, write_curves=False, dodis=-1,plotBESTSR=True): 
    
    #if not 'HiggsSU' in file_nominal:
    #    execfile("summary_harvest_tree_description_SM.py")
    #else: 
    #    execfile("summary_harvest_tree_description_HiggsSU.py")

    print_theo = True
    
    if file_up==None or file_down==None:
        print_theo = False
        print "No up or down variations given! Proceed without them."

    print 'Producing contour plot based on ', file_nominal

    if 'GGM' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_GGM_version271117_SR0Ccurves.root")
    elif 'GG_' in file_nominal and '12k' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_GG_version271117_SR0B_LLE12kcurves.root")
    elif 'GG_' in file_nominal and 'i33' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_GG_version271117_SR0B+_LLEi33curves.root")    
    elif 'VVz_' in file_nominal and '12k' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_VVz_version271117_SR0B_LLE12kcurves.root")
    elif 'VVz_' in file_nominal and 'i33' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_VVz_version271117_SR0B+_LLEi33curves.root")  
    elif 'VVh_' in file_nominal and '12k' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_VVh_version271117_SR0B_LLE12kcurves.root")
    elif 'VVh_' in file_nominal and 'i33' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_VVh_version271117_SR0B+_LLEi33curves.root")      
    elif 'LV_' in file_nominal and '12k' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_LV_version271117_SR0B_LLE12kcurves.root")
    elif 'LV_' in file_nominal and 'i33' in file_nominal: file_tightlimit = ROOT.TFile.Open("limit_LV_version271117_SR0B+_LLEi33curves.root")      
        
    try: 
        graph = file_tightlimit.Get("firstObsH_graph0")
        graph.SetFillColor(0)
    except: 
        try: 
            graph = file_tightlimit.Get("firstObsH_graph1")
            graph.SetFillColor(0)
        except: 
            graph = file_tightlimit.Get("firstObsH_graph2")  
            graph.SetFillColor(0)
        
    #graph.SetFillColor(0)
    graph.SetLineWidth(2)
    graph.SetLineColor(ROOT.kGreen)
    
    try: 
        graph_exp = file_tightlimit.Get("firstExpH_graph0")
        graph_exp.SetFillColor(0)
    except:
        try: 
            graph_exp = file_tightlimit.Get("firstExpH_graph1")
            graph_exp.SetFillColor(0)
        except: 
            graph_exp = file_tightlimit.Get("firstExpH_graph2")
            graph_exp.SetFillColor(0)
        
    #graph_exp.SetFillColor(0)
    graph_exp.SetLineWidth(2)
    graph_exp.SetLineStyle(2)    
    graph_exp.SetLineColor(ROOT.kGreen)    
    
    if 'GGM' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_GGM_version271117_SR0Dcurves.root")
    elif 'GG_' in file_nominal and '12k' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_GG_version271117_SR0A_LLE12kcurves.root")  
    elif 'GG_' in file_nominal and 'i33' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_GG_version271117_SR0A+_LLEi33curves.root")    
    elif 'VVz_' in file_nominal and '12k' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_VVz_version271117_SR0A_LLE12kcurves.root")  
    elif 'VVz_' in file_nominal and 'i33' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_VVz_version271117_SR0A+_LLEi33curves.root")  
    elif 'VVh_' in file_nominal and '12k' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_VVh_version271117_SR0A_LLE12kcurves.root")  
    elif 'VVh_' in file_nominal and 'i33' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_VVh_version271117_SR0A+_LLEi33curves.root")      
    elif 'LV_' in file_nominal and '12k' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_LV_version271117_SR0A_LLE12kcurves.root")  
    elif 'LV_' in file_nominal and 'i33' in file_nominal: file_looselimit = ROOT.TFile.Open("limit_LV_version271117_SR0A+_LLEi33curves.root")     
        
    try: 
        graph_loose = file_looselimit.Get("firstObsH_graph0")
        graph_loose.SetFillColor(0)
    except: 
        try: 
            graph_loose = file_looselimit.Get("firstObsH_graph1")
            graph_loose.SetFillColor(0)
        except: 
            graph_loose = file_looselimit.Get("firstObsH_graph2")
            graph_loose.SetFillColor(0)
    
        
    #graph_loose.SetFillColor(0)
    graph_loose.SetLineWidth(2)
    graph_loose.SetLineColor(ROOT.kMagenta)
    
    try: 
        graph_loose_exp = file_looselimit.Get("firstExpH_graph0")
        graph_loose_exp.SetFillColor(0)
    except: 
        try: 
            graph_loose_exp = file_looselimit.Get("firstExpH_graph1")
            graph_loose_exp.SetFillColor(0)
        except: 
            graph_loose_exp = file_looselimit.Get("firstExpH_graph2")
            graph_loose_exp.SetFillColor(0)
        
    #graph_loose_exp.SetFillColor(0)
    graph_loose_exp.SetLineWidth(2)
    graph_loose_exp.SetLineStyle(2)    
    graph_loose_exp.SetLineColor(ROOT.kMagenta)     

    graph_run1 = run1_limit().Clone()
    graph_run1.SetFillColor(17)
    graph_run1.SetLineWidth(0)
    graph_run1.SetLineStyle(0)    
    graph_run1.SetLineColor(18)   

    firstObs = None
    firstExp = None
    firstPOneSigma = None
    firstMOneSigma = None
    histosOrig = []

    f = ROOT.TFile.Open(file_nominal,'READ')
    if dodis>0:
        drawFirstObs=False
        allObs=False
        firstExp = f.Get( 'sigp0exp' ) ## you need to have expected discovery significance
        for hizzie in [firstExp]: hizzie.SetDirectory(0)
    else:
        firstObs = f.Get( 'sigp1clsf' )
        firstExp = f.Get( 'sigp1expclsf' )
        firstPOneSigma = f.Get( 'sigclsu1s' )
        firstMOneSigma = f.Get( 'sigclsd1s' )
        print firstPOneSigma
        for hizzie in [firstObs,firstExp,firstPOneSigma,firstMOneSigma]: hizzie.SetDirectory(0)

    f.Close()    
    
    if print_theo:
        f_up = ROOT.TFile.Open(file_up,'READ')
        upObs = f_up.Get( 'sigp1clsf' ) 
        upObs.SetDirectory(0)
        f_up.Close()
        f_down = ROOT.TFile.Open(file_down,'READ')
        downObs = f_down.Get( 'sigp1clsf' ) 
        downObs.SetDirectory(0)
        f_down.Close() 
        histosOrig = [upObs, downObs]      


    firstExpH = FixAndSetBorders(file_nominal, firstExp , 'firstExpH' , '' , 0 )
    if dodis<0:
        firstObsH = FixAndSetBorders(file_nominal, firstObs , 'firstObsH' , '' , 0 )

    if print_theo:
        histos = []
        for h in histosOrig: histos += [ FixAndSetBorders(file_nominal, h , h.GetName()+'H' , '' , 0 ) ]

    firstPOneSigmaG=ROOT.TGraph()
    firstMOneSigmaG=ROOT.TGraph()
    
    if firstOneSigma and dodis<0: # Only need these for the +/-1 sigma band
        firstPOneSigmaG_pre = ContourGraph(file_nominal, FixAndSetBorders(file_nominal, firstPOneSigma , 'firstPOneSigmaG' , '' , 0 ) )
        if firstPOneSigmaG_pre!=None: firstPOneSigmaG = firstPOneSigmaG_pre.Clone()
        firstMOneSigmaG_pre = ContourGraph(file_nominal, FixAndSetBorders(file_nominal, firstMOneSigma , 'firstMOneSigmaG' , '' , 0 ) )
        if firstMOneSigmaG_pre!=None: firstMOneSigmaG = firstMOneSigmaG_pre.Clone()
        if firstMOneSigmaG.GetN()==0 or firstPOneSigmaG.GetN()==0: firstOneSigma=False
    
    
    # set text style
    ROOT.gStyle.SetPaintTextFormat(".2g")

    # Start drawing
    c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    #c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 ,600, 600)
    
    pwd=ROOT.gDirectory

    # create and draw the frame2 
    if 'GG_' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 110, 1000., 2600., 100, 10., 2600. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )        
    elif 'C1C1' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 400., 1400., 100, 10., 1300. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )
    elif 'C1N2z' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 600., 1600., 100, 10., 1700. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'VV' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 600., 1600., 100, 10., 1700. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'LV' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 450., 1200., 100, 10., 1200. )
        frame2.SetXTitle( "m_{#tilde{l}_{L}/#tilde{#nu}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )            
    elif 'GGM' in file_nominal:
        if upperlimit: frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 130., 800., 100, 0., 100. )
        else: frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 130., 500., 100, 0., 100. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} [GeV]" )
        frame2.SetYTitle( "BR(#tilde{#chi}^{0}_{1} #rightarrow Z#tilde{G}) [%]" )
    else:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1500., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )

    ROOT.CombinationGlob.SetFrameStyle2D( frame2, 1.0 )

    frame2.GetYaxis().SetTitleOffset(1.35)
 
    frame2.GetXaxis().SetTitleFont( 42 )
    frame2.GetYaxis().SetTitleFont( 42 )
    frame2.GetXaxis().SetLabelFont( 42 )
    frame2.GetYaxis().SetLabelFont( 42 )
 
    frame2.GetXaxis().SetTitleSize( 0.04 )
    frame2.GetYaxis().SetTitleSize( 0.04 )
    frame2.GetXaxis().SetLabelSize( 0.04 )
    frame2.GetYaxis().SetLabelSize( 0.04 )
 
    frame2.Draw()
    
    
    # Set up the legend (Legend for GGM in bottom right corner)
    if 'GGM' in file_nominal:
      #leg = ROOT.TLegend(0.15,0.64,0.37,0.76)
      leg = ROOT.TLegend(0.63,0.23,0.85,0.32)
      leg.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize );
      leg.SetTextSize( 0.03 );
      leg.SetTextFont( 42 );
      leg.SetFillColor( 0 );
      leg.SetFillStyle(1001);
    else:
        leg = ROOT.TLegend(0.15,0.54,0.37,0.76)
        #leg = ROOT.TLegend(0.14,0.6,0.37,0.74)
        leg.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize );
        leg.SetTextSize( 0.03 );
        leg.SetTextFont( 42 );
        leg.SetFillColor( 0 );
        #leg.SetFillStyle(1001);
    
    #if graph_run1:
    #    graph_run1.Draw("LFsame")

    # Draw the +/- 1 sigma yellow band 
    if firstOneSigma: 
        grshadeExp = DrawExpectedBand( firstPOneSigmaG , firstMOneSigmaG , ROOT.CombinationGlob.c_DarkYellow , 1001 , 0).Clone()
        grshadeExp.Draw("Fsame")

    colors = [ ROOT.CombinationGlob.c_DarkGreen, ROOT.CombinationGlob.c_DarkRed ,
               ROOT.CombinationGlob.c_DarkOrange , ROOT.CombinationGlob.c_DarkGray , ROOT.CombinationGlob.c_BlueT3, ROOT.CombinationGlob.c_DarkPink , ROOT.CombinationGlob.c_VDarkYellow, ROOT.CombinationGlob.c_HiggsGreen,
               ROOT.CombinationGlob.c_LightPink , ROOT.CombinationGlob.c_LightYellow, ROOT.CombinationGlob.c_Black ]
               
    c_myYellow   = ROOT.TColor.GetColor("#ffe938")
    c_myRed      = ROOT.TColor.GetColor("#aa000")
    c_myExp      = ROOT.TColor.GetColor("#28373c")       

    #colors = [ ROOT.CombinationGlob.c_DarkGreen , ROOT.CombinationGlob.c_DarkGray , ROOT.CombinationGlob.c_BlueT3 , ROOT.CombinationGlob.c_DarkRed ,
    #           ROOT.CombinationGlob.c_DarkOrange , ROOT.CombinationGlob.c_DarkPink , ROOT.CombinationGlob.c_VDarkYellow ]
    #if len(colors)<len(histos):
    #    print 'Only have',len(colors),'colors for',len(histos),'histograms.  Will crash...'
    
    if graph and not 'GGM' in file_nominal:
        graph.Draw("Lsame")
        graph_exp.Draw("Lsame")
        graph_loose.Draw("Lsame")
        graph_loose_exp.Draw("Lsame")   

    #c2 = ROOT.TCanvas("dummy","dummy", 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    c2 = ROOT.TCanvas("dummy","dummy", 0 , 0 , 700, 1000)

    newHists = []
    
    if print_theo:   
        print len(histos)
        print histos
        anewhist={}
        anewhist_pre={}
        for i in xrange(len(histos)):
            print i
            try:
                anewhist[i]=[]
                #print histos[i]
                (leg,anewhist_pre[i]) = DrawContourLine95Graph(file_nominal, leg, histos[i],histos[i].GetName()+str(i), '' , c_myRed , 3, 2, dodis) 
                #print "Pre:", anewhist_pre[i]
                for hist in anewhist_pre[i]:
                    dummy = ROOT.TGraph()
                    dummy = hist.Clone()
                    anewhist[i].append(dummy)
                #print  anewhist[i]   
                #ROOT.gDirectory.Append(anewhist[i])
                #anewhist[i].Draw("Lsame")
                #newHists += [anewhist[i]]
            except:
                print "One or more of the +-1 sigma variations of the observed limit does not exist"
         

    anewhist00=[]#ROOT.TGraph()
    if drawFirstObs:
        (leg,anewhist0) = DrawContourLine95Graph(file_nominal, leg, firstObsH,firstObsH.GetName()+"_graph", '', ROOT.CombinationGlob.c_DarkRed, 1, 4, dodis)
        print anewhist0
        for hist in anewhist0:
            dummy = ROOT.TGraph()
            dummy = hist.Clone()
            anewhist00.append(dummy)
        #ROOT.gDirectory.Append(anewhist00)
        #anewhist00.Draw("Lsame")
        (leg,anewhist000) = DummyLegendExpected( leg, 'Observed limit (#pm1 #sigma^{SUSY}_{theory})', 0, 1001, ROOT.CombinationGlob.c_DarkRed, 1, 4)
        #newHists += [anewhist00]
        #newHists += [anewhist000]
        #(leg,anewhist) = DummyLegendExpected( leg, 'Observed limit (#pm1 #sigma^{SUSY}_{theory})', c_myRed, 1001, ROOT.CombinationGlob.c_DarkRed, 1, 4)
        #newHists += [anewhist]
    
    print anewhist00 
    
    anewhist11=[] #ROOT.TGraph()

    if firstOneSigma:
        (leg,anewhist1) = DrawContourLine95Graph(file_nominal, leg, firstExpH,firstExpH.GetName()+"_graph", '', c_myExp, 6, 2, dodis)
        print anewhist1
        for hist in anewhist1:
            dummy = ROOT.TGraph()
            dummy = hist.Clone()
            anewhist11.append(dummy)            
        #ROOT.gDirectory.Append(anewhist11)
        #anewhist11.Draw("Lsame")
        #newHists += [anewhist11]
        if dodis<0:
            (leg,anewhist2) = DummyLegendExpected( leg, 'Expected limit (#pm1 #sigma_{exp})', ROOT.CombinationGlob.c_DarkYellow, 1001, c_myExp, 6, 2 )
            newHists += [anewhist2]
        else:
            (leg,anewhist2) = DummyLegendExpected( leg, 'Expected discovery significance', 0, 1001, c_myExp, 6, 2, dodis )
            newHists += [anewhist2]
    else:
        #(leg,anewhist) = DrawContourLine95( leg, firstExpH, file_nominal.split('_Higgsino')[0].split('MultiJet_')[1], ROOT.CombinationGlob.c_DarkBlueT3, 6 )
        try:
            (leg,anewhist1) = DrawContourLine95Graph(file_nominal, leg, firstExpH,firstExpH.GetName()+"_graph", 'Expected', c_myExp, 6, 2, dodis)
            for hist in anewhist1:
                dummy = ROOT.TGraph()
                dummy = hist.Clone()
                anewhist11.append(dummy)                  
                #anewhist11.append([anewhist1.Clone()])
            #ROOT.gDirectory.Append(anewhist11)
            c.cd()
            #anewhist11.Draw("Lsame")
            #newHists += [anewhist11]
        except:
            print "Expected limit does not exist"

    pwd.cd()
    #pwd.ls()
    c.cd()
    c.Update()
    
    if print_theo:   
        #print len(histos)
        #print histos
        #print anewhist
        for i2 in xrange(len(anewhist)):
            print i2
            #anewhist[i2].Dump()
            for i3 in anewhist[i2]:
                print i3
                i3.Draw("Lsame") 
    if drawFirstObs:
        #print anewhist00
        #anewhist00.Dump()
        #anewhist00.Print()
        for i4 in anewhist00:
            i4.Draw("Lsame")
    if firstOneSigma:
        #anewhist11.Dump()
        print anewhist11
        for i5 in anewhist11:
            i5.Draw("Lsame")
            

    
    c.Update()
    
    # legend
    textSizeOffset = +0.000;
    xmax = frame2.GetXaxis().GetXmax()
    xmin = frame2.GetXaxis().GetXmin()
    ymax = frame2.GetYaxis().GetXmax()
    ymin = frame2.GetYaxis().GetXmin()
    dx   = xmax - xmin
    dy   = ymax - ymin

    # Label the decay process
    Leg0 = None
    if 'GG_' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}#tilde{g} #rightarrow qqqqllll#nu#nu" )
    elif 'C1N2z' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{0}_{2} #rightarrow WZllll#nu#nu" )
    elif 'VVz' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{0}_{2} #rightarrow WW/Zllll#nu#nu" ) 
    elif 'VVh' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{0}_{2} #rightarrow WW/Hllll#nu#nu" )     
    elif   'LV' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{l}_{L}/#tilde{#nu} #tilde{l}_{L}/#tilde{#nu} #rightarrow l/#nul/#nullll#nu#nu" )    
    elif   'C1C1' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{#mp}_{1} #rightarrow WWllll#nu#nu" ) ##tilde{g}-#tilde{g}, #tilde{g}#rightarrow q#bar{q}'W#tilde{#chi}_{1}^{0}" )
    elif 'GGM' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "Higgsino #tilde{#chi}_{1}^{#pm}#tilde{#chi}_{2}^{0}#tilde{#chi}_{1}^{0}, #tilde{#chi}_{1}^{0} #rightarrow Z/h #tilde{G}" )
    else: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrow qqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x=1/2" )


    if Leg0 is not None:
        Leg0.SetTextAlign( 11 );
        Leg0.SetTextFont( 42 );
        Leg0.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
        Leg0.SetTextColor( 1 );
        Leg0.AppendPad();

    #Leg3 = ROOT.TLatex( (xmin+xmax)*0.6, ymax + dy*0.025, 'Expected Limit Comparison' )
    #Leg3.SetTextAlign( 11 );
    #Leg3.SetTextFont( 42 );
    #Leg3.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
    #Leg3.SetTextColor( 1 );
    #Leg3.AppendPad();


    #frame2.Draw( 'sameaxis' )
    #leg.Draw( 'same' )
    
    #leg.AddEntry(graph_run1,"ATLAS 8 TeV, 20.3 fb^{-1}","F")
    if graph and not 'GGM' in file_nominal:
        leg.AddEntry(graph,"SRB","l")
        leg.AddEntry(graph_loose,"SRA","l")
    leg.Draw( 'same' )
    Leg1 = ROOT.TLatex()
    Leg1.SetNDC()
    Leg1.SetTextAlign( 11 )
    Leg1.SetTextFont( 42 )
    Leg1.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg1.SetTextColor( 1 )
    if 'GGM' in file_nominal:    
        Leg1.DrawLatex(0.64,0.34, '#sqrt{s}=13 TeV, %.1f fb^{-1}'%(lumi) )
    else:
        Leg1.DrawLatex(0.16,0.82, '#sqrt{s}=13 TeV, %.1f fb^{-1}'%(lumi) )
    Leg1.AppendPad()

    Leg2 = ROOT.TLatex()
    Leg2.SetNDC()
    Leg2.SetTextAlign( 11 )
    Leg2.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg2.SetTextColor( 1 )
    Leg2.SetTextFont(42)
    if prefix is not None:
        if 'GGM' in file_nominal:
            Leg2.DrawLatex(0.64,0.4,prefix)
        else:  
            Leg2.DrawLatex(0.16,0.77,prefix)
        Leg2.AppendPad()
        
    c.Update()

    frame2.Draw( 'sameaxis' )
    #leg.Draw( 'same' )

    # Diagonal line
    diagonal = None 
    if ("LL" in file_nominal): diagonal = ROOT.TLine(400., 390., 1300., 1290.)
    if ("GG" in file_nominal): diagonal = ROOT.TLine(1000., 990., 2600., 2590.)
    if ("C1N2z" in file_nominal): diagonal = ROOT.TLine(600., 590., 1600., 1590.)
    if ("VVz" in file_nominal): diagonal = ROOT.TLine(600., 590., 1600., 1590.) 
    if ("VVh" in file_nominal): diagonal = ROOT.TLine(600., 590.-125., 1600., 1590.-125.) 
    if ("LV" in file_nominal): diagonal = ROOT.TLine(450., 440., 1200., 1190.)       
        
    if diagonal: 
        diagonal.SetLineStyle(2)
        diagonal.Draw()
    
    gtt = ROOT.TLatex(450,490,"m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV")
    if "C1N2z" in file_nominal:
        gtt.SetText(1050,1090,"m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")
    if "VVz" in file_nominal:
        gtt.SetText(1050,1120,"m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")
    if "VVh" in file_nominal:
        gtt.SetText(1050,965,"m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV + m_{h} ")  
    if "LV" in file_nominal:
        gtt.SetText(850,890,"m_{#tilde{l}_{L}/#tilde{#nu}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")    
    if "GG" in file_nominal:
        gtt.SetText(1650,1710,"m_{#tilde{g}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")            
    #else: gtt.SetText(550,590,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")

    gtt.SetTextSize(0.03)
    gtt.SetTextColor(12)
    gtt.SetTextAngle(25)
    if "SS" in file_nominal:
        gtt.SetTextAngle(40)
    gtt.SetTextFont(42)

    if not "gridx" in file_nominal:
        gtt.Draw("same")    
                   

    # update the canvas
    c.Update()
    
    clslimits = ROOT.TLatex()
    #clslimits = ROOT.TLatex(360,887-197,"All limits at 95% CL_{S}")
    clslimits.SetNDC()
    clslimits.SetTextSize(0.03)
    clslimits.SetTextFont(42)
    if 'GGM' in file_nominal:
        clslimits.DrawLatex(0.64,0.19,"All limits at 95% CL")  
    else:
        clslimits.DrawLatex(0.46,0.87,"All limits at 95% CL")
    if print_theo:
        obsPOneSigma = ROOT.TLine()
        obsPOneSigma.SetLineStyle(3)
        obsPOneSigma.SetLineWidth(2)
        obsPOneSigma.SetLineColor(c_myRed)
        if 'GGM' in file_nominal:
            obsPOneSigma.DrawLineNDC(0.64,0.308,0.6777,0.308)
        else:
            obsPOneSigma.DrawLineNDC(0.159,0.745,0.198,0.745)

        obsMOneSigma = ROOT.TLine()
        obsMOneSigma.SetLineStyle(3)
        obsMOneSigma.SetLineWidth(2)
        obsMOneSigma.SetLineColor(c_myRed)
        if 'GGM' in file_nominal:
            obsPOneSigma.DrawLineNDC(0.64,0.287,0.6777,0.287)
        else:    
            obsMOneSigma.DrawLineNDC(0.159,0.715,0.198,0.715)
  
    singlelimitline = ROOT.TLine()
    singlelimitline.SetLineStyle(2)
    singlelimitline.SetLineWidth(2)
    singlelimitline.SetLineColor(ROOT.kMagenta)
    if not 'GGM' in file_nominal: singlelimitline.DrawLineNDC(0.159,0.56,0.198,0.56)
    
    singlelimitline2 = ROOT.TLine()
    singlelimitline2.SetLineStyle(2)
    singlelimitline2.SetLineWidth(2)
    singlelimitline2.SetLineColor(ROOT.kGreen)
    if not 'GGM' in file_nominal: singlelimitline2.DrawLineNDC(0.159,0.615,0.198,0.615)    
  
    atlasLabel = ROOT.TLatex()
    atlasLabel.SetNDC()
    atlasLabel.SetTextFont(72)
    atlasLabel.SetTextColor(ROOT.kBlack)
    atlasLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    atlasLabel.DrawLatex(0.6, 0.85,"ATLAS")
    #else:
    if 'GGM' in file_nominal:
       atlasLabel.DrawLatex(0.64,0.45,"ATLAS")
    else:
        atlasLabel.DrawLatex(0.16, 0.87,"ATLAS")
    atlasLabel.AppendPad()

    progressLabel = ROOT.TLatex()
    progressLabel.SetNDC()
    progressLabel.SetTextFont(42)
    progressLabel.SetTextColor(ROOT.kBlack)
    progressLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    progressLabel.DrawLatex(0.76, 0.85,"Internal")
    #else:
    #print 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())
    if 'GGM' in file_nominal:
       progressLabel.DrawLatex(0.64+0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()), 0.45,"Internal")
    else:    
        progressLabel.DrawLatex(0.16+0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()), 0.87,"Internal")
    #else: progressLabel.DrawLatex(0.19, 0.81,"Internal")
    progressLabel.AppendPad()

    c.Update()    
    
    if printCLs:
   
        listTree = harvesttree("outputlist/"+file_nominal.replace(".root",""))                         
        nentries = listTree.GetEntries()
        print nentries
                          
        pointLabel = ROOT.TLatex()
        pointLabel.SetTextSize(0.02)
        pointLabel.SetTextFont(42)
        pointLabel.SetTextColor(ROOT.kGray+2)
       
        from array import array
        m0floatbranch = array('f',[0])
        m12floatbranch = array('f',[0])
        CLsfloatbranch = array('f',[0])
        CLsexpfloatbranch = array('f',[0])
        clsu1sfloatbranch = array('f',[0])
        clsd1sfloatbranch = array('f',[0])                     
        listTree.SetBranchStatus("NLSP",1)
        listTree.SetBranchStatus("LSP",1)
        listTree.SetBranchStatus("CLs",1)
        listTree.SetBranchStatus("CLsexp",1)
        listTree.SetBranchStatus("clsu1s",1)
        listTree.SetBranchStatus("clsd1s",1)                   
        listTree.SetBranchAddress("NLSP",m0floatbranch)
        listTree.SetBranchAddress("LSP",m12floatbranch)
        listTree.SetBranchAddress("CLs",CLsfloatbranch)
        listTree.SetBranchAddress("CLsexp",CLsexpfloatbranch)
        listTree.SetBranchAddress("clsu1s",clsu1sfloatbranch)
        listTree.SetBranchAddress("clsd1s",clsd1sfloatbranch)                  

        for entry in range(nentries):
            status = listTree.GetEntry(entry)
            mstop = m0floatbranch[0]
            mneutralino= m12floatbranch[0]
            CLs = CLsfloatbranch[0]
            diff = float(clsu1sfloatbranch[0]) - float(clsd1sfloatbranch[0])
            print mstop, mneutralino, CLs
            if (mstop < frame2.GetXaxis().GetXmin() or mstop>frame2.GetXaxis().GetXmax() or mneutralino>frame2.GetYaxis().GetXmax()-5. or mneutralino<frame2.GetYaxis().GetXmin()):
                continue 
            if CLs<0.05:
                pointLabel.SetTextColor(ROOT.kRed)
                pointLabel.DrawLatex(mstop,mneutralino,"#bf{%1.2f}" % (CLs))
            else:
                pointLabel.SetTextColor(14)
                pointLabel.DrawLatex(mstop,mneutralino,"%1.2f" % (CLs))
                                
    if plotBESTSR:
        print 'dol: plotBESTSR'
        pickle_file0=file_nominal.replace(".root",".pkl")
        if 'GGM' in file_nominal: pickle_file0="exclusion_GGM_comb_nominal__1_harvest_list.pkl"
        print pickle_file0
        pointLabel = ROOT.TLatex()
        pointLabel.SetTextSize(0.02)
        pointLabel.SetTextFont(42)
        try:
            pickle_file=open(pickle_file0,'rb')
            analysis = pickle.load(pickle_file)
            for key in analysis['nominal'].keys():
                my_tuble = analysis['nominal'][key]
                if my_tuble=='unknown': continue
                point=my_tuble[0]
                NLSP=float(point[0])
                LSP=float(point[1])
                analysisname=my_tuble[2]
                if (NLSP < frame2.GetXaxis().GetXmin() or NLSP>frame2.GetXaxis().GetXmax()-50. or LSP>frame2.GetYaxis().GetXmax()+1. or LSP<frame2.GetYaxis().GetXmin()):
                    continue
                if analysisname=="A" and 'GGM' in file_nominal: analysisname = "SRC"
                if analysisname=="B" and 'GGM' in file_nominal: analysisname = "SRD"
                print NLSP,LSP,":",analysisname
                #pointLabel.DrawLatex(m0,m12,"%1.0f" % (m0))
                pointLabel.SetTextColor(13)
                pointLabel.DrawLatex(NLSP,LSP,analysisname)           
        except:
            print "There does not seem to be a pickle file - cannot print analysis name"    
        
    if upperlimit:
        listTree = harvesttree(file_nominal.replace("Nominal_hypotest","upperlimit").replace(".root",""))                          
        nentries = listTree.GetEntries()
                           
        pointLabel = ROOT.TLatex()
        pointLabel.SetTextSize(0.02)
        pointLabel.SetTextFont(42)
        
        from array import array
        m0floatbranch = array('f',[0])
        mchfloatbranch = array('f',[0])        
        m12floatbranch = array('f',[0])
        upperLimitfloatbranch = array('f',[0])
        expectedUpperLimitfloatbranch = array('f',[0])                   
        listTree.SetBranchStatus("NLSP",1)  
        listTree.SetBranchStatus("LSP",1)
        listTree.SetBranchStatus("upperLimit",1)
        listTree.SetBranchStatus("expectedUpperLimit",1)                 
        listTree.SetBranchAddress("NLSP",m0floatbranch)    
        listTree.SetBranchAddress("LSP",m12floatbranch)
        listTree.SetBranchAddress("upperLimit",upperLimitfloatbranch)
        listTree.SetBranchAddress("expectedUpperLimit",expectedUpperLimitfloatbranch)
    
        fout_upperlimit = open(outputfilename+"upperlimits.txt","w")

        for entry in range(nentries):
            listTree.GetEntry(entry)
            mgluino = listTree.NLSP
            mLSP = listTree.LSP
            
            myvalue_onxaxis = mLSP

            if (mgluino < frame2.GetXaxis().GetXmin() or mgluino>frame2.GetXaxis().GetXmax()-10. or mLSP>frame2.GetYaxis().GetXmax()-5. or mLSP<frame2.GetYaxis().GetXmin()):
                if not 'GGM' in file_nominal:
                    continue
            
            #extract cross section:
            xsec = -1.
            if 'GG_' in file_nominal and 'i33' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_Gluino_i33.txt")
            elif 'GG_' in file_nominal and '12k' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_Gluino_12k.txt")
            elif 'VV' in file_nominal and '12k' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_Wino_12k.txt")     
            elif 'VV' in file_nominal and 'i33' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_Wino_i33.txt")     
            elif 'LV_' in file_nominal and '12k' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_LV_LH_12k.txt")  
            elif 'LV_' in file_nominal and 'i33' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_LV_LH_i33.txt")    
            elif 'RH_' in file_nominal and '12k' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_LV_RH_12k.txt")  
            elif 'RH_' in file_nominal and 'i33' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_LV_RH_i33.txt")    
            elif 'GGM_' in file_nominal: xseclist = harvestXsec("xsec_vs_mass_GGM.txt")                
            xsecnentries = xseclist.GetEntries()
                #print 'n_xsec=',xsecnentries
            for xsecentry in range(xsecnentries):
                xsecstatus = xseclist.GetEntry(xsecentry)
                xsec_NLSP    =float(xseclist.NLSP)
                xsec_LSP   =float(xseclist.LSP)
                xsec        =float(xseclist.crossSect)
                if xsec_NLSP==mgluino and xsec_LSP==mLSP:
            
                    print xsec, listTree.upperLimit, xsec*listTree.upperLimit, mgluino, mLSP, xsec_LSP
            
                    exXsec = listTree.upperLimit*1000.*xsec
                    pointLabel.SetTextColor(12)
                    if exXsec >= 100.:
                        pointLabel.DrawLatex(mgluino,myvalue_onxaxis,"%1.0f" % (exXsec))
                        fout_upperlimit.write(str(mgluino)+"  "+str(myvalue_onxaxis)+"  {0:1.0f} \n".format(exXsec))        
                    else:
                        pointLabel.DrawLatex(mgluino,myvalue_onxaxis,"%1.1f" % (exXsec))
                        fout_upperlimit.write(str(mgluino)+"  "+str(myvalue_onxaxis)+"  {0:1.1f} \n".format(exXsec))

        axistitle =ROOT.TLatex()
        axistitle.SetTextSize(0.03)
        axistitle.SetTextAngle(90)
        axistitle.SetTextFont(42)
        axistitle.SetNDC()
        axistitle.DrawLatex(0.98,0.18,"Numbers give 95% CL excluded model cross sections [fb]")
        fout_upperlimit.close()

    # create plots
    # store histograms to output file
    if dodis==5:
        figtitle='dis5_'
    elif dodis==3:
        figtitle='dis3_'
    else:
        figtitle='limit_'

    if outputfilename=='':
        outFileNom = 'plots/'+figtitle+file_nominal.replace('.root','')
    else:
        outFileNom = 'plots/'+figtitle+outputfilename
    if allObs: outFileNom += '_OBS'
    else:      outFileNom += '_EXP'
    ROOT.CombinationGlob.imgconv( c, outFileNom );
    
    if write_curves:
        #print anewhist00
        files_curves = ROOT.TFile.Open(figtitle+outputfilename+"curves.root","RECREATE")
        for i6 in anewhist00:
            i6.Write()
        for i7 in anewhist11:
            i7.Write()
        if firstMOneSigmaG != None and firstPOneSigmaG != None:
            firstMOneSigmaG.Write()
            firstPOneSigmaG.Write()
        files_curves.Close()

    del leg
    del frame2


def MirrorBorders( hist ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    # corner points
    hist.SetBinContent(0,0,hist.GetBinContent(1,1))
    hist.SetBinContent(numx+1,numy+1,hist.GetBinContent(numx,numy))
    hist.SetBinContent(numx+1,0,hist.GetBinContent(numx,1))
    hist.SetBinContent(0,numy+1,hist.GetBinContent(1,numy))

    # Fix the other points 
    for i in xrange(1,numx+1):
        hist.SetBinContent(i,0,       hist.GetBinContent(i,1));
        hist.SetBinContent(i,numy+1, hist.GetBinContent(i,numy));
    for i in xrange(1,numy+1):
        hist.SetBinContent(0,i,      hist.GetBinContent(1,i));
        hist.SetBinContent(numx+1,i, hist.GetBinContent(numx,i));


def AddBorders( hist, name='StupidName', title='StupidTitle'):
    nbinsx = hist.GetNbinsX()
    nbinsy = hist.GetNbinsY()
  
    xbinwidth = ( hist.GetXaxis().GetBinCenter(nbinsx) - hist.GetXaxis().GetBinCenter(1) ) / float(nbinsx-1)
    ybinwidth = ( hist.GetYaxis().GetBinCenter(nbinsy) - hist.GetYaxis().GetBinCenter(1) ) / float(nbinsy-1)
  
    xmin = hist.GetXaxis().GetBinCenter(0) - xbinwidth/2. 
    xmax = hist.GetXaxis().GetBinCenter(nbinsx+1) + xbinwidth/2. 
    ymin = hist.GetYaxis().GetBinCenter(0) - ybinwidth/2. 
    ymax = hist.GetYaxis().GetBinCenter(nbinsy+1) + ybinwidth/2. 
  
    hist2 = ROOT.TH2F(name, title, nbinsx+2, xmin, xmax, nbinsy+2, ymin, ymax);
  
    for ibin1 in xrange(hist.GetNbinsX()+2):
        for ibin2 in xrange(hist.GetNbinsY()+2):
            hist2.SetBinContent( ibin1+1, ibin2+1, hist.GetBinContent(ibin1,ibin2) );
  
    return hist2


def SetBorders( hist, val=0 ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    for i in xrange(numx+2):
        hist.SetBinContent(i,0,val);
        hist.SetBinContent(i,numy+1,val);
    for i in xrange(numy+2):
        hist.SetBinContent(0,i,val);
        hist.SetBinContent(numx+1,i,val);


def FixAndSetBorders(file_nominal, hist, name='hist3', title='hist3', val=0 ):
    hist0 = hist.Clone() # histogram we can modify
    
    if "GGM" in file_nominal:
        #if not "gridx" in file_nominal and not "CNsl" in file_nominal:
        MirrorBorders( hist0 ) # mirror values of border bins into overflow bins
        #if 'GG1stepx12' in file_nominal:
        #    MirrorBorders( hist0 )
        #    MirrorBorders( hist0 )
        #    MirrorBorders( hist0 )
        #    print "Here"
    
    hist1 = AddBorders( hist0, "hist1", "hist1" );   
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    SetBorders( hist1, val );                              
    # set overflow bins to value 1
    
    histX = AddBorders( hist1, "histX", "histX" )
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    
    hist3 = histX.Clone()
    hist3.SetName( name )
    hist3.SetTitle( title )
    
    del hist0, hist1, histX
    return hist3 # this can be used for filled contour histograms


def DrawContourLine95( leg, hist, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2 ):
    # contour plot
    h = hist.Clone()
    h.SetContour(1)
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
  
    #print linewidth, linestyle
    h.SetLineColor( linecolor )
    h.SetLineWidth( linewidth )
    h.SetLineStyle( linestyle )
    #h.Print()
    h.Draw( "samecont3" );
    
    if text is not '': leg.AddEntry(h,text,'l')
    return leg,h
    
def DrawContourLine95Graph(file_nominal, leg, hist,name, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2, dodis=-1 ):
    # contour plot
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    gr = gr0.Clone(h.GetName())
    h.SetContour(1)
    
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    if dodis>0:
        h.SetContourLevel( 0, dodis )
    else:
        h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    #print hist
    list0 = contours[0]
    #list0.Print()
    #print "Number of lists: ",
    #print list0.GetEntries()
    if list0.GetEntries()==0:
        return None
    
    #list0.Print()
    
    all_graphs = []
    
    for k in xrange(list0.GetSize()):
        gr = list0[k]
        #gr.Print()
        #grTmp = ROOT.TGraph()

        #for k in xrange(list0.GetSize()):
        #    if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
        gr.SetName(name+str(k))
        #print gr
        #print "\n"
        #gr.Print()
        #print linewidth, linestyle
        gr.SetLineColor( linecolor )
        gr.SetLineWidth( linewidth )
        gr.SetLineStyle( linestyle )
        #h.Print()
        #gr.Draw( "same" );
       
        #if 'GGM' in file_nominal:
        #    for j in range(0,gr.GetN()):
        #        if gr.GetY()[j] > 90. and gr.GetX()[j] > 200.: 
        #            gr.SetPoint(j,gr.GetX()[j],101.)

        #print gr.GetN()
        points_to_be_removed=[]
        #print points_to_be_removed
        if not 'GGM' in file_nominal:                    
            my_min = min(gr.GetX())
            diagonal_cut=35.
            diagonal_add=50.
            parallel_cut=1.
            offset=0.
            if 'VVh' in file_nominal: offset=125.
            if 'C1C1' in file_nominal:
                diagonal_cut=35.
                diagonal_add=50.
                parallel_cut=1.
            if 'GG' in file_nominal:
                diagonal_cut=22.
                diagonal_add=100.
                parallel_cut=2.
            if 'C1N2z' in file_nominal:
                diagonal_cut=60.
                diagonal_add=100.
                parallel_cut=8.  
            if 'VV' in file_nominal:
                diagonal_cut=21.
                diagonal_add=90.
                parallel_cut=8.                  
            if 'LV' in file_nominal:
                diagonal_cut=22.
                diagonal_add=50.
                parallel_cut=8.                
            for j in range(0,gr.GetN()):    
                if abs(gr.GetX()[j] - gr.GetY()[j]) <diagonal_cut+offset: # remove all points along the diagonal
                    points_to_be_removed.append(j)   
                if abs(gr.GetX()[j] - min(gr.GetX())) < parallel_cut:   # remove the vertical line...
                    points_to_be_removed.append(j)  
                
            for j in range(0,gr.GetN()):   
                if (gr.GetX()[j]-gr.GetY()[j]) < diagonal_add+offset : # Add a point up to the diagonal using the dM=10 GeV point
                    gr.SetPoint(j, gr.GetX()[j], gr.GetX()[j]-(10.+offset))

            grtemp= ROOT.TGraph()
            j7=0
            xtemp=gr.GetX()
            ytemp=gr.GetY()        
            for j6 in range(1,gr.GetN()):
                if not j6 in points_to_be_removed:
                    grtemp.SetPoint(j7,xtemp[j6],ytemp[j6])
                    j7+=1
                
            #grtemp.Print()
    
            #print points_to_be_removed    
        
            cut_points = 0
            x=grtemp.GetX()
            y=grtemp.GetY()
            for j2 in range(1,grtemp.GetN()):
                #print x[j2],x[j2-1]
                if x[j2]>x[j2-1] and x[j2-1]<my_min+50.:
                    cut_points=j2
                    break
            #print cut_points
    
            gr2 = ROOT.TGraph()
            j4=0
            for j3 in range(cut_points,grtemp.GetN()):
                gr2.SetPoint(j4,x[j3],y[j3])
                j4+=1
            for j5 in range(0,cut_points):
                gr2.SetPoint(j4,x[j5],y[j5])
                j4+=1
            gr2.SetLineColor( linecolor )
            gr2.SetLineWidth( linewidth )
            gr2.SetLineStyle( linestyle )
            gr2.SetName(name+"2")
            #gr2.Print()
            if gr2.GetN()>0: all_graphs+=[gr2]
        else:
            all_graphs+=[gr]
            
    if text is not '': leg.AddEntry(all_graphs[0],text,'l')
    print all_graphs
    return leg,all_graphs    


def ContourGraph(file_nominal, hist ):
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    #h.Print()
    h.GetYaxis().SetRangeUser(0,2800)
    h.GetXaxis().SetRangeUser(0,6100)
    gr = gr0.Clone(h.GetName())
    h.SetContour( 1 )
 
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    if list0.GetEntries()==0:
        return None

    #list.Print()
    gr = list0[0]
    #gr.Print()
    #grTmp = ROOT.TGraph()

    for k in xrange(list0.GetSize()):
        if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
    gr.SetName(hist.GetName())
    #print gr
    #gr.Print() 
    #print "\n"   
    
    #if 'GGM' in file_nominal:
    #    for j in range(0,gr.GetN()):
    #        if gr.GetY()[j] > 90. and gr.GetX()[j] > 200.: 
    #            gr.SetPoint(j,gr.GetX()[j],101.)
    
    points_to_be_removed=[]
    if not 'GGM' in file_nominal:
        my_min = min(gr.GetX())
        #print my_min
        diagonal_cut=35.
        diagonal_add=50.
        parallel_cut=1.
        offset=0.
        if 'VVh' in file_nominal: offset=125.
        if 'C1C1' in file_nominal:
            diagonal_cut=30.
            diagonal_add=50.
            parallel_cut=1.
        if 'GG' in file_nominal:
            diagonal_cut=35.
            diagonal_add=100.   
            parallel_cut=2.
        if 'C1N2z' in file_nominal or 'VV' in file_nominal:
            diagonal_cut=60.
            diagonal_add=100.
            parallel_cut=8.    
        if 'LV' in file_nominal:
            diagonal_cut=20.
            diagonal_add=50.
            parallel_cut=15.  
        for j in range(0,gr.GetN()):    
            if abs(gr.GetX()[j] - gr.GetY()[j]) <diagonal_cut+offset: # remove all points along the diagonal
                points_to_be_removed.append(j) 
            if abs(gr.GetX()[j] - min(gr.GetX())) < parallel_cut:   # remove the vertical line...
                points_to_be_removed.append(j)
                #print j
            #hack for gluino grids
            if 'GG' in file_nominal and 'LLEi33' in file_nominal:
               if abs(gr.GetX()[j] - gr.GetY()[j]) < 950. and gr.GetX()[j]<1300.: 
                points_to_be_removed.append(j) 
            if 'GG' in file_nominal and 'LLE12k' in file_nominal:
               if abs(gr.GetX()[j] - gr.GetY()[j]) < 1400. and gr.GetX()[j]<1500.: 
                points_to_be_removed.append(j)                
                #print j
            if 'LV' in file_nominal and 'LLE12k' in file_nominal:
               if abs(gr.GetX()[j] - gr.GetY()[j]) < 400. and gr.GetX()[j]<700.: 
                points_to_be_removed.append(j)                 
        #print points_to_be_removed
        
        for j in range(0,gr.GetN()):   
            if (gr.GetX()[j]-gr.GetY()[j]) < diagonal_add+offset: # Add a point up to the diagonal using the dM=10 GeV point
                gr.SetPoint(j, gr.GetX()[j], gr.GetX()[j]-(10.+offset))
        
        grtemp= ROOT.TGraph()
        j7=0
        xtemp=gr.GetX()
        ytemp=gr.GetY()        
        for j6 in range(1,gr.GetN()):
            if not j6 in points_to_be_removed:
                grtemp.SetPoint(j7,xtemp[j6],ytemp[j6])
                j7+=1
                
        #grtemp.Print()
        
        cut_points = 0
        x=grtemp.GetX()
        y=grtemp.GetY()
        for j2 in range(1,grtemp.GetN()):
            #print x[j2],x[j2-1]
            if x[j2]>x[j2-1] and x[j2-1]<my_min+150.:
                cut_points=j2
                break
        #print cut_points

        gr2 = ROOT.TGraph()
        j4=0
        for j3 in range(cut_points,grtemp.GetN()):
            gr2.SetPoint(j4,x[j3],y[j3])
            j4+=1
        for j5 in range(0,cut_points):
            gr2.SetPoint(j4,x[j5],y[j5])
            j4+=1

        return gr2
    
    return gr


def DrawExpectedBand( gr1, gr2, fillColor, fillStyle, cut = 0):

    number_of_bins = max(gr1.GetN(),gr2.GetN());
   
    gr1N = gr1.GetN();
    gr2N = gr2.GetN();
    #gr2.Print()

    N = number_of_bins;
   
    xx0 = ROOT.Double(0)
    yy0 = ROOT.Double(0)
    x0 = ROOT.Double(0)
    y0 = ROOT.Double(0)    
    xx1 = ROOT.Double(0)
    yy1 = ROOT.Double(0)
   
    x1=[]
    y1=[]
    x2=[]
    y2=[]    
   
    for j in xrange(gr1N):
        gr1.GetPoint(j,xx0,yy0)
        x1 += [float(xx0)]
        y1 += [float(yy0)]
    if gr1N < N:
        for i in xrange(gr1N,N):
            x1 += [ float(x1[gr1N-1]) ]
            y1 += [ float(y1[gr1N-1]) ]
   
    #gr2.Print()
    for j in xrange(gr2N):
        gr2.GetPoint(j,xx1,yy1)
        x2 += [float(xx1)]
        y2 += [float(yy1)]
    if gr2N < N:
        for i in xrange(gr2N,N):
            x2 += [ float(x2[gr2N-1]) ]
            y2 += [ float(y2[gr2N-1]) ]
            
    #print x1
    #print x2


    grshade = ROOT.TGraphAsymmErrors(2*N)
    for i in xrange(N):
        if x1[i] > cut:
            grshade.SetPoint(i,x1[i],y1[i])
        if x2[N-i-1] > cut:
            grshade.SetPoint(N+i,x2[N-i-1],y2[N-i-1])
   
    #grshade.Print()
   
    # Apply the cut in the shade plot if there is something that doesnt look good...
    Nshade = grshade.GetN()
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0!=0 and y0!=0 :
            x00 = x0
            y00 = y0
            break
            
    #print x00, y00
   
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0 == 0 and y0 == 0:
            grshade.SetPoint(j,x00,y00)
   
    #grshade.Print()
   
    # Now draw the plot...
    grshade.SetFillStyle(fillStyle);
    grshade.SetFillColor(fillColor);
    grshade.SetMarkerStyle(21);
    #grshade.Draw("Fsame");
    return grshade;


def DummyLegendExpected(leg, what,  fillColor, fillStyle, lineColor, lineStyle, lineWidth, dodis=-1):
    gr = ROOT.TGraph()
    gr.SetFillColor(fillColor)
    gr.SetFillStyle(fillStyle)
    gr.SetLineColor(lineColor)
    gr.SetLineStyle(lineStyle)
    gr.SetLineWidth(lineWidth)
    if dodis==3: what = "Expected discovery 3 #sigma"
    if dodis==5: what = "Expected discovery 5 #sigma"
    leg.AddEntry(gr,what,"LF")
    return leg,gr


def InterpretNames(filename):
     name = filename.split('_hypotest')[0].split('OneLepton_')[1]
     if "GG1stepx12" in name: name = name.split('_GG1stepx12')[0]
     if "GG1stepgridx" in name: name = name.split('_GG1stepgridx')[0]
     if "SS1stepx12" in name: name = name.split('_SS1stepx12')[0]
     if "SS1stepgridx" in name: name = name.split('_SS1stepgridx')[0]
     if "GG2WWZZ" in name: name = name.split('_GG2WWZZ')[0]
     if "GG2CNsl" in name: name = name.split('_GG2CNsl')[0]
     if "SS2WWZZ" in name: name = name.split('_SS2WWZZ')[0]
     if "SS2CNsl" in name: name = name.split('_SS2CNsl')[0]
     
     print name
     if name=="combined_5_6_jets": descriptionname="3, 5, 6 jets SRs combined, default"
     elif name=="combined_5_6_jets_onebin": descriptionname="3, 5, 6 jets SRs combined, one bin"
     elif name=="combined_5_6_loose_onebin": descriptionname="relaxed 3, 5, 6 jets SRs combined, one bin"
     elif name=="combined_5_6_jets_met3jet": descriptionname="3 (fit in E_{T}^{miss}), 5, 6 jets SRs combined"
     elif name=="combined_5_6_jets_met3jet_2": descriptionname="3 (fit in E_{T}^{miss}, hard m_{eff} cut), 5, 6 jets SRs combined"     
     elif name=="combined_5_6_jets_met3jet_onebin": descriptionname="3 (fit in E_{T}^{miss}), 5, 6 jets SRs combined, one bin"
     elif name=="combined_5_6_jets_meffonly": descriptionname="3, 5, 6 jets SRs combined, shape fit in m_{eff}" 
     elif name=="combined_5_6_jets_bjetveto": descriptionname="3, 5, 6 jets SRs combined, b-jet veto" 
     elif name=="combined_3_6_7_jets_onebin": descriptionname="3, 6, 7 jets SRs combined, one bin"
     elif name=="combined_3_6_7_jets": descriptionname="3, 6, 7 jets SRs combined" 
     elif name=="combined_3_5_jets": descriptionname="3 and 5 jets SRs combined" 
     elif name=="combined_20sys" or name=="combined_20sys_2": descriptionname="3, 5, 6 jets SRs combined, 20 % syst." 
     elif name=="combined_50sys" or name=="combined_50sys_2": descriptionname="3, 5, 6 jets SRs combined, 50 % syst." 
     elif name=="combined_80sys" or name=="combined_80sys_2": descriptionname="3, 5, 6 jets SRs combined, 80 % syst."
     elif name=="combined_30sys_2": descriptionname="3, 5, 6 jets SRs combined, 30 % syst."   
     elif name=="3jetSR_exclu": descriptionname="3 jet SR, exclusive"
     elif name=="3jetSR_inclu": descriptionname="3 jet SR, inclusive"
     elif name=="5jetSR_exclu": descriptionname="5 jet SR, exclusive"
     elif name=="5jetSR_inclu": descriptionname="5 jet SR, inclusive" 
     elif name=="6jetSR_exclu": descriptionname="6 jet SR, exclusive"
     elif name=="6jetSR_inclu": descriptionname="6 jet SR, inclusive" 
     elif name=="combined_5_6_jets_met3jet_mt6jet": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR)" 
     elif name=="combined_5_6_jets_met3jet_mt6jet_met250": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR), lower E_{T}^{miss} cut in 6 jets SR"   
     elif name=="combined_5_6_jets_met3jet_mt6jet_mt120": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR), lower m_{T} cut in 6 jets SR" 
     elif name=="combined_5_6_jets_met3jet_mt5jet": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{T} (5 jets SR), m_{eff} (6 jets SR)" 
     elif name=="combined_5_6_jets_met3jet_mt6jet_mt130_met300": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR); m_{T} > 130 GeV and E_{T}^{miss} > 300 GeV"  
     elif name=="combined_5_6_jets_met3jet_mt6jet_mt130": descriptionname="shape fit: E_{T}^{miss} (3 jets SR), m_{eff} (5 jets SR), m_{T} (6 jets SR); m_{T} > 130 GeV"  
     else: descriptionname = "No description found"
     
     
     return descriptionname
     
hh=[]     

def DrawContourMassLine(hist, mass, color=14 ):

  h = ROOT.TH2F( hist )
  hh.append(h)
  h.SetContour( 1 )
  h.SetContourLevel( 0, mass )

  h.SetLineColor( color )
  h.SetLineStyle( 7 )
  h.SetLineWidth( 1 )
  #h.Print("range")
  h.Draw( "samecont3" )
  
  return
  
  
def run1_limit():
    from array import array

    xval = array("f", [779.5, 779.5, 780.6, 781.7, 782.8, 783.8, 784.9, 786.0, 785.9, 784.8, 783.6, 782.5, 781.3, 780.2, 779.0, 778.3, 777.6, 776.1, 774.6, 773.1, 771.7, 770.2, 770.0, 768.7, 766.5, 763.5, 759.7, 755.9, 752.1, 750.0, 748.3, 741.8, 730.0, 729.4, 710.0, 690.0, 670.0, 655.5, 650.0, 630.0, 610.0, 590.0, 570.0, 550.0, 531.4])
    xerrminus = array("f", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    xerrplus = array("f", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    yval = array("f", [769.5, 757.2, 732.8, 708.2, 683.8, 659.2, 634.8, 610.2, 585.8, 561.2, 536.8, 512.2, 487.8, 463.2, 438.8, 414.2, 389.8, 365.2, 340.8, 316.2, 291.8, 267.2, 264.1, 242.8, 218.2, 193.8, 169.2, 144.8, 120.2, 106.4, 95.75, 71.25, 48.02, 46.75, 38.04, 31.08, 25.57, 22.25, 21.48, 19.05, 17.07, 14.94, 12.85, 11.22, 10.0])
    yerrminus = array("f", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    yerrplus = array("f", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    numpoints = 45
    p8549_d23x1y1 = ROOT.TGraphAsymmErrors(numpoints, xval, yval, xerrminus, xerrplus, yerrminus, yerrplus)
    p8549_d23x1y1.SetName("/HepData/8549/d23x1y1")
    p8549_d23x1y1.SetTitle("/HepData/8549/d23x1y1")
    p8549_d23x1y1.SetPoint(46,400.,400.)
    return p8549_d23x1y1