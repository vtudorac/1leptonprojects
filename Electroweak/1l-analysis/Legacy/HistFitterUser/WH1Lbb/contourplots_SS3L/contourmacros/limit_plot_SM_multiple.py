#!/bin/env python

import ROOT
ROOT.gROOT.ProcessLine('#include "contourmacros/CombinationGlob.C"')

ROOT.CombinationGlob.Initialize()

execfile("summary_harvest_tree_description.py")


def limit_plot_SM_multiple( file_nominal,  file_up=None, file_down=None, outputfilename='', prefix=None, lumi=21, drawFirstObs=False,firstOneSigma=False): 
    
    print_theo = True
    
    
    if file_up==None or file_down==None:
        print_theo = False
        print "No up or down variations given! Proceed without them."
    

    #if not 'HiggsSU' in file_nominal:
    #    execfile("summary_harvest_tree_description_SM.py")
    #else: 
    #    execfile("summary_harvest_tree_description_HiggsSU.py")

    """
    if 'GGx12' in file_nominal and not "gridx" in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/limit_contour_GG1stepx12_version150116_combSRcurves.root")
        #file_oldlimit = ROOT.TFile.Open("contourmacros/contour_GG1stepx12_combOneZero_version090215curves.root")
        graph = file_oldlimit.Get("firstObsH_graph0")
        graph3 = file_oldlimit.Get("firstExpH_graph0")
    elif 'GGgridx' in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/limit_contour_GG1stepgridx_version150116_combSRcurves.root")
        #file_oldlimit = ROOT.TFile.Open("contourmacros/contour_GG1stepgridx_combOneZero_version090215curves.root")
        graph = file_oldlimit.Get("firstObsH_graph0")
        graph3 = file_oldlimit.Get("firstExpH_graph0")
    elif 'SSx12' in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/contour_SS1stepx12_combhardsoft_version120115curves.root")
        graph = file_oldlimit.Get("firstObsH_graph")
        graph3 = file_oldlimit.Get("firstExpH_graph")
    elif 'SSgridx' in file_nominal:
        file_oldlimit = ROOT.TFile.Open("contourmacros/contour_SS1stepgridx_combhardsoft_version141114curves.root")   
        graph = file_oldlimit.Get("firstObsH_graph")
        graph3 = file_oldlimit.Get("firstExpH_graph")
    else:
        file_oldlimit = ROOT.TFile.Open("contourmacros/limit_contour_GG1stepgridx_version150116_combSRcurves.root")
        graph = file_oldlimit.Get("firstObsH_graph0")
        graph3 = file_oldlimit.Get("firstExpH_graph0")
        
    graph.Print()        
    graph.SetFillColor(17)
    graph.SetLineWidth(2)
    graph.SetMarkerStyle(21)
    graph.SetMarkerSize(0.3)
    graph.SetFillStyle(1001)
    graph.SetLineColor(15)
    graph2=graph.Clone()
    graph2.SetLineStyle(3)
    """

    #graph.SetDirectory(0)

    print 'Producing contour plot based on ', file_nominal

    firstObsH = {}
    firstExpH = {}
    histosOrig = {}
    firstObs=None
    firstExp=None
    firstPOneSigma = None
    firstMOneSigma = None
    firstPOneSigmaG = {}
    firstMOneSigmaG = {}   
    upObs = None
    downObs = None
       


    i=0
    for filename in file_nominal.split(","):
        f = ROOT.TFile.Open(filename,'READ')
        firstObs = f.Get( 'sigp1clsf' )
        firstExp = f.Get( 'sigp1expclsf' )
        firstPOneSigma = f.Get( 'sigclsu1s' )
        firstMOneSigma = f.Get( 'sigclsd1s' )      
        
        for hizzie in [firstObs,firstExp,firstPOneSigma,firstMOneSigma]: hizzie.SetDirectory(0)
        f.Close()     
        firstObsH[filename] = FixAndSetBorders(filename, firstObs , 'firstObsH'+str(i) , '' , 0 )
        firstExpH[filename] = FixAndSetBorders(filename, firstExp , 'firstExpH'+str(i) , '' , 0 )
        
        firstPOneSigmaG[filename] =ROOT.TGraph()
        firstMOneSigmaG[filename] =ROOT.TGraph()
    
        if firstOneSigma: # Only need these for the +/-1 sigma band
            firstPOneSigmaG_pre = ContourGraph(filename, FixAndSetBorders(file_nominal, firstPOneSigma , 'firstPOneSigmaG' , '' , 0 ) )
            if firstPOneSigmaG_pre!=None: firstPOneSigmaG[filename] = firstPOneSigmaG_pre.Clone()
        firstMOneSigmaG_pre = ContourGraph(filename, FixAndSetBorders(file_nominal, firstMOneSigma , 'firstMOneSigmaG' , '' , 0 ) )
        if firstMOneSigmaG_pre!=None: firstMOneSigmaG[filename] = firstMOneSigmaG_pre.Clone()
        if firstMOneSigmaG[filename].GetN()==0 or firstPOneSigmaG[filename].GetN()==0: firstOneSigma=False
        i+=1

    
    if print_theo:
        histos={}
        j=0
        for filename in file_nominal.split(","):
            f_up = ROOT.TFile.Open(file_up.split(",")[j],'READ')
            upObs = f_up.Get( 'sigp1clsf' ) 
            upObs.SetDirectory(0)
            f_up.Close()
            f_down = ROOT.TFile.Open(file_down.split(",")[j],'READ')
            downObs = f_down.Get( 'sigp1clsf' ) 
            downObs.SetDirectory(0)
            f_down.Close() 
            histosOrig[filename] = [upObs, downObs]      
            j+=1
    
            histos[filename]  = []
            for h in histosOrig[filename]: histos[filename] += [ FixAndSetBorders(filename, h , h.GetName()+'H' , '' , 0 ) ]
        
        
        print histos

    
    # set text style
    ROOT.gStyle.SetPaintTextFormat(".2g")

    # Start drawing
    c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    #c = ROOT.TCanvas('LimitPlot','A limit plot', 0 , 0 ,600, 600)
    
    pwd=ROOT.gDirectory

    # create and draw the frame2 
    if 'GG_' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 110, 1000., 2600., 100, 10., 2600. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )        
    elif 'C1C1' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 400., 1400., 100, 10., 1300. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )
    elif 'C1N2z' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 600., 1600., 100, 10., 1700. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'VV' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 600., 1600., 100, 10., 1700. )
        frame2.SetXTitle( "m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )    
    elif 'LV' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 450., 1200., 100, 10., 1200. )
        frame2.SetXTitle( "m_{#tilde{l}_{L}/#tilde{#nu}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )            
    elif 'GGM' in file_nominal:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 0., 800., 100, 0., 200. )
        frame2.SetXTitle( "#tilde{h}" )
        frame2.SetYTitle( "BR" )
    else:
        frame2 = ROOT.TH2F('frame2', 'Simplified model limit for comparisons', 100, 225., 1500., 100, 25., 1025. )
        frame2.SetXTitle( "m_{#tilde{g}} [GeV]" )
        frame2.SetYTitle( "m_{#tilde{#chi}^{0}_{1}} [GeV]" )

    ROOT.CombinationGlob.SetFrameStyle2D( frame2, 1.0 )

    frame2.GetYaxis().SetTitleOffset(1.35)
 
    frame2.GetXaxis().SetTitleFont( 42 )
    frame2.GetYaxis().SetTitleFont( 42 )
    frame2.GetXaxis().SetLabelFont( 42 )
    frame2.GetYaxis().SetLabelFont( 42 )
 
    frame2.GetXaxis().SetTitleSize( 0.04 )
    frame2.GetYaxis().SetTitleSize( 0.04 )
    frame2.GetXaxis().SetLabelSize( 0.04 )
    frame2.GetYaxis().SetLabelSize( 0.04 )
 
    frame2.Draw()
    
#    if 'GGx12' in file_nominal or 'GGgridx' in file_nominal or 'SSx12' in file_nominal or 'SSgridx' in file_nominal: 
    #graph.Draw("Fsame")     
    #graph2.Draw("Lsame")
    # Set up the legend
    leg = ROOT.TLegend(0.15,0.57,0.37,0.76)
    #leg = ROOT.TLegend(0.14,0.6,0.37,0.74)
    leg.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize );
    leg.SetTextSize( 0.03 );
    leg.SetTextFont( 42 );
    leg.SetFillColor( 0 );
    leg.SetFillStyle(1001);


    
    colors = []
    
    c_myYellow   = ROOT.TColor.GetColor("#ffe938")
    c_myRed      = ROOT.TColor.GetColor("#aa000")
    c_myExp      = ROOT.TColor.GetColor("#28373c")     
    
    for SRregion in file_nominal.split(','):
        if 'i33' in SRregion: colors.append(ROOT.CombinationGlob.c_VDarkBlue )#
        elif '12k' in SRregion: colors.append(c_myRed)#


        else:
            print "Unknown SR! - Set dummy colour."
            colors.append(ROOT.CombinationGlob.c_DarkGreen)
    
    if len(colors)<len(firstObsH):
        print 'Only have',len(colors),'colors for',len(firstObsH),'histograms.  Will crash...'
        
    # Draw the +/- 1 sigma yellow band 
    if firstOneSigma: 
        grshadeExp={}
        for filename in file_nominal.split(","):
            if 'i33' in filename: 
                grshadeExp[filename] = DrawExpectedBand( firstPOneSigmaG[filename]  , firstMOneSigmaG[filename]  , ROOT.CombinationGlob.c_LightBlue , 1001 , 0).Clone()
            elif '12k' in filename: 
                grshadeExp[filename] = DrawExpectedBand( firstPOneSigmaG[filename]  , firstMOneSigmaG[filename]  , ROOT.CombinationGlob.c_DarkYellow , 1001 , 0).Clone() 
            else: 
                grshadeExp[filename] = DrawExpectedBand( firstPOneSigmaG[filename]  , firstMOneSigmaG[filename]   , ROOT.CombinationGlob.c_DarkYellow , 1001 , 0).Clone()
            grshadeExp[filename].Draw("Fsame") 

    newHists = []

    c2 = ROOT.TCanvas("dummy","dummy", 0 , 0 , ROOT.CombinationGlob.StandardCanvas[0] , ROOT.CombinationGlob.StandardCanvas[1] )
    #c2 = ROOT.TCanvas("dummy","dummy", 0 , 0 , 700, 1000)
    

    
    if print_theo:
        anewhist={}
        anewhist_pre={}        
        for filename in file_nominal.split(","):
            print len(histos[filename])
            print histos[filename]
            anewhist[filename]={}
            anewhist_pre[filename]={}
            for i in xrange(len(histos[filename])):
                print i
                try:
                    anewhist[filename][i]=[]
                    if 'i33' in filename: (leg,anewhist_pre[filename][i]) = DrawContourLine95Graph(filename, leg, histos[filename][i],histos[filename][i].GetName()+str(i), '' , ROOT.CombinationGlob.c_VDarkBlue , 3, 2) 
                    elif '12k' in filename: (leg,anewhist_pre[filename][i]) = DrawContourLine95Graph(filename, leg, histos[filename][i],histos[filename][i].GetName()+str(i), '' , c_myRed , 3, 2) 
                    else: (leg,anewhist_pre[filename][i]) = DrawContourLine95Graph(filename, leg, histos[filename][i],histos[filename][i].GetName()+str(i), '' , c_myRed , 3, 2) 
                    for hist in anewhist_pre[filename][i]:
                        dummy = ROOT.TGraph()
                        dummy = hist.Clone()
                        anewhist[filename][i].append(dummy)
                except:
                    print "One or more of the +-1 sigma variations of the observed limit does not exist"

    entry2 = leg.AddEntry("","Observed (#pm 1 #sigma_{theory}^{SUSY})","l")
    entry2.SetLineColor(ROOT.kBlack) 
    entry2.SetLineWidth(4)
    entry3 = leg.AddEntry("","Expected (#pm 1 #sigma_{exp})","l")
    entry3.SetLineColor(ROOT.kBlack) 
    entry3.SetLineWidth(4)    
    entry3.SetLineStyle(2)  

    j=0
    
    anewhist11=[]#ROOT.TGraph()
    anewhist00=[]#ROOT.TGraph()   
    anewhist000=[]#ROOT.TGraph() 
    for mykey in file_nominal.split(","):   
        try:
            (leg,anewhist1) = DrawContourLine95Graph(mykey, leg, firstExpH[mykey],firstExpH[mykey].GetName()+"_graph", '', colors[j], 6, 2 ) 
            for hist in anewhist1:
                dummy = ROOT.TGraph()
                dummy = hist.Clone()
                anewhist11.append(dummy)
            #anewhist11=anewhist1.Clone()
            #ROOT.gDirectory.Append(anewhist11)
            #(leg,anewhist000) = DummyLegendExpected( leg, 'dummy', 0, 1001, colors[j], 1, 4 )
            #c.cd()
            #anewhist11.Draw("Lsame")
            #newHists.append(anewhist11)
        except:
            print "Expected limit does not exist for "+mykey        

        
        if drawFirstObs:
            try: 
                print mykey
                (leg,anewhist0) = DrawContourLine95Graph(mykey, leg, firstObsH[mykey],firstObsH[mykey].GetName()+"_graph", '', colors[j],1,4)
                for hist in anewhist0:
                    dummy = ROOT.TGraph()
                    dummy = hist.Clone()
                    anewhist00.append(dummy)                
                #anewhist00=anewhist0.Clone()
                #c.cd()                
                if '12k' in mykey: 
                    entry = leg.AddEntry("",InterpretNames(mykey),'lf')
                    entry.SetFillColor(ROOT.CombinationGlob.c_DarkYellow)
                    entry.SetLineColor(c_myRed)
                    entry.SetLineWidth(4)
                elif 'i33' in mykey: 
                    entry = leg.AddEntry("",InterpretNames(mykey),'lf')
                    entry.SetFillColor(ROOT.CombinationGlob.c_LightBlue)
                    entry.SetLineColor(ROOT.CombinationGlob.c_VDarkBlue)
                    entry.SetLineWidth(4)
                else:
                    entry = leg.AddEntry("",InterpretNames(mykey),'lf')
                    entry.SetFillColor(ROOT.CombinationGlob.c_DarkYellow)
                    entry.SetLineColor(c_myRed) 
                    entry.SetLineWidth(4)
                entry.SetFillStyle(1001)
                #newHists.append(anewhist00)
            except:
                print "Observed limit does not exist for "+mykey             
        j+=1
        
          
          
    newHists.append(anewhist11)         
    newHists.append(anewhist00)

    pwd.cd()
    #pwd.ls()
    c.cd()
    c.Update() 
    
    if print_theo:   
        #print len(histos)
        #print histos
        #print anewhist
        for i2 in anewhist.keys():
            #print i2
            anewhist[i2].keys()
            for i3 in anewhist[i2].keys():
                #print i3
                for i4 in anewhist[i2][i3]:
                    i4.Draw("Lsame") 

    
    #print newHists
    
    for h in newHists:
        #print h
        for h2 in h:
            #print h2
            h2.Draw("Lsame")
     
    c.Update()
    
    #if 'GGx12' in file_nominal or 'GGgridx' in file_nominal or 'SSx12' in file_nominal or 'SSgridx' in file_nominal: 
    #    graph.Draw("Fsame") 

    # legend
    textSizeOffset = +0.000;
    xmax = frame2.GetXaxis().GetXmax()
    xmin = frame2.GetXaxis().GetXmin()
    ymax = frame2.GetYaxis().GetXmax()
    ymin = frame2.GetYaxis().GetXmin()
    dx   = xmax - xmin
    dy   = ymax - ymin

    # Label the decay process
    Leg0 = None
    if 'GG' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}#tilde{g} #rightarrow qqqqllll#nu#nu" )
    elif 'C1N2z' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{0}_{2} #rightarrow WZllll#nu#nu" )
    elif 'VVz' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{0}_{2} #rightarrow WW/Zllll#nu#nu" ) 
    elif 'VVh' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{0}_{2} #rightarrow WW/hllll#nu#nu" )     
    elif   'LV' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{l}_{L}/#tilde{#nu} #tilde{l}_{L}/#tilde{#nu} #rightarrow l/#nul/#nullll#nu#nu" )    
    elif   'C1C1' in file_nominal: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{#chi}_{1}^{#pm}#tilde{#chi}^{#mp}_{1} #rightarrow WWllll#nu#nu" ) ##tilde{g}-#tilde{g}, #tilde{g}#rightarrow q#bar{q}'W#tilde{#chi}_{1}^{0}" )
    else: Leg0 = ROOT.TLatex( xmin, ymax + dy*0.025, "#tilde{g}-#tilde{g} #rightarrow qqqqWW#tilde{#chi}_{1}^{0}#tilde{#chi}_{1}^{0}, x=1/2" )
    


    if Leg0 is not None:
        Leg0.SetTextAlign( 11 );
        Leg0.SetTextFont( 42 );
        Leg0.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
        Leg0.SetTextColor( 1 );
        Leg0.AppendPad();

    #Leg3 = ROOT.TLatex( (xmin+xmax)*0.6, ymax + dy*0.025, 'Expected Limit Comparison' )
    #Leg3.SetTextAlign( 11 );
    #Leg3.SetTextFont( 42 );
    #Leg3.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize);
    #Leg3.SetTextColor( 1 );
    #Leg3.AppendPad();


    #frame2.Draw( 'sameaxis' )
    #leg.Draw( 'same' )
    
    c.Update()
    
    leg.Draw( 'same' )
    Leg1 = ROOT.TLatex()
    Leg1.SetNDC()
    Leg1.SetTextAlign( 11 )
    Leg1.SetTextFont( 42 )
    Leg1.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg1.SetTextColor( 1 )
    Leg1.DrawLatex(0.16,0.82, '#sqrt{s}=13 TeV, %.1f fb^{-1}'%(lumi) )
    Leg1.AppendPad()

    Leg2 = ROOT.TLatex()
    Leg2.SetNDC()
    Leg2.SetTextAlign( 11 )
    Leg2.SetTextSize( ROOT.CombinationGlob.DescriptionTextSize )
    Leg2.SetTextColor( 1 )
    Leg2.SetTextFont(42)
    if prefix is not None: 
        Leg2.DrawLatex(0.16,0.77,prefix)
        Leg2.AppendPad()
        
    c.Update()

    frame2.Draw( 'sameaxis' )
    #leg.Draw( 'same' )

    # Diagonal line
    diagonal = None 
    if ("LL" in file_nominal): diagonal = ROOT.TLine(400., 390., 1300., 1290.)
    if ("GG" in file_nominal): diagonal = ROOT.TLine(1000., 990., 2600., 2590.)
    if ("C1N2z" in file_nominal): diagonal = ROOT.TLine(600., 590., 1600., 1590.)
    if ("VVz" in file_nominal): diagonal = ROOT.TLine(600., 590., 1600., 1590.)  
    if ("VVh" in file_nominal): diagonal = ROOT.TLine(600., 590.-125., 1600., 1590.-125.)      
    if ("LV" in file_nominal): diagonal = ROOT.TLine(450., 440., 1200., 1190.)       
        
    if diagonal: 
        diagonal.SetLineStyle(2)
        diagonal.Draw()
    
    gtt = ROOT.TLatex(450,490,"m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV")
    if "C1N2z" in file_nominal:
        gtt.SetText(1050,1090,"m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")
    if "VVz" in file_nominal:
        gtt.SetText(1050,1090,"m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")
    if "VVh" in file_nominal:
        gtt.SetText(1050,965,"m_{#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV + m_{h} ")       
    if "LV" in file_nominal:
        gtt.SetText(850,890,"m_{#tilde{l}_{L}/#tilde{#nu}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")    
    if "GG" in file_nominal:
        gtt.SetText(1650,1710,"m_{#tilde{g}} - m_{#tilde{#chi}^{0}_{1}} < 10 GeV ")            
    #else: gtt.SetText(550,590,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}")

    gtt.SetTextSize(0.03)
    gtt.SetTextColor(12)
    gtt.SetTextAngle(24)
    if "SS" in file_nominal:
        gtt.SetTextAngle(40)
    gtt.SetTextFont(42)

    if not "gridx" in file_nominal:
        gtt.Draw("same")    
                   

    # update the canvas
    c.Update()
    
    clslimits = ROOT.TLatex()
    #clslimits = ROOT.TLatex(360,887-197,"All limits at 95% CL_{S}")
    clslimits.SetNDC()
    clslimits.SetTextSize(0.03)
    clslimits.SetTextFont(42)
    if 'GG' in file_nominal.split(",")[0] or 'VV' in file_nominal.split(",")[0] or 'LV' in file_nominal.split(",")[0] or 'RH' in file_nominal.split(",")[0]:
        clslimits.DrawLatex(0.5,0.85,"All limits at 95% CL") 
    else:
        clslimits.DrawLatex(0.17,0.7,"All limits at 95% CL")  
  
    atlasLabel = ROOT.TLatex()
    atlasLabel.SetNDC()
    atlasLabel.SetTextFont(72)
    atlasLabel.SetTextColor(ROOT.kBlack)
    atlasLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    atlasLabel.DrawLatex(0.6, 0.85,"ATLAS")
    #else:
    atlasLabel.DrawLatex(0.16, 0.87,"ATLAS")
    atlasLabel.AppendPad()

    progressLabel = ROOT.TLatex()
    progressLabel.SetNDC()
    progressLabel.SetTextFont(42)
    progressLabel.SetTextColor(ROOT.kBlack)
    progressLabel.SetTextSize( 0.05 )

    #if "gridx" in file_nominal: 
    #    progressLabel.DrawLatex(0.76, 0.85,"Internal")
    #else:
    #print 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())
    progressLabel.DrawLatex(0.16+0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()), 0.87,"Internal")
    #else: progressLabel.DrawLatex(0.19, 0.81,"Internal")
    progressLabel.AppendPad()
    
    #now expected lines for legend
    expectedline = ROOT.TLine()
    expectedline.SetLineStyle(3)
    expectedline.SetLineWidth(2)
    expectedline.SetLineColor(ROOT.kBlack)
    expectedline.DrawLineNDC(0.16,0.725,0.20,0.725)
    expectedline.DrawLineNDC(0.16,0.745,0.20,0.745)
    
    c.Update()    
    
    print "Here"
    
    if outputfilename=='':
        outFileNom = 'plots/limit_'+file_nominal.split(",")[0].replace('.root','')
    else:
        outFileNom = 'plots/limit_'+outputfilename

    ROOT.CombinationGlob.imgconv( c, outFileNom );

    del leg
    del frame2


def MirrorBorders( hist ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    # corner points
    hist.SetBinContent(0,0,hist.GetBinContent(1,1))
    hist.SetBinContent(numx+1,numy+1,hist.GetBinContent(numx,numy))
    hist.SetBinContent(numx+1,0,hist.GetBinContent(numx,1))
    hist.SetBinContent(0,numy+1,hist.GetBinContent(1,numy))

    # Fix the other points 
    for i in xrange(1,numx+1):
        hist.SetBinContent(i,0,       hist.GetBinContent(i,1));
        hist.SetBinContent(i,numy+1, hist.GetBinContent(i,numy));
    for i in xrange(1,numy+1):
        hist.SetBinContent(0,i,      hist.GetBinContent(1,i));
        hist.SetBinContent(numx+1,i, hist.GetBinContent(numx,i));


def AddBorders( hist, name='StupidName', title='StupidTitle'):
    nbinsx = hist.GetNbinsX()
    nbinsy = hist.GetNbinsY()
  
    xbinwidth = ( hist.GetXaxis().GetBinCenter(nbinsx) - hist.GetXaxis().GetBinCenter(1) ) / float(nbinsx-1)
    ybinwidth = ( hist.GetYaxis().GetBinCenter(nbinsy) - hist.GetYaxis().GetBinCenter(1) ) / float(nbinsy-1)
  
    xmin = hist.GetXaxis().GetBinCenter(0) - xbinwidth/2. 
    xmax = hist.GetXaxis().GetBinCenter(nbinsx+1) + xbinwidth/2. 
    ymin = hist.GetYaxis().GetBinCenter(0) - ybinwidth/2. 
    ymax = hist.GetYaxis().GetBinCenter(nbinsy+1) + ybinwidth/2. 
  
    hist2 = ROOT.TH2F(name, title, nbinsx+2, xmin, xmax, nbinsy+2, ymin, ymax);
  
    for ibin1 in xrange(hist.GetNbinsX()+2):
        for ibin2 in xrange(hist.GetNbinsY()+2):
            hist2.SetBinContent( ibin1+1, ibin2+1, hist.GetBinContent(ibin1,ibin2) );
  
    return hist2


def SetBorders( hist, val=0 ):
    numx = hist.GetNbinsX()
    numy = hist.GetNbinsY()
  
    for i in xrange(numx+2):
        hist.SetBinContent(i,0,val);
        hist.SetBinContent(i,numy+1,val);
    for i in xrange(numy+2):
        hist.SetBinContent(0,i,val);
        hist.SetBinContent(numx+1,i,val);


def FixAndSetBorders(file_nominal, hist, name='hist3', title='hist3', val=0 ):
    hist0 = hist.Clone() # histogram we can modify
    
    #if not "gridx" in file_nominal:
    if not "gridx" in file_nominal and not "CNsl" in file_nominal:
        MirrorBorders( hist0 ) # mirror values of border bins into overflow bins
        #if 'GG1stepx12' in file_nominal:
        #    MirrorBorders( hist0 )
        #    MirrorBorders( hist0 )
        #    MirrorBorders( hist0 )
        #    print "Here"
    
    hist1 = AddBorders( hist0, "hist1", "hist1" );   
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    SetBorders( hist1, val );                              
    # set overflow bins to value 1
    
    histX = AddBorders( hist1, "histX", "histX" )
    # add new border of bins around original histogram,
    # ... so 'overflow' bins become normal bins
    
    hist3 = histX.Clone()
    hist3.SetName( name )
    hist3.SetTitle( title )
    
    del hist0, hist1, histX
    return hist3 # this can be used for filled contour histograms


def DrawContourLine95( leg, hist, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2 ):
    # contour plot
    h = hist.Clone()
    h.SetContour(1)
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
  
    #print linewidth, linestyle
    h.SetLineColor( linecolor )
    h.SetLineWidth( linewidth )
    h.SetLineStyle( linestyle )
    #h.Print()
    h.Draw( "samecont3" );
    
    if text is not '': leg.AddEntry(h,text,'l')
    return leg,h
    
def DrawContourLine95Graph(file_nominal, leg, hist,name, text='', linecolor=ROOT.kBlack, linestyle=2, linewidth=2 ):
    # contour plot
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    gr = gr0.Clone(h.GetName())
    h.SetContour(1)
    
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    #print "Number of lists: ",
    #print list0.GetEntries()
    if list0.GetEntries()==0:
        return None
    
    #list0.Print()
    
    all_graphs = []
    
    for k in xrange(list0.GetSize()):
        gr = list0[k]
        #gr.Print()
        #grTmp = ROOT.TGraph()

        #for k in xrange(list0.GetSize()):
        #    if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
        gr.SetName(name+str(k))
        #print gr
        #print "\n"
        #gr.Print()
        #print linewidth, linestyle
        gr.SetLineColor( linecolor )
        gr.SetLineWidth( linewidth )
        gr.SetLineStyle( linestyle )
        #h.Print()
        #gr.Draw( "same" );
    
        points_to_be_removed=[]
        if True:                    
            my_min = min(gr.GetX())
            diagonal_cut=35.
            diagonal_add=50.
            parallel_cut=1.
            offset=0.
            if 'VVh' in file_nominal: offset=125.
            if 'C1C1' in file_nominal:
                diagonal_cut=35.
                diagonal_add=50.
                parallel_cut=1.
            if 'GG' in file_nominal:
                diagonal_cut=22.
                diagonal_add=100.
                parallel_cut=2.
            if 'C1N2z' in file_nominal:
                diagonal_cut=60.
                diagonal_add=100.
                parallel_cut=8.  
            if 'VV' in file_nominal:
                diagonal_cut=21.
                diagonal_add=90.
                parallel_cut=8.                  
            if 'LV' in file_nominal:
                diagonal_cut=22#22
                diagonal_add=50#50.
                parallel_cut=8.#8.                
            for j in range(0,gr.GetN()):    
                if abs(gr.GetX()[j] - gr.GetY()[j]) <diagonal_cut+offset: # remove all points along the diagonal
                    points_to_be_removed.append(j)   
                if abs(gr.GetX()[j] - min(gr.GetX())) < parallel_cut:   # remove the vertical line...
                    points_to_be_removed.append(j)  
                
            for j in range(0,gr.GetN()):   
                if (gr.GetX()[j]-gr.GetY()[j]) < (diagonal_add+offset): # Add a point up to the diagonal using the dM=10 GeV point
                    gr.SetPoint(j, gr.GetX()[j], gr.GetX()[j]-(10.+offset))

            grtemp= ROOT.TGraph()
            j7=0
            xtemp=gr.GetX()
            ytemp=gr.GetY()        
            for j6 in range(1,gr.GetN()):
                if not j6 in points_to_be_removed:
                    grtemp.SetPoint(j7,xtemp[j6],ytemp[j6])
                    j7+=1
                
            #grtemp.Print()
            
            if grtemp.GetN()==0: continue
    
            #print points_to_be_removed    
        
            cut_points = 0.
            x=grtemp.GetX()
            y=grtemp.GetY()
            for j2 in range(1,grtemp.GetN()):
                #print x[j2],x[j2-1]
                if x[j2]>x[j2-1] and x[j2-1]<my_min+50.:
                    cut_points=j2
                    break
            print cut_points
    
            gr2 = ROOT.TGraph()
            j4=0
            for j3 in range(cut_points,grtemp.GetN()):
                gr2.SetPoint(j4,x[j3],y[j3])
                j4+=1
            for j5 in range(0,cut_points):
                gr2.SetPoint(j4,x[j5],y[j5])
                j4+=1
            gr2.SetLineColor( linecolor )
            gr2.SetLineWidth( linewidth )
            gr2.SetLineStyle( linestyle )
            gr2.SetName(name+"2")
            if gr2: all_graphs+=[gr2]
            
            #gr2.Print()
            
        else:
            all_graphs+=[gr]
            
    if text is not '': leg.AddEntry(all_graphs[0],text,'l')
    return leg,all_graphs    


def ContourGraph(file_nominal, hist ):
    gr0 = ROOT.TGraph()
    h = hist.Clone()
    #h.Print()
    h.GetYaxis().SetRangeUser(0,2800)
    h.GetXaxis().SetRangeUser(0,6100)
    gr = gr0.Clone(h.GetName())
    h.SetContour( 1 )
 
    pval = ROOT.CombinationGlob.cl_percent[1]
    signif = ROOT.TMath.NormQuantile(1-pval)
    h.SetContourLevel( 0, signif )
    h.Draw("CONT LIST")
    h.SetDirectory(0)
    ROOT.gPad.Update()
 
    contours = ROOT.gROOT.GetListOfSpecials().FindObject("contours")
    #print contours
    list0 = contours[0]
    #list0.Print()
    if list0.GetEntries()==0:
        return None

    #list.Print()
    gr = list0[0]
    #gr.Print()
    #grTmp = ROOT.TGraph()

    for k in xrange(list0.GetSize()):
        if gr.GetN() < list0[k].GetN(): gr = list0[k]
 
    gr.SetName(hist.GetName())
    #print gr
    #gr.Print() 
    #print "\n"   
    
    points_to_be_removed=[]
    if True:
        my_min = min(gr.GetX())
        #print my_min
        diagonal_cut=35.
        diagonal_add=50.
        parallel_cut=1.
        offset=0.
        if 'VVh' in file_nominal: offset=125.
        if 'C1C1' in file_nominal:
            diagonal_cut=30.
            diagonal_add=50.
            parallel_cut=1.
        if 'GG' in file_nominal:
            diagonal_cut=35.
            diagonal_add=100.   
            parallel_cut=2.
        if 'C1N2z' in file_nominal or 'VV' in file_nominal:
            diagonal_cut=60.
            diagonal_add=100.
            parallel_cut=8.    
        if 'LV' in file_nominal:
            diagonal_cut=20.
            diagonal_add=50.
            parallel_cut=15.  
        for j in range(0,gr.GetN()):    
            if abs(gr.GetX()[j] - gr.GetY()[j]) <diagonal_cut+offset: # remove all points along the diagonal
                points_to_be_removed.append(j) 
            if abs(gr.GetX()[j] - min(gr.GetX())) < parallel_cut:   # remove the vertical line...
                points_to_be_removed.append(j)
                #print j
            #hack for gluino grids
            if 'GG' in file_nominal and 'LLEi33' in file_nominal:
               if abs(gr.GetX()[j] - gr.GetY()[j]) < 950. and gr.GetX()[j]<1300.: 
                points_to_be_removed.append(j) 
            if 'GG' in file_nominal and 'LLE12k' in file_nominal:
               if abs(gr.GetX()[j] - gr.GetY()[j]) < 1400. and gr.GetX()[j]<1500.: 
                points_to_be_removed.append(j)                
                #print j
            if 'LV' in file_nominal and 'LLE12k' in file_nominal:
               if abs(gr.GetX()[j] - gr.GetY()[j]) < 400. and gr.GetX()[j]<700.: 
                points_to_be_removed.append(j)                 
        #print points_to_be_removed
        
        for j in range(0,gr.GetN()):   
            if (gr.GetX()[j]-gr.GetY()[j]) < (diagonal_add+offset): # Add a point up to the diagonal using the dM=10 GeV point
                gr.SetPoint(j, gr.GetX()[j], gr.GetX()[j]-(10.+offset))
        
        grtemp= ROOT.TGraph()
        j7=0
        xtemp=gr.GetX()
        ytemp=gr.GetY()        
        for j6 in range(1,gr.GetN()):
            if not j6 in points_to_be_removed:
                grtemp.SetPoint(j7,xtemp[j6],ytemp[j6])
                j7+=1
                
        #grtemp.Print()
        
        cut_points = 0
        x=grtemp.GetX()
        y=grtemp.GetY()
        for j2 in range(1,grtemp.GetN()):
            #print x[j2],x[j2-1]
            if x[j2]>x[j2-1] and x[j2-1]<my_min+150.:
                cut_points=j2
                break
        #print cut_points

        gr2 = ROOT.TGraph()
        j4=0
        for j3 in range(cut_points,grtemp.GetN()):
            gr2.SetPoint(j4,x[j3],y[j3])
            j4+=1
        for j5 in range(0,cut_points):
            gr2.SetPoint(j4,x[j5],y[j5])
            j4+=1
            
        #gr2.Print()    

        return gr2
    
    return gr


def DrawExpectedBand( gr1, gr2, fillColor, fillStyle, cut = 0):

    number_of_bins = max(gr1.GetN(),gr2.GetN());
   
    gr1N = gr1.GetN();
    gr2N = gr2.GetN();
    #gr2.Print()

    N = number_of_bins;
   
    xx0 = ROOT.Double(0)
    yy0 = ROOT.Double(0)
    x0 = ROOT.Double(0)
    y0 = ROOT.Double(0)    
    xx1 = ROOT.Double(0)
    yy1 = ROOT.Double(0)
   
    x1=[]
    y1=[]
    x2=[]
    y2=[]    
   
    for j in xrange(gr1N):
        gr1.GetPoint(j,xx0,yy0)
        x1 += [float(xx0)]
        y1 += [float(yy0)]
    if gr1N < N:
        for i in xrange(gr1N,N):
            x1 += [ float(x1[gr1N-1]) ]
            y1 += [ float(y1[gr1N-1]) ]
   
    #gr2.Print()
    for j in xrange(gr2N):
        gr2.GetPoint(j,xx1,yy1)
        x2 += [float(xx1)]
        y2 += [float(yy1)]
    if gr2N < N:
        for i in xrange(gr2N,N):
            x2 += [ float(x2[gr2N-1]) ]
            y2 += [ float(y2[gr2N-1]) ]
            
    #print x1
    #print x2


    grshade = ROOT.TGraphAsymmErrors(2*N)
    for i in xrange(N):
        if x1[i] > cut:
            grshade.SetPoint(i,x1[i],y1[i])
        if x2[N-i-1] > cut:
            grshade.SetPoint(N+i,x2[N-i-1],y2[N-i-1])
   
    #grshade.Print()
   
    # Apply the cut in the shade plot if there is something that doesnt look good...
    Nshade = grshade.GetN()
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0!=0 and y0!=0 :
            x00 = x0
            y00 = y0
            break
            
    #print x00, y00
   
    for j in xrange(Nshade):
        grshade.GetPoint(j,x0,y0)
        if x0 == 0 and y0 == 0:
            grshade.SetPoint(j,x00,y00)
   
    #grshade.Print()
   
    # Now draw the plot...
    grshade.SetFillStyle(fillStyle);
    grshade.SetFillColor(fillColor);
    grshade.SetMarkerStyle(21);
    #grshade.Draw("Fsame");
    return grshade;


def DummyLegendExpected(leg, what,  fillColor, fillStyle, lineColor, lineStyle, lineWidth):
    gr = ROOT.TGraph()
    gr.SetFillColor(fillColor)
    gr.SetFillStyle(fillStyle)
    gr.SetLineColor(lineColor)
    gr.SetLineStyle(lineStyle)
    gr.SetLineWidth(lineWidth)
    leg.AddEntry(gr,what,"LF")
    return leg,gr


def InterpretNames(filename):
     if 'i33' in filename: descriptionname = "#lambda_{i33} #neq 0"     
     elif '12k' in filename: descriptionname = "#lambda_{12k} #neq 0"    
     
     else: descriptionname = "unknown region"
     
     return descriptionname
     
hh=[]     

def DrawContourMassLine(hist, mass, color=14 ):

  h = ROOT.TH2F( hist )
  hh.append(h)
  h.SetContour( 1 )
  h.SetContourLevel( 0, mass )

  h.SetLineColor( color )
  h.SetLineStyle( 7 )
  h.SetLineWidth( 1 )
  #h.Print("range")
  h.Draw( "samecont3" )
  
  return
  
