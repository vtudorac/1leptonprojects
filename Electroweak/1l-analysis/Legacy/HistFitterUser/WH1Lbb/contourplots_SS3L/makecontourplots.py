#!/bin/env python

import os,sys,ROOT

print 'Running makecontourplots'

if len(sys.argv)>1:
    #if sys.argv[1]!='-b': infiles = sys.argv[1:]
    outputname = sys.argv[1]
    if len(sys.argv)>2: infiles = sys.argv[2:]
else:
    print 'Please specify an input file(s)'
    sys.exit(1)
#print 'Using files',infiles

if len(infiles)>1:
    file_up = infiles[1]
else:
    file_up = None
   
if len(infiles)>2:
    file_down = infiles[2]
else:
    file_down = None   

print 'Calling over to macro'


execfile( 'contourmacros/limit_plot_SM.py' )

#################################################
## OBS bands depend on w/ or w.o up/down files ##
#################################################
#######################################################################################################################
## for exclusion contours : the last argument by default == -1.  
if 'combined' in infiles[0]:
  limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'Wh SS + 3L combined', 36.1, True, True, True ) ### OBS and EXP w/ band
elif 'SS' in infiles[0]:
  limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'Wh SS', 36.1, True, True, True ) ### OBS and EXP w/ band   
else:
  limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'Wh 3L', 36.1, True, True, True ) ### OBS and EXP w/ band  
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '4 lepton', 10.0, True, True, False ) ### OBS and EXP w.o b
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , 'combined #tau - SR', 36.5, False, False, True ) ###  EXP w/ band
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '4 lepton', 10.0, False, False, False ) ### EXP w.o band

#######################################################################################################################
## for discovery significance: make sure you have makelist for hypo_discovery and dodis = 3 or 5 here
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '4 lepton', 10.0, True, True, True, False, False, True,3) 
#limit_plot_SM_special( infiles[0] , file_up , file_down, outputname , '4 lepton', 10.0, True, True, True, False, False, True,5) 

#######################################################################################################################
# Arguments:
# drawFirstObs=False, allObs=False, firstOneSigma=False, upperlimit=False, printCLs=False,write_curves=True, dodis=-1
# dodis = -1 (default) or 3 or 5
#######################################################################################################################



