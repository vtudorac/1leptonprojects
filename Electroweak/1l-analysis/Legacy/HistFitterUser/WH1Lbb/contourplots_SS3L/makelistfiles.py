#!/bin/env python

import os,sys,ROOT,math

if len(sys.argv)>1:
    if sys.argv[1]!='-b': infile = sys.argv[1]
    elif len(sys.argv)>2: infile = sys.argv[2]
    else:
        print 'Please specify an input file'
        sys.exit(1)
print 'Using file',infile

ROOT.gSystem.Load('libSusyFitter.so')

print '### For 4-lepton analysis: Please have the namings including : LLE12k, LLEi33 ###'

## choose here if you want discovery or exclusion parameters ##
#dowhich = 'hypo_discovery'  ## for discovery p0, p0exp
dowhich = 'hypo'  ## for exclusion CLs...   

print dowhich
##############################################################################################################
#if 'LLE12k' in infile:
#    format     = dowhich + '_C1C1_%f_%f_LLE12k'
#    interpretation = 'NLSP:LSP'
#    cutStr = '1'
#    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
#elif 'LLEi33' in infile:
#    format     = dowhich + '_C1C1_%f_%f_LLEi33'
#    interpretation = 'NLSP:LSP'
#    cutStr = '1'
#    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
if 'LLE12k' in infile and 'GG' in infile:
    format     = dowhich + '_GG_%f_%f_LLE12k'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLEi33' in infile and 'GG' in infile:
    format     = dowhich + '_GG_%f_%f_LLEi33'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLE12k' in infile and 'VVz' in infile:
    format     = dowhich + '_VVz_%f_%f_LLE12k'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLEi33' in infile and 'VVz' in infile:
    format     = dowhich + '_VVz_%f_%f_LLEi33'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;  
elif 'LLE12k' in infile and 'VVh' in infile:
    format     = dowhich + '_VVh_%f_%f_LLE12k'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLEi33' in infile and 'VVh' in infile:
    format     = dowhich + '_VVh_%f_%f_LLEi33'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;      
elif 'LLE12k' in infile and 'LV' in infile:
    format     = dowhich + '_LV_%f_%f_LLE12k'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLEi33' in infile and 'LV' in infile:
    format     = dowhich + '_LV_%f_%f_LLEi33'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ; 
elif 'LLE12k' in infile and 'RH' in infile:
    format     = dowhich + '_RHSlep_%f_%f_LLE12k'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLEi33' in infile and 'RH' in infile:
    format     = dowhich + '_RHSlep_%f_%f_LLEi33'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;            
elif 'GGM' in infile:
    format     = dowhich + '_GGMHino_%f_%f'
    interpretation = 'LSP:NLSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLE12k' in infile and 'RH' in infile:
    format     = dowhich + '_RH_%f_%f_LLE12k'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'LLEi33' in infile and 'RH' in infile:
    format     = dowhich + '_RH_%f_%f_LLEi33'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'Wh' in infile and 'SS' in infile:
    format     = dowhich + '_C1N2_Wh_hall_%f_%f'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;
elif 'SS' in infile:
    format     = dowhich + '_C1N2_Wh_hall_%f_%f'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;              
elif 'Wh' in infile:
    format     = dowhich + '_%f_%f'
    interpretation = 'NLSP:LSP'
    cutStr = '1'
    outputfile = ROOT.CollectAndWriteHypoTestResults( infile, format, interpretation, cutStr ) ;    
    


