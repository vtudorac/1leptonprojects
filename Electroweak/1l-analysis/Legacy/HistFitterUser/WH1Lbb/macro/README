
>>>>> Structure <<<<<<

HistPlotter class : reading in the workspaces and does the core of the work.
HistStyle class   : Most of the style of the plots is contained in the class (to the best of my ability)


>>>>> Some notes before running your histfitter job <<<<<

   Its useful if you can maintain a naming scheme: "Region_variableName"
     - Only one "_"
     - VariableName = the variable that is plotted for that 'VR' in HistFitter
     - Region = any string wihout "_", as the region name is parsed with respect to "_". Useful to keep
       the 'Region' name uniform across the same selection only changing variableName when you want to make
       a new plot in HistFitter( e.g. VR5J_mt, VR5J_met, etc..)


   If you maintain this type of structure, suppose you want to make 3 plots in some arbitrary region called CR1,
   and your histogram names are 'mt', 'met', and 'njet', then after adding these regions,HistFitter will give you "regions", called
     - CR1_mt_mt, CR1_met_met, CR1_njet_njet.
   Where it is assumed you added to HistFitter three regions for each of the plots:
     - CR1_mt for the mt distribution, and so on (you get an extra _mt, because HistFitter will append also the histogram name to the region)

>>>>> Setup package for usage <<<<<

  Checkout the package:
    => svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/UBC/mgignac/HistPlotter/trunk/ HistPlotter

  1) Setup HistFitter in the usual way, such that we have $HISTFITTER env variable set
  2) Compile this package, using 'make'
  3) You are now ready to use the package!


>>>>> Typical command <<<<<

  ./output.o  [-name] [-dir] [-save] [-region] 

   where:
     - dir     : the 'results' directory created by HistFitter after your HistFitter jobs finishes
     - name    : a directory that contains the fit results. Should be found in [dir], this is left configurable because in principle you 
                 can have many fit results saved from HistFitter that would be found in [dir]
     - save    : path to a directory where you want the plots to be written
     - region  : the specify region you want to plot. If your unsure of the name of your region (or you don't want to dig through your HF config), run with
                all other field above set, plus '--printRegionsOnly', which will pring all regions found in the workspace. On a similar note, if you want to 
                plot absoutely everything in the work space, don't specify any region.

>>>>> Some additional commmands/suggestions <<<<<

   1) For multiplicity plots, sometimes you want to center the bin label (for example b-jet multicplity plot). For this, run with '--integerBins'
   2) The scales of both the central and ratio canavs can be adjusted on a plot-by-plot basis. Run with:
        - '-ratioYMin'
        - '-ratioYMax'
        - '-mainYMin'
        - '-mainYMax'
   3) If you need to isolated a particular plot, use the full region name: i.e. from the example above CR1_mt_mt. You can then adjust details like the scale, etc
   4) Adding signal, you should have another directory with workspaces in the [-dir], and run with [-sigName]. For example
          ==> In [-dir], you find [-name], you should also find a folder [-sigName]

   5) Adding a region name to the canvas, simply run with '-regionName myRegionName'. If you need to modify where this sits on the canvas, unfortunately for the 
      time being you need to go in a modify the ~L710 (if you can't find it, grep for m_regionName), and recompile. That being said I think the position should
      work out of the box for what we need at the moment

