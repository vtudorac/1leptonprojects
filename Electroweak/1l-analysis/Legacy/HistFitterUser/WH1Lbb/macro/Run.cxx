#include "HistPlotter.h"

int main(int argc, char** argv)
{

  TString name;
  TString sigName = "";
  TString sigName2 = "";
  TString dir;
  TString save;
  TString regionName = "";
  TString regionName2 = "";
  TString region = "";
  TString units = "GeV";
  double ratioYmin = -1.0;
  double ratioYmax = -1.0;
  double mainYmin = 0.005;
  double mainYmax = 5000;

  float min = 0;
  float max = 0;

  bool printRegionsOnly=false;
  bool integerBins=false;
  bool logY=true;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-name") == 0){
      name = argv[++i];
    }
    else if (strcmp(argv[i], "-sigName") == 0){
      sigName = argv[++i];
    }
    else if (strcmp(argv[i], "-sigName2") == 0){
      sigName2 = argv[++i];
    }
    else if (strcmp(argv[i], "-dir") == 0){
      dir = argv[++i];
    }
    else if (strcmp(argv[i], "-save") == 0){
      save = argv[++i];
    }
    else if (strcmp(argv[i], "-region") == 0){
      region = argv[++i];
    }
    else if (strcmp(argv[i], "-units") == 0){
      units = argv[++i];
    }
   else if (strcmp(argv[i], "-regionName") == 0){
      regionName = argv[++i];
    }
   else if (strcmp(argv[i], "-regionName2") == 0){
      regionName2 = argv[++i];
    }
    else if (strcmp(argv[i], "-ratioYMin") == 0){
      ratioYmin = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-ratioYMax") == 0){
      ratioYmax = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-mainYMin") == 0){
      mainYmin = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-mainYMax") == 0){
      mainYmax = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-minRange") == 0){
      min= atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-maxRange") == 0){
      max= atof(argv[++i]);
    }
    else if (strcmp(argv[i], "--printRegionsOnly") == 0){
      printRegionsOnly=true; 
    }
    else if (strcmp(argv[i], "--integerBins") == 0){
      integerBins=true;
    }
    else if (strcmp(argv[i], "--noLogY") == 0){
      logY=false;
    }



    else{
      cout << "Error: invalid argument!" << endl;
      exit(0);
    }
  }

  cout << "Name:   " << name << endl;
  cout << "Save:   " << save << endl;


  HistPlotter* plotter = new HistPlotter();

  plotter->printRegionsOnly(printRegionsOnly);
  plotter->integerBins(integerBins);
  plotter->setLogY(logY);
  plotter->setRegionName(regionName);
  plotter->setRegionName2(regionName2);
  //plotter->setHistStatus("Preliminary");
  //plotter->setHistStatus("Internal");
//  plotter->setHistStatus("");
  //plotter->setRatioYMin(0.5);//ratioYmin);//VR
  //plotter->setRatioYMax(1.6);//ratioYmax);//VR
  plotter->setRatioYMin(ratioYmin);//SR use default 0-2.3 
  plotter->setRatioYMax(ratioYmax);//SR
  plotter->setMainYMin(mainYmin);
  plotter->setMainYMax(mainYmax);
  plotter->setErrorRange(min,max);


  // Overlay the signal

  if(region.Contains("SRLM")){
    plotter->addProcess("VH","Others",kGray,2);
    plotter->addProcess("ttH","Others",kGray,2);
    plotter->addProcess("triboson_Sherpa221","Others",kGray,2);
    plotter->addProcess("zjets_Sherpa221","Others",kGray,2);
    plotter->addProcess("ttv_NLO","t#bar{t}V",kOrange,2);
    plotter->addProcess("diboson_Sherpa221","Diboson",kViolet-8,2);
    plotter->addProcess("wjets_Sherpa221","W+jets",kGreen+2,2);
    plotter->addProcess("singletop","Single top",kRed+2,1);
    plotter->addProcess("ttbar_bin1","t#bar{t}",kAzure+1,1);
    plotter->addProcess("ttbar_bin2","t#bar{t}",kAzure+1,1);
    plotter->addProcess("ttbar_bin3","t#bar{t}",kAzure+1,1);

    plotter->addSigProcess("C1N2_Wh_hall_225p0_0p0","m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2},#tilde{#chi}^{0}_{1})=(225,0) GeV",kRed); // 
    plotter->addSigProcess("C1N2_Wh_hall_250p0_50p0","m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2},#tilde{#chi}^{0}_{1})=(250,50) GeV",kBlue); // 
  
    plotter->setXaxisNdivisions(4,6,0);

  
  }

   if(region.Contains("SRMM")){

    plotter->addProcess("VH","Others",kGray,2);
    plotter->addProcess("ttH","Others",kGray,2);
    plotter->addProcess("triboson_Sherpa221","Others",kGray,2);
    plotter->addProcess("zjets_Sherpa221","Others",kGray,2);
    plotter->addProcess("ttv_NLO","t#bar{t}V",kOrange,2);
    plotter->addProcess("diboson_Sherpa221","Diboson",kViolet-8,2);
    plotter->addProcess("wjets_Sherpa221","W+jets",kGreen+2,2);
    plotter->addProcess("singletop","Single top",kRed+2,1);
    plotter->addProcess("ttbar_bin1","t#bar{t}",kAzure+1,1);
    plotter->addProcess("ttbar_bin2","t#bar{t}",kAzure+1,1);
    plotter->addProcess("ttbar_bin3","t#bar{t}",kAzure+1,1);

    plotter->addSigProcess("C1N2_Wh_hall_375p0_150p0","m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2},#tilde{#chi}^{0}_{1})=(375, 150) GeV",kRed); // 
    plotter->addSigProcess("C1N2_Wh_hall_500_50","m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2},#tilde{#chi}^{0}_{1})=(500, 50) GeV",kBlue); // 
  
   plotter->setXaxisNdivisions(4,6,0);
  } 
  
    if(region.Contains("SRHM")){

    plotter->addProcess("VH","Others",kGray,2);
    plotter->addProcess("ttH","Others",kGray,2);
    plotter->addProcess("triboson_Sherpa221","Others",kGray,2);
    plotter->addProcess("zjets_Sherpa221","Others",kGray,2);
    plotter->addProcess("ttv_NLO","t#bar{t}V",kOrange,2);
    plotter->addProcess("diboson_Sherpa221","Diboson",kViolet-8,2);
    plotter->addProcess("wjets_Sherpa221","W+jets",kGreen+2,2);
    plotter->addProcess("singletop","Single top",kRed+2,1);
    plotter->addProcess("ttbar_bin1","t#bar{t}",kAzure+1,1);
    plotter->addProcess("ttbar_bin2","t#bar{t}",kAzure+1,1);
    plotter->addProcess("ttbar_bin3","t#bar{t}",kAzure+1,1);

    plotter->addSigProcess("C1N2_Wh_hall_550p0_75p0","m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2},#tilde{#chi}^{0}_{1})=(550,75) GeV",kRed); // 
    plotter->addSigProcess("C1N2_Wh_hall_625p0_0p0","m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2},#tilde{#chi}^{0}_{1})=(625,0) GeV",kBlue); // 
    
    plotter->setXaxisNdivisions(4,6,0);
  }  
  
  
  plotter->addXaxisLabelToMap("mct2","m_{CT} [GeV]");
  plotter->addXaxisLabelToMap("mt","m_{T} [GeV]");
  plotter->addXaxisLabelToMap("met","E_{T}^{miss} [GeV]");
  plotter->addXaxisLabelToMap("mbb","m_{bb} [GeV]");

  plotter->addRegionToPlot(region);
  plotter->setUnits(units);


  plotter->addPlots(dir,name,sigName,sigName2,save,"ALL","Validation_combined_BasicMeasurement_model_afterFit.root","w");
  plotter->Process();

  return 0;
}
