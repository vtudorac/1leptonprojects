#!/usr/bin/env python

import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

fakeCount={
	'SRjet1':([4.31],'cuts',0.5),
	'SRjet23':([2.85],'cuts',0.5),
	'VRjet1':([8.02],'cuts',0.5),
	'VRjet23':([20.08],'cuts',0.5),
}
fakeStatErr={
	'SRjet1':[0.99/4.31],
	'SRjet23':[0.83/2.85],
	'VRjet1':[1.39/8.02],
	'VRjet23':[2.02/20.08],
}
fakeSystCorr={
	'SRjet1':[1.19/4.31],
	'SRjet23':[0.76/2.85],
	'VRjet1':[2.58/8.02],
	'VRjet23':[4.45/20.08],
}
fakeSystUncorr={
	'SRjet1':[2.68/4.31],
	'SRjet23':[1.68/2.85],
	'VRjet1':[5.20/8.02],
	'VRjet23':[12.82/20.08],
}

def addFakeCount(FakeSample):
    for srname,config in fakeCount.iteritems():
        FakeSample.buildHisto(config[0],srname,config[1],config[2])
        print "adding FakeCount",config[0],srname,config[1],config[2]

def genFakeSyst(sysfake_corr,sysfake_uncorr,statfake):
    for name in fakeCount.keys():
        sysfake_corr[name] = Systematic("SysFakeCorr","1.",1.+fakeSystCorr[name][0],1.-fakeSystCorr[name][0],"user","userHistoSys")
        sysfake_uncorr[name] = Systematic("SysFakeUncor_"+name,"1.",1.+fakeSystUncorr[name][0],1.-fakeSystUncorr[name][0],"user","userHistoSys")
        statfake[name] = Systematic("StatFake_"+name,"1.",1.+fakeStatErr[name][0],1.-fakeStatErr[name][0],"user","userHistoSys")

