################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import Measurement,Sample
from systematic import Systematic
from math import sqrt

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

##########################

# Set observed and expected number of events in counting experiment
ndata     =  999. 	# Number of events observed in data
nbkg      =  999.	# Number of predicted bkg events
nsig      =  1.  	# Number of predicted signal events
nbkgErr   =  0. #XXX	# (Absolute) Statistical error on bkg estimate
nsigErr    =  0. #XXX          # (Absolute) Statistical error on signal estimate
lumiError = 0.032 	# Relative luminosity uncertainty
SR = "XXX"

try:
    pickedSRs
except NameError:
    pickedSRs = None
    print "\n Choose a SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
    
if pickedSRs != None and len(pickedSRs) > 1:
    print "\n Choose a single(!) SR with -r <SRName> or I refuse to work"
    sys.exit(1) 
elif pickedSRs != None and len(pickedSRs) == 1:
    SR = pickedSRs[0]
    print "\n SR chosen from command line: ", SR

if SR == "VRjet1":
    ndata = 17.
    nbkg = 16.84
    nbkgErr = 6.06/nbkg
elif SR == "VRjet23":
    ndata = 54.
    nbkg = 55.62
    nbkgErr = 14.41/nbkg
elif SR == "SRjet1":
    ndata = 2.
    nbkg = 6.74
    nbkgErr = 2.17/nbkg  
elif SR == "SRjet23":
    ndata = 8.
    nbkg = 5.33
    nbkgErr = 1.59/nbkg
   
         
    
print 1. + nbkgErr    

# correlated systematic between background and signal (1 +- relative uncertainties)
#corb = Systematic("cor",configMgr.weights, [1. + nbkgErr],[1. - nbkgErr], "user","userHistoSys")  # AK: 0.97/3.63=0.26
corb = Systematic("cor",configMgr.weights, (1. + nbkgErr),(1. - nbkgErr), "user","userOverallSys")  # AK: 0.97/3.63=0.26

##########################

# Setting the parameters of the hypothesis test
#configMgr.nTOYs=5000
configMgr.calculatorType=0 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
configMgr.nTOYs=10000
configMgr.keepSignalRegionType = True
##########################

# Give the analysis a name MyDiscoveryAnalysis_Moriond_SoftLepton_SR1L3j.py
configMgr.analysisName = "MyDiscoveryAnalysis_Moriond2017SS_"+SR
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

# Define cuts
configMgr.cutsDict[SR] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto([nbkg],SR,"cuts")
#bkgSample.buildStatErrors([nbkgErr],"SR5JEl","cuts")
bkgSample.addSystematic(corb)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_"+SR,1.,0.,30.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto([nsig],SR,"cuts")
#sigSample.buildStatErrors([nsigErr],"SR5JEl","cuts")

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([ndata],SR,"cuts")

# Define top-level
ana = configMgr.addTopLevelXML("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="BasicMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_"+SR)

#meas.addParamSetting("Lumi","const",1.0)

# Add the channel
chan = ana.addChannel("cuts",[SR],1,0.5,1.5)
ana.setSignalChannels([chan])

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
