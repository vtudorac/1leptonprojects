################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch

#import Winter1617.SignalAccUncertainties

from logger import Logger
log = Logger('OneLeptonbb')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        #chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onDOL= False
if onDOL:    onLxplus=False  ## previous status does not detect the batch system in lxplus

debug = False #!!!

if onDOL:  debug = True  ## for test

if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]


SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
SystList.append("JVT")      # JVT scale factors
SystList.append("pileup")   # pileup (common)
SystList.append("BTag")     # b-tagging uncertainties
#SystList.append("Dibosons") # scale variation for renormalization, factorization, resummation and generator comparison
#SystList.append("Wjets")    # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
#SystList.append("Ttbar")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
#SystList.append("SingleTop")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
#SystList.append("ttbarPDF") # 
#SystList.append("wjetsPDF") # 
#SystList.append("Zjets")    # scale variation for renormalization, factorization, resummation,matching


if debug:
    SystList=[]
    
#for creating the backup chache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False
if onDOL: doHistoBuilding = False

# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=True

useNJetNormFac=False
# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["HM","MM","LM"] #this is the default - modify from command line

#activate inclusive signal regions for model-dependent fit and deactivate exclusive SRs: incl=True
#note that for inclusive SRs you cannot run more than one at a time (they are not orthogonal), still to get similar normalization factors as for the exclusion SRs, the same exclusive SRs are run 
#for the validation regions, the inclusive SRs are automatically enabled at the same time as the exclusive SRs are run
incl=False

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r LM,MM,HM  option)"

CRregions=CRregions+["general"]

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["LM"] = False
ValidRegList["MM"] = False
ValidRegList["HM"] = False
ValidRegList["general"] = False


# for tables (turn doTableInputs to True)
doTableInputs = False #This effectively means no validation plots but only validation tables (but is 100x faster)
if onDOL:  doTableInputs = False
for cr in CRregions:
    if "LM" in cr and doTableInputs:
        ValidRegList["LM"] = True
    if "MM" in cr and doTableInputs:
        ValidRegList["MM"] = True	
    if "HM" in cr and doTableInputs:
        ValidRegList["HM"] = True
    if "general" in cr and doTableInputs:
        ValidRegList["general"] = True        

#N-1 plots in SRs        
VRplots=True

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=True


# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    
    #sigSamples=["GG_oneStep_945_785_625"]
    #sigSamples=["GG_oneStep_637_626_615"]    
   
    #sigSamples=["GG_oneStep_1105_865_625"]
    sigSamples=["C1N2_Wh_hall_450p0_0p0"]
    
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
'''    
if myFitType==FitType.Exclusion:
    if onDOL:
        analysissuffix += '_'+ sigSamples[0]
    if 'GG_oneStep' in sigSamples[0]  and not onDOL:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]  and not onDOL:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]
'''	
	

mylumi= 36.06596#36.46161#14.78446 #30 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  

mysamples = ["triboson", "VH", "Zjets","ttv","ttH","singletop","diboson","Wjets","ttbar","data"]
doOnlySignal=False

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
	
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "OneLeptonbb"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
useBackupCache = True
if (onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "OneLeptonbb"+analysissuffix_BackupCache+analysisextension
    if not onLxplus: histBackupCacheFileName = "data/backup.root"
    #if not onLxplus: histBackupCacheFileName = "data/OneLepton_6J_GG_oneStep_samples_Wjets.root"
    else: histBackupCacheFileName = "data/OneLepton_2J_4Jhighx_4Jlowx_6J_GG_oneStep.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

if onDOL:
    configMgr.fixSigXSec=False
else:
    configMgr.fixSigXSec=True

useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
#configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)
doBlindSRinBGfit  = False # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = []

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #


inputDir=""
inputDirData=""
inputDirSig=""

if not onLxplus:
    inputDir="/project/etp2/SUSYWh/NEW/Ntuples_HF/"
    inputDirSig="/project/etp2/SUSYWh/NEW/Ntuples_HF/"
    inputDirData="/project/etp2/SUSYWh/NEW/Ntuples_HF/"
    
#background files, separately for electron and muon channel:

#bgdFiles_e = [inputDir+"allTrees_T_06_10_bkg.root"]
#bgdFiles_m = [inputDir+"allTrees_T_06_10_bkg.root"]
#bgdFiles_em = [inputDir+"allTrees_T_06_10_bkg.root"]
files_em={}
files_em['Wjets']=[inputDir+"wjets_Sherpa221_merged.root"]
files_em['Zjets']=[inputDir+"zjets_Sherpa221_merged.root"]
files_em['diboson']=[inputDir+"diboson_Sherpa221_merged.root"]
files_em['ttbar']=[inputDir+"ttbar_merged.root"]
files_em['ttv']=[inputDir+"ttv_NLO_merged_processed.root"]
files_em['ttH']=[inputDir+"ttH_merged.root"]
files_em['singletop']=[inputDir+"singletop_merged.root"]
files_em['triboson']=[inputDir+"triboson_Sherpa221_merged.root"]
files_em['VH']=[inputDir+"VH_merged_processed_INC.root"]


#signal files
if myFitType==FitType.Exclusion or doOnlySignal:
    sigFiles_e={}
    sigFiles_m={}
    sigFiles_em={}
    for sigpoint in sigSamples:
        sigFiles_e[sigpoint]=[inputDirSig+"SignalPoints1603.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"SignalPoints1603.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"SignalPoints1603.root"]
        
#data files
#dataFiles = [inputDirData+"allTrees_T_06_03_dataOnly.root"] # 949.592 /pb
#dataFiles = [inputDirData+"allDS2_T_06_03.root"]    # DS2: 13277.66 /pb (remember to check lumiErr: should be 2.9%)
#dataFiles = [inputDirData+"allDS2v01_T_06_03.root"] # DS2.1: 14784.46 /pb (remember to check lumiErr: should be 3.0%)
#dataFiles = [inputDirData+"allTrees_T_06_05_data.root"] # DS3: 36461.61 /pb (remember to check lumiErr: should be 4.1%)
dataFiles = [inputDirData+"data_merged_processed_tree_NoSys.root"] # (lumiErr=0.032)

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&  nLep_base==1&&nLep_signal==1 && trigMatch_metTrig"
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>27.) "
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>27.)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>27.) || (AnalysisType==2 && lep1Pt>27.))"
Hack = "&& ( fabs(jet1Pt-bjet1Pt)<0.001 && fabs(jet2Pt-bjet2Pt)<0.001)"   #Hack for 2 leading b-jets used in L 380-382 (the b1.1 ntuple version)

# ------- SRs --------------------------------------------------------------------------- #
SRSelection        = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160."
configMgr.cutsDict["SRLM"]=  SRSelection + "&& mt>=100. && mt<140. && met>200." + CommonSelection
configMgr.cutsDict["SRMM"]=  SRSelection + "&& mt>=140. && mt<200. && met>200." + CommonSelection
configMgr.cutsDict["SRHM"]=  SRSelection + "&& mt>=200. && met>200." + CommonSelection

configMgr.cutsDict["SRLMincl"]=  SRSelection + "&& mt>=100. && met>200." + CommonSelection
configMgr.cutsDict["SRMMincl"]=  SRSelection + "&& mt>=140. && met>200." + CommonSelection
configMgr.cutsDict["SRHMincl"]=  SRSelection + "&& mt>=200. && met>200." + CommonSelection

configMgr.cutsDict["SRLMnomt"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160. && met>200." + CommonSelection
configMgr.cutsDict["SRLMnomet"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160. && mt>=100. && mt<140." + CommonSelection
configMgr.cutsDict["SRLMnomct2"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135.&& mt>=100. && mt<140. && met>200." + CommonSelection
configMgr.cutsDict["SRLMnombb"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mct2>160. && mt>=100. && mt<140. && met>200." + CommonSelection

configMgr.cutsDict["SRMMnomt"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160. && met>200." + CommonSelection
configMgr.cutsDict["SRMMnomet"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160. && mt>=140. && mt<200." + CommonSelection
configMgr.cutsDict["SRMMnomct2"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mt>=140. && mt<200. && met>200." + CommonSelection
configMgr.cutsDict["SRMMnombb"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mct2>160. && mt>=140. && mt<200. && met>200." + CommonSelection

configMgr.cutsDict["SRHMnomt"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160. && met>200." + CommonSelection
configMgr.cutsDict["SRHMnomet"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mct2>160. && mt>=200." + CommonSelection
configMgr.cutsDict["SRHMnomct2"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>105. && mbb<135. && mt>=200. && met>200." + CommonSelection
configMgr.cutsDict["SRHMnombb"]= "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mct2>160. && mt>=200. && met>200." + CommonSelection

TRSelection        = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && ((mbb>=50 && mbb<=105.) || mbb>=135.) && mct2<=160."
configMgr.cutsDict["TRLM"]=  TRSelection + "&& mt>=100 && mt<140 && met>200" + CommonSelection
configMgr.cutsDict["TRMM"]=  TRSelection + "&& mt>=140 && mt<200 && met>200" + CommonSelection
configMgr.cutsDict["TRHM"]=  TRSelection + "&& mt>=200 && met>200" + CommonSelection
#configMgr.cutsDict["TR"]=  TRSelection + "&& mt>=100. && met>190." + CommonSelection

configMgr.cutsDict["WR"] = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>=50. && mbb<=80. && mct2>160. && mt>40. & mt<100. && met>200." + CommonSelection
configMgr.cutsDict["STCR"] = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && mbb>195. && mct2>160. && mt>=100. && met>200." + CommonSelection

VRSelection        = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && ((mbb>=50 && mbb<=95.) || (mbb>=145. && mbb < 195.)) && mct2>160."

configMgr.cutsDict["VRtt1off"] =   VRSelection + "&& mt>=100. && mt<140. && met>180." + CommonSelection
configMgr.cutsDict["VRtt2off"]=  VRSelection + "&& mt>=140. && mt<200. && met>180." + CommonSelection
configMgr.cutsDict["VRtt3off"]=  VRSelection + "&& mt>=200. && met>180." + CommonSelection

VRSelection2        = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && (mbb>105. && mbb<135.) && mct2<=160."

configMgr.cutsDict["VRtt1on"] =   VRSelection2 + "&& mt>=100. && mt<140. && met>200." + CommonSelection
configMgr.cutsDict["VRtt2on"]=  VRSelection2 + "&& mt>=140. && mt<200. && met>200." + CommonSelection
configMgr.cutsDict["VRtt3on"]=  VRSelection2 + "&& mt>=200. && met>200." + CommonSelection

d=configMgr.cutsDict
#second part: splitting into electron and muon channel for validation purposes	d=configMgr.cutsDict
defined_regions = []
if 'HM' in CRregions: 
    defined_regions+=["SRHM","SRHMincl","SRHMnomt","SRHMnomet","SRHMnomct2","SRHMnombb"]

if 'LM' in CRregions: 
    defined_regions+=["SRLM","SRLMincl","SRLMnomt","SRLMnomet","SRLMnomct2","SRLMnombb"]
    
if 'MM' in CRregions: 
    defined_regions+=["SRMM","SRMMincl","SRMMnomt","SRMMnomet","SRMMnomct2","SRMMnombb"] 
    
    
defined_regions+=["WR","STCR","TRLM","TRMM","TRHM","VRtt1off","VRtt2off","VRtt3off","VRtt1on","VRtt2on","VRtt3on"]   


for pre_region in defined_regions:
            configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection#+Hack #hack removed following discussions in FAR, now allowing again the 2 b-jets to be any jets in the leading 3 jets
            configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection#+Hack
            configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection#+Hack  

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","jvtWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights

#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")

muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")

muonHighTTVAWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1up")
muonLowTTVAWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1down")
muonHighTTVAWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1up")
muonLowTTVAWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1down")

#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#### electron related uncertainties acting on weights ####
#ID 
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Rec
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Iso
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")


# JVT weight systematics
#JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyUp")
#JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyDown")

JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1up")
JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1down")


# pileup weight systematics
pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

    eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
    eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}

for region in ['LM','MM','HM','general']:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 35% syst and an overallSys
   
     
    basicChanSyst[region].append(Systematic("trigger",configMgr.weights,1.04,0.96,"user","userOverallSys"))
    #basicChanSyst[region].append(Systematic("diboson",configMgr.weights,1.06,0.94,"user","userOverallSys"))
    #basicChanSyst[region].append(Systematic("ttV",configMgr.weights,1.125,0.875,"user","userOverallSys"))
    
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_NoSys","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_NoSys","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_NoSys","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_NoSys","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_NoSys","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_NoSys","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6","_NoSys","_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_7","_NoSys","_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_8restTerm","_NoSys","_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree","overallNormHistoSys"))
	     
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_NoSys","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_NoSys","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_NoSys","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_NoSys","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_NoSys","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_NoSys","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_NoSys","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_NoSys","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_NoSys","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_NoSys","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys"))

    	        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID","_NoSys","_MUON_ID__1up","_MUON_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS","_NoSys","_MUON_MS__1up","_MUON_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SCALE","_NoSys","_MUON_SCALE__1up","_MUON_SCALE__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_stat",configMgr.weights,muonHighTTVAWeights_stat,muonLowTTVAWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_sys",configMgr.weights,muonHighTTVAWeights_sys,muonLowTTVAWeights_sys,"weight","overallNormHistoSys"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Eff",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys")) 

    if "JVT" in SystList :
        #basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))
    if "BTag" in SystList:
        bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")
        eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,eTagFromCHighWeights,eTagFromCLowWeights,"weight","overallNormHistoSys")

        
    #signal uncertainties                                                                                       
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
    
    #Generator Systematics
    generatorSyst = []  
    
    #ttbar uncertainties
    import theoryUncertainties_1Lbb_ttbar
    theoryUncertainties_1Lbb_ttbar.TheorUnc(generatorSyst)
    
    #single top uncertainties
    import theoryUncertainties_1Lbb_singletop
    theoryUncertainties_1Lbb_singletop.TheorUnc(generatorSyst)  
   
    #Wjets uncertainties
    import theoryUncertainties_1Lbb_Wjets
    theoryUncertainties_1Lbb_Wjets.TheorUnc(generatorSyst) 
    
    #Zjets uncertainties
    import theoryUncertainties_1Lbb_Zjets
    theoryUncertainties_1Lbb_Zjets.TheorUnc(generatorSyst)            

# ********************************************************************* #
#                              Background samples

# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets_Sherpa221"
WSample = Sample(WSampleName,kAzure-4)
WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.setFileList(files_em['Wjets'])

#
#TTbarSample_bin1 = Sample("ttbar_bin1",kGreen-9)
#TTbarSample_bin1.setStatConfig(useStat)
#TTbarSample_bin1.setNormFactor("mu_Top_bin1",1.,0.,5.) #correlate first and second ttbar bins
#TTbarSample_bin1.setPrefixTreeName("ttbar")
#TTbarSample_bin1.setFileList(files_em['ttbar'])
#TTbarSample_bin1.addSampleSpecificWeight("(mt < 100.)")

TTbarSample_bin2 = Sample("ttbar_bin1",kGreen-9)
TTbarSample_bin2.setStatConfig(useStat)
TTbarSample_bin2.setNormFactor("mu_Top_bin1",1.,0.,5.)
TTbarSample_bin2.setPrefixTreeName("ttbar")
TTbarSample_bin2.setFileList(files_em['ttbar'])
TTbarSample_bin2.addSampleSpecificWeight("(mt < 140.)")

TTbarSample_bin3 = Sample("ttbar_bin2",kGreen-9)
TTbarSample_bin3.setStatConfig(useStat)
TTbarSample_bin3.setNormFactor("mu_Top_bin2",1.,0.,5.)
TTbarSample_bin3.setPrefixTreeName("ttbar")
TTbarSample_bin3.setFileList(files_em['ttbar'])
TTbarSample_bin3.addSampleSpecificWeight("(mt >= 140. && mt < 200.)")

TTbarSample_bin4 = Sample("ttbar_bin3",kGreen-9)
TTbarSample_bin4.setStatConfig(useStat)
TTbarSample_bin4.setNormFactor("mu_Top_bin3",1.,0.,5.)
TTbarSample_bin4.setPrefixTreeName("ttbar")
TTbarSample_bin4.setFileList(files_em['ttbar'])
TTbarSample_bin4.addSampleSpecificWeight("(mt >= 200.)")

#
DibosonsSampleName = "diboson_Sherpa221"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
DibosonsSample.setFileList(files_em['diboson'])
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
SingleTopSample.setNormFactor("mu_ST",1.,0.,5.)
SingleTopSample.setStatConfig(useStat)
#SingleTopSample.setNormByTheory()
SingleTopSample.setFileList(files_em['singletop'])
#
ZSampleName = "zjets_Sherpa221"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
ZSample.setFileList(files_em['Zjets'])
#
ttbarVSampleName = "ttv_NLO"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
ttbarVSample.setFileList(files_em['ttv'])
#
ttbarHSampleName = "ttH"
ttbarHSample = Sample(ttbarHSampleName,kGreen-8)
ttbarHSample.setStatConfig(useStat)
ttbarHSample.setNormByTheory()
ttbarHSample.setFileList(files_em['ttH'])
#
TribosonsSampleName = "triboson_Sherpa221"
TribosonsSample = Sample(TribosonsSampleName,kOrange-8)
TribosonsSample.setStatConfig(useStat)
TribosonsSample.setNormByTheory()
TribosonsSample.setFileList(files_em['triboson'])
#
VHSampleName = "VH"
VHSample = Sample(VHSampleName,kGreen-8)
VHSample.setStatConfig(useStat)
VHSample.setNormByTheory()
VHSample.setFileList(files_em['VH'])
#

#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()

if doOnlySignal: 
    sigSample = Sample(sigSamples[0],kPink)       
    sigSample.setStatConfig(useStat)
    sigSample.setNormByTheory()
    new_samples = ["C1N2_Wh_hall_275_100","C1N2_Wh_hall_375_100","C1N2_Wh_hall_400_100","C1N2_Wh_hall_425_100","C1N2_Wh_hall_450_100","C1N2_Wh_hall_475_100","C1N2_Wh_hall_425_75","C1N2_Wh_hall_450_75","C1N2_Wh_hall_475_75","C1N2_Wh_hall_500_75","C1N2_Wh_hall_475_50","C1N2_Wh_hall_500_50","C1N2_Wh_hall_525_50","C1N2_Wh_hall_500_25","C1N2_Wh_hall_525_25","C1N2_Wh_hall_550_25","C1N2_Wh_hall_575_25","C1N2_Wh_hall_525_0","C1N2_Wh_hall_550_0","C1N2_Wh_hall_575_0","C1N2_Wh_hall_600_0","C1N2_Wh_hall_275p0_145p0","C1N2_Wh_hall_275p0_125p0","C1N2_Wh_hall_300p0_125p0","C1N2_Wh_hall_325p0_125p0","C1N2_Wh_hall_350p0_125p0","C1N2_Wh_hall_375p0_125p0","C1N2_Wh_hall_400p0_125p0","C1N2_Wh_hall_425p0_125p0","C1N2_Wh_hall_450p0_125p0","C1N2_Wh_hall_300p0_170p0","C1N2_Wh_hall_300p0_150p0","C1N2_Wh_hall_325p0_150p0",
"C1N2_Wh_hall_350p0_150p0","C1N2_Wh_hall_375p0_150p0","C1N2_Wh_hall_400p0_150p0","C1N2_Wh_hall_425p0_150p0","C1N2_Wh_hall_325p0_190p0","C1N2_Wh_hall_325p0_175p0","C1N2_Wh_hall_400p0_175p0","C1N2_Wh_hall_425p0_175p0","C1N2_Wh_hall_450p0_175p0","C1N2_Wh_hall_350p0_200p0","C1N2_Wh_hall_375p0_200p0","C1N2_Wh_hall_400p0_200p0","C1N2_Wh_hall_425p0_200p0","C1N2_Wh_hall_450p0_200p0","C1N2_Wh_hall_500p0_100p0","C1N2_Wh_hall_500p0_125p0","C1N2_Wh_hall_500p0_150p0","C1N2_Wh_hall_475p0_150p0","C1N2_Wh_hall_475p0_125p0","C1N2_Wh_hall_525p0_75p0","C1N2_Wh_hall_525p0_100p0","C1N2_Wh_hall_525p0_125p0","C1N2_Wh_hall_550p0_50p0","C1N2_Wh_hall_550p0_75p0","C1N2_Wh_hall_550p0_125p0","C1N2_Wh_hall_575p0_50p0","C1N2_Wh_hall_575p0_75p0","C1N2_Wh_hall_575p0_100p0","C1N2_Wh_hall_575p0_125p0","C1N2_Wh_hall_600p0_15p0","C1N2_Wh_hall_600p0_50p0","C1N2_Wh_hall_600p0_75p0","C1N2_Wh_hall_600p0_100p0","C1N2_Wh_hall_600p0_125p0","C1N2_Wh_hall_625p0_0p0","C1N2_Wh_hall_625p0_25p0","C1N2_Wh_hall_625p0_50p0","C1N2_Wh_hall_625p0_100p0","C1N2_Wh_hall_650p0_0p0","C1N2_Wh_hall_650p0_25p0","C1N2_Wh_hall_650p0_50p0","C1N2_Wh_hall_650p0_75p0","C1N2_Wh_hall_650p0_100p0","C1N2_Wh_hall_675p0_25p0","C1N2_Wh_hall_675p0_50p0","C1N2_Wh_hall_675p0_75p0","C1N2_Wh_hall_675p0_100p0","C1N2_Wh_hall_700p0_0p0","C1N2_Wh_hall_700p0_25p0","C1N2_Wh_hall_700p0_50p0","C1N2_Wh_hall_750p0_0p0","C1N2_Wh_hall_750p0_25p0","C1N2_Wh_hall_750p0_50p0","C1N2_Wh_hall_800p0_0p0","C1N2_Wh_hall_800p0_25p0","C1N2_Wh_hall_800p0_50p0","C1N2_Wh_hall_550p0_100p0","C1N2_Wh_hall_450p0_150p0","C1N2_Wh_hall_625p0_75p0","C1N2_Wh_hall_675p0_0p0","C1N2_Wh_hall_375p0_175p0","C1N2_Wh_hall_350p0_175p0","C1N2_Wh_hall_375p0_225p0",
"C1N2_Wh_hall_400p0_225p0","C1N2_Wh_hall_400p0_250p0","C1N2_Wh_hall_425p0_225p0","C1N2_Wh_hall_425p0_250p0","C1N2_Wh_hall_450p0_225p0","C1N2_Wh_hall_450p0_250p0","C1N2_Wh_hall_475p0_175p0","C1N2_Wh_hall_475p0_200p0","C1N2_Wh_hall_475p0_225p0","C1N2_Wh_hall_475p0_250p0","C1N2_Wh_hall_500p0_175p0","C1N2_Wh_hall_500p0_200p0","C1N2_Wh_hall_500p0_225p0","C1N2_Wh_hall_500p0_250p0","C1N2_Wh_hall_525p0_150p0","C1N2_Wh_hall_525p0_175p0","C1N2_Wh_hall_525p0_200p0","C1N2_Wh_hall_525p0_225p0","C1N2_Wh_hall_525p0_250p0","C1N2_Wh_hall_550p0_150p0","C1N2_Wh_hall_550p0_175p0","C1N2_Wh_hall_550p0_200p0","C1N2_Wh_hall_550p0_225p0",
"C1N2_Wh_hall_550p0_250p0","C1N2_Wh_hall_575p0_150p0","C1N2_Wh_hall_575p0_175p0","C1N2_Wh_hall_575p0_200p0","C1N2_Wh_hall_575p0_225p0","C1N2_Wh_hall_575p0_250p0","C1N2_Wh_hall_600p0_150p0","C1N2_Wh_hall_600p0_175p0","C1N2_Wh_hall_600p0_200p0","C1N2_Wh_hall_600p0_225p0","C1N2_Wh_hall_600p0_250p0","C1N2_Wh_hall_625p0_125p0","C1N2_Wh_hall_625p0_150p0","C1N2_Wh_hall_625p0_175p0","C1N2_Wh_hall_625p0_200p0","C1N2_Wh_hall_625p0_225p0","C1N2_Wh_hall_625p0_250p0","C1N2_Wh_hall_650p0_125p0","C1N2_Wh_hall_650p0_150p0","C1N2_Wh_hall_650p0_175p0","C1N2_Wh_hall_650p0_200p0","C1N2_Wh_hall_650p0_225p0","C1N2_Wh_hall_650p0_250p0","C1N2_Wh_hall_675p0_125p0","C1N2_Wh_hall_675p0_150p0","C1N2_Wh_hall_675p0_175p0","C1N2_Wh_hall_675p0_200p0","C1N2_Wh_hall_675p0_225p0","C1N2_Wh_hall_675p0_250p0","C1N2_Wh_hall_700p0_100p0","C1N2_Wh_hall_700p0_125p0","C1N2_Wh_hall_700p0_150p0","C1N2_Wh_hall_700p0_175p0","C1N2_Wh_hall_700p0_200p0","C1N2_Wh_hall_700p0_225p0","C1N2_Wh_hall_700p0_250p0","C1N2_Wh_hall_750p0_100p0","C1N2_Wh_hall_750p0_125p0","C1N2_Wh_hall_750p0_150p0","C1N2_Wh_hall_750p0_175p0","C1N2_Wh_hall_750p0_200p0","C1N2_Wh_hall_750p0_225p0","C1N2_Wh_hall_750p0_250p0","C1N2_Wh_hall_800p0_100p0","C1N2_Wh_hall_800p0_125p0","C1N2_Wh_hall_800p0_150p0","C1N2_Wh_hall_800p0_175p0","C1N2_Wh_hall_800p0_200p0","C1N2_Wh_hall_800p0_225p0","C1N2_Wh_hall_800p0_250p0"] 
    old_sample = True
    if sigSamples[0] in new_samples:
        old_sample=False 
    if old_sample:
        sigSample.addSampleSpecificWeight("0.58/0.82")
    else: 
        sigSample.addSampleSpecificWeight("0.58")   
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)
    sigSample.setFileList(sigFiles_em[sigSamples[0]])


# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")

#creating now list of samples
samplelist=[]
if 'triboson' in mysamples: samplelist.append(TribosonsSample)
if 'VH' in mysamples: samplelist.append(VHSample)
if 'Zjets' in mysamples: samplelist.append(ZSample)
if 'ttH' in mysamples: samplelist.append(ttbarHSample)
if 'ttv' in mysamples: samplelist.append(ttbarVSample)
if 'singletop' in mysamples: samplelist.append(SingleTopSample)
if 'diboson' in mysamples: samplelist.append(DibosonsSample)
if 'Wjets' in mysamples: samplelist.append(WSample)
if 'ttbar' in mysamples: 
    #samplelist.append(TTbarSample_bin1)
    samplelist.append(TTbarSample_bin2)
    samplelist.append(TTbarSample_bin3)
    samplelist.append(TTbarSample_bin4)
if 'data' in mysamples: samplelist.append(DataSample)

if doOnlySignal: samplelist = [sigSample,DataSample]

#bkgOnly.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
bkgOnly.addSamples(samplelist)
    
if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.022) ## new recommendations for Moriond 2017 from February 2017
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in ['general','LM','MM','HM']:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################

tmp = appendTo(bkgOnly.addChannel("cuts",["WREM"],1,0.5,1.5),[elmuChans["general"],bReqChans["general"]])
bkgOnly.addBkgConstrainChannels(tmp)

tmp = appendTo(bkgOnly.addChannel("cuts",["STCREM"],1,0.5,1.5),[elmuChans["general"],bReqChans["general"]])
bkgOnly.addBkgConstrainChannels(tmp)

#tmp = appendTo(bkgOnly.addChannel("cuts",["TREM"],1,0.5,1.5),[elmuChans["general"],bReqChans["general"]])
#bkgOnly.addBkgConstrainChannels(tmp)

if "HM" in CRregions or incl:
    print 'Here'
    tmp = appendTo(bkgOnly.addChannel("cuts",["TRHMEM"],1,0.5,1.5),[elmuChans["HM"],bReqChans["HM"]])
    bkgOnly.addBkgConstrainChannels(tmp)
    
if "MM" in CRregions or incl:
    tmp = appendTo(bkgOnly.addChannel("cuts",["TRMMEM"],1,0.5,1.5),[elmuChans["MM"],bReqChans["MM"]])
    bkgOnly.addBkgConstrainChannels(tmp)   
    
if "LM" in CRregions or incl:
    tmp = appendTo(bkgOnly.addChannel("cuts",["TRLMEM"],1,0.5,1.5),[elmuChans["LM"],bReqChans["LM"]])
    bkgOnly.addBkgConstrainChannels(tmp)   

# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"]=False
for region in CRregions:
    ValidRegList["OneLep"]  += ValidRegList[region]

validation = None
#run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"] or VRplots:
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
       for region in CRregions:
           appendIfMatchName(c,bReqChans[region])
           appendIfMatchName(c,bVetoChans[region])
           appendIfMatchName(c,bAgnosticChans[region])
           appendIfMatchName(c,elChans[region])
           appendIfMatchName(c,muChans[region])      
           appendIfMatchName(c,elmuChans[region])
	   
   
    if doTableInputs:
        if "HM" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SRHMEM"],1,0.5,1.5),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("cuts",["SRHMEl"],1,0.5,1.5),[bReqChans["HM"],elChans["HM"]])
            appendTo(validation.addValidationChannel("cuts",["SRHMMu"],1,0.5,1.5),[bReqChans["HM"],muChans["HM"]])
            appendTo(validation.addValidationChannel("cuts",["SRHMinclEM"],1,0.5,1.5),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("cuts",["SRHMinclEl"],1,0.5,1.5),[bReqChans["HM"],elChans["HM"]])
            appendTo(validation.addValidationChannel("cuts",["SRHMinclMu"],1,0.5,1.5),[bReqChans["HM"],muChans["HM"]])            
        if "LM" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SRLMEM"],1,0.5,1.5),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("cuts",["SRLMEl"],1,0.5,1.5),[bReqChans["LM"],elChans["LM"]])
            appendTo(validation.addValidationChannel("cuts",["SRLMMu"],1,0.5,1.5),[bReqChans["LM"],muChans["LM"]])
            appendTo(validation.addValidationChannel("cuts",["SRLMinclEM"],1,0.5,1.5),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("cuts",["SRLMinclEl"],1,0.5,1.5),[bReqChans["LM"],elChans["LM"]])
            appendTo(validation.addValidationChannel("cuts",["SRLMinclMu"],1,0.5,1.5),[bReqChans["LM"],muChans["LM"]])            
        if "MM" in CRregions:
            appendTo(validation.addValidationChannel("cuts",["SRMMEM"],1,0.5,1.5),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("cuts",["SRMMEl"],1,0.5,1.5),[bReqChans["MM"],elChans["MM"]])
            appendTo(validation.addValidationChannel("cuts",["SRMMMu"],1,0.5,1.5),[bReqChans["MM"],muChans["MM"]])
            appendTo(validation.addValidationChannel("cuts",["SRMMinclEM"],1,0.5,1.5),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("cuts",["SRMMinclEl"],1,0.5,1.5),[bReqChans["MM"],elChans["MM"]])
            appendTo(validation.addValidationChannel("cuts",["SRMMinclMu"],1,0.5,1.5),[bReqChans["MM"],muChans["MM"]])           
                     
        if doBlindSRinBGfit:
            if "HM" in CRregions:
                validation.getChannel("cuts",["SRHMEM"]).blind = True
                validation.getChannel("cuts",["SRHMEl"]).blind = True
                validation.getChannel("cuts",["SRHMMu"]).blind = True
                validation.getChannel("cuts",["SRHMinclEM"]).blind = True
                validation.getChannel("cuts",["SRHMinclEl"]).blind = True
                validation.getChannel("cuts",["SRHMinclMu"]).blind = True                
            if "LM" in CRregions:    
                validation.getChannel("cuts",["SRLMEM"]).blind = True 
                validation.getChannel("cuts",["SRLMEl"]).blind = True 
                validation.getChannel("cuts",["SRLMMu"]).blind = True
                validation.getChannel("cuts",["SRLMinclEM"]).blind = True 
                validation.getChannel("cuts",["SRLMinclEl"]).blind = True 
                validation.getChannel("cuts",["SRLMinclMu"]).blind = True               
            if "MM" in CRregions:    
                validation.getChannel("cuts",["SRMMEM"]).blind = True  
                validation.getChannel("cuts",["SRMMEl"]).blind = True 
                validation.getChannel("cuts",["SRMMMu"]).blind = True
                validation.getChannel("cuts",["SRMMinclEM"]).blind = True  
                validation.getChannel("cuts",["SRMMinclEl"]).blind = True 
                validation.getChannel("cuts",["SRMMinclMu"]).blind = True                
        
            
        if 'general' in CRregions:
            appendTo(validation.addValidationChannel("cuts",["VRtt1offEM"],1,0.5,1.5),[bReqChans["general"],elmuChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt2offEM"],1,0.5,1.5),[bReqChans["general"],elmuChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt3offEM"],1,0.5,1.5),[bReqChans["general"],elmuChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt1onEM"],1,0.5,1.5),[bReqChans["general"],elmuChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt2onEM"],1,0.5,1.5),[bReqChans["general"],elmuChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt3onEM"],1,0.5,1.5),[bReqChans["general"],elmuChans["general"]])  
            
            appendTo(validation.addValidationChannel("cuts",["VRtt1offEl"],1,0.5,1.5),[bReqChans["general"],elChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt2offEl"],1,0.5,1.5),[bReqChans["general"],elChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt3offEl"],1,0.5,1.5),[bReqChans["general"],elChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt1onEl"],1,0.5,1.5),[bReqChans["general"],elChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt2onEl"],1,0.5,1.5),[bReqChans["general"],elChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt3onEl"],1,0.5,1.5),[bReqChans["general"],elChans["general"]])  
            
            appendTo(validation.addValidationChannel("cuts",["VRtt1offMu"],1,0.5,1.5),[bReqChans["general"],muChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt2offMu"],1,0.5,1.5),[bReqChans["general"],muChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt3offMu"],1,0.5,1.5),[bReqChans["general"],muChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt1onMu"],1,0.5,1.5),[bReqChans["general"],muChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt2onMu"],1,0.5,1.5),[bReqChans["general"],muChans["general"]])
            appendTo(validation.addValidationChannel("cuts",["VRtt3onMu"],1,0.5,1.5),[bReqChans["general"],muChans["general"]])
	
# FOR PLOTS	    
    if VRplots:   
        if "LM" in CRregions:
            appendTo(validation.addValidationChannel("mt",["SRLMnomtEM"],6,50.,550.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["SRLMnomct2EM"],6,100.,400.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["SRLMnombbEM"],6,50.,650.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["SRLMnometEM"],6,150.,450.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mt",["SRLMEM"],6,100.,140.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["SRLMEM"],6,160.,400.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["SRLMEM"],6,105.,135.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["SRLMEM"],6,200.,400.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mt",["TRLM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["TRLM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["TRLM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["TRLM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mt",["STCR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["STCR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["STCR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["STCR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mt",["WR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["WR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["WR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["WR"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])	    	         	    	    	    
        if "MM" in CRregions:
            appendTo(validation.addValidationChannel("mt",["SRMMnomtEM"],6,50.,550.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("mct2",["SRMMnomct2EM"],6,100.,400.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("mbb",["SRMMnombbEM"],6,50.,650.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("met",["SRMMnometEM"],6,150.,450.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("mt",["SRMMEM"],6,140.,200.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("mct2",["SRMMEM"],6,160.,400.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("mbb",["SRMMEM"],6,105.,135.),[bReqChans["MM"],elmuChans["MM"]])
            appendTo(validation.addValidationChannel("met",["SRMMEM"],6,200.,400.),[bReqChans["MM"],elmuChans["MM"]])	
            appendTo(validation.addValidationChannel("mt",["TRMM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["TRMM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["TRMM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["TRMM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])	    	    	        
        if "HM" in CRregions:
            appendTo(validation.addValidationChannel("mt",["SRHMnomtEM"],6,50.,550.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("mct2",["SRHMnomct2EM"],6,100.,400.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("mbb",["SRHMnombbEM"],6,50.,650.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("met",["SRHMnometEM"],6,150.,450.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("mt",["SRHMEM"],6,200.,400.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("mct2",["SRHMEM"],6,160.,400.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("mbb",["SRHMEM"],6,105.,135.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("met",["SRHMEM"],6,200.,400.),[bReqChans["HM"],elmuChans["HM"]])
            appendTo(validation.addValidationChannel("mt",["TRHM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mct2",["TRHM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("mbb",["TRHM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])
            appendTo(validation.addValidationChannel("met",["TRHM"],6,40.,1000.),[bReqChans["LM"],elmuChans["LM"]])  

        # All validation regions should use over-flow bins!
        for v in validation.channels : 
            if not 'cuts' in v.name: v.useOverflowBin=True


# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #

#this is nothing we need to implement now (but will want to do so soon) - just giving a short template here

if myFitType==FitType.Exclusion:     
    SR_channels = {}
    SRs=[]
    if 'HM' in CRregions and not incl:
        SRs+=["SRHMEM"]   
    if 'MM' in CRregions and not incl:
        SRs+=["SRMMEM"]        
    if 'LM' in CRregions and not incl:
        SRs+=["SRLMEM"]   
    if 'HM' in CRregions and incl:
        SRs=["SRHMinclEM"]   
    if 'MM' in CRregions and incl:
        SRs=["SRMMinclEM"]        
    if 'LM' in CRregions and incl:
        SRs=["SRLMinclEM"]           
        

    for sig in sigSamples:
        SR_channels[sig] = []
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%sig)
        for c in myTopLvl.channels:
            for region in ['LM','MM','HM','general']:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
        
            
        sigSample = Sample(sig,kPink)    
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
        new_samples = ["C1N2_Wh_hall_275_100","C1N2_Wh_hall_375_100","C1N2_Wh_hall_400_100","C1N2_Wh_hall_425_100","C1N2_Wh_hall_450_100","C1N2_Wh_hall_475_100","C1N2_Wh_hall_425_75","C1N2_Wh_hall_450_75","C1N2_Wh_hall_475_75","C1N2_Wh_hall_500_75","C1N2_Wh_hall_475_50","C1N2_Wh_hall_500_50","C1N2_Wh_hall_525_50","C1N2_Wh_hall_500_25","C1N2_Wh_hall_525_25","C1N2_Wh_hall_550_25","C1N2_Wh_hall_575_25","C1N2_Wh_hall_525_0","C1N2_Wh_hall_550_0","C1N2_Wh_hall_575_0","C1N2_Wh_hall_600_0","C1N2_Wh_hall_275p0_145p0","C1N2_Wh_hall_275p0_125p0","C1N2_Wh_hall_300p0_125p0","C1N2_Wh_hall_325p0_125p0","C1N2_Wh_hall_350p0_125p0","C1N2_Wh_hall_375p0_125p0","C1N2_Wh_hall_400p0_125p0","C1N2_Wh_hall_425p0_125p0","C1N2_Wh_hall_450p0_125p0","C1N2_Wh_hall_300p0_170p0","C1N2_Wh_hall_300p0_150p0","C1N2_Wh_hall_325p0_150p0",
"C1N2_Wh_hall_350p0_150p0","C1N2_Wh_hall_375p0_150p0","C1N2_Wh_hall_400p0_150p0","C1N2_Wh_hall_425p0_150p0","C1N2_Wh_hall_325p0_190p0","C1N2_Wh_hall_325p0_175p0","C1N2_Wh_hall_400p0_175p0","C1N2_Wh_hall_425p0_175p0","C1N2_Wh_hall_450p0_175p0","C1N2_Wh_hall_350p0_200p0","C1N2_Wh_hall_375p0_200p0","C1N2_Wh_hall_400p0_200p0","C1N2_Wh_hall_425p0_200p0","C1N2_Wh_hall_450p0_200p0","C1N2_Wh_hall_500p0_100p0","C1N2_Wh_hall_500p0_125p0","C1N2_Wh_hall_500p0_150p0","C1N2_Wh_hall_475p0_150p0","C1N2_Wh_hall_475p0_125p0","C1N2_Wh_hall_525p0_75p0","C1N2_Wh_hall_525p0_100p0","C1N2_Wh_hall_525p0_125p0","C1N2_Wh_hall_550p0_50p0","C1N2_Wh_hall_550p0_75p0","C1N2_Wh_hall_550p0_125p0","C1N2_Wh_hall_575p0_50p0","C1N2_Wh_hall_575p0_75p0","C1N2_Wh_hall_575p0_100p0","C1N2_Wh_hall_575p0_125p0","C1N2_Wh_hall_600p0_15p0","C1N2_Wh_hall_600p0_50p0","C1N2_Wh_hall_600p0_75p0","C1N2_Wh_hall_600p0_100p0","C1N2_Wh_hall_600p0_125p0","C1N2_Wh_hall_625p0_0p0","C1N2_Wh_hall_625p0_25p0","C1N2_Wh_hall_625p0_50p0","C1N2_Wh_hall_625p0_100p0","C1N2_Wh_hall_650p0_0p0","C1N2_Wh_hall_650p0_25p0","C1N2_Wh_hall_650p0_50p0","C1N2_Wh_hall_650p0_75p0","C1N2_Wh_hall_650p0_100p0","C1N2_Wh_hall_675p0_25p0","C1N2_Wh_hall_675p0_50p0","C1N2_Wh_hall_675p0_75p0","C1N2_Wh_hall_675p0_100p0","C1N2_Wh_hall_700p0_0p0","C1N2_Wh_hall_700p0_25p0","C1N2_Wh_hall_700p0_50p0","C1N2_Wh_hall_750p0_0p0","C1N2_Wh_hall_750p0_25p0","C1N2_Wh_hall_750p0_50p0","C1N2_Wh_hall_800p0_0p0","C1N2_Wh_hall_800p0_25p0","C1N2_Wh_hall_800p0_50p0","C1N2_Wh_hall_550p0_100p0","C1N2_Wh_hall_450p0_150p0","C1N2_Wh_hall_625p0_75p0","C1N2_Wh_hall_675p0_0p0","C1N2_Wh_hall_375p0_175p0","C1N2_Wh_hall_350p0_175p0","C1N2_Wh_hall_375p0_225p0",
"C1N2_Wh_hall_400p0_225p0","C1N2_Wh_hall_400p0_250p0","C1N2_Wh_hall_425p0_225p0","C1N2_Wh_hall_425p0_250p0","C1N2_Wh_hall_450p0_225p0","C1N2_Wh_hall_450p0_250p0","C1N2_Wh_hall_475p0_175p0","C1N2_Wh_hall_475p0_200p0","C1N2_Wh_hall_475p0_225p0","C1N2_Wh_hall_475p0_250p0","C1N2_Wh_hall_500p0_175p0","C1N2_Wh_hall_500p0_200p0","C1N2_Wh_hall_500p0_225p0","C1N2_Wh_hall_500p0_250p0","C1N2_Wh_hall_525p0_150p0","C1N2_Wh_hall_525p0_175p0","C1N2_Wh_hall_525p0_200p0","C1N2_Wh_hall_525p0_225p0","C1N2_Wh_hall_525p0_250p0","C1N2_Wh_hall_550p0_150p0","C1N2_Wh_hall_550p0_175p0","C1N2_Wh_hall_550p0_200p0","C1N2_Wh_hall_550p0_225p0",
"C1N2_Wh_hall_550p0_250p0","C1N2_Wh_hall_575p0_150p0","C1N2_Wh_hall_575p0_175p0","C1N2_Wh_hall_575p0_200p0","C1N2_Wh_hall_575p0_225p0","C1N2_Wh_hall_575p0_250p0","C1N2_Wh_hall_600p0_150p0","C1N2_Wh_hall_600p0_175p0","C1N2_Wh_hall_600p0_200p0","C1N2_Wh_hall_600p0_225p0","C1N2_Wh_hall_600p0_250p0","C1N2_Wh_hall_625p0_125p0","C1N2_Wh_hall_625p0_150p0","C1N2_Wh_hall_625p0_175p0","C1N2_Wh_hall_625p0_200p0","C1N2_Wh_hall_625p0_225p0","C1N2_Wh_hall_625p0_250p0","C1N2_Wh_hall_650p0_125p0","C1N2_Wh_hall_650p0_150p0","C1N2_Wh_hall_650p0_175p0","C1N2_Wh_hall_650p0_200p0","C1N2_Wh_hall_650p0_225p0","C1N2_Wh_hall_650p0_250p0","C1N2_Wh_hall_675p0_125p0","C1N2_Wh_hall_675p0_150p0","C1N2_Wh_hall_675p0_175p0","C1N2_Wh_hall_675p0_200p0","C1N2_Wh_hall_675p0_225p0","C1N2_Wh_hall_675p0_250p0","C1N2_Wh_hall_700p0_100p0","C1N2_Wh_hall_700p0_125p0","C1N2_Wh_hall_700p0_150p0","C1N2_Wh_hall_700p0_175p0","C1N2_Wh_hall_700p0_200p0","C1N2_Wh_hall_700p0_225p0","C1N2_Wh_hall_700p0_250p0","C1N2_Wh_hall_750p0_100p0","C1N2_Wh_hall_750p0_125p0","C1N2_Wh_hall_750p0_150p0","C1N2_Wh_hall_750p0_175p0","C1N2_Wh_hall_750p0_200p0","C1N2_Wh_hall_750p0_225p0","C1N2_Wh_hall_750p0_250p0","C1N2_Wh_hall_800p0_100p0","C1N2_Wh_hall_800p0_125p0","C1N2_Wh_hall_800p0_150p0","C1N2_Wh_hall_800p0_175p0","C1N2_Wh_hall_800p0_200p0","C1N2_Wh_hall_800p0_225p0","C1N2_Wh_hall_800p0_250p0"]
        old_sample = True
        if sig in new_samples:
            old_sample=False 
        if old_sample:
            sigSample.addSampleSpecificWeight("0.58/0.82")
        else:
            sigSample.addSampleSpecificWeight("0.58")	
        
        #signal-specific uncertainties  
        sigSample.setStatConfig(useStat)
        sigSample.addSystematic(xsecSig)


        if not doHistoBuilding:
            myTopLvl.addSamples(sigSample)
            myTopLvl.setSignalSample(sigSample)

        #Create channels for each SR
        for sr in SRs:
            if not doShapeFit:
                ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
            else:
                if sr=='SRHMEM':
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
                    ch.useOverflowBin=False 
                elif sr=='SRMMEM':
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
                    ch.useOverflowBin=False 
                elif sr=='SRLMEM':
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
                    ch.useOverflowBin=False        
                elif sr=='SRHMinclEM':
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
                    ch.useOverflowBin=False 
                elif sr=='SRMMinclEM':
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
                    ch.useOverflowBin=False 
                elif sr=='SRLMinclEM':
                    ch = myTopLvl.addChannel("cuts",[sr],1,0.5,1.5)
                    ch.useOverflowBin=False                        
                else: 
                    raise RuntimeError("This region is not yet implemented in a shape fit mode: %s"%sr)
                
            for region in CRregions:
                if region in sr:
                    if 'El' in sr:                
                        elChans[region].append(ch) 
                    elif 'Mu' in sr:
                        muChans[region].append(ch)
                    elif 'EM' in sr:
                        elmuChans[region].append(ch)
                    else: raise RuntimeError("Unexpected signal region %s"%sr)
                    bReqChans[region].append(ch)
          
            #setup the SR channel
            myTopLvl.setSignalChannels(ch)        
            SR_channels[sig].append(ch)
        

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in ['LM','MM','HM','general']:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan
        
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
	#print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
	    #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in ['LM','MM','HM','general']:
        SetupChannels(elChans[region],basicChanSyst[region])
        SetupChannels(muChans[region],basicChanSyst[region])
        SetupChannels(elmuChans[region],basicChanSyst[region])
##Final semi-hacks for signal samples in exclusion fits

if myFitType==FitType.Exclusion and not doHistoBuilding:
    for sig in sigSamples:
        myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig)
    
            #if "SR2J" in chan.name:	    
                #theSample.removeWeight("pileupWeight")
                #theSample.removeSystematic("pileup")

             
            sys_region = ""
            if "HM" in chan.name: sys_region = "HM"
            elif "MM" in chan.name: sys_region = "MM"
            elif "LM" in chan.name: sys_region = "LM"          
            else: 
                print "Unknown region! - Take systematics from the general regions."
                sys_region = "general"

            # replacement of JES PunchThrough systematic for AF2 signal samples (not for FullSim signal samples)
            #if not debug and not (sig in FullSimSig):
            #    print "This is an AFII signal sample -> removing JES_PunchThrough_MC15, adding JES_PunchThrough_AFII and JES_RelativeNonClosure_AFII systematics"
            #    theSample.removeSystematic("JES_PunchThrough_MC15")
            #    theSample.addSystematic(Systematic("JES_PunchThrough_AFII","_NoSys","_JET_PunchThrough_AFII__1up","_JET_PunchThrough_AFII__1down","tree","overallNormHistoSys"))
            #    theSample.addSystematic(Systematic("JES_RelativeNonClosure_AFII","_NoSys","_JET_RelativeNonClosure_AFII__1up","_JET_RelativeNonClosure_AFII__1down","tree","overallNormHistoSys"))            

            #for syst in basicChanSyst[sys_region]:
            #    theSample.addSystematic(syst)   
                
            theSigFiles=[]
            if chan in elChans_all:
                theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all:
                theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all:
                theSigFiles = sigFiles_em[sig]	                  
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0:
                theSample.setFileList(theSigFiles)
            else:
                print "ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name,chan.name)
                chan.removeSample(theSample)
                

# b-tag reg/veto/agnostic channels


for region in ['LM','MM','HM','general']:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])	    

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")
       
    for chan in (bVetoChans[region]+bReqChans[region]+bAgnosticChans[region]):
        if not doOnlySignal:
            #if 'diboson' in mysamples: chan.getSample("diboson_Sherpa221").addSystematic(DBComm[region])
            #if 'Zjets' in mysamples: chan.getSample("zjets_Sherpa221").addSystematic(ZjetsComm[region])           
           
            if 'ttbar' in mysamples: 
                #chan.getSample("ttbar_bin1").setNormRegions([("WREM","cuts"),("TRLMEM","cuts"),("TRMMEM","cuts"),("TRHMEM","cuts"),("STCREM","cuts")])
                chan.getSample("ttbar_bin1").setNormRegions([("WREM","cuts"),("TRLMEM","cuts"),("TRMMEM","cuts"),("TRHMEM","cuts"),("STCREM","cuts")])
                chan.getSample("ttbar_bin2").setNormRegions([("WREM","cuts"),("TRLMEM","cuts"),("TRMMEM","cuts"),("TRHMEM","cuts"),("STCREM","cuts")])
                chan.getSample("ttbar_bin3").setNormRegions([("WREM","cuts"),("TRLMEM","cuts"),("TRMMEM","cuts"),("TRHMEM","cuts"),("STCREM","cuts")])
            if 'Wjets' in mysamples: chan.getSample("wjets_Sherpa221").setNormRegions([("WREM","cuts"),("TRLMEM","cuts"),("TRMMEM","cuts"),("TRHMEM","cuts"),("STCREM","cuts")])
            if 'singletop' in mysamples: chan.getSample("singletop").setNormRegions([("WREM","cuts"),("TRLMEM","cuts"),("TRMMEM","cuts"),("TRHMEM","cuts"),("STCREM","cuts")])
                    
                    
            if 'diboson' in mysamples:
                chan.getSample('diboson_Sherpa221').addSystematic(Systematic("diboson",configMgr.weights,1.06,0.94,"user","userOverallSys"))
            if 'ttv' in mysamples:
                chan.getSample('ttv_NLO').addSystematic(Systematic("ttV",configMgr.weights,1.125,0.875,"user","userOverallSys"))
                
#####################################################
	# Add separate Normalization Factors for ttbar and W+jets for each CR                               #
######################################################
if useNJetNormFac:
   
    for chan in AllChannels_all:
        mu_W_Xj = "mu_W_XJ"
        mu_Top_Xj = "mu_Top_XJ"
        ### doldol
        if "2J" in chan.name:            
                mu_W_Xj = "mu_W_2J"
                mu_Top_Xj = "mu_Top_2J"
        elif "4Jhighx" in chan.name:            
                mu_W_Xj = "mu_W_4Jhighx"
                mu_Top_Xj = "mu_Top_4Jhighx"
        elif "4Jlowx" in chan.name:            
                mu_W_Xj = "mu_W_4Jlowx"
                mu_Top_Xj = "mu_Top_4Jlowx"
        elif "6J" in chan.name:            
                mu_W_Xj = "mu_W_6J"
                mu_Top_Xj = "mu_Top_6J"
        
        else:
            log.warning("Channel %s gets no separated normalization factor" % chan.name)
            
        if 'Wjets' in mysamples: chan.getSample(WSampleName).addNormFactor(mu_W_Xj,1.,4.,0.)
        if 'ttbar' in mysamples: chan.getSample(TTbarSampleName).addNormFactor(mu_Top_Xj,1.,4.,0.)
        if 'singletop' in mysamples: chan.getSample(SingleTopSampleName).addNormFactor(mu_Top_Xj,1.,4.,0.1)
        log.info("Adding additional normalization factors (%s, %s) to channel (%s)" %(mu_W_Xj, mu_Top_Xj, chan.name))

    meas.addParamSetting("mu_W","const",1.0)
    meas.addParamSetting("mu_Top","const",1.0)
    
    if validation:
        meas_valid  = validation.getMeasurement("BasicMeasurement")
        meas_valid.addParamSetting("mu_W","const",1.0)
        meas_valid.addParamSetting("mu_Top","const",1.0)
        #meas.addParamSetting("alpha_"+pdfInterSyst[region].name,"const",1.0)

       
    if myFitType==FitType.Exclusion:
        for sig in sigSamples:
            meas_excl=configMgr.getFitConfig("Sig_%s"%sig).getMeasurement("BasicMeasurement")
            meas_excl.addParamSetting("mu_W","const",1.0)
            meas_excl.addParamSetting("mu_Top","const",1.0)
     
        


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(kGreen-9)
entry.SetFillColor(kGreen-9)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(kAzure-4)
entry.SetFillColor(kAzure-4)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(kGreen-5)
entry.SetFillColor(kGreen-5)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarH","lf")
entry.SetLineColor(ttbarHSample.color)
entry.SetFillColor(ttbarHSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation :
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:        
    myTopLvl=configMgr.getFitConfig("Sig_%s"%sig)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"
    
c.Close()
MeffBins = [ '1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    #chan.titleX="m^{incl}_{eff} [GeV]"
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True

#configMgr.fitConfigs.remove(bkgOnly)


# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 

