################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch

import Winter1617.SignalAccUncertainties

from logger import Logger
log = Logger('OneLeptonbb')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        #chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onDOL= False
if onDOL:    onLxplus=False  ## previous status does not detect the batch system in lxplus

debug = False #!!!

if onDOL:  debug = True  ## for test

if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]

SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
SystList.append("JVT")      # JVT scale factors
SystList.append("pileup")   # pileup (common)
SystList.append("BTag")     # b-tagging uncertainties
SystList.append("Dibosons") # scale variation for renormalization, factorization, resummation and generator comparison
SystList.append("Wjets")    # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
SystList.append("Ttbar")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation#
SystList.append("SingleTop")    # Radiation and QCD scales,Hadronization/fragmentation,Hard scattering generation
SystList.append("ttbarPDF") # 
SystList.append("wjetsPDF") # 
SystList.append("Zjets")    # scale variation for renormalization, factorization, resummation,matching

if debug:
    SystList=[]
    
#for creating the backup chache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False
if onDOL: doHistoBuilding = False

# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=True

useNJetNormFac=False
# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["LM","MM","HM"] #this is the default - modify from command line


# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r LM,MM,HM  option)"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["LM"] = False
ValidRegList["MM"] = False
ValidRegList["HM"] = False

# for tables (turn doTableInputs to True)
doTableInputs = True #This effectively means no validation plots but only validation tables (but is 100x faster)
if onDOL:  doTableInputs = False
for cr in CRregions:
    if "LM" in cr and doTableInputs:
        ValidRegList["LM"] = True
    if "MM" in cr and doTableInputs:
        ValidRegList["MM"] = True	
    if "HM" in cr and doTableInputs:
        ValidRegList["HM"] = True

#N-1 plots in SRs        
VRplots=False

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=True

# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    
    #sigSamples=["GG_oneStep_945_785_625"]
    #sigSamples=["GG_oneStep_637_626_615"]    
   
    #sigSamples=["GG_oneStep_1105_865_625"]
    sigSamples=["C1N2_Wh_hall_450p0_0p0"]
    
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
'''    
if myFitType==FitType.Exclusion:
    if onDOL:
        analysissuffix += '_'+ sigSamples[0]
    if 'GG_oneStep' in sigSamples[0]  and not onDOL:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]  and not onDOL:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]
'''		

mylumi= 36.06596#36.46161#14.78446 #30 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  

mysamples = ["triboson", "VH", "Zjets","ttv","ttH","singletop","diboson","Wjets","ttbar","data"]
doOnlySignal=False

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
	
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "OneLeptonbb"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":
useBackupCache = False
if (onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "OneLepton"+analysissuffix_BackupCache+analysisextension
    if not onLxplus: histBackupCacheFileName = "data/backup.root"
    #if not onLxplus: histBackupCacheFileName = "data/OneLepton_6J_GG_oneStep_samples_Wjets.root"
    else: histBackupCacheFileName = "data/OneLepton_2J_4Jhighx_4Jlowx_6J_GG_oneStep.root"
    if onDOL:    histBackupCacheFileName = "/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/HISTFITTER/HistFitterUser/MET_jets_leptons/data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 0.001 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

if onDOL:
    configMgr.fixSigXSec=False
else:
    configMgr.fixSigXSec=True

useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = True # Blind the CRs (default is False)
configMgr.blindVR = True # Blind the VRs (default is False)
doBlindSRinBGfit  = True # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = []

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

inputDir=""
inputDirData=""
inputDirSig=""

if not onLxplus:
    inputDir="/project/etp2/SUSYWh/NEW/"
    inputDirSig="/project/etp2/SUSYWh/NEW/"
    inputDirData="/project/etp2/SUSYWh/NEW/"
    
#background files, separately for electron and muon channel:

#bgdFiles_e = [inputDir+"allTrees_T_06_10_bkg.root"]
#bgdFiles_m = [inputDir+"allTrees_T_06_10_bkg.root"]
#bgdFiles_em = [inputDir+"allTrees_T_06_10_bkg.root"]
files_em={}
files_em['Wjets']=[inputDir+"wjets_Sherpa221_merged.root"]
files_em['Zjets']=[inputDir+"zjets_Sherpa221_merged.root"]
files_em['diboson']=[inputDir+"diboson_Sherpa221_merged.root"]
files_em['ttbar']=[inputDir+"ttbar_merged.root"]
files_em['ttv']=[inputDir+"ttv_merged.root"]
files_em['ttH']=[inputDir+"ttH_merged.root"]
files_em['singletop']=[inputDir+"singletop_merged.root"]
files_em['triboson']=[inputDir+"triboson_Sherpa221_merged.root"]
files_em['VH']=[inputDir+"VH_merged.root"]

#signal files
sigFiles_e={}
sigFiles_m={}
sigFiles_em={}
for sigpoint in sigSamples:
    sigFiles_e[sigpoint]=[inputDirSig+"Signal_merged.root"]
    sigFiles_m[sigpoint]=[inputDirSig+"Signal_merged.root"]
    sigFiles_em[sigpoint] = [inputDirSig+"Signal_merged.root"]
        
#data files
#dataFiles = [inputDirData+"allTrees_T_06_03_dataOnly.root"] # 949.592 /pb
#dataFiles = [inputDirData+"allDS2_T_06_03.root"]    # DS2: 13277.66 /pb (remember to check lumiErr: should be 2.9%)
#dataFiles = [inputDirData+"allDS2v01_T_06_03.root"] # DS2.1: 14784.46 /pb (remember to check lumiErr: should be 3.0%)
#dataFiles = [inputDirData+"allTrees_T_06_05_data.root"] # DS3: 36461.61 /pb (remember to check lumiErr: should be 4.1%)
dataFiles = [inputDirData+"data_merged_processed_tree_NoSys.root"] # (lumiErr=0.032)

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&  nLep_base==1&&nLep_signal==1 && trigMatch_singleLepTrig"
OneEleSelection = "&& (AnalysisType==1 && lep1Pt>27.) "
OneMuoSelection = "&& (AnalysisType==2 && lep1Pt>27.)"
OneLepSelection = "&& ( (AnalysisType==1 && lep1Pt>27.) || (AnalysisType==2 && lep1Pt>27.))"


# ------- HM region --------------------------------------------------------------------------- #
SRSelection        = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && jet1Pt==bjet1Pt && jet2Pt==bjet2Pt && mbb>105. && mbb<135. && mct2>160."
configMgr.cutsDict["SRLM"]=  SRSelection + "&& mt>=100 && mt<=140 && met>190." + CommonSelection
configMgr.cutsDict["SRMM"]=  SRSelection + "&& mt>=140 && mt<=200 && met>200." +CommonSelection
configMgr.cutsDict["SRHM"]=  SRSelection + "&& mt>200 && met>210." +CommonSelection

#SRSelection        = "nJet25>=2 && nJet25<=3 && nBJet25_MV2c10==2 && jet1Pt==bjet1Pt && jet2Pt==bjet2Pt && mbb>105. && mbb<135. && mct2>180."
#configMgr.cutsDict["SRLM"]=  SRSelection + "&& mt>=100 && mt<=140 && met>230" + CommonSelection
#configMgr.cutsDict["SRMM"]=  SRSelection + "&& mt>=140 && mt<=200 &&met>210" +CommonSelection
#configMgr.cutsDict["SRHM"]=  SRSelection + "&& mt>200&&met>130" +CommonSelection


d=configMgr.cutsDict
#second part: splitting into electron and muon channel for validation purposes	d=configMgr.cutsDict
defined_regions = []
if 'LM' in CRregions: 
    defined_regions+=["SRLM"]
    
if 'MM' in CRregions: 
    defined_regions+=["SRMM"]  
    
if 'HM' in CRregions: 
    defined_regions+=["SRHM"] 


for pre_region in defined_regions:
            configMgr.cutsDict[pre_region+"El"] = d[pre_region]+OneEleSelection
            configMgr.cutsDict[pre_region+"Mu"] = d[pre_region]+OneMuoSelection
            configMgr.cutsDict[pre_region+"EM"] = d[pre_region]+OneLepSelection  

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
weights=["genWeight","eventWeight","leptonWeight","bTagWeight","jvtWeight","pileupWeight"]#"triggerWeight"

configMgr.weights = weights

#need that later for QCD BG
#configMgr.weightsQCD = "qcdWeight"
#configMgr.weightsQCDWithB = "qcdBWeight"

#example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

#trigHighWeights = replaceWeight(weights,"triggerWeight","triggerWeightUp")
#trigLowWeights = replaceWeight(weights,"triggerWeight","triggerWeightDown")

#muon related uncertainties acting on weights
muonEffHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1up")
muonEffLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT__1down")
muonEffHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1up")
muonEffLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS__1down")
muonEffHighWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1up")
muonEffLowWeights_stat_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_STAT_LOWPT__1down")
muonEffHighWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1up")
muonEffLowWeights_sys_lowpt = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_SYS_LOWPT__1down")

muonIsoHighWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1up")
muonIsoLowWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_STAT__1down")
muonIsoHighWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1up")
muonIsoLowWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_ISO_SYS__1down")

muonHighTTVAWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1up")
muonLowTTVAWeights_stat = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_STAT__1down")
muonHighTTVAWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1up")
muonLowTTVAWeights_sys = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_TTVA_SYS__1down")

#muonHighWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
#muonLowWeights_trigger_stat = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
#muonHighWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
#muonLowWeights_trigger_syst = replaceWeight(weights,"leptonWeight","trigWeight_MUON_EFF_TrigSystUncertainty__1down")

#### electron related uncertainties acting on weights ####
#ID 
electronEffIDHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffIDLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Rec
electronEffRecoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffRecoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")

#Iso
electronIsoHighWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronIsoLowWeights = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")


# JVT weight systematics
#JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyUp")
#JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JvtEfficiencyDown")

JVTHighWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1up")
JVTLowWeights = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1down")


# pileup weight systematics
pileupup = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupdown = replaceWeight(weights,"pileupWeight","pileupWeightDown")

if "BTag" in SystList:
    bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
    bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

    cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
    cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

    mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
    mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
    
    eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
    eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

    eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
    eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}

for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 35% syst and an overallSys
   
     
  #  basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.30,0.7,"user","userOverallSys"))
   
    	
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_NoSys","_JET_JER_SINGLE_NP__1up","_NoSys","tree","overallNormHistoSysOneSide"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_NoSys","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_NoSys","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_NoSys","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_NoSys","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_NoSys","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_NoSys","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_NoSys","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_NoSys","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_NoSys","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6","_NoSys","_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_7","_NoSys","_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_8restTerm","_NoSys","_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree","overallNormHistoSys"))
	     
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_NoSys","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_NoSys","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_NoSys","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_NoSys","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_NoSys","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_NoSys","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_NoSys","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_NoSys","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_NoSys","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_NoSys","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_NoSys","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys"))

    	        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_NoSys","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_NoSys","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_NoSys","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_NoSys","_MET_SoftTrk_ResoPara","_NoSys","tree","overallNormHistoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_NoSys","_MET_SoftTrk_ResoPerp","_NoSys","tree","overallNormHistoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_NoSys","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_NoSys","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID","_NoSys","_MUON_ID__1up","_MUON_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS","_NoSys","_MUON_MS__1up","_MUON_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SCALE","_NoSys","_MUON_SCALE__1up","_MUON_SCALE__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_stat",configMgr.weights,muonHighTTVAWeights_stat,muonLowTTVAWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_sys",configMgr.weights,muonHighTTVAWeights_sys,muonLowTTVAWeights_sys,"weight","overallNormHistoSys"))
    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muonEffHighWeights_stat,muonEffLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muonEffHighWeights_sys,muonEffLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muonIsoHighWeights_stat,muonIsoLowWeights_stat,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muonIsoHighWeights_sys,muonIsoLowWeights_sys,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muonEffHighWeights_stat_lowpt,muonEffLowWeights_stat_lowpt,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muonEffHighWeights_sys_lowpt,muonEffLowWeights_sys_lowpt,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Eff",configMgr.weights,electronEffIDHighWeights,electronEffIDLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,electronEffRecoHighWeights,electronEffRecoLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,electronIsoHighWeights,electronIsoLowWeights,"weight","overallNormHistoSys")) 

    if "JVT" in SystList :
        #basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("JVT",configMgr.weights, JVTHighWeights ,JVTLowWeights,"weight","overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup",configMgr.weights,pileupup,pileupdown,"weight","overallNormHistoSys"))
    if "BTag" in SystList:
        bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,bTagHighWeights,bTagLowWeights,"weight","overallNormHistoSys")
        cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,cTagHighWeights,cTagLowWeights,"weight","overallNormHistoSys")
        mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,mTagHighWeights,mTagLowWeights,"weight","overallNormHistoSys")
        eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,eTagHighWeights,eTagLowWeights,"weight","overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,eTagFromCHighWeights,eTagFromCLowWeights,"weight","overallNormHistoSys")

        
    #signal uncertainties                                                                                       
    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
    
    #Generator Systematics
    generatorSyst = []  

# ********************************************************************* #
#                              Background samples

# ********************************************************************* #

configMgr.nomName = "_NoSys"

WSampleName = "wjets_Sherpa221"
WSample = Sample(WSampleName,kAzure-4)
#WSample.setNormFactor("mu_W",1.,0.,5.)
WSample.setStatConfig(useStat)
WSample.setFileList(files_em['Wjets'])

#
TTbarSampleName = "ttbar"
TTbarSample = Sample(TTbarSampleName,kGreen-9)
#TTbarSample.setNormFactor("mu_Top",1.,0.,5.)
TTbarSample.setStatConfig(useStat)
TTbarSample.setFileList(files_em['ttbar'])
#
DibosonsSampleName = "diboson_Sherpa221"
DibosonsSample = Sample(DibosonsSampleName,kOrange-8)
DibosonsSample.setStatConfig(useStat)
DibosonsSample.setNormByTheory()
DibosonsSample.setFileList(files_em['diboson'])
#
SingleTopSampleName = "singletop"
SingleTopSample = Sample(SingleTopSampleName,kGreen-5)
#SingleTopSample.setNormFactor("mu_Top",1.,0.,5.)
SingleTopSample.setStatConfig(useStat)
#SingleTopSample.setNormByTheory()
SingleTopSample.setFileList(files_em['singletop'])
#
ZSampleName = "zjets_Sherpa221"
ZSample = Sample(ZSampleName,kBlue+3)
ZSample.setStatConfig(useStat)
ZSample.setNormByTheory()
ZSample.setFileList(files_em['Zjets'])
#
ttbarVSampleName = "ttv"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
ttbarVSample.setFileList(files_em['ttv'])
#
ttbarHSampleName = "ttH"
ttbarHSample = Sample(ttbarHSampleName,kGreen-8)
ttbarHSample.setStatConfig(useStat)
ttbarHSample.setNormByTheory()
ttbarHSample.setFileList(files_em['ttH'])
#
TribosonsSampleName = "triboson_Sherpa221"
TribosonsSample = Sample(TribosonsSampleName,kOrange-8)
TribosonsSample.setStatConfig(useStat)
TribosonsSample.setNormByTheory()
TribosonsSample.setFileList(files_em['triboson'])
#
VHSampleName = "VH"
VHSample = Sample(VHSampleName,kGreen-8)
VHSample.setStatConfig(useStat)
VHSample.setNormByTheory()
VHSample.setFileList(files_em['VH'])
#
#QCD sample for later
#QCDSample = Sample("QCD",kYellow)
#QCDSample.setFileList([inputDir_QCD+"",inputDir_QCD+""]) 
#QCDSample.setQCD(True,"histoSys")
#QCDSample.setStatConfig(False)
#
#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()

sigSample = Sample(sigSamples[0],kPink)    
sigSample.setStatConfig(useStat)
sigSample.setNormByTheory()
sigSample.addSampleSpecificWeight("0.58/0.82")
sigSample.setNormFactor("mu_SIG",1.,0.,5.)
sigSample.setFileList(sigFiles_em[sigSamples[0]])


# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("Sig_"+sigSamples[0])

#creating now list of samples
samplelist=[]
samplelist.append(sigSample)
if 'triboson' in mysamples: samplelist.append(TribosonsSample)
if 'VH' in mysamples: samplelist.append(VHSample)
if 'Zjets' in mysamples: samplelist.append(ZSample)
if 'ttH' in mysamples: samplelist.append(ttbarHSample)
if 'ttv' in mysamples: samplelist.append(ttbarVSample)
if 'singletop' in mysamples: samplelist.append(SingleTopSample)
if 'diboson' in mysamples: samplelist.append(DibosonsSample)
if 'Wjets' in mysamples: samplelist.append(WSample)
if 'ttbar' in mysamples: samplelist.append(TTbarSample)
if 'data' in mysamples: samplelist.append(DataSample)

#bkgOnly.addSamples([ZSample,ttbarVSample,SingleTopSample,DibosonsSample,WSample,TTbarSample,DataSample])
bkgOnly.addSamples(samplelist)
    
if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.032) ## new recommendations for Moriond 2017 from February 2017
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

bkgOnly.setSignalSample(sigSample)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in CRregions:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################

if "LM" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["SRLMEM"],1,0.5,1.5),[elmuChans["LM"],bReqChans["LM"]])
    bkgOnly.addSignalChannels(tmp)
    #bkgOnly.addBkgConstrainChannels(tmp)
    
if "MM" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["SRMMEM"],1,0.5,1.5),[elmuChans["MM"],bReqChans["MM"]])
    bkgOnly.addSignalChannels(tmp)
    #bkgOnly.addBkgConstrainChannels(tmp)    

if "HM" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts",["SRHMEM"],1,0.5,1.5),[elmuChans["HM"],bReqChans["HM"]])

    bkgOnly.addSignalChannels(tmp)
    #bkgOnly.addBkgConstrainChannels(tmp) 



# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan
        
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
	#print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
	    #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region],basicChanSyst[region])
        SetupChannels(muChans[region],basicChanSyst[region])
        SetupChannels(elmuChans[region],basicChanSyst[region])
#

                

# b-tag reg/veto/agnostic channels


for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])	    

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")
                         


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("","t#bar{t}","lf")
entry.SetLineColor(kGreen-9)
entry.SetFillColor(kGreen-9)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","W+jets","lf")
entry.SetLineColor(kAzure-4)
entry.SetFillColor(kAzure-4)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Diboson","lf")
entry.SetLineColor(DibosonsSample.color)
entry.SetFillColor(DibosonsSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Single Top","lf")
entry.SetLineColor(kGreen-5)
entry.SetFillColor(kGreen-5)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","Z+jets","lf")
entry.SetLineColor(ZSample.color)
entry.SetFillColor(ZSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarH","lf")
entry.SetLineColor(ttbarHSample.color)
entry.SetFillColor(ttbarHSample.color)
entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg

c.Close()
MeffBins = [ '1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    #chan.titleX="m^{incl}_{eff} [GeV]"
    

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05 
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan.ATLASLabelText = "Internal"
            chan.showLumi = True

#configMgr.fitConfigs.remove(bkgOnly)
