###############################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed,kViolet
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch
print os.getcwd()
import FakeSyst 
import theoryUncertainties_SS_WZ
import theoryUncertainties_SS_Rare
import theoryUncertainties_SS_ttV
import theoryUncertainties_SS_WW_ZZ
#import Winter1617.SignalAccUncertainties

from logger import Logger
log = Logger('SS3L')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        #chan.setFileList(bgdFiles)
        for sample in chan.sampleList:
            if sample.name != "Fakes" and sample.name != "ChargeFlip":
                for syst in systList:
                    #print "Adding "+syst.name+ " to " +chan.name
                    sample.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onDOL= False
if onDOL:    onLxplus=False  ## previous status does not detect the batch system in lxplus

debug = False #!!!

if onDOL:  debug = True  ## for test

if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList_SS=[]
SystList_3L=[]

#SystList_SS.append("JER")      # Jet Energy Resolution (common)
#SystList_SS.append("JES")      # Jet Energy Scale (common)
#SystList_SS.append("MET")      # MET (common)
#SystList_SS.append("LEP")      # lepton uncertainties (common)
#SystList_SS.append("LepEff")   # lepton scale factors (common)
#SystList_SS.append("Trigger")  # trigger uncertainties
#SystList_SS.append("ChFlip")   # charge flip uncertainties
#SystList_SS.append("JVT")      # JVT scale factors
#SystList_SS.append("pileup")   # pileup (common)
#SystList_SS.append("BTag")     # b-tagging uncertainties
# #SystList_SS.append("TheoDummy") # Dummy uncertainty as approximation for the theoretical uncertainties
#SystList_SS.append("Fake")     # Correlated and uncorrelated fake estimation uncertainties
#SystList_SS.append("DDChargeFlip")  #Charge flip uncertainties from likelihood method
#SystList_SS.append("WZ")       # generator uncertainties for WZ  
#SystList_SS.append("Rare")
#SystList_SS.append("ttV")
#SystList_SS.append("WW_ZZ")
# SystList_SS.append("FakeDummy")


#SystList_3L.append("JER")      # Jet Energy Resolution (common)
#SystList_3L.append("JES")      # Jet Energy Scale (common)
#SystList_3L.append("MET")      # MET (common)
#SystList_3L.append("LEP")      # lepton uncertainties (common)
SystList_3L.append("LepEff")   # lepton scale factors (common)
#SystList_3L.append("Trigger")  # trigger uncertainties
#SystList_3L.append("ChFlip")   # charge flip uncertainties
#SystList_3L.append("JVT")      # JVT scale factors
#SystList_3L.append("pileup")   # pileup (common)
#SystList_3L.append("BTag")     # b-tagging uncertainties
# #SystList_3L.append("TheoDummy") # Dummy uncertainty as approximation for the theoretical uncertainties
#SystList_3L.append("Fake")     # Correlated and uncorrelated fake estimation uncertainties
#SystList_3L.append("DDChargeFlip")  #Charge flip uncertainties from likelihood method
#SystList_3L.append("WZ")       # generator uncertainties for WZ  
#SystList_3L.append("Rare")
#SystList_3L.append("ttV")
#SystList_3L.append("WW_ZZ")
# SystList_3L.append("FakeDummy")

if debug:
    SystList=[]
    
#for creating the backup chache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False
if onDOL: doHistoBuilding = False

# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=True

useNJetNormFac=False
# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["jet1", "jet23","3L"]

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r jet1, jet23 option)"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["jet1"] = False
ValidRegList["jet23"] = False


# for tables (turn doTableInputs to True)
doTableInputs = False #This effectively means no validation plots but only validation tables (but is 100x faster)

#N-1 plots in SRs        
VRplots=False

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=True

# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    
    sigSamples=["C1N2_Wh_hall_175p0_0p0"]
    
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
'''    
if myFitType==FitType.Exclusion:
    if onDOL:
        analysissuffix += '_'+ sigSamples[0]
    if 'GG_oneStep' in sigSamples[0]  and not onDOL:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]  and not onDOL:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]
'''		

#mylumi= 36.06596#36.46161#14.78446 #30 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  
mylumi = 1.

#mysamples = ["triboson", "higgs", "zjets","ttv","singletop","diboson","wjets","ttbar","vgamma", "multitop", "drellyan","data"]
mysamples = ["Rare","ttV","WW","WZ","ZZ","data_nom"]
doOnlySignal=False
useFakePrediction= True
useDataDrivenCF=True

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
	
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "SS3L"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":

useBackupCache = True
useBackupJeanette = True # True to choose etp3/jlorenz backup file, False for etp1/dkoeck

if (onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "SS3L"+analysissuffix_BackupCache+analysisextension
    if not onLxplus:
        if useBackupJeanette:
            histBackupCacheFileName = "/project/etp3/jlorenz/shape_fit/HistFitter_January2017/HistFitterUser/WH1Lbb/data/backup_SS3L.root"
        else:
            histBackupCacheFileName = "/project/etp1/dkoeck/HistFitter/data/" + MyAnalysisName_BackupCache + ".root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1. #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

configMgr.fixSigXSec=False

useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=50000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = True

#insert additional parameter for WZ in the combination 
doCombination=True

#blinding of various regions
configMgr.blindSR = False # Blind the SRs (default is False)
configMgr.blindCR = False # Blind the CRs (default is False)
configMgr.blindVR = False # Blind the VRs (default is False)
doBlindSRinBGfit  = False # Blind SR when performing a background fit

#useSignalInBlindedData=True
#configMgr.ReduceCorrMatrix=True
configMgr.ReduceCorrMatrix=True                        

#we have only signal regions in this config file, make sure this is discovered
configMgr.keepSignalRegionType = True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = []

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

inputDir=""
inputDirData=""
inputDirSig=""

if not onLxplus:
    inputDir="/project/etp1/dkoeck/ntuples/HF_trees/"
    inputDirSig="/project/etp1/dkoeck/ntuples/HF_trees/"
    inputDirData="/project/etp1/dkoeck/ntuples/HF_trees/"

#background files, separately for electron and muon channel:


files_em={}
#files_em['ttV']=[inputDir + "background.36100.root"]
bkgr_file_mctruth = "TruthIncl/background.36100.inclTruth.wOMissing.no363492.root"
bkgr_file_mctruth_twz ="TruthIncl/background.36100.inclTruth.inclMissingtWZ.no363492.root"
bkgr_file_mctruth_twz_th = "TruthIncl/background.36100.inclTruth.tWZ.tH.root"
bkgr_file = "background_complete_MuFix_36100.root"
bkgr_updatedVVV_mctruth = "TruthIncl/background.36100.updatedVVVMcTruth_incltHtWZ.root"
files_em['ttV']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['Rare']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['WW']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['WZ']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['ZZ']=[inputDir + bkgr_updatedVVV_mctruth]


if useFakePrediction:
#    files_em['Fakes']=["/project/etp1/dkoeck/ntuples/Fakes_Charge.root"]
    files_em['Fakes']=["/project/etp1/dkoeck/ntuples/HF_trees/Fakes_HF.root"]
if useDataDrivenCF:
    files_em['ChargeFlip']=["/project/etp1/dkoeck/ntuples/CF/ChargeFlip.root"]

#Signal files
sigFiles_e={}
sigFiles_m={}
sigFiles_em={}
sig_extension=["C1N2_Wh_hall_202p5_72p5","C1N2_Wh_hall_212p5_12p5","C1N2_Wh_hall_212p5_37p5","C1N2_Wh_hall_212p5_62p5","C1N2_Wh_hall_215p0_85p0","C1N2_Wh_hall_225p0_0p0","C1N2_Wh_hall_225p0_25p0","C1N2_Wh_hall_225p0_50p0","C1N2_Wh_hall_225p0_75p0","C1N2_Wh_hall_237p5_12p5","C1N2_Wh_hall_237p5_37p5","C1N2_Wh_hall_237p5_62p5","C1N2_Wh_hall_237p5_87p5","C1N2_Wh_hall_250p0_0p0","C1N2_Wh_hall_250p0_25p0","C1N2_Wh_hall_250p0_50p0"]

for sigpoint in sigSamples:
    if sigpoint in sig_extension:       
        sigFiles_e[sigpoint]=[inputDirSig+"C1N2_Wh_hall_ext36100.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"C1N2_Wh_hall_ext36100.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"C1N2_Wh_hall_ext36100.root"]
    else:   
        sigFiles_e[sigpoint]=[inputDirSig+"C1N2_Wh_hall_36100.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"C1N2_Wh_hall_36100.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"C1N2_Wh_hall_36100.root"]
        
#data files
dataFiles = [inputDirData+"TruthIncl/data.36100.root"] # need to check lumierror, exact lumi value

#now looking at 3L files:

files_3L={}
files_3L['background'] = ["/project/etp5/SUSYEWKWH/SS/trees_Jun18/Excl_newWeight_histo/L3_BG13TeV.root"]
files_3L['data'] = ["/project/etp5/SUSYEWKWH/SS/trees_Jun18/Excl_newWeight_histo/L_DATA13TeV.root"]
files_3L['fakes'] = ["/project/etp5/SUSYEWKWH/SS/trees_Jun18/Excl_newWeight_histo/L3_FakesMM13TeV.root"]
files_3L['signal'] = ["/project/etp5/SUSYEWKWH/SS/trees_Jun18/Excl_newWeight_histo/L3_SMWh13TeV.root"]


# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&NlepBL==2 && nSigLep==2 && nBJets20==0"
#CommonSelection = "&&  nLep_base==2 && nLep_signal==2 && ((lep1Charge==1 && lep2Charge==1) || (lep1Charge==-1 && lep2Charge==-1)) "

EleEleSelection = "&& (AnalysisType==1 && AnalysisType_lep2==1) "
MuoMuoSelection = "&& (AnalysisType==2 && AnalysisType_lep2==2)"
EleMuoSelection = "&& ( (AnalysisType==1 && AnalysisType_lep2==2) || (AnalysisType==2 && AnalysisType_lep2==1))"
Zmassveto       = "&& ((mll - 91.2)>10 || (mll-91.2)<-10)"
Zmasspeak       = "&& ( (((mll - 91.2)<=10) && (mll - 91.2)>=0) || (((mll - 91.2)>=-10 && (mll - 91.2)<=0)))"

# ------- SR regions --------------------------------------------------------------------------- #
SRSelection = "Pt_l>=25000 && Pt_subl>=25000"

configMgr.cutsDict["SRjet1"] = SRSelection + "&&nJets20==1 &&DeltaEtaLep<=1.5 && met>=100000 && mt>=140000 && meff>=260000.&& mljj_comb<180000 &&MT2>=80000" + CommonSelection 
configMgr.cutsDict["SRjet23"] = SRSelection +  "&& (nJets20==2||nJets20==3) && met>=100000 && mt>=120000 && meff>=240000.&& mljj_comb<130000 &&MT2>=70000" + CommonSelection

#---------VR regions --------------------------------------------------------------------------- #
configMgr.cutsDict["VRjet1"] = SRSelection + "&&nJets20==1 &&DeltaEtaLep<1.5 && met<=100000 && met>=70000 && mt>=140000 && mljj_comb>130000" + CommonSelection
configMgr.cutsDict["VRjet23"] = SRSelection + "&& (nJets20==2||nJets20==3) && met>=100000 && mt<=120000 && mt>=65000 && meff>=240000.&& mljj_comb>130000 " + CommonSelection


#3L cuts

configMgr.cutsDict["baseThreeLep"]= "(LepPt2>25000. && Mlll>20000. && MET>20000. && pass3l && passtrigger )" #trig matching
configMgr.cutsDict["SR3LDFOS0j"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passhiggs==1 && njets==0 && MET>60000.  && mhiggs<90000. )"
configMgr.cutsDict["SR3LDFOS1ja"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passhiggs==1 && njets>0  && MET>30000.  && MET<100000. && mhiggs<60000. && deltaRmin<1.4)"
configMgr.cutsDict["SR3LDFOS1jb"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passhiggs==1 && njets>0  && MET>100000. && mhiggs<70000. && deltaRmin<1.4 && dphi12<2.8)"
configMgr.cutsDict["SR3LSFOS0ja"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passSFOS==1 && njets==0 && Mtmin>110000. && mSFOS>20000. && fabs(mSFOS/1000.-91.2)>10. && MET>80000. && MET<120000.)"
configMgr.cutsDict["SR3LSFOS0jb"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passSFOS==1 && njets==0  && Mtmin>110000. && mSFOS>20000. && fabs(mSFOS/1000.-91.2)>10. && MET>120000.)"
configMgr.cutsDict["SR3LSFOS1J"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passSFOS==1 && njets>0  && Mtmin>110000. && mSFOS>20000. && fabs(mSFOS/1000.-91.2)>10. && MET>110000.)"
configMgr.cutsDict["VR3L_offZ_highMET"]="("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passSFOS==1 && MET>80000. && fabs(mSFOS/1000.-91.2)>10. && mSFOS>20000 && Mtmin<110000.)"
configMgr.cutsDict["CR3L_onZ_highMET"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passSFOS==1 && MET>80000. && fabs(mSFOS/1000.-91.2)<10. && mSFOS>20000)"



# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
#weights=["wmu_nom","wtrig_nom","wel_nom","wjet_nom","wchflip_nom","lumiScaling","wpu_nom_bkg","wpu_nom_sig"]
weights=["mcweight","wmu_nom","wtrig_nom","wel_nom","wjet_nom","wchflip_nom","lumiScaling","wpu_nom_bkg","wpu_nom_sig"]

#3L weights
weights_3L = ["bTagWeight","pileupWeight","JVTSF", "ElecSF","MuonSF","EvtWeight", "NormWeight","36.1"]

#weights=["1"]
#configMgr.weights = weights

#muon related uncertainties acting on weights
muon_bad_stat_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_stat_up")
muon_bad_stat_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_stat_down")
muon_bad_sys_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_sys_up")
muon_bad_sys_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_sys_down")

muon_stat_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_up")
muon_stat_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_down")
muon_sys_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_up")
muon_sys_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_down")

muon_stat_lowpt_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_lowpt_up")
muon_stat_lowpt_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_lowpt_down")
muon_sys_lowpt_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_lowpt_up")
muon_sys_lowpt_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_lowpt_down")

muon_iso_stat_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_stat_up")
muon_iso_stat_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_stat_down")
muon_iso_sys_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_sys_up")
muon_iso_sys_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_sys_down")

muon_ttva_stat_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_stat_up")
muon_ttva_stat_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_stat_down")
muon_ttva_sys_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_sys_up")
muon_ttva_sys_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_sys_down")

muon_trig_stat_UpWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_stat_up")
muon_trig_stat_DownWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_stat_down")
muon_trig_sys_UpWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_sys_up")
muon_trig_sys_DownWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_sys_down")

#### electron related uncertainties acting on weights ####
#CFID
el_cf_UpWeights = replaceWeight(weights, "wel_nom", "wel_cid_up")
el_cf_DownWeights = replaceWeight(weights, "wel_nom", "wel_cid_down")
#ID
el_id_UpWeights =  replaceWeight(weights, "wel_nom", "wel_id_up")
el_id_DownWeights =  replaceWeight(weights, "wel_nom", "wel_id_down")
#Iso
el_iso_UpWeights =  replaceWeight(weights, "wel_nom", "wel_iso_up")
el_iso_DownWeights =  replaceWeight(weights, "wel_nom", "wel_iso_down")
#Rec
el_reco_UpWeights =  replaceWeight(weights, "wel_nom", "wel_reco_up")
el_reco_DownWeights =  replaceWeight(weights, "wel_nom", "wel_reco_down")
#Trig
el_trig_UpWeights =  replaceWeight(weights, "wtrig_nom", "wel_trig_up")
el_trig_DownWeights =  replaceWeight(weights, "wtrig_nom", "wel_trig_down")
                                        
## jet weight based systematics
                                    
jet_jvt_UpWeights = replaceWeight(weights, "wjet_nom", "wjet_jvt_up")
jet_jvt_DownWeights = replaceWeight(weights, "wjet_nom", "wjet_jvt_down")

# trigger weights
trig_UpWeights = replaceWeight(weights, "wtrig_nom", "wtrig_up")
trig_DownWeights = replaceWeight(weights, "wtrig_nom", "wtrig_down")

chflip_UpWeights = replaceWeight(weights, "wchflip_nom", "wchflip_up")
chflip_DownWeights = replaceWeight(weights, "wchflip_nom", "wchflip_down")

pu_UpWeights_bkg =  replaceWeight(weights, "wpu_nom_bkg", "wpu_up_bkg")
pu_DownWeights_bkg =  replaceWeight(weights, "wpu_nom_bkg", "wpu_down_bkg")

pu_UpWeights_sig = replaceWeight(weights, "wpu_nom_sig", "wpu_up_sig")
pu_DownWeights_sig =  replaceWeight(weights, "wpu_nom_sig", "wpu_down_sig")

#3L weights
syst_btagB_UP = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_B_up")
syst_btagB_DOWN = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_B_down")

syst_btagC_UP = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_C_up")
syst_btagC_DOWN = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_C_down")

syst_btagLight_UP = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_Light_up")
syst_btagLight_DOWN = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_Light_down")

syst_ftagExtrapo_UP = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_extrapolation_up")
syst_ftagExtrapo_DOWN = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_extrapolation_down")

syst_ftagExtrapoCharm_UP = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_extrapolationFromCharm_up")
syst_ftagExtrapoCharm_DN = replaceWeight(weights_3L,"bTagWeight","syst_FT_EFF_extrapolationFromCharm_down")

syst_jvt_UP = replaceWeight(weights_3L,"JVTSF","syst_jvtSF_up")
syst_jvt_DN = replaceWeight(weights_3L,"JVTSF","syst_jvtSF_down")

syst_ElecSF_ID_UP = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up")
syst_ElecSF_ID_DN = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down")

syst_ElecIsoSF_UP = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up")
syst_ElecIsoSF_DN = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up")

syst_ElecSF_Reco_UP = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up")
syst_ElecSF_Reco_DN = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down")

syst_ElecTrig_UP = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up")
syst_ElecTrig_DN = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down")

syst_ElecTrigEff_UP = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_TriggerEff_up")
syst_ElecTrigEff_DN = replaceWeight(weights_3L,"ElecSF","syst_EL_EFF_TriggerEff_down")

syst_MuEffStat_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_STAT_up")
syst_MuEffStat_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_STAT_down")

syst_MuEffSys_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_SYS_up")
syst_MuEffSys_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_SYS_down")

syst_MuEffTrigStat_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_TrigStatUncertainty_up")
syst_MuEffTrigStat_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_TrigStatUncertainty_down")

syst_MuEffTrigSys_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_TrigSystUncertainty_up")
syst_MuEffTrigSys_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_TrigSystUncertainty_down")

syst_MuEffStatLow_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_STAT_LOWPT_up")
syst_MuEffStatLow_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_STAT_LOWPT_down")

syst_MuEffSysLow_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_SYS_LOWPT_up")
syst_MuEffSysLow_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_EFF_SYS_LOWPT_down")

syst_MuIsoStat_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_ISO_STAT_up")
syst_MuIsoStat_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_ISO_STAT_down")

syst_MuIsoSys_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_ISO_SYS_up")
syst_MuIsoSys_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_ISO_SYS_down")

syst_MuTTVAStat_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_TTVA_STAT_up")
syst_MuTTVAStat_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_TTVA_STAT_down")

syst_MuTTVASys_UP = replaceWeight(weights_3L,"MuonSF","syst_MUON_TTVA_SYS_up")
syst_MuTTVASys_DN = replaceWeight(weights_3L,"MuonSF","syst_MUON_TTVA_SYS_down")


if "BTag" in SystList_SS:
    jet_b_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_b_up")
    jet_b_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_b_down")
    jet_c_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_c_up")
    jet_c_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_c_down")
    jet_light_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_light_up")
    jet_light_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_light_down")

    jet_extra1_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra1_up")
    jet_extra1_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra1_down")

    jet_extra2_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra2_up")
    jet_extra2_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra2_down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}

if useFakePrediction:
    sysfake_uncorr={}
    sysfake_corr={}
    statfake={}
    
for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    
for region in ['jet1','jet23']:    
    #adding dummy 35% syst and an overallSys  
#    basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.35,0.65,"user","userOverallSys"))
    if "TheoDummy" in SystList_SS:
        basicChanSyst[region].append(Systematic("TheoDummy",configMgr.weights,1.30,0.70,"user","userOverallSys"))
    #basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.15,0.85,"user","userOverallSys"))
   
    #Example systematic uncertainty
    if "JER" in SystList_SS: 
        basicChanSyst[region].append(Systematic("JER","_nom","_JET_JER_SINGLE_NP__1up","_nom","tree","overallNormHistoSysOneSideSym"))

    if "JES" in SystList_SS: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_nom","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_nom","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_nom","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET","_nom","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallHistoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_nom","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_nom","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_nom","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_nom","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_nom","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_nom","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6","_nom","_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_7","_nom","_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_8restTerm","_nom","_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree","overallNormHistoSys"))
	     
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_nom","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_nom","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_nom","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_nom","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_nom","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_nom","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_nom","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_nom","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_nom","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_nom","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","overallNormHistoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_nom","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","overallNormHistoSys"))

    	        
    if "MET" in SystList_SS:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_nom","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","overallNormHistoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_nom","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_nom","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_nom","_MET_SoftTrk_ResoPara","_nom","tree","overallNormHistoSysOneSideSym"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_nom","_MET_SoftTrk_ResoPerp","_nom","tree","overallNormHistoSysOneSideSym"))             
    if "LEP" in SystList_SS:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_nom","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","overallNormHistoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_nom","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID","_nom","_MUON_ID__1up","_MUON_ID__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS","_nom","_MUON_MS__1up","_MUON_MS__1down","tree","overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SCALE","_nom","_MUON_SCALE__1up","_MUON_SCALE__1down","tree","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_stat",configMgr.weights,muon_ttva_stat_UpWeights,muon_ttva_stat_DownWeights,"weight","overallHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_sys",configMgr.weights,muon_ttva_sys_UpWeights ,muon_ttva_sys_DownWeights, "weight","overallHistoSys"))

    if "LepEff" in SystList_SS:
        #basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muon_stat_UpWeights,muon_stat_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muon_sys_UpWeights,muon_sys_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_bad_stat",configMgr.weights,muon_bad_stat_UpWeights,muon_bad_stat_DownWeights,"weight","overallHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_bad_sys",configMgr.weights,muon_bad_sys_UpWeights,muon_bad_sys_DownWeights,"weight","overallHistoSys"))

        #basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muon_iso_stat_UpWeights,muon_iso_stat_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muon_iso_sys_UpWeights,muon_iso_sys_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muon_stat_lowpt_UpWeights,muon_stat_lowpt_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muon_sys_lowpt_UpWeights,muon_sys_lowpt_DownWeights,"weight","overallNormHistoSys"))

        #basicChanSyst[region].append(Systematic("EL_Cf",configMgr.weights,el_cf_UpWeights,el_cf_DownWeights,"weight","overallHistoSys"))
        basicChanSyst[region].append(Systematic("EG_Id",configMgr.weights,el_id_UpWeights,el_id_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,el_reco_UpWeights,el_reco_DownWeights,"weight","overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,el_iso_UpWeights,el_iso_DownWeights,"weight","overallNormHistoSys"))


    if "JVT" in SystList_SS:
        basicChanSyst[region].append(Systematic("JVT",configMgr.weights,jet_jvt_UpWeights,jet_jvt_DownWeights,"weight","overallNormHistoSys")) 

    if "pileup" in SystList_SS:
        basicChanSyst[region].append(Systematic("pileup_bkg",configMgr.weights,pu_UpWeights_bkg,pu_DownWeights_bkg,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("pileup_sig",configMgr.weights,pu_UpWeights_sig,pu_DownWeights_sig,"weight","overallNormHistoSys"))

    if "BTag" in SystList_SS:
        bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,jet_b_UpWeights,jet_b_DownWeights,"weight","overallNormHistoSys")
        cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,jet_c_UpWeights,jet_c_DownWeights,"weight","overallNormHistoSys")
        mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,jet_light_UpWeights,jet_light_DownWeights,"weight","overallNormHistoSys")
        eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,jet_extra1_UpWeights,jet_extra1_DownWeights,"weight","overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,jet_extra2_UpWeights,jet_extra2_DownWeights,"weight","overallNormHistoSys")

    if "Trigger" in SystList_SS:
        basicChanSyst[region].append(Systematic("MUON_TRIG_STAT",muon_trig_stat_UpWeights,muon_trig_stat_DownWeights,configMgr.weights,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TRIG_SYS",configMgr.weights,muon_trig_sys_UpWeights,muon_trig_sys_DownWeights,"weight","overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("TRIG",configMgr.weights,trig_UpWeights,trig_DownWeights,"weight","overallNormHistoSys"))

    if "ChFlip" in SystList_SS:
        basicChanSyst[region].append(Systematic("ChFlip",configMgr.weights,chflip_UpWeights,chflip_DownWeights,"weight","overallNormHistoSys"))

    #signal uncertainties                                                                                       
#    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
    
    #Generator Systematics
    generatorSyst = []  
    if "WZ" in SystList_SS:
        theoryUncertainties_SS_WZ.TheorUnc(generatorSyst)
    if "Rare" in SystList_SS:
        theoryUncertainties_SS_Rare.TheorUnc(generatorSyst)
    if "ttV" in SystList_SS:
        theoryUncertainties_SS_ttV.TheorUnc(generatorSyst)
    if "WW_ZZ" in SystList_SS:
        theoryUncertainties_SS_WW_ZZ.TheorUnc(generatorSyst)
        
        
        
        
#starting with 3L uncertainties:
if "JER" in SystList_3L: 
    basicChanSyst["3L"].append(Systematic("JER","_CENTRAL","_JER","_CENTRAL","tree","overallNormHistoSysOneSideSym"))
if "JES" in SystList_3L:     
    basicChanSyst["3L"].append(Systematic("JES_Group1","_CENTRAL","_JET_GroupedNP_1_UP","_JET_GroupedNP_1_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("JES_Group2","_CENTRAL","_JET_GroupedNP_2_UP","_JET_GroupedNP_2_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("JES_Group3","_CENTRAL","_JET_GroupedNP_3_UP","_JET_GroupedNP_3_DN","tree","overallNormHistoSys"))
if "MET" in SystList_3L: 
    basicChanSyst["3L"].append(Systematic("MET_SoftTrk_ResoPara","_CENTRAL","_MET_SoftTrk_ResoPara","_CENTRAL","tree","overallNormHistoSysOneSideSym"))
    basicChanSyst["3L"].append(Systematic("MET_SoftTrk_ResoPerp","_CENTRAL","_MET_SoftTrk_ResoPerp","_CENTRAL","tree","overallNormHistoSysOneSideSym"))
    basicChanSyst["3L"].append(Systematic("MET_SoftTrk","_CENTRAL","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"))
if "LEP" in SystList_3L: 
    basicChanSyst["3L"].append(Systematic("EG_RESOLUTION_ALL","_CENTRAL","_EG_RESOLUTION_ALL_UP","_EG_RESOLUTION_ALL_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("EG_SCALE_ALL","_CENTRAL","_EG_SCALE_ALL_UP","_EG_SCALE_ALL_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("MUON_ID","_CENTRAL","_MUONS_ID_UP","_MUONS_ID_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("MUON_MS","_CENTRAL","_MUONS_MS_UP","_MUONS_MS_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("MUON_SCALE","_CENTRAL","_MUONS_SCALE_UP","_MUONS_SCALE_DN","tree","overallNormHistoSys"))
    basicChanSyst["3L"].append(Systematic("MUON_TTVA_stat",configMgr.weights,syst_MuTTVAStat_UP,syst_MuTTVAStat_DN,"weight","overallHistoSys"))
    basicChanSyst["3L"].append(Systematic("MUON_TTVA_sys",configMgr.weights,syst_MuTTVASys_UP,syst_MuTTVASys_DN, "weight","overallHistoSys"))

if "pileup" in SystList_3L:   
    basicChanSyst["3L"].append(Systematic("pileup","_CENTRAL","_PRW_DATASF_UP","_PRW_DATASF_DN","tree","overallNormHistoSys"))
if "LepEff" in SystList_3L:
    basicChanSyst["3L"].append(Systematic("EG_Id",configMgr.weights,syst_ElecSF_ID_UP,syst_ElecSF_ID_DN,'weight','overallNormHistoSys'))
    print configMgr.weights
    print syst_ElecSF_ID_UP
    print syst_ElecSF_ID_DN
    #basicChanSyst["3L"].append(Systematic("EG_Iso",configMgr.weights,syst_ElecIsoSF_UP,syst_ElecIsoSF_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("EG_Reco",configMgr.weights,syst_ElecSF_Reco_UP,syst_ElecSF_Reco_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("MUON_Eff_stat",configMgr.weights,syst_MuEffStat_UP,syst_MuEffStat_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("MUON_Eff_sys",configMgr.weights,syst_MuEffSys_UP,syst_MuEffSys_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,syst_MuEffStatLow_UP,syst_MuEffStatLow_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,syst_MuEffSysLow_UP,syst_MuEffSysLow_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,syst_MuIsoStat_UP,syst_MuIsoStat_DN,'weight','overallNormHistoSys'))
    #basicChanSyst["3L"].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,syst_MuIsoSys_UP,syst_MuIsoSys_DN,'weight','overallNormHistoSys'))

if "JVT" in SystList_3L:
    basicChanSyst["3L"].append(Systematic("JVT",configMgr.weights,syst_jvt_UP,syst_jvt_DN,"weight","overallNormHistoSys"))         

if "BTag" in SystList_3L:
    bTagSyst["3L"]      = Systematic("btag_BT",configMgr.weights,syst_btagB_UP,syst_btagB_DOWN,"weight","overallNormHistoSys")
    cTagSyst["3L"]      = Systematic("btag_CT",configMgr.weights,syst_btagC_UP,syst_btagC_DOWN,"weight","overallNormHistoSys")
    mTagSyst["3L"]      = Systematic("btag_LightT",configMgr.weights,syst_btagLight_UP,syst_btagLight_DOWN,"weight","overallNormHistoSys")
    eTagSyst["3L"]      = Systematic("btag_Extra",configMgr.weights,syst_ftagExtrapo_UP,syst_ftagExtrapo_DOWN,"weight","overallNormHistoSys")
    eTagFromCSyst["3L"] = Systematic("btag_ExtraFromCharm",configMgr.weights,syst_ftagExtrapoCharm_UP,syst_ftagExtrapoCharm_DN,"weight","overallNormHistoSys")

if "Trigger" in SystList_3L:
    basicChanSyst["3L"].append(Systematic("MUON_TRIG_STAT",configMgr.weights,syst_MuEffTrigStat_UP,syst_MuEffTrigStat_DN,'weight','overallNormHistoSys'))
    basicChanSyst["3L"].append(Systematic("MUON_TRIG_SYS",configMgr.weights,syst_MuEffTrigSys_UP,syst_MuEffTrigSys_DN,'weight','overallNormHistoSys'))
    basicChanSyst["3L"].append(Systematic("TRIG",configMgr.weights,syst_ElecTrigEff_UP,syst_ElecTrigEff_DN,'weight','overallNormHistoSys'))    

if "ChFlip" in SystList_3L:
        basicChanSyst["3L"].append(Systematic("ChFlip",configMgr.weights,chflip_UpWeights,chflip_DownWeights,"weight","overallNormHistoSys"))    

# ********************************************************************* #
#                              Background samples
# ********************************************************************* #

configMgr.nomName = "_nom"

SS_samples=[]
samples_3L=[]

WWSampleName = "WW"
WWSample = Sample(WWSampleName,kOrange-8)
WWSample.setStatConfig(useStat)
WWSample.setNormByTheory()
WWSample.setFileList(files_em['WW'])
SS_samples.append(WWSample)

WZSampleName = "WZ"
WZSample = Sample(WZSampleName,kOrange-8)
WZSample.setStatConfig(useStat)
if doCombination:
    WZSample.setNormFactor("mu_WZ",1.,0.,5.)
    WZSample.setNormRegions([("CR3L_onZ_highMET","cuts")])
else: WZSample.setNormByTheory()
WZSample.setFileList(files_em['WZ'])
SS_samples.append(WZSample)

ZZSampleName = "ZZ"
ZZSample = Sample(ZZSampleName,kOrange-8)
ZZSample.setStatConfig(useStat)
ZZSample.setNormByTheory()
ZZSample.setFileList(files_em['ZZ'])
SS_samples.append(ZZSample)

ttbarVSampleName = "ttV"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
ttbarVSample.setFileList(files_em['ttV'])
SS_samples.append(ttbarVSample)

RareSampleName = "Rare"
RareSample = Sample(RareSampleName,kGreen-8)
RareSample.setStatConfig(useStat)
RareSample.setNormByTheory()
RareSample.setFileList(files_em['Rare'])
SS_samples.append(RareSample)

#data sample for later
DataSample = Sample("data_nom",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()
SS_samples.append(DataSample)

if useFakePrediction:
    FakeSample = Sample("Fakes",kViolet-9)
    FakeSample.setStatConfig(False)
    FakeSyst.addFakeCount(FakeSample)
    #    FakeSample.setFileList(files_em['Fakes'])
if useDataDrivenCF:
    ChargeFlipSample = Sample("ChargeFlip",kYellow)
    ChargeFlipSample.setStatConfig(False)
    ChargeFlipSample.setFileList(files_em['ChargeFlip'])
    ChargeFlipSample.addWeight("qFwt")
    
#3L samples:
wzdibosonSample_3L=Sample("WZ", kPink)
wzdibosonSample_3L.setStatConfig(useStat)
wzdibosonSample_3L.setNormFactor("mu_WZ",1.,0.,5.)
wzdibosonSample_3L.setNormRegions([("CR3L_onZ_highMET","cuts")])
#if not noSysts and doThUncert: wzdibosonSample.addSystematic(syst_VVxsec)
wzdibosonSample_3L.setFileList(files_3L['background'])
wzdibosonSample_3L.setSuffixTreeName("_CENTRAL")
samples_3L.append(wzdibosonSample_3L)


zzdibosonSample_3L=Sample("ZZ", kGreen)
zzdibosonSample_3L.setStatConfig(useStat)
zzdibosonSample_3L.setNormByTheory()
#if not noSysts and doThUncert: zzdibosonSample.addSystematic(syst_VVxsec)
zzdibosonSample_3L.setFileList(files_3L['background'])
zzdibosonSample_3L.setSuffixTreeName("_CENTRAL")
samples_3L.append(zzdibosonSample_3L)

higgsSample_3L=Sample("Higgs", kPink)
higgsSample_3L.setStatConfig(useStat)
higgsSample_3L.setNormByTheory()
#if not noSysts and doThUncert: higgsSample.addSystematic(syst_ttHxsec)
higgsSample_3L.setFileList(files_3L['background'])
higgsSample_3L.setSuffixTreeName("_CENTRAL")
samples_3L.append(higgsSample_3L)

ttVSample_3L=Sample("ttV", kOrange)
ttVSample_3L.setStatConfig(useStat)
ttVSample_3L.setNormByTheory()
ttVSample_3L.setFileList(files_3L['background'])
ttVSample_3L.setSuffixTreeName("_CENTRAL")
#if not noSysts and doThUncert: ttVSample.addSystematic(syst_ttWxsec)
#if not noSysts and doThUncert: ttVSample.addSystematic(syst_ttZxsec)
#if not noSysts and doThUncert: ttVSample.addSystematic(syst_ttWWxsec)
#if not noSysts and doThUncert: ttVSample.addSystematic(syst_tZxsec)
samples_3L.append(ttVSample_3L)

tribosonSample_3L=Sample("VVV", kBlue+2)
tribosonSample_3L.setStatConfig(useStat)
tribosonSample_3L.setNormByTheory()
tribosonSample_3L.setFileList(files_3L['background'])
tribosonSample_3L.setSuffixTreeName("_CENTRAL")
#if not noSysts and doThUncert: tribosonSample.addSystematic(syst_VVVxsec)
samples_3L.append(tribosonSample_3L)

multitopSample_3L=Sample("MultiT", kBlue-9)
multitopSample_3L.setStatConfig(useStat)
multitopSample_3L.setNormByTheory()
multitopSample_3L.setFileList(files_3L['background'])
multitopSample_3L.setSuffixTreeName("_CENTRAL")
#multitopSample.buildHisto([0.01], "SR3LSFOS0ja", "cuts")
samples_3L.append(multitopSample_3L)

fakeSample_3L=Sample("Fakes",kMagenta+6)
fakeSample_3L.setNormByTheory()
fakeSample_3L.setStatConfig(False)
fakeSample_3L.addSampleSpecificWeight("1/36.1")
fakeSample_3L.setFileList(files_3L['fakes'])
fakeSample_3L.setSuffixTreeName("_CENTRAL")
samples_3L.append(fakeSample_3L)

# Data setup ----------------------------------------------------------------------------------------------------

dataSample_3L = Sample("Data_CENTRAL",kBlack)
dataSample_3L.setData()
dataSample_3L.setFileList(files_3L['data'])
samples_3L.append(dataSample_3L)
    
    
# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("BkgOnly")

#creating now list of samples
samplelist=[]
    
for sample in SS_samples:
    for weight in weights:
         sample.addSampleSpecificWeight(weight)
    sample.addSampleSpecificWeight("(isTruthLep1==1&&isTruthLep2==1)")
    
for sample in samples_3L:
    for weight in weights_3L:
        sample.addSampleSpecificWeight(weight)
if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.032) ## new recommendations for Moriond 2017 from February 2017
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
SS_channels = []
channels_3L = []
for region in CRregions:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################


if '3L' in CRregions: 
        CR3LonZ = appendTo(bkgOnly.addChannel("cuts",["CR3L_onZ_highMET"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L]) 
        bkgOnly.addBkgConstrainChannels(CR3LonZ)
        
        
        
if myFitType==FitType.Background:    
    validation = configMgr.addFitConfigClone(bkgOnly,"Validation")
    for c in validation.channels:
       for region in CRregions:
           appendIfMatchName(c,bReqChans[region])
           appendIfMatchName(c,bVetoChans[region])
           appendIfMatchName(c,bAgnosticChans[region])
           appendIfMatchName(c,elChans[region])
           appendIfMatchName(c,muChans[region])      
           appendIfMatchName(c,elmuChans[region])
       appendIfMatchName(c,channels_3L)
           
    if "jet1" in CRregions:
        appendTo(validation.addValidationChannel("cuts",["VRjet1"],1,0.5,1.5),[elmuChans["jet1"],bVetoChans["jet1"],SS_channels]) 
        appendTo(validation.addValidationChannel("cuts",["SRjet1"],1,0.5,1.5),[elmuChans["jet1"],bVetoChans["jet1"],SS_channels]) 
    if "jet23" in CRregions:
        appendTo(validation.addValidationChannel("cuts",["VRjet23"],1,0.5,1.5),[elmuChans["jet23"],bVetoChans["jet23"],SS_channels])
        appendTo(validation.addValidationChannel("cuts",["SRjet23"],1,0.5,1.5),[elmuChans["jet23"],bVetoChans["jet23"],SS_channels])
        
    if "3L" in CRregions:
        appendTo(validation.addValidationChannel("cuts",["VR3L_offZ_highMET"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
        appendTo(validation.addValidationChannel("cuts",["SR3LDFOS0j"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
        appendTo(validation.addValidationChannel("cuts",["SR3LDFOS1ja"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
        appendTo(validation.addValidationChannel("cuts",["SR3LDFOS1jb"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
        appendTo(validation.addValidationChannel("cuts",["SR3LSFOS0ja"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
        appendTo(validation.addValidationChannel("cuts",["SR3LSFOS0jb"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
        appendTo(validation.addValidationChannel("cuts",["SR3LSFOS1J"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])       
    
if myFitType==FitType.Exclusion:
    
    SR_channels = {}       
        
    for sig in sigSamples:
        
        #need to translate naming into 3L - naming SS: C1N2_Wh_hall_202p5_72p5
        signame_3L="SMAwh13TeV_"+sig.split('_')[-2].split('p')[0]+"_"+sig.split('_')[-1].split('p')[0]
        name = sig.split('_')[-2].split('p')[0]+"_"+sig.split('_')[-1].split('p')[0]
        SR_channels[sig] = []
        
        myTopLvl = configMgr.addFitConfigClone(bkgOnly,"Sig_%s"%name)
        for c in myTopLvl.channels:
            for region in ['jet1','jet23','3L']:
                appendIfMatchName(c,bReqChans[region])
                appendIfMatchName(c,bVetoChans[region])
                appendIfMatchName(c,bAgnosticChans[region])
                appendIfMatchName(c,elChans[region])
                appendIfMatchName(c,muChans[region])
                appendIfMatchName(c,elmuChans[region])
            appendIfMatchName(c,channels_3L)
            appendIfMatchName(c,SS_channels)
    
    
        sigSample = Sample("Sig_"+name,kPink)    
        sigSample.setStatConfig(useStat)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
         
        if not doHistoBuilding:
            myTopLvl.addSamples(sigSample)
            myTopLvl.setSignalSample(sigSample)

        if "jet1" in CRregions:
            tmp = appendTo(myTopLvl.addChannel("cuts",["SRjet1"],1,0.5,1.5),[elmuChans["jet1"],bVetoChans["jet1"],SS_channels])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp)
        
        if "jet23" in CRregions:
            tmp = appendTo(myTopLvl.addChannel("cuts",["SRjet23"],1,0.5,1.5),[elmuChans["jet23"],bVetoChans["jet23"],SS_channels])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp)
            
        if "3L" in CRregions:
            tmp = appendTo(myTopLvl.addChannel("cuts",["SR3LDFOS0j"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp)            

            tmp = appendTo(myTopLvl.addChannel("cuts",["SR3LDFOS1ja"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp) 
            
            tmp = appendTo(myTopLvl.addChannel("cuts",["SR3LDFOS1jb"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp) 
            
            tmp = appendTo(myTopLvl.addChannel("cuts",["SR3LSFOS0ja"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp)  
            
            tmp = appendTo(myTopLvl.addChannel("cuts",["SR3LSFOS0jb"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp) 
            
            tmp = appendTo(myTopLvl.addChannel("cuts",["SR3LSFOS1J"],1,0.5,1.5),[elmuChans["3L"],bVetoChans["3L"],channels_3L])
            myTopLvl.setSignalChannels(tmp)
            SR_channels[sig].append(tmp)                   

###################################################################################################################
# Now adding samples - need to do this after adding channels, as we need to add different samples per channel.....#
###################################################################################################################
samplelist = SS_samples
samplelist.append(FakeSample)  
samplelist.append(ChargeFlipSample)    

#ok, now start to add the SS samples to the SS regions...
for channel in SS_channels:
    for sample in samplelist:
        channel.addSample(sample)
        
#data driven charge flip systematics
#DDcf_UpWeights = replaceWeight(bkgOnly.getSample("ChargeFlip").weights , "qFwt", "qFwt_sys_1up")
#DDcf_DownWeights = replaceWeight(bkgOnly.getSample("ChargeFlip").weights , "qFwt", "qFwt_sys_1dn")
        
        
#now we need to do the same for 3L  
for channel in channels_3L:
    for sample in samples_3L:
        channel.addSample(sample)
        
 # ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input trees)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]       
        
        
#postprocessing for exclusion fit
if myFitType==FitType.Exclusion and not doHistoBuilding:
    for sig in sigSamples:
        name = sig.split('_')[-2].split('p')[0]+"_"+sig.split('_')[-1].split('p')[0]
        signame_3L="SMAwh13TeV_"+sig.split('_')[-2].split('p')[0]+"_"+sig.split('_')[-1].split('p')[0]
        myTopLvl=configMgr.getFitConfig("Sig_%s"%name)        
        for chan in myTopLvl.channels:
            theSample = chan.getSample("Sig_"+name)
                
            if chan in elmuChans_all and chan in SS_channels:
                theSample.setFileList(sigFiles_em[sig])
                for weight in weights:
                    theSample.addSampleSpecificWeight(weight)
                theSample.setPrefixTreeName(sig)  
            elif chan in channels_3L:    
                theSample.setSuffixTreeName("_CENTRAL")
                theSample.setPrefixTreeName(signame_3L)
                theSample.setFileList(files_3L['signal'])
                for weight in weights_3L:
                    theSample.addSampleSpecificWeight(weight)                
            else:
                raise ValueError("Unexpected channel name %s"%(chan.name))


# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan
        
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
 	#print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
	    #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in CRregions:
        print basicChanSyst[region]
        SetupChannels(elChans[region],basicChanSyst[region])
        SetupChannels(muChans[region],basicChanSyst[region])
        SetupChannels(elmuChans[region],basicChanSyst[region])
#

                

# b-tag reg/veto/agnostic channels


for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList_SS+SystList_3L and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])	    

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList_SS+SystList_3L and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")

    if useFakePrediction or useDataDrivenCF:
            if "FakeDummy" in SystList_SS and useFakePrediction and not "Fake" in SystList_SS:
                chan.getSample("Fakes").addSystematic(Systematic("FakeDummy",configMgr.weights,1.30,0.70,"user","userOverallSys"))
            if "DDChargeFlip" in SystList_SS and useDataDrivenCF:
                chan.getSample("ChargeFlip").addSystematic(Systematic("DDChargeFlip", bkgOnly.getSample("ChargeFlip").weights, DDcf_UpWeights, DDcf_DownWeights,"weight","overallNormHistoSys"))
            if "Fake" in SystList_SS and useFakePrediction and not "FakeDummy" in SystList_SS:
                FakeSyst.genFakeSyst(sysfake_corr,sysfake_uncorr,statfake)
                channelname = chan.channelName[:7] if (len(chan.channelName) > 11) else chan.channelName[:6]
                chan.getSample("Fakes").addSystematic(sysfake_corr[channelname])
                chan.getSample("Fakes").addSystematic(sysfake_uncorr[channelname])
                chan.getSample("Fakes").addSystematic(statfake[ channelname])
                
      
                                                                        
                                                


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
if not useFakePrediction:
    # entry = leg.AddEntry("","t#bar{t}","lf")
    # entry.SetLineColor(kGreen-9)
    # entry.SetFillColor(kGreen-9)
    # entry.SetFillStyle(compFillStyle)
    # #
    # entry = leg.AddEntry("","W+jets","lf")
    # entry.SetLineColor(kAzure-4)
    # entry.SetFillColor(kAzure-4)
    # entry.SetFillStyle(compFillStyle)
    # #
    # entry = leg.AddEntry("","Single Top","lf")
    # entry.SetLineColor(kGreen-5)
    # entry.SetFillColor(kGreen-5)
    # entry.SetFillStyle(compFillStyle)
    # #
    # entry = leg.AddEntry("","Z+jets","lf")
    # entry.SetLineColor(ZSample.color)
    # entry.SetFillColor(ZSample.color)
    # entry.SetFillStyle(compFillStyle)
    # #
    entry = leg.AddEntry("","t#bar{t}V","lf")
    entry.SetLineColor(kGreen-9)
    entry.SetFillColor(kGreen-9)
    entry.SetFillStyle(compFillStyle)

else:
    entry = leg.AddEntry("","Fakes","lf")
    entry.SetLineColor(FakeSample.color)
    entry.SetFillColor(FakeSample.color)
    entry.SetFillStyle(compFillStyle)
    

# entry = leg.AddEntry("","Diboson","lf")
# entry.SetLineColor(DibosonsSample.color)
# entry.SetFillColor(DibosonsSample.color)
# entry.SetFillStyle(compFillStyle)
#
# entry = leg.AddEntry("","Multi Top","lf")
# entry.SetLineColor(MultiTopSample.color)
# entry.SetFillColor(MultiTopSample.color)
# entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","ChargeFlip","lf")
#entry.SetLineColor(ChargeFlipSample.color)
#entry.SetFillColor(ChargeFlipSample.color)
#entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
# entry = leg.AddEntry("","Higgs","lf")
# entry.SetLineColor(HiggsSample.color)
# entry.SetFillColor(HiggsSample.color)
# entry.SetFillStyle(compFillStyle)
# #
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg

c.Close()
MeffBins = [ '1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    #chan.titleX="m^{incl}_{eff} [GeV]"
    

#if myFitType==FitType.Exclusion:
#    for sig in sigSamples:
#        for chan in SR_channels[sig]:
#            chan.titleY = "Events"
#            chan.minY = 0.05 
#            chan.maxY = 80
#            chan.ATLASLabelX = 0.125
#            chan.ATLASLabelY = 0.85
#            chan.ATLASLabelText = "Internal"
#            chan.showLumi = True

#configMgr.fitConfigs.remove(bkgOnly)
