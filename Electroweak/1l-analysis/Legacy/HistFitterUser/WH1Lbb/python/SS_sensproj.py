################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed,kViolet
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import commands
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys
import fnmatch
print os.getcwd()
import FakeSyst 
import theoryUncertainties_SS_WZ
import theoryUncertainties_SS_Rare
import theoryUncertainties_SS_ttV
import theoryUncertainties_SS_WW_ZZ
#import Winter1617.SignalAccUncertainties

from logger import Logger
log = Logger('SS')


# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels,systList):
    for chan in channels:
        #chan.setFileList(bgdFiles)
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not  - useful to see when to load input trees from eos or from a local directory
onLxplus='lx' in commands.getstatusoutput("hostname")[1] or 'vm' in commands.getstatusoutput("hostname")[1] or '.cern.ch' in commands.getstatusoutput("hostname")[1]
onDOL= False
if onDOL:    onLxplus=False  ## previous status does not detect the batch system in lxplus

debug = False #!!!

if onDOL:  debug = True  ## for test

if debug:
    print "WARNING: Systematics disabled for testing purposes !!!"

# here we have the possibility to activate different groups of systematic uncertainties
SystList=[]

SystList.append("JER")      # Jet Energy Resolution (common)
SystList.append("JES")      # Jet Energy Scale (common)
SystList.append("MET")      # MET (common)
SystList.append("LEP")      # lepton uncertainties (common)
SystList.append("LepEff")   # lepton scale factors (common)
SystList.append("Trigger")  # trigger uncertainties
SystList.append("ChFlip")   # charge flip uncertainties
SystList.append("JVT")      # JVT scale factors
SystList.append("pileup")   # pileup (common)
SystList.append("BTag")     # b-tagging uncertainties
# #SystList.append("TheoDummy") # Dummy uncertainty as approximation for the theoretical uncertainties
SystList.append("Fake")     # Correlated and uncorrelated fake estimation uncertainties
SystList.append("DDChargeFlip")  #Charge flip uncertainties from likelihood method
SystList.append("WZ")       # generator uncertainties for WZ  
SystList.append("Rare")
SystList.append("ttV")
SystList.append("WW_ZZ")
# # SystList.append("FakeDummy")


if debug:
    SystList=[]
    
#for creating the backup chache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False
if onDOL: doHistoBuilding = False

# Toggle N-1 plots
doNMinus1Plots=False

# Use reduced JES systematics
useReducedJESNP=True

useNJetNormFac=False
# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal=True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["jet1", "jet23"]

# Tower selected from command-line
# pickedSRs is set by the "-r" HistFitter option    
try:
    pickedSRs
except NameError:
    pickedSRs = None
    
if pickedSRs != None and len(pickedSRs) >= 1: 
    CRregions = pickedSRs
    print "\n Tower defined from command line: ", pickedSRs,"     (-r jet1, jet23 option)"

#activate associated validation regions:
ValidRegList={}
#for plotting (turn to True if you want to use them):
ValidRegList["jet1"] = False
ValidRegList["jet23"] = False


# for tables (turn doTableInputs to True)
doTableInputs = False #This effectively means no validation plots but only validation tables (but is 100x faster)
if onDOL:  doTableInputs = False
for cr in CRregions:
    if "LM" in cr and doTableInputs:
        ValidRegList["LM"] = True
    if "MM" in cr and doTableInputs:
        ValidRegList["MM"] = True	
    if "HM" in cr and doTableInputs:
        ValidRegList["HM"] = True

#N-1 plots in SRs        
VRplots=False

# choose which kind of fit you want to perform: ShapeFit(True) or NoShapeFit(False) 
doShapeFit=True

# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    
    sigSamples=["C1N2_Wh_hall_175p0_0p0"]
    
    
# define the analysis name:
analysissuffix = ''

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr
    
analysissuffix_BackupCache = analysissuffix
'''    
if myFitType==FitType.Exclusion:
    if onDOL:
        analysissuffix += '_'+ sigSamples[0]
    if 'GG_oneStep' in sigSamples[0]  and not onDOL:
        analysissuffix += '_GG_oneStep'
    if 'SS_oneStep' in sigSamples[0]  and not onDOL:
        if onMOGON: analysissuffix += '_SS_oneStep'
        else: analysissuffix += '_' + sigSamples[0]
'''		

#mylumi= 36.06596#36.46161#14.78446 #30 #13.27766 #13.78288 #9.49592 #8.31203 #6.74299 #6.260074  #5.515805   # @0624 mylumi=3.76215 #3.20905   #3.31668  #3.34258  
mylumi= 300.0
#mylumi=3000.
#mysamples = ["triboson", "higgs", "zjets","ttv","singletop","diboson","wjets","ttbar","vgamma", "multitop", "drellyan","data"]
mysamples = ["Rare","ttV","WW","WZ","ZZ","data"]
doOnlySignal=False
useFakePrediction= True
useDataDrivenCF=True

#check for a user argument given with -u
myoptions=configMgr.userArg
analysisextension=""
if myoptions!="":
    if 'sensitivity' in myoptions: #userArg should be something like 'sensitivity_3'
        mylumi=float(myoptions.split('_')[-1])
        analysisextension += "_"+myoptions
	
    if myoptions=='doOnlySignal': 
        doOnlySignal=True
        analysisextension += "_onlysignal"
    if 'samples' in myoptions: #userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split("_"):
            if not sam is 'samples': 
                mysamples.append(sam)
        analysisextension += "_"+myoptions
else:
    print "No additional user arguments given - proceed with default analysis!"
    
print " using lumi:", mylumi
        
# First define HistFactory attributes
configMgr.analysisName = "SS_sens"+analysissuffix+analysisextension
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

#activate using of background histogram cache file to speed up processes
#if myoptions!="":

useBackupCache = True
useBackupJeanette = False # True to choose etp3/jlorenz backup file, False for etp1/dkoeck

if (onLxplus) and not myFitType==FitType.Discovery:
    useBackupCache = False #!!!
if onDOL:
    useBackupCache = False
if useBackupCache:
    configMgr.useCacheToTreeFallback = useBackupCache # enable the fallback to trees
    configMgr.useHistBackupCacheFile = useBackupCache # enable the use of an alternate data file
    MyAnalysisName_BackupCache = "SS_sens"+analysissuffix_BackupCache+analysisextension
    if not onLxplus:
        if useBackupJeanette:
            histBackupCacheFileName = "/project/etp3/jlorenz/shape_fit/HistFitter_January2017/HistFitterUser/WH1Lbb/data/backup_SS.root"
        else:
            histBackupCacheFileName = "/project/etp1/dkoeck/HistFitter/data/" + "SS_sens_jet1_jet23.root"
    #if not onLxplus: histBackupCacheFileName = "data/OneLepton_6J_GG_oneStep_samples_Wjets.root"
    else: histBackupCacheFileName = "data/OneLepton_2J_4Jhighx_4Jlowx_6J_GG_oneStep.root"
    if onDOL:    histBackupCacheFileName = "/publicfs/atlas/atlasnew/SUSY/users/xuda/Analysis2016/HISTFITTER/HistFitterUser/MET_jets_leptons/data/"+MyAnalysisName_BackupCache+"_BackupCache.root"
    print " using backup cache file: "+histBackupCacheFileName
    #the data file of your background fit (= the backup cache file) - set something meaningful if turning useCacheToTreeFallback and useHistBackupCacheFile to True
    configMgr.histBackupCacheFile = histBackupCacheFileName

#configMgr.histBackupCacheFile ="/data/vtudorac/HistFitterTrunk/HistFitterUser/MET_jets_leptons/python/ICHEP/CacheFiles/OneLepton_6JGGx12ShapeFit.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 36.1 #input lumi 1 pb-1 ^= normalization of the HistFitter trees
configMgr.outputLumi =  mylumi # for test
configMgr.setLumiUnits("fb-1") #Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1.

if onDOL:
    configMgr.fixSigXSec=False
else:
    configMgr.fixSigXSec=True

useToys = False #!!!
if useToys:
    configMgr.calculatorType=0  #frequentist calculator (uses toys)
    configMgr.nTOYs=10000       #number of toys when using frequentist calculator
    print " using frequentist calculator (toys)"
else:
    configMgr.calculatorType=2  #asymptotic calculator (creates asimov data set for the background hypothesis)
    print " using asymptotic calculator"
configMgr.testStatType=3        #one-sided profile likelihood test statistics
configMgr.nPoints=20            #number of values scanned of signal-strength for upper-limit determination of signal strength.

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

#writing xml files for debugging purposes
configMgr.writeXML = True

#blinding of various regions
configMgr.blindSR = True # Blind the SRs (default is False)
configMgr.blindCR = True # Blind the CRs (default is False)
configMgr.blindVR = True # Blind the VRs (default is False)
doBlindSRinBGfit  = True # Blind SR when performing a background fit

#useSignalInBlindedData=True
configMgr.ReduceCorrMatrix=True

#we have only signal regions in this config file, make sure this is discovered
configMgr.keepSignalRegionType = True

#using of statistical uncertainties?
useStat=True

#Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
FullSimSig = []

# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

inputDir=""
inputDirData=""
inputDirSig=""

if not onLxplus:
    inputDir="/project/etp1/dkoeck/ntuples/HF_trees/"
    inputDirSig="/project/etp1/dkoeck/ntuples/HF_trees/"
    inputDirData="/project/etp1/dkoeck/ntuples/HF_trees/"

#background files, separately for electron and muon channel:


files_em={}
#files_em['ttV']=[inputDir + "background.36100.root"]
bkgr_file_mctruth = "TruthIncl/background.36100.inclTruth.wOMissing.no363492.root"
bkgr_file_mctruth_twz ="TruthIncl/background.36100.inclTruth.inclMissingtWZ.no363492.root"
bkgr_file_mctruth_twz_th = "TruthIncl/background.36100.inclTruth.tWZ.tH.root"
bkgr_file = "background_complete_MuFix_36100.root"
bkgr_updatedVVV_mctruth = "TruthIncl/background.36100.updatedVVVMcTruth_incltHtWZ.root"
files_em['ttV']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['Rare']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['WW']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['WZ']=[inputDir + bkgr_updatedVVV_mctruth]
files_em['ZZ']=[inputDir + bkgr_updatedVVV_mctruth]


if useFakePrediction:
#    files_em['Fakes']=["/project/etp1/dkoeck/ntuples/Fakes_Charge.root"]
    files_em['Fakes']=["/project/etp1/dkoeck/ntuples/HF_trees/Fakes_update_130318_230118_nom.root"]
if useDataDrivenCF:
    files_em['ChargeFlip']=["/project/etp1/dkoeck/ntuples/CF/ChargeFlip.root"]

#Signal files
sigFiles_e={}
sigFiles_m={}
sigFiles_em={}
sig_extension=["C1N2_Wh_hall_202p5_72p5","C1N2_Wh_hall_212p5_12p5","C1N2_Wh_hall_212p5_37p5","C1N2_Wh_hall_212p5_62p5","C1N2_Wh_hall_215p0_85p0","C1N2_Wh_hall_225p0_0p0","C1N2_Wh_hall_225p0_25p0","C1N2_Wh_hall_225p0_50p0","C1N2_Wh_hall_225p0_75p0","C1N2_Wh_hall_237p5_12p5","C1N2_Wh_hall_237p5_37p5","C1N2_Wh_hall_237p5_62p5","C1N2_Wh_hall_237p5_87p5","C1N2_Wh_hall_250p0_0p0","C1N2_Wh_hall_250p0_25p0","C1N2_Wh_hall_250p0_50p0"]

for sigpoint in sigSamples:
    if sigpoint in sig_extension:       
        sigFiles_e[sigpoint]=[inputDirSig+"C1N2_Wh_hall_ext36100.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"C1N2_Wh_hall_ext36100.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"C1N2_Wh_hall_ext36100.root"]
    else:   
        sigFiles_e[sigpoint]=[inputDirSig+"C1N2_Wh_hall_36100.root"]
        sigFiles_m[sigpoint]=[inputDirSig+"C1N2_Wh_hall_36100.root"]
        sigFiles_em[sigpoint] = [inputDirSig+"C1N2_Wh_hall_36100.root"]
        
#data files
dataFiles = [inputDirData+"TruthIncl/data.36100.root"] # need to check lumierror, exact lumi value

# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

#first part: common selections, SR, CR, VR
CommonSelection = "&&NlepBL==2 && nSigLep==2 && nBJets20==0"
#CommonSelection = "&&  nLep_base==2 && nLep_signal==2 && ((lep1Charge==1 && lep2Charge==1) || (lep1Charge==-1 && lep2Charge==-1)) "

EleEleSelection = "&& (AnalysisType==1 && AnalysisType_lep2==1) "
MuoMuoSelection = "&& (AnalysisType==2 && AnalysisType_lep2==2)"
EleMuoSelection = "&& ( (AnalysisType==1 && AnalysisType_lep2==2) || (AnalysisType==2 && AnalysisType_lep2==1))"
Zmassveto       = "&& ((mll - 91.2)>10 || (mll-91.2)<-10)"
Zmasspeak       = "&& ( (((mll - 91.2)<=10) && (mll - 91.2)>=0) || (((mll - 91.2)>=-10 && (mll - 91.2)<=0)))"

# ------- SR regions --------------------------------------------------------------------------- #
SRSelection = "Pt_l>=25000 && Pt_subl>=25000"

#configMgr.cutsDict["SRjet1"] = SRSelection + "&&nJets20==1 &&DeltaEtaLep<=1.5 && met>=100000 && mt>=140000 && meff>=260000.&& mljj_comb<180000 &&MT2>=80000" + CommonSelection 
#configMgr.cutsDict["SRjet23"] = SRSelection +  "&& (nJets20==2||nJets20==3) && met>=100000 && mt>=120000 && meff>=240000.&& mljj_comb<130000 &&MT2>=70000" + CommonSelection

configMgr.cutsDict["SRjet1"] = SRSelection + "&&nJets20==1 &&DeltaEtaLep<=1.5 && met>=100000 && mt>=140000 && meff>=260000.&& mljj_comb<180000 &&MT2>=80000" + "&&NlepBL>=2 && nSigLep>=2 && nBJets20==0"
configMgr.cutsDict["SRjet23"] = SRSelection +  "&& (nJets20==2||nJets20==3) && met>=100000 && mt>=120000 && meff>=240000.&& mljj_comb<130000 &&MT2>=70000" + "&&NlepBL>=2 && nSigLep>=2 && nBJets20==0"

#---------VR regions --------------------------------------------------------------------------- #
configMgr.cutsDict["VRjet1"] = SRSelection + "&&nJets20==1 &&DeltaEtaLep<1.5 && met<=100000 && met>=70000 && mt>=140000 && mljj_comb>130000" + CommonSelection
configMgr.cutsDict["VRjet23"] = SRSelection + "&& (nJets20==2||nJets20==3) && met>=100000 && mt<=120000 && mt>=65000 && meff>=240000.&& mljj_comb>130000 " + CommonSelection


# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #


#all the weights we need for a default analysis - add b-tagging weight later below
#weights=["wmu_nom","wtrig_nom","wel_nom","wjet_nom","wchflip_nom","lumiScaling","wpu_nom_bkg","wpu_nom_sig"]
weights=["mcweight","wmu_nom","wtrig_nom","wel_nom","wjet_nom","wchflip_nom","lumiScaling","wpu_nom_bkg","wpu_nom_sig"]

configMgr.weights = weights

#muon related uncertainties acting on weights
muon_bad_stat_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_stat_up")
muon_bad_stat_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_stat_down")
muon_bad_sys_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_sys_up")
muon_bad_sys_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_bad_sys_down")

muon_stat_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_up")
muon_stat_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_down")
muon_sys_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_up")
muon_sys_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_down")

muon_stat_lowpt_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_lowpt_up")
muon_stat_lowpt_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_stat_lowpt_down")
muon_sys_lowpt_UpWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_lowpt_up")
muon_sys_lowpt_DownWeights = replaceWeight(weights, "wmu_nom", "wmu_sys_lowpt_down")

muon_iso_stat_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_stat_up")
muon_iso_stat_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_stat_down")
muon_iso_sys_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_sys_up")
muon_iso_sys_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_iso_sys_down")

muon_ttva_stat_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_stat_up")
muon_ttva_stat_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_stat_down")
muon_ttva_sys_UpWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_sys_up")
muon_ttva_sys_DownWeights =  replaceWeight(weights, "wmu_nom", "wmu_ttva_sys_down")

muon_trig_stat_UpWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_stat_up")
muon_trig_stat_DownWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_stat_down")
muon_trig_sys_UpWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_sys_up")
muon_trig_sys_DownWeights = replaceWeight(weights, "wtrig_nom", "wmu_trig_sys_down")

#### electron related uncertainties acting on weights ####
#CFID
el_cf_UpWeights = replaceWeight(weights, "wel_nom", "wel_cid_up")
el_cf_DownWeights = replaceWeight(weights, "wel_nom", "wel_cid_down")
#ID
el_id_UpWeights =  replaceWeight(weights, "wel_nom", "wel_id_up")
el_id_DownWeights =  replaceWeight(weights, "wel_nom", "wel_id_down")
#Iso
el_iso_UpWeights =  replaceWeight(weights, "wel_nom", "wel_iso_up")
el_iso_DownWeights =  replaceWeight(weights, "wel_nom", "wel_iso_down")
#Rec
el_reco_UpWeights =  replaceWeight(weights, "wel_nom", "wel_reco_up")
el_reco_DownWeights =  replaceWeight(weights, "wel_nom", "wel_reco_down")
#Trig
el_trig_UpWeights =  replaceWeight(weights, "wtrig_nom", "wel_trig_up")
el_trig_DownWeights =  replaceWeight(weights, "wtrig_nom", "wel_trig_down")
                                        
## jet weight based systematics
                                    
jet_jvt_UpWeights = replaceWeight(weights, "wjet_nom", "wjet_jvt_up")
jet_jvt_DownWeights = replaceWeight(weights, "wjet_nom", "wjet_jvt_down")

# trigger weights
trig_UpWeights = replaceWeight(weights, "wtrig_nom", "wtrig_up")
trig_DownWeights = replaceWeight(weights, "wtrig_nom", "wtrig_down")

chflip_UpWeights = replaceWeight(weights, "wchflip_nom", "wchflip_up")
chflip_DownWeights = replaceWeight(weights, "wchflip_nom", "wchflip_down")

pu_UpWeights_bkg =  replaceWeight(weights, "wpu_nom_bkg", "wpu_up_bkg")
pu_DownWeights_bkg =  replaceWeight(weights, "wpu_nom_bkg", "wpu_down_bkg")

pu_UpWeights_sig = replaceWeight(weights, "wpu_nom_sig", "wpu_up_sig")
pu_DownWeights_sig =  replaceWeight(weights, "wpu_nom_sig", "wpu_down_sig")


if "BTag" in SystList:
    jet_b_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_b_up")
    jet_b_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_b_down")
    jet_c_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_c_up")
    jet_c_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_c_down")
    jet_light_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_light_up")
    jet_light_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_light_down")

    jet_extra1_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra1_up")
    jet_extra1_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra1_down")

    jet_extra2_UpWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra2_up")
    jet_extra2_DownWeights =  replaceWeight(weights, "wjet_nom", "wjet_extra2_down")



basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst ={}
eTagFromCSyst ={}

if useFakePrediction:
    sysfake_uncorr={}
    sysfake_corr={}
    statfake={}
    
for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    
    #adding dummy 35% syst and an overallSys  
#    basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.35,0.65,"user","userOverallSys"))
    if "TheoDummy" in SystList:
        basicChanSyst[region].append(Systematic("TheoDummy",configMgr.weights,1.30,0.70,"user","userOverallSys"))
    #basicChanSyst[region].append(Systematic("dummy",configMgr.weights,1.15,0.85,"user","userOverallSys"))
   
    #Example systematic uncertainty
    if "JER" in SystList: 
        #basicChanSyst[region].append(Systematic("JER","_nom","_JET_JER__1up","_JET_JER__1up","tree","overallNormHistoSysOneSide")) 
        basicChanSyst[region].append(Systematic("JER","_nom","_JET_JER_SINGLE_NP__1up","_nom","tree","histoSysOneSide"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1","_nom","_JET_GroupedNP_1__1up","_JET_GroupedNP_1__1down","tree","histoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2","_nom","_JET_GroupedNP_2__1up","_JET_GroupedNP_2__1down","tree","histoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3","_nom","_JET_GroupedNP_3__1up","_JET_GroupedNP_3__1down","tree","histoSys"))
            basicChanSyst[region].append(Systematic("JET","_nom","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","histoSys")) 
        else :
             basicChanSyst[region].append(Systematic("JES_BJES_Response","_nom","_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_1","_nom","_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_2","_nom","_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_3","_nom","_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_4","_nom","_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EffectiveNP_5","_nom","_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_6","_nom","_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_7","_nom","_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES__EffectiveNP_8restTerm","_nom","_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree","histoSys"))
	     
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling","_nom","_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure","_nom","_JET_EtaIntercalibration_NonClosure__1up","_JET_EtaIntercalibration_NonClosure__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat","_nom","_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Composition","_nom","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_Flavor_Response","_nom","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu","_nom","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV","_nom","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm","_nom","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology","_nom","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_PunchThrough_MC15","_nom","_JET_PunchThrough_MC15__1up","_JET_PunchThrough_MC15__1down","tree","histoSys"))
             basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt","_nom","_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree","histoSys"))

    	        
    if "MET" in SystList:
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Reso","_nom","_MET_SoftCalo_Reso","_MET_SoftCalo_Reso","tree","histoSysOneSide"))
        #basicChanSyst[region].append(Systematic("MET_SoftCalo_Scale","_nom","_MET_SoftCalo_ScaleUp","_MET_SoftCalo_ScaleDown","tree","histoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk","_nom","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara","_nom","_MET_SoftTrk_ResoPara","_nom","tree","histoSysOneSide"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp","_nom","_MET_SoftTrk_ResoPerp","_nom","tree","histoSysOneSide"))             
    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL","_nom","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree","histoSys"))     
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL","_nom","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID","_nom","_MUON_ID__1up","_MUON_ID__1down","tree","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS","_nom","_MUON_MS__1up","_MUON_MS__1down","tree","histoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SCALE","_nom","_MUON_SCALE__1up","_MUON_SCALE__1down","tree","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_stat",configMgr.weights,muon_ttva_stat_UpWeights,muon_ttva_stat_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_sys",configMgr.weights,muon_ttva_sys_UpWeights ,muon_ttva_sys_DownWeights, "weight","histoSys"))

    if "LepEff" in SystList :
        basicChanSyst[region].append(Systematic("MUON_Eff_stat",configMgr.weights,muon_stat_UpWeights,muon_stat_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys",configMgr.weights,muon_sys_UpWeights,muon_sys_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_bad_stat",configMgr.weights,muon_bad_stat_UpWeights,muon_bad_stat_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_bad_sys",configMgr.weights,muon_bad_sys_UpWeights,muon_bad_sys_DownWeights,"weight","histoSys"))

        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_stat",configMgr.weights,muon_iso_stat_UpWeights,muon_iso_stat_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_Iso_sys",configMgr.weights,muon_iso_sys_UpWeights,muon_iso_sys_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_stat_lowpt",configMgr.weights,muon_stat_lowpt_UpWeights,muon_stat_lowpt_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_Eff_sys_lowpt",configMgr.weights,muon_sys_lowpt_UpWeights,muon_sys_lowpt_DownWeights,"weight","histoSys"))

        basicChanSyst[region].append(Systematic("EL_Cf",configMgr.weights,el_cf_UpWeights,el_cf_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("EG_Id",configMgr.weights,el_id_UpWeights,el_id_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("EG_Reco",configMgr.weights,el_reco_UpWeights,el_reco_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("EG_Iso",configMgr.weights,el_iso_UpWeights,el_iso_DownWeights,"weight","histoSys"))


    if "JVT" in SystList :
        basicChanSyst[region].append(Systematic("JVT",configMgr.weights,jet_jvt_UpWeights,jet_jvt_DownWeights,"weight","histoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("pileup_bkg",configMgr.weights,pu_UpWeights_bkg,pu_DownWeights_bkg,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("pileup_sig",configMgr.weights,pu_UpWeights_sig,pu_DownWeights_sig,"weight","histoSys"))

    if "BTag" in SystList:
        bTagSyst[region]      = Systematic("btag_BT",configMgr.weights,jet_b_UpWeights,jet_b_DownWeights,"weight","histoSys")
        cTagSyst[region]      = Systematic("btag_CT",configMgr.weights,jet_c_UpWeights,jet_c_DownWeights,"weight","histoSys")
        mTagSyst[region]      = Systematic("btag_LightT",configMgr.weights,jet_light_UpWeights,jet_light_DownWeights,"weight","histoSys")
        eTagSyst[region]      = Systematic("btag_Extra",configMgr.weights,jet_extra1_UpWeights,jet_extra1_DownWeights,"weight","histoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm",configMgr.weights,jet_extra2_UpWeights,jet_extra2_DownWeights,"weight","histoSys")

    if "Trigger" in SystList:
        basicChanSyst[region].append(Systematic("MUON_TRIG_STAT",muon_trig_stat_UpWeights,muon_trig_stat_DownWeights,configMgr.weights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("MUON_TRIG_SYS",configMgr.weights,muon_trig_sys_UpWeights,muon_trig_sys_DownWeights,"weight","histoSys"))
        basicChanSyst[region].append(Systematic("TRIG",configMgr.weights,trig_UpWeights,trig_DownWeights,"weight","histoSys"))

    if "ChFlip" in SystList:
        basicChanSyst[region].append(Systematic("ChFlip",configMgr.weights,chflip_UpWeights,chflip_DownWeights,"weight","histoSys"))

    #signal uncertainties                                                                                       
#    xsecSig = Systematic("SigXSec",configMgr.weights,xsecSigHighWeights,xsecSigLowWeights,"weight","overallSys")
    
    #Generator Systematics
    generatorSyst = []  
    if "WZ" in SystList:
        theoryUncertainties_SS_WZ.TheorUnc(generatorSyst)
    if "Rare" in SystList:
        theoryUncertainties_SS_Rare.TheorUnc(generatorSyst)
    if "ttV" in SystList:
        theoryUncertainties_SS_ttV.TheorUnc(generatorSyst)
    if "WW_ZZ" in SystList:
        theoryUncertainties_SS_WW_ZZ.TheorUnc(generatorSyst)
# ********************************************************************* #
#                              Background samples
# ********************************************************************* #

configMgr.nomName = "_nom"

WWSampleName = "WW"
WWSample = Sample(WWSampleName,kOrange-8)
WWSample.setStatConfig(useStat)
WWSample.setNormByTheory()
WWSample.setFileList(files_em['WW'])

WZSampleName = "WZ"
WZSample = Sample(WZSampleName,kOrange-8)
WZSample.setStatConfig(useStat)
WZSample.setNormByTheory()
WZSample.setFileList(files_em['WZ'])

ZZSampleName = "ZZ"
ZZSample = Sample(ZZSampleName,kOrange-8)
ZZSample.setStatConfig(useStat)
ZZSample.setNormByTheory()
ZZSample.setFileList(files_em['ZZ'])

ttbarVSampleName = "ttV"
ttbarVSample = Sample(ttbarVSampleName,kGreen-8)
ttbarVSample.setStatConfig(useStat)
ttbarVSample.setNormByTheory()
ttbarVSample.setFileList(files_em['ttV'])

RareSampleName = "Rare"
RareSample = Sample(RareSampleName,kGreen-8)
RareSample.setStatConfig(useStat)
RareSample.setNormByTheory()
RareSample.setFileList(files_em['Rare'])

#data sample for later
DataSample = Sample("data",kBlack)
DataSample.setFileList(dataFiles)
DataSample.setData()

sigSample = Sample(sigSamples[0],kPink)    
sigSample.setStatConfig(useStat)
sigSample.setNormByTheory()
sigSample.setNormFactor("mu_SIG",1.,0.,5.)
sigSample.setFileList(sigFiles_em[sigSamples[0]])

if useFakePrediction:
    FakeSample = Sample("Fakes",kViolet-9)
    FakeSample.setStatConfig(False)
 #   FakeSyst.addFakeCount(FakeSample)
    FakeSample.setFileList(files_em['Fakes'])
if useDataDrivenCF:
    ChargeFlipSample = Sample("ChargeFlip",kYellow)
    ChargeFlipSample.setStatConfig(False)
    ChargeFlipSample.setFileList(files_em['ChargeFlip'])
    
    
# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #
this_sigsample=sigSamples[0]
signame=this_sigsample.split('_')[0]+'_'+this_sigsample.split('_')[1]+'_'+this_sigsample.split('_')[2]+'_'+this_sigsample.split('_')[3].split('p')[0]+'_'+this_sigsample.split('_')[4].split('p')[0]
bkgOnly = configMgr.addFitConfig("SigProj_"+signame)

#creating now list of samples
samplelist=[]

if myFitType==FitType.Exclusion:
    samplelist.append(sigSample)

if not useFakePrediction:
    if 'ttV' in mysamples: samplelist.append(ttbarVSample)
    if 'Rare' in mysamples: samplelist.append(RareSample)
    if 'WW' in mysamples: samplelist.append(WWSample)
    if 'WZ' in mysamples: samplelist.append(WZSample)
    if 'ZZ' in mysamples: samplelist.append(ZZSample)
    if 'data' in mysamples: samplelist.append(DataSample)
else:
    if 'ttV' in mysamples: samplelist.append(ttbarVSample)
    if 'WW' in mysamples: samplelist.append(WWSample)
    if 'ZZ' in mysamples: samplelist.append(ZZSample)
    if 'WZ' in mysamples: samplelist.append(WZSample)
    if 'Rare' in mysamples: samplelist.append(RareSample)
    if 'data' in mysamples: samplelist.append(DataSample)
            
    
if not useFakePrediction:
    bkgOnly.addSamples(samplelist)
else:
    bkgOnly.addSamples(samplelist)
    bkgOnly.addSamples(FakeSample)  
    #bkgOnly.getSample("Fakes").addSampleSpecificWeight("FakeWeight")    
    bkgOnly.getSample("Fakes").removeWeight("lumiScaling")
    bkgOnly.getSample("Fakes").removeWeight("mcweight")
    bkgOnly.getSample("Fakes").removeWeight("wpu_nom_sig")
    bkgOnly.getSample("Fakes").removeWeight("wpu_nom_bkg")
    bkgOnly.getSample("Fakes").removeWeight("wchflip_nom")
    bkgOnly.getSample("Fakes").removeWeight("wjet_nom")
    bkgOnly.getSample("Fakes").removeWeight("wel_nom")
    bkgOnly.getSample("Fakes").removeWeight("wtrig_nom")
    bkgOnly.getSample("Fakes").removeWeight("wmu_nom")
    bkgOnly.getSample("Fakes").addWeight("FakeWeight")
#    bkgOnly.getSample("Fakes").addWeight("({}/".format(configMgr.inputLumi) + "(" + str(mylumi) + ")" + ")")

if useDataDrivenCF:
    bkgOnly.addSamples(ChargeFlipSample)    
    #bkgOnly.getSample("ChargeFlip").addSampleSpecificWeight("qFwt")    
    bkgOnly.getSample("ChargeFlip").removeWeight("lumiScaling")
    bkgOnly.getSample("ChargeFlip").removeWeight("mcweight")
    bkgOnly.getSample("ChargeFlip").removeWeight("wpu_nom_sig")
    bkgOnly.getSample("ChargeFlip").removeWeight("wpu_nom_bkg")
    bkgOnly.getSample("ChargeFlip").removeWeight("wchflip_nom")
    bkgOnly.getSample("ChargeFlip").removeWeight("wjet_nom")
    bkgOnly.getSample("ChargeFlip").removeWeight("wel_nom")
    bkgOnly.getSample("ChargeFlip").removeWeight("wtrig_nom")
    bkgOnly.getSample("ChargeFlip").removeWeight("wmu_nom")
    bkgOnly.getSample("ChargeFlip").addWeight("qFwt")
#    bkgOnly.getSample("ChargeFlip").addWeight("({}/".format(configMgr.inputLumi) + "(" + str(mylumi) + ")" + ")")
    
#mctruth information    
for sample in mysamples:
    if sample == 'data':
        continue
    else:
        bkgOnly.getSample(sample).addSampleSpecificWeight("(isTruthLep1==1&&isTruthLep2==1)")
    

#data driven charge flip systematics
DDcf_UpWeights = replaceWeight(bkgOnly.getSample("ChargeFlip").weights , "qFwt", "qFwt_sys_1up")
DDcf_DownWeights = replaceWeight(bkgOnly.getSample("ChargeFlip").weights , "qFwt", "qFwt_sys_1dn")


if useStat:
    bkgOnly.statErrThreshold=0.001
else:
    bkgOnly.statErrThreshold=None

#Add Measurement
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.029) ## DS2
#meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.030) ## DS2.1
meas=bkgOnly.addMeasurement("BasicMeasurement",lumi=1.0,lumiErr=0.032) ## new recommendations for Moriond 2017 from February 2017
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi","const",1.0)

#b-tag classification of channels and lepton flavor classification of channels
bReqChans = {}
bVetoChans = {}
bAgnosticChans = {}
elChans = {}
muChans = {}
elmuChans = {}
for region in CRregions:
    bReqChans[region] = []
    bVetoChans[region] = []
    bAgnosticChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################

if myFitType==FitType.Background:
    if "jet1" in CRregions:
        tmp = appendTo(bkgOnly.addChannel("cuts",["SRjet1"],1,0.5,1.5),[elmuChans["jet1"],bVetoChans["jet1"]])
        bkgOnly.addBkgConstrainChannels(tmp)
    if "jet23" in CRregions:
        tmp = appendTo(bkgOnly.addChannel("cuts",["SRjet23"],1,0.5,1.5),[elmuChans["jet23"],bVetoChans["jet23"]])
        bkgOnly.addBkgConstrainChannels(tmp)

    appendTo(bkgOnly.addValidationChannel("cuts",["VRjet1"],1,0.5,1.5),[elmuChans["jet1"],bVetoChans["jet1"]])    
    appendTo(bkgOnly.addValidationChannel("cuts",["VRjet23"],1,0.5,1.5),[elmuChans["jet23"],bVetoChans["jet23"]])

    
if myFitType==FitType.Exclusion:
    bkgOnly.setSignalSample(sigSample) 
    if "jet1" in CRregions:
        tmp = appendTo(bkgOnly.addChannel("cuts",["SRjet1"],1,0.5,1.5),[elmuChans["jet1"],bVetoChans["jet1"]])
        bkgOnly.addSignalChannels(tmp)
    if "jet23" in CRregions:
        tmp = appendTo(bkgOnly.addChannel("cuts",["SRjet23"],1,0.5,1.5),[elmuChans["jet23"],bVetoChans["jet23"]])
        bkgOnly.addSignalChannels(tmp)

    
# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #

AllChannels = {}
AllChannels_all=[]
elChans_all=[]
muChans_all=[]
elmuChans_all=[]

for region in CRregions:
    AllChannels[region] = bReqChans[region] + bVetoChans[region] + bAgnosticChans[region]
    AllChannels_all +=  AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]   
    elmuChans_all += elmuChans[region]

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan
        
    for chan in AllChannels_all:
        #        if tgtchan=="All" or tgtchan==chan.name:
	#print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal:
	    #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))
   
if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region],basicChanSyst[region])
        SetupChannels(muChans[region],basicChanSyst[region])
        SetupChannels(elmuChans[region],basicChanSyst[region])
#

                

# b-tag reg/veto/agnostic channels


for region in CRregions:    
    for chan in bReqChans[region]:
        #chan.hasBQCD = True #need this QCD BG later
        #chan.addSystematic(bTagSyst)  
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])	    

	    	 
    for chan in bVetoChans[region]:
        #chan.hasBQCD = False #need this QCD BG later
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

            
    for chan in bAgnosticChans[region]:
        chan.removeWeight("bTagWeight")

    if useFakePrediction or useDataDrivenCF:
        for chan in elmuChans[region]:
            if "TheoDummy" in SystList:
                chan.getSample("Fakes").removeSystematic("TheoDummy")
                chan.getSample("ChargeFlip").removeSystematic("TheoDummy")
                if myFitType==FitType.Exclusion:
                    chan.getSample(sigSamples[0]).removeSystematic("TheoDummy")
            if "FakeDummy" in SystList and useFakePrediction and not "Fake" in SystList:
                chan.getSample("Fakes").addSystematic(Systematic("FakeDummy",configMgr.weights,1.30,0.70,"user","userOverallSys"))
            if "DDChargeFlip" in SystList and useDataDrivenCF:
                chan.getSample("ChargeFlip").addSystematic(Systematic("DDChargeFlip", bkgOnly.getSample("ChargeFlip").weights, DDcf_UpWeights, DDcf_DownWeights,"weight","histoSys"))
            if "Fake" in SystList and useFakePrediction and not "FakeDummy" in SystList:
                FakeSyst.genFakeSyst(sysfake_corr,sysfake_uncorr,statfake)
                channelname = chan.channelName[:7] if (len(chan.channelName) > 11) else chan.channelName[:6]
                chan.getSample("Fakes").addSystematic(sysfake_corr[channelname])
                chan.getSample("Fakes").addSystematic(sysfake_uncorr[channelname])
                chan.getSample("Fakes").addSystematic(statfake[ channelname])
                
            if "JER" in SystList:
                chan.getSample("Fakes").removeSystematic("JER")
                chan.getSample("ChargeFlip").removeSystematic("JER")
            if "JES" in SystList:
                for syst in ["JES_Group1","JES_Group2","JES_Group3","JET"]:
                    chan.getSample("Fakes").removeSystematic(syst)
                    chan.getSample("ChargeFlip").removeSystematic(syst)
            if "MET" in SystList:
                for syst in ["MET_SoftTrk","MET_SoftTrk_ResoPara","MET_SoftTrk_ResoPerp"]:
                    chan.getSample("Fakes").removeSystematic(syst)
                    chan.getSample("ChargeFlip").removeSystematic(syst)

            if "LEP" in SystList:
                for syst in ["EG_RESOLUTION_ALL","EG_SCALE_ALL","MUON_ID","MUON_MS","MUON_SCALE","MUON_TTVA_stat","MUON_TTVA_sys"]:
                    chan.getSample("Fakes").removeSystematic(syst)
                    chan.getSample("ChargeFlip").removeSystematic(syst)
#                    if syst == "MUON_SCALE": chan.getSample("WZ").removeSystematic(syst)

            if "LepEff" in SystList :
                for syst in ["MUON_Eff_stat","MUON_Eff_sys","MUON_Eff_bad_stat","MUON_Eff_bad_sys","MUON_Eff_Iso_stat","MUON_Eff_Iso_sys","MUON_Eff_stat_lowpt","MUON_Eff_sys_lowpt","EL_Cf","EG_Id","EG_Reco","EG_Iso"]:
                    chan.getSample("Fakes").removeSystematic(syst)
                    chan.getSample("ChargeFlip").removeSystematic(syst)

            if "JVT" in SystList :
                chan.getSample("Fakes").removeSystematic("JVT")
                chan.getSample("ChargeFlip").removeSystematic("JVT")

            if "pileup" in SystList:
                chan.getSample("Fakes").removeSystematic("pileup_bkg")
                chan.getSample("Fakes").removeSystematic("pileup_sig")
                chan.getSample("ChargeFlip").removeSystematic("pileup_bkg")
                chan.getSample("ChargeFlip").removeSystematic("pileup_sig")

            if "BTag" in SystList:
                for syst in ["btag_BT","btag_CT","btag_LightT","btag_Extra","btag_ExtraFromCharm"]:
                    chan.getSample("Fakes").removeSystematic(syst)
                    chan.getSample("ChargeFlip").removeSystematic(syst)

            if "Trigger" in SystList:
                for syst in ["MUON_TRIG_STAT","MUON_TRIG_SYS","TRIG"]:
                    chan.getSample("Fakes").removeSystematic(syst)
                    chan.getSample("ChargeFlip").removeSystematic(syst)

            if "ChFlip" in SystList:
                chan.getSample("Fakes").removeSystematic("ChFlip")
                chan.getSample("ChargeFlip").removeSystematic("ChFlip")
      
                                                                        
                                                


# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55,0.45,0.87,0.89,"") #without signal
#leg = ROOT.TLegend(0.55,0.35,0.87,0.89,"") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("","Data 2015+2016 (#sqrt{s}=13 TeV)","lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Standard Model","lf")
entry.SetLineColor(kBlack)#ZSample.color)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
if not useFakePrediction:
    # entry = leg.AddEntry("","t#bar{t}","lf")
    # entry.SetLineColor(kGreen-9)
    # entry.SetFillColor(kGreen-9)
    # entry.SetFillStyle(compFillStyle)
    # #
    # entry = leg.AddEntry("","W+jets","lf")
    # entry.SetLineColor(kAzure-4)
    # entry.SetFillColor(kAzure-4)
    # entry.SetFillStyle(compFillStyle)
    # #
    # entry = leg.AddEntry("","Single Top","lf")
    # entry.SetLineColor(kGreen-5)
    # entry.SetFillColor(kGreen-5)
    # entry.SetFillStyle(compFillStyle)
    # #
    # entry = leg.AddEntry("","Z+jets","lf")
    # entry.SetLineColor(ZSample.color)
    # entry.SetFillColor(ZSample.color)
    # entry.SetFillStyle(compFillStyle)
    # #
    entry = leg.AddEntry("","t#bar{t}V","lf")
    entry.SetLineColor(kGreen-9)
    entry.SetFillColor(kGreen-9)
    entry.SetFillStyle(compFillStyle)

else:
    entry = leg.AddEntry("","Fakes","lf")
    entry.SetLineColor(FakeSample.color)
    entry.SetFillColor(FakeSample.color)
    entry.SetFillStyle(compFillStyle)
    

# entry = leg.AddEntry("","Diboson","lf")
# entry.SetLineColor(DibosonsSample.color)
# entry.SetFillColor(DibosonsSample.color)
# entry.SetFillStyle(compFillStyle)
#
# entry = leg.AddEntry("","Multi Top","lf")
# entry.SetLineColor(MultiTopSample.color)
# entry.SetFillColor(MultiTopSample.color)
# entry.SetFillStyle(compFillStyle)
#
#entry = leg.AddEntry("","ChargeFlip","lf")
#entry.SetLineColor(ChargeFlipSample.color)
#entry.SetFillColor(ChargeFlipSample.color)
#entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","ttbarV","lf")
entry.SetLineColor(ttbarVSample.color)
entry.SetFillColor(ttbarVSample.color)
entry.SetFillStyle(compFillStyle)
#
# entry = leg.AddEntry("","Higgs","lf")
# entry.SetLineColor(HiggsSample.color)
# entry.SetFillColor(HiggsSample.color)
# entry.SetFillStyle(compFillStyle)
# #
#entry = leg.AddEntry("","Multijet","lf")
#entry.SetLineColor(QCDSample.color)
#entry.SetFillColor(QCDSample.color)
#entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg

c.Close()
MeffBins = [ '1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    else:
        chan.minY = 0.05 
        chan.maxY = 100
    chan.ATLASLabelX = 0.27  #AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True
    #chan.titleX="m^{incl}_{eff} [GeV]"
    

#if myFitType==FitType.Exclusion:
#    for sig in sigSamples:
#        for chan in SR_channels[sig]:
#            chan.titleY = "Events"
#            chan.minY = 0.05 
#            chan.maxY = 80
#            chan.ATLASLabelX = 0.125
#            chan.ATLASLabelY = 0.85
#            chan.ATLASLabelText = "Internal"
#            chan.showLumi = True

#configMgr.fitConfigs.remove(bkgOnly)
