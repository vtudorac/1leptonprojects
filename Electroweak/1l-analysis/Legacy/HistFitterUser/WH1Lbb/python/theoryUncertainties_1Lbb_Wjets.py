import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

WjetsSystematics={}

WjetsSystematics['WjetsScale_SRLM'] = Systematic("WjetsScale", configMgr.weights, 1.+0.104, 1.-0.104 , "user","userOverallSys")
WjetsSystematics['WjetsScale_SRMM'] = Systematic("WjetsScale", configMgr.weights, 1.+0.114, 1.-0.114 , "user","userOverallSys")
WjetsSystematics['WjetsScale_SRHM'] = Systematic("WjetsScale", configMgr.weights, 1.+0.011, 1.-0.011 , "user","userOverallSys")
WjetsSystematics['WjetsScale_STCR'] = Systematic("WjetsScale", configMgr.weights, 1.+0.09, 1.-0.09 , "user","userOverallSys")
WjetsSystematics['WjetsScale_TRLM'] = Systematic("WjetsScale", configMgr.weights, 1.+0.093, 1.-0.093 , "user","userOverallSys")
WjetsSystematics['WjetsScale_TRMM'] = Systematic("WjetsScale", configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
WjetsSystematics['WjetsScale_TRHM'] = Systematic("WjetsScale", configMgr.weights, 1.+0.039, 1.-0.039 , "user","userOverallSys")
WjetsSystematics['WjetsScale_VRtt1on'] = Systematic("WjetsScale", configMgr.weights, 1.+0.057, 1.-0.057 , "user","userOverallSys")
WjetsSystematics['WjetsScale_VRtt2on'] = Systematic("WjetsScale", configMgr.weights, 1.+0.095, 1.-0.095 , "user","userOverallSys")
WjetsSystematics['WjetsScale_VRtt3on'] = Systematic("WjetsScale", configMgr.weights, 1.+0.082, 1.-0.082 , "user","userOverallSys")
WjetsSystematics['WjetsScale_VRtt1off'] = Systematic("WjetsScale", configMgr.weights, 1.+0.107, 1.-0.107 , "user","userOverallSys")
WjetsSystematics['WjetsScale_VRtt2off'] = Systematic("WjetsScale", configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
WjetsSystematics['WjetsScale_VRtt3off'] = Systematic("WjetsScale", configMgr.weights, 1.+0.129, 1.-0.129 , "user","userOverallSys")


"""
WjetsSystematics['WjetsRenormalisation_SRLM'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.090, 1.-0.090 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_SRMM'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.359, 1.-0.359 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_SRHM'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.015, 1.-0.015 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_STCR'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.080, 1.-0.080 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_TRLM'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.092, 1.-0.092 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_TRMM'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.057, 1.-0.057 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_TRHM'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.028, 1.-0.028 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_VRtt1on'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.065, 1.-0.065 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_VRtt2on'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.095, 1.-0.095 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_VRtt3on'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.197, 1.-0.197 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_VRtt1off'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.104, 1.-0.104 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_VRtt2off'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
WjetsSystematics['WjetsRenormalisation_VRtt3off'] = Systematic("WjetsRenormalisation", configMgr.weights, 1.+0.365, 1.-0.365 , "user","userOverallSys")

WjetsSystematics['WjetsFactorisation_SRLM'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.012, 1.-0.012 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_SRMM'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.004, 1.-0.004 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_SRHM'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.011, 1.-0.011 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_STCR'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.005, 1.-0.005 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_TRLM'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.004, 1.-0.004 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_TRMM'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.01, 1.-0.01 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_TRHM'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.025, 1.-0.025 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_VRtt1on'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.017, 1.-0.017 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_VRtt2on'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.011, 1.-0.011 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_VRtt3on'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.012, 1.-0.012 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_VRtt1off'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.003, 1.-0.003 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_VRtt2off'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
WjetsSystematics['WjetsFactorisation_VRtt3off'] = Systematic("WjetsFactorisation", configMgr.weights, 1.+0.017, 1.-0.017 , "user","userOverallSys")
"""

WjetsSystematics['WjetsAlphaVariation_SRLM'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.009, 1.-0.009 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_SRMM'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.005, 1.-0.005 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_SRHM'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.002, 1.-0.002 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_STCR'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.008, 1.-0.008 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_TRLM'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.002, 1.-0.002 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_TRMM'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.002, 1.-0.002 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_TRHM'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.002, 1.-0.002 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_VRtt1on'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.002, 1.-0.002 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_VRtt2on'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.005, 1.-0.005 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_VRtt3on'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.004, 1.-0.004 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_VRtt1off'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.006, 1.-0.006 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_VRtt2off'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.002, 1.-0.002 , "user","userOverallSys")
WjetsSystematics['WjetsAlphaVariation_VRtt3off'] = Systematic("WjetsAlphaVariation", configMgr.weights, 1.+0.013, 1.-0.013 , "user","userOverallSys")

WjetsSystematics['WjetsPDF_SRLM'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.043, 1.-0.043 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_SRMM'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.072, 1.-0.072 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_SRHM'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.057, 1.-0.057 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_STCR'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.019, 1.-0.019 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_TRLM'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_TRMM'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.039, 1.-0.039 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_TRHM'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.021, 1.-0.021 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_VRtt1on'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.047, 1.-0.047 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_VRtt2on'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.104, 1.-0.104 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_VRtt3on'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.057, 1.-0.057 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_VRtt1off'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.022, 1.-0.022 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_VRtt2off'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.033, 1.-0.033 , "user","userOverallSys")
WjetsSystematics['WjetsPDF_VRtt3off'] = Systematic("WjetsPDF", configMgr.weights, 1.+0.036, 1.-0.036 , "user","userOverallSys")

"""
WjetsSystematics['WjetsLHAPDF_SRLM'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.054, 1.-0.054 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_SRMM'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.075, 1.-0.075 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_SRHM'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.048, 1.-0.048 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_STCR'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.012, 1.-0.012 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_TRLM'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_TRMM'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.052, 1.-0.052 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_TRHM'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.03, 1.-0.03 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_VRtt1on'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.058, 1.-0.058 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_VRtt2on'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.079, 1.-0.079 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_VRtt3on'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.018, 1.-0.018 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_VRtt1off'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.04, 1.-0.04 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_VRtt2off'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.044, 1.-0.044 , "user","userOverallSys")
WjetsSystematics['WjetsLHAPDF_VRtt3off'] = Systematic("WjetsLHAPDF", configMgr.weights, 1.+0.019, 1.-0.019 , "user","userOverallSys")
"""

WjetsSystematics['WjetsCKKW_SRLM'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_SRMM'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.006, 1.-0.006 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_SRHM'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.035, 1.-0.035 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_STCR'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.018, 1.-0.018 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_TRLM'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.024, 1.-0.024 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_TRMM'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.027, 1.-0.027 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_TRHM'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.014, 1.-0.014 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_VRtt1on'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.025, 1.-0.025 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_VRtt2on'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.009, 1.-0.009 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_VRtt3on'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.02, 1.-0.02 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_VRtt1off'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.023, 1.-0.023 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_VRtt2off'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.028, 1.-0.028 , "user","userOverallSys")
WjetsSystematics['WjetsCKKW_VRtt3off'] = Systematic("WjetsCKKW", configMgr.weights, 1.+0.047, 1.-0.047 , "user","userOverallSys")

WjetsSystematics['WjetsQSF_SRLM'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.076, 1.-0.076 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_SRMM'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.017, 1.-0.017 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_SRHM'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.039, 1.-0.039 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_STCR'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.024, 1.-0.024 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_TRLM'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.020, 1.-0.020 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_TRMM'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.031, 1.-0.031 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_TRHM'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.026, 1.-0.026 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_VRtt1on'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.032, 1.-0.032 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_VRtt2on'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.013, 1.-0.013 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_VRtt3on'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.018, 1.-0.018 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_VRtt1off'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.031, 1.-0.031 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_VRtt2off'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.035, 1.-0.035 , "user","userOverallSys")
WjetsSystematics['WjetsQSF_VRtt3off'] = Systematic("WjetsQSF", configMgr.weights, 1.+0.058, 1.-0.058 , "user","userOverallSys")

def TheorUnc(generatorSyst):
           
    for key in WjetsSystematics: 
           name=key.split('_')
           #print key
           name1=name[-1]
           #print name1
                      
           
           if "SRLM" in name1:               
              generatorSyst.append((("wjets_Sherpa221","SRLMEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRLMEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRLMMu"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRLMinclEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRLMinclEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRLMinclMu"), WjetsSystematics[key]))              
           elif "SRMM" in name1:               
              generatorSyst.append((("wjets_Sherpa221","SRMMEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRMMEl"), WjetsSystematics[key]))	      
              generatorSyst.append((("wjets_Sherpa221","SRMMMu"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRMMinclEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRMMinclEl"), WjetsSystematics[key]))	      
              generatorSyst.append((("wjets_Sherpa221","SRMMinclMu"), WjetsSystematics[key]))	             
           elif "SRHM" in name1:               
              generatorSyst.append((("wjets_Sherpa221","SRHMEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRHMEl"), WjetsSystematics[key]))      
              generatorSyst.append((("wjets_Sherpa221","SRHMMu"), WjetsSystematics[key]))  
              generatorSyst.append((("wjets_Sherpa221","SRHMinclEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","SRHMinclEl"), WjetsSystematics[key]))      
              generatorSyst.append((("wjets_Sherpa221","SRHMinclMu"), WjetsSystematics[key]))                             
           elif "TRLM" in name1:               
              generatorSyst.append((("wjets_Sherpa221","TRLMEM"), WjetsSystematics[key]))  
           elif "TRMM" in name1:               
              generatorSyst.append((("wjets_Sherpa221","TRMMEM"), WjetsSystematics[key]))    	      	      
           elif "TRHM" in name1:               
              generatorSyst.append((("wjets_Sherpa221","TRHMEM"), WjetsSystematics[key])) 
           elif "STCR" in name1:               
              generatorSyst.append((("wjets_Sherpa221","STCREM"), WjetsSystematics[key])) 	                     
           elif "VRtt1on" in name1:               
              generatorSyst.append((("wjets_Sherpa221","VRtt1onEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt1onEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt1onMu"), WjetsSystematics[key]))
           elif "VRtt2on" in name1:               
              generatorSyst.append((("wjets_Sherpa221","VRtt2onEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt2onEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt2onMu"), WjetsSystematics[key]))    	      
           elif "VRtt3on" in name1:               
              generatorSyst.append((("wjets_Sherpa221","VRtt3onEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt3onEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt3onMu"), WjetsSystematics[key]))   
           elif "VRtt1off" in name1:               
              generatorSyst.append((("wjets_Sherpa221","VRtt1offEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt1offEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt1offMu"), WjetsSystematics[key]))
           elif "VRtt2off" in name1:               
              generatorSyst.append((("wjets_Sherpa221","VRtt2offEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt2offEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt2offMu"), WjetsSystematics[key]))  	      
           elif "VRtt3off" in name1:               
              generatorSyst.append((("wjets_Sherpa221","VRtt3offEM"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt3offEl"), WjetsSystematics[key]))
              generatorSyst.append((("wjets_Sherpa221","VRtt3offMu"), WjetsSystematics[key]))       
      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      	   
