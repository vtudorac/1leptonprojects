import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

ZjetsSystematics={}

ZjetsSystematics['ZjetsScale_SRLM'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.134, 1.-0.134 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_WR'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.194, 1.-0.194 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_STCR'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.136, 1.-0.136 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_TRMM'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.0056, 1.-0.0056 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_TRHM'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.253, 1.-0.253 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_VRtt1on'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.259, 1.-0.259 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_VRtt2on'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.366, 1.-0.366 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_VRtt3on'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.023, 1.-0.023 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_VRtt1off'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.281, 1.-0.281 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_VRtt2off'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.104, 1.-0.104 , "user","userOverallSys")
ZjetsSystematics['ZjetsScale_VRtt3off'] = Systematic("ZjetsScale", configMgr.weights, 1.+0.104, 1.-0.104 , "user","userOverallSys")

"""
ZjetsSystematics['ZjetsFactorisation_SRLM'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.017, 1.-0.017 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_WR'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.035, 1.-0.035 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_STCR'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.005, 1.-0.005 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_TRMM'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.006, 1.-0.006 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_TRHM'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.038, 1.-0.038 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_VRtt1on'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.031, 1.-0.031 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_VRtt2on'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.038, 1.-0.038 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_VRtt3on'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.004, 1.-0.004 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_VRtt1off'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.077, 1.-0.077 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_VRtt2off'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.031, 1.-0.031 , "user","userOverallSys")
ZjetsSystematics['ZjetsFactorisation_VRtt3off'] = Systematic("ZjetsFactorisation", configMgr.weights, 1.+0.035, 1.-0.035 , "user","userOverallSys")
"""

ZjetsSystematics['ZjetsCKKW_SRLM'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.10, 1.-0.10 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_WR'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.07, 1.-0.07 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_STCR'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.108, 1.-0.108 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_TRMM'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.005, 1.-0.005 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_TRHM'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.082, 1.-0.082 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_VRtt1on'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_VRtt2on'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.041, 1.-0.041 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_VRtt3on'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.11, 1.-0.11 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_VRtt1off'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_VRtt2off'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.08, 1.-0.08 , "user","userOverallSys")
ZjetsSystematics['ZjetsCKKW_VRtt3off'] = Systematic("ZjetsCKKW", configMgr.weights, 1.+0.052, 1.-0.052 , "user","userOverallSys")

ZjetsSystematics['ZjetsQSF_SRLM'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.031, 1.-0.031 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_WR'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.061, 1.-0.061 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_STCR'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.071, 1.-0.071 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_TRMM'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.005, 1.-0.005 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_TRHM'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.06, 1.-0.06 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_VRtt1on'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.061, 1.-0.061 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_VRtt2on'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.033, 1.-0.033 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_VRtt3on'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.05, 1.-0.05 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_VRtt1off'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.085, 1.-0.085 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_VRtt2off'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.055, 1.-0.055 , "user","userOverallSys")
ZjetsSystematics['ZjetsQSF_VRtt3off'] = Systematic("ZjetsQSF", configMgr.weights, 1.+0.073, 1.-0.073 , "user","userOverallSys")

def TheorUnc(generatorSyst):
           
    for key in ZjetsSystematics: 
           name=key.split('_')
           #print key
           name1=name[-1]
           #print name1
                      
           
           if "SRLM" in name1:               
              generatorSyst.append((("zjets_Sherpa221","SRLMEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","SRLMEl"), ZjetsSystematics[key]))      
              generatorSyst.append((("zjets_Sherpa221","SRLMMu"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","SRLMinclEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","SRLMinclEl"), ZjetsSystematics[key]))      
              generatorSyst.append((("zjets_Sherpa221","SRLMinclMu"), ZjetsSystematics[key]))             
           elif "TRMM" in name1:               
              generatorSyst.append((("zjets_Sherpa221","TRMMEM"), ZjetsSystematics[key]))   	      	      
           elif "TRHM" in name1:               
              generatorSyst.append((("zjets_Sherpa221","TRHMEM"), ZjetsSystematics[key]))  
           elif "STCR" in name1:               
              generatorSyst.append((("zjets_Sherpa221","STCREM"), ZjetsSystematics[key])) 
           elif "WR" in name1:               
              generatorSyst.append((("zjets_Sherpa221","WREM"), ZjetsSystematics[key]))    	                     
           elif "VRtt1on" in name1:               
              generatorSyst.append((("zjets_Sherpa221","VRtt1onEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt1onEl"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt1onMu"), ZjetsSystematics[key]))
           elif "VRtt2on" in name1:               
              generatorSyst.append((("zjets_Sherpa221","VRtt2onEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt2onEl"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt2onMu"), ZjetsSystematics[key]))    	      
           elif "VRtt3on" in name1:               
              generatorSyst.append((("zjets_Sherpa221","VRtt3onEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt3onEl"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt3onMu"), ZjetsSystematics[key]))   
           elif "VRtt1off" in name1:               
              generatorSyst.append((("zjets_Sherpa221","VRtt1offEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt1offEl"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt1offMu"), ZjetsSystematics[key]))
           elif "VRtt2off" in name1:               
              generatorSyst.append((("zjets_Sherpa221","VRtt2offEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt2offEl"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt2offMu"), ZjetsSystematics[key]))  	      
           elif "VRtt3off" in name1:               
              generatorSyst.append((("zjets_Sherpa221","VRtt3offEM"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt3offEl"), ZjetsSystematics[key]))
              generatorSyst.append((("zjets_Sherpa221","VRtt3offMu"), ZjetsSystematics[key]))     
