import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

singletopSystematics={}

singletopSystematics['singletopGenerator_SRLM'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.0881, 1.-0.0881 , "user","userOverallSys")
singletopSystematics['singletopGenerator_SRMM'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.219, 1.-0.2119 , "user","userOverallSys")
singletopSystematics['singletopGenerator_SRHM'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.0649, 1.-0.0649 , "user","userOverallSys")
singletopSystematics['singletopGenerator_WR'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.3802, 1.-0.3802 , "user","userOverallSys")
singletopSystematics['singletopGenerator_TRLM'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.3101, 1.-0.3101 , "user","userOverallSys")
singletopSystematics['singletopGenerator_TRMM'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.3794, 1.-0.3794 , "user","userOverallSys")
singletopSystematics['singletopGenerator_TRHM'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.3749, 1.-0.3749 , "user","userOverallSys")
singletopSystematics['singletopGenerator_VRtt1on'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.6916, 1.-0.6916 , "user","userOverallSys")
singletopSystematics['singletopGenerator_VRtt2on'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.6382, 1.-0.6382 , "user","userOverallSys")
singletopSystematics['singletopGenerator_VRtt3on'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.5763, 1.-0.5763 , "user","userOverallSys")
singletopSystematics['singletopGenerator_VRtt1off'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.0306, 1.-0.0306 , "user","userOverallSys")
singletopSystematics['singletopGenerator_VRtt2off'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.2163, 1.-0.2163 , "user","userOverallSys")
singletopSystematics['singletopGenerator_VRtt3off'] = Systematic("singletopGenerator", configMgr.weights, 1.+0.1483, 1.-0.1483 , "user","userOverallSys")

singletopSystematics['singletopPS_SRLM'] = Systematic("singletopPS", configMgr.weights, 1.+0.1615, 1.-0.1615 , "user","userOverallSys")
singletopSystematics['singletopPS_SRMM'] = Systematic("singletopPS", configMgr.weights, 1.+0.2404, 1.-0.2404 , "user","userOverallSys")
singletopSystematics['singletopPS_SRHM'] = Systematic("singletopPS", configMgr.weights, 1.+0.1638, 1.-0.1638 , "user","userOverallSys")
singletopSystematics['singletopPS_WR'] = Systematic("singletopPS", configMgr.weights, 1.+0.115, 1.-0.115 , "user","userOverallSys")
singletopSystematics['singletopPS_TRLM'] = Systematic("singletopPS", configMgr.weights, 1.+0.0583, 1.-0.0583 , "user","userOverallSys")
singletopSystematics['singletopPS_TRMM'] = Systematic("singletopPS", configMgr.weights, 1.+0.0466, 1.-0.0466 , "user","userOverallSys")
singletopSystematics['singletopPS_TRHM'] = Systematic("singletopPS", configMgr.weights, 1.+0.0546, 1.-0.0546 , "user","userOverallSys")
singletopSystematics['singletopPS_VRtt1on'] = Systematic("singletopPS", configMgr.weights, 1.+0.1981, 1.-0.1981 , "user","userOverallSys")
singletopSystematics['singletopPS_VRtt2on'] = Systematic("singletopPS", configMgr.weights, 1.+0.0855, 1.-0.0855 , "user","userOverallSys")
singletopSystematics['singletopPS_VRtt3on'] = Systematic("singletopPS", configMgr.weights, 1.+0.1456, 1.-0.1456 , "user","userOverallSys")
singletopSystematics['singletopPS_VRtt1off'] = Systematic("singletopPS", configMgr.weights, 1.+0.1535, 1.-0.1535 , "user","userOverallSys")
singletopSystematics['singletopPS_VRtt2off'] = Systematic("singletopPS", configMgr.weights, 1.+0.1009, 1.-0.1009 , "user","userOverallSys")
singletopSystematics['singletopPS_VRtt3off'] = Systematic("singletopPS", configMgr.weights, 1.+0.1671, 1.-0.1671 , "user","userOverallSys")

singletopSystematics['singletopRadiation_SRLM'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.265, 1.-0.265 , "user","userOverallSys")
singletopSystematics['singletopRadiation_SRMM'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.146, 1.-0.146 , "user","userOverallSys")
singletopSystematics['singletopRadiation_SRHM'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.1194, 1.-0.1194 , "user","userOverallSys")
singletopSystematics['singletopRadiation_WR'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.8105, 1.-0.8105 , "user","userOverallSys")
singletopSystematics['singletopRadiation_TRLM'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.0786, 1.-0.0786 , "user","userOverallSys")
singletopSystematics['singletopRadiation_TRMM'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.0807, 1.-0.0807 , "user","userOverallSys")
singletopSystematics['singletopRadiation_TRHM'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.0722, 1.-0.0722 , "user","userOverallSys")
singletopSystematics['singletopRadiation_VRtt1on'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.3488, 1.-0.3488 , "user","userOverallSys")
singletopSystematics['singletopRadiation_VRtt2on'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.1284, 1.-0.1284 , "user","userOverallSys")
singletopSystematics['singletopRadiation_VRtt3on'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.3526, 1.-0.3526 , "user","userOverallSys")
singletopSystematics['singletopRadiation_VRtt1off'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.1912, 1.-0.1912 , "user","userOverallSys")
singletopSystematics['singletopRadiation_VRtt2off'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.1783, 1.-0.1783 , "user","userOverallSys")
singletopSystematics['singletopRadiation_VRtt3off'] = Systematic("singletopRadiation", configMgr.weights, 1.+0.1189, 1.-0.1189 , "user","userOverallSys")

singletopSystematics['singletopInterference_SRLM'] = Systematic("singletopInterference", configMgr.weights, 1.+0.393, 1.-0.393 , "user","userOverallSys")
singletopSystematics['singletopInterference_SRMM'] = Systematic("singletopInterference", configMgr.weights, 1.+0.5214, 1.-0.5214 , "user","userOverallSys")
singletopSystematics['singletopInterference_SRHM'] = Systematic("singletopInterference", configMgr.weights, 1.+0.2965, 1.-0.2965 , "user","userOverallSys")
singletopSystematics['singletopInterference_WR'] = Systematic("singletopInterference", configMgr.weights, 1.+0.8424, 1.-0.8424 , "user","userOverallSys")
singletopSystematics['singletopInterference_TRLM'] = Systematic("singletopInterference", configMgr.weights, 1.+0.2613, 1.-0.2613 , "user","userOverallSys")
singletopSystematics['singletopInterference_TRMM'] = Systematic("singletopInterference", configMgr.weights, 1.+0.5662, 1.-0.5662 , "user","userOverallSys")
singletopSystematics['singletopInterference_TRHM'] = Systematic("singletopInterference", configMgr.weights, 1.+0.7634, 1.-0.7634 , "user","userOverallSys")
singletopSystematics['singletopInterference_VRtt1on'] = Systematic("singletopInterference", configMgr.weights, 1.+1.7191, 1.-1. , "user","userOverallSys")
singletopSystematics['singletopInterference_VRtt2on'] = Systematic("singletopInterference", configMgr.weights, 1.+3.0321, 1.-1. , "user","userOverallSys")
singletopSystematics['singletopInterference_VRtt3on'] = Systematic("singletopInterference", configMgr.weights, 1.+1.0459, 1.-1. , "user","userOverallSys")
singletopSystematics['singletopInterference_VRtt1off'] = Systematic("singletopInterference", configMgr.weights, 1.+0.2478, 1.-0.2478 , "user","userOverallSys")
singletopSystematics['singletopInterference_VRtt2off'] = Systematic("singletopInterference", configMgr.weights, 1.+0.1996, 1.-0.1996 , "user","userOverallSys")
singletopSystematics['singletopInterference_VRtt3off'] = Systematic("singletopInterference", configMgr.weights, 1.+0.461, 1.-0.461 , "user","userOverallSys")

def TheorUnc(generatorSyst):
           
    for key in singletopSystematics: 
           name=key.split('_')
           #print key
           name1=name[-1]
           #print name1
           
             
           if "SRLM" in name1:               
              generatorSyst.append((("singletop","SRLMEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRLMEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRLMMu"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRLMinclEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRLMinclEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRLMinclMu"), singletopSystematics[key]))             
           elif "SRMM" in name1:               
              generatorSyst.append((("singletop","SRMMEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRMMEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRMMMu"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRMMinclEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRMMinclEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRMMinclMu"), singletopSystematics[key]))             
           elif "SRHM" in name1:               
              generatorSyst.append((("singletop","SRHMEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRHMEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRHMMu"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRHMinclEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRHMinclEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","SRHMinclMu"), singletopSystematics[key]))                
           elif "TRLM" in name1:               
              generatorSyst.append((("singletop","TRLMEM"), singletopSystematics[key]))
           elif "TRMM" in name1:               
              generatorSyst.append((("singletop","TRMMEM"), singletopSystematics[key]))
           elif "TRHM" in name1:               
              generatorSyst.append((("singletop","TRHMEM"), singletopSystematics[key]))      
           elif "WR" in name1:               
              generatorSyst.append((("singletop","WREM"), singletopSystematics[key]))	      
           elif "VRtt1on" in name1:               
              generatorSyst.append((("singletop","VRtt1onEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt1onEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt1onMu"), singletopSystematics[key]))
           elif "VRtt2on" in name1:               
              generatorSyst.append((("singletop","VRtt2onEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt2onEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt2onMu"), singletopSystematics[key]))    	      
           elif "VRtt3on" in name1:               
              generatorSyst.append((("singletop","VRtt3onEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt3onEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt3onMu"), singletopSystematics[key]))   
           elif "VRtt1off" in name1:               
              generatorSyst.append((("singletop","VRtt1offEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt1offEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt1offMu"), singletopSystematics[key]))
           elif "VRtt2off" in name1:               
              generatorSyst.append((("singletop","VRtt2offEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt2offEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt2offMu"), singletopSystematics[key]))  	      
           elif "VRtt3off" in name1:               
              generatorSyst.append((("singletop","VRtt3offEM"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt3offEl"), singletopSystematics[key]))
              generatorSyst.append((("singletop","VRtt3offMu"), singletopSystematics[key]))       
