import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

TtbarSystematics={}

TtbarSystematics['TtbarGenerator_SRLM'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.3938, 1.-0.3938 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_SRMM'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.3014, 1.-0.3014 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_SRHM'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.2267, 1.-0.2267 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_WR'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.7359, 1.-0.7359 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_STCRLM'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.5715, 1.-0.5715 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_STCRMM'] = Systematic("TtbarGenerator", configMgr.weights, 1.+ 1.0162, 1.-1. , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_STCRHM'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.7612, 1.-0.7612 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_VRtt1on'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.2254, 1.-0.2254 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_VRtt2on'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.178, 1.-0.178 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_VRtt3on'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.1386, 1.-0.1386 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_VRtt1off'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.3097, 1.-0.3097 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_VRtt2off'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.6264, 1.-0.6264 , "user","userOverallSys")
TtbarSystematics['TtbarGenerator_VRtt3off'] = Systematic("TtbarGenerator", configMgr.weights, 1.+0.1303, 1.-0.1303 , "user","userOverallSys")

TtbarSystematics['TtbarPS_SRLM'] = Systematic("TtbarPS", configMgr.weights, 1.+0.2756, 1.-0.2756 , "user","userOverallSys")
TtbarSystematics['TtbarPS_SRMM'] = Systematic("TtbarPS", configMgr.weights, 1.+0.3511, 1.-0.3511 , "user","userOverallSys")
TtbarSystematics['TtbarPS_SRHM'] = Systematic("TtbarPS", configMgr.weights, 1.+0.2644, 1.-0.2644 , "user","userOverallSys")
TtbarSystematics['TtbarPS_WR'] = Systematic("TtbarPS", configMgr.weights, 1.+0.3034, 1.-0.3034 , "user","userOverallSys")
TtbarSystematics['TtbarPS_STCRLM'] = Systematic("TtbarPS", configMgr.weights, 1.+0.6471, 1.-0.6471 , "user","userOverallSys")
TtbarSystematics['TtbarPS_STCRMM'] = Systematic("TtbarPS", configMgr.weights, 1.+0.9307, 1.-0.9307 , "user","userOverallSys")
TtbarSystematics['TtbarPS_STCRHM'] = Systematic("TtbarPS", configMgr.weights, 1.+0.5658, 1.-0.5658 , "user","userOverallSys")
TtbarSystematics['TtbarPS_VRtt1on'] = Systematic("TtbarPS", configMgr.weights, 1.+0.1828, 1.-0.1828 , "user","userOverallSys")
TtbarSystematics['TtbarPS_VRtt2on'] = Systematic("TtbarPS", configMgr.weights, 1.+0.386, 1.-0.386 , "user","userOverallSys")
TtbarSystematics['TtbarPS_VRtt3on'] = Systematic("TtbarPS", configMgr.weights, 1.+0.0767, 1.-0.0767 , "user","userOverallSys")
TtbarSystematics['TtbarPS_VRtt1off'] = Systematic("TtbarPS", configMgr.weights, 1.+0.3042, 1.-0.3042 , "user","userOverallSys")
TtbarSystematics['TtbarPS_VRtt2off'] = Systematic("TtbarPS", configMgr.weights, 1.+0.2696, 1.-0.2696 , "user","userOverallSys")
TtbarSystematics['TtbarPS_VRtt3off'] = Systematic("TtbarPS", configMgr.weights, 1.+0.4104, 1.-0.4104 , "user","userOverallSys")

TtbarSystematics['TtbarRadiation_SRLM'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.366, 1.-0.366 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_SRMM'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.2617, 1.-0.2617 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_SRHM'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.2078, 1.-0.2078 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_WR'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.1119, 1.-0.1119 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_STCRLM'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.1855, 1.-0.1855 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_STCRMM'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.0979, 1.-0.0979 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_STCRHM'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.501, 1.-0.501 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_VRtt1on'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.0718, 1.-0.0718 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_VRtt2on'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.0589, 1.-0.0589 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_VRtt3on'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.0137, 1.-0.0137 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_VRtt1off'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.4164, 1.-0.4164 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_VRtt2off'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.3469, 1.-0.3469 , "user","userOverallSys")
TtbarSystematics['TtbarRadiation_VRtt3off'] = Systematic("TtbarRadiation", configMgr.weights, 1.+0.1565, 1.-0.1565 , "user","userOverallSys")


def TheorUnc(generatorSyst):
           
    for key in TtbarSystematics: 
           name=key.split('_')
           #print key
           name1=name[-1]
           #print name1
                      
           
           if "SRLM" in name1:               
              generatorSyst.append((("ttbar_bin1","SRLMEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","SRLMEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","SRLMMu"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","SRLMinclEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","SRLMinclEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","SRLMinclMu"), TtbarSystematics[key]))              
           elif "SRMM" in name1:               
              generatorSyst.append((("ttbar_bin2","SRMMEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","SRMMEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","SRMMMu"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","SRMMinclEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","SRMMinclEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","SRMMinclMu"), TtbarSystematics[key]))  
           elif "SRHM" in name1:               
              generatorSyst.append((("ttbar_bin3","SRHMEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin3","SRHMEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin3","SRHMMu"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin3","SRHMinclEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin3","SRHMinclEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin3","SRHMinclMu"), TtbarSystematics[key]))                
           elif "STCRLM" in name1:               
              generatorSyst.append((("ttbar_bin1","STCREM"), TtbarSystematics[key]))  
           elif "STCRMM" in name1:               
              generatorSyst.append((("ttbar_bin2","STCREM"), TtbarSystematics[key]))    	      	      
           elif "STCRHM" in name1:               
              generatorSyst.append((("ttbar_bin3","STCREM"), TtbarSystematics[key])) 
           elif "WR" in name1:               
              generatorSyst.append((("ttbar_bin1","WREM"), TtbarSystematics[key])) 	       
           elif "VRtt1on" in name1:		
              generatorSyst.append((("ttbar_bin1","VRtt1onEM"), TtbarSystematics[key]))	
              generatorSyst.append((("ttbar_bin1","VRtt1onEl"), TtbarSystematics[key]))	
              generatorSyst.append((("ttbar_bin1","VRtt1onMu"), TtbarSystematics[key]))	
           elif "VRtt2on" in name1:		
              generatorSyst.append((("ttbar_bin2","VRtt2onEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","VRtt2onEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","VRtt2onMu"), TtbarSystematics[key]))	
           elif "VRtt3on" in name1:		
              generatorSyst.append((("ttbar_bin3","VRtt3onEM"), TtbarSystematics[key]))	
              generatorSyst.append((("ttbar_bin3","VRtt3onEl"), TtbarSystematics[key]))	
              generatorSyst.append((("ttbar_bin3","VRtt3onMu"), TtbarSystematics[key]))	
           elif "VRtt1off" in name1:		 
              generatorSyst.append((("ttbar_bin1","VRtt1offEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","VRtt1offEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin1","VRtt1offMu"), TtbarSystematics[key]))	 
           elif "VRtt2off" in name1:		
              generatorSyst.append((("ttbar_bin2","VRtt2offEM"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","VRtt2offEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin2","VRtt2offMu"), TtbarSystematics[key]))	 
           elif "VRtt3off" in name1:		 
              generatorSyst.append((("ttbar_bin3","VRtt3offEM"), TtbarSystematics[key]))	   
              generatorSyst.append((("ttbar_bin3","VRtt3offEl"), TtbarSystematics[key]))
              generatorSyst.append((("ttbar_bin3","VRtt3offMu"), TtbarSystematics[key]))

	      
