#!/usr/bin/env python

import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

QCD_VVV_up_down = {
    'SRjet1': (0.066, 0.056),
    'SRjet23': (0.086, 0.070),
    'VRjet1': ( 0.057, 0.044),
    'VRjet23': (0.121, 0.099),
}

PDF_VVV_up_down = {
    'SRjet1' : (0.052, 0.069),
    'SRjet23': (0.032, 0.054),
    'VRjet1' : (0.034, 0.065),
    'VRjet23' : (0.019,0.035),
}

Xsec_VVV_up_down = {
    'SRjet1' : (0.20, 0.20),
    'SRjet23': (0.20, 0.20),
    'VRjet1' : (0.20, 0.20),
    'VRjet23' : (0.20,0.20),
}



VVV_systs={}
ttH_systs={}

def TheorUnc(generatorSyst):
    VVV_DSID_up="(1.+((MCId==407311||MCId==407312||MCId==407313||MCId==407314||MCId==407315)*{}))"
    VVV_DSID_down="(1.-((MCId==407311||MCId==407312||MCId==407313||MCId==407314||MCId==407315)*{}))"
    
    for name,(qcd_up,qcd_down) in QCD_VVV_up_down.iteritems():
        VVV_systs["VVV_QCD_"+ name] = Systematic("VVV_QCD", configMgr.weights, configMgr.weights + [VVV_DSID_up.format(qcd_up)],configMgr.weights+[ VVV_DSID_down.format(qcd_down)], "weight", "userHistoSys")
    for name,(pdf_up,pdf_down) in PDF_VVV_up_down.iteritems():
        VVV_systs["VVV_PDF_"+ name] = Systematic("VVV_PDF", configMgr.weights, configMgr.weights + [VVV_DSID_up.format(pdf_up)],configMgr.weights+[ VVV_DSID_down.format(pdf_down)], "weight", "userHistoSys")
    for name,(xsec_up,xsec_down) in Xsec_VVV_up_down.iteritems():
        VVV_systs["VVV_XSEC_"+ name] = Systematic("VVV_XSEC", configMgr.weights, configMgr.weights + [VVV_DSID_up.format(xsec_up)],configMgr.weights+[ VVV_DSID_down.format(xsec_down)], "weight", "userHistoSys")

    for key in VVV_systs:
        name = key.split('_')
        name1 = name[-1]
        if 'SRjet1' in name1:
            generatorSyst.append((("Rare","SRjet1"), VVV_systs[key]))
        elif 'SRjet23' in name1:
            generatorSyst.append((("Rare","SRjet23"), VVV_systs[key]))
        elif 'VRjet1' in name1:
            generatorSyst.append((("Rare","VRjet1"), VVV_systs[key]))
        elif 'VRjet23' in name1:
            generatorSyst.append((("Rare","VRjet23"), VVV_systs[key]))

    ttH_DSID_up="(1. + ((MCId==341177||MCId==341270)*0.5))"
    ttH_DSID_down="(1. - ((MCId==341177||MCId==341270)*0.5))"
                    
    for name in QCD_VVV_up_down.keys():
        ttH_systs["ttH_theo_" + name] = Systematic("ttH_PMG", configMgr.weights, configMgr.weights + [ttH_DSID_up],configMgr.weights+[ ttH_DSID_down], "weight", "userHistoSys")
    for key in ttH_systs:
        name = key.split('_')
        name1 = name[-1]
        if 'SRjet1' in name1:
            generatorSyst.append((("Rare","SRjet1"), ttH_systs[key]))
        elif 'SRjet23' in name1:
            generatorSyst.append((("Rare","SRjet23"), ttH_systs[key]))
        elif 'VRjet1' in name1:
            generatorSyst.append((("Rare","VRjet1"), ttH_systs[key]))
        elif 'VRjet23' in name1:
            generatorSyst.append((("Rare","VRjet23"), ttH_systs[key]))

        
