#!/usr/bin/env python

import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

ww_zz_pmg = {
    'SRjet1': (0.06, 0.06),
    'SRjet23': (0.06, 0.06),
    'VRjet1': ( 0.06, 0.06),
    'VRjet23': (0.06, 0.06),
}

ZZ_systs={}
WW_systs={}

def TheorUnc(generatorSyst):
    for name, (pmg_up, pmg_down) in ww_zz_pmg.iteritems():
        WW_systs["WW_PMG_"+ name] = Systematic("WW_PMG", "1.", 1.+ pmg_up, 1.- pmg_down, "user", "userHistoSys")
    for name,(pmg_up,pmg_down) in ww_zz_pmg.iteritems():
        ZZ_systs["ZZ_PMG_"+ name] = Systematic("ZZ_PMG", "1.", 1.+ pmg_up, 1.- pmg_down, "user", "userHistoSys")

    for key in ZZ_systs:
        name = key.split('_')
        name1 = name[-1]     
        if 'SRjet1' in name1:
            generatorSyst.append((("ZZ","SRjet1"), ZZ_systs[key]))
        elif 'SRjet23' in name1:
            generatorSyst.append((("ZZ","SRjet23"), ZZ_systs[key]))
        elif 'VRjet1' in name1:
            generatorSyst.append((("ZZ","VRjet1"), ZZ_systs[key]))
        elif 'VRjet23' in name1:
            generatorSyst.append((("ZZ","VRjet23"), ZZ_systs[key]))

    for key in WW_systs:
        name = key.split('_')
        name1 = name[-1]     
        if 'SRjet1' in name1:
            generatorSyst.append((("WW","SRjet1"), WW_systs[key]))
        elif 'SRjet23' in name1:
            generatorSyst.append((("WW","SRjet23"), WW_systs[key]))
        elif 'VRjet1' in name1:
            generatorSyst.append((("WW","VRjet1"), WW_systs[key]))
        elif 'VRjet23' in name1:
            generatorSyst.append((("WW","VRjet23"), WW_systs[key]))









    
        
