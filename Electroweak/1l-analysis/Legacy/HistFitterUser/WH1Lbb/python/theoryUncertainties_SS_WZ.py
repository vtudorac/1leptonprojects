#!/usr/bin/env python

import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

QCD_up_down = {
    'SRjet1': (0.027 , 0.033),
    'SRjet23': (0.134, 0.121),
    'VRjet1': (0.065, 0.061),
    'VRjet23': (0.227, 0.160),
}

PDF_up_down = {
    'SRjet1' : (0.043, 0.068),
    'SRjet23': (0.024, 0.056),
    'VRjet1' : (0.027, 0.056),
    'VRjet23' : (0.017, 0.040),
    }

WZ_systs={}

def TheorUnc(generatorSyst):
    for name,(qcd_up,qcd_down) in QCD_up_down.iteritems():
        WZ_systs["WZ_QCD_"+ name] = Systematic("WZ_QCD", "1.", 1.+ qcd_up, 1.- qcd_down, "user", "userHistoSys")
    for name,(pdf_up,pdf_down) in PDF_up_down.iteritems():
        WZ_systs["WZ_PDF_"+ name] = Systematic("WZ_PDF", "1.", 1.+ pdf_up, 1.- pdf_down, "user", "userHistoSys")
    for name in PDF_up_down.keys():
        WZ_systs["WZ_Xsec_"+ name] = Systematic("WZ_Xsec", "1.", 1.+ 0.06, 1.- 0.06, "user", "userHistoSys")

        
    for key in WZ_systs:
        name = key.split('_')
        name1 = name[-1]     
        if 'SRjet1' in name1:
            generatorSyst.append((("WZ","SRjet1"), WZ_systs[key]))
        elif 'SRjet23' in name1:
            generatorSyst.append((("WZ","SRjet23"), WZ_systs[key]))
        elif 'VRjet1' in name1:
            generatorSyst.append((("WZ","VRjet1"), WZ_systs[key]))
        elif 'VRjet23' in name1:
            generatorSyst.append((("WZ","VRjet23"), WZ_systs[key]))

