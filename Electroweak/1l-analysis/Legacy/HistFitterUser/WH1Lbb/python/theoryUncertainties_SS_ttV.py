#!/usr/bin/env python

import ROOT
from ROOT import gSystem
gSystem.Load("libSusyFitter.so")

from systematic import Systematic
from configManager import configMgr

PGM_recom={
    'ttZ' : (0.12,"(1+(MCId==410218||MCId==410219||MCId==410220)*{})", "(1-(MCId==410218||MCId==410219||MCId==410220)*{})"),
    'ttW' : (0.13,"(1+(MCId==410155)*{})","(1-(MCId==410155)*{})"),
    'ttWW' : (0.50,"(1+(MCId==410081)*{})","(1-(MCId==410081)*{})"),
}


ttV_systs={}

def TheorUnc(generatorSyst):
    
    print configMgr.weights
    print "going into theor uncertainties for ttv"    
    for name, (var,dsids_up,dsids_down) in PGM_recom.iteritems():
        for region in ['SRjet1','SRjet23','VRjet1','VRjet23']:
            ttV_systs["ttV_"+ name + "_" + region] = Systematic("ttV_"+name, configMgr.weights, configMgr.weights + [dsids_up.format(var)],configMgr.weights+[ dsids_down.format(var)], "weight", "userHistoSys")
            print "ttV_"+ name + "_" + region
    for key in ttV_systs:
        name = key.split('_')
        name1 = name[-1]
        print key, "ttVsysts key"
        print ttV_systs[key],"ttvsysts"
        if 'SRjet1' in name1:
            generatorSyst.append((("ttV","SRjet1"), ttV_systs[key]))
        elif 'SRjet23' in name1:
            generatorSyst.append((("ttV","SRjet23"), ttV_systs[key]))
        elif 'VRjet1' in name1:
            generatorSyst.append((("ttV","VRjet1"), ttV_systs[key]))
        elif 'VRjet23' in name1:
            generatorSyst.append((("ttV","VRjet23"), ttV_systs[key]))
            


            
