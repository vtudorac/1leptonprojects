#!/bin/bash

in_ds=$1

echo $in_ds

HistFitter.py -t -w -r ${in_ds} python/MyDiscoveryAnalysis_2017SS.py
UpperLimitTable.py -a -N 40 -c ${in_ds} -w results/MyDiscoveryAnalysis_Moriond2017SS_${in_ds}/SPlusB_combined_BasicMeasurement_model.root -l 36.06
UpperLimitTable.py -c ${in_ds} -N 40 -w results/MyDiscoveryAnalysis_Moriond2017SS_${in_ds}/SPlusB_combined_BasicMeasurement_model.root -l 36.06
