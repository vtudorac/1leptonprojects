#!/bin/bash

# Absolute path to this script
script=$(readlink -f "$0")
# Absolute path this script is in
my_path=$(dirname "$script")


#make sure that tables are activated!

HistFitter.py -t -w -r HM,MM,LM -f -m "all" -D "corrMatrix" --fitname=Validation ${my_path}/python/OneLeptonbb_Summer2017.py 2>&1 | tee logs/out_HMMMLM.log

mkdir -p tables

## CR
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TRLMEM_cuts,TRMMEM_cuts,TRHMEM_cuts,WREM_cuts,STCREM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t CR -u TRLMEM_cuts,TRMMEM_cuts,TRHMEM_cuts,WREM_cuts,STCREM_cuts -o tables/MyTable1LepCR_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TREM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t TREM -u TRLMEM_cuts,TRMMEM_cuts,TRHMEM_cuts -o tables/MyTable1LepTR_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WREM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t WREM -u WREM_cuts -o tables/MyTable1LepWR_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c STCREM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t STCREM -u STCREM_cuts -o tables/MyTable1LepSTCR_bkgonly.tex

SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TRLMEM_cuts -o tables/Systematics1LepTRLMEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TRMMEM_cuts -o tables/Systematics1LepTRMMEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c TRHMEM_cuts -o tables/Systematics1LepTRHMEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c WREM_cuts -o tables/Systematics1LepWREM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c STCREM_cuts -o tables/Systematics1LepSTCREM_bkgonly.tex

## SR
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMEM_cuts,SRMMEM_cuts,SRHMEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SR -u SRLMEM_cuts,SRMMEM_cuts,SRHMEM_cuts -o tables/MyTable1LepSR_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMEl_cuts,SRMMEl_cuts,SRHMEl_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SR -u SRLMEl_cuts,SRMMEl_cuts,SRHMEl_cuts -o tables/MyTable1LepSREl_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMMu_cuts,SRMMMu_cuts,SRHMMu_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SR -u SRLMMu_cuts,SRMMMu_cuts,SRHMMu_cuts -o tables/MyTable1LepSRMu_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SRLMEM -u SRLMEM_cuts -o tables/MyTable1LepSRLM_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRMMEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SRMMEM -u SRMMEM_cuts -o tables/MyTable1LepSRMM_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRHMEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SRHMEM -u SRHMEM_cuts -o tables/MyTable1LepSRHM_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMinclEM_cuts,SRMMinclEM_cuts,SRHMinclEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t SRincl -u SRLMinclEM_cuts,SRMMinclEM_cuts,SRHMinclEM_cuts -o tables/MyTable1LepSRincl_bkgonly.tex

SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMEM_cuts -o tables/Systematics1LepSRLMEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRMMEM_cuts -o tables/Systematics1LepSRMMEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRHMEM_cuts -o tables/Systematics1LepSRHMEM_bkgonly.tex

SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMEl_cuts -o tables/Systematics1LepSRLMEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRMMEl_cuts -o tables/Systematics1LepSRMMEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRHMEl_cuts -o tables/Systematics1LepSRHMEl_bkgonly.tex

SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMMu_cuts -o tables/Systematics1LepSRLMMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRMMMu_cuts -o tables/Systematics1LepSRMMMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRHMMu_cuts -o tables/Systematics1LepSRHMMu_bkgonly.tex


## VRttxoff
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offEM_cuts,VRtt2offEM_cuts,VRtt3offEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VR -u VRtt1offEM_cuts,VRtt2offEM_cuts,VRtt3offEM_cuts -o tables/MyTable1LepVR1_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offMu_cuts,VRtt2offMu_cuts,VRtt3offMu_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VR -u VRtt1offMu_cuts,VRtt2offMu_cuts,VRtt3offMu_cuts -o tables/MyTable1LepVR1Mu_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offEl_cuts,VRtt2offEl_cuts,VRtt3offEl_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VR -u VRtt1offEl_cuts,VRtt2offEl_cuts,VRtt3offEl_cuts -o tables/MyTable1LepVR1El_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VRtt1offEM -u VRtt1offEM_cuts -o tables/MyTable1LepVRtt1off_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2offEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VRtt2offEM -u VRtt2offEM_cuts -o tables/MyTable1LepVRtt2off_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3offEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VRtt3offEM -u VRtt3offEM_cuts -o tables/MyTable1LepVRtt3off_bkgonly.tex

SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offEM_cuts -o tables/Systematics1LepVRtt1offEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2offEM_cuts -o tables/Systematics1LepVRtt2offEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3offEM_cuts -o tables/Systematics1LepVRtt3offEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offEl_cuts -o tables/Systematics1LepVRtt1offEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2offEl_cuts -o tables/Systematics1LepVRtt2offEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3offEl_cuts -o tables/Systematics1LepVRtt3offEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1offMu_cuts -o tables/Systematics1LepVRtt1offMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2offMu_cuts -o tables/Systematics1LepVRtt2offMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3offMu_cuts -o tables/Systematics1LepVRtt3offMu_bkgonly.tex


## VRttxon
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onEM_cuts,VRtt2onEM_cuts,VRtt3onEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VR -u VRtt1onEM_cuts,VRtt2onEM_cuts,VRtt3onEM_cuts -o tables/MyTable1LepVR2_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onEl_cuts,VRtt2onEl_cuts,VRtt3onEl_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VR -u VRtt1onEl_cuts,VRtt2onEl_cuts,VRtt3onEl_cuts -o tables/MyTable1LepVR2El_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onMu_cuts,VRtt2onMu_cuts,VRtt3onMu_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VR -u VRtt1onMu_cuts,VRtt2onMu_cuts,VRtt3onMu_cuts -o tables/MyTable1LepVR2Mu_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VRtt1onEM -u VRtt1onEM_cuts -o tables/MyTable1LepVRtt1on_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2onEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VRtt2onEM -u VRtt2onEM_cuts -o tables/MyTable1LepVRtt2on_bkgonly.tex
YieldsTable.py -b -y -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3onEM_cuts -s triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,[ttbar_bin1,ttbar_bin2,ttbar_bin3] -t VRtt3onEM -u VRtt3onEM_cuts -o tables/MyTable1LepVRtt3on_bkgonly.tex

SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onEM_cuts -o tables/Systematics1LepVRtt1onEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2onEM_cuts -o tables/Systematics1LepVRtt2onEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3onEM_cuts -o tables/Systematics1LepVRtt3onEM_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onMu_cuts -o tables/Systematics1LepVRtt1onMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2onMu_cuts -o tables/Systematics1LepVRtt2onMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3onMu_cuts -o tables/Systematics1LepVRtt3onMu_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt1onEl_cuts -o tables/Systematics1LepVRtt1onEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt2onEl_cuts -o tables/Systematics1LepVRtt2onEl_bkgonly.tex
SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c VRtt3onEl_cuts -o tables/Systematics1LepVRtt3onEl_bkgonly.tex


python scripts/pull_maker.py out_HMMMLM

python scripts/Run2PullPlot_VRSR2J.py


SysTable.py -% -w results/OneLeptonbb_HM_MM_LM_general/Validation_combined_BasicMeasurement_model_afterFit.root -c SRLMEM_cuts,SRMMEM_cuts,SRHMEM_cuts -o tables/Systematics1LepallSRsummary_bkgonly.tex systematics.py

