"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtilsSR2J module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtilsVRSR2
from pullPlotUtilsVRSR2 import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["TRLMEM"] = "TRLM"
    myRegionDict["TRMMEM"] = "TRMM"
    myRegionDict["TRHMEM"] = "TRHM"        
    myRegionDict["WREM"] = "WR"
    myRegionDict["STCREM_cuts"] = "STCR"
    
    myRegionDict["SRHMEM"] = "SRHM"
    myRegionDict["SRMMEM"] = "SRMM"
    myRegionDict["SRLMEM"] = "SRLM"   
    
    myRegionDict["VRtt1offEM"] = "VRtt1off"
    myRegionDict["VRtt2offEM"] = "VRtt2off"
    myRegionDict["VRtt3offEM"] = "VRtt3off"    
    
    myRegionDict["VRtt1onEM"] = "VRtt1on"
    myRegionDict["VRtt2onEM"] = "VRtt2on"
    myRegionDict["VRtt3onEM"] = "VRtt3on"      

    
        
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["TRLMEM_cuts","TRMMEM_cuts","TRHMEM_cuts","WREM_cuts","STCREM_cuts","VRtt1offEM_cuts","VRtt2offEM_cuts","VRtt3offEM_cuts","VRtt1onEM_cuts","VRtt2onEM_cuts","VRtt3onEM_cuts","SRHMEM_cuts","SRMMEM_cuts","SRLMEM_cuts"]


    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SR") != -1: return kBlue    
    elif name.find("VR") != -1: return kGreen    
    else: return kOrange       
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if "ttbar" in sample:         return kGreen - 9
    if "wjets" in sample:       return kAzure + 1
    if "zjets" in sample:     return kOrange
    if "diboson" in sample:     return kViolet - 8
    if "triboson" in sample:     return kViolet - 7
    if "singletop" in sample:     return kGreen + 2
    if "ttv" in sample:     return kOrange+1
    if sample == "ttH":     return kOrange+2
    if sample == "VH":     return kOrange+3
    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtilsSR2' default colours (which are all black)
    pullPlotUtilsVRSR2.getRegionColor = getRegionColor
    pullPlotUtilsVRSR2.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/../HistFitterUser/WH1Lbb/" # 

    # Where's the pickle file? - define list of pickle files
    pickleFilename = [ os.getenv("HISTFITTER")+"/../HistFitterUser/WH1Lbb/tables/MyTable1LepCR_bkgonly.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/WH1Lbb/tables/MyTable1LepVR1_bkgonly.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/WH1Lbb/tables/MyTable1LepVR2_bkgonly.pickle",
                       os.getenv("HISTFITTER")+"/../HistFitterUser/WH1Lbb/tables/MyTable1LepSR_bkgonly.pickle"
                      ]
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "CRVRSR"

    # Samples to stack on top of eachother in each region
    samples = "triboson_Sherpa221,VH,zjets_Sherpa221,ttv_NLO,ttH,singletop,diboson_Sherpa221,wjets_Sherpa221,group_ttbar_bin1_ttbar_bin2_ttbar_bin3"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    for mypickle in pickleFilename:
        if not os.path.exists(mypickle):
            print "pickle filename %s does not exist" % mypickle
            return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)

if __name__ == "__main__":
    main()
