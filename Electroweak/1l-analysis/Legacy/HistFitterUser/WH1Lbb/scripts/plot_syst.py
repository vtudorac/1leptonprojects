#!/usr/bin/env python

import pickle, os

import ROOT
from ROOT import *

gROOT.LoadMacro("~/atlasstyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
gROOT.LoadMacro("~/atlasstyle/AtlasLabels.C")

f = open("tables/Systematics1LepallSRsummary_bkgonly.pickle","rb")
t = pickle.load(f)

print t

c = TCanvas("systematics","systematics",800,600)


n_channels = t.keys()

h_totsyst = TH1F("h_totsyst","h_totsyst", len(n_channels), 0, len(n_channels))

list_of_syst = ['theo','exp','mu','statistics']

h={}
for syst in list_of_syst:
    h[syst] = TH1F(syst,syst, len(n_channels), 0, len(n_channels))

i=1
for key in n_channels:    
    h_totsyst.SetBinContent(i,t[key]['totsyserr']/t[key]['nfitted'])
    for syst in list_of_syst:
        h[syst].SetBinContent(i,t[key]['syserr_'+syst]/t[key]['nfitted'])   
    h_totsyst.GetXaxis().SetBinLabel(i, key.replace("_cuts",""))
    i=i+1
    
    
h_totsyst.SetFillColor(kCyan)
h_totsyst.SetLineColor(kCyan+1)
h_totsyst.SetLineWidth(5)
h_totsyst.LabelsOption("v","x")

h_totsyst.SetAxisRange(0.,1.,"Y")

h_totsyst.GetYaxis().SetTitle('Relative uncertainty')

h_totsyst.Draw()

colors = [kRed+1,kBlue+1,kMagenta,kGreen+3]

j=0
for syst in list_of_syst:
    h[syst].SetLineColor(colors[j])
    h[syst].SetLineWidth(5)
    h[syst].SetLineStyle(j+1)
    h[syst].Draw("same")
    j = j+1
    
    
l = TLegend(0.6,0.6,0.9,0.9)
l.SetTextFont(42)
l.SetBorderSize(0)
l.AddEntry(h_totsyst,"Total uncertainty", "lf")

description = ["Theoretical uncertainty","Experimental uncertainty","Norm. factor","MC statistical uncertainty"]

k=0
for syst in list_of_syst:
    l.AddEntry(h[syst],description[k],'l')
    k = k+1
    
l.Draw("same")    


ATLASLabel(0.2,0.85,"Internal")
   
l2 = ROOT.TLatex()
l2.SetNDC()
l2.SetTextFont(42)
    
l2.DrawLatex(0.2,0.73,"Wh (1 lepton + b#bar{b})")
l2.DrawLatex(0.2,0.79,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}")


c.Print("systematics_summary.png")
c.Print("systematics_summary.eps")
c.Print("systematics_summary.pdf")
