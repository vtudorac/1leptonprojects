namemap={}
namemap['statistics'] = ['gamma_stat_SRHMEM_cuts_bin_0','gamma_stat_SRHMEl_cuts_bin_0','gamma_stat_SRHMMu_cuts_bin_0','gamma_stat_SRHMinclEM_cuts_bin_0','gamma_stat_SRHMinclEl_cuts_bin_0','gamma_stat_SRHMinclMu_cuts_bin_0','gamma_stat_SRLMEM_cuts_bin_0','gamma_stat_SRLMEl_cuts_bin_0','gamma_stat_SRLMMu_cuts_bin_0','gamma_stat_SRLMinclEM_cuts_bin_0','gamma_stat_SRLMinclEl_cuts_bin_0','gamma_stat_SRLMinclMu_cuts_bin_0','gamma_stat_SRMMEM_cuts_bin_0','gamma_stat_SRMMEl_cuts_bin_0','gamma_stat_SRMMMu_cuts_bin_0','gamma_stat_SRMMinclEM_cuts_bin_0','gamma_stat_SRMMinclEl_cuts_bin_0','gamma_stat_SRMMinclMu_cuts_bin_0','gamma_stat_STCREM_cuts_bin_0','gamma_stat_TRHMEM_cuts_bin_0','gamma_stat_TRLMEM_cuts_bin_0','gamma_stat_TRMMEM_cuts_bin_0','gamma_stat_VRtt1offEM_cuts_bin_0','gamma_stat_VRtt1offEl_cuts_bin_0','gamma_stat_VRtt1offMu_cuts_bin_0','gamma_stat_VRtt1onEM_cuts_bin_0','gamma_stat_VRtt1onEl_cuts_bin_0','gamma_stat_VRtt1onMu_cuts_bin_0','gamma_stat_VRtt2offEM_cuts_bin_0','gamma_stat_VRtt2offEl_cuts_bin_0','gamma_stat_VRtt2offMu_cuts_bin_0','gamma_stat_VRtt2onEM_cuts_bin_0','gamma_stat_VRtt2onEl_cuts_bin_0','gamma_stat_VRtt2onMu_cuts_bin_0','gamma_stat_VRtt3offEM_cuts_bin_0','gamma_stat_VRtt3offEl_cuts_bin_0','gamma_stat_VRtt3offMu_cuts_bin_0','gamma_stat_VRtt3onEM_cuts_bin_0','gamma_stat_VRtt3onEl_cuts_bin_0','gamma_stat_VRtt3onMu_cuts_bin_0','gamma_stat_WREM_cuts_bin_0']
namemap['mu'] = ['mu_ST','mu_Top_bin1','mu_Top_bin2','mu_Top_bin3','mu_W']
namemap['theo']=['alpha_TtbarGenerator','alpha_TtbarPS','alpha_TtbarRadiation','alpha_WjetsAlphaVariation','alpha_WjetsCKKW','alpha_WjetsPDF','alpha_WjetsQSF','alpha_WjetsScale','alpha_ZjetsCKKW','alpha_ZjetsQSF','alpha_ZjetsScale','alpha_diboson','alpha_singletopGenerator','alpha_singletopInterference','alpha_singletopPS','alpha_singletopRadiation','alpha_ttV']
namemap['exp']=['alpha_EG_Eff','alpha_EG_Iso','alpha_EG_RESOLUTION_ALL','alpha_EG_Reco','alpha_EG_SCALE_ALL','alpha_JER','alpha_JES_Group1','alpha_JES_Group2','alpha_JES_Group3','alpha_JET','alpha_JVT','alpha_MET_SoftTrk','alpha_MET_SoftTrk_ResoPara','alpha_MET_SoftTrk_ResoPerp','alpha_MUON_Eff_Iso_stat','alpha_MUON_Eff_Iso_sys','alpha_MUON_Eff_stat','alpha_MUON_Eff_stat_lowpt','alpha_MUON_Eff_sys','alpha_MUON_Eff_sys_lowpt','alpha_MUON_ID','alpha_MUON_MS','alpha_MUON_SCALE','alpha_MUON_TTVA_stat','alpha_MUON_TTVA_sys','alpha_btag_BT','alpha_btag_CT','alpha_btag_Extra','alpha_btag_ExtraFromCharm','alpha_btag_LightT','alpha_pileup','alpha_trigger']









