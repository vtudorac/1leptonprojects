# 1L analysis

Contains all Histfitter configurations for various analyses. Including Histfitter itself as a submodule (currently tag `v0.63.0`).

### Setting up and compiling HistFitter

```
cd HistFitter/src
make
cd ..
source setup.sh
```

### Wh ANA-SUSY-2019-08

The Histfitter config file to use is `OneLeptonbb_Winter19.py`. This is also what is used in RECAST.

In order to run an exclusion fit for this, do e.g. the usual `Histfitter.py -wtp -F excl -r LM,MM,HM -g C1N2_Wh_hbb_700p0_50p0 python/OneLeptonbb_Winter19.py` for an exclusion fit for a this specific signal point.