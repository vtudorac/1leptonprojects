void readFileBkg(TString chain,const char *file_name,TH1F*&h1,TH1F*&h2,TH1F*&h3,TH1F*&h4,TH1F*&h5,float lumi,bool LMresolved,bool HMresolved,bool LMboosted,bool HMboosted,bool MMboosted);
void readFileSignal(TString chainname, const char *file_name, TH1F *&hSRLMresolved,TH1F *&hSRHMresolved,TH1F *&hSRLMboosted,TH1F *&hSRMMboosted,TH1F *&hSRHMboosted, float lumi,bool LMresolved,bool HMresolved,bool LMboosted,bool HMboosted,bool MMboosted);

#include "TH1.h"
#include "THStack.h"





void ZnDiscoverySRWW()
{
       // int nthreads = 5;
//ROOT::EnableImplicitMT(nthreads);

	gStyle->SetPadTickY(1);
	gStyle->SetPadTickX(1);
	gStyle->SetOptStat(0);
	gStyle->SetPalette(1);     
	
	TH1F *SRLMresolved;
	TH1F *SRHMresolved;
	TH1F *SRLMboosted;
	TH1F *SRMMboosted;
	TH1F *SRHMboosted;
	
	
	
	double ZnLMresolved[54];
	double ZnHMresolved[54];
	double ZnLMboosted[54];
	double ZnHMboosted[54];
	double ZnMMboosted[54];
	
	TH1F *TotBkgLMresolved;
	TH1F *SRLMresolvedWjets;
	TH1F *SRLMresolvedttbar;
	TH1F *SRLMresolvedZjets;
	TH1F *SRLMresolvedTtv;
	TH1F *SRLMresolvedDB;
	TH1F *SRLMresolvedST;
	TH1F *SRLMresolvedTth;
	TH1F *SRLMresolvedMB;
	TH1F *SRLMresolvedVh;
	
	TH1F *TotBkgHMresolved;
	TH1F *SRHMresolvedWjets;
	TH1F *SRHMresolvedttbar;
	TH1F *SRHMresolvedZjets;
	TH1F *SRHMresolvedTtv;
	TH1F *SRHMresolvedDB;
	TH1F *SRHMresolvedST;
	TH1F *SRHMresolvedTth;
	TH1F *SRHMresolvedMB;
	TH1F *SRHMresolvedVh;
	
	TH1F *TotBkgLMboosted;
	TH1F *SRLMboostedWjets;
	TH1F *SRLMboostedttbar;
	TH1F *SRLMboostedZjets;
	TH1F *SRLMboostedTtv;
	TH1F *SRLMboostedDB;
	TH1F *SRLMboostedST;
	TH1F *SRLMboostedTth;
	TH1F *SRLMboostedMB;
	TH1F *SRLMboostedVh;
	
	TH1F *TotBkgHMboosted;
	TH1F *SRHMboostedWjets;
	TH1F *SRHMboostedttbar;
	TH1F *SRHMboostedZjets;
	TH1F *SRHMboostedTtv;
	TH1F *SRHMboostedDB;
	TH1F *SRHMboostedST;
	TH1F *SRHMboostedTth;
	TH1F *SRHMboostedMB;
	TH1F *SRHMboostedVh;
	
	TH1F *TotBkgMMboosted;
	TH1F *SRMMboostedWjets;
	TH1F *SRMMboostedttbar;
	TH1F *SRMMboostedZjets;
	TH1F *SRMMboostedTtv;
	TH1F *SRMMboostedDB;
	TH1F *SRMMboostedST;
	TH1F *SRMMboostedTth;
	TH1F *SRMMboostedMB;
	TH1F *SRMMboostedVh;
	
	
	float lumi=139000;
	double err;
	
	bool LMresolved=true;
	bool HMresolved=true;
	bool LMboosted=true;
	bool HMboosted=true;
	bool MMboosted=true;
	
	
	
	
	double TotalBkgYieldsLMresolved;
	double TotalBkgYieldsHMresolved;
	double TotalBkgYieldsLMboosted;
	double TotalBkgYieldsHMboosted;
	double TotalBkgYieldsMMboosted;
	
	

	double SigYieldsLMresolved[54];
	double SigYieldsHMresolved[54];
	double SigYieldsLMboosted[54];
	double SigYieldsHMboosted[54];
	double SigYieldsMMboosted[54];
	
	

//===================================WW==================================
	
	TH2D *histLMresolved = new TH2D("histLMresolved","",100, 225., 1200., 150, 0., 600. );
	TGraph2D *grLMresolved = new TGraph2D();
	
	TH2D *histHMresolved = new TH2D("histHMresolved","",100, 225., 1200., 150, 0., 600. );
	TGraph2D *grHMresolved = new TGraph2D();
	
	TH2D *histLMboosted = new TH2D("histLMboosted","",100, 225., 1200., 150, 0., 600. );
	TGraph2D *grLMboosted = new TGraph2D();
	
	TH2D *histHMboosted = new TH2D("histHMboosted","",100, 225., 1200., 150, 0., 600. );
	TGraph2D *grHMboosted = new TGraph2D();
	
	TH2D *histMMboosted = new TH2D("histMMboosted","",100, 225., 1200., 150, 0., 600. );
	TGraph2D *grMMboosted = new TGraph2D();
	
        
	//  std::cout << "********************************************************" << std::endl;
	//  std::cout << "* you need to place the root inputs in <input.txt> ..." << std::endl;
	std::ifstream intxtFileLMresolved;
	intxtFileLMresolved.open("/data/vtudorac/EWK2ndWave/Discovery/ww.txt"); //"SM_LMresolved1step_625_545_465_NoSys"

	std::string filenameLMresolved;
	int i=0;
	double valx[54];
	double valy[54];
	double valZLMresolved[54];
	int idx = 0;
	
	while(!intxtFileLMresolved.eof())
	{      
		std::getline(intxtFileLMresolved, filenameLMresolved);

		//get first number
		size_t index1 = filenameLMresolved.find("_");
		index1 = filenameLMresolved.find("_",index1+1);
		size_t index2 = filenameLMresolved.find("_",index1+1);
		int first = atoi(filenameLMresolved.substr(index1+1,index2-index1+1).c_str());
		//get second number
		index1 = filenameLMresolved.rfind("_");
		int second = atoi(filenameLMresolved.substr(index1+1).c_str());
		std::cout << first << " " << second << std::endl;
		valx[idx] = first;
		valy[idx] = second;
		idx++;

		if (filenameLMresolved.size()<2)
		{
			break;
		}
		std::cout<<filenameLMresolved.c_str()<< "   ";
		std::string chainname1 = filenameLMresolved+"_NoSys";
		//cout<<chainname1<<endl;


		TChain *inTree = new TChain(chainname1.c_str());

		std::string rootfile="/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_sig_NoSys.root";

		//cout<<rootfile<<endl;

		inTree->Add(rootfile.c_str());

		//}
		
		//LMresolved=true;
		
		readFileSignal(chainname1,"/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_sig_NoSys.root",SRLMresolved,SRHMresolved,SRLMboosted,SRMMboosted,SRHMboosted,lumi,true,true,true,true,true);	

                double range1=SRLMresolved->GetXaxis()->GetNbins();
		SRLMresolved->Integral(0,range1);
		cout<<range1<<endl;
		std::cout << chainname1<<"=" << SRLMresolved->Integral (0,range1+1)<<" "<<SRHMresolved->Integral (0,range1+1)<<SRLMboosted->Integral (0,range1+1)<<SRHMboosted->Integral (0,range1+1)<<SRMMboosted->Integral (0,range1+1)<<std::endl;

		SigYieldsLMresolved[i]=(SRLMresolved->Integral (0,range1+1));
		SigYieldsHMresolved[i]=(SRHMresolved->Integral (0,range1+1));
		SigYieldsLMboosted[i]=(SRLMboosted->Integral (0,range1+1));
		SigYieldsMMboosted[i]=(SRMMboosted->Integral (0,range1+1));
		SigYieldsHMboosted[i]=(SRHMboosted->Integral (0,range1+1));
		
		
		//SigYieldsLMresolvedgridx[i]=(SRLMresolvedSignalLMresolvedgridx->Integral (0,range1+1));
		
		cout<<"i="<<i<<" yields="<<SigYieldsLMresolved[i]<<endl;
		cout<<"i="<<i<<" yields="<<SigYieldsHMresolved[i]<<endl;
		cout<<"i="<<i<<" yields="<<SigYieldsLMboosted[i]<<endl;
		cout<<"i="<<i<<" yields="<<SigYieldsMMboosted[i]<<endl;
		cout<<"i="<<i<<" yields="<<SigYieldsHMboosted[i]<<endl;
		
		i++;
		
	
		delete SRLMresolved;
		delete SRHMresolved;
		delete SRLMboosted;
		delete SRHMboosted;
		delete SRMMboosted;
		

	} 

	
//===================================Bckg==================================		
        
	readFileBkg("ttbar_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedttbar,SRHMresolvedttbar,SRLMboostedttbar,SRMMboostedttbar,SRHMboostedttbar,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);

	readFileBkg("wjets_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedWjets,SRHMresolvedWjets,SRLMboostedWjets,SRMMboostedWjets,SRHMboostedWjets,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
	readFileBkg("zjets_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedZjets,SRHMresolvedZjets,SRLMboostedZjets,SRMMboostedZjets,SRHMboostedZjets,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
        readFileBkg("diboson_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedDB,SRHMresolvedDB,SRLMboostedDB,SRMMboostedDB,SRHMboostedDB,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
        readFileBkg("singletop_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedST,SRHMresolvedST,SRLMboostedST,SRMMboostedST,SRHMboostedST,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
        readFileBkg("ttv_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedTtv,SRHMresolvedTtv,SRLMboostedTtv,SRMMboostedTtv,SRHMboostedTtv,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
	readFileBkg("tth_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedTth,SRHMresolvedTth,SRLMboostedTth,SRMMboostedTth,SRHMboostedTth,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
	readFileBkg("vh_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedVh,SRHMresolvedVh,SRLMboostedVh,SRMMboostedVh,SRHMboostedVh,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
	readFileBkg("multiboson_NoSys","/data/vtudorac/EWK2ndWave/NominalProduction/allTrees_bkg_NoSys.root",SRLMresolvedMB,SRHMresolvedMB,SRLMboostedMB,SRMMboostedMB,SRHMboostedMB,lumi,LMresolved,HMresolved,LMboosted,HMboosted,MMboosted);
	
	
	
	
	//readFileSignal("LMresolved_oneStep_1865_945_25_NoSys","/data/vtudorac/StrongTrees/Strong/user.eschanet.allTrees_v2_0_5_signal_strong1L_skim_simpleJER.root ",SRLMresolved,SRLMresolvedSignalLMresolvedgridx,SRLMresolvedSignalSSx12,SRLMresolvedSignalSSgridx,lumi,LMresolved,LMresolvedgridx,SSx12,SSgridx);
	
	

	
	TotBkgLMresolved= new TH1F("TotBkgLMresolved","TotBkgLMresolved",10,0,500);
	TotBkgLMresolved->Add(SRLMresolvedTtv); 
	TotBkgLMresolved->Add(SRLMresolvedZjets);
	TotBkgLMresolved->Add(SRLMresolvedttbar);
	TotBkgLMresolved->Add(SRLMresolvedWjets);
	TotBkgLMresolved->Add(SRLMresolvedST);
	TotBkgLMresolved->Add(SRLMresolvedDB);
	TotBkgLMresolved->Add(SRLMresolvedVh);
	TotBkgLMresolved->Add(SRLMresolvedTth);
	TotBkgLMresolved->Add(SRLMresolvedMB);
	
	TotBkgHMresolved= new TH1F("TotBkgHMresolved","TotBkgHMresolved",10,0,500);
	TotBkgHMresolved->Add(SRHMresolvedTtv); 
	TotBkgHMresolved->Add(SRHMresolvedZjets);
	TotBkgHMresolved->Add(SRHMresolvedttbar);
	TotBkgHMresolved->Add(SRHMresolvedWjets);
	TotBkgHMresolved->Add(SRHMresolvedST);
	TotBkgHMresolved->Add(SRHMresolvedDB);
	TotBkgHMresolved->Add(SRHMresolvedVh);
	TotBkgHMresolved->Add(SRHMresolvedTth);
	TotBkgHMresolved->Add(SRHMresolvedMB);
	
	TotBkgLMboosted= new TH1F("TotBkgLMboosted","TotBkgLMboosted",10,0,500);
	TotBkgLMboosted->Add(SRLMboostedTtv); 
	TotBkgLMboosted->Add(SRLMboostedZjets);
	TotBkgLMboosted->Add(SRLMboostedttbar);
	TotBkgLMboosted->Add(SRLMboostedWjets);
	TotBkgLMboosted->Add(SRLMboostedST);
	TotBkgLMboosted->Add(SRLMboostedDB);
	TotBkgLMboosted->Add(SRLMboostedVh);
	TotBkgLMboosted->Add(SRLMboostedTth);
	TotBkgLMboosted->Add(SRLMboostedMB);
	
	TotBkgMMboosted= new TH1F("TotBkgMMboosted","TotBkgMMboosted",10,0,500);
	TotBkgMMboosted->Add(SRMMboostedTtv); 
	TotBkgMMboosted->Add(SRMMboostedZjets);
	TotBkgMMboosted->Add(SRMMboostedttbar);
	TotBkgMMboosted->Add(SRMMboostedWjets);
	TotBkgMMboosted->Add(SRMMboostedST);
	TotBkgMMboosted->Add(SRMMboostedDB);
	TotBkgMMboosted->Add(SRMMboostedVh);
	TotBkgMMboosted->Add(SRMMboostedTth);
	TotBkgMMboosted->Add(SRMMboostedMB);
	
	TotBkgHMboosted= new TH1F("TotBkgHMboosted","TotBkgHMboosted",10,0,500);
	TotBkgHMboosted->Add(SRHMboostedTtv); 
	TotBkgHMboosted->Add(SRHMboostedZjets);
	TotBkgHMboosted->Add(SRHMboostedttbar);
	TotBkgHMboosted->Add(SRHMboostedWjets);
	TotBkgHMboosted->Add(SRHMboostedST);
	TotBkgHMboosted->Add(SRHMboostedDB);
	TotBkgHMboosted->Add(SRHMboostedVh);
	TotBkgHMboosted->Add(SRHMboostedTth);
	TotBkgHMboosted->Add(SRHMboostedMB);
	
	
        double range=SRLMresolvedttbar->GetXaxis()->GetNbins();
	
	cout<<"range="<<range<<endl;//pot pune SRLMresolvedTtbar->IntegralAndError (0, 200, err);
	//SRLMresolvedttbar->IntegralAndError (0, range, err);
	std::cout << "SRLMresolvedttbar="  << SRLMresolvedttbar->Integral (0,range+1)<<" SRHMresolvedttbar= "<<SRHMresolvedttbar->Integral (0,range+1) << " SRLMboostedttbar=" <<SRLMboostedttbar->Integral (0,range+1) << " SRMMboostedttbar=" <<SRMMboostedttbar->Integral (0,range+1)<< "SRHMboostedttbar=" <<SRHMboostedttbar->Integral (0,range+1)<<std::endl;	
	
	std::cout << "SRLMresolvedZjets="  << SRLMresolvedZjets->Integral (0,range+1)<<"SRHMresolvedZjets= "<<SRHMresolvedZjets->Integral (0,range+1) << "SRLMboostedZjets=" <<SRLMboostedZjets->Integral (0,range+1) << " SRMMboostedZjets=" <<SRMMboostedWjets->Integral (0,range+1)<< "SRHMboostedZjets=" <<SRHMboostedZjets->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedWjets="  << SRLMresolvedWjets->Integral (0,range+1)<<"SRHMresolvedWjets= "<<SRHMresolvedWjets->Integral (0,range+1) << "SRLMboostedWjets=" <<SRLMboostedWjets->Integral (0,range+1) << " SRMMboostedWjets=" <<SRMMboostedWjets->Integral (0,range+1)<< "SRHMboostedWjets=" <<SRHMboostedWjets->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedTtv="    << SRLMresolvedTtv->Integral (0,range+1)<<"SRHMresolvedTtv= "<<SRHMresolvedTtv->Integral (0,range+1) << "SRLMboostedTtv=" <<SRLMboostedTtv->Integral (0,range+1) << "SRMMboostedTtv=" <<SRMMboostedTtv->Integral (0,range+1)<< "SRHMboostedTtv=" <<SRHMboostedTtv->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedDB="     << SRLMresolvedDB->Integral (0,range+1)<<"SRHMresolvedDB= "<<SRHMresolvedDB->Integral (0,range+1) << "SRLMboostedDB" <<SRLMboostedDB->Integral (0,range+1) << "SRMMboostedDB=" <<SRMMboostedDB->Integral (0,range+1)<< "SRHMboostedDB=" <<SRHMboostedDB->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedST="     << SRLMresolvedST->Integral (0,range+1)<<"SRHMresolvedST= "<<SRHMresolvedST->Integral (0,range+1) << "SRLMboostedST=" <<SRLMboostedST->Integral (0,range+1) << "SRMMboostedST=" <<SRMMboostedST->Integral (0,range+1)<< "SRHMboostedST=" <<SRHMboostedST->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedTth="    << SRLMresolvedTth->Integral (0,range+1)<<"SRHMresolvedTth= "<<SRHMresolvedTth->Integral (0,range+1) << "SRLMboostedTth=" <<SRLMboostedTth->Integral (0,range+1) << "SRMMboostedTth=" <<SRMMboostedTth->Integral (0,range+1)<< "SRHMboostedTth=" <<SRHMboostedTth->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedMB="    << SRLMresolvedMB->Integral (0,range+1)<<"SRHMresolvedMB= "<<SRHMresolvedMB->Integral (0,range+1) << "SRLMboostedMB=" <<SRLMboostedMB->Integral (0,range+1) << "SRMMboostedMB=" <<SRMMboostedMB->Integral (0,range+1)<< "SRHMboostedMB=" <<SRHMboostedMB->Integral (0,range+1)<<std::endl;
	
	std::cout << "SRLMresolvedVh="    << SRLMresolvedVh->Integral (0,range+1)<<"SRHMresolvedVh= "<<SRHMresolvedVh->Integral (0,range+1) << "SRLMboostedVh=" <<SRLMboostedVh->Integral (0,range+1) << "SRMMboostedVh=" <<SRMMboostedVh->Integral (0,range+1)<< "SRHMboostedVh=" <<SRHMboostedVh->Integral (0,range+1)<<std::endl;
	
	
	
	
	std::cout << " TotBkgLMresolved="  <<  TotBkgLMresolved->Integral (0,range+1)<< " TotBkgHMresolved="  <<  TotBkgHMresolved->Integral (0,range+1) << "TotBkgLMboosted=" <<TotBkgLMboosted->Integral (0,range+1)<< "TotBkgMMboosted=" <<TotBkgMMboosted->Integral (0,range+1) << "TotBkgHMboosted=" <<TotBkgHMboosted->Integral (0,range+1)<<std::endl;
	
	
	TotalBkgYieldsLMresolved=TotBkgLMresolved->Integral (0,range+1);
	TotalBkgYieldsHMresolved=TotBkgHMresolved->Integral (0,range+1);
	TotalBkgYieldsLMboosted= TotBkgLMboosted->Integral (0,range+1);
	TotalBkgYieldsMMboosted= TotBkgMMboosted->Integral (0,range+1);
	TotalBkgYieldsHMboosted= TotBkgHMboosted->Integral (0,range+1);
	
	
	
	
//===================================WW SRLM Resolved==================================	
		
        //TFile* fileLMresolved= new TFile("/data/vtudorac/HistFitter63/HistFitterUser/MET_jets_leptons/contourplot/contourmacros/limit_OneLepton_LMresolved_2J_4Jlowx_4Jhighx_6J_25042017curves.root");
	//TH2D *hist1LMresolved = (TH2D*)fileLMresolved->Get("firstObsH_graph0");
	//TH2D *hist2 = (TH2D*)fileLMresolved->Get("firstObsH_graph1");
	//TH2D *hist3LMresolved = (TH2D*)fileLMresolved->Get("firstExpH_graph0");
	
	double x,y;
	for(int i=0;i<54;i++)
	{

		cout<<"SigYilds= "<<SigYieldsLMresolved[i]<<" BkgYields=  "<<TotalBkgYieldsLMresolved<<endl;
			
		
		
		
		ZnLMresolved[i]=RooStats::NumberCountingUtils::BinomialExpZ(SigYieldsLMresolved[i],TotalBkgYieldsLMresolved,0.30);
		
		//cout<<MaxRatioLMresolved[i]<<endl;
		x=valx[i]; y=valy[i];
		
		cout<<"x="<<x<<"  y="<<y<<" "<<"Zn= "<<ZnLMresolved[i]<<endl;
		if(ZnLMresolved[i]<0)
		{	
		histLMresolved->Fill(x,y,0.00001);
		grLMresolved->SetPoint (i,x,y,0.00001) ;
		}
		if(ZnLMresolved[i]>0)
		{	
		histLMresolved->Fill(x,y,ZnLMresolved[i]);
		grLMresolved->SetPoint (i,x,y,ZnLMresolved[i]) ;
		}
		
	
	
	}
	
	
	TCanvas* c1LMresolved = new TCanvas;
	histLMresolved->Draw();
	grLMresolved->Draw("COLZ same");
	grLMresolved->SetMaximum(5);
	//grLMresolved->SetMinimum(0);
	
        //histLMresolved->GetZaxis()->SetRangeUser(-0.0001,5);

	gStyle->SetPaintTextFormat("1.2f");
	grLMresolved->SetMarkerSize(2);
	grLMresolved->GetZaxis()->SetTitle( "Zn" );
	histLMresolved->GetYaxis()->SetTitle("m_{#tilde{#chi_{1}^{0}}} [GeV]    ");
	histLMresolved->GetXaxis()->SetTitle("m_{#tilde{#chi^{#pm}_{1}}/#tilde{#chi_{2}^{0}}} [GeV]    ");
	//hist->GetZaxis()->SetTitle( "signal yields" );
	histLMresolved->SetMarkerSize(1.2);
	histLMresolved->Draw("same TEXT0");
	histLMresolved->Draw("axis same ");
	/*
	hist1LMresolved->SetLineWidth(3);
	//hist2->SetLineWidth(3);
	hist3LMresolved->SetLineWidth(3);
        hist3LMresolved->SetLineStyle(3);
	hist1LMresolved->SetLineColor(kBlack);	
	hist3LMresolved->SetLineColor(kBlack);
	//hist2->SetLineColor(kBlack);	
        hist1LMresolved->Draw("cont2same");
	//hist2->Draw("cont2same");
	hist3LMresolved->Draw("cont2same");
	
	TLegend *leg2LMresolved = new TLegend(0.20,0.70,0.30,0.60);   
    	
    	leg2LMresolved->SetTextSize( 0.03 );
    	leg2LMresolved->SetTextFont( 42 );
    	leg2LMresolved->SetFillColor( 0 );
	leg2LMresolved->SetBorderSize(0);
	leg2LMresolved->AddEntry(hist1LMresolved,"observed","l");
	leg2LMresolved->AddEntry(hist3LMresolved,"expected","l");
    	leg2LMresolved->Draw();
	
	*/
	TLatex* textLMresolved=new TLatex ;
	textLMresolved->SetTextFont(42);
	textLMresolved->SetTextSize(0.035);
	textLMresolved->SetTextColor(kBlack);
	textLMresolved->SetNDC(true);
	textLMresolved->DrawLatex(0.20,0.83,"#bf{#it{ATLAS}} Internal");
	textLMresolved->DrawLatex(0.20,0.78,"One-Lepton");
	c1LMresolved->SetRightMargin(0.15);
	c1LMresolved->Draw();
	
        
	textLMresolved->DrawLatex(0.20,0.73,"WW SRLM-resolved");
	c1LMresolved->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryLMresolved_WW.pdf");
        c1LMresolved->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryLMresolved_WW.eps");
	c1LMresolved->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryLMresolved_WW.png");


//===================================WW SRHM Resolved==================================	
		
        //TFile* fileHMresolved= new TFile("/data/vtudorac/HistFitter63/HistFitterUser/MET_jets_leptons/contourplot/contourmacros/limit_OneLepton_LMresolved_2J_4Jlowx_4Jhighx_6J_25042017curves.root");
	//TH2D *hist1HMresolved = (TH2D*)fileHMresolved->Get("firstObsH_graph0");
	//TH2D *hist2 = (TH2D*)fileHMresolved->Get("firstObsH_graph1");
	//TH2D *hist3HMresolved = (TH2D*)fileHMresolved->Get("firstExpH_graph0");
	
	double xHMresolved,yHMresolved;
	for(int i=0;i<54;i++)
	{

		cout<<"SigYilds= "<<SigYieldsHMresolved[i]<<" BkgYields=  "<<TotalBkgYieldsHMresolved<<endl;
			
		
		
		
		ZnHMresolved[i]=RooStats::NumberCountingUtils::BinomialExpZ(SigYieldsHMresolved[i],TotalBkgYieldsHMresolved,0.30);
		
		//cout<<MaxRatioLMresolved[i]<<endl;
		xHMresolved=valx[i]; yHMresolved=valy[i];
		
		cout<<"xHMresolved="<<xHMresolved<<"  yHMresolved="<<yHMresolved<<" "<<"Zn= "<<ZnLMresolved[i]<<endl;
		if(ZnHMresolved[i]<0)
		{	
		histHMresolved->Fill(xHMresolved,yHMresolved,0.00001);
		grHMresolved->SetPoint (i,xHMresolved,yHMresolved,0.00001) ;
		}
		if(ZnHMresolved[i]>0)
		{	
		histHMresolved->Fill(xHMresolved,yHMresolved,ZnHMresolved[i]);
		grHMresolved->SetPoint (i,xHMresolved,yHMresolved,ZnHMresolved[i]) ;
		}
		
	
	
	}
	
	
	TCanvas* c1HMresolved = new TCanvas;
	histHMresolved->Draw();
	grHMresolved->Draw("COLZ same");
	grHMresolved->SetMaximum(5);
	//grHMresolved->SetMinimum(0);
	
        //histHMresolved->GetZaxis()->SetRangeUser(-0.0001,5);

	gStyle->SetPaintTextFormat("1.2f");
	grHMresolved->SetMarkerSize(2);
	grHMresolved->GetZaxis()->SetTitle( "Zn" );
	histHMresolved->GetYaxis()->SetTitle("m_{#tilde{#chi_{1}^{0}}} [GeV]    ");
	histHMresolved->GetXaxis()->SetTitle("m_{#tilde{#chi^{#pm}_{1}}/#tilde{#chi_{2}^{0}}} [GeV]    ");
	//hist->GetZaxis()->SetTitle( "signal yields" );
	histHMresolved->SetMarkerSize(1.2);
	histHMresolved->Draw("same TEXT0");
	histHMresolved->Draw("axis same ");
	/*
	hist1HMresolved->SetLineWidth(3);
	//hist2->SetLineWidth(3);
	hist3HMresolved->SetLineWidth(3);
        hist3HMresolved->SetLineStyle(3);
	hist1HMresolved->SetLineColor(kBlack);	
	hist3HMresolved->SetLineColor(kBlack);
	//hist2->SetLineColor(kBlack);	
        hist1HMresolved->Draw("cont2same");
	//hist2->Draw("cont2same");
	hist3HMresolved->Draw("cont2same");
	
	TLegend *leg2HMresolved = new TLegend(0.20,0.70,0.30,0.60);   
    	
    	leg2HMresolved->SetTextSize( 0.03 );
    	leg2HMresolved->SetTextFont( 42 );
    	leg2HMresolved->SetFillColor( 0 );
	leg2HMresolved->SetBorderSize(0);
	leg2HMresolved->AddEntry(hist1HMresolved,"observed","l");
	leg2HMresolved->AddEntry(hist3HMresolved,"expected","l");
    	leg2HMresolved->Draw();
	
	*/
	TLatex* textHMresolved=new TLatex ;
	textHMresolved->SetTextFont(42);
	textHMresolved->SetTextSize(0.035);
	textHMresolved->SetTextColor(kBlack);
	textHMresolved->SetNDC(true);
	textHMresolved->DrawLatex(0.20,0.83,"#bf{#it{ATLAS}} Internal");
	textHMresolved->DrawLatex(0.20,0.78,"One-Lepton");
	c1HMresolved->SetRightMargin(0.15);
	c1HMresolved->Draw();
	
        
	textHMresolved->DrawLatex(0.20,0.73,"WW SRHM-resolved");
	c1HMresolved->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryHMresolved_WW.pdf");
        c1HMresolved->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryHMresolved_WW.eps");
	c1HMresolved->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryHMresolved_WW.png");


//===================================WW SRLM boosted==================================	
		
        //TFile* fileLMboosted= new TFile("/data/vtudorac/HistFitter63/HistFitterUser/MET_jets_leptons/contourplot/contourmacros/limit_OneLepton_LMboosted_2J_4Jlowx_4Jhighx_6J_25042017curves.root");
	//TH2D *hist1LMboosted = (TH2D*)fileLMboosted->Get("firstObsH_graph0");r
	//TH2D *hist2 = (TH2D*)fileLMboosted->Get("firstObsH_graph1");
	//TH2D *hist3LMboosted = (TH2D*)fileLMboosted->Get("firstExpH_graph0");
	
	double xLMresolved,yLMresolved;
	for(int i=0;i<54;i++)
	{

		cout<<"SigYilds= "<<SigYieldsLMboosted[i]<<" BkgYields=  "<<TotalBkgYieldsLMboosted<<endl;
			
		
		
		
		ZnLMboosted[i]=RooStats::NumberCountingUtils::BinomialExpZ(SigYieldsLMboosted[i],TotalBkgYieldsLMboosted,0.30);
		
		//cout<<MaxRatioLMboosted[i]<<endl;
		xLMresolved=valx[i]; yLMresolved=valy[i];
		
		cout<<"x="<<xLMresolved<<"  y="<<yLMresolved<<" "<<"Zn= "<<ZnLMboosted[i]<<endl;
		if(ZnLMboosted[i]<0)
		{	
		histLMboosted->Fill(xLMresolved,yLMresolved,0.00001);
		grLMboosted->SetPoint (i,xLMresolved,yLMresolved,0.00001) ;
		}
		if(ZnLMboosted[i]>0)
		{	
		histLMboosted->Fill(xLMresolved,yLMresolved,ZnLMboosted[i]);
		grLMboosted->SetPoint (i,xLMresolved,yLMresolved,ZnLMboosted[i]) ;
		}
		
	
	
	}
	
	
	TCanvas* c1LMboosted = new TCanvas;
	histLMboosted->Draw();
	grLMboosted->Draw("COLZ same");
	grLMboosted->SetMaximum(5);
	//grLMboosted->SetMinimum(0);
	
        //histLMboosted->GetZaxis()->SetRangeUser(-0.0001,5);

	gStyle->SetPaintTextFormat("1.2f");
	grLMboosted->SetMarkerSize(2);
	grLMboosted->GetZaxis()->SetTitle( "Zn" );
	histLMboosted->GetYaxis()->SetTitle("m_{#tilde{#chi_{1}^{0}}} [GeV]    ");
	histLMboosted->GetXaxis()->SetTitle("m_{#tilde{#chi^{#pm}_{1}}/#tilde{#chi_{2}^{0}}} [GeV]    ");
	//hist->GetZaxis()->SetTitle( "signal yields" );
	histLMboosted->SetMarkerSize(1.2);
	histLMboosted->Draw("same TEXT0");
	histLMboosted->Draw("axis same ");
	/*
	hist1LMboosted->SetLineWidth(3);
	//hist2->SetLineWidth(3);
	hist3LMboosted->SetLineWidth(3);
        hist3LMboosted->SetLineStyle(3);
	hist1LMboosted->SetLineColor(kBlack);	
	hist3LMboosted->SetLineColor(kBlack);
	//hist2->SetLineColor(kBlack);	
        hist1LMboosted->Draw("cont2same");
	//hist2->Draw("cont2same");
	hist3LMboosted->Draw("cont2same");
	
	TLegend *leg2LMboosted = new TLegend(0.20,0.70,0.30,0.60);   
    	
    	leg2LMboosted->SetTextSize( 0.03 );
    	leg2LMboosted->SetTextFont( 42 );
    	leg2LMboosted->SetFillColor( 0 );
	leg2LMboosted->SetBorderSize(0);
	leg2LMboosted->AddEntry(hist1LMboosted,"observed","l");
	leg2LMboosted->AddEntry(hist3LMboosted,"expected","l");
    	leg2LMboosted->Draw();
	
	*/
	TLatex* textLMboosted=new TLatex ;
	textLMboosted->SetTextFont(42);
	textLMboosted->SetTextSize(0.035);
	textLMboosted->SetTextColor(kBlack);
	textLMboosted->SetNDC(true);
	textLMboosted->DrawLatex(0.20,0.83,"#bf{#it{ATLAS}} Internal");
	textLMboosted->DrawLatex(0.20,0.78,"One-Lepton");
	c1LMboosted->SetRightMargin(0.15);
	c1LMboosted->Draw();
	
        
	textLMboosted->DrawLatex(0.20,0.73,"WW SRLM-boosted");
	c1LMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryLMboosted_WW.pdf");
        c1LMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryLMboosted_WW.eps");
	c1LMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryLMboosted_WW.png");
	

//===================================WW SRHM boosted==================================	
		
        //TFile* fileHMboosted= new TFile("/data/vtudorac/HistFitter63/HistFitterUser/MET_jets_leptons/contourplot/contourmacros/limit_OneLepton_HMboosted_2J_4Jlowx_4Jhighx_6J_25042017curves.root");
	//TH2D *hist1HMboosted = (TH2D*)fileHMboosted->Get("firstObsH_graph0");r
	//TH2D *hist2 = (TH2D*)fileHMboosted->Get("firstObsH_graph1");
	//TH2D *hist3HMboosted = (TH2D*)fileHMboosted->Get("firstExpH_graph0");
	
	double xHMboosted,yHMboosted;
	for(int i=0;i<54;i++)
	{

		cout<<"SigYilds= "<<SigYieldsHMboosted[i]<<" BkgYields=  "<<TotalBkgYieldsHMboosted<<endl;
			
		
		
		
		ZnHMboosted[i]=RooStats::NumberCountingUtils::BinomialExpZ(SigYieldsHMboosted[i],TotalBkgYieldsHMboosted,0.30);
		
		//cout<<MaxRatioHMboosted[i]<<endl;
		xHMboosted=valx[i]; yHMboosted=valy[i];
		
		cout<<"x="<<xHMboosted<<"  y="<<yHMboosted<<" "<<"Zn= "<<ZnHMboosted[i]<<endl;
		if(ZnHMboosted[i]<0)
		{	
		histHMboosted->Fill(xHMboosted,yHMboosted,0.00001);
		grHMboosted->SetPoint (i,xHMboosted,yHMboosted,0.00001) ;
		}
		if(ZnHMboosted[i]>0)
		{	
		histHMboosted->Fill(xHMboosted,yHMboosted,ZnHMboosted[i]);
		grHMboosted->SetPoint (i,xHMboosted,yHMboosted,ZnHMboosted[i]) ;
		}
		
	
	
	}
	
	
	TCanvas* c1HMboosted = new TCanvas;
	histHMboosted->Draw();
	grHMboosted->Draw("COLZ same");
	grHMboosted->SetMaximum(5);
	//grHMboosted->SetMinimum(0);
	
        //histHMboosted->GetZaxis()->SetRangeUser(-0.0001,5);

	gStyle->SetPaintTextFormat("1.2f");
	grHMboosted->SetMarkerSize(2);
	grHMboosted->GetZaxis()->SetTitle( "Zn" );
	histHMboosted->GetYaxis()->SetTitle("m_{#tilde{#chi_{1}^{0}}} [GeV]    ");
	histHMboosted->GetXaxis()->SetTitle("m_{#tilde{#chi^{#pm}_{1}}/#tilde{#chi_{2}^{0}}} [GeV]    ");
	//hist->GetZaxis()->SetTitle( "signal yields" );
	histHMboosted->SetMarkerSize(1.2);
	histHMboosted->Draw("same TEXT0");
	histHMboosted->Draw("axis same ");
	/*
	hist1HMboosted->SetLineWidth(3);
	//hist2->SetLineWidth(3);
	hist3HMboosted->SetLineWidth(3);
        hist3HMboosted->SetLineStyle(3);
	hist1HMboosted->SetLineColor(kBlack);	
	hist3HMboosted->SetLineColor(kBlack);
	//hist2->SetLineColor(kBlack);	
        hist1HMboosted->Draw("cont2same");
	//hist2->Draw("cont2same");
	hist3HMboosted->Draw("cont2same");
	
	TLegend *leg2HMboosted = new TLegend(0.20,0.70,0.30,0.60);   
    	
    	leg2HMboosted->SetTextSize( 0.03 );
    	leg2HMboosted->SetTextFont( 42 );
    	leg2HMboosted->SetFillColor( 0 );
	leg2HMboosted->SetBorderSize(0);
	leg2HMboosted->AddEntry(hist1HMboosted,"observed","l");
	leg2HMboosted->AddEntry(hist3HMboosted,"expected","l");
    	leg2HMboosted->Draw();
	
	*/
	TLatex* textHMboosted=new TLatex ;
	textHMboosted->SetTextFont(42);
	textHMboosted->SetTextSize(0.035);
	textHMboosted->SetTextColor(kBlack);
	textHMboosted->SetNDC(true);
	textHMboosted->DrawLatex(0.20,0.83,"#bf{#it{ATLAS}} Internal");
	textHMboosted->DrawLatex(0.20,0.78,"One-Lepton");
	c1HMboosted->SetRightMargin(0.15);
	c1HMboosted->Draw();
	
        
	textHMboosted->DrawLatex(0.20,0.73,"WW SRHM-boosted");
	c1HMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryHMboosted_WW.pdf");
        c1HMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryHMboosted_WW.eps");
	c1HMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryHMboosted_WW.png");




//===================================WW SRMM boosted==================================	
		
        //TFile* fileMMboosted= new TFile("/data/vtudorac/HistFitter63/HistFitterUser/MET_jets_leptons/contourplot/contourmacros/limit_OneLepton_MMboosted_2J_4Jlowx_4Jhighx_6J_25042017curves.root");
	//TH2D *hist1MMboosted = (TH2D*)fileMMboosted->Get("firstObsH_graph0");r
	//TH2D *hist2 = (TH2D*)fileMMboosted->Get("firstObsH_graph1");
	//TH2D *hist3MMboosted = (TH2D*)fileMMboosted->Get("firstExpH_graph0");
	
	double xMMboosted,yMMboosted;
	for(int i=0;i<54;i++)
	{

		cout<<"SigYilds= "<<SigYieldsMMboosted[i]<<" BkgYields=  "<<TotalBkgYieldsMMboosted<<endl;
			
		
		
		
		ZnMMboosted[i]=RooStats::NumberCountingUtils::BinomialExpZ(SigYieldsMMboosted[i],TotalBkgYieldsMMboosted,0.30);
		
		//cout<<MaxRatioMMboosted[i]<<endl;
		xMMboosted=valx[i]; yMMboosted=valy[i];
		
		cout<<"x="<<xMMboosted<<"  y="<<yMMboosted<<" "<<"Zn= "<<ZnMMboosted[i]<<endl;
		if(ZnMMboosted[i]<0)
		{	
		histMMboosted->Fill(xMMboosted,yMMboosted,0.00001);
		grMMboosted->SetPoint (i,xMMboosted,yMMboosted,0.00001) ;
		}
		if(ZnMMboosted[i]>0)
		{	
		histMMboosted->Fill(xMMboosted,yMMboosted,ZnMMboosted[i]);
		grMMboosted->SetPoint (i,xMMboosted,yMMboosted,ZnMMboosted[i]) ;
		}
		
	
	
	}
	
	
	TCanvas* c1MMboosted = new TCanvas;
	histMMboosted->Draw();
	grMMboosted->Draw("COLZ same");
	grMMboosted->SetMaximum(5);
	//grMMboosted->SetMinimum(0);
	
        //histMMboosted->GetZaxis()->SetRangeUser(-0.0001,5);

	gStyle->SetPaintTextFormat("1.2f");
	grMMboosted->SetMarkerSize(2);
	grMMboosted->GetZaxis()->SetTitle( "Zn" );
	histMMboosted->GetYaxis()->SetTitle("m_{#tilde{#chi_{1}^{0}}} [GeV]    ");
	histMMboosted->GetXaxis()->SetTitle("m_{#tilde{#chi^{#pm}_{1}}/#tilde{#chi_{2}^{0}}} [GeV]    ");
	//hist->GetZaxis()->SetTitle( "signal yields" );
	histMMboosted->SetMarkerSize(1.2);
	histMMboosted->Draw("same TEXT0");
	histMMboosted->Draw("axis same ");
	/*
	hist1MMboosted->SetLineWidth(3);
	//hist2->SetLineWidth(3);
	hist3MMboosted->SetLineWidth(3);
        hist3MMboosted->SetLineStyle(3);
	hist1MMboosted->SetLineColor(kBlack);	
	hist3MMboosted->SetLineColor(kBlack);
	//hist2->SetLineColor(kBlack);	
        hist1MMboosted->Draw("cont2same");
	//hist2->Draw("cont2same");
	hist3MMboosted->Draw("cont2same");
	
	TLegend *leg2MMboosted = new TLegend(0.20,0.70,0.30,0.60);   
    	
    	leg2MMboosted->SetTextSize( 0.03 );
    	leg2MMboosted->SetTextFont( 42 );
    	leg2MMboosted->SetFillColor( 0 );
	leg2MMboosted->SetBorderSize(0);
	leg2MMboosted->AddEntry(hist1MMboosted,"observed","l");
	leg2MMboosted->AddEntry(hist3MMboosted,"expected","l");
    	leg2MMboosted->Draw();
	
	*/
	TLatex* textMMboosted=new TLatex ;
	textMMboosted->SetTextFont(42);
	textMMboosted->SetTextSize(0.035);
	textMMboosted->SetTextColor(kBlack);
	textMMboosted->SetNDC(true);
	textMMboosted->DrawLatex(0.20,0.83,"#bf{#it{ATLAS}} Internal");
	textMMboosted->DrawLatex(0.20,0.78,"One-Lepton");
	c1MMboosted->SetRightMargin(0.15);
	c1MMboosted->Draw();
	
        
	textMMboosted->DrawLatex(0.20,0.73,"WW SRMM-boosted");
	c1MMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryMMboosted_WW.pdf");
        c1MMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryMMboosted_WW.eps");
	c1MMboosted->Print("/data/vtudorac/EWK2ndWave/Discovery/Plots/ZnDiscoveryMMboosted_WW.png");


		
}

void readFileBkg(TString chainname, const char *file_name, TH1F *&hSRLMresolved,TH1F *&hSRHMresolved,TH1F *&hSRLMboosted,TH1F *&hSRMMboosted,TH1F *&hSRHMboosted, float lumi,bool LMresolved,bool HMresolved,bool LMboosted,bool HMboosted,bool MMboosted)


{
	TChain *inTree = new TChain(chainname);
	inTree->AddFile(file_name);


	inTree->SetBranchStatus("genWeight",1);
	inTree->SetBranchStatus("eventWeight",1);
	inTree->SetBranchStatus("leptonWeight",1);
	inTree->SetBranchStatus("pileupWeight",1);
	inTree->SetBranchStatus("polWeight",1);
	inTree->SetBranchStatus("jvtWeight",1);
	inTree->SetBranchStatus("bTagWeight",1);
	inTree->SetBranchStatus("trigWeight_singleLepTrig",1);
	inTree->SetBranchStatus("trigMatch_singleLepTrig",1);
	
	inTree->SetBranchStatus("lep1Pt",1);
	inTree->SetBranchStatus("nLep_base",1);
	inTree->SetBranchStatus("nLep_signal",1);
	inTree->SetBranchStatus("dPhi_lep_met",1);
	inTree->SetBranchStatus("meffInc30",1);
	inTree->SetBranchStatus("met",1);
	inTree->SetBranchStatus("mt",1);
	inTree->SetBranchStatus("nFatjets",1);
	inTree->SetBranchStatus("DatasetNumber",1);
	inTree->SetBranchStatus("mjj",1);
	inTree->SetBranchStatus("nBJet30_DL1",1);
	inTree->SetBranchStatus("nJet30",1);
	inTree->SetBranchStatus("fatjet1Ztagged",1);
	inTree->SetBranchStatus("fatjet1Wtagged",1);
	inTree->SetBranchStatus("fatjet1Pt",1);
	inTree->SetBranchStatus("met_Signif",1);
	inTree->SetBranchStatus("nLep_combiBase",1);
	inTree->SetBranchStatus("nLep_combiBaseHighPt",1);


        Int_t  nJet30,nFatjets,nLep_base,DatasetNumber,nLep_signal,nBJet30_DL1,nLep_combiBase,nLep_combiBaseHighPt;
	Float_t lep1Pt,z0,d0,meffInc30,met,mt,mjj,dPhi_lep_met,fatjet1Wtagged,fatjet1Pt,met_Signif;
	Double_t totalWeight,eventWeight,genWeight,leptonWeight,pileupWeight,jvtWeight,bTagWeight,trigWeight_singleLepTrig,polWeight,fatjet1Ztagged;
	Bool_t trigMatch_singleLepTrig;

	
	inTree->SetBranchAddress("eventWeight",&eventWeight);
	inTree->SetBranchAddress("leptonWeight",&leptonWeight);
	inTree->SetBranchAddress("pileupWeight",&pileupWeight);
	inTree->SetBranchAddress("jvtWeight",&jvtWeight);
	inTree->SetBranchAddress("bTagWeight",&bTagWeight);	
	inTree->SetBranchAddress("genWeight",&genWeight);
	inTree->SetBranchAddress("polWeight",&polWeight);
	inTree->SetBranchAddress("lep1Pt",&lep1Pt);
	inTree->SetBranchAddress("dPhi_lep_met",&dPhi_lep_met);
	inTree->SetBranchAddress("met",&met);
	inTree->SetBranchAddress("mt",&mt);
	inTree->SetBranchAddress("meffInc30",&meffInc30);
	inTree->SetBranchAddress("trigWeight_singleLepTrig",&trigWeight_singleLepTrig);
	inTree->SetBranchAddress("trigMatch_singleLepTrig",&trigMatch_singleLepTrig);
	inTree->SetBranchAddress("nFatjets",&nFatjets);
	inTree->SetBranchAddress("nLep_base",&nLep_base);
	inTree->SetBranchAddress("nLep_signal",&nLep_signal);
	inTree->SetBranchAddress("mjj",&mjj);
	inTree->SetBranchAddress("nBJet30_DL1",&nBJet30_DL1);
	inTree->SetBranchAddress("nJet30",&nJet30);
	inTree->SetBranchAddress("DatasetNumber",&DatasetNumber);
	inTree->SetBranchAddress("fatjet1Ztagged",&fatjet1Ztagged);
	inTree->SetBranchAddress("fatjet1Wtagged",&fatjet1Wtagged);
	inTree->SetBranchAddress("fatjet1Pt",&fatjet1Pt);
	inTree->SetBranchAddress("met_Signif",&met_Signif);
	inTree->SetBranchAddress("nLep_combiBase",&nLep_combiBase);
	inTree->SetBranchAddress("nLep_combiBaseHighPt",&nLep_combiBaseHighPt);
	


	unsigned long int entries = inTree->GetEntries();
	cout << "Numar total de evenimente: " << entries << endl;

     if (chainname=="data" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_data","hLMresolved_data",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_data","hHMresolved_data",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_data","hLMboosted_data",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_data","hMMboosted_data",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_data","hHMboosted_data",10,0,500);
	
            }
	    
    if (chainname=="ttbar_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_ttbar","hLMresolved_ttbar",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_ttbar","hHMresolved_ttbar",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_ttbar","hLMboosted_ttbar",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_ttbar","hMMboosted_ttbar",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_ttbar","hHMboosted_ttbar",10,0,500);
	
            }    
 if (chainname=="wjets_NoSys") 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_wjets","hLMresolved_wjets",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_wjets","hHMresolved_wjets",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_wjets","hLMboosted_wjets",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_wjets","hMMboosted_wjets",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_wjets","hHMboosted_wjets",10,0,500);
	
            }            
 if (chainname=="zjets_NoSys")   
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_zjets","hLMresolved_zjets",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_zjets","hHMresolved_zjets",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_zjets","hLMboosted_zjets",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_zjets","hMMboosted_zjets",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_zjets","hHMboosted_zjets",10,0,500);
	
	
	
            }  
  if (chainname=="ttv_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_ttv","hLMresolved_ttv",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_ttv","hHMresolved_ttv",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_ttv","hLMboosted_ttv",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_ttv","hMMboosted_ttv",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_ttv","hHMboosted_ttv",10,0,500);
	
            } 
	    
   if (chainname=="singletop_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_singletop","hLMresolved_singletop",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_singletop","hHMresolved_singletop",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_singletop","hLMboosted_singletop",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_singletop","hMMboosted_singletop",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_singletop","hHMboosted_singletop",10,0,500);

            }          

  if (chainname=="diboson_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_diboson","hLMresolved_diboson",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_diboson","hHMresolved_diboson",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_diboson","hLMboosted_diboson",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_diboson","hMMboosted_diboson",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_diboson","hHMboosted_diboson",10,0,500);

            }    
  if (chainname=="multiboson_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_multiboson","hLMresolved_multiboson",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_multiboson","hHMresolved_multiboson",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_multiboson","hLMboosted_multiboson",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_multiboson","hMMboosted_multiboson",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_multiboson","hHMboosted_multiboson",10,0,500);

            }    
   if (chainname=="tth_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_tth","hLMresolved_tth",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_tth","hHMresolved_tth",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_tth","hLMboosted_tth",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_tth","hMMboosted_tth",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_tth","hHMboosted_tth",10,0,500);

	
	
            } 
	    
   if (chainname=="vh_NoSys" ) 
	   { 
	     
	hSRLMresolved= new TH1F("hLMresolved_vh","hLMresolved_vh",10,0,500);
	hSRHMresolved= new TH1F("hHMresolved_vh","hHMresolved_vh",10,0,500);
	hSRLMboosted= new TH1F("hLMboosted_vh","hLMboosted_vh",10,0,500);
	hSRMMboosted= new TH1F("hMMboosted_vh","hMMboosted_vh",10,0,500);
	hSRHMboosted= new TH1F("hHMboosted_vh","hHMboosted_vh",10,0,500);

	
            }    
   
	for(unsigned long int entry = 0; entry < entries; entry++)
	{


		int nEntries=inTree->GetEntry(entry);


		//float weight=genWeight*eventWeight*leptonWeight*jvtWeight*bTagWeight*pileupWeight*lumi;
		double weight=genWeight*eventWeight*leptonWeight*jvtWeight*pileupWeight*trigWeight_singleLepTrig*polWeight*lumi;
		
		if (LMresolved==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 &&lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.6 && mjj>70. && mjj<105. && nFatjets==0 && mt>200)

		{



			if (chainname=="data") { 


				hSRLMresolved->Fill(lep1Pt);									
			}				


			if (chainname=="ttbar_NoSys") { 

				hSRLMresolved->Fill(lep1Pt,weight);		

			}   

			if (chainname=="wjets_NoSys") {  


				hSRLMresolved->Fill(lep1Pt,weight);		

			}

			if (chainname=="zjets_NoSys") { 

				hSRLMresolved->Fill(lep1Pt,weight);


			}
			if (chainname=="ttv_NoSys") { 

				hSRLMresolved->Fill(lep1Pt,weight);


			} 
			if (chainname=="diboson_NoSys") { 
				hSRLMresolved->Fill(lep1Pt,weight);

			}
			if (chainname=="singletop_NoSys") { 
				hSRLMresolved->Fill(lep1Pt,weight);

			}
                        if (chainname=="tth_NoSys") { 
				hSRLMresolved->Fill(lep1Pt,weight);

			}
                        if (chainname=="multiboson_NoSys") { 
				hSRLMresolved->Fill(lep1Pt,weight);

			}
                        if (chainname=="vh_NoSys") { 
				hSRLMresolved->Fill(lep1Pt,weight);

			}
		}


            if (HMresolved==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 &&lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.6 && mjj>70. && mjj<105. && nFatjets==0 && mt>300)
		

		{



			if (chainname=="data") { 


				hSRHMresolved->Fill(lep1Pt);									
			}				


			if (chainname=="ttbar_NoSys") { 

				hSRHMresolved->Fill(lep1Pt,weight);		

			}   

			if (chainname=="wjets_NoSys") {  


				hSRHMresolved->Fill(lep1Pt,weight);		

			}

			if (chainname=="zjets_NoSys") { 

				hSRHMresolved->Fill(lep1Pt,weight);


			}
			if (chainname=="ttv_NoSys") { 

				hSRHMresolved->Fill(lep1Pt,weight);


			} 
			if (chainname=="diboson_NoSys") { 
				hSRHMresolved->Fill(lep1Pt,weight);

			}
			if (chainname=="singletop_NoSys") { 
				hSRHMresolved->Fill(lep1Pt,weight);

			}
			
			if (chainname=="tth_NoSys") { 
				hSRHMresolved->Fill(lep1Pt,weight);

			}
                        if (chainname=="multiboson_NoSys") { 
				hSRHMresolved->Fill(lep1Pt,weight);

			}
                        if (chainname=="vh_NoSys") { 
				hSRHMresolved->Fill(lep1Pt,weight);

			}


		}
           
		if (LMboosted==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 && lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && (/*(met_Signif>12. && mjj>70. && mjj<90.&& meffInc30>600) ||*/ (met_Signif>15. && meffInc30>600.))&&mt>120&&mt<200)


		{



			if (chainname=="data") { 


				hSRLMboosted->Fill(lep1Pt);									
			}				


			if (chainname=="ttbar_NoSys") { 

				hSRLMboosted->Fill(lep1Pt,weight);		

			}   

			if (chainname=="wjets_NoSys") {  


				hSRLMboosted->Fill(lep1Pt,weight);		

			}

			if (chainname=="zjets_NoSys") { 

				hSRLMboosted->Fill(lep1Pt,weight);


			}
			if (chainname=="ttv_NoSys") { 

				hSRLMboosted->Fill(lep1Pt,weight);


			} 
			if (chainname=="diboson_NoSys") { 
				hSRLMboosted->Fill(lep1Pt,weight);

			}
			if (chainname=="singletop_NoSys") { 
				hSRLMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="tth_NoSys") { 
				hSRLMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="multiboson_NoSys") { 
				hSRLMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="vh_NoSys") { 
				hSRLMboosted->Fill(lep1Pt,weight);

			}
		}


		
		
		if (MMboosted==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 && lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && (/*(met_Signif>12. && mjj>70. && mjj<90.&& meffInc30>600) ||*/ (met_Signif>15. &&  meffInc30>600.))&&mt>200&& mt<300 )
		

		{



			if (chainname=="data") { 


				hSRMMboosted->Fill(lep1Pt);									
			}				


			if (chainname=="ttbar_NoSys") { 

				hSRMMboosted->Fill(lep1Pt,weight);		

			}   

			if (chainname=="wjets_NoSys") {  


				hSRMMboosted->Fill(lep1Pt,weight);		

			}

			if (chainname=="zjets_NoSys") { 

				hSRMMboosted->Fill(lep1Pt,weight);


			}
			if (chainname=="ttv_NoSys") { 

				hSRMMboosted->Fill(lep1Pt,weight);


			} 
			if (chainname=="diboson_NoSys") { 
				hSRMMboosted->Fill(lep1Pt,weight);

			}
			if (chainname=="singletop_NoSys") { 
				hSRMMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="tth_NoSys") { 
				hSRMMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="multiboson_NoSys") { 
				hSRMMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="vh_NoSys") { 
				hSRMMboosted->Fill(lep1Pt,weight);

			}
		}
if (HMboosted==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 && lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && ((met_Signif>12. && mjj>70. && mjj<90.&&  meffInc30>850 ) /*|| (met_Signif>15. && meffInc30>850)*/) && mt>300)
		
		{



			if (chainname=="data") { 


				hSRHMboosted->Fill(lep1Pt);									
			}				


			if (chainname=="ttbar_NoSys") { 

				hSRHMboosted->Fill(lep1Pt,weight);		

			}   

			if (chainname=="wjets_NoSys") {  


				hSRHMboosted->Fill(lep1Pt,weight);		

			}

			if (chainname=="zjets_NoSys") { 

				hSRHMboosted->Fill(lep1Pt,weight);


			}
			if (chainname=="ttv_NoSys") { 

				hSRHMboosted->Fill(lep1Pt,weight);


			} 
			if (chainname=="diboson_NoSys") { 
				hSRHMboosted->Fill(lep1Pt,weight);

			}
			if (chainname=="singletop_NoSys") { 
				hSRHMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="tth_NoSys") { 
				hSRHMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="multiboson_NoSys") { 
				hSRHMboosted->Fill(lep1Pt,weight);

			}
                        if (chainname=="vh_NoSys") { 
				hSRHMboosted->Fill(lep1Pt,weight);

			}
		}


       }

}




void readFileSignal(TString chainname1, const char *file_name, TH1F *&hSRLMresolved,TH1F *&hSRHMresolved,TH1F *&hSRLMboosted,TH1F *&hSRMMboosted,TH1F *&hSRHMboosted, float lumi,bool LMresolved,bool HMresolved,bool LMboosted,bool HMboosted,bool MMboosted){
        TChain *inTree = new TChain(chainname1);
	inTree->AddFile(file_name);
         cout<<file_name<<endl;

        inTree->SetBranchStatus("genWeight",1);
	inTree->SetBranchStatus("eventWeight",1);
	inTree->SetBranchStatus("leptonWeight",1);
	inTree->SetBranchStatus("pileupWeight",1);
	inTree->SetBranchStatus("polWeight",1);
	inTree->SetBranchStatus("jvtWeight",1);
	inTree->SetBranchStatus("bTagWeight",1);
	inTree->SetBranchStatus("trigWeight_singleLepTrig",1);
	inTree->SetBranchStatus("trigMatch_singleLepTrig",1);
	
	inTree->SetBranchStatus("lep1Pt",1);
	inTree->SetBranchStatus("nLep_base",1);
	inTree->SetBranchStatus("nLep_signal",1);
	inTree->SetBranchStatus("dPhi_lep_met",1);
	inTree->SetBranchStatus("meffInc30",1);
	inTree->SetBranchStatus("met",1);
	inTree->SetBranchStatus("mt",1);
	inTree->SetBranchStatus("nFatjets",1);
	inTree->SetBranchStatus("DatasetNumber",1);
	inTree->SetBranchStatus("mjj",1);
	inTree->SetBranchStatus("nBJet30_DL1",1);
	inTree->SetBranchStatus("nJet30",1);
	inTree->SetBranchStatus("fatjet1Ztagged",1);
	inTree->SetBranchStatus("fatjet1Wtagged",1);
	inTree->SetBranchStatus("fatjet1Pt",1);
	inTree->SetBranchStatus("met_Signif",1);
	inTree->SetBranchStatus("nLep_combiBase",1);
	inTree->SetBranchStatus("nLep_combiBaseHighPt",1);
	
	


        Int_t  nJet30,nFatjets,nLep_base,DatasetNumber,nLep_signal,nBJet30_DL1,nLep_combiBase,nLep_combiBaseHighPt;
	Float_t lep1Pt,z0,d0,meffInc30,met,mt,mjj,dPhi_lep_met,fatjet1Wtagged,fatjet1Pt,met_Signif;
	Double_t totalWeight,eventWeight,genWeight,leptonWeight,pileupWeight,jvtWeight,bTagWeight,trigWeight_singleLepTrig,polWeight,fatjet1Ztagged;
	Bool_t trigMatch_singleLepTrig;

	
	inTree->SetBranchAddress("eventWeight",&eventWeight);
	inTree->SetBranchAddress("leptonWeight",&leptonWeight);
	inTree->SetBranchAddress("pileupWeight",&pileupWeight);
	inTree->SetBranchAddress("jvtWeight",&jvtWeight);
	inTree->SetBranchAddress("bTagWeight",&bTagWeight);	
	inTree->SetBranchAddress("genWeight",&genWeight);
	inTree->SetBranchAddress("polWeight",&polWeight);
	inTree->SetBranchAddress("lep1Pt",&lep1Pt);
	inTree->SetBranchAddress("dPhi_lep_met",&dPhi_lep_met);
	inTree->SetBranchAddress("met",&met);
	inTree->SetBranchAddress("mt",&mt);
	inTree->SetBranchAddress("meffInc30",&meffInc30);
	inTree->SetBranchAddress("trigWeight_singleLepTrig",&trigWeight_singleLepTrig);
	inTree->SetBranchAddress("trigMatch_singleLepTrig",&trigMatch_singleLepTrig);
	inTree->SetBranchAddress("nFatjets",&nFatjets);
	inTree->SetBranchAddress("nLep_base",&nLep_base);
	inTree->SetBranchAddress("nLep_signal",&nLep_signal);
	inTree->SetBranchAddress("mjj",&mjj);
	inTree->SetBranchAddress("nBJet30_DL1",&nBJet30_DL1);
	inTree->SetBranchAddress("nJet30",&nJet30);
	inTree->SetBranchAddress("DatasetNumber",&DatasetNumber);
	inTree->SetBranchAddress("fatjet1Ztagged",&fatjet1Ztagged);
	inTree->SetBranchAddress("fatjet1Wtagged",&fatjet1Wtagged);
	inTree->SetBranchAddress("fatjet1Pt",&fatjet1Pt);
	inTree->SetBranchAddress("met_Signif",&met_Signif);
	inTree->SetBranchAddress("nLep_combiBase",&nLep_combiBase);
	inTree->SetBranchAddress("nLep_combiBaseHighPt",&nLep_combiBaseHighPt);
	  
	unsigned long int entries = inTree->GetEntries();
	cout << "Numar total de evenimente: " << entries << endl;



        
        hSRHMresolved= new TH1F("hSRHMresolved","hSRHMresolved",10,0,500);
	hSRLMresolved= new TH1F("hSRLMresolved","hSRLMresolved",10,0,500);
	hSRLMboosted= new TH1F("hSRLMboosted","hSRLMboosted",10,0,500);
	hSRMMboosted= new TH1F("hSRMMboosted","hSRMMboosted",10,0,500);
	hSRHMboosted= new TH1F("hSRHMboosted","hSRHMboosted",10,0,500);
	
       int n=0;

	for(unsigned long int entry = 0; entry < entries; entry++)
	{


		int nEntries=inTree->GetEntry(entry);


		double weight=genWeight*eventWeight*leptonWeight*jvtWeight*pileupWeight*trigWeight_singleLepTrig*polWeight*lumi;
		
                
		
		if (LMresolved==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 &&lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.6 && mjj>70. && mjj<105. && nFatjets==0 && mt>200.)
		
		

		{
		
             
			if ("chainname1") { 

				hSRLMresolved->Fill(lep1Pt,weight);
				
				
											


			}


		}
		if (HMresolved==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 &&lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.6 && mjj>70. && mjj<105. && nFatjets==0 && mt>300)
		
		

		{
		
             
			if ("chainname1") { 

				hSRHMresolved->Fill(lep1Pt,weight);
				
				
											


			}


		}
	
	
	
		if (LMboosted==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 && lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && (/*(met_Signif>12. && mjj>70. && mjj<90.&& meffInc30>600) ||*/ (met_Signif>15. && meffInc30>600 ))&&mt>120&&mt<200)

		
		

		{
		
             
			if ("chainname1") { 

				hSRLMboosted->Fill(lep1Pt,weight);
				
				
											


			}


		}	
		
		
		
			if (MMboosted==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 && lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && (/*(met_Signif>12. && mjj>70. && mjj<90. && meffInc30>600) || */(met_Signif>15. && meffInc30>600.))&&mt>200&& mt<300 )
		
		

		{
		
             
			if ("chainname1") { 

				hSRMMboosted->Fill(lep1Pt,weight);
				
				
											


			}


		}	
		
		
		if (HMboosted==true && nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2 && lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && (/*(met_Signif>12. && mjj>70. && mjj<90. && meffInc30>850.) ||*/ (met_Signif>15. && meffInc30>850)) && mt>300)
		

		{
		
             
			if ("chainname1") { 

				hSRHMboosted->Fill(lep1Pt,weight);
				
				
											


			}


		}	
		
		
	}

}
