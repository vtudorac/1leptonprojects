//
//   @file    AtlasStyle.h         
//   
//            ATLAS Style, based on a style file from BaBar
//
//   @author M.Sutton
// 
//   Copyright (C) 2010 Atlas Collaboration
//
//   $Id: AtlasStyle.h 244550 2017-04-04 13:50:55Z dxu $

#ifndef  PLOTTER_ATLASSTYLE_H
#define PLOTTER_ATLASSTYLE_H

#include "TStyle.h"

void SetAtlasStyle();

TStyle* AtlasStyle(); 

#endif // __ATLASSTYLE_H
