#include "HistPlotter.h"
#include "TGaxis.h"

// ---------------------------------------------------------------------------------------------------- //
HistPlotter::HistPlotter()
{

  m_removeEmptyBins  =  true; 
  m_showRegions      =  true;
  m_logY             =  false;
  m_showRegionName   =  false;
  m_ratio_yMax       = -1.0;
  m_ratio_yMin       = -1.0;
  m_main_yMax        = -1.0;
  m_main_yMin        = -1.0;
  m_histStatus       = "Preliminary";
  m_units            = "GeV";
  m_regionsToPlot    = "";            // Default is to plot everything found in the workspace
  m_regionName       = "";
  m_regionName2       = "";
  m_printRegionsOnly = false;
  m_integerBins      = true;
  m_xAxisNdivisions  = std::make_tuple(0, 0, 0);
  m_processVec.clear();

  // Shut up Roofit
  RooMsgService::instance().getStream(1).removeTopic(NumIntegration);
  RooMsgService::instance().getStream(1).removeTopic(Plotting);

  // TODO: configurable?
  // TGaxis::SetMaxDigits(2);

}
// ---------------------------------------------------------------------------------------------------- //
void HistPlotter::addPlots(TString histDir,
                           TString name,
                           TString sigName,
                           TString sigName2,
			   TString saveDir,
			   TString regions,
			   TString rootFileName,
			   TString workSpaceName)
{

  PlotInfo* info = new PlotInfo();

  info->name            = name;
  info->histDir         = histDir+"/"+name+"/";
  info->signalDir       = sigName != "" ? histDir+"/"+sigName+"/"+rootFileName : "";
  info->signalDir2       = sigName2 != "" ? histDir+"/"+sigName2+"/"+rootFileName : "";
  info->saveDir         = saveDir+"/";
  info->regions         = regions;
  info->rootFileName    = rootFileName;
  info->workSpaceName   = workSpaceName;

  m_plots.push_back(info);

}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::init()
{
  // ATLAS Publication style
  HistStyle::init();

  // 
  reverseLegendMap();

  // Load HistFitter libraries 
  //gSystem->Load("libSusyFitter.so");


}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::Process()
{

  init();

  for(int p=0; p<m_plots.size(); p++){
    //
    PlotInfo* info = m_plots[p];
    //
    RooWorkspace* w  = getWorkspace(info->histDir+info->rootFileName,info->workSpaceName);
    if(w==NULL){ 
      std::cout << "ERROR::Workspace not found, no plotting performed for: " << info->name << std::endl; 
      continue;
    }


    RooFitResult* rFit = (RooFitResult*)w->obj("RooExpandedFitResult_afterFit");
    if(rFit==NULL){
      std::cout << "WARNING::Fit result is NULL, errors will not be displayed for:  " << info->name << std::endl; 
    }

    TString outputPrefix = "";

    RooSimultaneous* pdf    = (RooSimultaneous*) w->pdf("simPdf");
    RooAbsData* data        = (RooAbsData*)w->data("obsData");
    RooArgSet* params       = (RooArgSet*) pdf->getParameters(*data) ;
    RooCategory* regionCat  = (RooCategory*) w->cat("channelCat");

    std::vector<TString> regionsVec = getRegions(info->regions, regionCat);

    unsigned int numPlots = regionsVec.size();
    
    // iterate over all the regions 
    for(unsigned int iVec=0; iVec<numPlots; iVec++){

      TString regionCatLabel = regionsVec[iVec];
      TFile* hepdata = new TFile("hepData_"+regionCatLabel+".root","RECREATE");
      hepdata->Close();

      RooAbsPdf* regionPdf      = (RooAbsPdf*) pdf->getPdf(regionCatLabel.Data());
      TString dataCatLabel      = Form("channelCat==channelCat::%s",regionCatLabel.Data());
      RooAbsData* regionData    = (RooAbsData*) data->reduce(dataCatLabel.Data());

     if(regionPdf==NULL || regionData==NULL){   
       continue; 
      }

      TCanvas* c1 = getCanvas();

      RooRealVar* regionVar =(RooRealVar*) ((RooArgSet*) regionPdf->getObservables(*regionData))->find(Form("obs_x_%s",regionCatLabel.Data()));


      TPad* pad1 = getMainTPad();
      TPad* pad2 = getRatioTPad();
 
      pad1->Draw();
      pad2->Draw();

      TString dataCut = getDataCut(regionCatLabel);

      RooDataSet* sliceData = (RooDataSet*)regionData;

      if( !dataCut.IsWhitespace() ){
        TString regionVar_name = regionVar->GetName();
        sliceData = (RooDataSet*)regionData->reduce(RooArgSet(*regionVar),regionVar_name+dataCut) ;
      }
  
      // Make central plot; stack all backgrounds and display total PDF and data
      pad1->cd();
      makeCentralPlot(sliceData,regionPdf,regionVar,w,rFit,regionCatLabel,pad1,info->signalDir,info->signalDir2);

      // Ratio plot
      pad2->cd();
      makeRatioPlot(data,sliceData,regionPdf,regionVar,w,rFit,dataCatLabel,regionCatLabel);

      c1->SaveAs(info->saveDir+info->name+"_"+regionCatLabel+".png");
      c1->SaveAs(info->saveDir+info->name+"_"+regionCatLabel+".pdf");
      c1->SaveAs(info->saveDir+info->name+"_"+regionCatLabel+".eps");
      c1->SaveAs(info->saveDir+info->name+"_"+regionCatLabel+".C");

      delete c1;

    } // loop over plots
  } // loop over data structure



}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::makeCentralPlot(RooDataSet* regionData,RooAbsPdf* regionPdf,RooRealVar* regionVar,
				  RooWorkspace* w,RooFitResult* rFit,TString regionCatLabel,TPad*& pad,TString signalDir,TString signalDir2)
{

  // Make legend
  //TLegend* leg1 = getLegend(0.48,0.65,0.70,0.92,0.06);
  //TLegend* leg2 = getLegend(0.68,0.65,0.92,0.92,0.06);
  //TLegend* leg3 = getLegend(0.44,0.57,0.88,0.63,0.055);
  //  leg3->SetTextSize(0.044);

  //  TLegend* leg1 = getLegend(0.50,0.65,0.72,0.92,0.06);

  TLegend* leg1 = getLegend(0.48,0.65,0.70,0.92,0.06);

// 2col legend
  //TLegend* leg1 = getLegend(0.48,0.65,0.94,0.92,0.06);
  //leg1->SetNColumns(2);

  TLegend* leg2 = getLegend(0.70,0.65,0.94,0.92,0.06);
  //TLegend* leg3 = getLegend(0.46,0.57,0.90,0.63,0.055);//1signal
  //TLegend* leg3 = getLegend(0.46,0.51,0.90,0.63,0.055);//2signals -- dol
  TLegend* leg3 = getLegend(0.46,0.51,0.75,0.63,0.055);//2signals -- dol
  leg3->SetTextSize(0.042);
  //leg3->SetTextSize(0.039);


  // Create plot
  RooPlot* frame = regionVar->frame(); 
  frame->SetMinimum(0.00000001);
  frame->SetName(Form("frame_%s",regionCatLabel.Data()));

  // We have to specify a lot of extra properties to get the legend display to what we want
  regionData->plotOn(frame,RooFit::DataError(RooAbsData::Poisson),MarkerColor(kBlack),
                     MarkerStyle(20),FillColor(kWhite),LineColor(kBlack),LineWidth(2),XErrorSize(0),MarkerSize(1.2));

  TString y_axis = frame->GetYaxis()->GetTitle();

  y_axis.ReplaceAll("( ","");
  y_axis.ReplaceAll(")",m_units);
 
  if(m_integerBins){
    frame->GetYaxis()->SetTitle("Events");
  }
  else{
    frame->GetYaxis()->SetTitle(y_axis);
  }

  if (m_xAxisNdivisions != std::make_tuple(0, 0, 0)) {
    frame->GetXaxis()->SetNdivisions(std::get<0>(m_xAxisNdivisions),
                                     std::get<1>(m_xAxisNdivisions),
                                     std::get<2>(m_xAxisNdivisions),
                                     kFALSE);
  }

  if(m_removeEmptyBins) RemoveEmptyDataBins(w,frame);
  addToLegend(frame,leg1,"Data",false,false);

  regionPdf->plotOn(frame,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5),LineColor(kBlack),LineWidth(4));
    
  addToLegend(frame,leg1,"Total SM",false,true,true);

  // Plot each component separately
  AddComponentsToPlot(w, frame, regionPdf, regionVar, regionCatLabel.Data(),leg1,leg2,true);

  gStyle->SetHatchesLineWidth( 1.5 * gStyle->GetHatchesLineWidth() );

  // Error band
  if(rFit != NULL) { 	

    // Small, robust hack to deal with incorect error bands from roofit
    double binWidthFrac = frame->GetXaxis()->GetNbins()>=1 ? frame->GetXaxis()->GetBinWidth(1)*0.0 : 0.0;
    double xMinCut = frame->GetXaxis()->GetBinLowEdge(1) + binWidthFrac;
    double xMaxCut = frame->GetXaxis()->GetBinUpEdge( frame->GetXaxis()->GetNbins() ) - binWidthFrac;

    regionPdf->plotOn(frame,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5),
                      FillColor(kBlack),
                      //FillStyle(3354),
		      FillStyle(3004),
                      LineColor(kBlack),
                      VisualizeError(*rFit),
                      Range(xMinCut,xMaxCut,kFALSE)); // for Range, kFALSE says don't renormalize 

  }

    
  // TODO: Adding of signal disabled until i understand the format coming from HF
  AddSignalToPlot(frame,regionVar,regionCatLabel,leg3,signalDir);
  AddSignalToPlot(frame,regionVar,regionCatLabel,leg3,signalDir2);

  // re-plot data and pdf, so they are on top of error and components
  regionPdf->plotOn(frame,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5),LineColor(kBlack),LineWidth(4));  

  regionData->plotOn(frame,RooFit::DataError(RooAbsData::Poisson),MarkerStyle(20),MarkerColor(kBlack),LineColor(kBlack),
                     LineWidth(2),XErrorSize(0),MarkerSize(1.2));

  if(m_removeEmptyBins) RemoveEmptyDataBins(w,frame);

  SetFrameStyle(frame,pad,regionCatLabel);
  
  leg1->Draw("same");
  leg2->Draw("same");
  leg3->Draw("same");

  // Key to call this last!
  addRegionToCanvas(frame,regionVar,regionCatLabel);

  // TODO: Disabled for now, not being used. Would be very useful for the future
  doHEPData(regionData,regionPdf,rFit,regionVar,regionCatLabel);


}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::makeRatioPlot(RooAbsData* data,RooDataSet* regionData,RooAbsPdf* regionPdf,RooRealVar* regionVar,
				RooWorkspace* w,RooFitResult* rFit,TString dataCatLabel,TString regionCatLabel)
{



  // Construct a histogram with the ratio of the data w.r.t the pdf curve
  RooHist* hratio = makeRatioHist(regionData,regionPdf,regionVar,dataCatLabel); // 
  hratio->SetMarkerColor(kBlack);
  hratio->SetLineColor(kBlack);
  hratio->SetLineWidth(2);


  RooCurve* hratioPdfError = 0; //getRatioBand();
  hratioPdfError = MakePdfErrorRatioHist(w, regionData, regionPdf, regionVar, rFit);
  
  RooPlot* frame2 = regionVar->frame() ;
  if (rFit != NULL)   frame2->addPlotable(hratioPdfError,"F");
  //frame2->getAttLine(hratioPdfError->GetName())->SetLineWidth(6);
  //frame2->getAttLine(hratioPdfError->GetName())->SetLineColor(kBlack);
  //frame2->getAttFill(hratioPdfError->GetName())->SetFillStyle(3354);

  frame2->addPlotable(hratio,"P 0");

  SetRatioPadStyle(frame2, getXaxisLabel(regionCatLabel) );

  frame2->GetXaxis()->SetNdivisions(4,6,0,kFALSE);// 2J, 6J
  if(regionCatLabel.Contains("SR4J"))
    frame2->GetXaxis()->SetNdivisions(3,6,0,kFALSE);//4Jhighx, 4Jlowx, 9J
  if(regionCatLabel.Contains("SR6J"))
    frame2->GetXaxis()->SetNdivisions(3,6,0,kFALSE);//4Jhighx, 4Jlowx, 9J
  if(regionCatLabel.Contains("SR2J") || regionCatLabel.Contains("multi") )
    frame2->GetXaxis()->SetNdivisions(4,6,0,kFALSE);// 2J, 6J
  if(regionCatLabel.Contains("SRLM") || regionCatLabel.Contains("SRMM") ||  regionCatLabel.Contains("SRHM") )
    frame2->GetXaxis()->SetNdivisions(3,6,0,kFALSE);// HM, LM, MM  
    
  if(regionCatLabel.Contains("nMinusOne")) {
    frame2->GetXaxis()->SetNdivisions(7,5,0,kFALSE);
  }

  if (m_xAxisNdivisions != std::make_tuple(0, 0, 0)) {
    frame2->GetXaxis()->SetNdivisions(std::get<0>(m_xAxisNdivisions),
                                      std::get<1>(m_xAxisNdivisions),
                                      std::get<2>(m_xAxisNdivisions),
                                      kFALSE);
  }

  /*
    Set ratio minimum 
  */
  if(m_ratio_yMin>0.0){
    frame2->SetMinimum(m_ratio_yMin);
  }
  else{
    frame2->SetMinimum(0.0);
  }

  /*
    Set ratio maximum
  */
  if(m_ratio_yMax>0.0){
    frame2->SetMaximum(m_ratio_yMax);
  }
  else{
    frame2->SetMaximum(2.3);
  }

  if( m_integerBins ){
    for(int n=1; n<frame2->GetXaxis()->GetNbins()+1; n++){
      int lowBinEdge = (int)frame2->GetXaxis()->GetBinLowEdge(n);
      frame2->GetXaxis()->SetBinLabel(n, TString::Format("%i",lowBinEdge).Data() );
    }
    frame2->GetXaxis()->SetTitleOffset(1.14);
    frame2->GetXaxis()->SetLabelSize(0.215);
  }
  //else{
  //  frame2->GetXaxis()->SetNdivisions(502);
  //}


  frame2->Draw();
  
  TF1* line = new TF1("line", "1", -1e5, 1e5);
  line->SetLineColor(1);
  line->Draw("same");}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::doHEPDataSignal(RooPlot* frame_dummy, TString regionCatLabel, TString process)
{

  RooCurve* MC_central = (RooCurve*) frame_dummy->findObject(0,RooCurve::Class()) ;

  MC_central->Print();

  Double_t x,point;
  Double_t binCenter;
  Double_t prevPoint = -1;
  Double_t prevX = -1;
  TFile* hepdata = new TFile("hepData_"+regionCatLabel+".root","UPDATE");
  TGraphAsymmErrors * g = new TGraphAsymmErrors();
  for (int i=0; i<MC_central->GetN(); i++) {
    MC_central->GetPoint(i,x,point);
    if (!(prevPoint < 0) and abs(prevX-x)>1e-5 and abs(prevPoint-point)<1e-5) {
      binCenter = prevX+0.5*(x-prevX);
      // std::cout << "#center#" << binCenter << ", " << point << std::endl;
      if (!(point == 0)) {
        g->SetPoint(g->GetN(), binCenter, point);
      }
    }
    prevPoint = point;
    prevX = x;
    //std::cout << "####" << x << ", " << point << std::endl;
  }
  g->Write(process);
  hepdata->Close();
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::doHEPData(RooDataSet* regionData,RooAbsPdf* regionPdf,RooFitResult* rFit,RooRealVar* regionVar,TString regionCatLabel)
{

  if( rFit == NULL ){
    std::cout << " <HistPlotter::doHEPData> WARNING fit result is NULL, HEP Data will not be output! " << std::endl;
  }

  RooPlot* frame_dummy =  regionVar->frame(); 
 
  regionData->plotOn(frame_dummy,RooFit::DataError(RooAbsData::Poisson),MarkerColor(kBlack),LineColor(kBlack));
  regionPdf->plotOn(frame_dummy,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5));

  // Find curve object
  RooCurve* MC_central = (RooCurve*) frame_dummy->findObject(0,RooCurve::Class()) ;
  if (!MC_central) {
    cout << "<HistPlotter::doHEPData> Error::Cannot make ratio histogram, cannot find data hist";
    return;
  }

  // Find histogram object
  RooHist* data = (RooHist*) frame_dummy->findObject(0,RooHist::Class()) ;
  if (!data) {
    cout << "<HistPlotter::doHEPData> Error::Cannot make ratio histogram, cannot find total PDF hist";
    return;
  }

  Double_t xstart,xstop,y ;
  MC_central->GetPoint(0,xstart,y) ;
  MC_central->GetPoint(MC_central->GetN()-1,xstop,y);

  TGraphAsymmErrors* data_gr = new TGraphAsymmErrors();
  data_gr->SetName("data");
  TGraphAsymmErrors* mc_gr   =  new TGraphAsymmErrors();
  mc_gr->SetName("mc");
  
  for(Int_t i=0 ; i<data->GetN() ; i++) {    
    Double_t x,point;
    data->GetPoint(i,x,point) ;

    // Only calculate pull for bins inside curve range
    if (x<xstart || x>xstop) continue ;
    
    Double_t dyl = data->GetErrorYlow(i) ;
    Double_t dyh = data->GetErrorYhigh(i) ;
    float mcc    = MC_central->interpolate(x);

    data_gr->SetPoint(i,x,point);
    data_gr->SetPointEYhigh(i,dyh);
    data_gr->SetPointEYlow(i,dyl);

    mc_gr->SetPoint(i,x,mcc);
 
    std::cout << "<HistPlotter::doHEPData> DATA : " << point << " + " << dyh << " - " << dyl << std::endl;   
    std::cout << "<HistPlotter::doHEPData> MC   : " << mcc << std::endl;
    //
  }


  // +++++++++++++++++++++++ //
  //        MC ERRORS        //
  // ++++++++++++++++++++++++ //

  regionPdf->plotOn(frame_dummy,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5),VisualizeError(*rFit,1.0));

  // Find curve object
  RooCurve* MC_errors = (RooCurve*) frame_dummy->findObject(0,RooCurve::Class()) ;
  if (!MC_errors) {
    cout << " <HistPlotter::doHEPData> Error::Cannot get errors! " << std::endl;
    return;
  }
  
  Int_t j = 0;
  Bool_t bottomCurve = kFALSE;

  for(Int_t i=1; i<MC_errors->GetN()-1; i++){
    Double_t x = 0.;
    Double_t y = 0.;
    MC_errors->GetPoint(i,x,y) ;
    
    // errorBand curve has twice as many points as does a normal/nominal (pdf) curve
    // first it walks through all +1 sigma points (topCurve), then the -1 sigma points (bottomCurve)
    // to divide the errorCurve by the pdfCurve, we need to count back for the pdfCurve once we're in the middle of errorCurve
    if( i >= (MC_central->GetN()-1) ) bottomCurve = kTRUE;
    
    Double_t xNom = x;
    Double_t yNom = y;

    // each errorCurve has two more points just outside the plot, so we need to treat them separately
    if( i == (MC_central->GetN() - 1) ||  i == MC_central->GetN() ){
      continue;
    }
    
    if( bottomCurve ){

      MC_central->GetPoint(j,xNom,yNom);

      double mc_gr_y,mc_gr_x;
      for(Int_t k=0; k<mc_gr->GetN(); k++){
	mc_gr->GetPoint(k,mc_gr_x,mc_gr_y);

// 	std::cout << "Curve y nom : " << yNom << std::endl;
// 	std::cout << "Matching:     " << mc_gr_y << std::endl;

	if( TMath::Abs(mc_gr_y-yNom)<0.0001 ){
	  std::cout << "Low diff: " << y-yNom << std::endl;
	  mc_gr->SetPointEYlow(k,TMath::Abs(y-yNom) );
	  break;
	}
      }

//       std::cout << "Bottom curve nominal: " << yNom << " for bin: " << j << std::endl;
//       std::cout << "Bottom curve error:   " << y << " for bin: " << j << std::endl;
    
      j--;
    } 
    else {

      j++;
      MC_central->GetPoint(j,xNom,yNom);

      double mc_gr_y,mc_gr_x;
      for(Int_t k=0; k<mc_gr->GetN(); k++){
	mc_gr->GetPoint(k,mc_gr_x,mc_gr_y);

// 	std::cout << "Curve y nom : " << yNom << std::endl;
// 	std::cout << "Matching:     " << mc_gr_y << std::endl;

	if( TMath::Abs(mc_gr_y-yNom)<0.0001 ){
	  std::cout << "High diff: " << y-yNom << std::endl;
	  mc_gr->SetPointEYhigh(k,TMath::Abs(y-yNom) );
	  break;
	}
      }


//       std::cout << "Top curve nominal: " << yNom << " for bin: " << j << std::endl;
//       std::cout << "Top curve error:   " << y << " for bin: " << j << std::endl;
    }

  }

  mc_gr->Print("Range");
  

  TFile* hepdata = new TFile("hepData_"+regionCatLabel+".root","UPDATE");

  data_gr->Write();
  mc_gr->Write();
  hepdata->Write();
  hepdata->Close();


}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::AddSignalToPlot(RooPlot*& frame,RooRealVar* obsRegion,TString regionCatLabel,TLegend* leg,TString signalDir)
{

  if( signalDir=="" ) return;

  TString beforeFit = signalDir; 
  beforeFit.ReplaceAll("_afterFit","");

  RooWorkspace* w = getWorkspace(beforeFit,"combined");

  // w->Print();
  // abort();

  if(w==NULL){
    cout << "<HistPlotter::AddSignalToPlot> WARNING Workspace for signal not found, no signal will be added to plots!" << endl;
    return;
  }

  RooSimultaneous* pdf    = (RooSimultaneous*) w->pdf("simPdf");
  RooCategory* regionCat  = (RooCategory*) w->cat("channelCat");
  RooAbsPdf* regionPdf    = (RooAbsPdf*) pdf->getPdf(regionCatLabel.Data());

  AddComponentsToPlot(w,frame,regionPdf,obsRegion,regionCatLabel.Data(),leg,NULL,false);

}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::AddComponentsToPlot(RooWorkspace* w, 
				      RooPlot* frame, 
				      RooAbsPdf* regionPdf, 
				      RooRealVar* obsRegion, 
				      TString regionCatLabel,
				      TLegend* leg1,
				      TLegend* leg2,
				      bool stack) 
{

  TString RRSPdfName = Form("%s_model",regionCatLabel.Data()); 
  RooRealSumPdf* RRSPdf = (RooRealSumPdf*) regionPdf->getComponents()->find(RRSPdfName);
  
  RooArgList RRSComponentsList =  RRSPdf->funcList();
 
  RooLinkedListIter iter = RRSComponentsList.iterator() ;
  RooProduct* component;
  vector<TString> compNameVec;
  vector<TString> globalCompNameVec;
  vector <double> compFracVec;
  vector<TString> compStackNameVec;
  vector <double> compStackFracVec;
  compNameVec.clear();
  globalCompNameVec.clear();
  compStackNameVec.clear();
  compFracVec.clear();
  compStackFracVec.clear();

  TString binWidthName =  Form("binWidth_obs_x_%s_0",regionCatLabel.Data());
  RooRealVar* regionBinWidth = ((RooRealVar*) RRSPdf->getVariables()->find(Form("binWidth_obs_x_%s_0",regionCatLabel.Data()))) ;

  if(regionBinWidth==NULL){
    return; //?
  }

  std::map<TString, RooProduct*> compMap;
  while( (component = (RooProduct*) iter.Next())) {
    compMap[component->GetName()] = component;
  }

  for (auto processName : m_processVec) {
    // look for matching components
    // for processes in the order they were added
    for (auto comp : compMap) {
      if (comp.first.Contains(processName)) {
        std::cout << "found component match for " << processName << ": " << comp.first << std::endl;
        globalCompNameVec.push_back(comp.first);
      }
    }
  }

  for (auto compName : globalCompNameVec) {
    if (compMap.find(compName) != compMap.end()) {
      component = compMap[compName];
    } else {
      continue;
    }
    std::cout << "Adding component " << component->GetName() << std::endl;
    TString  componentName = component->GetName();
    TString stackComponentName = componentName; 
    if(!compStackNameVec.empty()){  stackComponentName  = Form("%s,%s",compStackNameVec.back().Data() ,componentName.Data()); }
    compNameVec.push_back(componentName);
    compStackNameVec.push_back(stackComponentName);

    double componentFrac = GetComponentFrac(w,componentName,RRSPdfName,obsRegion,regionBinWidth) ;
    double stackComponentFrac = componentFrac; 
    if(!compStackFracVec.empty()){  stackComponentFrac  = compStackFracVec.back() + componentFrac; } 
    compFracVec.push_back(componentFrac);
    compStackFracVec.push_back(stackComponentFrac);

  }

  // normalize data to expected number of events 
  double normCount = regionPdf->expectedEvents(*obsRegion);

  for( int iVec = (compFracVec.size()-1) ; iVec>-1; iVec--){

    Int_t compPlotColor = -1;

    TString process = compNameVec[iVec];

    process.ReplaceAll("_"+regionCatLabel+"_","");
    process.ReplaceAll("L_x_","");
    process.ReplaceAll("overallSyst_x_StatUncert","");
    process.ReplaceAll("overallSyst_x_Exp","");

    std::cout << process << std::endl;

    // Only plot what the user has added via addProcess(..)
    if(stack){
      std::map<TString,Int_t>::const_iterator it = m_colorMap.find(process);
      if( it == m_colorMap.end() ) continue;
      else{
        compPlotColor = it->second;
      }
    }

    if( !stack ){  
      std::map<TString,Int_t>::const_iterator it = m_colorMapSig.find(process);
      if( it == m_colorMapSig.end() ) continue;
      else{
        compPlotColor = it->second;
      }
    }
    
    // Don't plot this process!
    if( compPlotColor < 0 ) continue;

    // Stacking is for backgrounds
    if(stack){
      regionPdf->plotOn(frame,Components(compStackNameVec[iVec].Data()),
			FillColor(compPlotColor),FillStyle(1001),DrawOption("F"),LineColor(compPlotColor),
			Normalization(compStackFracVec[iVec]*normCount,RooAbsReal::NumEvent),
			Precision(1e-5));

      // Ad to legend
      int nLeg = getNLegend(process);
      cout<< nLeg<<endl;
      if(nLeg==1)
        addToLegend(frame,leg1,process);
      else if(nLeg==2)
        addToLegend(frame,leg2,process);

    }
    else{
      regionPdf->plotOn(frame,Components(compNameVec[iVec].Data()),FillColor(kWhite),
			LineColor(compPlotColor),LineStyle(7),LineWidth(4),
			Normalization(compFracVec[iVec]*normCount,RooAbsReal::NumEvent),
			Precision(1e-5));


      doHEPDataSignal(frame, regionCatLabel, process);

      // Signal always should go into leg1 (actually the third legend) 
      addToLegend(frame,leg1,process,false,false,false,true);
    }

  } // Loop over all processes

}
// -------------------------------------------------------------------------------------------------------- //
Double_t HistPlotter::GetComponentFrac(RooWorkspace* w, const char* Component, const char* RRSPdfName, 
				       RooRealVar* observable, RooRealVar* binWidth){

    RooAbsReal*  i_RRSPdf = ((RooAbsPdf*)w->pdf(RRSPdfName))->createIntegral(RooArgSet(*observable));
    RooAbsReal*  i_component =   ((RooProduct*)w->obj(Component))->createIntegral(RooArgSet(*observable));

    Double_t Int_RRSPdf = i_RRSPdf->getVal();
    Double_t Int_component = i_component->getVal();

    Double_t componentFrac = 0.;
    if(Int_RRSPdf != 0.) componentFrac =  Int_component * binWidth->getVal() / Int_RRSPdf;

    delete  i_RRSPdf;
    delete  i_component;

    return componentFrac;
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::RemoveEmptyDataBins(RooWorkspace* w, RooPlot* frame)
{

  const char* histname = 0;

  // Find histogram object
  RooHist* hist = (RooHist*) frame->findObject(histname,RooHist::Class()) ;
  if (!hist) {
    return ;
  }
  
  for(Int_t i=0; i<hist->GetN(); i++){
    Double_t x,y;
    hist->GetPoint(i,x,y) ;

    // std::cout << "Data (central/high/low) " << y << " + " << hist->GetErrorYhigh(i) << " - " << hist->GetErrorYlow(i) << std::endl;

    if( fabs(y)< 0.0000001 && hist->GetErrorYhigh(i) > 0.){
      hist->RemovePoint(i);
      if(i != hist->GetN()) --i;
    }
  }

    return;
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::SetFrameStyle(RooPlot*& frame,TPad*& pad,TString region)
{

  // histName==0 takes last RooPlot
  RooHist* hist = (RooHist*) frame->findObject(0,RooHist::Class()) ;

  Double_t maxBin = 0.0;
  Double_t minBin = 999999999;

  for(Int_t i=0; i<hist->GetN(); i++){
    Double_t x,y;
    hist->GetPoint(i,x,y);
    if(y>maxBin         ) maxBin=y;
    if(y<minBin && y>0  ) minBin=y;
  }

  float var = minBin/maxBin;

  /*
    Set lower scale
  */
  // User passed minimum
  if(m_main_yMin>0){
    frame->SetMinimum(m_main_yMin);
  }
  // Oterwise automatic
  else{
    if(m_logY){
      if(maxBin>1000) frame->SetMinimum(0.06);
      else            frame->SetMinimum(0.06);
    }
    else{
      frame->SetMinimum(0.001);
    }
  }

  /*
    Set upper scale
  */
  // User passed maximum
  if(m_main_yMax>0){
    frame->SetMaximum(m_main_yMax);
  }
  // Oterwise automatic
  else{
    if(m_logY){
      if(var<0.70 && minBin>0)  frame->SetMaximum(maxBin*300.0);
      else                      frame->SetMaximum(maxBin*40.0);
    }
    else{
      frame->SetMaximum(maxBin*1.50);
    }
  }

  SetMainPadStyle(frame);
  frame->Draw();
  if(m_logY) pad->SetLogy();

  // Add text to canavs
  //AddText("#sqrt{s} = 13 TeV, 3.2 fb^{-1}",true,0.19,0.80,0.05);
  //AddText("ATLAS",true,0.19,0.88,0.060,72);
  //AddText(m_histStatus,true,0.32,0.88,0.055,42);
  //AddText("#sqrt{s} = 13 TeV, 13.3 fb^{-1}",true,0.19,0.81,0.05);
  AddText("#sqrt{s} = 13 TeV, 140 fb^{-1}",true,0.19,0.81,0.05);
  AddText("ATLAS",true,0.19,0.89,0.060,72);
  AddText(m_histStatus,true,0.32,0.89,0.055,42);

  if( !m_regionName.IsWhitespace() ){

    double x_coordinate = 0.26;
    //if( m_regionName.Length()>=5 ) x_coordinate = 0.23;
    if( m_regionName.Length()>=5 ) x_coordinate = 0.23;
    if( m_regionName.Length()>=9 ) x_coordinate = 0.21;
    //void HistPlotter::AddText(TString myText,bool NDC,double xMin, double yMin, double textSize,int textFont)
    //AddText(m_regionName,true,x_coordinate,0.72,0.055,72);
    //AddText(m_regionName,true,x_coordinate,0.72,0.045);//change font since Jan28
    AddText(m_regionName,true,x_coordinate,0.72,0.06);//change font since Jul03
  }

  if( !m_regionName2.IsWhitespace() ){

    double x_coordinate = 0.28;
    //if( m_regionName.Length()>=5 ) x_coordinate = 0.21;
    //if( m_regionName2.Length()>=5 ) x_coordinate = 0.17;
    //AddText(m_regionName2,true,x_coordinate,0.65,0.055,72);
    //AddText(m_regionName2,true,0.275,0.66,0.045);//tt change font since Jan28
    AddText(m_regionName2,true,0.25,0.65,0.045);//W change font since Jan28
  }


}
// :------------------------------------------------------------------------------------------------------- //
void HistPlotter::addRegionToCanvas(RooPlot* frame,RooRealVar* regionVar,TString regionCatLabel)
{

  // First check if user has defined this plot to contain arrows
  // TODO: we cannot have arrows and a region name???

  std::map<TString,ArrowInfo*>::const_iterator it = m_arrowMap.find(regionCatLabel);
  if( it != m_arrowMap.end() ){
    ArrowInfo* arrow = it->second;
 
    for(unsigned int a=0; a<arrow->name.size(); a++){
      
      TArrow* arrow_ver = new TArrow(arrow->lowerCut[a],m_main_yMin+0.001,arrow->lowerCut[a],arrow->height[a] + arrow->height[a]*0.045,0.035,"|");
      arrow_ver->SetLineColor(kRed);
      arrow_ver->SetLineWidth(4);
      arrow_ver->Draw();

      TArrow* arrow_hor = new TArrow(arrow->lowerCut[a],arrow->height[a],arrow->lowerCutWidth[a],arrow->height[a],0.035,">");
      arrow_hor->SetLineColor(kRed);
      arrow_hor->SetLineWidth(4);
      arrow_hor->Draw();

    }
   
  }
  else{
    if(m_showRegionName) AddText(getRegionName(regionCatLabel),true,0.18,0.65,0.05,72);
  }
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addToLegend(RooPlot* frame,TLegend* leg,TString name,bool add_map_only,
			      bool is_curve,bool totalSM,bool signal)
{
 
  // Get legend name
  TString legend_entry_name = "";
  std::map<TString,TString>::const_iterator it_leg = m_legendMap.find(name);
  if( it_leg == m_legendMap.end() ){
    // Only add entry if found in map
    if(add_map_only) return;
    //
    legend_entry_name = name;
  }
  else{
    legend_entry_name = it_leg->second; 
  } 

  // For lack of better ideas, hard coded right now...
  if(totalSM){
    TLegendEntry* entry = leg->AddEntry("",legend_entry_name,"LF");
    entry->SetLineWidth(4);
    entry->SetLineColor(kBlack);
    entry->SetFillColor(1);
    entry->SetFillStyle(3004);
    return;
  }
  if(signal){

    // Name
    TString sig_entry = "Signal";
    std::map<TString,TString>::const_iterator it_leg_sig = m_legendMapSig.find(name);
    if( it_leg_sig == m_legendMapSig.end() ){
      std::cout << "<HistPlotter::addToLegend> WARNING Doesn't look like you gave this signal process a name?" << std::endl;
    }
    else{
      sig_entry = it_leg_sig->second;
    }

    // Color
    Int_t compPlotColor = kBlue;
    std::map<TString,Int_t>::const_iterator it_sig_color = m_colorMapSig.find(name);
    if( it_sig_color != m_colorMapSig.end() ) compPlotColor = it_sig_color->second;
    
    TLegendEntry* entry = leg->AddEntry("",sig_entry,"L");
    entry->SetLineWidth(4);
    entry->SetLineStyle(7);
    entry->SetLineColor( compPlotColor );
    entry->SetFillColor(kWhite);
    return;
  }
  //

  if(is_curve){
    RooCurve* curve = (RooCurve*) frame->findObject(0,RooCurve::Class()) ;
    if(curve){
      leg->AddEntry(curve,legend_entry_name,"F");
      return;
    }
  }
  else{
    RooHist* hist = (RooHist*) frame->findObject(0,RooHist::Class());
    if(hist){
      //leg->AddEntry(hist,legend_entry_name,"LP");
      leg->AddEntry(hist,legend_entry_name,"EP");
      return;
    }
  }
 
  cout << "Error::Cannot add" << name << " to legend"  << endl;
  
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::AddText(TString myText,bool NDC,double xMin, double yMin, double textSize,int textFont)
{
  TLatex text;
  // Normalized coordinates
  if(NDC) text.SetNDC();
  text.SetTextAlign(13);
  text.SetTextSize(textSize);
  text.SetTextFont(textFont);
  text.DrawLatex(xMin,yMin,myText);
  text.AppendPad();

}
// -------------------------------------------------------------------------------------------------------- //
TString HistPlotter::getRegionName(TString regionCatLabel)
{
  std::map<TString, TString>::const_iterator it;
  

  // Is this standard HF format? e.g. "REGION_variableName" 
  // If string doesn't contain the "_" separator, check to see if user has 
  // defined this label in the xAxisMap
  if( !regionCatLabel.Contains("_")) {
    it = m_regionMap.find(regionCatLabel);
    if( it == m_regionMap.end() ) return regionCatLabel;
    else                         return it->second;
  }
  else{
    TString label = ((TObjString*)regionCatLabel.Tokenize("_")->At(0))->String();
    
    it = m_regionMap.find(label);
    if( it == m_regionMap.end() ) return label;
    else                         return it->second;
  }

}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addProcess(TString process,TString legendName,Int_t color,int nLeg)
{

  // Define color for plotting
  m_colorMap.insert( pair<TString,Int_t>(process,color) );

  // Key is legend name, so if some processes are to be grouped, 
  // the use of a map ensures they are only added once if they
  // have the same legendName
  m_legendMap.insert( pair<TString,TString>(legendName,process) );

  // Save where to put this process in the legend
  groupProcessInMap(nLeg,process);

  m_processVec.push_back(process);


}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addSigProcess(TString process,TString legendName,Int_t color)
{
  m_colorMapSig.insert( std::pair<TString,Int_t>(process,color) );
  m_legendMapSig.insert( pair<TString,TString>(legendName,process) );

  m_processVec.push_back(process);
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addDataCut(TString regionName,TString cut)
{
  m_dataCuts.insert( pair<TString,TString>(regionName,cut) );
}
// -------------------------------------------------------------------------------------------------------- //
TString HistPlotter::getDataCut(TString region)
{
  std::map<TString, TString>::const_iterator it;
  it=m_dataCuts.find(region);
  if( it == m_dataCuts.end() ) return "";
  else                         return it->second;
}
// -------------------------------------------------------------------------------------------------------- //
TString HistPlotter::getXaxisLabel(TString regionCatLabel)
{
 
  std::map<TString, TString>::const_iterator it;

  // Is this standard HF format? e.g. "REGION_variableName" 
  // If string doesn't contain the "_" separator, check to see if user has 
  // defined this label in the xAxisMap
  if( !regionCatLabel.Contains("_")) {
    it = m_xAxisMap.find(regionCatLabel);
    
    if( it == m_xAxisMap.end() ) return regionCatLabel;
    else                         return it->second;
  }

  // TODO: Generalize such that if its not found, search the entire string for the name!
  else{
  
    //TObjArray *tx = regionCatLabel.Tokenize("_");
    //tx->Print();
    //cout<<((TObjString *)(tx->At(1)))->String()<<endl;
    TString label = ((TObjString*)regionCatLabel.Tokenize("_")->At(1))->String();
    
    cout<<label<<endl;
    it = m_xAxisMap.find(label);
    if( it == m_xAxisMap.end() ) return label;
    else                         return it->second;
  }
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addXaxisLabelToMap(TString oldLabel,TString newLabel)
{
  m_xAxisMap.insert( pair<TString,TString>(oldLabel,newLabel));
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addRegionToMap(TString oldLabel,TString newLabel)
{
  m_regionMap.insert( pair<TString,TString>(oldLabel,newLabel));
}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::groupProcessInMap(int nLeg,TString process)
{

  if(nLeg>3 || nLeg<1){
    std::cout << "<HistPlotter::groupProcessInMap> WARNING Can only specify nLeg [1,2], defaulting to 2" << std::endl;
    nLeg==2;
  }

  auto it = m_legendGrouping.find(process); 

  if(it != m_legendGrouping.end()){
    std::cout << "<HistPlotter::groupProcessInMap> WARNING Process " << process << " already registered for TLegend: " << it->second << std::endl;
    return;
  }
  else{
    m_legendGrouping.insert(std::pair<TString,int>(process,nLeg) );
  }

}
// -------------------------------------------------------------------------------------------------------- //
int HistPlotter::getNLegend(TString process)
{

  auto it = m_legendGrouping.find(process);

  if( it == m_legendGrouping.end() ){
    std::cout << "<HistPlotter::getNLegend> WARNING Cannot find process: " << process << std::endl;
    return 1;
  }   
  else{
    return it->second;
  }


}
// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::addArrowToCanvas(TString name,TString text,TString type,float height,
				   float lowerCut,float lowerCutWidth,
				   float upperCut)
{

  if(upperCut<0) upperCut=lowerCut;  

  ArrowInfo* arrow = 0;

  std::map<TString,ArrowInfo*>::const_iterator it = m_arrowMap.find(name);
  if( it != m_arrowMap.end() ) arrow = it->second;
  else arrow = new ArrowInfo();

  arrow->name.push_back(name);
  arrow->text.push_back(text);
  arrow->type.push_back(type);
  arrow->height.push_back(height);
  arrow->lowerCut.push_back(lowerCut);
  arrow->lowerCutWidth.push_back(lowerCutWidth);
  arrow->upperCut.push_back(upperCut);

  // Add to map if wasn't already found
  if( it == m_arrowMap.end() ) m_arrowMap.insert( pair<TString,ArrowInfo*>(name,arrow) );
}

// -------------------------------------------------------------------------------------------------------- //
void HistPlotter::reverseLegendMap()
{
  // Reverse map entries
  std::map<TString, TString>::const_iterator it;
  std::map<TString, TString> temp_map;
  for(it = m_legendMap.begin(); it != m_legendMap.end(); it++){
    TString first = it->first;
    TString second = it->second;
    temp_map.insert( pair<TString,TString>(second,first) );
  }
  m_legendMap = temp_map;

  std::map<TString, TString>::const_iterator sig;
  std::map<TString, TString> sig_map;
  for(sig= m_legendMapSig.begin(); sig != m_legendMapSig.end(); sig++){
    TString first = sig->first;
    TString second = sig->second;
    sig_map.insert( pair<TString,TString>(second,first) );
  }
  m_legendMapSig = sig_map;

}
// -------------------------------------------------------------------------------------------------------- //
RooWorkspace* HistPlotter::getWorkspace(TString rootFileName,TString workSpaceName)
{

  TFile* file = new TFile(rootFileName,"READ");

  if( !file->IsOpen() ){
      cout << "Error opening rootfile: " << rootFileName << endl;
      return NULL;
  }

  RooWorkspace* wS = (RooWorkspace*)file->Get(workSpaceName);

  if(wS==NULL){
    cout << "Warning::Workspace is NULL!" << endl;
    return NULL;
  }

  file->Close();

  return wS;

}
// -------------------------------------------------------------------------------------------------------- //
vector<TString> HistPlotter::getRegions(TString regions,RooCategory* cat)
{

  vector<TString> OutStringVec;
  OutStringVec.clear();

  //
  TIterator* iter = cat->typeIterator() ;
  RooCatType* catType ;

  //
  while( (catType = (RooCatType*) iter->Next())) {
    TString regionCatLabel = catType->GetName();

    if( m_printRegionsOnly ){
      std::cout << "<HistPlotter::getRegions> Print region: " << regionCatLabel << std::endl;
      continue;
    }

    // Default is to take all, if the user didn't request
    if(m_regionsToPlot.IsWhitespace()){
      OutStringVec.push_back(regionCatLabel);
    }
    else if (m_regionsToPlot.Contains(regionCatLabel)){
      OutStringVec.push_back(regionCatLabel);
    }

  }

  return OutStringVec;

}
// ------------------------------------------------------------------------------------------------------------------------//
void HistPlotter::addRegionToPlot(TString region)
{
  m_regionsToPlot += region+"  ";
}
// ------------------------------------------------------------------------------------------------------------------------//
RooHist* HistPlotter::makeRatioHist(/*RooAbsData* data*/ RooDataSet* regionData,RooAbsPdf* regionPdf,RooRealVar* regionVar,TString dataCatLabel)
{

  RooPlot* frame_dummy =  regionVar->frame(); 
  //data->plotOn(frame_dummy,Cut(dataCatLabel),RooFit::DataError(RooAbsData::Poisson));

  regionData->plotOn(frame_dummy,RooFit::DataError(RooAbsData::Poisson),MarkerColor(kBlack),LineColor(kBlack));
  regionPdf->plotOn(frame_dummy,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5));

  // *** LARGELY COPIED FROM ROOHIST IN HF TRUNK ***

  // Find curve object
  RooCurve* curve = (RooCurve*) frame_dummy->findObject(0,RooCurve::Class()) ;
  if (!curve) {
    cout << "Error::Cannot make ratio histogram, cannot find data hist";
    return NULL;
  }

  // Find histogram object
  RooHist* hist = (RooHist*) frame_dummy->findObject(0,RooHist::Class()) ;
  if (!hist) {
    cout << "Error::Cannot make ratio histogram, cannot find total PDF hist";
    return NULL ;
  }

  // Copy all non-content properties from hist
  RooHist* return_hist = new RooHist( hist->getNominalBinWidth() ) ;

  // Determine range of curve 
  Double_t xstart,xstop,y ;
  curve->GetPoint(0,xstart,y) ;
  curve->GetPoint(curve->GetN()-1,xstop,y) ;
  
  // Add histograms, calculate Poisson confidence interval on sum value
  for(Int_t i=0 ; i<hist->GetN() ; i++) {    
    Double_t x,point;
    hist->GetPoint(i,x,point) ;

    // Only calculate pull for bins inside curve range
    if (x<xstart || x>xstop) continue ;
    
    Double_t yy ;
    Double_t dyl = hist->GetErrorYlow(i) ;
    Double_t dyh = hist->GetErrorYhigh(i) ;
  
    // std::cout << "Curve value: " << curve->interpolate(x) << std::endl;

    yy = point / curve->interpolate(x) ;
    dyl /= curve->interpolate(x) ;
    dyh /= curve->interpolate(x) ;
    
    // do not add a ratio plot point if no data in bin
    if(fabs(point) < 0.000001 ) continue;

    return_hist->addBinWithError(x,yy,dyl,dyh);
  }

  return return_hist ;

}
// ------------------------------------------------------------------------------------------------------------------------//
RooCurve* HistPlotter::MakePdfErrorRatioHist(RooWorkspace* w, RooDataSet*  regionData, RooAbsPdf* regionPdf, 
					     RooRealVar* regionVar, RooFitResult* rFit, Double_t Nsigma){

  if(rFit==NULL) return 0;

  // curvename=0 means that the last RooCurve is taken from the RooPlot
  const char* curvename = 0;

  RooPlot* frame =  regionVar->frame(); 
  regionData->plotOn(frame, RooFit::DataError(RooAbsData::Poisson));

  // normalize pdf to number of expected events, not to number of events in dataset
  double normCount = regionPdf->expectedEvents(*regionVar);
  regionPdf->plotOn(frame,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5));
  RooCurve* curveNom = (RooCurve*) frame->findObject(curvename,RooCurve::Class()) ;
  if (!curveNom) {
    return 0 ;
  }

  if(rFit != NULL) regionPdf->plotOn(frame,Normalization(1,RooAbsReal::RelativeExpected),Precision(1e-5),FillColor(kBlue-5),FillStyle(3354),VisualizeError(*rFit,Nsigma));

  // Find curve object
  RooCurve* curveError = (RooCurve*) frame->findObject(curvename,RooCurve::Class()) ;
  if (!curveError) {
    return 0 ;
  }

  RooCurve* ratioBand = new RooCurve ;
  ratioBand->SetName(Form("%s_ratio_errorband",curveNom->GetName())) ;
  ratioBand->SetLineWidth(2) ;
  ratioBand->SetLineColor(kBlue-5);
  ratioBand->SetFillColor(kBlue-5);
  ratioBand->SetFillStyle(3004);
  ratioBand->SetFillColor(kBlack);
  

  Int_t j = 0;
  Bool_t bottomCurve = kFALSE;
  for(Int_t i=1; i<curveError->GetN()-1; i++){
    Double_t x = 0.;
    Double_t y = 0.;
    curveError->GetPoint(i,x,y) ;

    // errorBand curve has twice as many points as does a normal/nominal (pdf) curve
    // first it walks through all +1 sigma points (topCurve), then the -1 sigma points (bottomCurve)
    // to divide the errorCurve by the pdfCurve, we need to count back for the pdfCurve once we're in the middle of errorCurve
    if( i >= (curveNom->GetN()-1) ) bottomCurve = kTRUE;
    
    Double_t xNom = x;
    Double_t yNom = y;
    
    // each errorCurve has two more points just outside the plot, so we need to treat them separately
    if( i == (curveNom->GetN() - 1) ||  i == curveNom->GetN() ){
      //  xNom = x;
      //  yNom = 0.;
      ratioBand->addPoint(x, 0.);   
      continue;
    }
    
    
    if( bottomCurve){
      curveNom->GetPoint(j,xNom,yNom);

      j--;
    } else {
      j++;
      curveNom->GetPoint(j,xNom,yNom);
    }
    
    // only divide by yNom if it is non-zero
    if(  fabs(yNom) > 0.00001 ){ 
      ratioBand->addPoint(x, (y / yNom));  
    } else { 
      ratioBand->addPoint(x, 0.);       	    
    }
  }
  

  return ratioBand;
}
HistPlotter::~HistPlotter(){}





/*
   if(regionCatLabel.Contains("nbj")){
    frame2->GetXaxis()->SetBinLabel(1,"0");
    frame2->GetXaxis()->SetBinLabel(2,"1");
    frame2->GetXaxis()->SetBinLabel(3,"2");
    frame2->GetXaxis()->SetTitleOffset(1.16);
    frame2->GetXaxis()->SetLabelSize(0.23);
  }

  if(regionCatLabel.Contains("mct")){
    frame2->SetNdivisions(506);
  }
*/

