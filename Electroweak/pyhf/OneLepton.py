import cabinetry
print("cabinetry version: %s" % cabinetry.__version__)
print("located at: %s" % cabinetry.__path__[0])
cabinetry.set_logging()
import pyhf
print("pyhf version: %s" % pyhf.__version__)
print("located at: %s" % pyhf.__path__[0])
import tools
import plot_utils
import numpy as np
print("numpy version: %s" % np.__version__)
import mplhep as hep
from pathlib import Path
import awkward as ak
print("awkward version: %s" % ak.__version__)
import uproot
print("uproot version: %s" % uproot.__version__)
print("located at: %s" % uproot.__path__[0])
import logging
from tabulate import tabulate


input_path="/eos/user/v/vtudorac/skimmed_with_JET_LargeR_JER/"
#print(configs/config.yml)
config = cabinetry.configuration.load('configs/config.yml')
# create histogram path
Path(config['General']['HistogramFolder']).mkdir(parents=True, exist_ok=True)
# set histogram path and update files
config['General']['InputPath'] = input_path+'{SamplePath}'
#print("aici",config['General']['InputPath'])
#config['General']['VariationPath'] = input_path+'NoSys'
#tools.update_files(config, input_path)

# update region definitions
tools.update_regions(config)
tools.update_systematics(config)

# and print
cabinetry.configuration.print_overview(config)
print(config)
# create histograms
cabinetry.templates.build(config, method="uproot")

# build a workspace
# suppress WARNING messages from cabinetry - VSCode memory consumption is too high
#logging.getLogger("cabinetry").setLevel(logging.ERROR)
ws = cabinetry.workspace.build(config)
#logging.getLogger("cabinetry").setLevel(logging.DEBUG)
# fix workspace, i.e. replace zero/nans with epsilontools.zeroless_ws(ws)

# get model and observed data
model, data = cabinetry.model_utils.model_and_data(ws)

# obtain pre-fit model predictions
model_pred_prefit = cabinetry.model_utils.prediction(model)

# blind data in the SR
blinded_data = tools.blind_data(data, config, model_pred_prefit, model)

# show yields
cabinetry.tabulate.yields(model_pred_prefit, blinded_data, per_bin=False, per_channel=True)

# prune SRs & VRs from workspace
redundant_channels = [region['Name'] for region in config['Regions'] if 'SR' in region['Name'] or 'VR' in region['Name']]
#pruned_workspace = pyhf.Workspace(ws).prune(channels=redundant_channels)


# get pruned model and pruned observed data
#pruned_model, pruned_data = cabinetry.model_utils.model_and_data(pruned_workspace)

'''
# fit
bkgOnlyFit_results = cabinetry.fit.fit(pruned_model, pruned_data)

# visualize
cabinetry.visualize.pulls(bkgOnlyFit_results)

# put back the VRs & SRs to the results
bkgOnlyFit_results_for_full_model = cabinetry.model_utils.match_fit_results(model, bkgOnlyFit_results)


# obtain post-fit model prediction and use it to blind SR data
model_pred_postfit = cabinetry.model_utils.prediction(model, fit_results=bkgOnlyFit_results_for_full_model)
blinded_data = tools.blind_data(data, config, model_pred_postfit, model)


# show post-fit yield table
cabinetry.tabulate.yields(model_pred_postfit, blinded_data, per_bin=False, per_channel=True)

# visualize agreement
#tools.plot_pulls(model_pred_postfit, blinded_data, ylim=2, xticks_fontsize=15)

# show post-fit yield table
cabinetry.tabulate.yields(model_pred_postfit, data, per_bin=False, per_channel=True)

# visualize agreement
#tools.plot_pulls(model_pred_postfit, data, ylim=2, xticks_fontsize=15)
# calculate uncertainties breakdown
unc_per_channel = tools.uncertainties_impact_method1(model, bkgOnlyFit_results_for_full_model)
cabinetry.workspace.save(ws, "workspace.json")
'''
