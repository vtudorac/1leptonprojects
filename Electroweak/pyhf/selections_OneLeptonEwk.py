import fnmatch,collections

selections=collections.OrderedDict()

def getPreselections():

    
   selections["preSelection_1l"]="(nLep_base==1) * (nLep_signal==1) * (nJet30>=1) * (nJet30<=3) * (met>200.) * (mt>50.) * (trigMatch_singleLepTrig==1) * (nLep_combiBase<3) * (nLep_combiBaseHighPt<2)"
   selections["boostedSRSelection_WZ"] = "(lep1Pt>25.) * (nJet30>=1) * (nJet30<=3) *(meffInc30>600)* (met>200.) * (dPhi_lep_met<2.6) * (nFatjets>=1) * (fatjet1Ztagged==1) * (fatjet1Pt>250.) * (met_Signif>12.) * (((mjj>80.) * (mjj<100.) * (meffInc30<850.)) | (meffInc30>850.))"
   selections["SRLMboostedWZ"] = selections["preSelection_1l"] + "*" + selections["boostedSRSelection_WZ"] + "* (mt>120.) *(mt<200.)"
  
  
    
   selections["WDB1LBoostedSelection"] = "(lep1Pt>25.) * (nJet30>=1) * (nJet30<=3) * (nBJet30_DL1==0) * (met>200.) * (dPhi_lep_met<2.9) * (nFatjets>=1) * (fatjet1Wtagged==1) * (fatjet1Pt>250.)"
   selections["WDB1LCRboosted"] = selections["preSelection_1l"] + "*" + selections["WDB1LBoostedSelection"] + "* (met_Signif<12.) * (mt>50.) * (mt<80.)"
   
   
   selections["TBoostedSelection"] = "(lep1Pt>25.) * (nJet30>=1) * (nJet30<=3) * (nBJet30_DL1>0) * (met>200.) * (dPhi_lep_met<2.9) * (nFatjets>=1) * (fatjet1Wtagged==1) * (fatjet1Pt>250.)*(meffInc30>600)"
   selections["TCRboosted"] = selections["preSelection_1l"] + "*" + selections["TBoostedSelection"] + "* (met_Signif<12) * (mt>50.) * (mt<80.)"
   
   
   selections["WDB1LVR1boosted"] = selections["preSelection_1l"] + "*" + selections["WDB1LBoostedSelection"] + "* (met_Signif<12.) * (mt>80.)"
   selections["WDB1LVR2boosted"] = selections["preSelection_1l"] + "*" + selections["WDB1LBoostedSelection"] + "* (met_Signif>12.) * (mt>50.) *( mt<120.)"
   
getPreselections()
