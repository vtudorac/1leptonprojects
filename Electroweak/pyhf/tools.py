from os import listdir
import glob
from selections_OneLeptonEwk import selections as selection_strings
from cabinetry.model_utils import ModelPrediction
from typing import Any, Dict, List, Tuple
import math
import numpy as np
import pandas as pd
from concurrent.futures._base import Future
import scipy
import pyhf
from cabinetry.fit.results_containers import FitResults
from cabinetry import model_utils
import cabinetry
import matplotlib.pyplot as plt
import copy

def update_regions(config: Dict[str, Any]) -> None:
    '''
    Utility function to fetch latest region definition string
    '''

    # loop ever regions
    for region in config['Regions']:
        region_name = region['Name']
        # strip out _var or -something
        if '_' in region_name:
            region_name = region_name.split('_')[0]
        if '-' in region_name:
            region_name = region_name.split('-')[0]

        selection_string = selection_strings[region_name]
        
        # cleaning
        selection_string = selection_string.split('*')
        selection_string = [cut for cut in selection_string if '||' not in cut]
        selection_string = [cut.replace('!', '~') for cut in selection_string]
        
        # join selection cuts and assign
        region['Filter'] = '*'.join(selection_string)
        

	

def update_systematics(config:Dict[str,Any])->None:
    allNewSys = []
    treeNames = {}
    for sample in config['Samples']:
        treeNames[sample['Name']] = sample['Tree'].replace('_NoSys', '')
    for systematic in config['Systematics']:
        samples = systematic['Samples']
        systematics_name=systematic['Name']
    	
        for sample in samples:
            newSys = copy.deepcopy(systematic)
            #print("newSys=",newSys) 
            newSys['Name'] = systematics_name + '_' + sample
            newSys['Samples'] = [sample]
            for v in ('Up', 'Down'):
                newSys[v]['Tree'] = newSys[v]['Tree'].replace(
                    "SAMPLE", treeNames[sample])
            allNewSys.append(newSys)

    config['Systematics'] = allNewSys
    
    
def update_files(config: Dict[str, Any], path: str) -> None:
    '''
    Utility function to fetch available local files
    '''

    # output lists
    # files = {'Data': [],
    #          'Other': [],
    #          'WJets': [],
    #          'ZJets': [],
    #          'Signal_800_1': [],
    #          'Signal_900_1': [],
    #           .
    #           .
    #           .
    #          }
    files = {}
    # read config and construct files dict
    for sample in config['Samples']:
        files[sample['Name']] = []


    for file in listdir(path):
        if not '.root' in file:
            continue

        if '.data.' in file and 'Data' in files.keys():
            files['Data'].append(file)
        elif 'Sh_2211_W' in file and 'WJets' in files.keys():
            files['WJets'].append(file)
        elif 'Sh_2211_Z' in file and 'ZJets' in files.keys():
            files['ZJets'].append(file)
        elif '_TT_' in file:
            key_name = 'Signal_'+'_'.join(file.split('.')[2].split('_')[4:6])
            if key_name in files.keys():
                files[key_name].append(file)
        else:
            if not 'Sh_2211_W' in file and not 'Sh_2211_Z' in file and not '_TT_' in file and not '.data.' in file and 'Other' in files.keys():
                files['Other'].append(file)

    # put them into the config
    for sample in config['Samples']:
        sample['SamplePath'] = files[sample['Name']]


def check_missing_campaigns(path: str) -> None:
    '''
    Utility function to check if there are missing MC campaigns
    VK: would be good to check against a frozen list of DSIDs
    DSIDs =['345935', '363355', '363356', '363357', '363358', '363359', '363360', '363489', '363494', '364250', '364253', '364254', '364255', '364283', '364284', '407345', '407346', '407347', '410155', '410156', '410157', '410220', '410278', '410470', '410644', '410645', '410646', '410647', '410658', '410659', '700320', '700321', '700322', '700323', '700324', '700325', '700326', '700327', '700328', '700329', '700330', '700331', '700332', '700333', '700334', '700335', '700336', '700337', '700338', '700339', '700340', '700341', '700342', '700343', '700344', '700345', '700346', '700543', '700544', '436705', '436706', '436708', '436709', '436712', '436713', '436714', '436715', '436717', '436718', '436719', '436720', '436728', '436729', '436730', '436731', '436739', '436740', '436741', '436742', '436753', '436754', '436755', '436756', '436764', '436765', '436766', '436767', '436778', '436779', '436780', '436781', '500383', '500384', '500385', '500386', '500387', '500388', '500389', '500390', '500391', '500392', '500393', '500394', '500395', '500396', '500397', '500398', '500399', '500400', '500401', '500402', '500403', '500404', '500405', '500406', '500407', '500408', '500409', '500410', '500411', '500412', '500413', '500414', '500415', '500416', '500417', '500418', '500419', '500420', '500421', '500422', '500423', '500424', '500425', '500426', '500427', '500428', '500429', '500430', '500431', '500432', '500433', '500434', '500435', '500436', '500437', '500438', '500439', '500440', '500441', '500442', '500443', '500444', '500445', '500446', '500447', '500448', '500449', '500450', '500451', '500452', '500453', '500454', '500455', '500456', '511813', '511814', '511815', '511816', '511817', '511818', '511819', '511820', '511821', '511822', '511823', '511824', '511825', '511826', '511827', '511828', '511829', '511830', '521265', '521266', '521267', '521268', '521269', '521270', '521271', '521272', '521273', '521274', '521275', '521276', '521277', '521278', '521279', '521280', '521281', '521282', '521283']
    '''

    # dict to hold DSID : [mc16a, mc16d, mc16e]
    database = {}

    # iterate over files and put to database
    total_number_of_files = 0
    for file in glob.glob(path+'mc16_13TeV.*.root'):
        total_number_of_files+=1
        filename = file.split('/')[-1]
        dsid = filename.split('.')[1]
        rtag = filename.split('.')[-2].split('_')[-2]

        if dsid not in database.keys():
            database[dsid] = [filename]
        
        else:
            database[dsid].append(filename)

    # check the length of the sample list of each key
    missing_files = 0
    missing_dsids = []
    print("checking for missing MC campaigns...\n")
    for dsid in database.keys():
        number_of_files_per_dsid = len(database[dsid])
        assert number_of_files_per_dsid <= 3, "number of files for DSID=%s greater than 3. something going wrong..." % dsid

        if number_of_files_per_dsid < 3:
            missing_files+=1
            missing_dsids.append(dsid)
            if number_of_files_per_dsid < 2:
                missing_files+=1
                print("from DSID=%s there is actually only a single file!" % dsid)

    print("\nSummary: Total %i files found. There are %i missing files." % (total_number_of_files, missing_files))
    assert total_number_of_files+missing_files == 606, "Missing DSID(s)!"
    print("\nfiles are missing from the following DSID:")
    print(missing_dsids)

    print("\nExamples (from files found):")
    for dsid in missing_dsids:
        print(dsid, database[dsid][0])


def blind_data(data, 
               config,
               model_pred: ModelPrediction,
               model):
    '''
    Ulitity function to blind the data in the SRs (defined in config)
    by substituting with floor of total MC (found in model_pred)
    * model is still needed to find the SR indices - vangelis: could I avoid?
    '''

    SRs =[]

    for region in config['Regions']:
        region_name = region['Name']
        
        # strip out _var
        if '_' in region_name:
            region_name = region_name.split('_')[0]

        if 'SR' in region_name: SRs.append(region_name)
    
    # create a copy by-value of data so we don't modify the original list
    local_data = data.copy()

    for region in SRs:
        sr_indices = model.config.channel_slices[region]
        total_mc = np.sum(model_pred.model_yields[sr_indices])
        local_data[sr_indices] = [np.floor(total_mc)]
    
    return local_data
    
    
def plot_pulls(model_postfit: ModelPrediction, 
               data: List[float],
               ylim: int = 2,
               xticks_fontsize: int = 16
               ) -> None:
    '''
    Draw the Data/MC(post-fit) pull plot
    '''

    model_yields = model_postfit.model_yields
    model_errors = model_postfit.total_stdev_model_channels
    region_names = model_postfit.model.config.channels.copy()
    regions_to_remove = [region for region in region_names if '_' in region]
    for region in regions_to_remove:
        region_names.remove(region)

    sigmas = []
    for i,region in enumerate(model_yields):
        if i == len(region_names): break

        yields = list(map(lambda x: x[0], region))
        total_SM = sum(yields)
        total_SM_error = model_errors[i][-1]
        datum = data[i] # only use the first len(model_yields) entries
        sigma = calculate_ATLAS_significance(datum, total_SM, total_SM_error)
        sigmas.append(sigma)

    fig = plt.figure(figsize=(7, 4), dpi=150)
    plt.bar(region_names, sigmas)

    # cosmetics
    plt.ylim([-ylim, ylim])
    y_hlines = range(-ylim, ylim+1)
    for y in y_hlines:
        plt.axhline(y=y, linewidth= 1, color='gray', linestyle= 'dashed')
    # plt.ylabel(r'$({n}_{obs} - n_{pred})/\sigma_{tot}$')
    plt.ylabel("Significance")

    # Set x tick font size
    ax = plt.gca()
    for label in (ax.get_xticklabels()):
        label.set_fontsize(xticks_fontsize)

    fig.set_tight_layout(True)



def zeroless_ws(workspace, epsilon = 1e-5):
    '''
    Utility function to look all the bins of the workspace and substitute zeros (nominal and statterror) with 1e-5
    this is a temporary fix until the next release of cabinetry+pyhf that could handle zero statterror
    
    pro tip: first try epsilon 1e-5. if does not work, increase.
    '''
    
    # loop over regions
    for region_idx in range( len(workspace['channels']) ):
        region = workspace['channels'][region_idx] #dict

        # loop over samples
        for sample_idx in range( len(region['samples']) ):
            sample = region['samples'][sample_idx] #dict
            
            # flag indicating zero bins found
            zero_found = False

            # keep a copy of the nominal yield
            data_tmp = sample['data']

            # loop over modifiers
            for modifier_idx in range( len(sample['modifiers']) ):
                modifier = sample['modifiers'][modifier_idx]

                # replace statterror zero bins
                if modifier['type'] == 'staterror':
                    single_bin = True if len(modifier['data']) == 1 else False
                    
                    for bin_idx in range( len(modifier['data']) ):
                        counts = modifier['data'][bin_idx]
                        if counts == 0.0:
                            zero_found = True
                            print("found a zero staterror bin!")
                            if single_bin:
                                print("this is a single bin regions, potential nan values will be replaced as well.")
                            # VK: this actually not needed anymore
                            # modifier['data'][bin_idx] = epsilon

                # replace nan entries -- issue found only on single bin regions with systematics
                if single_bin and zero_found:
                    # weights
                    if modifier['type'] == 'normsys':
                        modifier['data'] = {'hi': 1.0, 'lo': 1.0}

                    # histosys entries
                    if modifier['type'] == 'histosys':
                        modifier['data']['hi_data'] = data_tmp
                        modifier['data']['lo_data'] = data_tmp

                # there are cases where histosys can be nan without zero_found == True
                # VK: not sure about the single_bin value here
                if modifier['type'] == 'histosys':
                    if np.isnan(modifier['data']['hi_data']).any():
                        modifier['data']['hi_data'] = data_tmp
                    if np.isnan(modifier['data']['lo_data']).any():
                        modifier['data']['lo_data'] = data_tmp

                # there are cases where normsys can be zero without zero_found == True
                # VK: not sure about the single_bin value here
                if modifier['type'] == 'normsys':
                    if modifier['data']['hi'] == 0.0 or np.isnan(modifier['data']['hi']):
                        modifier['data']['hi'] = 1.0
                    if modifier['data']['lo'] == 0.0 or np.isnan(modifier['data']['lo']):
                        modifier['data']['lo'] = 1.0
                    # if modifier lo data is less than zero, replace with 1 / hi
                    if modifier['data']['lo'] < 0.0:
                        raise ValueError(f"normsys low value is negative in region {region['name']}, sample {sample['name']}, modifier {modifier['name']}.")
                        # modifier['data']['lo'] = 1 / modifier['data']['hi']
                    elif modifier['data']['hi'] < 0.0:
                        raise ValueError(f"normsys high value is negative in region {region['name']}, sample {sample['name']}, modifier {modifier['name']}.")
                    
            
            # for binned regions zero nominal yield + zero staterror is problematic
            if single_bin == True: continue
            # I only need to do this for the first sample, but just in case I do in all of them...
            # if single_bin == True or sample_idx > 0: continue
            # modify data_tmp: 1) replace negative with zero 2) in each zero add epsilon
            data_tmp = list(map(lambda x: 0.0 if (x<0.0) else x, data_tmp))
            data_tmp = list(map(lambda x: x+epsilon if (x==0.0) else x, data_tmp))
            sample['data'] = data_tmp
            # staterror
            sample['modifiers'][0]['data'] = list(map(lambda x: epsilon if (x==0.0) else x, sample['modifiers'][0]['data']))

# helper function definition
def get_results_df(results: Dict[str, Future]) -> pd.DataFrame:
    '''
    Convert the results dictionary into a dataframe
    '''

    # create dataframe structure
    results_df = pd.DataFrame(columns=['stop', 
                                       'chi1',
                                       'observed_p_value',
                                       'observed_p_value_plusOneSigma',
                                       'observed_p_value_minusOneSigma',
                                       'expected_p_value',
                                       'expected_p_value_plusOneSigma',
                                       'expected_p_value_minusOneSigma',
                                       'observed_signif',
                                       'observed_signif_plusOneSigma',
                                       'observed_signif_minusOneSigma',
                                       'expected_signif',
                                       'expected_signif_plusOneSigma',
                                       'expected_signif_minusOneSigma'])

    # fill dataframe from results dictionary
    for i,key in enumerate(results.keys()):
        stop_m = int(key.split('_')[-2])
        chi1_m = int(key.split('_')[-1])
        obs_p_val = float(results[key].result()[0][1] if results[key].result()[0][1] is not None else np.nan)
        obs_p_val_plusOneSigma = float(results[key].result()[0][2] if results[key].result()[0][2] is not None else np.nan)
        obs_p_val_minusOneSigma = float(results[key].result()[0][0] if results[key].result()[0][0] is not None else np.nan)
        exp_p_val = float(results[key].result()[1][1] if results[key].result()[1] is not None else np.nan)
        exp_p_val_plusOneSigma = float(results[key].result()[1][2] if results[key].result()[1] is not None else np.nan)
        exp_p_val_minusOneSigma = float(results[key].result()[1][0] if results[key].result()[1] is not None else np.nan)
        obs_signif = scipy.stats.norm.isf(obs_p_val, 0, 1)
        obs_signif_plusOneSigma = scipy.stats.norm.isf(obs_p_val_plusOneSigma, 0, 1)
        obs_signif_minusOneSigma = scipy.stats.norm.isf(obs_p_val_minusOneSigma, 0, 1)
        exp_signif = scipy.stats.norm.isf(exp_p_val, 0, 1)
        exp_signif_plusOneSigma = scipy.stats.norm.isf(exp_p_val_plusOneSigma, 0, 1)
        exp_signif_minusOneSigma = scipy.stats.norm.isf(exp_p_val_minusOneSigma, 0, 1)
        
        results_df.loc[i] = pd.Series({'stop':stop_m,
                                       'chi1':chi1_m,
                                       'observed_p_value':obs_p_val, 
                                       'observed_p_value_plusOneSigma':obs_p_val_plusOneSigma, 
                                       'observed_p_value_minusOneSigma':obs_p_val_minusOneSigma, 
                                       'expected_p_value':exp_p_val, 
                                       'expected_p_value_plusOneSigma':exp_p_val_plusOneSigma, 
                                       'expected_p_value_minusOneSigma':exp_p_val_minusOneSigma,
                                       'observed_signif':obs_signif, 
                                       'observed_signif_plusOneSigma':obs_signif_plusOneSigma, 
                                       'observed_signif_minusOneSigma':obs_signif_minusOneSigma, 
                                       'expected_signif':exp_signif, 
                                       'expected_signif_plusOneSigma':exp_signif_plusOneSigma, 
                                       'expected_signif_minusOneSigma':exp_signif_minusOneSigma})

    # calculate DM column
    results_df['DeltaM'] = results_df['stop'] - results_df['chi1']
    # fix types
    results_df = results_df.astype({'stop':int, 'chi1':int, 'DeltaM':int})
    # bring DeltaM column after chi1
    results_df = results_df[['stop', 
                             'chi1', 
                             'DeltaM', 
                             'observed_p_value', 
                             'observed_p_value_plusOneSigma', 
                             'observed_p_value_minusOneSigma', 
                             'expected_p_value', 
                             'expected_p_value_plusOneSigma', 
                             'expected_p_value_minusOneSigma', 
                             'observed_signif', 
                             'observed_signif_plusOneSigma', 
                             'observed_signif_minusOneSigma', 
                             'expected_signif', 
                             'expected_signif_plusOneSigma', 
                             'expected_signif_minusOneSigma']]

    return results_df

def force_signal_yield_to_one(workspace: Dict, config: Dict) -> Dict:
    '''
    Force signal yield in SR to 1.00 and remove stat error
    '''
    # make sure the SR channel is the first channel in the workspace
    sr_channel_name = [ region['Name'] for region in config['Regions'] if 'SR' in region['Name'] ][0]
    sr_channel = workspace['channels'][0]
    if sr_channel['name'] != sr_channel_name:
        raise ValueError("SR channel should be the first channel in the workspace")
    
    # loop over the samples of the SR channel
    for sample in sr_channel['samples']:
        if 'Signal' in sample['name']:
            sample['data'] = [1.0]
            # check the type of the fist modifier
            first_modifier_type = sample['modifiers'][0]['type']
            if first_modifier_type != 'staterror':
                raise AssertionError("First modifier should be the staterror")
            # delete the staterror modifier
            del sample['modifiers'][0]

    return workspace

def uncertainties_impact_method1(model: pyhf.pdf.Model, 
                                 fit_results: FitResults
                                 ) -> Dict[str, Dict[str, Tuple[float, float]]]:
    '''
    This function calculates the systematic uncertainty breakdown on the post-fit background prediction using the method 1 of the HistFitter paper (https://arxiv.org/pdf/1410.1280.pdf, page 27).
    
    Returns:
    Dict[region, Dict[NP, Tuple[error, relative_error]]] sorted in descending order
    '''

    # get the channels of the model
    channels = model_utils._filter_channels(model, None)

    # keep the nominal total post-fit prediction per channel handy
    nominal_model_postfit = cabinetry.model_utils.prediction(model, fit_results=fit_results)
    nominal_total_yield_per_channel = [round(np.sum(yield_per_channel), 2) for yield_per_channel in nominal_model_postfit.model_yields]
    
    results = {}
    # loop over the post-fit results
    for unc, label in zip(fit_results.uncertainty, fit_results.labels):
        # print the uncertainty label you are working on
        print(f" estimating the impact of {label} ...")
    
        # set all the parameter uncertainties to zero except for the current parameter
        tmp_unc_array = np.array([0.0 if label_ != label else unc for label_ in fit_results.labels])

        # make a new FitResults object with same properties as fit_results except new uncertainties
        tmp_fit_results = FitResults(fit_results.bestfit,
                                     tmp_unc_array,
                                     fit_results.labels,
                                     fit_results.corr_mat,
                                     fit_results.best_twice_nll,
                                     fit_results.goodness_of_fit)

        # obtain post-fit model prediction
        tmp_model_postfit = cabinetry.model_utils.prediction(model, fit_results=tmp_fit_results)

        # uncertainties per channel
        total_unc_per_channel = [round(unc_per_channel[-1], 2) for unc_per_channel in tmp_model_postfit.total_stdev_model_channels]
        total_rel_unc_per_channel = ( np.round((np.array(total_unc_per_channel)/np.array(nominal_total_yield_per_channel))*100, 1) ).tolist()

        # combine channels and total_unc_per_channel in a dict
        channel_unc_dict = dict(zip(channels, zip(total_unc_per_channel, total_rel_unc_per_channel)))

        results[label] = channel_unc_dict

    # results per channel
    results_per_channel = {}
    for channel in channels:
        results_per_channel[channel] = {unc_label: result[channel] for unc_label, result in results.items()}
        # sort by descending uncertainty
        results_per_channel[channel] = {k: v for k, v in sorted(results_per_channel[channel].items(), key=lambda item: item[1][0], reverse=True)}

    return results_per_channel

def calculate_Gaussian_significance(obs: float, pred: float, pred_error: float) -> float:
    '''
    Calculate the Gaussian significance
    '''
    return (obs - pred)/pred_error

def calculate_ATLAS_significance(nbObs: float, nbExp: float, nbExpEr: float) -> float:
    '''
    Calculate the significance as defined in https://cds.cern.ch/record/2643488
    '''
    factor1 = nbObs*math.log( (nbObs*(nbExp+nbExpEr**2))/(nbExp**2+nbObs*nbExpEr**2) )
    factor2 = (nbExp**2/nbExpEr**2)*math.log( 1 + (nbExpEr**2*(nbObs-nbExp))/(nbExp*(nbExp+nbExpEr**2)) )

    if nbObs < nbExp:
        pull  = -math.sqrt(2*(factor1 - factor2))
    else:
        pull  = math.sqrt(2*(factor1 - factor2))
    
    return pull

def make_normalized_systematics(workspace: Dict[str, Any], 
                                norm_samples: List[str], 
                                norm_regions: List[str],
                                systematics: List[str]
                                ) -> Dict[str, Any]:
    """
    This is a utility function to turn systematics to normalized systematics.
    The normalized systeamatics definition is given in the HistFitter paper (https://arxiv.org/abs/1410.1280) in Section 4.4.
    
    THIS IS STILL WIP!
    """
    for sample in norm_samples:
        for sytematic in systematics:
            # init temporaty histograms
            sum_nominal = sum_up = sum_down = 0
            # print(f"sample: {sample}, systematic: {sytematic}")
            
            # loop over channels
            channels = workspace['channels']
            for channel in channels:
                # only if in a normalization channel/region
                if channel['name'] in norm_regions:
                    # loop over the samples of the channel
                    for s in channel['samples']:
                        if s['name'] == sample:
                            # get the nominal histogram and accumulate it
                            histo_nominal = s['data'][0]
                            sum_nominal += histo_nominal
                            # get the modifiers
                            for modifier in s['modifiers']:
                                if modifier['name'] == sytematic:
                                    # get the up/down histograms and accumulate them
                                    sum_up += modifier['data']['hi']*histo_nominal
                                    sum_down += modifier['data']['lo']*histo_nominal
                                    
            # thus far you should have accumulated the histograms
            # print(f"nominal sum: {sum_nominal}")
            # print(f"up sum: {sum_up}")
            # print(f"down sum: {sum_down}")
            
            # calculate the correction factors if the variation sums are not zero
            if sum_up != 0:
                corr_up = sum_nominal/sum_up
            else:
                corr_up = 1
            if sum_down != 0:
                corr_down = sum_nominal/sum_down
            else:
                corr_down = 1
            if sum_up == 0 and sum_down != 0 or sum_up != 0 and sum_down == 0:
                print("WARNING: one of the sums is zero but the other is not!")
            
            # multiply the correction factors with the up/down modifier data of the workspace
            # apply to all the channels/regions
            # JL: I believe it would need the ratio of SR to CR yields to do that properly.
            for channel in channels:
                    # loop over the samples of the channel
                    for s in channel['samples']:
                        if s['name'] == sample:
                            # get the modifiers
                            for modifier in s['modifiers']:
                                if modifier['name'] == sytematic:
                                    modifier['data']['hi'] *= corr_up
                                    modifier['data']['lo'] *= corr_down
                                    
        #     # as a cross check re-calculate the sums
        #     sum_nominal = sum_up = sum_down = 0
        #     for channel in channels:
        #         print(channel['name'])
        #         # if channel['name'] in norm_regions:
        #         # loop over the samples of the channel
        #         for s in channel['samples']:
        #             if s['name'] == sample:
        #                 # get the nominal histogram and accumulate it
        #                 histo_nominal = s['data'][0]
        #                 sum_nominal += histo_nominal
        #                 # get the modifiers
        #                 for modifier in s['modifiers']:
        #                     if modifier['name'] == sytematic:
        #                         # get the up/down histograms and accumulate them
        #                         sum_up += modifier['data']['hi']*histo_nominal
        #                         sum_down += modifier['data']['lo']*histo_nominal
                                    
        #     # thus far you should have accumulated the histograms
        #     print(f"sample: {sample}, systematic: {sytematic}")
        #     print(f"nominal sum: {sum_nominal}")
        #     print(f"up sum: {sum_up}")
        #     print(f"down sum: {sum_down}")
        #     break
        # break
        
    return workspace
